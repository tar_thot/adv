<!-- Product 01 -->
<table width="490" border="0" cellspacing="5" cellpadding="0">
    <tr>
        <td width="180" height="150" align="center" bgcolor="#CCCCCC"><img
                src="./resizer.php?imgfile=./photo/hotels/<?= $rows['photo2'] ?>&size=200" width="155" height="115"/>
        </td>
        <td class="list-detail" valign="top">
            <table width="310" height="150" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td height="25" colspan="2" class="h"><strong><?= $rows['hot_name'] ?></strong></td>
                </tr>
                <tr>
                    <td height="20" colspan="2" valign="top" class="location"><strong>Rating :</strong>
                        <?php
                        $istar = 0.5;
                        while ($istar <= $rows['stars']) {
                            if (count(explode(".5", $istar)) != 1) echo "<img src=\"images/star1.gif\" border=\"0\" width=\"6\" height=\"11\" align=\"absmiddle\">";
                            else echo "<img src=\"images/star2.gif\" border=\"0\" width=\"6\" height=\"11\" align=\"absmiddle\">";
                            $istar += 0.5;
                        }
                        ?>
        </td>
                </tr>
                <tr>
                    <td height="20" colspan="2" valign="top" class="location"><strong>Ref
                            :</strong> <?= $rows['hot_ref'] ?></td>
                </tr>
                <tr>
                    <td height="20" colspan="2" valign="top" class="location"><strong>Area
                            :</strong> <?= get_value('hotel_con_area', 'con_id', 'con_name', $rows['area_id']) ?></td>
                </tr>
                <tr>
                    <td height="20" colspan="2" valign="top" class="location"><strong>Room Type
                            :</strong> <?= get_value('hotel_con_room_type', 'con_id', 'con_name', $rows['room_type_id']) ?>
                    </td>
                </tr>
                <tr>
                    <td height="25" align="center" bgcolor="c5d8df"><a href="hotel_detail.php?hotels_id=<?= $rows[0] ?>"
                                                                       class="linkfoot">More detail</a> <img
                            src="images/blank.gif" width="10" height="25" border="0" align="absmiddle"/> | <img
                            src="images/blank.gif" width="10" height="25" border="0" align="absmiddle"/> <a
                            href="hotel_detail.php?hotels_id=<?= $rows[0] ?>#div_book" class="linkfoot">Booking</a></td>
                    <td width="120" align="center" bgcolor="c5d8df" class="h2"></td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<!-- Product 01 -->