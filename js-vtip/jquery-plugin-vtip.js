/**
 Vertigo Tip by www.vertigo-project.com
 Requires jQuery
 */

this.vtip = function () {
    this.xOffset = -10; // x distance from mouse
    this.yOffset = 50; // y distance from mouse       

    $(".vtip").unbind().hover(
        function (e) {
            this.t = this.title;
            this.title = '';
            this.top = (e.pageY - yOffset);
            this.left = (e.pageX + xOffset);

            $("body").append("<p id=\"vtip\"><img id=\"balloon\" src=\"images/vtip_balloon.png\" />" + this.t + "</p>");

            //  $('p#vtip #vtipArrow').attr("src", 'images/vtip_arrow.png');
            $('p#vtip').css("top", this.top + "px").css("left", this.left + "px").show();
            
        },
        function () {
            this.title = this.t;
            $("p#vtip").hide().remove();
        }
    ).mousemove(
        function (e) {
            this.top = (e.pageY - yOffset);
            this.left = (e.pageX + xOffset);

            $("p#vtip").css("top", this.top + "px").css("left", this.left + "px");
        }
    );            
    
};

jQuery(document).ready(function ($) {
    vtip();
})