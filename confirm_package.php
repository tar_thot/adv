<?php session_start(); ?>
<?php include("./administrator/lib/connect.php"); ?>
<?php include("./administrator/lib/function.php"); ?>
<?php include("./administrator/lib/constant.php"); ?>
<?php ob_start(); ?>
<?php
$num_of_items = 0;
$sum_total_price = 0;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<title>Best price Package, Hotels, Transfer, Air Tickets</title>
	<meta name="description" content="ADV Tour., Phi Phi Island Tour & Transfer operator, Hotel & Transport arrangement in the south of Thailand
"/>
	<meta name="keywords"
		  content="Phi Phi Island, Ferries, Package, Tours, Package, Hotels, Diving, Liveaboard, Thailand"/>
	<meta name="Classification" content="World wide tour operate">
	<meta name="Author" content="S.S.ADV. Co., Ltd. Bangkok,Thailand">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link href="css/font.css" rel="stylesheet" type="text/css"/>

	<!-- menu slide -->
	<script type="text/javascript" src="js/script.js"></script>
	<!-- menu slide -->

	<!-- js-vallenato -->
	<script type="text/javascript" src="js-vtip/jquery-1.3.2.min.js"></script>
	<script type="text/javascript" src="js-vtip/jquery-plugin-vtip.js"></script>
	<script src="js-vallenato/vallenato.js" type="text/javascript" charset="utf-8"></script>
	<link rel="stylesheet" href="js-vallenato/vallenato.css" type="text/css" media="screen" charset="utf-8">

	<!-- js-vallenato -->
</head>

<body>
<!-- include top login-->
<?php include("inc_login.php"); ?>
<!-- end include top login -->

<!-- include logo & menu-->
<?php include("inc_menu.php"); ?>
<!-- end include logo & menu -->



<!-- pic slide -->
<!--<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td height="400" bgcolor="#e9f1f4" align="center">

<table width="1000" border="0" align="center" cellpadding="0" cellspacing="0">
<tr>
<td width="250"><iframe src="slide-pic_left/slide.html" frameborder="0" scrolling="no" width="250" height="400"></iframe></td>
<td width="20"><img src="images/blank.gif" width="20" height="20" border="0" /></td>
<td width="730"><iframe src="slide-pic_right/slide.html" frameborder="0" scrolling="no" width="730" height="400"></iframe></td>
</tr>
</table>

</td>
</tr>
<tr>
<td height="15" valign="top" background="images/under_pic.jpg"><img src="images/blank.gif" width="100" height="15" border="0" /></td>
</tr>
</table>-->
<!-- pic slide -->


<table width="1000" border="0" align="center" cellpadding="0" cellspacing="0">
	<tr>
		<td valign="top" class="h1"><img src="images/blank.gif" width="100" height="10" border="0"/></td>
	</tr>
	<tr>
		<td height="40" bgcolor="#FFFFFF" class="h2"><img src="images/blank.gif" width="10" height="40" border="0"
														  align="absmiddle">Package Infomation
		</td>
	</tr>
</table>
<table width="1000" border="0" align="center" cellpadding="0" cellspacing="0">
	<tr>
		<td width="500" align="left" valign="top"><span class="h1"><img src="images/pic_under_line.jpg" width="200"
																		height="15" border="0"/></span></td>
		<td align="right"><span class="h1"><img src="images/pic_under_line1.jpg" width="200" height="15"
												border="0"/></span></td>
	</tr>
</table>

<table width="1000" align="center" border="0" cellspacing="0" cellpadding="2" bgcolor="#e9b120">
	<tr bgcolor="#cccccc">
		<td width="50" align="center">&nbsp;</td>
		<td width="803">&nbsp;</td>
		<td width="125" align="right">&nbsp;</td>
	</tr>
</table>


<!-- Start Get Package Variable -->


<?php
include("value_get.php");
# Get Variable Cart from SESSION

$packages_id = $_SESSION['cart_package']['packages_id'];
$agentgrade_id = $_SESSION['cart_package']['agentgrade_id'];
$res_pac_ratetype_id = $_SESSION['cart_package']['res_pac_ratetype_id'];
$res_pac_room_num = $_SESSION['cart_package']['res_pac_room_num'];
$res_pac_adult_num = $_SESSION['cart_package']['res_pac_adult_num'];
$res_pac_child_num = $_SESSION['cart_package']['res_pac_child_num'];
$res_pac_infant_num = $_SESSION['cart_package']['res_pac_infant_num'];
$items_package_producttype_id_arr = $_SESSION['cart_package']['items_package_producttype_id_arr'];
$items_id_arr = $_SESSION['cart_package']['items_id_arr'];
$items_name_arr = $_SESSION['cart_package']['items_name_arr'];
$items_ratetype_arr = $_SESSION['cart_package']['items_ratetype_arr'];
$res_pac_travel_date_arr = $_SESSION['cart_package']['res_pac_travel_date_arr'];
$res_pac_pickup_from_arr = $_SESSION['cart_package']['res_pac_pickup_from_arr'];
$res_pac_pickup_to_arr = $_SESSION['cart_package']['res_pac_pickup_to_arr'];
$res_pac_room_no_arr = $_SESSION['cart_package']['res_pac_room_no_arr'];
$adults_prices = $_SESSION['cart_package']['adults_prices'];
$childs_prices = $_SESSION['cart_package']['childs_prices'];
$package_prices = $_SESSION['cart_package']['package_prices'];



$i = 0;
if (isset($packages_id)) {
	?>
	<table width="1000" align="center" border="0" cellspacing="0" cellpadding="3" bgcolor="#e9b120">
		<tr bgcolor="#e9b120" height="0">
			<td width="50" align="center"></td>
			<td width="803"></td>
			<td width="125"></td>
		</tr>
		<?php

		$i++;
		$num_of_items++;
		$sum_total_price = $sum_total_price + $package_prices * 1;


		?>
		<tr bgcolor="#EEEEEE">

			<td colspan="3">

				<b>Package Name</b> : <?= get_value('packages', 'pac_id', 'pac_name', $packages_id) ?><br/>
				<b>Package Ref</b> : <?= get_value('packages', 'pac_id', 'pac_ref', $packages_id) ?><br/>
				<?php
				foreach ($items_name_arr as $items_name_ids => $value) {

					//$rpt_item_ids = $items_name_ids + 1;
					?>

					<?php if ($items_package_producttype_id_arr[$items_name_ids] != 7) { // Hotel ?>
						<b>Travel Date
							for </b><?= $value ?> : <?= DateFormat($res_pac_travel_date_arr[$items_name_ids], "s") ?>
						<br/>

					<?php } else { ?>
						<b>Check in Date
							for </b><?= $value ?> : <?= DateFormat($res_pac_travel_date_arr[$items_name_ids], "s") ?>
						<br/>

					<?php } // end if($row[combo_product_category_id] != 7){
					?>

					<?php
					if ($items_package_producttype_id_arr[$items_name_ids] == 2) {
						// Pick Up Transfer
						?>

						<b>Pick Up From for</b> <?= $value ?>: <?= $res_pac_pickup_from_arr[$items_name_ids] ?> <br/>
						<b>Drop Off for</b> <?= $value ?>: <?= $res_pac_pickup_to_arr[$items_name_ids] ?> <br/>
						<b>Room No. for</b> <?= $value ?>: <?= $res_pac_room_no_arr[$items_name_ids] ?> <br/><br/>

						<?php
					} else if ($items_package_producttype_id_arr[$items_name_ids] == 5) { // END if($items_package_producttype_id[$items_name_ids] == 2){
						// Bus Transfer
						?>

						<b>Pick Up From for</b> <?= $value ?>: <?= $res_pac_pickup_from_arr[$items_name_ids] ?> <br/>
						<b>Drop Off for</b> <?= $value ?>: <?= $res_pac_pickup_to_arr[$items_name_ids] ?> <br/>
						<b>Room No. for</b> <?= $value ?>: <?= $res_pac_room_no_arr[$items_name_ids] ?> <br/><br/>

						<?php
					} else if ($items_package_producttype_id_arr[$items_name_ids] == 6) { // END if($items_package_producttype_id[$items_name_ids] == 2){
						// Private Land Transfer
						?>

						<b>Pick Up From for</b> <?= $value ?>: <?= $res_pac_pickup_from_arr[$items_name_ids] ?> <br/>
						<b>Drop Off for</b> <?= $value ?>: <?= $res_pac_pickup_to_arr[$items_name_ids] ?> <br/>
						<b>Room No. for</b> <?= $value ?>: <?= $res_pac_room_no_arr[$items_name_ids] ?> <br/><br/>

						<?php
					} else { // END if($items_package_producttype_id[$items_name_ids] == 2){
						?>

						<?php
					} // END }else{ // END if($items_package_producttype_id[$items_name_ids] == 2){
					?>

					<?php
				}
				?>

				<b>Rate Type</b> : <?= get_value('package_ratetypes', 'pacrt_id', 'pacrt_name', $res_pac_ratetype_id) ?>
				<br/>
				<?= number_format($res_pac_adult_num, 0) ?> Adult
				+ <?= number_format($res_pac_child_num, 0) ?> Child
				+ <?= number_format($res_pac_infant_num, 0) ?> Infant
			</td>

		</tr>
		<tr bgcolor="#e9b120">
			<td colspan="20" height="1"></td>
		</tr>
	</table>
	<?php
}// END if(isset($boattransfers_id_arr)){
?>


<!-- End Get Package Variable -->


<?php
//echo "num_of_items : $num_of_items";
if ($num_of_items > 0) {
	?>
	<table width="1000" align="center" border="0" cellspacing="0" cellpadding="2" bgcolor="#e9b120">
		<tr bgcolor="#eeeeee">
			<td colspan="20" align="right"><strong>Total : THB <?= number_format($sum_total_price, 2) ?></strong></td>
		</tr>
		<tr bgcolor="#e9b120">
			<td colspan="20" height="1"></td>
		</tr>
		<tr bgcolor="#e9b120">
			<td colspan="20" height="1"></td>
		</tr>
	</table>


	<form action="clear_package.php" method="POST">
		<input class="form-control" type="hidden" name="clear_package" value="1"/>
		<p align="center">
			<input class="form-control" type="submit" name="clearorder" value="Cancel Package" style="width:150;"
				   onClick="return(confirm('Are you sure to Cancel ?'))">
		</p>
	</form>


	<table width="1000" border="0" align="center" cellpadding="0" cellspacing="0">
		<tr>
			<td valign="top" class="h1"><img src="images/blank.gif" width="100" height="10" border="0"/></td>
		</tr>
		<tr>
			<td height="40" bgcolor="#FFFFFF" class="h2"><img src="images/blank.gif" width="10" height="40" border="0"
															  align="absmiddle">Reservation Form
			</td>
		</tr>
	</table>
	<table width="1000" border="0" align="center" cellpadding="0" cellspacing="0">
		<tr>
			<td width="500" align="left" valign="top"><span class="h1"><img src="images/pic_under_line.jpg" width="200"
																			height="15" border="0"/></span></td>
			<td align="right"><span class="h1"><img src="images/pic_under_line1.jpg" width="200" height="15"
													border="0"/></span></td>
		</tr>
	</table>

	<script Language="JavaScript">
		function check(theForm) {
			if (theForm.firstname.value == "") {
				alert("Please enter a value for the \"First Name\" field.");
				theForm.firstname.focus();
				return (false);
			}
			if (theForm.lastname.value == "") {
				alert("Please enter a value for the \"Last Name\" field.");
				theForm.lastname.focus();
				return (false);
			}

			if (theForm.email.value == "") {
				alert("Please enter a value for the \"E-mail\" field.");
				theForm.email.focus();
				return (false);
			}
			if (theForm.email.value.indexOf("@") < 0 || theForm.email.value.indexOf(".") < 0) {
				alert("Your \"E-mail\" is invalid");
				theForm.email.focus();
				return (false);
			}

			var check_term = document.form.check_term.checked;
			if (check_term == "") {
				alert("Terms & Conditions is required.");
				form.check_term.focus();
				return (false);
			}

			if (theForm.verify_code.value == "") {
				alert("Please enter a value for the \"Verify Code\" field.");
				theForm.verify_code.focus();
				return (false);
			}
			return (true);
		}
	</script>
	<table width="1000" align="center" border="0" cellspacing="1" cellpadding="3" bgcolor="#ffffff">
		<tr bgcolor="#cccccc">
			<td align="left"><strong>PERSONAL INFORMATION</strong></td>
		</tr>
	</table>
	<form method="POST" action="save_package.php" onSubmit="return check(this)" name="form" id="form">
		<input class="form-control" type="hidden" name="sum_total_price" value="<?= $sum_total_price ?>"/>

		<?php if (!$client_id) { ?>

			<table width="1000" align="center" border="0" cellspacing="1" cellpadding="3" bgcolor="#eeeeee">
				<tr>
					<td width="30%" align="right"><strong>Title Name : </strong></td>
					<td width="70%"><select name="titlename_id">

							<?php listbox('lis_titlename', 'lis_id', 'lis_name', $titlename_id, 'N'); ?>
						</select>
					</td>
				</tr>
				<tr>
					<td width="30%" align="right"><strong>First Name : </strong></td>
					<td width="70%">
						<input class="form-control" type="text" name="firstname" style="width:200px;"><strong
							style="color:#FF0000;"> * </strong><br/></td>
				</tr>
				<tr>
					<td width="30%" align="right"><strong>Last Name : </strong></td>
					<td width="70%">
						<input class="form-control" type="text" name="lastname" style="width:200px;"><strong
							style="color:#FF0000;"> * </strong><br/></td>
				</tr>
				<tr>
					<td align="right"><strong>Email Address : </strong></td>
					<td><input class="form-control" type="text" name="email" style="width:200px;"><strong
							style="color:#FF0000;"> * </strong></td>
				</tr>


				<tr>
					<td align="right"><strong>Phone Number : </strong></td>
					<td><input class="form-control" type="text" name="phone" style="width:200px;"></td>
				</tr>

				<tr>
					<td align="right"><strong>Fax Number : </strong></td>
					<td><input class="form-control" type="text" name="fax_number" style="width:200px;"></td>
				</tr>

				<tr>
					<td align="right"><strong>Address : </strong></td>
					<td><textarea name="address" rows="6" style="width:60%;"></textarea></td>
				</tr>

				<tr>
					<td align="right"><strong>Remark : </strong></td>
					<td><textarea name="comment" rows="6" style="width:60%;"></textarea></td>
				</tr>

				<tr>
					<td align="right"><strong>Booking By : </strong></td>
					<td><input class="form-control" type="text" name="book_by" style="width:200px;"></td>
				</tr>

				<?php if ($_SESSION['agents']['ag_id']) { ?>
					<tr>
						<td align="right"><strong>Agent Voucher : </strong></td>
						<td><input class="form-control" type="text" name="agent_voucher" style="width:200px;"></td>
					</tr>

					<tr>
						<td align="right"><strong>Cash Collection : </strong></td>
						<td><input class="form-control" type="text" name="cash_collection" style="width:200px;"></td>
					</tr>
				<?php } ?>
				<tr>
					<td align="right"></td>
					<td><input class="form-control" type="checkbox" name="check_term" id="check_term" value="1"> I have
						read and accept all
						<a href="files_upload/Terms_Condition_Tour.pdf" target="_blank">Terms and Conditions</a>.
					</td>
				</tr>

				<tr>
					<td align="right">Verify&nbsp;Code&nbsp;:</td>
					<td valign="top"><img src="./captcha/captcha.php" align="absmiddle"/>
						<input class="form-control" type="text" name="verify_code" id="verify_code" maxlength="5"
							   size="5"/>
						<font color="red">*</font></td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td> * Please enter the code shown above in image format.</td>
				</tr>
				<tr>
					<td colspan="2">&nbsp;</td>
				</tr>
				<tr>
					<td colspan="2" align="center"><input class="form-control" name="submit" type="submit"
														  value="Submit">&nbsp;<input class="form-control" name="reset"
																					  type="reset" value="Reset"></td>
				</tr>
			</table>

		<?php } else { // END if(!$client_id){

			$sql_client = "SELECT * FROM clients WHERE cl_id = $client_id' ";
			$result_client = mysql_query($sql_client);
			$num_client = mysql_num_rows($result_client);
			$row_client = mysql_fetch_array($result_client);

			$client_titlename = get_value('lis_titlename', 'lis_id', 'lis_name', $row_client['titlename_id']);
			?>

			<table width="1000" align="center" border="0" cellspacing="1" cellpadding="3" bgcolor="#eeeeee">
				<tr>
					<td width="30%" align="right"><strong>Title Name : </strong></td>
					<td width="70%"><input class="form-control" type="text" name="titlename" style="width:200px;"
										   value="<?= $client_titlename ?>" readonly="readonly"><br/></td>
					<input class="form-control" type="hidden" name="titlename_id"
						   value="<?= $row_client['titlename_id'] ?>"/>
				</tr>
				<tr>
					<td width="30%" align="right"><strong>First Name : </strong></td>
					<td width="70%">
						<input class="form-control" type="text" name="firstname" style="width:200px;"
							   value="<?= $row_client['cl_name'] ?>" readonly="readonly"><br/></td>
				</tr>
				<tr>
					<td width="30%" align="right"><strong>Last Name : </strong></td>
					<td width="70%">
						<input class="form-control" type="text" name="lastname" style="width:200px;"
							   value="<?= $row_client['cl_lname'] ?>" readonly="readonly"><br/></td>
				</tr>
				<tr>
					<td align="right"><strong>Email Address : </strong></td>
					<td><input class="form-control" type="text" name="email" style="width:200px;"
							   value="<?= $row_client['cl_email'] ?>" readonly="readonly"></td>
				</tr>


				<tr>
					<td align="right"><strong>Phone Number : </strong></td>
					<td><input class="form-control" type="text" name="phone" style="width:200px;"
							   value="<?= $row_client['cl_phone'] ?>" readonly="readonly"></td>
				</tr>

				<tr>
					<td align="right"><strong>Fax Number : </strong></td>
					<td><input class="form-control" type="text" name="fax_number" style="width:200px;"
							   value="<?= $row_client['cl_fax'] ?>" readonly="readonly"></td>
				</tr>

				<tr>
					<td align="right"><strong>Address : </strong></td>
					<td><textarea name="address" rows="6" style="width:60%;"
								  readonly="readonly"><?= $row_client['cl_addr1'] ?></textarea></td>
				</tr>

				<tr>
					<td align="right"><strong>Remark : </strong></td>
					<td><textarea name="comment" rows="6" style="width:60%;"></textarea></td>
				</tr>

				<tr>
					<td align="right"><strong>Booking By : </strong></td>
					<td><input class="form-control" type="text" name="book_by" style="width:200px;"
							   value="<?= $client_titlename ?> <?= $row_client['cl_name'] ?> <?= $row_client['cl_lname'] ?>"
							   readonly="readonly"></td>
				</tr>
				<tr>
					<td align="right"></td>
					<td><input class="form-control" type="checkbox" name="check_term" id="check_term" value="1"> I have
						read and accept all
						<a href="files_upload/Terms_Condition_Tour.pdf">Terms and Conditions</a>.
					</td>
				</tr>

				<tr>
					<td align="right">Verify&nbsp;Code&nbsp;:</td>
					<td valign="top"><img src="./captcha/captcha.php" align="absmiddle"/>
						<input class="form-control" type="text" name="verify_code" id="verify_code" maxlength="5"
							   size="5"/>
						<font color="red">*</font></td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td> * Please enter the code shown above in image format.</td>
				</tr>
				<tr>
					<td colspan="2">&nbsp;</td>
				</tr>
				<tr>
					<td colspan="2" align="center"><input class="form-control" name="submit" type="submit"
														  value="Submit">&nbsp;<input class="form-control" name="reset"
																					  type="reset" value="Reset"></td>
				</tr>
			</table>

		<?php } ?>

	</form>


<?php
}else{ // END if ($num_of_items > 0){
?>
	<table width="1000" align="center" border="0" cellspacing="1" cellpadding="3" bgcolor="#ffffff">
		<tr bgcolor="#eeeeee">
			<td align="center" colspan="3"><strong> ---- Empty Cart. ---- </strong></td>
		</tr>
	</table>
	<?php
} // END }else{ // END if ($num_of_items > 0){
?>


<br/>
<!-- include footer-->
<?php include("inc_footer.php"); ?>
<!-- end include footer -->


</body>
</html>
