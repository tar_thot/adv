<table width="1000" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
        <td height="100" bgcolor="#FFFFFF">

            <script type="text/javascript">
                function frmSearchClick() {
                    document.frm_search.submit();
                }
            </script>
            <form name="frm_search" id="frm_search" action="traintransfer_list.php" method="post">
                <table width="980" border="0" align="center" cellpadding="0" cellspacing="0">
                    <tr>
                        <td height="5" align="center" valign="top"><img src="images/blank.gif" width="100" height="5"
                                                                        border="0"/></td>
                    </tr>
                    <tr>
                        <td width="60"><img src="images/icon_location.jpg" width="50" height="50" hspace="5" vspace="5"
                                            border="0"/></td>
                        <td width="190" valign="middle">
                            <table width="200" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class="h1"><font color="#000000">SEARCH PRODUCTS</font></td>
                                </tr>
                                <tr>
                                    <td class="h4"><font color="#000000">Product: Train Transfer</font></td>
                                </tr>
                                <tr>
                                    <td>www.booking.adv-tour.com</td>
                                </tr>
                            </table>
                        </td>
                        <td width="1" bgcolor="#CCCCCC"><img src="images/blank.gif" width="1" height="50" border="0"/>
                        </td>
                        <td width="9" valign="top"><img src="images/blank.gif" width="9" height="20" border="0"/></td>
                        <td width="720" valign="top">
                            <table width="720" border="0" cellspacing="1" cellpadding="1">
                                <tr>
                                    <td width="40">Name:</td>
                                    <td width="200"><input class="form-control" type="text" name="train_name"
                                                           value="<?= $train_name ?>"/></td>
                                    <td width="40" align="center">From:</td>
                                    <td width="200"><select class="form-control" name="province_id" id="province_id"
                                                            style="width:170px">
                                            <option value="">-- Select --</option>
                                            <?php listbox('traintransfer_con_province', 'con_id', 'con_name', $province_id, 'N'); ?>
                                        </select></td>
                                    <td width="40" align="center">Class:</td>
                                    <td width="200"><select class="form-control" name="traintransfer_class_id"
                                                            id="traintransfer_class_id" style="width:150px;">
                                            <option value="">-- Select --</option>
                                            <?php listbox('traintransfer_class', 'c_id', 'c_name', $traintransfer_class_id, 'N'); ?>
                                        </select></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td height="5" align="center" valign="top"><img src="images/blank.gif" width="100"
                                                                                    height="5" border="0"/></td>
                                </tr>
                                <tr>
                                    <td>Ref:</td>
                                    <td><input class="form-control" type="text" name="train_ref"
                                               value="<?= $train_ref ?>"/></td>
                                    <td align="center">To:</td>
                                    <td><select class="form-control" name="province_to_id" id="province_to_id"
                                                style="width:170px">
                                            <option value="">-- Select --</option>
                                            <?php listbox('traintransfer_con_province', 'con_id', 'con_name', $province_to_id, 'N'); ?>
                                        </select></td>
                                    <td width="40" align="center">Train:</td>
                                    <td width="200"><select class="form-control" name="train_type_id" id="train_type_id"
                                                            style="width:150px;">
                                            <option value="">-- Select --</option>
                                            <?php listbox('traintransfer_con_train', 'con_id', 'con_name', $train_type_id, 'N'); ?>
                                        </select></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td height="5" align="center" valign="top"><img src="images/blank.gif" width="100"
                                                                                    height="5" border="0"/></td>
                                </tr>
                                <tr>
                                    <td width="40">Time(DEP.):</td>
                                    <td width="200"><select class="form-control" name="time_from_id" id="time_from_id">
                                            <option value="">-- Select --</option>
                                            <?php listbox('traintransfer_con_time', 'con_id', 'con_name', $time_from_id, 'N'); ?>
                                        </select></td>
                                    <td width="40" align="center">Time(ARR.):</td>
                                    <td width="200"><select class="form-control" name="time_to_id" id="time_to_id"
                                                            style="width:170px;">
                                            <option value="">-- Select --</option>
                                            <?php listbox('traintransfer_con_time', 'con_id', 'con_name', $time_to_id, 'N'); ?>
                                        </select></td>
                                </tr>
                            </table>
                            <table width="720" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td height="5" align="center" valign="top"><img src="images/blank.gif" width="100"
                                                                                    height="5" border="0"/></td>
                                </tr>
                                <tr>
                                    <!--  <td height="30" align="center" bgcolor="#1e73a4"><a href="#" class="search" onclick="frmSearchClick();">Search</a></td>-->

                                </tr>
                                <tr>
                                    <td>
                                        <div height="30" align="center">
                                            <a href="#" class="btn btn-primary btn-block" onclick="frmSearchClick();">Search</a>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td height="5" align="center" valign="top"><img src="images/blank.gif" width="100"
                                                                                    height="5" border="0"/></td>
                                </tr>
                            </table>

                        </td>
                    </tr>
                </table>
            </form>
        </td>
    </tr>
</table>