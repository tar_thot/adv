<?php session_start(); ?>
<?php include("./administrator/lib/connect.php"); ?>
<?php include("./administrator/lib/function.php"); ?>
<?php include("./administrator/lib/constant.php"); ?>
<?php ob_start(); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<title>Best price Package, Hotels, Transfer, Air Tickets</title>
	<meta name="description" content="ADV Tour., Phi Phi Island Tour & Transfer operator, Hotel & Transport arrangement in the south of Thailand
"/>
	<meta name="keywords"
		  content="Ko Tao Island, Samui Island, Phi Phi Island, Ferries, Boat Transfer, Tours, Package, Hotels, Diving, Liveaboard, Thailand"/>
	<meta name="Classification" content="World wide tour operate">
	<meta name="Author" content="S.S.ADV. Co., Ltd. Bangkok,Thailand">


</head>

<body>

<!-- Detail -->
<br/>

<!-- Start Cal Cart For Activity -->
<?php
include("value_get.php");


# Delete cart by index id

$activities_id_arr = $_SESSION['cart_activity']['activities_id_arr'];
$agentgrade_id_arr = $_SESSION['cart_activity']['agentgrade_id_arr'];
$res_act_travel_date_arr = $_SESSION['cart_activity']['res_act_travel_date_arr'];
$res_act_ratetype_id_arr = $_SESSION['cart_activity']['res_act_ratetype_id_arr'];
$res_act_adult_num_arr = $_SESSION['cart_activity']['res_act_adult_num_arr'];
$res_act_child_num_arr = $_SESSION['cart_activity']['res_act_child_num_arr'];
$res_act_infant_num_arr = $_SESSION['cart_activity']['res_act_infant_num_arr'];
$activity_prices_arr = $_SESSION['cart_activity']['activity_prices_arr'];
$actalloday_id_arr = $_SESSION['cart_activity']['actalloday_id_arr'];
$num_of_people_arr = $_SESSION['cart_activity']['num_of_people_arr'];


if ($_GET['delete_index'] != '') {
	$delete_index = $_GET['delete_index'];
	//echo "<br> Delete Index: $delete_index";

	array_splice($activities_id_arr, $delete_index, 1);
	array_splice($agentgrade_id_arr, $delete_index, 1);
	array_splice($res_act_travel_date_arr, $delete_index, 1);
	array_splice($res_act_ratetype_id_arr, $delete_index, 1);
	array_splice($res_act_adult_num_arr, $delete_index, 1);
	array_splice($res_act_child_num_arr, $delete_index, 1);
	array_splice($res_act_infant_num_arr, $delete_index, 1);
	array_splice($activity_prices_arr, $delete_index, 1);
	array_splice($actalloday_id_arr, $delete_index, 1);
	array_splice($num_of_people_arr, $delete_index, 1);
}


$_SESSION['cart_activity']['activities_id_arr'] = $activities_id_arr;
$_SESSION['cart_activity']['agentgrade_id_arr'] = $agentgrade_id_arr;
$_SESSION['cart_activity']['res_act_travel_date_arr'] = $res_act_travel_date_arr;
$_SESSION['cart_activity']['res_act_ratetype_id_arr'] = $res_act_ratetype_id_arr;
$_SESSION['cart_activity']['res_act_adult_num_arr'] = $res_act_adult_num_arr;
$_SESSION['cart_activity']['res_act_child_num_arr'] = $res_act_child_num_arr;
$_SESSION['cart_activity']['res_act_infant_num_arr'] = $res_act_infant_num_arr;
$_SESSION['cart_activity']['activity_prices_arr'] = $activity_prices_arr;
$_SESSION['cart_activity']['actalloday_id_arr'] = $actalloday_id_arr;
$_SESSION['cart_activity']['num_of_people_arr'] = $num_of_people_arr;

echo "<script type=\"text/javascript\">window.location='cart.php'</script>";
exit();
				
				

?>
<!-- End Cal Cart For Activity -->


<br/>
<!-- Detail -->


</body>
</html>
