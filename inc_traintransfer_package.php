<!-- Product 01 -->
<table width="490" border="0" cellspacing="5" cellpadding="0">
    <tr>
        <td width="180" height="150" align="center" bgcolor="#CCCCCC"><img
                src="./resizer.php?imgfile=./photo/traintransfers/<?= $row_traintransfers['photo2'] ?>&size=200"
                width="155" height="115"/></td>
        <td valign="top">
            <table width="310" height="150" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td height="25" colspan="2" class="">
                        <strong><?= $row_traintransfers['train_name'] ?></strong></td>
                </tr>
                <tr>
                    <td height="20" colspan="2" valign="top" class="location"><strong>Ref
                            :</strong> <?= $row_traintransfers['train_ref'] ?></td>
                </tr>
                <tr>
                    <td height="20" colspan="2" valign="top" class="location"><strong>Train
                            :</strong> <?= get_value('traintransfer_con_train', 'con_id,con_name', $row_traintransfers['train_type_id']) ?>
                    </td>
                </tr>
                <tr>
                    <td height="20" colspan="2" valign="top" class="location"><strong>Train No.
                            :</strong> <?= $row_traintransfers['train_number'] ?></td>
                </tr>
                <tr>
                    <td height="20" colspan="2" valign="top" class="location"><strong>Class
                            :</strong> <?= get_value('traintransfer_class', 'c_id', 'c_name', $row_traintransfers['traintransfer_class_id']) ?>
                    </td>
                </tr>
                <tr>
                    <td height="20" colspan="2" valign="top" class="location"><strong>Rate type
                            :</strong> <?= $tr_ratetype_name ?></td>
                </tr>
                <tr>
                    <td height="20" colspan="2" valign="top" class="location"><strong>From
                            :</strong> <?= get_value('traintransfer_con_province', 'con_id,con_name', $row_traintransfers['province_id']) ?>
                    </td>
                </tr>
                <tr>
                    <td height="20" colspan="2" valign="top" class="location"><strong>To
                            :</strong> <?= get_value('traintransfer_con_province', 'con_id', 'con_name', $row_traintransfers['province_to_id']) ?>
                    </td>
                </tr>
                <tr>
                    <td height="20" colspan="2" valign="top" class="location"><strong>Time
                            :</strong> <?= get_value('traintransfer_con_time', 'con_id', 'con_name', $row_traintransfers['time_from_id']) ?>
                        - <?= get_value('traintransfer_con_time', 'con_id', 'con_name', $row_traintransfers['time_to_id']) ?>
                    </td>
                </tr>

            </table>
        </td>
    </tr>
</table>
<hr>
<!-- Product 01 -->