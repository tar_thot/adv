<?php session_start(); ?>

<?php include("./administrator/lib/connect.php"); ?>
<?php include("./administrator/lib/function.php"); ?>
<?php include("./administrator/lib/constant.php"); ?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Best price Package, Hotels, Transfer, Air Tickets</title>
    <meta name="description"
          content="ADV Tour., Phi Phi Island Tour & Transfer operator, Hotel & Transport arrangement in the south of Thailand"/>
    <meta name="keywords"
          content="Ko Tao Island, Samui Island, Phi Phi Island, Ferries, Boat Transfer, Tours, Package, Hotels, Diving, Liveaboard, Thailand"/>
    <meta name="Classification" content="World wide tour operate">
    <meta name="Author" content="S.S.ADV. Co., Ltd. Bangkok,Thailand">

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link href="css/font.css" rel="stylesheet" type="text/css"/>

    <!-- menu slide -->
    <script type="text/javascript" src="js/script.js"></script>
    <!-- menu slide -->

    <!-- Latest compiled and minified CSS -->


    <!-- js-vallenato -->
    <script type="text/javascript" src="js-vtip/jquery-1.3.2.min.js"></script>
    <script type="text/javascript" src="js-vtip/jquery-plugin-vtip.js"></script>
    <script src="js-vallenato/vallenato.js" type="text/javascript" charset="utf-8"></script>
    <link rel="stylesheet" href="js-vallenato/vallenato.css" type="text/css" media="screen" charset="utf-8">
    <!-- js-vallenato -->


    <script type="text/javascript" src="./javascript/calendar/calendarDateInput.js"></script>
    <script type="text/javascript" src="./javascript/for_popup_calendar/cal2.js"></script>
    <script type="text/javascript" src="./javascript/for_popup_calendar/cal_conf2.js"></script>
</head>

<body>
<!-- include top login-->
<?php include("inc_login.php"); ?>
<!-- end include top login -->

<!-- include logo & menu-->
<?php include("inc_menu.php"); ?>
<!-- end include logo & menu -->

<!-- Detail -->

<!-- Sort by -->

<table width="1000" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
        <td valign="top" class="h1"><img src="images/blank.gif" width="100" height="10" border="0"/></td>
    </tr>
    <tr>
        <td height="40" bgcolor="#FFFFFF" class="h2"><img src="images/blank.gif" width="10" height="40" border="0"
                                                          align="absmiddle"> Registration Form
        </td>
    </tr>
</table>
<table width="1000" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
        <td width="500" align="left" valign="top"><span class="h1"><img src="images/pic_under_line.jpg" width="200"
                                                                        height="15" border="0"/></span></td>
        <td align="right"><span class="h1"><img src="images/pic_under_line1.jpg" width="200" height="15"
                                                border="0"/></span></td>
    </tr>
</table>

<table width="1000" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
        <td height="100" bgcolor="#FFFFFF">

            <script type="text/javascript">
                <!--
                function MM_validateForm() { //v4.0
                    if (document.getElementById) {
                        var i, p, q, nm, test, num, min, max, errors = '', args = MM_validateForm.arguments;
                        for (i = 0; i < (args.length - 2); i += 3) {
                            test = args[i + 2];
                            val = document.getElementById(args[i]);
                            if (val) {
                                nm = val.name;
                                if ((val = val.value) != "") {
                                    if (test.indexOf('isEmail') != -1) {
                                        p = val.indexOf('@');
                                        if (p < 1 || p == (val.length - 1)) errors += '- ' + nm + ' must contain an e-mail address.\n';
                                    } else if (test != 'R') {
                                        num = parseFloat(val);
                                        if (isNaN(val)) errors += '- ' + nm + ' must contain a number.\n';
                                        if (test.indexOf('inRange') != -1) {
                                            p = test.indexOf(':');
                                            min = test.substring(8, p);
                                            max = test.substring(p + 1);
                                            if (num < min || max < num) errors += '- ' + nm + ' must contain a number between ' + min + ' and ' + max + '.\n';
                                        }
                                    }
                                } else if (test.charAt(0) == 'R') errors += '- ' + nm + ' is required.\n';
                            }
                        }
                        if (errors) alert('The following error(s) occurred:\n' + errors);
                        document.MM_returnValue = (errors == '');
                    }
                }
                //-->
            </script>

            <script type="text/javascript">
                function checkForm() {
                    var verify_code = document.frm_register.verify_code.value;
                    var register_type = document.frm_register.register_type.value;
                    //var check_term = document.frm_register.check_term.checked;
                    //var emailFilter = /^.+@.+\..{2,3}$/;
                    //var emailFilter = /^[_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)*\@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*(\.([a-zA-Z]){2,4})$/;

                    //var str = document.frm_register.email_b2c.value;
                    //var strr = document.subscribe.confirm_email.value;

                    //alert('check_term : ' + check_term);
                    //if(str != strr){
                    //		alert ("Confirm E-mail is required");
                    //	return false;
                    //}

                    //if(str == ''){
                    //	alert ("E-mail is required");
                    //	return false;
                    //}


                    //if (!(emailFilter.test(str))) {
                    //alert ("E-mail not valid");
                    //	return false;
                    //	}

                    if (register_type == '') {
                        alert('Register Type is required. ');
                        return false;
                    }

                    if (verify_code == '') {
                        alert('Verify Code is required. ');
                        return false;
                    }

                    //if(check_term == ''){
                    //	alert('Terms & Conditions is required. ');
                    //	return false;
                    //}

                    return true;
                }
            </script>

            <script type="text/javascript">
                function DisplayCal(frm_name) {
                    addCalendar(frm_name, "Select Date", frm_name, "frm_register");
                    setFont("verdana", 9);
                    setWidth(90, 4, 20, 3);
                    setColor("#cccccc", "#cccccc", "#ffffff", "#ffffff", "#333333", "#cccccc", "#333333");
                    setFontColor("#333333", "#333333", "#333333", "#ffffff", "#333333");
                    setFormat("yyyy-mm-dd");
                    setSize(250, 200, -200, 16);
                    showCal(frm_name);
                }
            </script>

            <h4><img src="images/blamk.gif" width="10" height="1"> Please enter below information.</h4>

            <form name="frm_register" id="frm_register" action="send_register.php" method="post"
                  enctype="multipart/form-data" onsubmit="return checkForm();">
                <!-- <input type="hidden" name="cl_id" value="<?= $cl_id ?>">
<input type="hidden" name="ag_id" value="<?= $ag_id ?>">
<input type="hidden" name="product_type" value="1">
<input type="hidden" name="product_category" value="3">
<input type="hidden" name="product_id" value="<?= $tours_id ?>"> -->
                <table width="980" border="0" align="center" cellpadding="0" cellspacing="0">
                    <tr>

                        <td width="1" valign="middle"></td>

                        <td width="1000" valign="top" align="center">
                            <table width="720" border="0" cellspacing="1" cellpadding="1">

                                <script type="text/javascript">
                                    function checkType() {
                                        var register_type = $("#register_type").val();
                                        //alert('register_type : ' + register_type);
                                        //alert('555');
                                        if (register_type == '001') {
                                            $("#tr_b2c_detail").show();
                                            $("#tr_b2b_detail").hide();
                                        } else if (register_type == '002') {
                                            $("#tr_b2c_detail").hide();
                                            $("#tr_b2b_detail").show();
                                        } else {
                                            $("#tr_b2c_detail").hide();
                                            $("#tr_b2b_detail").hide();
                                        }
                                    }
                                </script>

                                <tr>
                                    <td width="300" align="right">Member Type: &nbsp;</td>
                                    <td width="400">
                                        <select name="register_type" id="register_type" class="form-control" role="menu"
                                                onchange="checkType();">
                                            <option value="">-- Select --</option>
                                            <option value="001">Member Registration (B2C)</option>
                                            <option value="002">Member Corporate / Travel Agent (B2B)</option>
                                        </select>
                                    </td>
                                </tr>

                                <tr id="tr_b2c_detail">
                                    <td colspan="2">
                                        <table width="720" border="0" cellspacing="1" cellpadding="1">
                                            <tr>
                                                <td width="300" align="right">Title Name: &nbsp;</td>
                                                <td width="400">
                                                    <select name="titlename_id_b2c" id="titlename_id_b2c"
                                                            class="form-control">
                                                        <?php listbox('lis_titlename', 'lis_id', 'lis_name', $titlename_id, 'N'); ?>
                                                    </select>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right"><font color="red">*</font>First Name: &nbsp;</td>
                                                <td><input class="form-control" type="text" name="fname_b2c"
                                                           id="fname_b2c" value="" size="40"></td>
                                            </tr>
                                            <tr>
                                                <td align="right"><font color="red">*</font>Last Name: &nbsp;</td>
                                                <td><input class="form-control" type="text" name="lname_b2c"
                                                           id="lname_b2c" value="" size="40"></td>
                                            </tr>
                                            <tr>
                                                <td align="right"><font color="red">*</font>Email Address: &nbsp;</td>
                                                <td><input class="form-control" type="text" name="email_b2c"
                                                           id="email_b2c" value="" size="40"></td>
                                            </tr>
                                            <tr>
                                                <td align="right">Birth Date: &nbsp;</td>
                                                <td>
                                                    <input class="form-control" type="date" name="birth_date"
                                                           value="<?= $today ?>">

                                                    <!-- <input type="date" name="birth_date" value="<?= $today ?>" readonly="readonly" style="width:90px;" />
					                                <a href = "javascript: &nbsp;DisplayCal('birth_date')"><img src ="./javascript/for_popup_calendar/calendar.gif" border = "0" align = "absmiddle"></a>
					                                -->
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right">Passport No. / ID No.: &nbsp;</td>
                                                <td><input class="form-control" type="text" name="passport_b2c"
                                                           id="passport_b2c" value="" size="40"></td>
                                            </tr>
                                            <tr>
                                                <td align="right">Nationality: &nbsp;</td>
                                                <td><input class="form-control" type="text" name="nationality_b2c"
                                                           id="nationality_b2c" value="" size="40"></td>
                                            </tr>
                                            <tr>
                                                <td align="right">Residential Address: &nbsp;</td>
                                                <td><textarea class="form-control" name="address_b2c"
                                                              rows="6"></textarea></td>
                                            </tr>
                                            <tr>
                                                <td align="right">Line ID: &nbsp;</td>
                                                <td><input class="form-control" type="text" name="line_b2c"
                                                           id="line_b2c" value="" size="40"></td>
                                            </tr>
                                            <tr>
                                                <td align="right">Facebook: &nbsp;</td>
                                                <td><input class="form-control" type="text" name="facebook_b2c"
                                                           id="facebook_b2c" value="" size="40"></td>
                                            </tr>
                                            <tr>
                                                <td align="right">Linkedin: &nbsp;</td>
                                                <td><input class="form-control" type="text" name="linkedin_b2c"
                                                           id="linkedin_b2c" value="" size="40"></td>
                                            </tr>

                                        </table>
                                    </td>
                                </tr>

                                <tr id="tr_b2b_detail">
                                    <td colspan="2">
                                        <table width="720" border="0" cellspacing="1" cellpadding="1">
                                            <tr>
                                                <td width="300" align="right"><font color="red">*</font>Company Name :
                                                    &nbsp;</td>
                                                <td width="400"><input class="form-control" type="text"
                                                                       name="company_name" id="company_name" value=""
                                                                       size="40"></td>
                                            </tr>
                                            <tr>
                                                <td align="right"><font color="red">*</font>Email Address (User Name):
                                                    &nbsp;</td>
                                                <td><input class="form-control" type="text" name="email_b2b"
                                                           id="email_b2b" value="" size="40"></td>
                                            </tr>
                                            <tr>
                                                <td align="right">Email Address 2: &nbsp;</td>
                                                <td><input class="form-control" type="text" name="email2_b2b"
                                                           id="email2_b2b" value="" size="40"></td>
                                            </tr>
                                            <tr>
                                                <td align="right">Address : &nbsp;</td>
                                                <td><textarea class="form-control" name="address_b2b"
                                                              rows="6"></textarea></td>
                                            </tr>
                                            <tr>
                                                <td align="right">City : &nbsp;</td>
                                                <td><input class="form-control" type="text" name="city_b2b"
                                                           id="city_b2b" value="" size="40"></td>
                                            </tr>
                                            <tr>
                                                <td align="right">Province : &nbsp;</td>
                                                <td><input class="form-control" type="text" name="province_b2b"
                                                           id="province_b2b" value="" size="40"></td>
                                            </tr>
                                            <tr>
                                                <td align="right">Country : &nbsp;</td>
                                                <td><input class="form-control" type="text" name="country_b2b"
                                                           id="country_b2b" value="" size="40"></td>
                                            </tr>
                                            <tr>
                                                <td align="right">Zip Code : &nbsp;</td>
                                                <td><input class="form-control" type="text" name="zipcode_b2b"
                                                           id="zipcode_b2b" value="" size="40"></td>
                                            </tr>
                                            <tr>
                                                <td align="right">Phone Number 1 : &nbsp;</td>
                                                <td><input class="form-control" type="text" name="phone1_b2b"
                                                           id="phone1_b2b" value="" size="40"></td>
                                            </tr>
                                            <tr>
                                                <td align="right">Phone Number 2 : &nbsp;</td>
                                                <td><input class="form-control" type="text" name="phone2_b2b"
                                                           id="phone2_b2b" value="" size="40"></td>
                                            </tr>
                                            <tr>
                                                <td align="right">Fax Number : &nbsp;</td>
                                                <td><input class="form-control" type="text" name="fax_b2b" id="fax_b2b"
                                                           value="" size="40"></td>
                                            </tr>
                                            <tr>
                                                <td align="right">Website : &nbsp;</td>
                                                <td><input class="form-control" type="text" name="website_b2b"
                                                           id="website_b2b" value="" size="40"></td>
                                            </tr>
                                            <tr>
                                                <td align="right">Company Established Date : &nbsp;</td>
                                                <td>
                                                    <input class="form-control" type="date" name="established_date"
                                                           value="<?= $today ?>">
                                                    <!-- <input type="text" name="established_date" value="<?= $today ?>" readonly="readonly" style="width:90px;" />
                                                    <a href = "javascript:DisplayCal('established_date')"><img src ="./javascript/for_popup_calendar/calendar.gif" border = "0" align = "absmiddle"></a>
                                                    -->
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right">Business Type : &nbsp;</td>
                                                <td><input class="form-control" type="text" name="business_type_b2b"
                                                           id="business_type_b2b" value="" size="40"></td>
                                            </tr>
                                            <tr>
                                                <td align="right">Type of Clients : &nbsp;</td>
                                                <td>
                                                    <select class="form-control" name="client_type" id="client_type">
                                                        <option value="">-- Select --</option>
                                                        <option value="FIT">FIT</option>
                                                        <option value="GROUP">GROUP</option>
                                                    </select>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right">Market Nationality : &nbsp;</td>
                                                <td><input class="form-control" type="text"
                                                           name="market_nationality_b2b" id="market_nationality_b2b"
                                                           value="" size="40"></td>
                                            </tr>
                                            <tr>
                                                <td width="700" colspan="2">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td align="right"><font color="#000000"><b>Contact Person</b></font>
                                                </td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td align="right">Title Name : &nbsp;</td>
                                                <td>
                                                    <select class="form-control" name="titlename_id_c_b2b"
                                                            id="titlename_id_c_b2b">
                                                        <?php listbox('lis_titlename', 'lis_id', 'lis_name', $titlename_id, 'N'); ?>
                                                    </select>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right">Name : &nbsp;</td>
                                                <td><input class="form-control" type="text" name="name_c_b2b"
                                                           id="name_c_b2b" value="" size="40"></td>
                                            </tr>
                                            <tr>
                                                <td align="right">Phone Number : &nbsp;</td>
                                                <td><input class="form-control" type="text" name="phone_c_b2b"
                                                           id="phone_c_b2b" value="" size="40"></td>
                                            </tr>
                                            <tr>
                                                <td align="right">Email : &nbsp;</td>
                                                <td><input class="form-control" type="text" name="email_c_b2b"
                                                           id="email_c_b2b" value="" size="40"></td>
                                            </tr>
                                            <tr>
                                                <td align="right">Line ID : &nbsp;</td>
                                                <td><input class="form-control" type="text" name="line_c_b2b"
                                                           id="line_c_b2b" value="" size="40"></td>
                                            </tr>
                                            <tr>
                                                <td align="right">Linkedin : &nbsp;</td>
                                                <td><input class="form-control" type="text" name="linkedin_c_b2b"
                                                           id="linkedin_c_b2b" value="" size="40"></td>
                                            </tr>
                                            <tr>
                                                <td width="700" colspan="2">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td align="right"><font color="#000000"><b>Account Contact
                                                            Person</b></font></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td align="right">Title Name : &nbsp;</td>
                                                <td>
                                                    <select class="form-control" name="titlename_id_a_b2b"
                                                            id="titlename_id_a_b2b">
                                                        <?php listbox('lis_titlename', 'lis_id', 'lis_name', $titlename_id, 'N'); ?>
                                                    </select>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right">Name : &nbsp;</td>
                                                <td><input class="form-control" type="text" name="name_a_b2b"
                                                           id="name_a_b2b" value="" size="40"></td>
                                            </tr>
                                            <tr>
                                                <td align="right">Phone Number : &nbsp;</td>
                                                <td><input class="form-control" type="text" name="phone_a_b2b"
                                                           id="phone_a_b2b" value="" size="40"></td>
                                            </tr>
                                            <tr>
                                                <td align="right">Email : &nbsp;</td>
                                                <td><input class="form-control" type="text" name="email_a_b2b"
                                                           id="email_a_b2b" value="" size="40"></td>
                                            </tr>
                                            <tr>
                                                <td width="700" colspan="2">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td align="right"><font color="#000000"><b>Reservation Contact
                                                            Person</b></font></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td align="right">Title Name : &nbsp;</td>
                                                <td>
                                                    <select class="form-control" name="titlename_id_r_b2b"
                                                            id="titlename_id_r_b2b">
                                                        <?php listbox('lis_titlename', 'lis_id', 'lis_name', $titlename_id, 'N'); ?>
                                                    </select>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right">Name : &nbsp;</td>
                                                <td><input class="form-control" type="text" name="name_r_b2b"
                                                           id="name_r_b2b" value="" size="40"></td>
                                            </tr>
                                            <tr>
                                                <td align="right">Phone Number : &nbsp;</td>
                                                <td><input class="form-control" type="text" name="phone_r_b2b"
                                                           id="phone_r_b2b" value="" size="40"></td>
                                            </tr>
                                            <tr>
                                                <td align="right">Email : &nbsp;</td>
                                                <td><input class="form-control" type="text" name="email_r_b2b"
                                                           id="email_r_b2b" value="" size="40"></td>
                                            </tr>
                                            <tr>
                                                <td width="700" colspan="2">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td align="right"><font color="#000000"><b>Required Documents</b></font>
                                                </td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td align="right">Valid Official Tourism License<br/>(if applicable) :
                                                    &nbsp;</td>
                                                <td><input class="form-control" type="file" name="photo_file1"
                                                           id="photo_file1" value="Choose File"/></td>
                                            </tr>
                                            <tr>
                                                <td width="700" colspan="2">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td align="right">Company Registration : &nbsp;</td>
                                                <td><input class="form-control" type="file" name="photo_file2"
                                                           id="photo_file2" value="Choose File"/></td>
                                            </tr>
                                            <tr>
                                                <td width="700" colspan="2">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td align="right">ID Card Copy of Authorized Person : &nbsp;</td>
                                                <td><input class="form-control" type="file" name="photo_file3"
                                                           id="photo_file3" value="Choose File"/></td>
                                            </tr>
                                            <tr>
                                                <td width="700" colspan="2">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td align="right">House Registration Copy of Authorized Person :
                                                    &nbsp;</td>
                                                <td><input class="form-control" type="file" name="photo_file4"
                                                           id="photo_file4" value="Choose File"/></td>
                                            </tr>
                                            <tr>
                                                <td width="700" colspan="2">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td align="right">&nbsp;</td>
                                                <td>
                                                    <div
                                                        style="font-size:0.9em; color:#C00; padding:5px; width:300px; background-color:#FFE0DD; position:relative;">
                                                        - Please use file size less than 2MB.<br/>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="700" colspan="2">&nbsp;</td>
                                            </tr>

                                        </table>

                                    </td>
                                </tr>
                                <tr>
                                    <td width="300" align="right"><font color="red">*</font>Verify&nbsp;Code&nbsp; :
                                        &nbsp;</td>
                                    <td width="400">
                                        <img src="./captcha/captcha.php"/>

                                        <input class="form-control" type="text" name="verify_code" id="verify_code"
                                               maxlength="5" size="5"/>

                                    </td>
                                </tr>

                            </table>

                            <table width="720" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td height="5" align="center" valign="top"><img src="images/blank.gif" width="100"
                                                                                    height="5" border="0"/></td>
                                </tr>
                                <tr>
                                    <td width="300" align="right"></td>
                                    <td width="400" align="center"><input class="form-control btn btn-info"
                                                                          type="submit" name="Submit" value="Submit"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td height="5" align="center" valign="top"><img src="images/blank.gif" width="100"
                                                                                    height="5" border="0"/></td>
                                </tr>
                            </table>

                        </td>
                    </tr>
                </table>
            </form>
        </td>
    </tr>
</table>

<script type="text/javascript">
    checkType();
</script>


<br/>

<!-- Hotel Photo-->

<!-- Hotel Map--><!-- Hotel Map-->

<!-- Hotel Map-->
<!-- Hotel Map-->

<!-- Detail -->

<!-- include footer-->
<?php include("inc_footer.php"); ?>
<!-- end include footer -->


</body>
</html>
