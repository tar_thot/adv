<?php include("./administrator/lib/connect.php"); ?>
<?php include("./administrator/lib/function.php"); ?>
<?php include("./administrator/lib/constant.php"); ?>
<?php ob_start(); ?>
<?php session_start();
error_reporting(E_ALL & ~E_NOTICE); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<title>Best price Package, Hotels, Transfer, Air Tickets</title>
	<meta name="description" content="ADV Tour., Phi Phi Island Tour & Transfer operator, Hotel & Transport arrangement in the south of Thailand
"/>
	<meta name="keywords"
		  content="Ko Tao Island, Samui Island, Phi Phi Island, Ferries, Boat Transfer, Tours, Package, Hotels, Diving, Liveaboard, Thailand"/>
	<meta name="Classification" content="World wide tour operate">
	<meta name="Author" content="S.S.ADV. Co., Ltd. Bangkok,Thailand">


</head>

<body>

<!-- Detail -->
<br/>

<!-- Start Cal Cart For Boat Transfer -->
<?php
//เพิ่มเติม 10/08/58======================================
while (list($xVarName, $xVarvalue) = each($_GET)) {
	${$xVarName} = $xVarvalue;
}


while (list($xVarName, $xVarvalue) = each($_POST)) {
	${$xVarName} = $xVarvalue;
}

while (list($xVarName, $xVarvalue) = each($_FILES)) {
	${$xVarName . "_name"} = $xVarvalue['name'];
	${$xVarName . "_type"} = $xVarvalue['type'];
	${$xVarName . "_size"} = $xVarvalue['size'];
	${$xVarName . "_error"} = $xVarvalue['error'];
	${$xVarName} = $xVarvalue['tmp_name'];
}
//เพิ่มเติม 10/08/58======================================

# Get Variable By POST
//echo "boattransfers_id : $_POST['boattransfers_id'] <br />";
//echo "agentgrade_id : $_POST['agentgrade_id'] <br />";
//echo "Travel Date : $_POST['res_bot_travel_date'] <br />";
//echo "Rate Type ID : $_POST['res_bot_ratetype_id'] <br />";
//echo "Adult : $_POST['res_bot_adult_num'] <br />";
//echo "Child : $_POST['res_bot_child_num'] <br />";
//echo "Infant : $_POST['res_bot_infant_num'] <br />";
//echo "Ticket No. : $_POST['res_bot_ticket'] <br />";

#Get Variable Allocation Value
$sql_boattransfer_allocationdaily = "select * from boattransfer_allocationdaily where boattransfers_id = " . $_POST['boattransfers_id'];
$sql_boattransfer_allocationdaily .= " AND boaalloday_date = '" . $_POST['res_bot_travel_date'] . "'";

//echo "sql_boattransfer_allocationdaily : $sql_boattransfer_allocationdaily <br />";
$result_boattransfer_allocationdaily = mysql_query($sql_boattransfer_allocationdaily);
$row_boattransfer_allocationdaily = mysql_fetch_array($result_boattransfer_allocationdaily);
$boaalloday_value = $row_boattransfer_allocationdaily['boaalloday_value'] * 1;
$boaalloday_id = $row_boattransfer_allocationdaily['0'];
//echo "Allocation :  $boaalloday_value <br />";
//echo "boaalloday_id :  $boaalloday_id <br />";

#Get Variable Number of people
//$num_of_people = $_POST['res_bot_adult_num']*1 + $_POST['res_bot_child_num']*1 + $_POST['res_bot_infant_num']*1 ;
$num_of_people = $_POST['res_bot_adult_num'] * 1 + $_POST['res_bot_child_num'] * 1;
//echo "num_of_people :  $num_of_people <br />";


#Check Number of people > Allocation Value
if ($num_of_people > $boaalloday_value) {
	//echo "num_of_people > boaalloday_value : So Can Not Book " ;
	$alert_text = '';
	$alert_text .= ' Cannot add to cart. The allotment of ' . DateFormat($_POST['res_bot_travel_date'], "s");
	$alert_text .= ' is  ' . $boaalloday_value . ' .';
	$alert_text .= ' \\n You may send us booking request to check the availability.  Tel. 076 – 232 561, 232 562, 232 095  Email ';
	echo "<script type=\"text/javascript\">alert('$alert_text');</script>";
	echo "<script type=\"text/javascript\">window.location='boattransfer_detail.php?boattransfers_id= " . $_POST['boattransfers_id'] . "'</script>";
	exit();
} else {
	//echo "num_of_people <= boaalloday_value : So Can Book " ;
}
//echo "<br />";


#Get Price for Adult, Child, Infant

#Get period ID
$sql_boattransfer_period = "select * from boattransfer_period where boattransfers_id = '" . $_POST['boattransfers_id'] . "' ";
$sql_boattransfer_period .= "AND ('" . $_POST['res_bot_travel_date'] . "' BETWEEN botpe_datefrom AND botpe_dateto) ";
//echo "sql_boattransfer_period : $sql_boattransfer_period <br />";
$result_boattransfer_period = mysql_query($sql_boattransfer_period);
$row_boattransfer_period = mysql_fetch_array($result_boattransfer_period);
$botpe_name = $row_boattransfer_period['botpe_name'];
$boattransfer_period_id = $row_boattransfer_period['0'];
//echo "botpe_name  : $botpe_name  <br />";
//echo "boattransfer_period_id  : $boattransfer_period_id  <br />";


#Check Period (18-12-14)
if (!isset($botpe_name)) {

	$alert_text = '';
	$alert_text .= ' Cannot add to cart. Please select other date.';

	echo "<script type=\"text/javascript\">alert('$alert_text');</script>";
	echo "<script type=\"text/javascript\">window.location='boattransfer_detail.php?boattransfers_id=" . $_POST['boattransfers_id'] . "'</script>";
	exit();
} else {

}


#Get rate_1, rate_2, rate_3
$sql_boattransfer_rates = "select * from boattransfer_rates where boattransfers_id = " . $_POST['boattransfers_id'];

if ($_POST['boattransferpricetype_id'] != 1 || $_POST['agentgrade_id'] == 0) {
	$sql_boattransfer_rates .= " AND agentgrade_id = " . $_POST['agentgrade_id'];
} else {
	$sql_boattransfer_rates .= " AND agentgrade_id = '0' ";
}

//$sql_boattransfer_rates .= "AND agentgrade_id = '$_POST['agentgrade_id']' ";
$sql_boattransfer_rates .= " AND boattransferratetype_id = " . $_POST['res_bot_ratetype_id'];
$sql_boattransfer_rates .= " AND boattransferperiod_id = '$boattransfer_period_id' ";
//echo "sql_boattransfer_rates : $sql_boattransfer_rates <br />";
$result_boattransfer_rates = mysql_query($sql_boattransfer_rates);
$row_boattransfer_rates = mysql_fetch_array($result_boattransfer_rates);


if ($_POST['boattransferpricetype_id'] != 1 || $_POST['agentgrade_id'] == 0) {
	$rate_1 = $row_boattransfer_rates['rate_1'];
	$adults_prices = $rate_1 * $_POST['res_bot_adult_num'];
	//echo "rate_1 : $rate_1 <br />";
	//echo "adults_prices : $adults_prices <br />";
	$rate_2 = $row_boattransfer_rates['rate_2'];
	$childs_prices = $rate_2 * $_POST['res_bot_child_num'];
	//echo "rate_2 : $rate_2 <br />";
	//echo "childs_prices : $childs_prices <br />";
	$rate_3 = $row_boattransfer_rates['rate_3'];
	$infants_prices = $rate_3 * $_POST['res_bot_infant_num'];
	//echo "rate_3 : $rate_3 <br />";
	//echo "infants_prices : $infants_prices <br />";
} else {

	$rate_1 = $row_boattransfer_rates['rate_1'];
	$rate_2 = $row_boattransfer_rates['rate_2'];
	$rate_3 = $row_boattransfer_rates['rate_3'];

	$sql_percents = "select * from boattransfer_ratepercents where boattransfers_id = " . $_POST['boattransfers_id'];

	$sql_percents .= " and agentgrade_id = " . $_POST['agentgrade_id'];
	$sql_percents .= "order by botrateper_id DESC ";

	$result_percents = mysql_query($sql_percents);
	$row_percents = mysql_fetch_array($result_percents);

	$per_discount = 100 - $row_percents['botrateper_discount'];
	$price_discount_1 = ($per_discount * $rate_1) / 100;
	$price_discount_2 = ($per_discount * $rate_2) / 100;
	$price_discount_3 = ($per_discount * $rate_3) / 100;

	$adults_prices = $price_discount_1 * $_POST['res_bot_adult_num'];
	$childs_prices = $price_discount_2 * $_POST['res_bot_child_num'];
	$infants_prices = $price_discount_3 * $_POST['res_bot_infant_num'];

}


$boattransfer_prices = $adults_prices + $childs_prices + $infants_prices;
//echo "boattransfer_prices : $boattransfer_prices <br />";


# Add to cart

# Get Variable Cart from SESSION
$boattransfers_id_arr = $_SESSION['cart_boat']['boattransfers_id_arr'];
$agentgrade_id_arr = $_SESSION['cart_boat']['agentgrade_id_arr'];
$res_bot_travel_date_arr = $_SESSION['cart_boat']['res_bot_travel_date_arr'];
$res_bot_ratetype_id_arr = $_SESSION['cart_boat']['res_bot_ratetype_id_arr'];
$res_bot_adult_num_arr = $_SESSION['cart_boat']['res_bot_adult_num_arr'];
$res_bot_child_num_arr = $_SESSION['cart_boat']['res_bot_child_num_arr'];
$res_bot_infant_num_arr = $_SESSION['cart_boat']['res_bot_infant_num_arr'];
$res_bot_ticket_arr = $_SESSION['cart_boat']['res_bot_ticket_arr'];
$adults_prices_arr = $_SESSION['cart_boat']['adults_prices_arr'];
$childs_prices_arr = $_SESSION['cart_boat']['childs_prices_arr'];
$boattransfer_prices_arr = $_SESSION['cart_boat']['boattransfer_prices_arr'];
$boaalloday_id_arr = $_SESSION['cart_boat']['boaalloday_id_arr'];
$num_of_people_arr = $_SESSION['cart_boat']['num_of_people_arr'];

# Add Variable POST  to  Variable Cart
$boattransfers_id_arr[] = $_POST['boattransfers_id'];
$agentgrade_id_arr[] = $_POST['agentgrade_id'];
$res_bot_travel_date_arr[] = $_POST['res_bot_travel_date'];
$res_bot_ratetype_id_arr[] = $_POST['res_bot_ratetype_id'];
$res_bot_adult_num_arr[] = $_POST['res_bot_adult_num'];
$res_bot_child_num_arr[] = $_POST['res_bot_child_num'];
$res_bot_infant_num_arr[] = $_POST['res_bot_infant_num'];
$res_bot_ticket_arr[] = $_POST['res_bot_ticket'];
$adults_prices_arr[] = $adults_prices;
$childs_prices_arr[] = $childs_prices;
$boattransfer_prices_arr[] = $boattransfer_prices;
$boaalloday_id_arr[] = $boaalloday_id;
$num_of_people_arr[] = $num_of_people;

# Add Variable Cart to Variable SESSION
$_SESSION['cart_boat']['boattransfers_id_arr'] = $boattransfers_id_arr;
$_SESSION['cart_boat']['agentgrade_id_arr'] = $agentgrade_id_arr;
$_SESSION['cart_boat']['res_bot_travel_date_arr'] = $res_bot_travel_date_arr;
$_SESSION['cart_boat']['res_bot_ratetype_id_arr'] = $res_bot_ratetype_id_arr;
$_SESSION['cart_boat']['res_bot_adult_num_arr'] = $res_bot_adult_num_arr;
$_SESSION['cart_boat']['res_bot_child_num_arr'] = $res_bot_child_num_arr;
$_SESSION['cart_boat']['res_bot_infant_num_arr'] = $res_bot_infant_num_arr;
$_SESSION['cart_boat']['res_bot_ticket_arr'] = $res_bot_ticket_arr;
$_SESSION['cart_boat']['adults_prices_arr'] = $adults_prices_arr;
$_SESSION['cart_boat']['childs_prices_arr'] = $childs_prices_arr;
$_SESSION['cart_boat']['boattransfer_prices_arr'] = $boattransfer_prices_arr;
$_SESSION['cart_boat']['boaalloday_id_arr'] = $boaalloday_id_arr;
$_SESSION['cart_boat']['num_of_people_arr'] = $num_of_people_arr;



echo "<script>window.location='cart.php'</script>";

exit();


?>
<!-- End Cal Cart For Boat Transfer -->


<br/>
<!-- Detail -->


</body>
</html>
