<?php session_start(); ?>
<?php include("./administrator/lib/connect.php"); ?>
<?php include("./administrator/lib/function.php"); ?>
<?php include("./administrator/lib/constant.php"); ?>
<?php ob_start(); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<title>Best price Package, Hotels, Transfer, Air Tickets</title>
	<meta name="description" content="ADV Tour., Phi Phi Island Tour & Transfer operator, Hotel & Transport arrangement in the south of Thailand
"/>
	<meta name="keywords"
		  content="Ko Tao Island, Samui Island, Phi Phi Island, Ferries, Boat Transfer, Tours, Package, Hotels, Diving, Liveaboard, Thailand"/>
	<meta name="Classification" content="World wide tour operate">
	<meta name="Author" content="S.S.ADV. Co., Ltd. Bangkok,Thailand">


</head>

<body>

<!-- Detail -->
<br/>

<!-- Start Cal Cart For Pick Up Transfer -->
<?php
include("value_get.php");


# Delete cart by index id

# Get Variable Cart from SESSION
$privatelandtransfers_id_arr = $_SESSION['cart_private']['privatelandtransfers_id_arr'];
$agentgrade_id_arr = $_SESSION['cart_private']['agentgrade_id_arr'];
$privatelandtransfer_type_id_arr = $_SESSION['cart_private']['privatelandtransfer_type_id_arr'];
$res_plt_travel_date_arr = $_SESSION['cart_private']['res_plt_travel_date_arr'];
$res_plt_ratetype_id_arr = $_SESSION['cart_private']['res_plt_ratetype_id_arr'];
$res_plt_car_num_arr = $_SESSION['cart_private']['res_plt_car_num_arr'];
$res_plt_adult_num_arr = $_SESSION['cart_private']['res_plt_adult_num_arr'];
$res_plt_child_num_arr = $_SESSION['cart_private']['res_plt_child_num_arr'];
$res_plt_infant_num_arr = $_SESSION['cart_private']['res_plt_infant_num_arr'];
$privatelandtransfer_prices_arr = $_SESSION['cart_private']['privatelandtransfer_prices_arr'];
$rplt_pickupfrom_arr = $_SESSION['cart_private']['rplt_pickupfrom_arr'];
$rplt_pickupto_arr = $_SESSION['cart_private']['rplt_pickupto_arr'];
$rplt_room_arr = $_SESSION['cart_private']['rplt_room_arr'];


if ($_GET['delete_index'] != '') {
	$delete_index = $_GET['delete_index'];
	//echo "<br> Delete Index: $delete_index";

	array_splice($privatelandtransfers_id_arr, $delete_index, 1);
	array_splice($agentgrade_id_arr, $delete_index, 1);
	array_splice($privatelandtransfer_type_id_arr, $delete_index, 1);
	array_splice($res_plt_travel_date_arr, $delete_index, 1);
	array_splice($res_plt_ratetype_id_arr, $delete_index, 1);
	array_splice($res_plt_car_num_arr, $delete_index, 1);
	array_splice($res_plt_adult_num_arr, $delete_index, 1);
	array_splice($res_plt_child_num_arr, $delete_index, 1);
	array_splice($res_plt_infant_num_arr, $delete_index, 1);
	array_splice($privatelandtransfer_prices_arr, $delete_index, 1);
	array_splice($rplt_pickupfrom_arr, $delete_index, 1);
	array_splice($rplt_pickupto_arr, $delete_index, 1);
	array_splice($rplt_room_arr, $delete_index, 1);

}


# Add Variable Cart to Variable SESSION
$_SESSION['cart_private']['privatelandtransfers_id_arr'] = $privatelandtransfers_id_arr;
$_SESSION['cart_private']['agentgrade_id_arr'] = $agentgrade_id_arr;
$_SESSION['cart_private']['privatelandtransfer_type_id_arr'] = $privatelandtransfer_type_id_arr;
$_SESSION['cart_private']['res_plt_travel_date_arr'] = $res_plt_travel_date_arr;
$_SESSION['cart_private']['res_plt_ratetype_id_arr'] = $res_plt_ratetype_id_arr;
$_SESSION['cart_private']['res_plt_car_num_arr'] = $res_plt_car_num_arr;
$_SESSION['cart_private']['res_plt_adult_num_arr'] = $res_plt_adult_num_arr;
$_SESSION['cart_private']['res_plt_child_num_arr'] = $res_plt_child_num_arr;
$_SESSION['cart_private']['res_plt_infant_num_arr'] = $res_plt_infant_num_arr;
$_SESSION['cart_private']['privatelandtransfer_prices_arr'] = $privatelandtransfer_prices_arr;
$_SESSION['cart_private']['rplt_pickupfrom_arr'] = $rplt_pickupfrom_arr;
$_SESSION['cart_private']['rplt_pickupto_arr'] = $rplt_pickupto_arr;
$_SESSION['cart_private']['rplt_room_arr'] = $rplt_room_arr;

echo "<script type=\"text/javascript\">window.location='cart.php'</script>";
exit();
				
				

?>
<!-- End Cal Cart For Pick Up Transfer -->


<br/>
<!-- Detail -->


</body>
</html>
