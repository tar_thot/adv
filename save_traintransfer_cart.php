<?php session_start(); ?>
<?php include("./administrator/lib/connect.php"); ?>
<?php include("./administrator/lib/function.php"); ?>
<?php include("./administrator/lib/constant.php"); ?>
<?php ob_start(); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<title>Best price Package, Hotels, Transfer, Air Tickets</title>
	<meta name="description" content="ADV Tour., Phi Phi Island Tour & Transfer operator, Hotel & Transport arrangement in the south of Thailand
"/>
	<meta name="keywords"
		  content="Ko Tao Island, Samui Island, Phi Phi Island, Ferries, Boat Transfer, Tours, Package, Hotels, Diving, Liveaboard, Thailand"/>
	<meta name="Classification" content="World wide tour operate">
	<meta name="Author" content="S.S.ADV. Co., Ltd. Bangkok,Thailand">


</head>

<body>

<!-- Detail -->
<br/>

<!-- Start Cal Cart For Train Transfer -->
<?php
//เพิ่มเติม 10/08/58======================================
while (list($xVarName, $xVarvalue) = each($_GET)) {
	${$xVarName} = $xVarvalue;
}


while (list($xVarName, $xVarvalue) = each($_POST)) {
	${$xVarName} = $xVarvalue;
}

while (list($xVarName, $xVarvalue) = each($_FILES)) {
	${$xVarName . "_name"} = $xVarvalue['name'];
	${$xVarName . "_type"} = $xVarvalue['type'];
	${$xVarName . "_size"} = $xVarvalue['size'];
	${$xVarName . "_error"} = $xVarvalue['error'];
	${$xVarName} = $xVarvalue['tmp_name'];
}
//เพิ่มเติม 10/08/58======================================

# Get Variable By POST
//echo "traintransfers_id : $_POST[traintransfers_id] <br />";
//echo "agentgrade_id : $_POST[agentgrade_id] <br />";
//echo "Travel Date : $_POST[res_tra_travel_date] <br />";
//echo "Rate Type ID : $_POST[res_tra_ratetype_id] <br />";
//echo "Adult : $_POST[res_tra_adult_num] <br />";
//echo "Child : $_POST[res_tra_child_num] <br />";
//echo "Infant : $_POST[res_tra_infant_num] <br />";

#Get Variable Allocation Value
$sql_traintransfer_allocationdaily = "select * from traintransfer_allocationdaily where traintransfers_id = '" . $_POST['traintransfers_id'] . "' ";
$sql_traintransfer_allocationdaily .= "AND traalloday_date = '" . $_POST['res_tra_travel_date'] . "' ";

//echo "sql_traintransfer_allocationdaily : $sql_traintransfer_allocationdaily <br />";

$result_traintransfer_allocationdaily = mysql_query($sql_traintransfer_allocationdaily);
$row_traintransfer_allocationdaily = mysql_fetch_array($result_traintransfer_allocationdaily);
$traalloday_value = $row_traintransfer_allocationdaily['traalloday_value'] * 1;
$traalloday_id = $row_traintransfer_allocationdaily[0];
//echo "Allocation :  $traalloday_value <br />";
//echo "actalloday_id :  $traalloday_id <br />";

#Get Variable Number of people
//$num_of_people = $_POST['res_bot_adult_num']*1 + $_POST['res_bot_child_num']*1 + $_POST['res_bot_infant_num']*1 ;
$num_of_people = $_POST['res_tra_adult_num'] * 1 + $_POST['res_tra_child_num'] * 1;
//echo "num_of_people :  $num_of_people <br />";


#Check Number of people > Allocation Value
if ($num_of_people > $traalloday_value) {
	//echo "num_of_people > traalloday_value : So Can Not Book " ;
	$alert_text = '';
	$alert_text .= ' Cannot add to cart. The allotment of ' . DateFormat($_POST['res_tra_travel_date'], "s");
	$alert_text .= ' is  ' . $traalloday_value . ' .';
	$alert_text .= ' \\n You may send us booking request to check the availability.  Tel. 076 – 232 561, 232 562, 232 095  Email ';

	echo "<script type=\"text/javascript\">alert('$alert_text');</script>";
	echo "<script type=\"text/javascript\">window.location='traintransfer_detail.php?traintransfers_id=" . $_POST['traintransfers_id'] . "'</script>";
	exit();
} else {
	//echo "num_of_people <= toualloday_value : So Can Book " ;
}
//echo "<br />";


#Get Price for Adult, Child, Infant

#Get period ID
$sql_traintransfer_period = "select * from traintransfer_period where traintransfers_id = '" . $_POST['traintransfers_id'] . "' ";
$sql_traintransfer_period .= "AND ('" . $_POST['res_tra_travel_date'] . "' BETWEEN trape_datefrom AND trape_dateto) ";
//echo "sql_traintransfer_period : $sql_traintransfer_period <br />";
$result_traintransfer_period = mysql_query($sql_traintransfer_period);
$row_traintransfer_period = mysql_fetch_array($result_traintransfer_period);
$trape_name = $row_traintransfer_period['trape_name'];
$traintransfer_period_id = $row_traintransfer_period[0];
//echo "trape_name  : $trape_name  <br />";
//echo "traintransfer_period_id  : $traintransfer_period_id  <br />";


#Check Period
if (!$trape_name) {

	$alert_text = '';
	$alert_text .= ' Cannot add to cart. Please select other date.';

	echo "<script type=\"text/javascript\">alert('$alert_text');</script>";
	echo "<script type=\"text/javascript\">window.location='traintransfer_detail.php?traintransfers_id=" . $_POST['traintransfers_id'] . "'</script>";
	exit();
} else {

}


#Get rate_1, rate_2, rate_3
$sql_traintransfer_rates = "select * from traintransfer_rates where traintransfers_id = '" . $_POST['traintransfers_id'] . "' ";

if ($_POST['traintransferpricetype_id'] != 1 || $_POST['agentgrade_id'] == 0) {
	$sql_traintransfer_rates .= "AND agentgrade_id = '" . $_POST['agentgrade_id'] . "' ";
} else {
	$sql_traintransfer_rates .= "AND agentgrade_id = '0' ";
}

$sql_traintransfer_rates .= "AND traintransferratetype_id = '" . $_POST['res_tra_ratetype_id'] . "' ";
$sql_traintransfer_rates .= "AND traintransferperiod_id = '$traintransfer_period_id' ";
//echo "sql_traintransfer_rates : $sql_traintransfer_rates <br />";
$result_traintransfer_rates = mysql_query($sql_traintransfer_rates);
$row_traintransfer_rates = mysql_fetch_array($result_traintransfer_rates);

if ($_POST['traintransferpricetype_id'] != 1 || $_POST['agentgrade_id'] == 0) {
	$rate_1 = $row_traintransfer_rates['rate_1'];
	$adults_prices = $rate_1 * $_POST['res_tra_adult_num'];
	//echo "rate_1 : $rate_1 <br />";
	//echo "adults_prices : $adults_prices <br />";
	$rate_2 = $row_traintransfer_rates['rate_2'];
	$childs_prices = $rate_2 * $_POST['res_tra_child_num'];
	//echo "rate_2 : $rate_2 <br />";
	//echo "childs_prices : $childs_prices <br />";
	$rate_3 = $row_traintransfer_rates['rate_3'];
	$infants_prices = $rate_3 * $_POST['res_tra_infant_num'];
	//echo "rate_3 : $rate_3 <br />";
	//echo "infants_prices : $infants_prices <br />";
} else {

	$rate_1 = $row_traintransfer_rates['rate_1'];
	$rate_2 = $row_traintransfer_rates['rate_2'];
	$rate_3 = $row_traintransfer_rates['rate_3'];

	$sql_percents = "select * from traintransfer_ratepercents where traintransfers_id = '" . $_POST['traintransfers_id'] . "' ";

	$sql_percents .= "and agentgrade_id = '" . $_POST['agentgrade_id'] . "' ";
	$sql_percents .= "order by trarateper_id DESC ";

	$result_percents = mysql_query($sql_percents);
	$row_percents = mysql_fetch_array($result_percents);

	$per_discount = 100 - $row_percents['trarateper_discount'];
	$price_discount_1 = ($per_discount * $rate_1) / 100;
	$price_discount_2 = ($per_discount * $rate_2) / 100;
	$price_discount_3 = ($per_discount * $rate_3) / 100;

	$adults_prices = $price_discount_1 * $_POST['res_tra_adult_num'];
	$childs_prices = $price_discount_2 * $_POST['res_tra_child_num'];
	$infants_prices = $price_discount_3 * $_POST['res_tra_infant_num'];

}


$traintransfer_prices = $adults_prices + $childs_prices + $infants_prices;
//echo "traintransfer_prices : $traintransfer_prices <br />";


# Add to cart

# Get Variable Cart from SESSION
$traintransfers_id_arr = $_SESSION['cart_train']['traintransfers_id_arr'];
$agentgrade_id_arr = $_SESSION['cart_train']['agentgrade_id_arr'];
$res_tra_travel_date_arr = $_SESSION['cart_train']['res_tra_travel_date_arr'];
$res_tra_ratetype_id_arr = $_SESSION['cart_train']['res_tra_ratetype_id_arr'];
$res_tra_adult_num_arr = $_SESSION['cart_train']['res_tra_adult_num_arr'];
$res_tra_child_num_arr = $_SESSION['cart_train']['res_tra_child_num_arr'];
$res_tra_infant_num_arr = $_SESSION['cart_train']['res_tra_infant_num_arr'];
$adults_prices_arr = $_SESSION['cart_train']['adults_prices_arr'];
$childs_prices_arr = $_SESSION['cart_train']['childs_prices_arr'];
$traintransfer_prices_arr = $_SESSION['cart_train']['traintransfer_prices_arr'];
$traalloday_id_arr = $_SESSION['cart_train']['traalloday_id_arr'];
$num_of_people_arr = $_SESSION['cart_train']['num_of_people_arr'];

# Add Variable POST  to  Variable Cart
$traintransfers_id_arr[] = $_POST['traintransfers_id'];
$agentgrade_id_arr[] = $_POST['agentgrade_id'];
$res_tra_travel_date_arr[] = $_POST['res_tra_travel_date'];
$res_tra_ratetype_id_arr[] = $_POST['res_tra_ratetype_id'];
$res_tra_adult_num_arr[] = $_POST['res_tra_adult_num'];
$res_tra_child_num_arr[] = $_POST['res_tra_child_num'];
$res_tra_infant_num_arr[] = $_POST['res_tra_infant_num'];
$adults_prices_arr[] = $adults_prices;
$childs_prices_arr[] = $childs_prices;
$traintransfer_prices_arr[] = $traintransfer_prices;
$traalloday_id_arr[] = $traalloday_id;
$num_of_people_arr[] = $num_of_people;

# Add Variable Cart to Variable SESSION
$_SESSION['cart_train']['traintransfers_id_arr'] = $traintransfers_id_arr;
$_SESSION['cart_train']['agentgrade_id_arr'] = $agentgrade_id_arr;
$_SESSION['cart_train']['res_tra_travel_date_arr'] = $res_tra_travel_date_arr;
$_SESSION['cart_train']['res_tra_ratetype_id_arr'] = $res_tra_ratetype_id_arr;
$_SESSION['cart_train']['res_tra_adult_num_arr'] = $res_tra_adult_num_arr;
$_SESSION['cart_train']['res_tra_child_num_arr'] = $res_tra_child_num_arr;
$_SESSION['cart_train']['res_tra_infant_num_arr'] = $res_tra_infant_num_arr;
$_SESSION['cart_train']['adults_prices_arr'] = $adults_prices_arr;
$_SESSION['cart_train']['childs_prices_arr'] = $childs_prices_arr;
$_SESSION['cart_train']['traintransfer_prices_arr'] = $traintransfer_prices_arr;
$_SESSION['cart_train']['traalloday_id_arr'] = $traalloday_id_arr;
$_SESSION['cart_train']['num_of_people_arr'] = $num_of_people_arr;



echo "<script type=\"text/javascript\">window.location='cart.php'</script>";
exit();


?>
<!-- End Cal Cart For Activity -->


<br/>
<!-- Detail -->


</body>
</html>
