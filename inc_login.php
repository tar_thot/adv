<?php
if (isset($_SESSION['agents'])) {
    $agent_id = $_SESSION['agents']['ag_id'];
    $agent_name = $_SESSION['agents']['ag_name'];

    $agentpaytype_id = $_SESSION['agents']['agentpaytype_id'];
    $agent_balance_credit = $_SESSION['agents']['ag_balance_credit'];
    $agent_min_credit = $_SESSION['agents']['ag_min_credit'];

} else if (isset($_SESSION['clients'])) {
    $client_id = $_SESSION['clients']['cl_id'];
    $client_name = $_SESSION['clients']['cl_name'];
    $client_lname = $_SESSION['clients']['cl_lname'];
    $client_title = $_SESSION['clients']['titlename_id'];
    $client_titlename = get_value('lis_titlename', 'lis_id', 'lis_name', $client_title);

}
?>
<div id="header-nav">
    <div class="container" style="width: 1001px;">
        <div class="row">
            <div class="col-md-3 col-sm-3">
                <?php if (isset($agent_id) && !isset($client_id)) { ?>
                    <div id="agent-form">
                        <table width="110%" border="0" class="div-center">
                            <tr>
                                <td><strong><font color="#FFFFFF">Agent name :&nbsp;</font></strong></td>
                                <td><font color="#FFFFFF"><?= $agent_name ?>&nbsp;</font></td>

                                <?php
                                if ($agentpaytype_id == 2) {
                                    ?>
                                    <td><strong>(Credit
                                            :</strong> <?= number_format($agent_balance_credit) ?>
                                        THB<strong>)</strong></td>
                                    <?php
                                } elseif ($agentpaytype_id == 3) {
                                    ?>
                                    <td><strong><font color="#FFFFFF">&nbsp;(Invoice [Prepayment])</font></strong></td>
                                    <?php
                                } else {
                                    ?>
                                    <td><strong><font color="#FFFFFF">&nbsp;(Payment Online)</font></strong></td>
                                    <?php
                                }
                                ?>
                            </tr>
                        </table>
                    </div>

                    <?php
                } else if (!isset($agent_id) && isset($client_id)) { ?>
                    <div id="client-form">
                        <table border="0" class="div-center">
                            <tr>
                                <td><strong><font color="#FFFFFF">Client name:</font></strong></td>
                                <td width="20" align="center">&nbsp;</td>
                                <td><strong><font
                                            color="#FFFFFF"><?= $client_titlename ?> <?= $client_name ?> <?= $client_lname ?></font></strong>
                                </td>
                            </tr>
                        </table>
                    </div>

                    <?php
                }
                ?>
            </div>
            <div class="col-md-5 col-sm-5">
                <?php if (!isset($agent_id) && !isset($client_id)) { ?>
                    <div id="login-form" style="display: none;">
                        <form action="check_login.php" method="post" enctype="multipart/form-data" name="frm_login"
                              onSubmit="MM_validateForm('username','','R','password','','R');return document.MM_returnValue">
                            <div class="signin-form">
                                <table class="div-center" border="0">
                                    <tr>
                                        <td align="center">
                                            <input type="text" name="username" id="username" class="form-control"
                                                   placeholder="Username"/>
                                        </td>
                                        <td width="10" align="center">&nbsp;</td>
                                        <td align="center">
                                            <input type="password" name="password" id="password" class="form-control"
                                                   placeholder="Password"/>
                                        </td>
                                        <td width="10" align="center">&nbsp;</td>
                                        <td width="30" align="center">
                                            <input type="submit" name="button" id="button" value="Login"
                                                   class="btn btn-primary"/>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </form>
                    </div>

                    <?php
                }
                ?>
            </div>

            <div class="col-md-4 col-sm-4">
                <div class="row">
                    <table class="div-center" border="0" align="right" cellpadding="0" cellspacing="0">
                        <tr>
                            <?php if (!isset($agent_id) && !isset($client_id)) { ?>
                            <td width="" align="right"><a href="#" onclick="Showlogin()"><font color="#FFFFFF"><b> Login&nbsp;&nbsp;</b></font></a>
                                <?php }else{ ?>
                            <td width="" align="right"><a href="./logout.php"><font
                                        color="#FFFFFF"><b>Logout&nbsp;</b></font></a>
                                <?php } ?>

                            </td>
                            <td width="" align="center"><font color="#FFFFFF"><b> &nbsp; | &nbsp;</b></font></td>
                            <td width="" align="right"><a href="register.php"><font color="#EC8324"><b>
                                            Register </b></font></a></td>
                            <td width="" align="center"><font color="#FFFFFF"><b> &nbsp; | &nbsp;</b></font></td>
                            <td width="" align="left"><font color="#FFFFFF"><b> &nbsp;Language :&nbsp;</b></font></td>
                            <td width="" align="left"><img src="images/flag_eng.jpg" width="15"
                                                           height="10" hspace="10" border="0"/><img
                                    src="images/flag_tha.jpg" width="15" height="10" border="0"/></td>
                        </tr>
                    </table>
                </div>


            </div>
        </div>
    </div>

</div>
<style type="text/css">
    <!--
    a:link {
        text-decoration: none;
    }

    a:visited {
        text-decoration: none;
    }

    a:hover {
        text-decoration: none;
    }

    a:active {
        text-decoration: none;
    }

    -->
</style>
<script type="text/javascript">
    function Showlogin(s) {

        if (document.getElementById("login-form").style.display == 'none') {
            document.getElementById("login-form").style.display = 'block';
            //alert(document.getElementById("login-form").style.display);
        } else {
            document.getElementById("login-form").style.display = 'none'
        }
    }
    function MM_validateForm() { //v4.0
        if (document.getElementById) {
            var i, p, q, nm, test, num, min, max, errors = '', args = MM_validateForm.arguments;
            for (i = 0; i < (args.length - 2); i += 3) {
                test = args[i + 2];
                val = document.getElementById(args[i]);
                if (val) {
                    nm = val.name;
                    if ((val = val.value) != "") {
                        if (test.indexOf('isEmail') != -1) {
                            p = val.indexOf('@');
                            if (p < 1 || p == (val.length - 1)) errors += '- ' + nm + ' must contain an e-mail address.\n';
                        } else if (test != 'R') {
                            num = parseFloat(val);
                            if (isNaN(val)) errors += '- ' + nm + ' must contain a number.\n';
                            if (test.indexOf('inRange') != -1) {
                                p = test.indexOf(':');
                                min = test.substring(8, p);
                                max = test.substring(p + 1);
                                if (num < min || max < num) errors += '- ' + nm + ' must contain a number between ' + min + ' and ' + max + '.\n';
                            }
                        }
                    } else if (test.charAt(0) == 'R') {
                        if (nm == 'username') nm = 'User Name';
                        if (nm == 'password') nm = 'Password';
                        //if(nm=='verifycode') nm='Verify Code';
                        errors += '- ' + nm + ' is required.\n';
                    }
                }
            }
            if (errors) alert('The following error(s) occurred:\n' + errors);
            document.MM_returnValue = (errors == '');
        }
    }
</script>