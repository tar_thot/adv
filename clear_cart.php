<?php ob_start(); ?>
<?php session_start(); ?>

<?php
if ($_POST['clear_cart'] == 1) {
	//echo "<br><br>Click Clear cart";

	# clear_cart
	unset($_SESSION['cart_boat']);
	unset($_SESSION['cart_pickup']);
	unset($_SESSION['cart_tour']);
	unset($_SESSION['cart_activity']);
	unset($_SESSION['cart_bus']);
	unset($_SESSION['cart_private']);
	unset($_SESSION['cart_hotel']);
	unset($_SESSION['cart_train']);

}
?>

<META HTTP-EQUIV="Refresh" CONTENT="0;URL=cart.php">