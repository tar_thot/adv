<?php session_start(); ?>
<?php include("./administrator/lib/connect.php"); ?>
<?php include("./administrator/lib/function.php"); ?>
<?php include("./administrator/lib/constant.php");
error_reporting(E_ALL & ~E_NOTICE); ?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Best price Package, Hotels, Transfer, Air Tickets</title>
    <meta name="description"
          content="ADV Tour., Phi Phi Island Tour & Transfer operator, Hotel & Transport arrangement in the south of Thailand"/>
    <meta name="keywords"
          content="Ko Tao Island, Samui Island, Phi Phi Island, Ferries, Boat Transfer, Tours, Package, Hotels, Diving, Liveaboard, Thailand"/>
    <meta name="Classification" content="World wide tour operate">

    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet">
    <link href="css/font.css" rel="stylesheet" type="text/css"/>

    <!-- menu slide -->
    <script type="text/javascript" src="js/script.js"></script>
    <!-- menu slide -->


    <!-- js-vallenato -->
    <script type="text/javascript" src="js-vtip/jquery-1.3.2.min.js"></script>
    <script type="text/javascript" src="js-vtip/jquery-plugin-vtip.js"></script>
    <script src="js-vallenato/vallenato.js" type="text/javascript" charset="utf-8"></script>
    <link rel="stylesheet" href="js-vallenato/vallenato.css" type="text/css" media="screen" charset="utf-8">
    <script src="./js/jquery.min.js"></script>
    <script src="./js/bootstrap.min.js"></script>
</head>

<body>
<!-- include top login-->
<?php include("inc_login.php"); ?>
<!-- end include top login -->

<!-- include logo & menu-->
<?php include("inc_menu.php"); ?>
<!-- end include logo & menu -->

<!-- Detail -->

<!-- Sort by -->
<?php
$sql_reservations = "SELECT * FROM `reservations` ";
if (isset($_SESSION['clients']['cl_id'])) {
    $cl_id = $_SESSION['clients']['cl_id'];
    $sql_reservations .= 'where cl_id = ' . $cl_id;

} else if (isset($_SESSION['agents']['ag_id'])) {
    $ag_id = $_SESSION['agents']['ag_id'];
    $sql_reservations .= 'where agents_id = ' . $ag_id;

} else {
    $sql_reservations .= 'where cl_id = -1';
}
if (isset($_POST['bookingno'])) {
    $bookingno = $_POST['bookingno'];
    $email = $_POST['email'];
    $phone = $_POST['phone'];
    $sql_reservations = "SELECT * FROM reservations where res_id_str = '$bookingno' and res_email = '$email' and res_phone = '$phone'";
}
$sql_reservations .= " order by res_id DESC";
$result_reservations = mysql_query($sql_reservations);
//Book Date 	Agent Name	First Name	Last Name	 Service Name
$total = mysql_num_rows($result_reservations);
$limit = 10;
//$i = $start = isset($_GET['start']) ? $_GET['start'] : 1;
$i = isset($_GET['page']) ? $_GET['page'] * $limit - $limit + 1 : 1;
$start = isset($_GET['page']) ? $_GET['page'] * $limit - $limit : 1;
$page_no = isset($_GET['page']) ? $_GET['page'] : 1;
if (isset($_GET['page'])) {
    if ($page_no <= 1) {
        $start = 0;
        $i = 0;
    }

} else {
    $start = 0;
}
$sql_reservations .= " LIMIT $start,$limit ";


$page = ceil($total / $limit);
$result_reservations = mysql_query($sql_reservations);
if (isset($_GET['start']) and $start >= $limit) {
    $i = $start + 1;
}
if ($i == 0) {
    $i = 1;
}


//echo $sql_reservations;
?>
<center>
    <div class="container">
        <div class="">
            <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                    <td valign="top" class="h1"><img src="images/blank.gif" width="100" height="10" border="0"/></td>
                </tr>
                <tr>
                    <td height="40" bgcolor="#FFFFFF" class="h2"><img src="images/blank.gif" width="10" height="40"
                                                                      border="0" align="absmiddle"> History
                    </td>
                    <td height="40" bgcolor="#FFFFFF" class="">
                        <div align="right">  <?= "Data Found : $total Record(s)" ?>&nbsp;</div>
                    </td>

                </tr>
            </table>

            <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="500" align="left" valign="top">
                    <span class="h1"><img src="images/pic_under_line.jpg"
                                          width="200" height="15"
                                          border="0"/></span></td>
                    <td align="right"><span class="h1"><img src="images/pic_under_line1.jpg" width="200" height="15"
                                                            border="0"/></span></td>
                </tr>
            </table>

            <form action="./history.php" method="post">
                <div class="search-booking">
                    <div class="col-md-4 col-sm-4"></div>
                    <div class="col-md-8 col-sm-8" id="search-booking">
                        <table>
                            <tr>
                                <td><input class="form-control" type="email" name="email" value="<?= $email ?>" required

                                           placeholder="Enter Email."/>
                                </td>
                                <td><input class="form-control" type="text" name="phone" value="<?= $phone ?>" required

                                           placeholder="Enter phone no."/>
                                </td>
                                <td>
                                    <input class="form-control" type="text" name="bookingno" value="<?= $bookingno ?>"
                                           required
                                           placeholder="Enter booking no."/>
                                </td>

                                <td>&nbsp;
                                    <button class="btn btn-info" type="submit">Search <span
                                            class="glyphicon glyphicon-search" type="submit"></span></button>
                                </td>
                            </tr>
                        </table>

                    </div>
                    <div class="col-md-2"><br/></div>

                </div>
            </form>


            <?php
            if ($page > 0) { ?>
                <div>
                    <table class="table table-hover">
                        <tr bgcolor="#cccccc">
                            <th>No.</th>
                            <th>Booking No.</th>
                            <th>Booking Date</th>
                            <th>Payment Status</th>
                            <th>Booking Status</th>
                            <th>Prices</th>
                            <th width="40">Detail</th>
                        </tr>
                        <?php


                        while ($row_reservations = mysql_fetch_assoc($result_reservations)) {
                            $paymentstatus_id = $row_reservations['paymentstatus_id'] <= 1 ? 5 : $row_reservations['paymentstatus_id'];
                            $bookingstatus_id = $row_reservations['bookingstatus_id'] <= 1 ? 4 : $row_reservations['bookingstatus_id'];
                            ?>
                            <tr bgcolor="#FFFFFF">
                                <td style="vertical-align: middle;"><?= $i++ ?></td>
                                <td style="vertical-align: middle;"><?= $row_reservations['res_id_str'] ?></td>
                                <td style="vertical-align: middle;"><?= $row_reservations['res_date'] ?></td>
                                <td style="vertical-align: middle;"><?= get_value('lis_payment_status', 'lis_id', 'lis_name', $paymentstatus_id) ?></td>
                                <td style="vertical-align: middle;"><?= get_value('lis_booking_status', 'lis_id', 'lis_name', $bookingstatus_id) ?></td>
                                <td style="vertical-align: middle;"><?= $row_reservations['prices'] ?></td>
                                <td style="vertical-align: middle;">
                                    <a href="detail_resv.php?bookingno=<?= $row_reservations['res_id_str'] ?>&id=<?= $row_reservations['res_id'] ?>"
                                       class="btn btn-info" data-toggle="modal"
                                       data-target="#myModal<?= $row_reservations['res_id'] ?>">More detail</a>

                                    <!-- Modal HTML -->
                                    <div id="myModal<?= $row_reservations['res_id'] ?>" class="modal fade ">
                                        <div class="container" style="margin-top: 100px;">
                                            <div class="modal-content container">
                                                <!-- Content will be loaded here from "remote.php" file -->
                                            </div>

                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <!-- Modal -->

                        <?php } ?>

                    </table>
                    <div class="bs-example"><!-- Modal HTML -->
                        <div id="myModal" class="modal fade ">
                            <div class="container">
                                <div class="modal-content">
                                    <!-- Content will be loaded here from "detail_resv.php" file -->
                                </div>

                            </div>
                        </div>

                    </div>
                    <ul class="pagination">

                        <?php

                        for ($i = 0; $i < $page && $page > 1; $i++) {
                            ?>
                            <li class=<?= ($i + 1 == $page_no) ? "active" : "" ?>><a
                                    href="./history.php?page=<?= $i + 1 ?>"><?= $i + 1 ?></a>
                            </li>
                            <?php
                        }
                        ?>
                    </ul>
                </div>

                <?php
            }
            ?>
        </div>

</center>


<!-- Comment -->


<!-- include footer-->
<?php include("inc_footer.php"); ?>
<!-- end include footer -->


</body>
</html>
