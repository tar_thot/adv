<?php session_start(); ?>
<?php include("./administrator/lib/connect.php"); ?>
<?php include("./administrator/lib/function.php"); ?>
<?php include("./administrator/lib/constant.php"); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<title>Best price Package, Hotels, Transfer, Air Tickets</title>
	<meta name="description" content="ADV Tour., Phi Phi Island Tour & Transfer operator, Hotel & Transport arrangement in the south of Thailand
"/>
	<meta name="keywords"
		  content="Ko Tao Island, Samui Island, Phi Phi Island, Ferries, Boat Transfer, Tours, Package, Hotels, Diving, Liveaboard, Thailand"/>
	<meta name="Classification" content="World wide tour operate">
	<meta name="Author" content="S.S.ADV. Co., Ltd. Bangkok,Thailand">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link href="css/font.css" rel="stylesheet" type="text/css"/>

	<!-- menu slide -->
	<script type="text/javascript" src="js/script.js"></script>
	<!-- menu slide -->
	<!-- js-vallenato -->
	<script type="text/javascript" src="js-vtip/jquery-1.3.2.min.js"></script>
	<script type="text/javascript" src="js-vtip/jquery-plugin-vtip.js"></script>
	<script src="js-vallenato/vallenato.js" type="text/javascript" charset="utf-8"></script>
	<link rel="stylesheet" href="js-vallenato/vallenato.css" type="text/css" media="screen" charset="utf-8">
	<!-- js-vallenato -->
</head>

<body>
<!-- include top login-->
<?php include("inc_login.php"); ?>
<!-- end include top login -->

<!-- include logo & menu-->
<?php include("inc_menu.php"); ?>
<!-- end include logo & menu -->

<!-- Detail -->
<br/>
<?php
//get post data
$put_name = isset($_POST['put_name']) ? $_POST['put_name'] : '';
$put_ref = isset($_POST['put_ref']) ? $_POST['put_ref'] : '';
$area_id = isset($_POST['area_id']) ? $_POST['area_id'] : '';
$area_to_id = isset($_POST['area_to_id']) ? $_POST['area_to_id'] : '';
$pickuptransfer_type_id = isset($_POST['pickuptransfer_type_id']) ? $_POST['pickuptransfer_type_id'] : '';
$time_id = isset($_POST['time_id']) ? $_POST['time_id'] : '';
$put_forsearch_id = isset($_POST['put_forsearch_id']) ? $_POST['put_forsearch_id'] : '';
?>
<!--  Search  -->
<?php include('inc_search_pickuptransfer.php'); ?>
<table width="1000" border="0" align="center" cellpadding="0" cellspacing="0">
	<tr>
		<td width="500" align="left" valign="top"><span class="h1"><img src="images/pic_under_line.jpg" width="200"
																		height="15" border="0"/></span></td>
		<td align="right"><span class="h1"><img src="images/pic_under_line1.jpg" width="200" height="15"
												border="0"/></span></td>
	</tr>
</table>
<!--  Search  -->

<?php
//เพิ่มเติม 10/08/58======================================
while (list($xVarName, $xVarvalue) = each($_GET)) {
	${$xVarName} = $xVarvalue;
}


while (list($xVarName, $xVarvalue) = each($_POST)) {
	${$xVarName} = $xVarvalue;
}

while (list($xVarName, $xVarvalue) = each($_FILES)) {
	${$xVarName . "_name"} = $xVarvalue['name'];
	${$xVarName . "_type"} = $xVarvalue['type'];
	${$xVarName . "_size"} = $xVarvalue['size'];
	${$xVarName . "_error"} = $xVarvalue['error'];
	${$xVarName} = $xVarvalue['tmp_name'];
}
//เพิ่มเติม 10/08/58======================================
$sql = "SELECT * ";
$sql .= "FROM pickuptransfers ";
$sql .= "where put_id > 0 ";

if ($put_name) {
	$sql .= "AND put_name LIKE '%$put_name%' ";
}
if ($put_ref) {
	$sql .= "AND put_ref LIKE '%$put_ref%' ";
}
if ($area_id) {
	$sql .= "AND area_id = '$area_id' ";
}
if ($area_to_id) {
	$sql .= "AND area_to_id = '$area_to_id' ";
}
if ($pickuptransfer_type_id) {
	$sql .= "AND pickuptransfer_type_id = '$pickuptransfer_type_id' ";
}
if ($time_id) {
	$sql .= "AND time_id = '$time_id' ";
}
if (isset($put_forsearch_id)) {
	$sql .= "AND put_forsearch_id_arr LIKE '%$put_forsearch_id%' ";
}


$sql .= "order by put_name ASC ";

$wlinks = "";

if ($put_name) {
	$wlinks .= "&put_name=$put_name";
}
if ($put_ref) {
	$wlinks .= "&put_ref=$put_ref";
}
if ($area_id) {
	$wlinks .= "&area_id=$area_id";
}
if ($area_to_id) {
	$wlinks .= "&area_to_id=$area_to_id";
}
if ($pickuptransfer_type_id) {
	$wlinks .= "&pickuptransfer_type_id=$pickuptransfer_type_id";
}
if ($time_id) {
	$wlinks .= "&time_id=$time_id";
}
if ($put_forsearch_id) {
	$wlinks .= "&put_forsearch_id=$put_forsearch_id";
}


$start = isset($_GET['start']) ? $_GET['start'] : 0;

$limit = '10'; // แสดงผลหน้าละกี่หัวข้อ
$Qtotal = mysql_query($sql);
$total = mysql_num_rows($Qtotal); // หาจำนวน record

$sql .= "LIMIT $start,$limit ";
$results = mysql_query($sql);
$totalp = mysql_num_rows($results); // หาจำนวน record ที่เรียกออกมา

/* ตัวแบ่งหน้า */
$page = ceil($total / $limit); // เอา record ทั้งหมด หารด้วย จำนวนที่จะแสดงของแต่ละหน้า
//echo "$page <hr />";

/* เอาผลหาร มาวน เป็นตัวเลข เรียงกัน เช่น สมมุติว่าหารได้ 3 เอามาวลก็จะได้ 1 2 3 */
if (isset($_GET['page'])) {
	$get_page = $_GET['page'];
} else {
	$get_page = 1;
}

?>


<!-- Detail -->
<table width="1000" border="0" align="center" cellpadding="0" cellspacing="0">
	<tr>
		<td height="40" bgcolor="#FFFFFF" class="h1"><img src="images/blank.gif" width="10" height="40" border="0"
														  align="absmiddle"><font color="#1e73a4">S.S.ADV.: </font><font
				color="000000">Pick Up Transfer</font></td>
		<td width="250" align="center" bgcolor="#FFFFFF">Data Found : <?= $total ?> Record(s)</td>
	</tr>
</table>
<table width="1000" border="0" align="center" cellpadding="0" cellspacing="0">
	<tr>
		<td width="500" align="left" valign="top"><span class="h1"><img src="images/pic_under_line.jpg" width="200"
																		height="15" border="0"/></span></td>
		<td align="right"><span class="h1"><img src="images/pic_under_line1.jpg" width="200" height="15"
												border="0"/></span></td>
	</tr>
</table>

<!-- Sort by -->

<!-- Sort by -->
<!-- Product-->


<table width="1001" border="0" align="center" cellpadding="0" cellspacing="0">
	<tr>
		<td width="490"></td>
		<td width="20"></td>
		<td width="490" valign="top"></td>
	</tr>

	<?php

	while ($rows = mysql_fetch_array($results)) {

		?>


		<tr>
			<td bgcolor="#FFFFFF">

				<!-- Product 01 -->
				<?php include('inc_pickuptransfer.php'); ?>
				<!-- Product 01 --></td>
			<td><img src="images/blank.gif" width="20" height="20" border="0"/></td>
			<?php
			if ($rows = mysql_fetch_array($results)) {
				?>
				<td valign="top" bgcolor="#FFFFFF">
					<!-- Product 01 -->
					<?php include('inc_pickuptransfer.php'); ?>
					<!-- Product 01 --></td>
				<?php

			}
			?>
		</tr>

		<tr>
			<td><img src="images/blank.gif" width="20" height="10" border="0"/></td>
			<td><img src="images/blank.gif" width="20" height="10" border="0"/></td>
			<td valign="top"><img src="images/blank.gif" width="20" height="10" border="0"/></td>
		</tr>


		<?php
	}
	?>
</table>
<!-- Product-->

<table width="1000" border="0" align="center" cellpadding="0" cellspacing="0">
	<tr>
		<td><img src="images/blank.gif" width="20" height="10" border="0"/></td>
	</tr>
	<tr>
		<td valign="top" class="footdot"><img src="images/blank.gif" width="100" height="1" border="0"/></td>
	</tr>

	<tr>
		<td height="30" align="center"><?php include('inc_link_page_list.php'); ?></td>
	</tr>
	<tr>
		<td valign="top" class="footdot"><img src="images/blank.gif" width="100" height="1" border="0"/></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
</table>

<!-- Detail -->


<!-- include footer-->
<?php include("inc_footer.php"); ?>
<!-- end include footer -->


</body>
</html>
