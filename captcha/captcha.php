<?php
session_start();
$text = rand(10000, 99999);
$_SESSION["verify_code"] = $text;
$height = 25;
$width = 55;

$image_p = imagecreate($width, $height);
$black = imagecolorallocate($image_p, 204, 204, 204);    //bg-Color
$white = imagecolorallocate($image_p, 0, 0, 0);        //font-Color
$font_size = 14;

imagestring($image_p, $font_size, 5, 5, $text, $white);
imagejpeg($image_p, null, 80);
?>