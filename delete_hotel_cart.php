<?php session_start(); ?>
<?php include("./administrator/lib/connect.php"); ?>
<?php include("./administrator/lib/function.php"); ?>
<?php include("./administrator/lib/constant.php"); ?>
<?php ob_start(); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<title>Best price Package, Hotels, Transfer, Air Tickets</title>
	<meta name="description" content="ADV Tour., Phi Phi Island Tour & Transfer operator, Hotel & Transport arrangement in the south of Thailand
"/>
	<meta name="keywords"
		  content="Ko Tao Island, Samui Island, Phi Phi Island, Ferries, Boat Transfer, Tours, Package, Hotels, Diving, Liveaboard, Thailand"/>
	<meta name="Classification" content="World wide tour operate">
	<meta name="Author" content="S.S.ADV. Co., Ltd. Bangkok,Thailand">


</head>

<body>

<!-- Detail -->
<br/>

<!-- Start Cal Cart For Hotel -->
<?php
include("value_get.php");


# Delete cart by index id

$hotels_id_arr = $_SESSION['cart_hotel']['hotels_id_arr'];
$agentgrade_id_arr = $_SESSION['cart_hotel']['agentgrade_id_arr'];
$res_hot_check_in_arr = $_SESSION['cart_hotel']['res_hot_check_in_arr'];
$res_hot_check_out_arr = $_SESSION['cart_hotel']['res_hot_check_out_arr'];
$res_hot_ratetype_id_arr = $_SESSION['cart_hotel']['res_hot_ratetype_id_arr'];
$res_hot_adult_num_arr = $_SESSION['cart_hotel']['res_hot_adult_num_arr'];
$res_hot_child_num_arr = $_SESSION['cart_hotel']['res_hot_child_num_arr'];
$res_hot_infant_num_arr = $_SESSION['cart_hotel']['res_hot_infant_num_arr'];
$hotel_prices_arr = $_SESSION['cart_hotel']['hotel_prices_arr'];
$hotalloday_id_arr = $_SESSION['cart_hotel']['hotalloday_id_arr'];
$num_of_room_arr = $_SESSION['cart_hotel']['num_of_room_arr'];


if ($_GET['delete_index'] != '') {
	$delete_index = $_GET['delete_index'];
	//echo "<br> Delete Index: $delete_index";

	array_splice($hotels_id_arr, $delete_index, 1);
	array_splice($agentgrade_id_arr, $delete_index, 1);
	array_splice($res_hot_check_in_arr, $delete_index, 1);
	array_splice($res_hot_check_out_arr, $delete_index, 1);
	array_splice($res_hot_ratetype_id_arr, $delete_index, 1);
	array_splice($res_hot_adult_num_arr, $delete_index, 1);
	array_splice($res_hot_child_num_arr, $delete_index, 1);
	array_splice($res_hot_infant_num_arr, $delete_index, 1);
	array_splice($hotel_prices_arr, $delete_index, 1);
	array_splice($hotalloday_id_arr, $delete_index, 1);
	array_splice($num_of_room_arr, $delete_index, 1);
}


$_SESSION['cart_hotel']['hotels_id_arr'] = $hotels_id_arr;
$_SESSION['cart_hotel']['agentgrade_id_arr'] = $agentgrade_id_arr;
$_SESSION['cart_hotel']['res_hot_check_in_arr'] = $res_hot_check_in_arr;
$_SESSION['cart_hotel']['res_hot_check_out_arr'] = $res_hot_check_out_arr;
$_SESSION['cart_hotel']['res_hot_ratetype_id_arr'] = $res_hot_ratetype_id_arr;
$_SESSION['cart_hotel']['res_hot_adult_num_arr'] = $res_hot_adult_num_arr;
$_SESSION['cart_hotel']['res_hot_child_num_arr'] = $res_hot_child_num_arr;
$_SESSION['cart_hotel']['res_hot_infant_num_arr'] = $res_hot_infant_num_arr;
$_SESSION['cart_hotel']['hotel_prices_arr'] = $hotel_prices_arr;
$_SESSION['cart_hotel']['hotalloday_id_arr'] = $hotalloday_id_arr;
$_SESSION['cart_hotel']['num_of_room_arr'] = $num_of_room_arr;

echo "<script type=\"text/javascript\">window.location='cart.php'</script>";
exit();
				
				

?>
<!-- End Cal Cart For Hotel -->


<br/>
<!-- Detail -->


</body>
</html>
