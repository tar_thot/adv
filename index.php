<?php session_start(); ?>
<?php include("./administrator/lib/connect.php"); ?>
<?php include("./administrator/lib/function.php"); ?>
<?php include("./administrator/lib/constant.php");
error_reporting(E_ALL & ~E_NOTICE); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Best price Package, Hotels, Transfer, Air Tickets</title>
    <meta name="description"
          content="Adv Tour, Phi Phi Island Tour & Transfer operator, Hotel & Transport arrangement in the south of Thailand"/>
    <meta name="keywords"
          content="Ko Tao Island, Samui Island, Phi Phi Island, Ferries, Boat Transfer, Tours, Package, Hotels, Diving, Liveaboard, Thailand"/>
    <meta name="Classification" content="World wide tour operate">
    <meta name="Author" content="Adv Tour Co., Ltd. Bangkok,Thailand">
    <link rel="stylesheet" href="css/bootstrap.min.css">

    <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet">
    <link href="css/font.css" rel="stylesheet" type="text/css"/>
    <!-- menu slide -->
    <script type="text/javascript" src="js/script.js"></script>
    <!-- menu slide -->

    <!-- js-vallenato -->
    <script type="text/javascript" src="js-vtip/jquery-1.3.2.min.js"></script>
    <script type="text/javascript" src="js-vtip/jquery-plugin-vtip.js"></script>
    <script src="js-vallenato/vallenato.js" type="text/javascript" charset="utf-8"></script>
    <link rel="stylesheet" href="js-vallenato/vallenato.css" type="text/css" media="screen" charset="utf-8">
    <!-- js-vallenato -->

    <!-- bx slider -->
    <link rel="stylesheet" href="./bxslider/jquery.bxslider2.css" type="text/css"/>
    <link rel="stylesheet" href="./bxslider/github.css" type="text/css"/>
    <script src="./bxslider/jquery.min.js"></script>
    <script src="./bxslider/jquery.bxslider.js"></script>
    <script src="./bxslider/rainbow.min.js"></script>
    <script src="./bxslider/scripts.js"></script>
</head>
<!-- bx slider -->
</head>

<body>
<!-- include top login-->
<?php include("inc_login.php"); ?>
<!-- end include top login -->

<!-- include logo & menu-->
<?php include("inc_menu.php"); ?>
<!-- end include logo & menu -->
<?php
//$area_id = 0;
//$area_to_id = 0;
//$boattransfer_class_id = 0;
//$time_id = 0;
//$bot_forsearch_id = 0;
//$area_id = 0;
//$area_to_id = 0;
//$pickuptransfer_type_id = 0;
//$put_forsearch_id = 0;
//$category_id = 0;
//$cartransfer_type_id = 0;
//$privatelandtransfer_type_id = 0;
//$room_type_id = 0;
//$province_id =0;
//$train_type_id = 0;
//$traintransfer_class_id = 0;
//$time_from_id = 0;
//$time_to_id = 0;
//$put_name='';
//$put_ref = '';
//$tou_name = '';
//$tou_ref = '';
//$ct_ref = '';
//$ct_name = '';
//$act_name = '';
//$act_ref = '';
//$plt_name = '';
//$plt_ref = '';
//$hot_name = '';
//$hot_ref = '';
//$train_name = '';
//$train_ref = '';
?>


<!-- pic slide -->
<table width="100%" cellspacing="0" cellpadding="0">
    <tr>
        <td height="400" bgcolor="#e9f1f4" align="center">

            <table width="1000" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>

                    <script type="text/javascript">
                        $(document).ready(function () {
                            $('.slider1').bxSlider({
                                slideWidth: 250,
                                minSlides: 1,
                                maxSlides: 1,
                                moveSlides: 1,
                                slideMargin: 0,
                                auto: true,
                                autoControls: true

                            });
                        });
                    </script>

                    <td width="250">
                        <div style="height:400px; overflow:hidden;">
                            <div class="slider1">

                                <?php
                                $sql_left = "SELECT * ";
                                $sql_left .= "FROM sliders_left ";
                                $sql_left .= "WHERE con_id > 0 ";

                                $sql_left .= "order by con_id ASC ";

                                $results_left = mysql_query($sql_left);

                                while ($rows_left = mysql_fetch_array($results_left)) {
                                    ?>

                                    <div class="slide"><a href="<?= $rows_left['con_url'] ?>"><img
                                                src="./photo/sliders/<?= $rows_left['photo1'] ?>" width="250"
                                                height="400"></a></div>

                                    <?php
                                } // END while($rows_left = mysql_fetch_array($results_left)){
                                ?>

                            </div>
                        </div>
                    </td>
                    <td width="20"><img src="images/blank.gif" width="20" height="20" border="0"/></td>

                    <script type="text/javascript">
                        $(document).ready(function () {
                            $('.slider2').bxSlider({
                                slideWidth: 730,
                                minSlides: 1,
                                maxSlides: 1,
                                moveSlides: 1,
                                slideMargin: 0,
                                auto: true,
                                autoControls: true

                            });
                        });
                    </script>

                    <td width="720">
                        <div style="height:400px; overflow:hidden;">
                            <div class="slider2">

                                <?php
                                $sql_right = "SELECT * ";
                                $sql_right .= "FROM sliders_right ";
                                $sql_right .= "WHERE con_id > 0 ";
                                //$sql_left .= "AND con_name = '1' ";

                                $sql_right .= "order by con_id ASC ";

                                $results_right = mysql_query($sql_right);

                                while ($rows_right = mysql_fetch_array($results_right)) {
                                    ?>

                                    <div class="slide"><a href="<?= $rows_right['con_url'] ?>"><img
                                                src="./photo/sliders/<?= $rows_right['photo1'] ?>" width="730"
                                                height="400"></a></div>

                                    <?php
                                } // END while($rows_right = mysql_fetch_array($results_right)){
                                ?>

                            </div>
                        </div>
                    </td>
                </tr>
            </table>

        </td>
    </tr>
    <tr>
        <td height="15" valign="top" background="images/under_pic.jpg"><img src="images/blank.gif" width="100"
                                                                            height="15" border="0"/></td>
    </tr>
</table>
<!-- pic slide -->

<!-- Detail -->
<br/>
<table width="1000" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
        <td width="250" valign="top">

            <!-- box search -->
            <table width="250" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="50" align="center" valign="top" bgcolor="#FFFFFF"><img src="images/checklist.png"
                                                                                      width="50" height="50" hspace="5"
                                                                                      vspace="5" border="0"/></td>
                    <td valign="middle" bgcolor="#FFFFFF">
                        <table width="185" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td><h4>SEARCH PRODUCTS</h4></td>
                            </tr>
                            <tr>
                                <td>www.booking.adv-tour.com</td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>


            <table width="250" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td height="150" valign="top" bgcolor="#FFFFFF">


                        <!-- Start Redio For Select Product Section -->

                        <table width="230" border="0" align="center" cellpadding="0" cellspacing="0">
                            <tr>
                                <td valign="middle" bgcolor="#CCCCCC"><img src="images/blank.gif" width="100" height="1"
                                                                           border="0"/></td>
                            </tr>
                            <tr>
                                <td height="10"><img src="images/blank.gif" width="100" height="10" border="0"/>
                                    <script type="text/javascript">
                                        function checkSelectRedio(select_product_section) {

                                            var select_product_section = select_product_section;

                                            //alert('select_product_section : ' + select_product_section);

                                            $('#frm_boattransfers_search').hide();
                                            $('#frm_pickuptransfers_search').hide();
                                            $('#frm_tours_search').hide();
                                            $('#frm_activities_search').hide();
                                            $('#frm_cartransfers_search').hide();
                                            $('#frm_privatelandtransfers_search').hide();
                                            $('#frm_hotels_search').hide();
                                            $('#frm_traintransfers_search').hide();


                                            if (select_product_section == 'boattransfers') {

                                                $('#frm_boattransfers_search').show();
                                            }

                                            if (select_product_section == 'pickuptransfers') {
                                                $('#frm_pickuptransfers_search').show();
                                            }

                                            if (select_product_section == 'tours') {
                                                $('#frm_tours_search').show();
                                            }

                                            if (select_product_section == 'activities') {
                                                $('#frm_activities_search').show();
                                            }

                                            if (select_product_section == 'cartransfers') {
                                                $('#frm_cartransfers_search').show();
                                            }

                                            if (select_product_section == 'privatelandtransfers') {
                                                $('#frm_privatelandtransfers_search').show();
                                            }

                                            if (select_product_section == 'hotels') {
                                                $('#frm_hotels_search').show();
                                            }

                                            if (select_product_section == 'traintransfers') {
                                                $('#frm_traintransfers_search').show();
                                            }

                                        }
                                    </script>

                                    <form name="frm_select_section" action="">
                                        <input type="radio" name="select_product_section" value="boattransfers"
                                               checked="checked" onclick="checkSelectRedio(this.value);"> Boat Transfer
                                        <br/>
                                        <input type="radio" name="select_product_section" value="pickuptransfers"
                                               onclick="checkSelectRedio(this.value);"> Pick Up Transfer <br/>
                                        <input type="radio" name="select_product_section" value="tours"
                                               onclick="checkSelectRedio(this.value);"> Tour <br/>
                                        <input type="radio" name="select_product_section" value="activities"
                                               onclick="checkSelectRedio(this.value);"> Activity <br/>
                                        <input type="radio" name="select_product_section" value="cartransfers"
                                               onclick="checkSelectRedio(this.value);"> Bus Transfer <br/>
                                        <input type="radio" name="select_product_section" value="privatelandtransfers"
                                               onclick="checkSelectRedio(this.value);"> Private Land Transfer <br/>
                                        <input type="radio" name="select_product_section" value="hotels"
                                               onclick="checkSelectRedio(this.value);"> Hotel <br/>
                                        <input type="radio" name="select_product_section" value="traintransfers"
                                               onclick="checkSelectRedio(this.value);"> Train Transfer <br/>
                                    </form>
                                </td>
                            </tr>
                        </table>

                        <!-- End Redio For Select Product Section -->



                        <!--Start Form For Boat Transfer Section-->
                        <script type="text/javascript">
                            function frmBoattransfersSearchClick() {
                                document.frm_boattransfers_search.submit();
                            }
                        </script>
                        <form name="frm_boattransfers_search" id="frm_boattransfers_search"
                              action="boattransfer_list.php" method="post" style="display:none;">
                            <table width="230" border="0" align="center" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td valign="middle" bgcolor="#CCCCCC"><img src="images/blank.gif" width="100"
                                                                               height="1" border="0"/></td>
                                </tr>
                                <tr>
                                    <td height="30">Name</td>
                                </tr>
                                <tr>
                                    <td><input type="text" name="bot_name" class="form-control"
                                               value="<?= !empty($bot_name) ? $bot_name : '' ?>"/></td>
                                </tr>

                                <tr>
                                    <td height="30">Ref</td>
                                </tr>
                                <tr>
                                    <td><input type="text" name="bot_ref" class=" form-control"
                                               value="<?= !empty($bot_ref) ? $bot_ref : '' ?>"/></td>
                                </tr>

                                <tr>
                                    <td height="30">From</td>
                                </tr>
                                <tr>
                                    <td><select name="area_id" id="area_id" class="form-control">
                                            <option value="">-- Select --</option>

                                            <?php listbox('boattransfer_con_area', 'con_id', 'con_name', $area_id, 'N'); ?>
                                        </select></td>
                                </tr>

                                <tr>
                                    <td height="30">To</td>
                                </tr>
                                <tr>
                                    <td><select name="area_to_id" id="area_to_id" class=" form-control">
                                            <option value="">-- Select --</option>
                                            <?php listbox('boattransfer_con_area', 'con_id', 'con_name', $area_to_id, 'N'); ?>
                                        </select></td>
                                </tr>

                                <tr>
                                    <td height="30">Class</td>
                                </tr>
                                <tr>
                                    <td><select name="boattransfer_class_id" id="boattransfer_class_id"
                                                class=" form-control">
                                            <option value="">-- Select --</option>
                                            <?php listbox('boattransfer_class', 'c_id', 'c_name', $boattransfer_class_id, 'N'); ?>
                                        </select></td>
                                </tr>

                                <tr>
                                    <td height="30">Time</td>
                                </tr>
                                <tr>
                                    <td><select name="time_id" id="time_id" class="form-control">
                                            <option value="">-- Select --</option>
                                            <?php listbox('boattransfer_con_time', 'con_id', 'con_name', $time_id, 'N'); ?>
                                        </select></td>
                                </tr>

                                <tr>
                                    <td height="30">For Search</td>
                                </tr>
                                <tr>
                                    <td><select name="bot_forsearch_id" id="bot_forsearch_id" class="form-control">
                                            <option value="">-- Select --</option>
                                            <?php listbox('boattransfer_con_forsearch', 'con_id', 'con_name', $bot_forsearch_id, 'N'); ?>
                                        </select></td>
                                </tr>

                                <tr>
                                    <td height="10"><img src="images/blank.gif" width="100" height="10" border="0"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">
                                        <button type="submit" class="btn btn-info form-control">Search <span
                                                class="glyphicon glyphicon-search"></span>
                                        </button>
                                    </td>

                                </tr>
                                <tr>

                                </tr>
                                <tr>
                                    <td height="10"><img src="images/blank.gif" width="100" height="10" border="0"/>
                                    </td>
                                </tr>
                            </table>
                        </form>
                        <!--End Form For Boat Transfer Section-->


                        <!--Start Form For Pick Up Transfer Section-->
                        <script type="text/javascript">
                            function frmPickuptransfersSearchClick() {
                                document.frm_pickuptransfers_search.submit();
                            }
                        </script>
                        <form name="frm_pickuptransfers_search" id="frm_pickuptransfers_search"
                              action="pickuptransfer_list.php" method="post" style="display:none;">
                            <table width="230" border="0" align="center" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td valign="middle" bgcolor="#CCCCCC"><img src="images/blank.gif" width="100"
                                                                               height="1" border="0"/></td>
                                </tr>
                                <tr>
                                    <td height="30">Name</td>
                                </tr>
                                <tr>
                                    <td><input type="text" name="put_name" class="form-control "
                                               value="<?= $put_name ?>"/></td>
                                </tr>
                                <tr>
                                    <td height="30">Ref</td>
                                </tr>
                                <tr>
                                    <td><input type="text" name="put_ref" class="form-control "
                                               value="<?= $put_ref ?>"/></td>
                                </tr>
                                <tr>
                                    <td height="30">From</td>
                                </tr>
                                <tr>
                                    <td><select name="area_id" id="area_id" class="form-control ">
                                            <option value="">-- Select --</option>
                                            <?php listbox('pickuptransfer_con_area', 'con_id', 'con_name', $area_id, 'N'); ?>
                                        </select></td>
                                </tr>
                                <tr>
                                    <td height="30">To</td>
                                </tr>
                                <tr>
                                    <td><select name="area_to_id" id="area_to_id" class="form-control ">
                                            <option value="">-- Select --</option>
                                            <?php listbox('pickuptransfer_con_area', 'con_id', 'con_name', $area_to_id, 'N'); ?>
                                        </select></td>
                                </tr>
                                <tr>
                                    <td height="30">Transfer Type</td>
                                </tr>
                                <tr>
                                    <td><select name="pickuptransfer_type_id" id="pickuptransfer_type_id"
                                                class="form-control ">
                                            <option value="">-- Select --</option>
                                            <?php listbox('pickuptransfer_type', 'c_id', 'c_name', $pickuptransfer_type_id, 'N'); ?>
                                        </select></td>
                                </tr>
                                <tr>
                                    <td height="30">Time</td>
                                </tr>
                                <tr>
                                    <td><select name="time_id" id="time_id" class="form-control ">
                                            <option value="">-- Select --</option>
                                            <?php listbox('pickuptransfer_con_time', 'con_id', 'con_name', $time_id, 'N'); ?>
                                        </select></td>
                                </tr>

                                <tr>
                                    <td height="30">For Search</td>
                                </tr>
                                <tr>
                                    <td><select name="put_forsearch_id" id="put_forsearch_id" class="form-control ">
                                            <option value="">-- Select --</option>
                                            <?php listbox('pickuptransfer_con_forsearch', 'con_id', 'con_name', $put_forsearch_id, 'N'); ?>
                                        </select></td>
                                </tr>


                                <tr>
                                    <td height="10"><img src="images/blank.gif" width="100" height="10" border="0"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">
                                        <button type="submit" class="btn btn-info form-control">Search <span
                                                class="glyphicon glyphicon-search"></span>
                                        </button>
                                    </td>

                                </tr>
                                <tr>
                                    <td height="10"><img src="images/blank.gif" width="100" height="10" border="0"/>
                                    </td>
                                </tr>
                            </table>
                        </form>
                        <!--End Form For Pick Up Transfer Section-->


                        <!--Start Form For Tour Section-->
                        <script type="text/javascript">
                            function frmToursSearchClick() {
                                document.frm_tours_search.submit();
                            }
                        </script>
                        <form name="frm_tours_search" id="frm_tours_search" action="tour_list.php" method="post"
                              style="display:none;">
                            <table width="230" border="0" align="center" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td valign="middle" bgcolor="#CCCCCC"><img src="images/blank.gif" width="100"
                                                                               height="1" border="0"/></td>
                                </tr>
                                <tr>
                                    <td height="30">Name</td>
                                </tr>
                                <tr>
                                    <td><input type="text" name="tou_name" class="form-control "
                                               value="<?= $tou_name ?>"/></td>
                                </tr>

                                <tr>
                                    <td height="30">Ref</td>
                                </tr>
                                <tr>
                                    <td><input type="text" name="tou_ref" class="form-control "
                                               value="<?= $tou_ref ?>"/></td>
                                </tr>

                                <tr>
                                    <td height="30">Area</td>
                                </tr>
                                <tr>
                                    <td><select name="area_id" id="area_id" class="form-control ">
                                            <option value="">-- Select --</option>
                                            <?php listbox('tour_con_area', 'con_id', 'con_name', $area_id, 'N'); ?>
                                        </select></td>
                                </tr>

                                <tr>
                                    <td height="30">Category</td>
                                </tr>
                                <tr>
                                    <td><select name="category_id" id="category_id" class="form-control ">
                                            <option value="">-- Select --</option>
                                            <?php listbox('tour_con_category', 'con_id', 'con_name', $category_id, 'N'); ?>
                                        </select></td>
                                </tr>


                                <tr>
                                    <td height="10"><img src="images/blank.gif" width="100" height="10" border="0"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">
                                        <button type="submit" class="btn btn-info form-control">Search <span
                                                class="glyphicon glyphicon-search"></span>
                                        </button>
                                    </td>

                                </tr>
                                <tr>
                                    <td height="10"><img src="images/blank.gif" width="100" height="10" border="0"/>
                                    </td>
                                </tr>
                            </table>
                        </form>
                        <!--End Form For Tour Section-->


                        <!--Start Form For Activity Section-->
                        <script type="text/javascript">
                            function frmActivitiesSearchClick() {
                                document.frm_activities_search.submit();
                            }
                        </script>
                        <form name="frm_activities_search" id="frm_activities_search" action="activity_list.php"
                              method="post" style="display:none;">
                            <table width="230" border="0" align="center" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td valign="middle" bgcolor="#CCCCCC"><img src="images/blank.gif" width="100"
                                                                               height="1" border="0"/></td>
                                </tr>
                                <tr>
                                    <td height="30">Name</td>
                                </tr>
                                <tr>
                                    <td><input type="text" name="act_name" class="form-control "
                                               value="<?= $act_name ?>"/></td>
                                </tr>

                                <tr>
                                    <td height="30">Ref</td>
                                </tr>
                                <tr>
                                    <td><input type="text" name="act_ref" class="form-control "
                                               value="<?= $act_ref ?>"/></td>
                                </tr>

                                <tr>
                                    <td height="30">Area</td>
                                </tr>
                                <tr>
                                    <td><select name="area_id" id="area_id" class="form-control ">
                                            <option value="">-- Select --</option>
                                            <?php listbox('activity_con_area', 'con_id', 'con_name', $area_id, 'N'); ?>
                                        </select></td>
                                </tr>

                                <tr>
                                    <td height="30">Category</td>
                                </tr>
                                <tr>
                                    <td><select name="category_id" id="category_id" class="form-control ">
                                            <option value="">-- Select --</option>
                                            <?php listbox('activity_con_category', 'con_id', 'con_name', $category_id, 'N'); ?>
                                        </select></td>
                                </tr>


                                <tr>
                                    <td height="10"><img src="images/blank.gif" width="100" height="10" border="0"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">
                                        <button type="submit" class="btn btn-info form-control">Search <span
                                                class="glyphicon glyphicon-search"></span>
                                        </button>
                                    </td>

                                </tr>
                                <tr>
                                    <td height="10"><img src="images/blank.gif" width="100" height="10" border="0"/>
                                    </td>
                                </tr>
                            </table>
                        </form>
                        <!--End Form For Activity Section-->


                        <!--Start Form For Bus Transfer Section-->
                        <script type="text/javascript">
                            function frmCartransfersSearchClick() {
                                document.frm_cartransfers_search.submit();
                            }
                        </script>
                        <form name="frm_cartransfers_search" id="frm_cartransfers_search" action="bustransfer_list.php"
                              method="post" style="display:none;">
                            <table width="230" border="0" align="center" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td valign="middle" bgcolor="#CCCCCC"><img src="images/blank.gif" width="100"
                                                                               height="1" border="0"/></td>
                                </tr>
                                <tr>
                                    <td height="30">Name</td>
                                </tr>
                                <tr>
                                    <td><input type="text" name="ct_name" class="form-control "
                                               value="<?= $ct_name ?>"/></td>
                                </tr>
                                <tr>
                                    <td height="30">Ref</td>
                                </tr>
                                <tr>
                                    <td><input type="text" name="ct_ref" class="form-control " value="<?= $ct_ref ?>"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td height="30">From</td>
                                </tr>
                                <tr>
                                    <td><select name="area_id" id="area_id" class="form-control ">
                                            <option value="">-- Select --</option>
                                            <?php listbox('cartransfer_con_area', 'con_id', 'con_name', $area_id, 'N'); ?>
                                        </select></td>
                                </tr>
                                <tr>
                                    <td height="30">To</td>
                                </tr>
                                <tr>
                                    <td><select name="area_to_id" id="area_to_id" class="form-control">
                                            <option value="">-- Select --</option>
                                            <?php listbox('cartransfer_con_area', 'con_id', 'con_name', $area_to_id, 'N'); ?>
                                        </select></td>
                                </tr>
                                <tr>
                                    <td height="30">Transfer Type</td>
                                </tr>
                                <tr>
                                    <td><select name="cartransfer_type_id" id="cartransfer_type_id"
                                                class="form-control">
                                            <option value="">-- Select --</option>
                                            <?php listbox('cartransfer_type', 'c_id', 'c_name', $cartransfer_type_id, 'N'); ?>
                                        </select></td>
                                </tr>
                                <tr>
                                    <td height="30">Time</td>
                                </tr>
                                <tr>
                                    <td><select name="time_id" id="time_id" class="form-control">
                                            <option value="">-- Select --</option>
                                            <?php listbox('cartransfer_con_time', 'con_id', 'con_name', $time_id, 'N'); ?>
                                        </select></td>
                                </tr>


                                <tr>
                                    <td height="10"><img src="images/blank.gif" width="100" height="10" border="0"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">
                                        <button type="submit" class="btn btn-info form-control">Search <span
                                                class="glyphicon glyphicon-search"></span>
                                        </button>
                                    </td>

                                </tr>
                                <tr>
                                    <td height="10"><img src="images/blank.gif" width="100" height="10" border="0"/>
                                    </td>
                                </tr>
                            </table>
                        </form>
                        <!--End Form For Bus Transfer Section-->


                        <!--Start Form For Private Land Transfer Section-->
                        <script type="text/javascript">
                            function frmPrivatelandtransfersSearchClick() {
                                document.frm_privatelandtransfers_search.submit();
                            }
                        </script>
                        <form name="frm_privatelandtransfers_search" id="frm_privatelandtransfers_search"
                              action="privatelandtransfer_list.php" method="post" style="display:none;">
                            <table width="230" border="0" align="center" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td valign="middle" bgcolor="#CCCCCC"><img src="images/blank.gif" width="100"
                                                                               height="1" border="0"/></td>
                                </tr>
                                <tr>
                                    <td height="30">Name</td>
                                </tr>
                                <tr>
                                    <td><input type="text" name="plt_name" class="form-control"
                                               value="<?= $plt_name ?>"/></td>
                                </tr>
                                <tr>
                                    <td height="30">Ref</td>
                                </tr>
                                <tr>
                                    <td><input type="text" name="plt_ref" class="form-control" value="<?= $plt_ref ?>"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td height="30">From</td>
                                </tr>
                                <tr>
                                    <td><select name="area_id" id="area_id" class="form-control">
                                            <option value="">-- Select --</option>
                                            <?php listbox('privatelandtransfer_con_area', 'con_id', 'con_name', $area_id, 'N'); ?>
                                        </select></td>
                                </tr>
                                <tr>
                                    <td height="30">To</td>
                                </tr>
                                <tr>
                                    <td><select name="area_to_id" id="area_to_id" class="form-control">
                                            <option value="">-- Select --</option>
                                            <?php listbox('privatelandtransfer_con_area', 'con_id', 'con_name', $area_to_id, 'N'); ?>
                                        </select></td>
                                </tr>
                                <tr>
                                    <td height="30">Transfer Type</td>
                                </tr>
                                <tr>
                                    <td><select name="privatelandtransfer_type_id" id="privatelandtransfer_type_id"
                                                class="form-control">
                                            <option value="">-- Select --</option>
                                            <?php listbox('privatelandtransfer_type', 'c_id', 'c_name', $privatelandtransfer_type_id, 'N'); ?>
                                        </select></td>
                                </tr>
                                <tr>
                                    <td height="30">Time</td>
                                </tr>
                                <tr>
                                    <td><select name="time_id" id="time_id" class="form-control">
                                            <option value="">-- Select --</option>
                                            <?php listbox('privatelandtransfer_con_time', 'con_id', 'con_name', $time_id, 'N'); ?>
                                        </select></td>
                                </tr>


                                <tr>
                                    <td height="10"><img src="images/blank.gif" width="100" height="10" border="0"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">
                                        <button type="submit" class="btn btn-info form-control">Search <span
                                                class="glyphicon glyphicon-search"></span>
                                        </button>
                                    </td>

                                </tr>
                                <tr>
                                    <td height="10"><img src="images/blank.gif" width="100" height="10" border="0"/>
                                    </td>
                                </tr>
                            </table>
                        </form>
                        <!--End Form For Private Land Transfer Section-->


                        <!--Start Form For Hotel Section-->
                        <script type="text/javascript">
                            function frmHotelsSearchClick() {
                                document.frm_hotels_search.submit();
                            }
                        </script>
                        <form name="frm_hotels_search" id="frm_hotels_search" action="hotel_list.php" method="post"
                              style="display:none;">
                            <table width="230" border="0" align="center" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td valign="middle" bgcolor="#CCCCCC"><img src="images/blank.gif" width="100"
                                                                               height="1" border="0"/></td>
                                </tr>
                                <tr>
                                    <td height="30">Name</td>
                                </tr>
                                <tr>
                                    <td><input type="text" name="hot_name" class="form-control"
                                               value="<?= $hot_name ?>"/></td>
                                </tr>

                                <tr>
                                    <td height="30">Ref</td>
                                </tr>
                                <tr>
                                    <td><input type="text" name="hot_ref" class="form-control" value="<?= $hot_ref ?>"/>
                                    </td>
                                </tr>

                                <tr>
                                    <td height="30">Area</td>
                                </tr>
                                <tr>
                                    <td><select name="area_id" id="area_id" class="form-control">
                                            <option value="">-- Select --</option>
                                            <?php listbox('hotel_con_area', 'con_id', 'con_name', $area_id, 'N'); ?>
                                        </select></td>
                                </tr>

                                <tr>
                                    <td height="30">Room Type</td>
                                </tr>
                                <tr>
                                    <td><select name="room_type_id" id="room_type_id" class="form-control">
                                            <option value="">-- Select --</option>
                                            <?php listbox('hotel_con_room_type', 'con_id', 'con_name', $room_type_id, 'N'); ?>
                                        </select></td>
                                </tr>

                                <tr>
                                    <td height="10"><img src="images/blank.gif" width="100" height="10" border="0"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">
                                        <button type="submit" class="btn btn-info form-control">Search <span
                                                class="glyphicon glyphicon-search"></span>
                                        </button>
                                    </td>

                                </tr>
                                <tr>
                                    <td height="10"><img src="images/blank.gif" width="100" height="10" border="0"/>
                                    </td>
                                </tr>
                            </table>
                        </form>
                        <!--End Form For Hotel Section-->


                        <!--Start Form For Train Transfer Section-->
                        <script type="text/javascript">
                            function frmTraintransfersSearchClick() {
                                document.frm_traintransfers_search.submit();
                            }
                        </script>
                        <form name="frm_traintransfers_search" id="frm_traintransfers_search"
                              action="traintransfer_list.php" method="post" style="display:none;">
                            <table width="230" border="0" align="center" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td valign="middle" bgcolor="#CCCCCC"><img src="images/blank.gif" width="100"
                                                                               height="1" border="0"/></td>
                                </tr>
                                <tr>
                                    <td height="30">Name</td>
                                </tr>
                                <tr>
                                    <td><input type="text" name="train_name" class="form-control"
                                               value="<?= $train_name ?>"/></td>
                                </tr>

                                <tr>
                                    <td height="30">Ref</td>
                                </tr>
                                <tr>
                                    <td><input type="text" name="train_ref" class="form-control"
                                               value="<?= $train_ref ?>"/></td>
                                </tr>

                                <tr>
                                    <td height="30">From</td>
                                </tr>
                                <tr>
                                    <td><select name="province_id" id="province_id" class="form-control">
                                            <option value="">-- Select --</option>
                                            <?php listbox('traintransfer_con_province', 'con_id', 'con_name', $province_id, 'N'); ?>
                                        </select></td>
                                </tr>

                                <tr>
                                    <td height="30">To</td>
                                </tr>
                                <tr>
                                    <td><select name="province_to_id" id="province_to_id" class="form-control">
                                            <option value="">-- Select --</option>
                                            <?php listbox('traintransfer_con_province', 'con_id', 'con_name', $province_to_id, 'N'); ?>
                                        </select></td>
                                </tr>

                                <tr>
                                    <td height="30">Train</td>
                                </tr>
                                <tr>
                                    <td><select name="train_type_id" id="train_type_id" class="form-control">
                                            <option value="">-- Select --</option>
                                            <?php listbox('traintransfer_con_train', 'con_id', 'con_name', $train_type_id, 'N'); ?>
                                        </select></td>
                                </tr>

                                <tr>
                                    <td height="30">Class</td>
                                </tr>
                                <tr>
                                    <td><select name="traintransfer_class_id" id="traintransfer_class_id"
                                                class="form-control">
                                            <option value="">-- Select --</option>
                                            <?php listbox('traintransfer_class', 'c_id', 'c_name', $traintransfer_class_id, 'N'); ?>
                                        </select></td>
                                </tr>

                                <tr>
                                    <td height="30">Time (DEP.)</td>
                                </tr>
                                <tr>
                                    <td><select name="time_from_id" id="time_from_id" class="form-control">
                                            <option value="">-- Select --</option>
                                            <?php listbox('traintransfer_con_time', 'con_id', 'con_name', $time_from_id, 'N'); ?>
                                        </select></td>
                                </tr>

                                <tr>
                                    <td height="30">Time (ARR.)</td>
                                </tr>
                                <tr>
                                    <td><select name="time_to_id" id="time_to_id" class="form-control">
                                            <option value="">-- Select --</option>
                                            <?php listbox('traintransfer_con_time', 'con_id', 'con_name', $time_to_id, 'N'); ?>
                                        </select></td>
                                </tr>


                                <tr>
                                    <td height="10"><img src="images/blank.gif" width="100" height="10" border="0"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">
                                        <button type="submit" class="btn btn-info form-control">Search <span
                                                class="glyphicon glyphicon-search"></span>
                                        </button>
                                    </td>

                                </tr>
                                <tr>
                                    <td height="10"><img src="images/blank.gif" width="100" height="10" border="0"/>
                                    </td>
                                </tr>
                            </table>
                        </form>
                        <!--End Form For Train Transfer Section-->


                        <script type="text/javascript">

                            checkSelectRedio('boattransfers');
                            //checkSelectRedio('pickuptransfers');

                        </script>

                    </td>
                </tr>
                <tr>
                    <td height="10"><img src="images/linebox.png" width="250" height="10"/></td>
                </tr>
            </table>
            <!-- box search -->
            <br/>

            <!-- Top hotel -->
            <!--<table width="250" border="0" cellspacing="0" cellpadding="0">
            <tr>
            <td height="40" valign="middle" bgcolor="#ffcc00" class="h1"><img src="images/icon_hotel.jpg" width="40" height="40" border="0" align="absmiddle" /> Best Seller Hotel</td>
            </tr>
            <tr>
            <td height="100" bgcolor="#FFFFFF">&nbsp;</td>
            </tr>
            <tr>
            <td height="10"><img src="images/linebox.png" width="250" height="10" /></td>
            </tr>
            </table>-->
            <!-- Top hotel -->

            <!--<br />-->
            <!-- Top Exchang -->
            <table width="250" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td height="40" bgcolor="#90b51f" class="h3"><img src="images/icon_exchang.jpg" width="40"
                                                                      height="40" border="0" align="absmiddle"/> Exchang
                        Rages
                    </td>
                </tr>
                <tr>
                    <td height="165" bgcolor="#FFFFFF" align="center">
                        <iframe marginWidth=0 marginHeight=0 src="http://www.bangkokbank.com/fxbanner/banner1.htm"
                                frameBorder=0 width=173 scrolling=no height=165></iframe>
                    </td>
                </tr>
                <tr>
                    <td height="10"><img src="images/linebox.png" width="250" height="10"/></td>
                </tr>
            </table>
            <!-- Top Exchang -->
            <br/>
            <!-- Top Weather -->
            <table width="250" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td height="40" bgcolor="#457498" class="h3"><img src="images/icon_weather.jpg" width="40"
                                                                      height="40" border="0" align="absmiddle"/> Weather
                    </td>
                </tr>
                <tr>
                    <td height="155" bgcolor="#FFFFFF" align="center">
                        <div style="margin:auto;text-align:center;">
                            <div style="text-align:center;">
                                <a target="_blank"
                                   style="font-size:14px;color:#039;font-family:arial;font-weight:bold; text-decoration:none;"
                                   title="Golf WeatherOnline"
                                   href="http://www.weatheronline.co.uk/weather/maps/city?GLOC=&LANG=en&WMO=&LEVEL=100&R=400"></a>
                            </div>
                            <iframe width="180" height="150" marginheight="0" marginwidth="0" frameborder="0"
                                    src="http://www.weatheronline.co.uk/cgi-bin/homecif?WMO=48564&L=en&WIDTH=180&HEIGHT=150&CLOUD=0&V=2">
                            </iframe>
                        </div>


                    </td>
                </tr>
                <tr>
                    <td height="10"><img src="images/linebox.png" width="250" height="10"/></td>
                </tr>
            </table>
            <!-- Top Weather -->
        </td>
        <td width="20">&nbsp;</td>
        <td width="730" valign="top">
            <table width="730" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td height="40" bgcolor="#EC8324" class="h2"><img src="images/blank.gif" width="10" height="40"
                                                                      border="0" align="absmiddle">
                        <font color="FFFFFF"> Hot Products.</font></td>
                </tr>
            </table>
            <table width="730" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="365" height="15" valign="top" class="h1"><img src="images/pic_under_line.jpg" width="200"
                                                                             height="15" border="0"/></td>
                    <td align="right" valign="middle" class="h1"><img src="images/pic_under_line1.jpg" width="200"
                                                                      height="15" border="0"/></td>
                </tr>
            </table>
            <!-- bar slide -->
            <div id="accordion-container">
                <h2 class="accordion-header" id="head_armeenhello_16"><img src="images/icon-hotel.png" width="24"
                                                                           height="15" border="0" align="absmiddle"/>
                    Hotel</h2>
                <div class="accordion-content" id="armeenhello_16">
                    <p class="first-p">

                        <?php
                        $sql = "SELECT * ";
                        $sql .= "FROM hotels ";
                        $sql .= "WHERE hot_id > 0 ";
                        $sql .= "AND hot_hot = '1' ";

                        $sql .= "order by hot_name ASC ";

                        $result = mysql_query("SELECT * FROM hotels");
                        $results = mysql_query($sql);
                        while ($rows = mysql_fetch_array($results)){

                        ?>

                        <!-- hotel 01-->
                    <table width="710" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td width="170"><img src="resizer.php?imgfile=photo/hotels/<?= $rows['photo2'] ?>&size=200"
                                                 width="155" height="115"/>&nbsp;</td>
                            <td width="540" valign="top">
                                <table width="540" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td class="h2" height="25"><?= $rows['hot_name'] ?></td>
                                    </tr>
                                    <tr>
                                        <td class="easy" height="25"
                                            valign="top"><?= get_value('hotel_con_area', 'con_id', 'con_name', $rows['area_id']) ?></td>
                                    </tr>
                                    <tr>
                                        <td class="easy" height="25"
                                            valign="top"><?= get_value('hotel_con_room_type', 'con_id', 'con_name', $rows['room_type_id']) ?></td>
                                    </tr>
                                    <tr>
                                        <td class="easy"
                                            height="40"><?= mb_strimwidth(stripslashes($rows['description']), 0, 100, "...", "UTF-8") ?></td>
                                    </tr>
                                </table>
                                <table width="540" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td height="30" width="100" bgcolor="c5d8df" class="easy" align="center"><a
                                                href="hotel_detail.php?hotels_id=<?= $rows[0] ?>" class="linkfoot">More
                                                detail</a></td>
                                        <td height="30" width="80" bgcolor="c5d8df" class="easy" align="center"><a
                                                href="hotel_detail.php?hotels_id=<?= $rows[0] ?>#div_book"
                                                class="linkfoot">Booking</a></td>
                                        <td height="30" bgcolor="c5d8df" class="h2" align="right"></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <!-- hotel 01-->

                    <table width="710" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td><img src="images/blank.gif" width="10" height="10" border="0"/></td>
                        </tr>
                        <tr>
                            <td class="footdot"><img src="images/blank.gif" width="100" height="1" border="0"/></td>
                        </tr>
                        <tr>
                            <td><img src="images/blank.gif" width="10" height="10" border="0"/></td>
                        </tr>
                    </table>

                    <?php
                    } // END while($rows = mysql_fetch_array($results)){
                    ?>

                    </p>
                </div>
                <!-- 01 -->


                <h2 class="accordion-header" id="head_armeenhello_16"><img src="images/icon-bost.png" width="24"
                                                                           height="15" border="0" align="absmiddle"/>
                    Boat Services</h2>
                <div class="accordion-content" id="armeenhello_16">
                    <p class="first-p">

                        <?php
                        $sql = "SELECT * ";
                        $sql .= "FROM boattransfers ";
                        $sql .= "WHERE bot_id > 0 ";
                        $sql .= "AND bot_hot = '1' ";

                        $sql .= "order by bot_name ASC ";

                        $results = mysql_query($sql);

                        while ($rows = mysql_fetch_array($results)){
                        ?>

                        <!-- boat 01-->
                    <table width="710" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td width="170"><img
                                    src="resizer.php?imgfile=photo/boattransfers/<?= $rows['photo2'] ?>&size=200"
                                    width="155" height="115"/>&nbsp;</td>
                            <td width="540" valign="top">
                                <table width="540" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td class="h3" height="25"><?= $rows['bot_name'] ?></td>
                                    </tr>
                                    <tr>
                                        <td class="easy" height="20" valign="top">From
                                            : <?= get_value('boattransfer_con_area', 'con_id', 'con_name', $rows['area_id']) ?></td>
                                    </tr>
                                    <tr>
                                        <td class="easy" height="20" valign="top">To
                                            : <?= get_value('boattransfer_con_area', 'con_id', 'con_name', $rows['area_to_id']) ?></td>
                                    </tr>
                                    <tr>
                                        <td class="easy" height="20" valign="top">Class
                                            : <?= get_value('boattransfer_class', 'c_id', 'c_name', $rows['boattransfer_class_id']) ?></td>
                                    </tr>
                                    <tr>
                                        <td class="easy" height="20" valign="top">Time
                                            : <?= get_value('boattransfer_con_time', 'con_id', 'con_name', $rows['time_id']) ?></td>
                                    </tr>
                                </table>
                                <table width="540" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td height="30" width="100" bgcolor="c5d8df" class="easy" align="center"><a
                                                href="boattransfer_detail.php?boattransfers_id=<?= $rows[0] ?>"
                                                class="linkfoot">More detail</a></td>
                                        <td height="30" width="80" bgcolor="c5d8df" class="easy" align="center"><a
                                                href="boattransfer_detail.php?boattransfers_id=<?= $rows[0] ?>#div_book"
                                                class="linkfoot">Booking</a></td>
                                        <td height="30" bgcolor="c5d8df" class="h2" align="right"></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <!-- hotel 01-->

                    <table width="710" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td><img src="images/blank.gif" width="10" height="10" border="0"/></td>
                        </tr>
                        <tr>
                            <td class="footdot"><img src="images/blank.gif" width="100" height="1" border="0"/></td>
                        </tr>
                        <tr>
                            <td><img src="images/blank.gif" width="10" height="10" border="0"/></td>
                        </tr>
                    </table>

                    <?php
                    } // END while($rows = mysql_fetch_array($results)){
                    ?>

                    </p>
                </div>
                <!-- end 01 -->

                <!--02-->
                <h2 class="accordion-header" id="head_armeenhello_17"><img src="images/icon-car.png" width="24"
                                                                           height="15" border="0" align="absmiddle"/>
                    Bus Transfer</h2>
                <div class="accordion-content" id="armeenhello_17">
                    <p class="first-p">

                        <?php
                        $sql = "SELECT * ";
                        $sql .= "FROM cartransfers ";
                        $sql .= "WHERE ct_id > 0 ";
                        $sql .= "AND ct_hot = '1' ";

                        $sql .= "order by ct_name ASC ";

                        $results = mysql_query($sql);

                        while ($rows = mysql_fetch_array($results)){
                        ?>

                        <!-- hotel 01-->
                    <table width="710" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td width="170"><img
                                    src="resizer.php?imgfile=photo/bustransfers/<?= $rows['photo2'] ?>&size=200"
                                    width="155" height="115"/>&nbsp;</td>
                            <td width="540" valign="top">
                                <table width="540" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td class="h2" height="25"><?= $rows['ct_name'] ?></td>
                                    </tr>
                                    <tr>
                                        <td class="easy" height="20" valign="top">From
                                            : <?= get_value('cartransfer_con_area', 'con_id', 'con_name', $rows['area_id']) ?></td>
                                    </tr>
                                    <tr>
                                        <td class="easy" height="20" valign="top">To
                                            : <?= get_value('cartransfer_con_area', 'con_id', 'con_name', $rows['area_to_id']) ?></td>
                                    </tr>
                                    <tr>
                                        <td class="easy" height="20" valign="top">Transfer Type
                                            : <?= get_value('cartransfer_type', 'c_id', 'c_name', $rows['cartransfer_type_id']) ?></td>
                                    </tr>
                                    <tr>
                                        <td class="easy" height="20" valign="top">Time
                                            : <?= get_value('cartransfer_con_time', 'con_id', 'con_name', $rows['time_id']) ?></td>
                                    </tr>
                                </table>
                                <table width="540" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td height="30" width="100" bgcolor="c5d8df" class="easy" align="center"><a
                                                href="bustransfer_detail.php?bustransfers_id=<?= $rows[0] ?>"
                                                class="linkfoot">More detail</a></td>
                                        <td height="30" width="80" bgcolor="c5d8df" class="easy" align="center"><a
                                                href="bustransfer_detail.php?bustransfers_id=<?= $rows[0] ?>#div_book"
                                                class="linkfoot">Booking</a></td>
                                        <td height="30" bgcolor="c5d8df" class="h2" align="right"></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <!-- hotel 01-->

                    <table width="710" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td><img src="images/blank.gif" width="10" height="10" border="0"/></td>
                        </tr>
                        <tr>
                            <td class="footdot"><img src="images/blank.gif" width="100" height="1" border="0"/></td>
                        </tr>
                        <tr>
                            <td><img src="images/blank.gif" width="10" height="10" border="0"/></td>
                        </tr>
                    </table>

                    <?php
                    } // END while($rows = mysql_fetch_array($results)){
                    ?>

                    </p></div>
                <!-- end 02-->

                <!--02-->
                <h2 class="accordion-header" id="head_armeenhello_17"><img src="images/icon-car.png" width="24"
                                                                           height="15" border="0" align="absmiddle"/>
                    Pick Up Transfer</h2>
                <div class="accordion-content" id="armeenhello_17">
                    <p class="first-p">

                        <?php
                        $sql = "SELECT * ";
                        $sql .= "FROM pickuptransfers ";
                        $sql .= "WHERE put_id > 0 ";
                        $sql .= "AND put_hot = '1' ";

                        $sql .= "order by put_name ASC ";

                        $results = mysql_query($sql);

                        while ($rows = mysql_fetch_array($results)){
                        ?>

                        <!-- hotel 01-->
                    <table width="710" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td width="170"><img
                                    src="resizer.php?imgfile=photo/pickuptransfers/<?= $rows['photo2'] ?>&size=200"
                                    width="155" height="115"/>&nbsp;</td>
                            <td width="540" valign="top">
                                <table width="540" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td class="h2" height="25"><?= $rows['put_name'] ?></td>
                                    </tr>
                                    <tr>
                                        <td class="easy" height="20" valign="top">From
                                            : <?= get_value('pickuptransfer_con_area', 'con_id', 'con_name', $rows['area_id']) ?></td>
                                    </tr>
                                    <tr>
                                        <td class="easy" height="20" valign="top">To
                                            : <?= get_value('pickuptransfer_con_area', 'con_id', 'con_name', $rows['area_to_id']) ?></td>
                                    </tr>
                                    <tr>
                                        <td class="easy" height="20" valign="top">Transfer Type
                                            : <?= get_value('pickuptransfer_type', 'c_id', 'c_name', $rows['pickuptransfer_type_id']) ?></td>
                                    </tr>
                                    <tr>
                                        <td class="easy" height="20" valign="top">Time
                                            : <?= get_value('pickuptransfer_con_time', 'con_id', 'con_name', $rows['time_id']) ?></td>
                                    </tr>
                                </table>
                                <table width="540" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td height="30" width="100" bgcolor="c5d8df" class="easy" align="center"><a
                                                href="pickuptransfer_detail.php?pickuptransfers_id=<?= $rows[0] ?>"
                                                class="linkfoot">More detail</a></td>
                                        <td height="30" width="80" bgcolor="c5d8df" class="easy" align="center"><a
                                                href="pickuptransfer_detail.php?pickuptransfers_id=<?= $rows[0] ?>#div_book"
                                                class="linkfoot">Booking</a></td>
                                        <td height="30" bgcolor="c5d8df" class="h2" align="right"></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <!-- hotel 01-->

                    <table width="710" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td><img src="images/blank.gif" width="10" height="10" border="0"/></td>
                        </tr>
                        <tr>
                            <td class="footdot"><img src="images/blank.gif" width="100" height="1" border="0"/></td>
                        </tr>
                        <tr>
                            <td><img src="images/blank.gif" width="10" height="10" border="0"/></td>
                        </tr>
                    </table>

                    <?php
                    } // END while($rows = mysql_fetch_array($results)){
                    ?>

                    </p></div>
                <!-- end 02-->

                <!--02-->
                <h2 class="accordion-header" id="head_armeenhello_17"><img src="images/icon-car.png" width="24"
                                                                           height="15" border="0" align="absmiddle"/>
                    Private Land Transfer</h2>
                <div class="accordion-content" id="armeenhello_17">
                    <p class="first-p">

                        <?php
                        $sql = "SELECT * ";
                        $sql .= "FROM privatelandtransfers ";
                        $sql .= "WHERE plt_id > 0 ";
                        $sql .= "AND plt_hot = '1' ";

                        $sql .= "order by plt_name ASC ";

                        $results = mysql_query($sql);

                        while ($rows = mysql_fetch_array($results)){
                        ?>

                        <!-- hotel 01-->
                    <table width="710" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td width="170"><img
                                    src="resizer.php?imgfile=photo/privatelandtransfers/<?= $rows['photo2'] ?>&size=200"
                                    width="155" height="115"/>&nbsp;</td>
                            <td width="540" valign="top">
                                <table width="540" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td class="h2" height="25"><?= $rows['plt_name'] ?></td>
                                    </tr>
                                    <tr>
                                        <td class="easy" height="20" valign="top">From
                                            : <?= get_value('privatelandtransfer_con_area', 'con_id', 'con_name', $rows['area_id']) ?></td>
                                    </tr>
                                    <tr>
                                        <td class="easy" height="20" valign="top">To
                                            : <?= get_value('privatelandtransfer_con_area', 'con_id', 'con_name', $rows['area_to_id']) ?></td>
                                    </tr>
                                    <tr>
                                        <td class="easy" height="20" valign="top">Transfer Type
                                            : <?= get_value('privatelandtransfer_type', 'c_id', 'c_name', $rows['privatelandtransfer_type_id']) ?></td>
                                    </tr>
                                    <tr>
                                        <td class="easy" height="20" valign="top">Time
                                            : <?= get_value('privatelandtransfer_con_time', 'con_id', 'con_name', $rows['time_id']) ?></td>
                                    </tr>
                                </table>
                                <table width="540" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td height="30" width="100" bgcolor="c5d8df" class="easy" align="center">
                                            <a href="privatelandtransfer_detail.php?privatelandtransfers_id=<?= $rows[0] ?>"
                                               class="linkfoot">More detail</a></td>
                                        <td height="30" width="80" bgcolor="c5d8df" class="easy" align="center">
                                            <a href="privatelandtransfer_detail.php?privatelandtransfers_id=<?= $rows[0] ?>#div_book"
                                               class="linkfoot">Booking</a></td>
                                        <td height="30" bgcolor="c5d8df" class="h2" align="right"></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <!-- hotel 01-->

                    <table width="710" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td><img src="images/blank.gif" width="10" height="10" border="0"/></td>
                        </tr>
                        <tr>
                            <td class="footdot"><img src="images/blank.gif" width="100" height="1" border="0"/></td>
                        </tr>
                        <tr>
                            <td><img src="images/blank.gif" width="10" height="10" border="0"/></td>
                        </tr>
                    </table>

                    <?php
                    } // END while($rows = mysql_fetch_array($results)){
                    ?>

                    </p></div>
                <!-- end 02-->

                <!--02-->
                <h2 class="accordion-header" id="head_armeenhello_17"><img src="images/icon-tickert.png" width="24"
                                                                           height="15" border="0" align="absmiddle"/>
                    Train Transfer</h2>
                <div class="accordion-content" id="armeenhello_17">
                    <p class="first-p">

                        <?php
                        $sql = "SELECT * ";
                        $sql .= "FROM traintransfers ";
                        $sql .= "WHERE train_id > 0 ";
                        $sql .= "AND train_hot = '1' ";

                        $sql .= "order by train_name ASC ";

                        $results = mysql_query($sql);

                        while ($rows = mysql_fetch_array($results)){
                        ?>

                        <!-- hotel 01-->
                    <table width="710" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td width="170"><img
                                    src="resizer.php?imgfile=photo/traintransfers/<?= $rows['photo2'] ?>&size=200"
                                    width="155" height="115"/>&nbsp;</td>
                            <td width="540" valign="top">
                                <table width="540" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td class="h2" height="25"><?= $rows['train_name'] ?></td>
                                    </tr>
                                    <tr>
                                        <td class="easy" height="20" valign="top">Train
                                            : <?= get_value('traintransfer_con_train', 'con_id', 'con_name', $rows['train_type_id']) ?></td>
                                    </tr>
                                    <tr>
                                        <td class="easy" height="20" valign="top">Train No.
                                            : <?= $rows['train_number'] ?></td>
                                    </tr>
                                    <tr>
                                        <td class="easy" height="20" valign="top">Class
                                            : <?= get_value('traintransfer_class', 'c_id', 'c_name', $rows['traintransfer_class_id']) ?></td>
                                    </tr>
                                    <tr>
                                        <td class="easy" height="20" valign="top">From
                                            : <?= get_value('traintransfer_con_province', 'con_id', 'con_name', $rows['province_id']) ?></td>
                                    </tr>
                                    <tr>
                                        <td class="easy" height="20" valign="top">To
                                            : <?= get_value('traintransfer_con_province', 'con_id', 'con_name', $rows['province_to_id']) ?></td>
                                    </tr>
                                    <tr>
                                        <td class="easy" height="20" valign="top">Time
                                            : <?= get_value('traintransfer_con_time', 'con_id', 'con_name', $rows['time_from_id']) ?>
                                            - <?= get_value(traintransfer_con_time, 'con_id', 'con_name', $rows['time_to_id']) ?></td>
                                    </tr>
                                </table>
                                <table width="540" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td height="30" width="100" bgcolor="c5d8df" class="easy" align="center">
                                            <a href="traintransfer_detail.php?traintransfers_id=<?= $rows[0] ?>"
                                               class="linkfoot">More detail</a></td>
                                        <td height="30" width="80" bgcolor="c5d8df" class="easy" align="center">
                                            <a href="traintransfer_detail.php?traintransfers_id=<?= $rows[0] ?>#div_book"
                                               class="linkfoot">Booking</a></td>
                                        <td height="30" bgcolor="c5d8df" class="h2" align="right"></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <!-- hotel 01-->

                    <table width="710" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td><img src="images/blank.gif" width="10" height="10" border="0"/></td>
                        </tr>
                        <tr>
                            <td class="footdot"><img src="images/blank.gif" width="100" height="1" border="0"/></td>
                        </tr>
                        <tr>
                            <td><img src="images/blank.gif" width="10" height="10" border="0"/></td>
                        </tr>
                    </table>

                    <?php
                    } // END while($rows = mysql_fetch_array($results)){
                    ?>

                    </p></div>
                <!-- end 02-->

                <!--03-->
                <h2 class="accordion-header" id="head_armeenhello_17"><img src="images/icon-tour.png" width="24"
                                                                           height="15" border="0" align="absmiddle"/>
                    Tours</h2>
                <div class="accordion-content" id="armeenhello_17">
                    <p class="first-p">

                        <?php
                        $sql = "SELECT * ";
                        $sql .= "FROM tours ";
                        $sql .= "WHERE tou_id > 0 ";
                        $sql .= "AND tou_hot = '1' ";

                        $sql .= "order by tou_name ASC ";

                        $results = mysql_query($sql);

                        while ($rows = mysql_fetch_array($results)){
                        ?>

                        <!-- hotel 01-->
                    <table width="710" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td width="170"><img src="resizer.php?imgfile=photo/tours/<?= $rows['photo2'] ?>&size=200"
                                                 width="155" height="115"/>&nbsp;</td>
                            <td width="540" valign="top">
                                <table width="540" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td class="h2" height="25"><?= $rows['tou_name'] ?></td>
                                    </tr>
                                    <tr>
                                        <td class="easy" height="20" valign="top">Area
                                            : <?= get_value('tour_con_area', 'con_id', 'con_name', $rows['area_id']) ?></td>
                                    </tr>
                                    <tr>
                                        <td class="easy" height="20" valign="top">Category
                                            : <?= get_value('tour_con_category', 'con_id', 'con_name', $rows['category_id']) ?></td>
                                    </tr>
                                    <tr>
                                        <td class="easy"
                                            height="40"><?= mb_strimwidth(stripslashes($rows['tou_shortdetail']), 0, 100, "...", "UTF-8") ?></td>
                                    </tr>
                                </table>
                                <table width="540" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td height="30" width="100" bgcolor="c5d8df" class="easy" align="center"><a
                                                href="tour_detail.php?tours_id=<?= $rows[0] ?>" class="linkfoot">More
                                                detail</a></td>
                                        <td height="30" width="80" bgcolor="c5d8df" class="easy" align="center"><a
                                                href="tour_detail.php?tours_id=<?= $rows[0] ?>#div_book"
                                                class="linkfoot">Booking</a></td>
                                        <td height="30" bgcolor="c5d8df" class="h2" align="right"></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <!-- hotel 01-->

                    <table width="710" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td><img src="images/blank.gif" width="10" height="10" border="0"/></td>
                        </tr>
                        <tr>
                            <td class="footdot"><img src="images/blank.gif" width="100" height="1" border="0"/></td>
                        </tr>
                        <tr>
                            <td><img src="images/blank.gif" width="10" height="10" border="0"/></td>
                        </tr>
                    </table>

                    <?php
                    } // END while($rows = mysql_fetch_array($results)){
                    ?>

                    </p></div>
                <!-- end 03-->

                <!--03-->
                <h2 class="accordion-header" id="head_armeenhello_17"><img src="images/icon-tour.png" width="24"
                                                                           height="15" border="0" align="absmiddle"/>
                    Activity</h2>
                <div class="accordion-content" id="armeenhello_17">
                    <p class="first-p">

                        <?php
                        $sql = "SELECT * ";
                        $sql .= "FROM activities ";
                        $sql .= "WHERE act_id > 0 ";
                        $sql .= "AND act_hot = '1' ";

                        $sql .= "order by act_name ASC ";

                        $results = mysql_query($sql);

                        while ($rows = mysql_fetch_array($results)){
                        ?>

                        <!-- hotel 01-->
                    <table width="710" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td width="170"><img
                                    src="resizer.php?imgfile=photo/activities/<?= $rows['photo2'] ?>&size=200"
                                    width="155" height="115"/>&nbsp;</td>
                            <td width="540" valign="top">
                                <table width="540" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td class="h2" height="25"><?= $rows['act_name'] ?></td>
                                    </tr>
                                    <tr>
                                        <td class="easy" height="20" valign="top">Area
                                            : <?= get_value('activity_con_area', 'con_id', 'con_name', $rows['area_id']) ?></td>
                                    </tr>
                                    <tr>
                                        <td class="easy" height="20" valign="top">Category
                                            : <?= get_value('activity_con_category', 'con_id', 'con_name', $rows['category_id']) ?></td>
                                    </tr>
                                    <tr>
                                        <td class="easy"
                                            height="40"><?= mb_strimwidth(stripslashes($rows['act_shortdetail']), 0, 100, "...", "UTF-8") ?></td>
                                    </tr>
                                </table>
                                <table width="540" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td height="30" width="100" bgcolor="c5d8df" class="easy" align="center"><a
                                                href="activity_detail.php?activities_id=<?= $rows[0] ?>"
                                                class="linkfoot">More detail</a></td>
                                        <td height="30" width="80" bgcolor="c5d8df" class="easy" align="center"><a
                                                href="activity_detail.php?activities_id=<?= $rows[0] ?>#div_book"
                                                class="linkfoot">Booking</a></td>
                                        <td height="30" bgcolor="c5d8df" class="h2" align="right"></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <!-- hotel 01-->

                    <table width="710" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td><img src="images/blank.gif" width="10" height="10" border="0"/></td>
                        </tr>
                        <tr>
                            <td class="footdot"><img src="images/blank.gif" width="100" height="1" border="0"/></td>
                        </tr>
                        <tr>
                            <td><img src="images/blank.gif" width="10" height="10" border="0"/></td>
                        </tr>
                    </table>

                    <?php
                    } // END while($rows = mysql_fetch_array($results)){
                    ?>

                    </p></div>
                <!-- end 03-->

                <!--04-->
                <h2 class="accordion-header" id="head_armeenhello_17"><img src="images/icon-tickert.png" width="24"
                                                                           height="15" border="0" align="absmiddle"/>
                    Combo Product</h2>
                <div class="accordion-content" id="armeenhello_17">
                    <p class="first-p">

                        <?php
                        $sql = "SELECT * ";
                        $sql .= "FROM packages ";
                        $sql .= "WHERE pac_id > 0 ";
                        $sql .= "AND pac_hot = '1' ";

                        $sql .= "order by pac_name ASC ";

                        $results = mysql_query($sql);

                        while ($rows = mysql_fetch_array($results)){
                        ?>

                        <!-- hotel 01-->
                    <table width="710" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td width="170"><img
                                    src="resizer.php?imgfile=photo/packages/<?= $rows['photo2'] ?>&size=200" width="155"
                                    height="115"/>&nbsp;</td>
                            <td width="540" valign="top">
                                <table width="540" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td class="h2" height="25"><?= $rows['pac_name'] ?></td>
                                    </tr>
                                    <tr>
                                        <td class="easy" height="20" valign="top">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td class="easy" height="20" valign="top">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td class="easy" height="20" valign="top">&nbsp;</td>
                                    </tr>
                                </table>
                                <table width="540" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td height="30" width="100" bgcolor="c5d8df" class="easy" align="center">
                                            <a href="package_detail.php?packages_id=<?= $rows[0] ?>" class="linkfoot">More
                                                detail</a></td>
                                        <td height="30" width="80" bgcolor="c5d8df" class="easy" align="center">
                                            <a href="package_detail.php?packages_id=<?= $rows[0] ?>#div_book"
                                               class="linkfoot">Booking</a></td>
                                        <td height="30" bgcolor="c5d8df" class="h2" align="right"></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <!-- hotel 01-->

                    <table width="710" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td><img src="images/blank.gif" width="10" height="10" border="0"/></td>
                        </tr>
                        <tr>
                            <td class="footdot"><img src="images/blank.gif" width="100" height="1" border="0"/></td>
                        </tr>
                        <tr>
                            <td><img src="images/blank.gif" width="10" height="10" border="0"/></td>
                        </tr>
                    </table>

                    <?php } // END while($rows = mysql_fetch_array($results)){ ?>

                    </p></div>
                <!-- end 04-->
            </div>
            <!-- bar slide -->

            <table width="730" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td height="10" valign="middle" class="h1">&nbsp;</td>
                </tr>
                <tr>
                    <td height="40" valign="middle" bgcolor="#ffcc00" class="h2"><img src="images/icon_hotel.jpg"
                                                                                      width="40" height="40" border="0"
                                                                                      align="absmiddle"/> Best Seller
                        Hotel
                    </td>
                </tr>
                <tr>
                    <td height="10" valign="middle" class="h1"><img src="images/blank.gif" width="100" height="10"
                                                                    border="0"/></td>
                </tr>
                <tr>
                    <td height="10" valign="middle">

                        <?php
                        $i = 0;

                        $sql_best = "SELECT * ";
                        $sql_best .= "FROM hotels ";
                        $sql_best .= "WHERE hot_id > 0 ";
                        $sql_best .= "AND hot_best = '1' ";
                        //$sql_best .= "ORDER BY last_edit_time DESC ";
                        $sql_best .= "LIMIT 6 ";

                        $results_best = mysql_query($sql_best);
                        ?>

                        <table width="100%" border="0" cellspacing="0" cellpadding="0">

                            <?php while ($rows_best = mysql_fetch_array($results_best)) { ?>

                                <tr>
                                    <td height="150"><a href="hotel_detail.php?hotels_id=<?= $rows_best[0] ?>"
                                                        style="color:#000000;">
                                            <img src="photo/hotels/<?= $rows_best['photo1'] ?>" width="226" height="150"
                                                 border="0" class="borderpic"/><br/>
                                            <strong><?= $rows_best['hot_name'] ?></strong></a>
                                    </td>
                                    <td width="20">&nbsp;</td>

                                    <?php if ($rows_best = mysql_fetch_array($results_best)) { ?>

                                        <td><a href="hotel_detail.php?hotels_id=<?= $rows_best[0] ?>"
                                               style="color:#000000;">
                                                <img src="photo/hotels/<?= $rows_best['photo1'] ?>" width="226"
                                                     height="150" border="0" class="borderpic"/><br/>
                                                <strong><?= $rows_best['hot_name'] ?></strong></a>
                                        </td>
                                        <td width="20">&nbsp;</td>

                                    <?php } ?>

                                    <?php if ($rows_best = mysql_fetch_array($results_best)) { ?>

                                        <td><a href="hotel_detail.php?hotels_id=<?= $rows_best[0] ?>"
                                               style="color:#000000;">
                                                <img src="photo/hotels/<?= $rows_best['photo1'] ?>" width="226"
                                                     height="150" border="0" class="borderpic"/><br/>
                                                <strong><?= $rows_best['hot_name'] ?></strong></a>
                                        </td>

                                    <?php } ?>

                                </tr>

                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>

                            <?php } // END while($rows_best = mysql_fetch_array($results_best)){ ?>

                        </table>

                    </td>
                </tr>
                <tr>
                    <td height="10" valign="middle" class="h1">&nbsp;</td>
                </tr>
                <tr>
                    <td height="40" bgcolor="#FFFFFF" class="h2"><img src="images/blank.gif" width="10" height="40"
                                                                      border="0" align="absmiddle"><font
                            color="#1e73a4"> Welcome to S.S.ADV..</font></td>
                </tr>
            </table>
            <table width="730" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="365" height="15" valign="top" class="h1"><img src="images/pic_under_line.jpg" width="200"
                                                                             height="15" border="0"/></td>
                    <td align="right" valign="middle" class="h1"><img src="images/pic_under_line1.jpg" width="200"
                                                                      height="15" border="0"/></td>
                </tr>
            </table>
            <table width="710" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                    <td valign="top"><p align="justify"><strong>S.S.ADV.</strong>, the experienced boat operater in the
                            Andaman Sea established in 1999, have office network in Phuket Town and Krabi Town in order
                            to provide maximum travel assistance in each destination. </p>
                        <p><strong>Daytrip</strong> to Phi Phi Island by Royal Jet Cruiser 9, our comfortable new vessel
                            takes you to the island all through the year by one &amp; half hours cruise.</p>
                    </td>
                    <td width="180" align="right" valign="top"><img src="images/pic_andaman-01.jpg" width="160"
                                                                    height="130" border="0" class="borderpic"/></td>
                </tr>
            </table>

            <table width="710" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="355"><img src="images/blank.gif" width="10" height="10" border="0"/></td>
                    <td width="355" valign="top"><img src="images/blank.gif" width="10" height="10" border="0"/></td>
                </tr>
                <tr>
                    <td class="footdot"><img src="images/blank.gif" width="100" height="1" border="0"/></td>
                    <td class="footdot"><img src="images/blank.gif" width="100" height="1" border="0"/></td>
                </tr>
                <tr>
                    <td width="355"><img src="images/blank.gif" width="10" height="10" border="0"/></td>
                    <td width="355" valign="top"><img src="images/blank.gif" width="10" height="10" border="0"/></td>
                </tr>
                <tr>
                    <td><img src="images/pic_andaman-02.jpg" width="345" height="210" border="0" class="borderpic"/>
                    </td>
                    <td align="right"><img src="images/pic_andaman-03.jpg" width="345" height="210" border="0"
                                           class="borderpic"/></td>
                </tr>
                <tr>
                    <td width="355"><img src="images/blank.gif" width="10" height="10" border="0"/></td>
                    <td width="355" valign="top"><img src="images/blank.gif" width="10" height="10" border="0"/></td>
                </tr>
                <tr>
                    <td class="footdot"><img src="images/blank.gif" width="100" height="1" border="0"/></td>
                    <td class="footdot"><img src="images/blank.gif" width="100" height="1" border="0"/></td>
                </tr>
                <tr>
                    <td width="355"><img src="images/blank.gif" width="10" height="10" border="0"/></td>
                    <td width="355" valign="top"><img src="images/blank.gif" width="10" height="10" border="0"/></td>
                </tr>
            </table>
            <table width="710" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="70"><img src="images/pic_andaman-04.jpg" width="63" height="63" border="0"/></td>
                    <td width="185" valign="top" class="easy">
                        <span class="h2">Save money</span><br/>
                        Exclusive deals for S.S.ADV.
                    </td>
                    <td width="70"><img src="images/pic_andaman-05.jpg" width="63" height="63" border="0"/></td>
                    <td width="173" valign="top" class="easy">
                        <span class="h2">Easy choice</span><br/>
                        Hotels in destiantions<br/>
                        review from real custmers
                    </td>
                    <td width="75"><img src="images/pic_andaman-06.jpg" width="70" height="63" border="0"/></td>
                    <td valign="top" class="easy">
                        <span class="h2">Booking !!</span><br/>
                        Easy Booking !<br/>
                    </td>
                </tr>
            </table>


        </td>
    </tr>
</table>
<br/>
<!-- Detail -->

<!-- include footer-->
<?php include("inc_footer.php"); ?>
<!-- end include footer -->


</body>
</html>
