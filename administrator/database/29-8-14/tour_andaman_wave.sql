-- phpMyAdmin SQL Dump
-- version 2.10.3
-- http://www.phpmyadmin.net
-- 
-- Host: localhost
-- Generation Time: Aug 29, 2014 at 11:40 AM
-- Server version: 5.0.51
-- PHP Version: 5.2.6

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

-- 
-- Database: `tour_andaman_wave`
-- 

-- --------------------------------------------------------

-- 
-- Table structure for table `activities`
-- 

CREATE TABLE `activities` (
  `act_id` int(11) NOT NULL auto_increment,
  `category_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `province_id` int(11) NOT NULL,
  `area_id` int(11) NOT NULL,
  `act_name` varchar(200) collate utf8_unicode_ci NOT NULL,
  `act_detail` text collate utf8_unicode_ci NOT NULL,
  `photo1` varchar(100) collate utf8_unicode_ci NOT NULL,
  `photo2` varchar(100) collate utf8_unicode_ci NOT NULL,
  `photo3` varchar(100) collate utf8_unicode_ci NOT NULL,
  `photo4` varchar(100) collate utf8_unicode_ci NOT NULL,
  `photo5` varchar(100) collate utf8_unicode_ci NOT NULL,
  `photo6` varchar(100) collate utf8_unicode_ci NOT NULL,
  `photo7` varchar(100) collate utf8_unicode_ci NOT NULL,
  `photo8` varchar(100) collate utf8_unicode_ci NOT NULL,
  `photo9` varchar(100) collate utf8_unicode_ci NOT NULL,
  `photo10` varchar(100) collate utf8_unicode_ci NOT NULL,
  `photo11` varchar(100) collate utf8_unicode_ci NOT NULL,
  `photo12` varchar(100) collate utf8_unicode_ci NOT NULL,
  `photo13` varchar(100) collate utf8_unicode_ci NOT NULL,
  `photo14` varchar(100) collate utf8_unicode_ci NOT NULL,
  `photo15` varchar(100) collate utf8_unicode_ci NOT NULL,
  `photo16` varchar(100) collate utf8_unicode_ci NOT NULL,
  `photo17` varchar(100) collate utf8_unicode_ci NOT NULL,
  `photo18` varchar(100) collate utf8_unicode_ci NOT NULL,
  `photo19` varchar(100) collate utf8_unicode_ci NOT NULL,
  `photo20` varchar(100) collate utf8_unicode_ci NOT NULL,
  `act_ref` varchar(100) collate utf8_unicode_ci NOT NULL,
  `act_pro_time_arr` text collate utf8_unicode_ci NOT NULL,
  `act_pro_des_arr` text collate utf8_unicode_ci NOT NULL,
  `act_remark` text collate utf8_unicode_ci NOT NULL,
  `act_condition` text collate utf8_unicode_ci NOT NULL,
  `act_shortdetail` text collate utf8_unicode_ci NOT NULL,
  `activitypricetype_id` int(11) NOT NULL,
  `last_edit_time` datetime NOT NULL,
  `agents_sup_id` int(11) NOT NULL,
  `act_hot` int(1) NOT NULL,
  PRIMARY KEY  (`act_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

-- 
-- Dumping data for table `activities`
-- 

INSERT INTO `activities` VALUES (1, 8, 11, 9, 9, 'Phuket FantaSea', 'Phuket FantaSea\r\nPhuket FantaSea\r\nPhuket FantaSea\r\nPhuket FantaSea\r\nPhuket FantaSea\r\nPhuket FantaSea Phuket FantaSeaPhuket FantaSeaPhuket FantaSea\r\nPhuket FantaSeaPhuket FantaSeaPhuket FantaSeaPhuket FantaSea', '1398150587_1.jpg', '1398150587_2.jpg', '1398150587_3.jpg', '1398150587_4.jpg', '1398150655_5.jpg', '1398150655_6.jpg', '1398150655_7.jpg', '1398150655_8.jpg', '1398150655_9.jpg', '', '1398150655_11.jpg', '1398150655_12.jpg', '1398150655_13.jpg', '1398150655_14.jpg', '1398150655_15.jpg', '1398150655_16.jpg', '1398150655_17.jpg', '', '', '', 'ACT001', '~15:00 - 16:00 pm.~16.00 - 19:00 pm.~19.00 - 21:00 pm.~21.00 - 23:00 pm.~~~~~~~', '~Registration~Meal Time~Show time V1~Show time V2~~~~~~~', 'Remark Remark Remark \r\nRemark Remark Remark ', 'Condition Condition Condition \r\nCondition Condition Condition \r\nCondition Condition Condition ', 'Phuket FantaSeaPhuket FantaSeaPhuket FantaSea', 2, '2014-04-23 10:55:25', 13, 0);
INSERT INTO `activities` VALUES (2, 9, 1, 1, 7, 'Fishing Game by VAVA', 'Description Description Description \r\nDescription Description Description \r\nDescription Description Description \r\nDescription Description Description ', '1398237607_1.jpg', '1398237607_2.jpg', '1398237607_3.jpg', '1398237607_4.jpg', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'ACT002', '~T1~T2~T3~~~~~~~~', '~Description1~Description2~Description3~~~~~~~~', 'Remark Remark Remark Remark Remark Remark Remark Remark \r\nRemark Remark Remark Remark ', 'Condition 01 Condition 01\r\nCondition 01Condition 01\r\nCondition 01Condition 01\r\nCondition 01', 'Short DescriptionShort DescriptionShort Description', 1, '2014-05-26 17:08:51', 1, 0);

-- --------------------------------------------------------

-- 
-- Table structure for table `activity_allocation`
-- 

CREATE TABLE `activity_allocation` (
  `actallo_id` int(11) NOT NULL auto_increment,
  `activities_id` int(11) NOT NULL,
  `activityratetype_id` int(11) NOT NULL,
  `actallo_datefrom` date NOT NULL,
  `actallo_dateto` date NOT NULL,
  `actallo_global` int(11) NOT NULL,
  `last_edit_time` datetime NOT NULL,
  PRIMARY KEY  (`actallo_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

-- 
-- Dumping data for table `activity_allocation`
-- 

INSERT INTO `activity_allocation` VALUES (1, 2, 5, '2014-04-25', '2014-07-23', 77, '2014-04-23 14:56:03');
INSERT INTO `activity_allocation` VALUES (2, 2, 5, '2014-07-24', '2014-07-27', 777, '2014-04-23 15:31:45');

-- --------------------------------------------------------

-- 
-- Table structure for table `activity_allocationdaily`
-- 

CREATE TABLE `activity_allocationdaily` (
  `actalloday_id` int(11) NOT NULL auto_increment,
  `activities_id` int(11) NOT NULL,
  `activityratetype_id` int(11) NOT NULL,
  `actalloday_date` date NOT NULL,
  `actalloday_value` int(11) NOT NULL,
  `last_edit_time` datetime NOT NULL,
  PRIMARY KEY  (`actalloday_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=187 ;

-- 
-- Dumping data for table `activity_allocationdaily`
-- 

INSERT INTO `activity_allocationdaily` VALUES (1, 2, 5, '2014-04-25', 77, '2014-05-23 12:04:51');
INSERT INTO `activity_allocationdaily` VALUES (2, 2, 5, '2014-04-26', 77, '2014-05-23 12:04:51');
INSERT INTO `activity_allocationdaily` VALUES (3, 2, 5, '2014-04-27', 77, '2014-05-23 12:04:51');
INSERT INTO `activity_allocationdaily` VALUES (4, 2, 5, '2014-04-28', 77, '2014-05-23 12:04:51');
INSERT INTO `activity_allocationdaily` VALUES (5, 2, 5, '2014-04-29', 77, '2014-05-23 12:04:51');
INSERT INTO `activity_allocationdaily` VALUES (6, 2, 5, '2014-04-30', 77, '2014-05-23 12:04:51');
INSERT INTO `activity_allocationdaily` VALUES (7, 2, 5, '2014-05-01', 77, '2014-05-23 12:04:51');
INSERT INTO `activity_allocationdaily` VALUES (8, 2, 5, '2014-05-02', 22, '2014-05-23 12:04:51');
INSERT INTO `activity_allocationdaily` VALUES (9, 2, 5, '2014-05-03', 77, '2014-05-23 12:04:51');
INSERT INTO `activity_allocationdaily` VALUES (10, 2, 5, '2014-05-04', 44, '2014-05-23 12:04:51');
INSERT INTO `activity_allocationdaily` VALUES (11, 2, 5, '2014-05-05', 77, '2014-05-23 12:04:51');
INSERT INTO `activity_allocationdaily` VALUES (12, 2, 5, '2014-05-06', 77, '2014-05-23 12:04:51');
INSERT INTO `activity_allocationdaily` VALUES (13, 2, 5, '2014-05-07', 77, '2014-05-23 12:04:51');
INSERT INTO `activity_allocationdaily` VALUES (14, 2, 5, '2014-05-08', 88, '2014-05-23 12:04:51');
INSERT INTO `activity_allocationdaily` VALUES (15, 2, 5, '2014-05-09', 77, '2014-05-23 12:04:51');
INSERT INTO `activity_allocationdaily` VALUES (16, 2, 5, '2014-05-10', 77, '2014-05-23 12:04:51');
INSERT INTO `activity_allocationdaily` VALUES (17, 2, 5, '2014-05-11', 77, '2014-05-23 12:04:51');
INSERT INTO `activity_allocationdaily` VALUES (18, 2, 5, '2014-05-12', 77, '2014-05-23 12:04:51');
INSERT INTO `activity_allocationdaily` VALUES (19, 2, 5, '2014-05-13', 77, '2014-05-23 12:04:51');
INSERT INTO `activity_allocationdaily` VALUES (20, 2, 5, '2014-05-14', 77, '2014-05-23 12:04:51');
INSERT INTO `activity_allocationdaily` VALUES (21, 2, 5, '2014-05-15', 77, '2014-05-23 12:04:51');
INSERT INTO `activity_allocationdaily` VALUES (22, 2, 5, '2014-05-16', 77, '2014-05-23 12:04:51');
INSERT INTO `activity_allocationdaily` VALUES (23, 2, 5, '2014-05-17', 77, '2014-05-23 12:04:51');
INSERT INTO `activity_allocationdaily` VALUES (24, 2, 5, '2014-05-18', 77, '2014-05-23 12:04:51');
INSERT INTO `activity_allocationdaily` VALUES (25, 2, 5, '2014-05-19', 77, '2014-05-23 12:04:51');
INSERT INTO `activity_allocationdaily` VALUES (26, 2, 5, '2014-05-20', 77, '2014-05-23 12:04:51');
INSERT INTO `activity_allocationdaily` VALUES (27, 2, 5, '2014-05-21', 77, '2014-05-23 12:04:51');
INSERT INTO `activity_allocationdaily` VALUES (28, 2, 5, '2014-05-22', 77, '2014-05-23 12:04:51');
INSERT INTO `activity_allocationdaily` VALUES (29, 2, 5, '2014-05-23', 0, '2014-05-23 12:04:51');
INSERT INTO `activity_allocationdaily` VALUES (30, 2, 5, '2014-05-24', 77, '2014-04-23 15:30:40');
INSERT INTO `activity_allocationdaily` VALUES (31, 2, 5, '2014-05-25', 77, '2014-04-23 15:30:40');
INSERT INTO `activity_allocationdaily` VALUES (32, 2, 5, '2014-05-26', 77, '2014-04-23 15:30:40');
INSERT INTO `activity_allocationdaily` VALUES (33, 2, 5, '2014-05-27', 77, '2014-04-23 15:30:40');
INSERT INTO `activity_allocationdaily` VALUES (34, 2, 5, '2014-05-28', 77, '2014-04-23 15:30:40');
INSERT INTO `activity_allocationdaily` VALUES (35, 2, 5, '2014-05-29', 77, '2014-04-23 15:30:40');
INSERT INTO `activity_allocationdaily` VALUES (36, 2, 5, '2014-05-30', 77, '2014-04-23 15:30:40');
INSERT INTO `activity_allocationdaily` VALUES (37, 2, 5, '2014-05-31', 77, '2014-04-23 15:30:40');
INSERT INTO `activity_allocationdaily` VALUES (38, 2, 5, '2014-06-01', 77, '2014-04-23 15:30:40');
INSERT INTO `activity_allocationdaily` VALUES (39, 2, 5, '2014-06-02', 22, '2014-04-23 15:30:40');
INSERT INTO `activity_allocationdaily` VALUES (40, 2, 5, '2014-06-03', 77, '2014-04-23 15:30:40');
INSERT INTO `activity_allocationdaily` VALUES (41, 2, 5, '2014-06-04', 44, '2014-04-23 15:30:40');
INSERT INTO `activity_allocationdaily` VALUES (42, 2, 5, '2014-06-05', 77, '2014-04-23 15:30:40');
INSERT INTO `activity_allocationdaily` VALUES (43, 2, 5, '2014-06-06', 77, '2014-04-23 15:30:40');
INSERT INTO `activity_allocationdaily` VALUES (44, 2, 5, '2014-06-07', 77, '2014-04-23 15:30:40');
INSERT INTO `activity_allocationdaily` VALUES (45, 2, 5, '2014-06-08', 88, '2014-04-23 15:30:40');
INSERT INTO `activity_allocationdaily` VALUES (46, 2, 5, '2014-06-09', 77, '2014-04-23 15:30:40');
INSERT INTO `activity_allocationdaily` VALUES (47, 2, 5, '2014-06-10', 77, '2014-04-23 15:30:40');
INSERT INTO `activity_allocationdaily` VALUES (48, 2, 5, '2014-06-11', 77, '2014-04-23 15:30:40');
INSERT INTO `activity_allocationdaily` VALUES (49, 2, 5, '2014-06-12', 77, '2014-04-23 15:30:40');
INSERT INTO `activity_allocationdaily` VALUES (50, 2, 5, '2014-06-13', 77, '2014-04-23 15:30:40');
INSERT INTO `activity_allocationdaily` VALUES (51, 2, 5, '2014-06-14', 77, '2014-04-23 15:30:40');
INSERT INTO `activity_allocationdaily` VALUES (52, 2, 5, '2014-06-15', 77, '2014-04-23 15:30:40');
INSERT INTO `activity_allocationdaily` VALUES (53, 2, 5, '2014-06-16', 77, '2014-04-23 15:30:40');
INSERT INTO `activity_allocationdaily` VALUES (54, 2, 5, '2014-06-17', 77, '2014-04-23 15:30:40');
INSERT INTO `activity_allocationdaily` VALUES (55, 2, 5, '2014-06-18', 77, '2014-04-23 15:30:40');
INSERT INTO `activity_allocationdaily` VALUES (56, 2, 5, '2014-06-19', 77, '2014-04-23 15:30:40');
INSERT INTO `activity_allocationdaily` VALUES (57, 2, 5, '2014-06-20', 77, '2014-04-23 15:30:40');
INSERT INTO `activity_allocationdaily` VALUES (58, 2, 5, '2014-06-21', 77, '2014-04-23 15:30:40');
INSERT INTO `activity_allocationdaily` VALUES (59, 2, 5, '2014-06-22', 77, '2014-04-23 15:30:40');
INSERT INTO `activity_allocationdaily` VALUES (60, 2, 5, '2014-06-23', 77, '2014-04-23 15:30:40');
INSERT INTO `activity_allocationdaily` VALUES (61, 2, 5, '2014-06-24', 77, '2014-04-23 15:30:40');
INSERT INTO `activity_allocationdaily` VALUES (62, 2, 5, '2014-06-25', 77, '2014-04-23 15:30:40');
INSERT INTO `activity_allocationdaily` VALUES (63, 2, 5, '2014-06-26', 77, '2014-04-23 15:30:40');
INSERT INTO `activity_allocationdaily` VALUES (64, 2, 5, '2014-06-27', 77, '2014-04-23 15:30:40');
INSERT INTO `activity_allocationdaily` VALUES (65, 2, 5, '2014-06-28', 77, '2014-04-23 15:30:40');
INSERT INTO `activity_allocationdaily` VALUES (66, 2, 5, '2014-06-29', 77, '2014-04-23 15:30:40');
INSERT INTO `activity_allocationdaily` VALUES (67, 2, 5, '2014-06-30', 77, '2014-04-23 15:30:40');
INSERT INTO `activity_allocationdaily` VALUES (68, 2, 5, '2014-07-01', 77, '2014-04-23 15:30:40');
INSERT INTO `activity_allocationdaily` VALUES (69, 2, 5, '2014-07-02', 22, '2014-04-23 15:30:40');
INSERT INTO `activity_allocationdaily` VALUES (70, 2, 5, '2014-07-03', 77, '2014-04-23 15:30:40');
INSERT INTO `activity_allocationdaily` VALUES (71, 2, 5, '2014-07-04', 44, '2014-04-23 15:30:40');
INSERT INTO `activity_allocationdaily` VALUES (72, 2, 5, '2014-07-05', 77, '2014-04-23 15:30:40');
INSERT INTO `activity_allocationdaily` VALUES (73, 2, 5, '2014-07-06', 77, '2014-04-23 15:30:40');
INSERT INTO `activity_allocationdaily` VALUES (74, 2, 5, '2014-07-07', 77, '2014-04-23 15:30:40');
INSERT INTO `activity_allocationdaily` VALUES (75, 2, 5, '2014-07-08', 88, '2014-04-23 15:30:40');
INSERT INTO `activity_allocationdaily` VALUES (76, 2, 5, '2014-07-09', 77, '2014-04-23 15:30:40');
INSERT INTO `activity_allocationdaily` VALUES (77, 2, 5, '2014-07-10', 77, '2014-04-23 15:30:40');
INSERT INTO `activity_allocationdaily` VALUES (78, 2, 5, '2014-07-11', 77, '2014-04-23 15:30:40');
INSERT INTO `activity_allocationdaily` VALUES (79, 2, 5, '2014-07-12', 77, '2014-04-23 15:30:40');
INSERT INTO `activity_allocationdaily` VALUES (80, 2, 5, '2014-07-13', 77, '2014-04-23 15:30:40');
INSERT INTO `activity_allocationdaily` VALUES (81, 2, 5, '2014-07-14', 77, '2014-04-23 15:30:40');
INSERT INTO `activity_allocationdaily` VALUES (82, 2, 5, '2014-07-15', 77, '2014-04-23 15:30:40');
INSERT INTO `activity_allocationdaily` VALUES (83, 2, 5, '2014-07-16', 77, '2014-04-23 15:30:40');
INSERT INTO `activity_allocationdaily` VALUES (84, 2, 5, '2014-07-17', 77, '2014-04-23 15:30:40');
INSERT INTO `activity_allocationdaily` VALUES (85, 2, 5, '2014-07-18', 77, '2014-04-23 15:30:40');
INSERT INTO `activity_allocationdaily` VALUES (86, 2, 5, '2014-07-19', 77, '2014-04-23 15:30:40');
INSERT INTO `activity_allocationdaily` VALUES (87, 2, 5, '2014-07-20', 77, '2014-04-23 15:30:40');
INSERT INTO `activity_allocationdaily` VALUES (88, 2, 5, '2014-07-21', 77, '2014-04-23 15:30:40');
INSERT INTO `activity_allocationdaily` VALUES (89, 2, 5, '2014-07-22', 77, '2014-04-23 15:30:40');
INSERT INTO `activity_allocationdaily` VALUES (90, 2, 5, '2014-07-23', 77, '2014-04-23 15:30:40');
INSERT INTO `activity_allocationdaily` VALUES (91, 2, 5, '2014-04-24', 240, '2014-05-23 12:04:51');
INSERT INTO `activity_allocationdaily` VALUES (92, 2, 5, '2014-07-24', 777, '2014-04-23 15:31:45');
INSERT INTO `activity_allocationdaily` VALUES (93, 2, 5, '2014-07-25', 777, '2014-04-23 15:31:45');
INSERT INTO `activity_allocationdaily` VALUES (94, 2, 5, '2014-07-26', 777, '2014-04-23 15:31:45');
INSERT INTO `activity_allocationdaily` VALUES (95, 2, 5, '2014-07-27', 777, '2014-04-23 15:31:45');
INSERT INTO `activity_allocationdaily` VALUES (96, 2, 5, '2014-01-23', 0, '2014-05-23 12:04:51');
INSERT INTO `activity_allocationdaily` VALUES (97, 2, 5, '2014-01-24', 0, '2014-05-23 12:04:51');
INSERT INTO `activity_allocationdaily` VALUES (98, 2, 5, '2014-01-25', 0, '2014-05-23 12:04:51');
INSERT INTO `activity_allocationdaily` VALUES (99, 2, 5, '2014-01-26', 0, '2014-05-23 12:04:51');
INSERT INTO `activity_allocationdaily` VALUES (100, 2, 5, '2014-01-27', 0, '2014-05-23 12:04:51');
INSERT INTO `activity_allocationdaily` VALUES (101, 2, 5, '2014-01-28', 0, '2014-05-23 12:04:51');
INSERT INTO `activity_allocationdaily` VALUES (102, 2, 5, '2014-01-29', 0, '2014-05-23 12:04:51');
INSERT INTO `activity_allocationdaily` VALUES (103, 2, 5, '2014-01-30', 0, '2014-05-23 12:04:51');
INSERT INTO `activity_allocationdaily` VALUES (104, 2, 5, '2014-01-31', 0, '2014-05-23 12:04:51');
INSERT INTO `activity_allocationdaily` VALUES (105, 2, 5, '2014-02-01', 0, '2014-05-23 12:04:51');
INSERT INTO `activity_allocationdaily` VALUES (106, 2, 5, '2014-02-02', 0, '2014-05-23 12:04:51');
INSERT INTO `activity_allocationdaily` VALUES (107, 2, 5, '2014-02-03', 0, '2014-05-23 12:04:51');
INSERT INTO `activity_allocationdaily` VALUES (108, 2, 5, '2014-02-04', 0, '2014-05-23 12:04:51');
INSERT INTO `activity_allocationdaily` VALUES (109, 2, 5, '2014-02-05', 0, '2014-05-23 12:04:51');
INSERT INTO `activity_allocationdaily` VALUES (110, 2, 5, '2014-02-06', 0, '2014-05-23 12:04:51');
INSERT INTO `activity_allocationdaily` VALUES (111, 2, 5, '2014-02-07', 0, '2014-05-23 12:04:51');
INSERT INTO `activity_allocationdaily` VALUES (112, 2, 5, '2014-02-08', 0, '2014-05-23 12:04:51');
INSERT INTO `activity_allocationdaily` VALUES (113, 2, 5, '2014-02-09', 0, '2014-05-23 12:04:51');
INSERT INTO `activity_allocationdaily` VALUES (114, 2, 5, '2014-02-10', 0, '2014-05-23 12:04:51');
INSERT INTO `activity_allocationdaily` VALUES (115, 2, 5, '2014-02-11', 0, '2014-05-23 12:04:51');
INSERT INTO `activity_allocationdaily` VALUES (116, 2, 5, '2014-02-12', 0, '2014-05-23 12:04:51');
INSERT INTO `activity_allocationdaily` VALUES (117, 2, 5, '2014-02-13', 0, '2014-05-23 12:04:51');
INSERT INTO `activity_allocationdaily` VALUES (118, 2, 5, '2014-02-14', 0, '2014-05-23 12:04:51');
INSERT INTO `activity_allocationdaily` VALUES (119, 2, 5, '2014-02-15', 0, '2014-05-23 12:04:51');
INSERT INTO `activity_allocationdaily` VALUES (120, 2, 5, '2014-02-16', 0, '2014-05-23 12:04:51');
INSERT INTO `activity_allocationdaily` VALUES (121, 2, 5, '2014-02-17', 0, '2014-05-23 12:04:51');
INSERT INTO `activity_allocationdaily` VALUES (122, 2, 5, '2014-02-18', 0, '2014-05-23 12:04:51');
INSERT INTO `activity_allocationdaily` VALUES (123, 2, 5, '2014-02-19', 0, '2014-05-23 12:04:51');
INSERT INTO `activity_allocationdaily` VALUES (124, 2, 5, '2014-02-20', 0, '2014-05-23 12:04:51');
INSERT INTO `activity_allocationdaily` VALUES (125, 2, 5, '2014-02-21', 0, '2014-05-23 12:04:51');
INSERT INTO `activity_allocationdaily` VALUES (126, 2, 5, '2014-02-22', 0, '2014-05-23 12:04:51');
INSERT INTO `activity_allocationdaily` VALUES (127, 2, 5, '2014-02-23', 0, '2014-05-23 12:04:51');
INSERT INTO `activity_allocationdaily` VALUES (128, 2, 5, '2014-02-24', 0, '2014-05-23 12:04:51');
INSERT INTO `activity_allocationdaily` VALUES (129, 2, 5, '2014-02-25', 0, '2014-05-23 12:04:51');
INSERT INTO `activity_allocationdaily` VALUES (130, 2, 5, '2014-02-26', 0, '2014-05-23 12:04:51');
INSERT INTO `activity_allocationdaily` VALUES (131, 2, 5, '2014-02-27', 0, '2014-05-23 12:04:51');
INSERT INTO `activity_allocationdaily` VALUES (132, 2, 5, '2014-02-28', 0, '2014-05-23 12:04:51');
INSERT INTO `activity_allocationdaily` VALUES (133, 2, 5, '2014-03-01', 0, '2014-05-23 12:04:51');
INSERT INTO `activity_allocationdaily` VALUES (134, 2, 5, '2014-03-02', 0, '2014-05-23 12:04:51');
INSERT INTO `activity_allocationdaily` VALUES (135, 2, 5, '2014-03-03', 0, '2014-05-23 12:04:51');
INSERT INTO `activity_allocationdaily` VALUES (136, 2, 5, '2014-03-04', 0, '2014-05-23 12:04:51');
INSERT INTO `activity_allocationdaily` VALUES (137, 2, 5, '2014-03-05', 0, '2014-05-23 12:04:51');
INSERT INTO `activity_allocationdaily` VALUES (138, 2, 5, '2014-03-06', 0, '2014-05-23 12:04:51');
INSERT INTO `activity_allocationdaily` VALUES (139, 2, 5, '2014-03-07', 0, '2014-05-23 12:04:51');
INSERT INTO `activity_allocationdaily` VALUES (140, 2, 5, '2014-03-08', 0, '2014-05-23 12:04:51');
INSERT INTO `activity_allocationdaily` VALUES (141, 2, 5, '2014-03-09', 0, '2014-05-23 12:04:51');
INSERT INTO `activity_allocationdaily` VALUES (142, 2, 5, '2014-03-10', 0, '2014-05-23 12:04:51');
INSERT INTO `activity_allocationdaily` VALUES (143, 2, 5, '2014-03-11', 0, '2014-05-23 12:04:51');
INSERT INTO `activity_allocationdaily` VALUES (144, 2, 5, '2014-03-12', 0, '2014-05-23 12:04:51');
INSERT INTO `activity_allocationdaily` VALUES (145, 2, 5, '2014-03-13', 0, '2014-05-23 12:04:51');
INSERT INTO `activity_allocationdaily` VALUES (146, 2, 5, '2014-03-14', 0, '2014-05-23 12:04:51');
INSERT INTO `activity_allocationdaily` VALUES (147, 2, 5, '2014-03-15', 0, '2014-05-23 12:04:51');
INSERT INTO `activity_allocationdaily` VALUES (148, 2, 5, '2014-03-16', 0, '2014-05-23 12:04:51');
INSERT INTO `activity_allocationdaily` VALUES (149, 2, 5, '2014-03-17', 0, '2014-05-23 12:04:51');
INSERT INTO `activity_allocationdaily` VALUES (150, 2, 5, '2014-03-18', 0, '2014-05-23 12:04:51');
INSERT INTO `activity_allocationdaily` VALUES (151, 2, 5, '2014-03-19', 0, '2014-05-23 12:04:51');
INSERT INTO `activity_allocationdaily` VALUES (152, 2, 5, '2014-03-20', 0, '2014-05-23 12:04:51');
INSERT INTO `activity_allocationdaily` VALUES (153, 2, 5, '2014-03-21', 0, '2014-05-23 12:04:51');
INSERT INTO `activity_allocationdaily` VALUES (154, 2, 5, '2014-03-22', 0, '2014-05-23 12:04:51');
INSERT INTO `activity_allocationdaily` VALUES (155, 2, 5, '2014-03-23', 0, '2014-05-23 12:04:51');
INSERT INTO `activity_allocationdaily` VALUES (156, 2, 5, '2014-03-24', 0, '2014-05-23 12:04:51');
INSERT INTO `activity_allocationdaily` VALUES (157, 2, 5, '2014-03-25', 0, '2014-05-23 12:04:51');
INSERT INTO `activity_allocationdaily` VALUES (158, 2, 5, '2014-03-26', 0, '2014-05-23 12:04:51');
INSERT INTO `activity_allocationdaily` VALUES (159, 2, 5, '2014-03-27', 0, '2014-05-23 12:04:51');
INSERT INTO `activity_allocationdaily` VALUES (160, 2, 5, '2014-03-28', 0, '2014-05-23 12:04:51');
INSERT INTO `activity_allocationdaily` VALUES (161, 2, 5, '2014-03-29', 0, '2014-05-23 12:04:51');
INSERT INTO `activity_allocationdaily` VALUES (162, 2, 5, '2014-03-30', 0, '2014-05-23 12:04:51');
INSERT INTO `activity_allocationdaily` VALUES (163, 2, 5, '2014-03-31', 0, '2014-05-23 12:04:51');
INSERT INTO `activity_allocationdaily` VALUES (164, 2, 5, '2014-04-01', 0, '2014-05-23 12:04:51');
INSERT INTO `activity_allocationdaily` VALUES (165, 2, 5, '2014-04-02', 0, '2014-05-23 12:04:51');
INSERT INTO `activity_allocationdaily` VALUES (166, 2, 5, '2014-04-03', 0, '2014-05-23 12:04:51');
INSERT INTO `activity_allocationdaily` VALUES (167, 2, 5, '2014-04-04', 0, '2014-05-23 12:04:51');
INSERT INTO `activity_allocationdaily` VALUES (168, 2, 5, '2014-04-05', 0, '2014-05-23 12:04:51');
INSERT INTO `activity_allocationdaily` VALUES (169, 2, 5, '2014-04-06', 0, '2014-05-23 12:04:51');
INSERT INTO `activity_allocationdaily` VALUES (170, 2, 5, '2014-04-07', 0, '2014-05-23 12:04:51');
INSERT INTO `activity_allocationdaily` VALUES (171, 2, 5, '2014-04-08', 0, '2014-05-23 12:04:51');
INSERT INTO `activity_allocationdaily` VALUES (172, 2, 5, '2014-04-09', 0, '2014-05-23 12:04:51');
INSERT INTO `activity_allocationdaily` VALUES (173, 2, 5, '2014-04-10', 0, '2014-05-23 12:04:51');
INSERT INTO `activity_allocationdaily` VALUES (174, 2, 5, '2014-04-11', 0, '2014-05-23 12:04:51');
INSERT INTO `activity_allocationdaily` VALUES (175, 2, 5, '2014-04-12', 0, '2014-05-23 12:04:51');
INSERT INTO `activity_allocationdaily` VALUES (176, 2, 5, '2014-04-13', 0, '2014-05-23 12:04:51');
INSERT INTO `activity_allocationdaily` VALUES (177, 2, 5, '2014-04-14', 0, '2014-05-23 12:04:51');
INSERT INTO `activity_allocationdaily` VALUES (178, 2, 5, '2014-04-15', 0, '2014-05-23 12:04:51');
INSERT INTO `activity_allocationdaily` VALUES (179, 2, 5, '2014-04-16', 0, '2014-05-23 12:04:51');
INSERT INTO `activity_allocationdaily` VALUES (180, 2, 5, '2014-04-17', 0, '2014-05-23 12:04:51');
INSERT INTO `activity_allocationdaily` VALUES (181, 2, 5, '2014-04-18', 0, '2014-05-23 12:04:51');
INSERT INTO `activity_allocationdaily` VALUES (182, 2, 5, '2014-04-19', 0, '2014-05-23 12:04:51');
INSERT INTO `activity_allocationdaily` VALUES (183, 2, 5, '2014-04-20', 0, '2014-05-23 12:04:51');
INSERT INTO `activity_allocationdaily` VALUES (184, 2, 5, '2014-04-21', 0, '2014-05-23 12:04:51');
INSERT INTO `activity_allocationdaily` VALUES (185, 2, 5, '2014-04-22', 0, '2014-05-23 12:04:51');
INSERT INTO `activity_allocationdaily` VALUES (186, 2, 5, '2014-04-23', 0, '2014-05-23 12:04:51');

-- --------------------------------------------------------

-- 
-- Table structure for table `activity_con_area`
-- 

CREATE TABLE `activity_con_area` (
  `con_id` int(3) unsigned zerofill NOT NULL auto_increment,
  `con_name` varchar(200) collate utf8_unicode_ci NOT NULL,
  `photo1` varchar(100) collate utf8_unicode_ci NOT NULL,
  `last_edit_time` datetime NOT NULL,
  `province_id` int(11) NOT NULL,
  PRIMARY KEY  (`con_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=10 ;

-- 
-- Dumping data for table `activity_con_area`
-- 

INSERT INTO `activity_con_area` VALUES (001, 'Area01', '', '2014-04-03 16:55:10', 8);
INSERT INTO `activity_con_area` VALUES (002, 'Area02', '', '2014-04-03 16:25:12', 2);
INSERT INTO `activity_con_area` VALUES (003, 'Area03', '', '2014-04-03 16:25:20', 2);
INSERT INTO `activity_con_area` VALUES (007, 'Kata', '', '2014-04-03 16:50:49', 1);
INSERT INTO `activity_con_area` VALUES (008, 'Patong', '', '2014-04-03 16:50:45', 1);
INSERT INTO `activity_con_area` VALUES (009, 'Area07', '', '2014-04-22 10:57:53', 9);

-- --------------------------------------------------------

-- 
-- Table structure for table `activity_con_category`
-- 

CREATE TABLE `activity_con_category` (
  `con_id` int(3) unsigned zerofill NOT NULL auto_increment,
  `con_name` varchar(200) collate utf8_unicode_ci NOT NULL,
  `photo1` varchar(100) collate utf8_unicode_ci NOT NULL,
  `last_edit_time` datetime NOT NULL,
  PRIMARY KEY  (`con_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=11 ;

-- 
-- Dumping data for table `activity_con_category`
-- 

INSERT INTO `activity_con_category` VALUES (008, 'Show & Entertainment', '', '2014-04-22 11:25:32');
INSERT INTO `activity_con_category` VALUES (009, 'Fishing Tour', '', '2014-04-22 11:26:08');

-- --------------------------------------------------------

-- 
-- Table structure for table `activity_con_country`
-- 

CREATE TABLE `activity_con_country` (
  `con_id` int(3) unsigned zerofill NOT NULL auto_increment,
  `con_name` varchar(200) collate utf8_unicode_ci NOT NULL,
  `photo1` varchar(100) collate utf8_unicode_ci NOT NULL,
  `last_edit_time` datetime NOT NULL,
  PRIMARY KEY  (`con_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=12 ;

-- 
-- Dumping data for table `activity_con_country`
-- 

INSERT INTO `activity_con_country` VALUES (001, 'Thailand', '', '2014-04-03 15:58:12');
INSERT INTO `activity_con_country` VALUES (002, 'Malaysia', '', '2014-04-03 15:55:01');
INSERT INTO `activity_con_country` VALUES (010, 'Indonesia', '', '2014-04-03 16:31:06');
INSERT INTO `activity_con_country` VALUES (011, 'Vietnam', '', '2014-04-22 10:35:14');

-- --------------------------------------------------------

-- 
-- Table structure for table `activity_con_province`
-- 

CREATE TABLE `activity_con_province` (
  `con_id` int(3) unsigned zerofill NOT NULL auto_increment,
  `con_name` varchar(200) collate utf8_unicode_ci NOT NULL,
  `photo1` varchar(100) collate utf8_unicode_ci NOT NULL,
  `last_edit_time` datetime NOT NULL,
  `country_id` int(11) NOT NULL,
  PRIMARY KEY  (`con_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=10 ;

-- 
-- Dumping data for table `activity_con_province`
-- 

INSERT INTO `activity_con_province` VALUES (001, 'Phuket', '', '2014-04-03 16:31:43', 1);
INSERT INTO `activity_con_province` VALUES (002, 'Krabi', '', '2014-04-03 16:16:21', 1);
INSERT INTO `activity_con_province` VALUES (008, 'Kuala Lumpur', '', '2014-04-03 16:36:31', 2);
INSERT INTO `activity_con_province` VALUES (007, 'Jakarta', '', '2014-04-03 16:35:53', 10);
INSERT INTO `activity_con_province` VALUES (009, 'Hanoi', '', '2014-04-22 10:43:21', 11);

-- --------------------------------------------------------

-- 
-- Table structure for table `activity_period`
-- 

CREATE TABLE `activity_period` (
  `actpe_id` int(11) NOT NULL auto_increment,
  `activities_id` int(11) NOT NULL,
  `actpe_name` varchar(200) collate utf8_unicode_ci NOT NULL,
  `actpe_datefrom` date NOT NULL,
  `actpe_dateto` date NOT NULL,
  `arrange` int(11) NOT NULL,
  `last_edit_time` datetime NOT NULL,
  PRIMARY KEY  (`actpe_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=8 ;

-- 
-- Dumping data for table `activity_period`
-- 

INSERT INTO `activity_period` VALUES (1, 1, 'Period 2014', '2014-01-01', '2014-12-31', 0, '2014-04-22 15:51:25');
INSERT INTO `activity_period` VALUES (2, 1, 'Period 2015', '2015-01-01', '2015-12-31', 0, '2014-04-22 15:52:25');
INSERT INTO `activity_period` VALUES (7, 2, 'Period 2014 / 2', '2014-07-01', '2014-12-31', 0, '2014-04-23 14:23:09');
INSERT INTO `activity_period` VALUES (6, 2, 'Period 2014 / 1', '2014-01-01', '2014-06-30', 0, '2014-04-23 14:23:24');

-- --------------------------------------------------------

-- 
-- Table structure for table `activity_price_type`
-- 

CREATE TABLE `activity_price_type` (
  `a_id` int(11) NOT NULL auto_increment,
  `a_name` varchar(200) collate utf8_unicode_ci NOT NULL,
  `last_edit_time` datetime NOT NULL,
  `photo1` varchar(100) collate utf8_unicode_ci NOT NULL,
  PRIMARY KEY  (`a_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

-- 
-- Dumping data for table `activity_price_type`
-- 

INSERT INTO `activity_price_type` VALUES (1, 'แบบกำหนดเปอร์เซ็นต์ส่วนลด', '2014-04-03 15:20:41', '');
INSERT INTO `activity_price_type` VALUES (2, 'แบบกำหนดราคาเอง', '2014-04-03 15:20:45', '');

-- --------------------------------------------------------

-- 
-- Table structure for table `activity_ratepercents`
-- 

CREATE TABLE `activity_ratepercents` (
  `actrateper_id` int(11) NOT NULL auto_increment,
  `activities_id` int(11) NOT NULL,
  `agentgrade_id` int(11) NOT NULL,
  `actrateper_discount` int(3) NOT NULL,
  `last_edit_time` datetime NOT NULL,
  PRIMARY KEY  (`actrateper_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

-- 
-- Dumping data for table `activity_ratepercents`
-- 

INSERT INTO `activity_ratepercents` VALUES (1, 2, 1, 90, '2014-04-23 14:40:43');
INSERT INTO `activity_ratepercents` VALUES (2, 2, 2, 80, '2014-04-23 14:40:43');
INSERT INTO `activity_ratepercents` VALUES (3, 2, 3, 70, '2014-04-23 14:40:43');
INSERT INTO `activity_ratepercents` VALUES (4, 2, 4, 50, '2014-04-23 14:40:43');
INSERT INTO `activity_ratepercents` VALUES (5, 2, 5, 40, '2014-04-23 14:40:43');

-- --------------------------------------------------------

-- 
-- Table structure for table `activity_rates`
-- 

CREATE TABLE `activity_rates` (
  `actrate_id` int(11) NOT NULL auto_increment,
  `activities_id` int(11) NOT NULL,
  `activityratetype_id` int(11) NOT NULL,
  `activityperiod_id` int(11) NOT NULL,
  `agentgrade_id` int(11) NOT NULL,
  `rate_1` double(15,2) NOT NULL,
  `rate_2` double(15,2) NOT NULL,
  `rate_3` double(15,2) NOT NULL,
  `rate_4` double(15,2) NOT NULL,
  `rate_5` double(15,2) NOT NULL,
  `last_edit_time` datetime NOT NULL,
  PRIMARY KEY  (`actrate_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=9 ;

-- 
-- Dumping data for table `activity_rates`
-- 

INSERT INTO `activity_rates` VALUES (1, 1, 1, 1, 0, 9103.00, 9102.00, 9101.00, 0.00, 0.00, '2014-04-23 11:56:16');
INSERT INTO `activity_rates` VALUES (2, 1, 1, 2, 0, 9203.00, 0.00, 9201.00, 0.00, 0.00, '2014-04-23 11:56:16');
INSERT INTO `activity_rates` VALUES (3, 1, 2, 1, 0, 19103.00, 0.00, 19101.00, 0.00, 0.00, '2014-04-23 11:56:16');
INSERT INTO `activity_rates` VALUES (4, 1, 2, 2, 0, 19203.00, 0.00, 19201.00, 0.00, 0.00, '2014-04-23 11:56:16');
INSERT INTO `activity_rates` VALUES (5, 1, 1, 1, 1, 7103.00, 7102.00, 7101.00, 0.00, 0.00, '2014-04-23 11:58:21');
INSERT INTO `activity_rates` VALUES (6, 1, 1, 2, 1, 7203.00, 7202.00, 0.00, 0.00, 0.00, '2014-04-23 11:58:21');
INSERT INTO `activity_rates` VALUES (7, 1, 2, 1, 1, 17103.00, 17102.00, 17101.00, 0.00, 0.00, '2014-04-23 11:58:21');
INSERT INTO `activity_rates` VALUES (8, 1, 2, 2, 1, 17203.00, 17202.00, 17201.00, 0.00, 0.00, '2014-04-23 11:58:21');

-- --------------------------------------------------------

-- 
-- Table structure for table `activity_ratetypes`
-- 

CREATE TABLE `activity_ratetypes` (
  `actrt_id` int(11) NOT NULL auto_increment,
  `activities_id` int(11) NOT NULL,
  `actrt_name` varchar(200) collate utf8_unicode_ci NOT NULL,
  `photo1` varchar(100) collate utf8_unicode_ci NOT NULL,
  `actrt_detail` text collate utf8_unicode_ci NOT NULL,
  `last_edit_time` datetime NOT NULL,
  PRIMARY KEY  (`actrt_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

-- 
-- Dumping data for table `activity_ratetypes`
-- 

INSERT INTO `activity_ratetypes` VALUES (1, 1, 'Normal Seat', '', '', '2014-04-22 15:29:15');
INSERT INTO `activity_ratetypes` VALUES (2, 1, 'VIP Seat', '', '', '2014-04-22 15:31:15');
INSERT INTO `activity_ratetypes` VALUES (5, 2, 'Fishing Game', '', '', '2014-04-23 14:21:10');
INSERT INTO `activity_ratetypes` VALUES (6, 2, 'Fishing Game + Rod', '', '', '2014-04-23 14:22:06');

-- --------------------------------------------------------

-- 
-- Table structure for table `agents`
-- 

CREATE TABLE `agents` (
  `ag_id` int(5) unsigned zerofill NOT NULL auto_increment,
  `ag_ref` varchar(100) collate utf8_unicode_ci NOT NULL,
  `ag_username` varchar(100) collate utf8_unicode_ci NOT NULL,
  `ag_password` varchar(100) collate utf8_unicode_ci NOT NULL,
  `ag_name` varchar(200) collate utf8_unicode_ci NOT NULL,
  `ag_tel` varchar(100) collate utf8_unicode_ci NOT NULL,
  `ag_address` varchar(200) collate utf8_unicode_ci NOT NULL,
  `ag_contact_name` varchar(200) collate utf8_unicode_ci NOT NULL,
  `ag_contact_tel` varchar(100) collate utf8_unicode_ci NOT NULL,
  `ag_account_name` varchar(200) collate utf8_unicode_ci NOT NULL,
  `ag_account_tel` varchar(100) collate utf8_unicode_ci NOT NULL,
  `ag_reser_name` varchar(200) collate utf8_unicode_ci NOT NULL,
  `ag_reser_tel` varchar(100) collate utf8_unicode_ci NOT NULL,
  `ag_note` text collate utf8_unicode_ci NOT NULL,
  `agentfor_id` int(11) NOT NULL,
  `agentrelation_id` int(11) NOT NULL,
  `agenttype_id` int(11) NOT NULL,
  `agentgrade_id_tour` int(11) NOT NULL,
  `agentgrade_id_hotel` int(11) NOT NULL,
  `agentgrade_id_boat` int(11) NOT NULL,
  `agentgrade_id_car` int(11) NOT NULL,
  `agentgrade_id_train` int(11) NOT NULL,
  `agentgrade_id_activity` int(11) NOT NULL,
  `agentgrade_id_package` int(11) NOT NULL,
  `agentpaytype_id` int(11) NOT NULL,
  `agentcredittype_id` int(11) NOT NULL,
  `ag_balance_credit` double(15,2) NOT NULL,
  `ag_min_credit` double(15,2) NOT NULL,
  `last_edit_time` datetime NOT NULL,
  `member_id_last_edit` int(11) NOT NULL,
  `ag_email` varchar(100) collate utf8_unicode_ci NOT NULL,
  `ag_address2` text collate utf8_unicode_ci NOT NULL,
  `country_id` int(11) NOT NULL,
  `province_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `ag_zipcode` varchar(100) collate utf8_unicode_ci NOT NULL,
  `ag_tel2` varchar(100) collate utf8_unicode_ci NOT NULL,
  `ag_tel3` varchar(100) collate utf8_unicode_ci NOT NULL,
  `ag_fax` varchar(100) collate utf8_unicode_ci NOT NULL,
  `ag_email2` varchar(100) collate utf8_unicode_ci NOT NULL,
  `ag_licen_num` varchar(200) collate utf8_unicode_ci NOT NULL,
  `file1` varchar(100) collate utf8_unicode_ci NOT NULL,
  `file2` varchar(100) collate utf8_unicode_ci NOT NULL,
  `file3` varchar(100) collate utf8_unicode_ci NOT NULL,
  `file4` varchar(100) collate utf8_unicode_ci NOT NULL,
  `file5` varchar(100) collate utf8_unicode_ci NOT NULL,
  `ag_url1` varchar(100) collate utf8_unicode_ci NOT NULL,
  `ag_url2` varchar(100) collate utf8_unicode_ci NOT NULL,
  `ag_combirth_date` date NOT NULL,
  `photo1` varchar(100) collate utf8_unicode_ci NOT NULL,
  `photo2` varchar(100) collate utf8_unicode_ci NOT NULL,
  `photo3` varchar(100) collate utf8_unicode_ci NOT NULL,
  `photo4` varchar(100) collate utf8_unicode_ci NOT NULL,
  `photo5` varchar(100) collate utf8_unicode_ci NOT NULL,
  `ag_remarkinvoice` text collate utf8_unicode_ci NOT NULL,
  `ag_invoice_vat` int(1) NOT NULL,
  `ag_contactstart_date` date NOT NULL,
  `businesstype_id` int(11) NOT NULL,
  `ag_line` varchar(200) collate utf8_unicode_ci NOT NULL,
  `ag_skype` varchar(200) collate utf8_unicode_ci NOT NULL,
  `agenttypeofclient_id` int(11) NOT NULL,
  `ag_channel` varchar(200) collate utf8_unicode_ci NOT NULL,
  `ag_region1` varchar(200) collate utf8_unicode_ci NOT NULL,
  `ag_region2` varchar(200) collate utf8_unicode_ci NOT NULL,
  `ag_targetcountry1` varchar(200) collate utf8_unicode_ci NOT NULL,
  `ag_targetcountry2` varchar(200) collate utf8_unicode_ci NOT NULL,
  `ag_targetcountry3` varchar(200) collate utf8_unicode_ci NOT NULL,
  `ag_seasonality1` varchar(200) collate utf8_unicode_ci NOT NULL,
  `ag_seasonality2` varchar(200) collate utf8_unicode_ci NOT NULL,
  `ag_seasonality3` varchar(200) collate utf8_unicode_ci NOT NULL,
  `ag_competitor1` varchar(200) collate utf8_unicode_ci NOT NULL,
  `ag_competitor2` varchar(200) collate utf8_unicode_ci NOT NULL,
  `ag_competitor3` varchar(200) collate utf8_unicode_ci NOT NULL,
  `ag_paymentterm_num` int(3) NOT NULL,
  `ag_remark2` text collate utf8_unicode_ci NOT NULL,
  `ag_contact_title_id` int(11) NOT NULL,
  `ag_contact_lname` varchar(200) collate utf8_unicode_ci NOT NULL,
  `ag_account_title_id` int(11) NOT NULL,
  `ag_account_lname` varchar(200) collate utf8_unicode_ci NOT NULL,
  `ag_reser_title_id` int(11) NOT NULL,
  `ag_reser_lname` varchar(200) collate utf8_unicode_ci NOT NULL,
  `ag_contact_email` varchar(100) collate utf8_unicode_ci NOT NULL,
  `ag_account_email` varchar(100) collate utf8_unicode_ci NOT NULL,
  `ag_reser_email` varchar(100) collate utf8_unicode_ci NOT NULL,
  `ag_howtobuycredit` varchar(200) collate utf8_unicode_ci NOT NULL,
  `ag_create_date` date NOT NULL,
  PRIMARY KEY  (`ag_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=20 ;

-- 
-- Dumping data for table `agents`
-- 

INSERT INTO `agents` VALUES (00001, 'TA001', 'TA001001', 'asdas7', 'vava', '025555555', '12/2', 'TF_contact', '025555556', 'TF_account', '025555557', 'TF_Reservation ', '025555558', 'ทดสอบเพิ่ม Agent 01', 2, 2, 2, 5, 4, 3, 2, 1, 5, 0, 2, 1, 10000.00, 2000.00, '2014-03-28 11:03:02', 3, 'yajok.jojo@gmail.com', '', 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00', '', '', '', '', '', '', 0, '0000-00-00', 0, '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', 0, '', 0, '', '', '', '', '', '0000-00-00');
INSERT INTO `agents` VALUES (00007, 'TA003', 'TA003007', '83386', 'vava3', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 1, 10000.00, 1000.00, '2014-03-28 14:00:43', 3, 'yajok.jojo@gmail.com3', '', 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00', '', '', '', '', '', '', 0, '0000-00-00', 0, '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', 0, '', 0, '', '', '', '', '', '0000-00-00');
INSERT INTO `agents` VALUES (00004, 'TA002', 'TA002004', 'e19e9', 'vava2', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, '2014-03-28 13:56:39', 3, 'yajok.jojo@gmail.com2', '', 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00', '', '', '', '', '', '', 0, '0000-00-00', 0, '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', 0, '', 0, '', '', '', '', '', '0000-00-00');
INSERT INTO `agents` VALUES (00011, 'TA004', 'TA004011', '902f368c', 'vava4', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2, 10000.00, 1000.00, '2014-03-28 13:59:16', 3, 'yajok.jojo@gmail.com4', '', 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00', '', '', '', '', '', '', 0, '0000-00-00', 0, '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', 0, '', 0, '', '', '', '', '', '0000-00-00');
INSERT INTO `agents` VALUES (00012, 'SP001', 'SP001012', 'a5dcd8fb', 'AgentSP01 ', '02555555', '', '', '', '', '', '', '', '', 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, '2014-05-23 15:41:06', 3, 'AgentSP01@mail.com', '', 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '2014-05-23', '', '', '', '', '', '', 0, '2014-05-23', 0, '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', 0, '', 0, '', '', '', '', '', '2014-05-02');
INSERT INTO `agents` VALUES (00013, 'SP002', 'SP002013', 'a5d258fd', 'AgentSP02', '', '', '', '', '', '', '', '', '', 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.00, 0.00, '2014-04-09 16:19:08', 1, 'AgentSP02@mail.com', '', 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00', '', '', '', '', '', '', 0, '0000-00-00', 0, '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', 0, '', 0, '', '', '', '', '', '0000-00-00');
INSERT INTO `agents` VALUES (00014, 'HKT-PT-0001', 'hktpt0001014', '095cc952', 'PATONG TEST TOUR', '66 76 232 561', 'need separate \r\n- address 1\r\n- City / District\r\n- State / Province\r\n- Country\r\n- Zip Code', 'Testee', '565656', 'Jee', '11121323', 'Nee', '8569584', 'jhihguhiojokpl;', 1, 1, 0, 1, 2, 0, 3, 4, 5, 0, 2, 1, 30000.00, 10000.00, '2014-04-29 15:27:29', 1, 'yajok.jojo@gmail.com5', '', 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00', '', '', '', '', '', '', 0, '0000-00-00', 0, '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', 0, '', 0, '', '', '', '', '', '0000-00-00');
INSERT INTO `agents` VALUES (00015, 'AG009', '', '306950df', 'VAVA9', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0.00, 0.00, '2014-04-29 15:30:41', 1, 'VAVA9@mail.com', '', 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00', '', '', '', '', '', '', 0, '0000-00-00', 0, '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', 0, '', 0, '', '', '', '', '', '0000-00-00');
INSERT INTO `agents` VALUES (00016, 'AG0000e16', '', '55f80a8a', 'TFname16', '0255555516', 'test ที่อยู่ Agent 1 16', 'vava16', '02555555616', 'ฝ่ายบัญชี 16', '0555656516', 'TF_Reservation16', '066665656516', 'บันทึกภายใน 1 16', 2, 2, 0, 5, 4, 3, 3, 4, 5, 0, 2, 2, 10000.00, 1000.00, '2014-04-30 16:56:10', 3, '16@mail.com', 'test ที่อยู่ Agent 2 16', 13, 12, 11, '161616', '02555555616', '055555555316', '0289595959916', 'email2_16@mail.com', '01122316', '1398851368_1.jpg', '', '', '', '', 'http://phuketsolution.com/', 'http://phuketsolution.com/clients.html', '2012-01-30', '1398851182_1.jpg', '1398851182_2.jpg', '', '', '', 'Remark For Invoice 16', 1, '2013-12-13', 12, 'Line 16', 'Skype 16', 4, 'Channel 16', 'Region 1 16', 'Region 216', 'Target Country 1 16', 'Target Country 2 16', 'Target Country 3 :16', 'Seasonality 1 : 	16', 'Seasonality 216', 'Seasonality 316', 'Competitor 116', 'Competitor 116', 'Competitor 116', 15, 'บันทึกภายใน 2 16', 1, 'vavavava16', 3, 'สกุล ฝ่ายบัญชี 16', 2, 'TL_Reservation16', 'yajok16conta@mail.com', 'yajok16acc@mail.com', 'Reservation@mail16.com', '-', '0000-00-00');
INSERT INTO `agents` VALUES (00017, 'W-HKTTW-00001', '', '22045996', 'LET''S GO PHUKET', '62626522', '132356 werjosifdjasf', 'jack', '', '', '', '', '', '', 1, 2, 0, 3, 3, 4, 2, 2, 1, 0, 1, 0, 0.00, 0.00, '2014-06-14 15:45:20', 3, 'info@andamanwavemaster.com', '', 1, 1, 12, '83000', '8971512154', '15665263', '59523265', '', '44-1111', '1402735519_1.png', '', '', '', '', '', '', '2010-06-14', '1402735519_1.jpg', '', '', '', '', '', 1, '2014-06-14', 11, '', '', 3, 'walk-in', 'world wide', '', '', '', '', '1 nov - 30 apr', '', '', '', '', '', 0, '', 1, 'smith', 0, '', 0, '', '', '', '', '', '2014-06-14');
INSERT INTO `agents` VALUES (00018, 'HKT-PT-0002', '', '8f95e20f', 'TEST 123 TOUR', '', '', '', '', '', '', '', '', '', 1, 0, 0, 1, 1, 1, 1, 1, 1, 0, 1, 0, 0.00, 0.00, '2014-07-17 11:37:05', 3, '456@GMAIL.COM', '', 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '2014-07-17', '', '', '', '', '', '', 0, '2014-07-17', 0, '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', 0, '', 0, '', '', '', '', '', '2014-07-17');
INSERT INTO `agents` VALUES (00019, 'AGT-OVS-001', '', '569f7b1c', 'AVOCA TRAVEL', '', '', '', '', '', '', '', '', '', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 1, 50000.00, 0.00, '2014-07-26 15:57:44', 3, 'noor@avocatravels.co.za', '', 14, 0, 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '2014-07-26', '', '', '', '', '', '', 0, '2014-07-26', 0, '', '', 3, '', '', '', '', '', '', '', '', '', '', '', '', 0, '', 0, '', 0, '', 0, '', '', '', '', 'Bank Transfer', '2014-07-26');

-- --------------------------------------------------------

-- 
-- Table structure for table `agent_con_businesstype`
-- 

CREATE TABLE `agent_con_businesstype` (
  `con_id` int(3) unsigned zerofill NOT NULL auto_increment,
  `con_name` varchar(200) collate utf8_unicode_ci NOT NULL,
  `photo1` varchar(100) collate utf8_unicode_ci NOT NULL,
  `last_edit_time` datetime NOT NULL,
  PRIMARY KEY  (`con_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=21 ;

-- 
-- Dumping data for table `agent_con_businesstype`
-- 

INSERT INTO `agent_con_businesstype` VALUES (011, 'Tour Counter', '', '2014-07-26 15:49:41');
INSERT INTO `agent_con_businesstype` VALUES (012, 'Hotel', '', '2014-07-26 15:49:04');
INSERT INTO `agent_con_businesstype` VALUES (013, 'Tour Operator/ Grand Handling', '', '2014-07-26 15:48:45');
INSERT INTO `agent_con_businesstype` VALUES (014, 'ONLINE', '', '2014-07-26 15:46:45');
INSERT INTO `agent_con_businesstype` VALUES (015, 'DMC', '', '2014-07-26 15:48:25');
INSERT INTO `agent_con_businesstype` VALUES (016, 'Scuba Diving', '', '2014-07-26 15:49:15');
INSERT INTO `agent_con_businesstype` VALUES (017, 'Spa / Massage', '', '2014-07-26 15:49:27');
INSERT INTO `agent_con_businesstype` VALUES (018, 'Restaurant / Cafe', '', '2014-07-26 15:50:29');
INSERT INTO `agent_con_businesstype` VALUES (019, 'AGENT (OVERSEAS)', '', '2014-07-26 15:54:42');
INSERT INTO `agent_con_businesstype` VALUES (020, 'Private Agent (Overseas)', '', '2014-07-26 16:05:18');

-- --------------------------------------------------------

-- 
-- Table structure for table `agent_con_city`
-- 

CREATE TABLE `agent_con_city` (
  `con_id` int(3) unsigned zerofill NOT NULL auto_increment,
  `con_name` varchar(200) collate utf8_unicode_ci NOT NULL,
  `photo1` varchar(100) collate utf8_unicode_ci NOT NULL,
  `last_edit_time` datetime NOT NULL,
  `province_id` int(11) NOT NULL,
  PRIMARY KEY  (`con_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=14 ;

-- 
-- Dumping data for table `agent_con_city`
-- 

INSERT INTO `agent_con_city` VALUES (007, 'Kata', '', '2014-04-03 16:50:49', 1);
INSERT INTO `agent_con_city` VALUES (008, 'Patong', '', '2014-04-03 16:50:45', 1);
INSERT INTO `agent_con_city` VALUES (011, 'City_agent', '', '2014-04-29 16:32:26', 12);
INSERT INTO `agent_con_city` VALUES (012, 'PHUKET TOWN', '', '2014-06-14 15:39:41', 1);
INSERT INTO `agent_con_city` VALUES (013, 'CHALONG', '', '2014-06-14 15:39:54', 1);

-- --------------------------------------------------------

-- 
-- Table structure for table `agent_con_country`
-- 

CREATE TABLE `agent_con_country` (
  `con_id` int(3) unsigned zerofill NOT NULL auto_increment,
  `con_name` varchar(200) collate utf8_unicode_ci NOT NULL,
  `photo1` varchar(100) collate utf8_unicode_ci NOT NULL,
  `last_edit_time` datetime NOT NULL,
  PRIMARY KEY  (`con_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=20 ;

-- 
-- Dumping data for table `agent_con_country`
-- 

INSERT INTO `agent_con_country` VALUES (001, 'Thailand', '', '2014-04-03 15:58:12');
INSERT INTO `agent_con_country` VALUES (013, 'Country_agent', '', '2014-04-29 16:31:57');
INSERT INTO `agent_con_country` VALUES (011, 'Vietnam', '', '2014-04-22 10:35:14');
INSERT INTO `agent_con_country` VALUES (014, 'South Africa', '', '2014-07-26 15:51:31');
INSERT INTO `agent_con_country` VALUES (015, 'Hungary', '', '2014-07-26 15:51:39');
INSERT INTO `agent_con_country` VALUES (016, 'Malaysia', '', '2014-07-26 15:51:48');
INSERT INTO `agent_con_country` VALUES (017, 'China', '', '2014-07-26 15:52:01');
INSERT INTO `agent_con_country` VALUES (018, 'Singapore', '', '2014-07-26 15:52:21');
INSERT INTO `agent_con_country` VALUES (019, 'Egypt', '', '2014-07-26 15:52:31');

-- --------------------------------------------------------

-- 
-- Table structure for table `agent_con_province`
-- 

CREATE TABLE `agent_con_province` (
  `con_id` int(3) unsigned zerofill NOT NULL auto_increment,
  `con_name` varchar(200) collate utf8_unicode_ci NOT NULL,
  `photo1` varchar(100) collate utf8_unicode_ci NOT NULL,
  `last_edit_time` datetime NOT NULL,
  `country_id` int(11) NOT NULL,
  PRIMARY KEY  (`con_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=15 ;

-- 
-- Dumping data for table `agent_con_province`
-- 

INSERT INTO `agent_con_province` VALUES (001, 'Phuket', '', '2014-04-03 16:31:43', 1);
INSERT INTO `agent_con_province` VALUES (002, 'Krabi', '', '2014-04-03 16:16:21', 1);
INSERT INTO `agent_con_province` VALUES (012, 'Province_agent', '', '2014-04-29 16:32:08', 13);
INSERT INTO `agent_con_province` VALUES (013, 'Bangkok', '', '2014-07-16 15:49:50', 1);
INSERT INTO `agent_con_province` VALUES (014, 'Surat Thani', '', '2014-07-16 15:50:15', 1);

-- --------------------------------------------------------

-- 
-- Table structure for table `agent_credittype`
-- 

CREATE TABLE `agent_credittype` (
  `a_id` int(3) unsigned zerofill NOT NULL auto_increment,
  `a_name` varchar(200) collate utf8_unicode_ci NOT NULL,
  `last_edit_time` datetime NOT NULL,
  PRIMARY KEY  (`a_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

-- 
-- Dumping data for table `agent_credittype`
-- 

INSERT INTO `agent_credittype` VALUES (001, 'Credit แบบวางเงินไว้', '2014-03-26 17:40:45');
INSERT INTO `agent_credittype` VALUES (002, 'Credit แบบไม่ได้วางเงินไว้', '2014-03-26 17:40:47');

-- --------------------------------------------------------

-- 
-- Table structure for table `agent_for`
-- 

CREATE TABLE `agent_for` (
  `a_id` int(3) unsigned zerofill NOT NULL auto_increment,
  `a_name` varchar(200) collate utf8_unicode_ci NOT NULL,
  `last_edit_time` datetime NOT NULL,
  PRIMARY KEY  (`a_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

-- 
-- Dumping data for table `agent_for`
-- 

INSERT INTO `agent_for` VALUES (001, 'AMW', '2014-03-26 16:49:37');
INSERT INTO `agent_for` VALUES (002, 'DOTCOM', '2014-03-26 16:49:41');

-- --------------------------------------------------------

-- 
-- Table structure for table `agent_grade`
-- 

CREATE TABLE `agent_grade` (
  `a_id` int(3) unsigned zerofill NOT NULL auto_increment,
  `a_name` varchar(200) collate utf8_unicode_ci NOT NULL,
  `last_edit_time` datetime NOT NULL,
  PRIMARY KEY  (`a_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

-- 
-- Dumping data for table `agent_grade`
-- 

INSERT INTO `agent_grade` VALUES (001, 'A', '2014-03-26 16:52:52');
INSERT INTO `agent_grade` VALUES (002, 'B', '2014-03-26 16:52:52');
INSERT INTO `agent_grade` VALUES (003, 'C', '2014-03-26 16:53:11');
INSERT INTO `agent_grade` VALUES (004, 'D', '2014-03-26 16:53:17');
INSERT INTO `agent_grade` VALUES (005, 'E', '2014-03-26 16:53:29');

-- --------------------------------------------------------

-- 
-- Table structure for table `agent_paytype`
-- 

CREATE TABLE `agent_paytype` (
  `a_id` int(3) unsigned zerofill NOT NULL auto_increment,
  `a_name` varchar(200) collate utf8_unicode_ci NOT NULL,
  `last_edit_time` datetime NOT NULL,
  PRIMARY KEY  (`a_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

-- 
-- Dumping data for table `agent_paytype`
-- 

INSERT INTO `agent_paytype` VALUES (001, 'Payment Online', '2014-03-26 17:27:01');
INSERT INTO `agent_paytype` VALUES (002, 'Credit', '2014-03-26 17:38:45');

-- --------------------------------------------------------

-- 
-- Table structure for table `agent_relation`
-- 

CREATE TABLE `agent_relation` (
  `a_id` int(3) unsigned zerofill NOT NULL auto_increment,
  `a_name` varchar(200) collate utf8_unicode_ci NOT NULL,
  `last_edit_time` datetime NOT NULL,
  PRIMARY KEY  (`a_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

-- 
-- Dumping data for table `agent_relation`
-- 

INSERT INTO `agent_relation` VALUES (001, 'ดี', '2014-03-26 16:50:26');
INSERT INTO `agent_relation` VALUES (002, 'พอใช้', '2014-03-26 16:50:26');
INSERT INTO `agent_relation` VALUES (003, 'ไม่ดี', '2014-03-26 16:50:26');

-- --------------------------------------------------------

-- 
-- Table structure for table `agent_type`
-- 

CREATE TABLE `agent_type` (
  `a_id` int(3) unsigned zerofill NOT NULL auto_increment,
  `a_name` varchar(200) collate utf8_unicode_ci NOT NULL,
  `last_edit_time` datetime NOT NULL,
  PRIMARY KEY  (`a_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

-- 
-- Dumping data for table `agent_type`
-- 

INSERT INTO `agent_type` VALUES (001, 'Supplier', '2014-03-26 16:51:09');
INSERT INTO `agent_type` VALUES (002, 'Customer', '2014-03-26 16:51:12');

-- --------------------------------------------------------

-- 
-- Table structure for table `agent_typeofclient`
-- 

CREATE TABLE `agent_typeofclient` (
  `a_id` int(3) unsigned zerofill NOT NULL auto_increment,
  `a_name` varchar(200) collate utf8_unicode_ci NOT NULL,
  `last_edit_time` datetime NOT NULL,
  PRIMARY KEY  (`a_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

-- 
-- Dumping data for table `agent_typeofclient`
-- 

INSERT INTO `agent_typeofclient` VALUES (003, 'FIT', '2014-04-28 16:54:23');
INSERT INTO `agent_typeofclient` VALUES (004, 'GIT', '2014-04-28 16:54:18');

-- --------------------------------------------------------

-- 
-- Table structure for table `boattransfers`
-- 

CREATE TABLE `boattransfers` (
  `bot_id` int(11) NOT NULL auto_increment,
  `boattransfer_type_id` int(11) NOT NULL,
  `time_id` int(11) NOT NULL,
  `bot_time` varchar(100) collate utf8_unicode_ci NOT NULL,
  `boat_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `province_id` int(11) NOT NULL,
  `boattransferpricetype_id` int(11) NOT NULL,
  `bot_ref` varchar(100) collate utf8_unicode_ci NOT NULL,
  `agents_sup_id` int(11) NOT NULL,
  `bot_name` varchar(200) collate utf8_unicode_ci NOT NULL,
  `area_id` int(11) NOT NULL,
  `area_to_id` int(11) NOT NULL,
  `bot_hot` int(1) NOT NULL,
  `boattransfer_class_id` int(11) NOT NULL,
  `last_edit_time` datetime NOT NULL,
  PRIMARY KEY  (`bot_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

-- 
-- Dumping data for table `boattransfers`
-- 

INSERT INTO `boattransfers` VALUES (3, 2, 3, 'ประมาณ 35 นาที', 2, 1, 1, 1, 'BOA002', 4, 'Boat Transfer 02', 1, 2, 1, 3, '2014-08-11 14:32:39');
INSERT INTO `boattransfers` VALUES (2, 2, 2, 'ประมาณ 55 นาที', 1, 1, 2, 2, 'BOA001', 2, 'Boat Transfer 01', 3, 4, 1, 1, '2014-08-11 14:31:15');

-- --------------------------------------------------------

-- 
-- Table structure for table `boattransfer_allocation`
-- 

CREATE TABLE `boattransfer_allocation` (
  `boaallo_id` int(11) NOT NULL auto_increment,
  `boattransfers_id` int(11) NOT NULL,
  `boaallo_datefrom` date NOT NULL,
  `boaallo_dateto` date NOT NULL,
  `boaallo_global` int(11) NOT NULL,
  `boattransferratetype_id` int(11) NOT NULL,
  `last_edit_time` datetime NOT NULL,
  PRIMARY KEY  (`boaallo_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

-- 
-- Dumping data for table `boattransfer_allocation`
-- 

INSERT INTO `boattransfer_allocation` VALUES (1, 2, '2014-01-01', '2014-02-06', 4, 1, '2014-08-11 16:46:51');
INSERT INTO `boattransfer_allocation` VALUES (2, 2, '2014-01-01', '2014-02-11', 0, 1, '2014-08-11 16:48:39');
INSERT INTO `boattransfer_allocation` VALUES (3, 2, '2014-01-01', '2014-02-07', 7, 1, '2014-08-11 16:49:06');
INSERT INTO `boattransfer_allocation` VALUES (4, 3, '2014-01-01', '2014-04-15', 9, 3, '2014-08-11 16:53:58');

-- --------------------------------------------------------

-- 
-- Table structure for table `boattransfer_allocationdaily`
-- 

CREATE TABLE `boattransfer_allocationdaily` (
  `boaalloday_id` int(11) NOT NULL auto_increment,
  `boattransfers_id` int(11) NOT NULL,
  `boaalloday_date` date NOT NULL,
  `boaalloday_value` int(11) NOT NULL,
  `boattransferratetype_id` int(11) NOT NULL,
  `last_edit_time` datetime NOT NULL,
  PRIMARY KEY  (`boaalloday_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=232 ;

-- 
-- Dumping data for table `boattransfer_allocationdaily`
-- 

INSERT INTO `boattransfer_allocationdaily` VALUES (84, 2, '2014-02-11', 0, 1, '2014-08-11 16:49:55');
INSERT INTO `boattransfer_allocationdaily` VALUES (83, 2, '2014-02-10', 0, 1, '2014-08-11 16:49:55');
INSERT INTO `boattransfer_allocationdaily` VALUES (82, 2, '2014-02-09', 8, 1, '2014-08-11 16:49:55');
INSERT INTO `boattransfer_allocationdaily` VALUES (81, 2, '2014-02-08', 8, 1, '2014-08-11 16:49:55');
INSERT INTO `boattransfer_allocationdaily` VALUES (122, 2, '2014-02-07', 7, 1, '2014-08-11 16:49:55');
INSERT INTO `boattransfer_allocationdaily` VALUES (121, 2, '2014-02-06', 7, 1, '2014-08-11 16:49:55');
INSERT INTO `boattransfer_allocationdaily` VALUES (120, 2, '2014-02-05', 7, 1, '2014-08-11 16:49:55');
INSERT INTO `boattransfer_allocationdaily` VALUES (119, 2, '2014-02-04', 7, 1, '2014-08-11 16:49:55');
INSERT INTO `boattransfer_allocationdaily` VALUES (118, 2, '2014-02-03', 7, 1, '2014-08-11 16:49:55');
INSERT INTO `boattransfer_allocationdaily` VALUES (117, 2, '2014-02-02', 7, 1, '2014-08-11 16:49:55');
INSERT INTO `boattransfer_allocationdaily` VALUES (116, 2, '2014-02-01', 7, 1, '2014-08-11 16:49:55');
INSERT INTO `boattransfer_allocationdaily` VALUES (115, 2, '2014-01-31', 7, 1, '2014-08-11 16:49:55');
INSERT INTO `boattransfer_allocationdaily` VALUES (114, 2, '2014-01-30', 7, 1, '2014-08-11 16:49:55');
INSERT INTO `boattransfer_allocationdaily` VALUES (113, 2, '2014-01-29', 7, 1, '2014-08-11 16:49:55');
INSERT INTO `boattransfer_allocationdaily` VALUES (112, 2, '2014-01-28', 7, 1, '2014-08-11 16:49:55');
INSERT INTO `boattransfer_allocationdaily` VALUES (111, 2, '2014-01-27', 7, 1, '2014-08-11 16:49:55');
INSERT INTO `boattransfer_allocationdaily` VALUES (110, 2, '2014-01-26', 7, 1, '2014-08-11 16:49:55');
INSERT INTO `boattransfer_allocationdaily` VALUES (109, 2, '2014-01-25', 7, 1, '2014-08-11 16:49:55');
INSERT INTO `boattransfer_allocationdaily` VALUES (108, 2, '2014-01-24', 7, 1, '2014-08-11 16:49:55');
INSERT INTO `boattransfer_allocationdaily` VALUES (107, 2, '2014-01-23', 7, 1, '2014-08-11 16:49:55');
INSERT INTO `boattransfer_allocationdaily` VALUES (106, 2, '2014-01-22', 7, 1, '2014-08-11 16:49:55');
INSERT INTO `boattransfer_allocationdaily` VALUES (105, 2, '2014-01-21', 7, 1, '2014-08-11 16:49:55');
INSERT INTO `boattransfer_allocationdaily` VALUES (104, 2, '2014-01-20', 7, 1, '2014-08-11 16:49:55');
INSERT INTO `boattransfer_allocationdaily` VALUES (103, 2, '2014-01-19', 7, 1, '2014-08-11 16:49:55');
INSERT INTO `boattransfer_allocationdaily` VALUES (102, 2, '2014-01-18', 7, 1, '2014-08-11 16:49:55');
INSERT INTO `boattransfer_allocationdaily` VALUES (101, 2, '2014-01-17', 7, 1, '2014-08-11 16:49:55');
INSERT INTO `boattransfer_allocationdaily` VALUES (100, 2, '2014-01-16', 7, 1, '2014-08-11 16:49:55');
INSERT INTO `boattransfer_allocationdaily` VALUES (99, 2, '2014-01-15', 7, 1, '2014-08-11 16:49:55');
INSERT INTO `boattransfer_allocationdaily` VALUES (98, 2, '2014-01-14', 7, 1, '2014-08-11 16:49:55');
INSERT INTO `boattransfer_allocationdaily` VALUES (97, 2, '2014-01-13', 7, 1, '2014-08-11 16:49:55');
INSERT INTO `boattransfer_allocationdaily` VALUES (96, 2, '2014-01-12', 7, 1, '2014-08-11 16:49:55');
INSERT INTO `boattransfer_allocationdaily` VALUES (95, 2, '2014-01-11', 7, 1, '2014-08-11 16:49:55');
INSERT INTO `boattransfer_allocationdaily` VALUES (94, 2, '2014-01-10', 7, 1, '2014-08-11 16:49:55');
INSERT INTO `boattransfer_allocationdaily` VALUES (93, 2, '2014-01-09', 7, 1, '2014-08-11 16:49:55');
INSERT INTO `boattransfer_allocationdaily` VALUES (92, 2, '2014-01-08', 7, 1, '2014-08-11 16:49:55');
INSERT INTO `boattransfer_allocationdaily` VALUES (91, 2, '2014-01-07', 7, 1, '2014-08-11 16:49:55');
INSERT INTO `boattransfer_allocationdaily` VALUES (90, 2, '2014-01-06', 7, 1, '2014-08-11 16:49:55');
INSERT INTO `boattransfer_allocationdaily` VALUES (89, 2, '2014-01-05', 7, 1, '2014-08-11 16:49:55');
INSERT INTO `boattransfer_allocationdaily` VALUES (88, 2, '2014-01-04', 7, 1, '2014-08-11 16:49:55');
INSERT INTO `boattransfer_allocationdaily` VALUES (87, 2, '2014-01-03', 7, 1, '2014-08-11 16:49:55');
INSERT INTO `boattransfer_allocationdaily` VALUES (86, 2, '2014-01-02', 7, 1, '2014-08-11 16:49:55');
INSERT INTO `boattransfer_allocationdaily` VALUES (85, 2, '2014-01-01', 7, 1, '2014-08-11 16:49:55');
INSERT INTO `boattransfer_allocationdaily` VALUES (123, 2, '2014-02-12', 0, 1, '2014-08-11 16:49:55');
INSERT INTO `boattransfer_allocationdaily` VALUES (124, 2, '2014-02-13', 0, 1, '2014-08-11 16:49:55');
INSERT INTO `boattransfer_allocationdaily` VALUES (125, 2, '2014-02-14', 0, 1, '2014-08-11 16:49:55');
INSERT INTO `boattransfer_allocationdaily` VALUES (126, 2, '2014-02-15', 0, 1, '2014-08-11 16:49:55');
INSERT INTO `boattransfer_allocationdaily` VALUES (127, 3, '2014-01-01', 9, 3, '2014-08-11 16:53:58');
INSERT INTO `boattransfer_allocationdaily` VALUES (128, 3, '2014-01-02', 9, 3, '2014-08-11 16:53:58');
INSERT INTO `boattransfer_allocationdaily` VALUES (129, 3, '2014-01-03', 9, 3, '2014-08-11 16:53:58');
INSERT INTO `boattransfer_allocationdaily` VALUES (130, 3, '2014-01-04', 9, 3, '2014-08-11 16:53:58');
INSERT INTO `boattransfer_allocationdaily` VALUES (131, 3, '2014-01-05', 9, 3, '2014-08-11 16:53:58');
INSERT INTO `boattransfer_allocationdaily` VALUES (132, 3, '2014-01-06', 9, 3, '2014-08-11 16:53:58');
INSERT INTO `boattransfer_allocationdaily` VALUES (133, 3, '2014-01-07', 9, 3, '2014-08-11 16:53:58');
INSERT INTO `boattransfer_allocationdaily` VALUES (134, 3, '2014-01-08', 9, 3, '2014-08-11 16:53:58');
INSERT INTO `boattransfer_allocationdaily` VALUES (135, 3, '2014-01-09', 9, 3, '2014-08-11 16:53:58');
INSERT INTO `boattransfer_allocationdaily` VALUES (136, 3, '2014-01-10', 9, 3, '2014-08-11 16:53:58');
INSERT INTO `boattransfer_allocationdaily` VALUES (137, 3, '2014-01-11', 9, 3, '2014-08-11 16:53:58');
INSERT INTO `boattransfer_allocationdaily` VALUES (138, 3, '2014-01-12', 9, 3, '2014-08-11 16:53:58');
INSERT INTO `boattransfer_allocationdaily` VALUES (139, 3, '2014-01-13', 9, 3, '2014-08-11 16:53:58');
INSERT INTO `boattransfer_allocationdaily` VALUES (140, 3, '2014-01-14', 9, 3, '2014-08-11 16:53:58');
INSERT INTO `boattransfer_allocationdaily` VALUES (141, 3, '2014-01-15', 9, 3, '2014-08-11 16:53:58');
INSERT INTO `boattransfer_allocationdaily` VALUES (142, 3, '2014-01-16', 9, 3, '2014-08-11 16:53:58');
INSERT INTO `boattransfer_allocationdaily` VALUES (143, 3, '2014-01-17', 9, 3, '2014-08-11 16:53:58');
INSERT INTO `boattransfer_allocationdaily` VALUES (144, 3, '2014-01-18', 9, 3, '2014-08-11 16:53:58');
INSERT INTO `boattransfer_allocationdaily` VALUES (145, 3, '2014-01-19', 9, 3, '2014-08-11 16:53:58');
INSERT INTO `boattransfer_allocationdaily` VALUES (146, 3, '2014-01-20', 9, 3, '2014-08-11 16:53:58');
INSERT INTO `boattransfer_allocationdaily` VALUES (147, 3, '2014-01-21', 9, 3, '2014-08-11 16:53:58');
INSERT INTO `boattransfer_allocationdaily` VALUES (148, 3, '2014-01-22', 9, 3, '2014-08-11 16:53:58');
INSERT INTO `boattransfer_allocationdaily` VALUES (149, 3, '2014-01-23', 9, 3, '2014-08-11 16:53:58');
INSERT INTO `boattransfer_allocationdaily` VALUES (150, 3, '2014-01-24', 9, 3, '2014-08-11 16:53:58');
INSERT INTO `boattransfer_allocationdaily` VALUES (151, 3, '2014-01-25', 9, 3, '2014-08-11 16:53:58');
INSERT INTO `boattransfer_allocationdaily` VALUES (152, 3, '2014-01-26', 9, 3, '2014-08-11 16:53:58');
INSERT INTO `boattransfer_allocationdaily` VALUES (153, 3, '2014-01-27', 9, 3, '2014-08-11 16:53:58');
INSERT INTO `boattransfer_allocationdaily` VALUES (154, 3, '2014-01-28', 9, 3, '2014-08-11 16:53:58');
INSERT INTO `boattransfer_allocationdaily` VALUES (155, 3, '2014-01-29', 9, 3, '2014-08-11 16:53:58');
INSERT INTO `boattransfer_allocationdaily` VALUES (156, 3, '2014-01-30', 9, 3, '2014-08-11 16:53:58');
INSERT INTO `boattransfer_allocationdaily` VALUES (157, 3, '2014-01-31', 9, 3, '2014-08-11 16:53:58');
INSERT INTO `boattransfer_allocationdaily` VALUES (158, 3, '2014-02-01', 9, 3, '2014-08-11 16:53:58');
INSERT INTO `boattransfer_allocationdaily` VALUES (159, 3, '2014-02-02', 9, 3, '2014-08-11 16:53:58');
INSERT INTO `boattransfer_allocationdaily` VALUES (160, 3, '2014-02-03', 9, 3, '2014-08-11 16:53:58');
INSERT INTO `boattransfer_allocationdaily` VALUES (161, 3, '2014-02-04', 9, 3, '2014-08-11 16:53:58');
INSERT INTO `boattransfer_allocationdaily` VALUES (162, 3, '2014-02-05', 9, 3, '2014-08-11 16:53:58');
INSERT INTO `boattransfer_allocationdaily` VALUES (163, 3, '2014-02-06', 9, 3, '2014-08-11 16:53:58');
INSERT INTO `boattransfer_allocationdaily` VALUES (164, 3, '2014-02-07', 9, 3, '2014-08-11 16:53:58');
INSERT INTO `boattransfer_allocationdaily` VALUES (165, 3, '2014-02-08', 9, 3, '2014-08-11 16:53:58');
INSERT INTO `boattransfer_allocationdaily` VALUES (166, 3, '2014-02-09', 9, 3, '2014-08-11 16:53:58');
INSERT INTO `boattransfer_allocationdaily` VALUES (167, 3, '2014-02-10', 9, 3, '2014-08-11 16:53:58');
INSERT INTO `boattransfer_allocationdaily` VALUES (168, 3, '2014-02-11', 9, 3, '2014-08-11 16:53:58');
INSERT INTO `boattransfer_allocationdaily` VALUES (169, 3, '2014-02-12', 9, 3, '2014-08-11 16:53:58');
INSERT INTO `boattransfer_allocationdaily` VALUES (170, 3, '2014-02-13', 9, 3, '2014-08-11 16:53:58');
INSERT INTO `boattransfer_allocationdaily` VALUES (171, 3, '2014-02-14', 9, 3, '2014-08-11 16:53:58');
INSERT INTO `boattransfer_allocationdaily` VALUES (172, 3, '2014-02-15', 9, 3, '2014-08-11 16:53:58');
INSERT INTO `boattransfer_allocationdaily` VALUES (173, 3, '2014-02-16', 9, 3, '2014-08-11 16:53:58');
INSERT INTO `boattransfer_allocationdaily` VALUES (174, 3, '2014-02-17', 9, 3, '2014-08-11 16:53:58');
INSERT INTO `boattransfer_allocationdaily` VALUES (175, 3, '2014-02-18', 9, 3, '2014-08-11 16:53:58');
INSERT INTO `boattransfer_allocationdaily` VALUES (176, 3, '2014-02-19', 9, 3, '2014-08-11 16:53:58');
INSERT INTO `boattransfer_allocationdaily` VALUES (177, 3, '2014-02-20', 9, 3, '2014-08-11 16:53:58');
INSERT INTO `boattransfer_allocationdaily` VALUES (178, 3, '2014-02-21', 9, 3, '2014-08-11 16:53:58');
INSERT INTO `boattransfer_allocationdaily` VALUES (179, 3, '2014-02-22', 9, 3, '2014-08-11 16:53:58');
INSERT INTO `boattransfer_allocationdaily` VALUES (180, 3, '2014-02-23', 9, 3, '2014-08-11 16:53:58');
INSERT INTO `boattransfer_allocationdaily` VALUES (181, 3, '2014-02-24', 9, 3, '2014-08-11 16:53:58');
INSERT INTO `boattransfer_allocationdaily` VALUES (182, 3, '2014-02-25', 9, 3, '2014-08-11 16:53:58');
INSERT INTO `boattransfer_allocationdaily` VALUES (183, 3, '2014-02-26', 9, 3, '2014-08-11 16:53:58');
INSERT INTO `boattransfer_allocationdaily` VALUES (184, 3, '2014-02-27', 9, 3, '2014-08-11 16:53:58');
INSERT INTO `boattransfer_allocationdaily` VALUES (185, 3, '2014-02-28', 9, 3, '2014-08-11 16:53:58');
INSERT INTO `boattransfer_allocationdaily` VALUES (186, 3, '2014-03-01', 9, 3, '2014-08-11 16:53:58');
INSERT INTO `boattransfer_allocationdaily` VALUES (187, 3, '2014-03-02', 9, 3, '2014-08-11 16:53:58');
INSERT INTO `boattransfer_allocationdaily` VALUES (188, 3, '2014-03-03', 9, 3, '2014-08-11 16:53:58');
INSERT INTO `boattransfer_allocationdaily` VALUES (189, 3, '2014-03-04', 9, 3, '2014-08-11 16:53:58');
INSERT INTO `boattransfer_allocationdaily` VALUES (190, 3, '2014-03-05', 9, 3, '2014-08-11 16:53:58');
INSERT INTO `boattransfer_allocationdaily` VALUES (191, 3, '2014-03-06', 9, 3, '2014-08-11 16:53:58');
INSERT INTO `boattransfer_allocationdaily` VALUES (192, 3, '2014-03-07', 9, 3, '2014-08-11 16:53:58');
INSERT INTO `boattransfer_allocationdaily` VALUES (193, 3, '2014-03-08', 9, 3, '2014-08-11 16:53:58');
INSERT INTO `boattransfer_allocationdaily` VALUES (194, 3, '2014-03-09', 9, 3, '2014-08-11 16:53:58');
INSERT INTO `boattransfer_allocationdaily` VALUES (195, 3, '2014-03-10', 9, 3, '2014-08-11 16:53:58');
INSERT INTO `boattransfer_allocationdaily` VALUES (196, 3, '2014-03-11', 9, 3, '2014-08-11 16:53:58');
INSERT INTO `boattransfer_allocationdaily` VALUES (197, 3, '2014-03-12', 9, 3, '2014-08-11 16:53:58');
INSERT INTO `boattransfer_allocationdaily` VALUES (198, 3, '2014-03-13', 9, 3, '2014-08-11 16:53:58');
INSERT INTO `boattransfer_allocationdaily` VALUES (199, 3, '2014-03-14', 9, 3, '2014-08-11 16:53:58');
INSERT INTO `boattransfer_allocationdaily` VALUES (200, 3, '2014-03-15', 9, 3, '2014-08-11 16:53:58');
INSERT INTO `boattransfer_allocationdaily` VALUES (201, 3, '2014-03-16', 9, 3, '2014-08-11 16:53:58');
INSERT INTO `boattransfer_allocationdaily` VALUES (202, 3, '2014-03-17', 9, 3, '2014-08-11 16:53:58');
INSERT INTO `boattransfer_allocationdaily` VALUES (203, 3, '2014-03-18', 9, 3, '2014-08-11 16:53:58');
INSERT INTO `boattransfer_allocationdaily` VALUES (204, 3, '2014-03-19', 9, 3, '2014-08-11 16:53:58');
INSERT INTO `boattransfer_allocationdaily` VALUES (205, 3, '2014-03-20', 9, 3, '2014-08-11 16:53:58');
INSERT INTO `boattransfer_allocationdaily` VALUES (206, 3, '2014-03-21', 9, 3, '2014-08-11 16:53:58');
INSERT INTO `boattransfer_allocationdaily` VALUES (207, 3, '2014-03-22', 9, 3, '2014-08-11 16:53:58');
INSERT INTO `boattransfer_allocationdaily` VALUES (208, 3, '2014-03-23', 9, 3, '2014-08-11 16:53:58');
INSERT INTO `boattransfer_allocationdaily` VALUES (209, 3, '2014-03-24', 9, 3, '2014-08-11 16:53:58');
INSERT INTO `boattransfer_allocationdaily` VALUES (210, 3, '2014-03-25', 9, 3, '2014-08-11 16:53:58');
INSERT INTO `boattransfer_allocationdaily` VALUES (211, 3, '2014-03-26', 9, 3, '2014-08-11 16:53:58');
INSERT INTO `boattransfer_allocationdaily` VALUES (212, 3, '2014-03-27', 9, 3, '2014-08-11 16:53:58');
INSERT INTO `boattransfer_allocationdaily` VALUES (213, 3, '2014-03-28', 9, 3, '2014-08-11 16:53:58');
INSERT INTO `boattransfer_allocationdaily` VALUES (214, 3, '2014-03-29', 9, 3, '2014-08-11 16:53:58');
INSERT INTO `boattransfer_allocationdaily` VALUES (215, 3, '2014-03-30', 9, 3, '2014-08-11 16:53:58');
INSERT INTO `boattransfer_allocationdaily` VALUES (216, 3, '2014-03-31', 9, 3, '2014-08-11 16:53:58');
INSERT INTO `boattransfer_allocationdaily` VALUES (217, 3, '2014-04-01', 9, 3, '2014-08-11 16:53:58');
INSERT INTO `boattransfer_allocationdaily` VALUES (218, 3, '2014-04-02', 9, 3, '2014-08-11 16:53:58');
INSERT INTO `boattransfer_allocationdaily` VALUES (219, 3, '2014-04-03', 9, 3, '2014-08-11 16:53:58');
INSERT INTO `boattransfer_allocationdaily` VALUES (220, 3, '2014-04-04', 9, 3, '2014-08-11 16:53:58');
INSERT INTO `boattransfer_allocationdaily` VALUES (221, 3, '2014-04-05', 9, 3, '2014-08-11 16:53:58');
INSERT INTO `boattransfer_allocationdaily` VALUES (222, 3, '2014-04-06', 9, 3, '2014-08-11 16:53:58');
INSERT INTO `boattransfer_allocationdaily` VALUES (223, 3, '2014-04-07', 9, 3, '2014-08-11 16:53:58');
INSERT INTO `boattransfer_allocationdaily` VALUES (224, 3, '2014-04-08', 9, 3, '2014-08-11 16:53:58');
INSERT INTO `boattransfer_allocationdaily` VALUES (225, 3, '2014-04-09', 9, 3, '2014-08-11 16:53:58');
INSERT INTO `boattransfer_allocationdaily` VALUES (226, 3, '2014-04-10', 9, 3, '2014-08-11 16:53:58');
INSERT INTO `boattransfer_allocationdaily` VALUES (227, 3, '2014-04-11', 9, 3, '2014-08-11 16:53:58');
INSERT INTO `boattransfer_allocationdaily` VALUES (228, 3, '2014-04-12', 9, 3, '2014-08-11 16:53:58');
INSERT INTO `boattransfer_allocationdaily` VALUES (229, 3, '2014-04-13', 9, 3, '2014-08-11 16:53:58');
INSERT INTO `boattransfer_allocationdaily` VALUES (230, 3, '2014-04-14', 9, 3, '2014-08-11 16:53:58');
INSERT INTO `boattransfer_allocationdaily` VALUES (231, 3, '2014-04-15', 9, 3, '2014-08-11 16:53:58');

-- --------------------------------------------------------

-- 
-- Table structure for table `boattransfer_class`
-- 

CREATE TABLE `boattransfer_class` (
  `c_id` int(11) NOT NULL auto_increment,
  `c_name` varchar(200) collate utf8_unicode_ci NOT NULL,
  `photo1` varchar(100) collate utf8_unicode_ci NOT NULL,
  `last_edit_time` datetime NOT NULL,
  PRIMARY KEY  (`c_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

-- 
-- Dumping data for table `boattransfer_class`
-- 

INSERT INTO `boattransfer_class` VALUES (1, 'Standard Class', '', '0000-00-00 00:00:00');
INSERT INTO `boattransfer_class` VALUES (2, 'First Class', '', '0000-00-00 00:00:00');
INSERT INTO `boattransfer_class` VALUES (3, 'Premium Class', '', '0000-00-00 00:00:00');

-- --------------------------------------------------------

-- 
-- Table structure for table `boattransfer_con_area`
-- 

CREATE TABLE `boattransfer_con_area` (
  `con_id` int(3) unsigned zerofill NOT NULL auto_increment,
  `con_name` varchar(200) collate utf8_unicode_ci NOT NULL,
  `photo1` varchar(100) collate utf8_unicode_ci NOT NULL,
  `last_edit_time` datetime NOT NULL,
  `province_id` int(11) NOT NULL,
  PRIMARY KEY  (`con_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

-- 
-- Dumping data for table `boattransfer_con_area`
-- 

INSERT INTO `boattransfer_con_area` VALUES (001, 'Boat Transfer Area01', '', '2014-08-08 15:25:30', 1);
INSERT INTO `boattransfer_con_area` VALUES (002, 'Boat Transfer Area02', '', '2014-08-08 15:25:36', 1);
INSERT INTO `boattransfer_con_area` VALUES (003, 'Boat Transfer Area03', '', '2014-08-08 15:25:43', 2);
INSERT INTO `boattransfer_con_area` VALUES (004, 'Boat Transfer Area04', '', '2014-08-08 15:25:47', 2);
INSERT INTO `boattransfer_con_area` VALUES (005, 'Boat Transfer Area05', '', '2014-08-08 15:25:52', 3);
INSERT INTO `boattransfer_con_area` VALUES (006, 'Boat Transfer Area06', '', '2014-08-08 15:26:10', 3);

-- --------------------------------------------------------

-- 
-- Table structure for table `boattransfer_con_boat`
-- 

CREATE TABLE `boattransfer_con_boat` (
  `con_id` int(11) NOT NULL auto_increment,
  `con_name` varchar(200) collate utf8_unicode_ci NOT NULL,
  `photo1` varchar(100) collate utf8_unicode_ci NOT NULL,
  `last_edit_time` datetime NOT NULL,
  `boa_registration` varchar(100) collate utf8_unicode_ci NOT NULL,
  `boa_brand` varchar(100) collate utf8_unicode_ci NOT NULL,
  `boa_model` varchar(100) collate utf8_unicode_ci NOT NULL,
  `boa_standardseat_num` int(5) NOT NULL,
  `boa_firstseat_num` int(5) NOT NULL,
  `boa_premiumseat_num` int(5) NOT NULL,
  `boa_driver_name` varchar(200) collate utf8_unicode_ci NOT NULL,
  `boa_driver_tel` varchar(100) collate utf8_unicode_ci NOT NULL,
  `boa_driver_license` text collate utf8_unicode_ci NOT NULL,
  PRIMARY KEY  (`con_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

-- 
-- Dumping data for table `boattransfer_con_boat`
-- 

INSERT INTO `boattransfer_con_boat` VALUES (1, '', '', '2014-08-08 16:20:14', 'Boat Transfer Boat 01', 'ยี่ห้อเรือ 01', 'รุ่นเรือ 01', 100, 200, 300, 'ชื่อคนขับเรือ 01', '01111111111', 'ข้อมูลใบขับขี่ 01');
INSERT INTO `boattransfer_con_boat` VALUES (2, '', '', '2014-08-08 16:22:02', 'Boat Transfer Boat 02', 'ยี่ห้อเรือ 02', 'รุ่นเรือ 02', 300, 200, 100, 'ชื่อคนขับเรือ 02', '02222222222', 'ข้อมูลใบขับขี่ 02');

-- --------------------------------------------------------

-- 
-- Table structure for table `boattransfer_con_country`
-- 

CREATE TABLE `boattransfer_con_country` (
  `con_id` int(3) unsigned zerofill NOT NULL auto_increment,
  `con_name` varchar(200) collate utf8_unicode_ci NOT NULL,
  `photo1` varchar(100) collate utf8_unicode_ci NOT NULL,
  `last_edit_time` datetime NOT NULL,
  PRIMARY KEY  (`con_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

-- 
-- Dumping data for table `boattransfer_con_country`
-- 

INSERT INTO `boattransfer_con_country` VALUES (001, 'Boat Transfer Country 01', '', '2014-08-08 15:16:43');
INSERT INTO `boattransfer_con_country` VALUES (002, 'Boat Transfer Country 02', '', '2014-08-08 15:16:49');
INSERT INTO `boattransfer_con_country` VALUES (003, 'Boat Transfer Country 03', '', '2014-08-08 15:16:52');

-- --------------------------------------------------------

-- 
-- Table structure for table `boattransfer_con_destination`
-- 

CREATE TABLE `boattransfer_con_destination` (
  `con_id` int(3) unsigned zerofill NOT NULL auto_increment,
  `con_name` varchar(200) collate utf8_unicode_ci NOT NULL,
  `photo1` varchar(100) collate utf8_unicode_ci NOT NULL,
  `last_edit_time` datetime NOT NULL,
  `country_id` int(11) NOT NULL,
  `province_id` int(11) NOT NULL,
  `area_id` int(11) NOT NULL,
  PRIMARY KEY  (`con_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

-- 
-- Dumping data for table `boattransfer_con_destination`
-- 

INSERT INTO `boattransfer_con_destination` VALUES (001, 'Boat Transfer Destination01', '', '2014-08-08 15:31:49', 1, 1, 1);
INSERT INTO `boattransfer_con_destination` VALUES (002, 'Boat Transfer Destination02', '', '2014-08-08 15:31:56', 1, 1, 2);
INSERT INTO `boattransfer_con_destination` VALUES (003, 'Boat Transfer Destination03', '', '2014-08-08 15:32:19', 1, 1, 2);

-- --------------------------------------------------------

-- 
-- Table structure for table `boattransfer_con_province`
-- 

CREATE TABLE `boattransfer_con_province` (
  `con_id` int(3) unsigned zerofill NOT NULL auto_increment,
  `con_name` varchar(200) collate utf8_unicode_ci NOT NULL,
  `photo1` varchar(100) collate utf8_unicode_ci NOT NULL,
  `last_edit_time` datetime NOT NULL,
  `country_id` int(11) NOT NULL,
  PRIMARY KEY  (`con_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

-- 
-- Dumping data for table `boattransfer_con_province`
-- 

INSERT INTO `boattransfer_con_province` VALUES (001, 'Boat Transfer Province 01', '', '2014-08-08 15:21:40', 1);
INSERT INTO `boattransfer_con_province` VALUES (002, 'Boat Transfer Province 02', '', '2014-08-08 15:21:45', 1);
INSERT INTO `boattransfer_con_province` VALUES (003, 'Boat Transfer Province 03', '', '2014-08-08 15:21:51', 2);
INSERT INTO `boattransfer_con_province` VALUES (004, 'Boat Transfer Province 04', '', '2014-08-08 15:21:56', 3);

-- --------------------------------------------------------

-- 
-- Table structure for table `boattransfer_con_time`
-- 

CREATE TABLE `boattransfer_con_time` (
  `con_id` int(3) unsigned zerofill NOT NULL auto_increment,
  `con_name` varchar(100) collate utf8_unicode_ci NOT NULL,
  `photo1` varchar(100) collate utf8_unicode_ci NOT NULL,
  `last_edit_time` datetime NOT NULL,
  PRIMARY KEY  (`con_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

-- 
-- Dumping data for table `boattransfer_con_time`
-- 

INSERT INTO `boattransfer_con_time` VALUES (001, '04:05', '', '2014-08-08 15:46:46');
INSERT INTO `boattransfer_con_time` VALUES (002, '05:30', '', '2014-08-08 15:46:58');
INSERT INTO `boattransfer_con_time` VALUES (003, '19:00', '', '2014-08-08 15:47:16');

-- --------------------------------------------------------

-- 
-- Table structure for table `boattransfer_period`
-- 

CREATE TABLE `boattransfer_period` (
  `botpe_id` int(11) NOT NULL auto_increment,
  `boattransfers_id` int(11) NOT NULL,
  `botpe_name` varchar(200) collate utf8_unicode_ci NOT NULL,
  `botpe_datefrom` date NOT NULL,
  `botpe_dateto` date NOT NULL,
  `arrange` int(11) NOT NULL,
  `last_edit_time` datetime NOT NULL,
  PRIMARY KEY  (`botpe_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

-- 
-- Dumping data for table `boattransfer_period`
-- 

INSERT INTO `boattransfer_period` VALUES (1, 2, 'Boat Period 01', '2014-01-01', '2014-05-31', 0, '2014-08-11 15:40:18');
INSERT INTO `boattransfer_period` VALUES (3, 2, 'Boat Period 02', '2014-06-01', '2014-12-31', 0, '2014-08-11 15:40:40');
INSERT INTO `boattransfer_period` VALUES (4, 3, 'Boat Period 03', '2014-01-01', '2014-12-31', 0, '2014-08-11 15:54:22');

-- --------------------------------------------------------

-- 
-- Table structure for table `boattransfer_price_type`
-- 

CREATE TABLE `boattransfer_price_type` (
  `c_id` int(11) NOT NULL auto_increment,
  `c_name` varchar(200) collate utf8_unicode_ci NOT NULL,
  `photo1` varchar(100) collate utf8_unicode_ci NOT NULL,
  `last_edit_time` datetime NOT NULL,
  PRIMARY KEY  (`c_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

-- 
-- Dumping data for table `boattransfer_price_type`
-- 

INSERT INTO `boattransfer_price_type` VALUES (1, 'แบบกำหนดเปอร์เซ็นต์ส่วนลด', '', '2014-05-15 15:23:28');
INSERT INTO `boattransfer_price_type` VALUES (2, 'แบบกำหนดราคาเอง', '', '2014-05-15 15:23:24');

-- --------------------------------------------------------

-- 
-- Table structure for table `boattransfer_ratepercents`
-- 

CREATE TABLE `boattransfer_ratepercents` (
  `botrateper_id` int(11) NOT NULL auto_increment,
  `boattransfers_id` int(11) NOT NULL,
  `agentgrade_id` int(11) NOT NULL,
  `botrateper_discount` int(3) NOT NULL,
  `last_edit_time` datetime NOT NULL,
  PRIMARY KEY  (`botrateper_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

-- 
-- Dumping data for table `boattransfer_ratepercents`
-- 

INSERT INTO `boattransfer_ratepercents` VALUES (1, 3, 1, 99, '2014-08-11 15:56:00');
INSERT INTO `boattransfer_ratepercents` VALUES (2, 3, 2, 88, '2014-08-11 15:56:00');
INSERT INTO `boattransfer_ratepercents` VALUES (3, 3, 3, 77, '2014-08-11 15:56:00');
INSERT INTO `boattransfer_ratepercents` VALUES (4, 3, 4, 66, '2014-08-11 15:56:00');
INSERT INTO `boattransfer_ratepercents` VALUES (5, 3, 5, 55, '2014-08-11 15:56:00');

-- --------------------------------------------------------

-- 
-- Table structure for table `boattransfer_rates`
-- 

CREATE TABLE `boattransfer_rates` (
  `botrate_id` int(11) NOT NULL auto_increment,
  `boattransfers_id` int(11) NOT NULL,
  `agentgrade_id` int(11) NOT NULL,
  `rate_1` double(15,2) NOT NULL,
  `rate_2` double(15,2) NOT NULL,
  `rate_3` double(15,2) NOT NULL,
  `rate_4` double(15,2) NOT NULL,
  `rate_5` double(15,2) NOT NULL,
  `boattransferratetype_id` int(11) NOT NULL,
  `boattransferperiod_id` int(11) NOT NULL,
  `last_edit_time` datetime NOT NULL,
  PRIMARY KEY  (`botrate_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=14 ;

-- 
-- Dumping data for table `boattransfer_rates`
-- 

INSERT INTO `boattransfer_rates` VALUES (1, 2, 0, 1001.00, 1002.00, 1003.00, 0.00, 0.00, 1, 1, '2014-08-11 15:49:43');
INSERT INTO `boattransfer_rates` VALUES (2, 2, 0, 1004.00, 1005.00, 1006.00, 0.00, 0.00, 1, 3, '2014-08-11 15:49:43');
INSERT INTO `boattransfer_rates` VALUES (3, 2, 0, 1007.00, 1008.00, 1009.00, 0.00, 0.00, 2, 1, '2014-08-11 15:49:43');
INSERT INTO `boattransfer_rates` VALUES (4, 2, 0, 1010.00, 1011.00, 1012.00, 0.00, 0.00, 2, 3, '2014-08-11 15:49:43');
INSERT INTO `boattransfer_rates` VALUES (5, 2, 1, 2001.00, 2002.00, 2003.00, 0.00, 0.00, 1, 1, '2014-08-11 15:50:21');
INSERT INTO `boattransfer_rates` VALUES (6, 2, 1, 2004.00, 2005.00, 2006.00, 0.00, 0.00, 1, 3, '2014-08-11 15:50:21');
INSERT INTO `boattransfer_rates` VALUES (7, 2, 1, 2007.00, 2008.00, 2009.00, 0.00, 0.00, 2, 1, '2014-08-11 15:50:21');
INSERT INTO `boattransfer_rates` VALUES (8, 2, 1, 2010.00, 2011.00, 2012.00, 0.00, 0.00, 2, 3, '2014-08-11 15:50:21');
INSERT INTO `boattransfer_rates` VALUES (9, 2, 2, 3001.00, 3002.00, 3003.00, 0.00, 0.00, 1, 1, '2014-08-11 15:50:53');
INSERT INTO `boattransfer_rates` VALUES (10, 2, 2, 3004.00, 3005.00, 3006.00, 0.00, 0.00, 1, 3, '2014-08-11 15:50:53');
INSERT INTO `boattransfer_rates` VALUES (11, 2, 2, 3007.00, 3008.00, 3009.00, 0.00, 0.00, 2, 1, '2014-08-11 15:50:53');
INSERT INTO `boattransfer_rates` VALUES (12, 2, 2, 3010.00, 3011.00, 3012.00, 0.00, 0.00, 2, 3, '2014-08-11 15:50:53');
INSERT INTO `boattransfer_rates` VALUES (13, 3, 0, 5001.00, 5002.00, 5003.00, 0.00, 0.00, 3, 4, '2014-08-11 15:55:48');

-- --------------------------------------------------------

-- 
-- Table structure for table `boattransfer_ratetypes`
-- 

CREATE TABLE `boattransfer_ratetypes` (
  `botrt_id` int(11) NOT NULL auto_increment,
  `boattransfers_id` int(11) NOT NULL,
  `botrt_name` varchar(200) collate utf8_unicode_ci NOT NULL,
  `botrt_detail` text collate utf8_unicode_ci NOT NULL,
  `photo1` varchar(100) collate utf8_unicode_ci NOT NULL,
  `last_edit_time` datetime NOT NULL,
  PRIMARY KEY  (`botrt_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

-- 
-- Dumping data for table `boattransfer_ratetypes`
-- 

INSERT INTO `boattransfer_ratetypes` VALUES (1, 2, 'Boat Rate Type 01', '', '', '2014-08-11 14:56:37');
INSERT INTO `boattransfer_ratetypes` VALUES (2, 2, 'Boat Rate Type 02', '', '', '2014-08-11 14:56:42');
INSERT INTO `boattransfer_ratetypes` VALUES (3, 3, 'Boat Rate Type 03', '', '', '2014-08-11 15:54:09');

-- --------------------------------------------------------

-- 
-- Table structure for table `boattransfer_type`
-- 

CREATE TABLE `boattransfer_type` (
  `c_id` int(11) NOT NULL auto_increment,
  `c_name` varchar(200) collate utf8_unicode_ci NOT NULL,
  `photo1` varchar(100) collate utf8_unicode_ci NOT NULL,
  `last_edit_time` datetime NOT NULL,
  PRIMARY KEY  (`c_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

-- 
-- Dumping data for table `boattransfer_type`
-- 

INSERT INTO `boattransfer_type` VALUES (2, 'Join', '', '2014-05-15 14:52:08');

-- --------------------------------------------------------

-- 
-- Table structure for table `cartransfers`
-- 

CREATE TABLE `cartransfers` (
  `ct_id` int(11) NOT NULL auto_increment,
  `cartransfer_type_id` int(11) NOT NULL,
  `time_id` int(11) NOT NULL,
  `ct_time` varchar(100) collate utf8_unicode_ci NOT NULL,
  `car_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `province_id` int(11) NOT NULL,
  `cartransferpricetype_id` int(11) NOT NULL,
  `last_edit_time` datetime NOT NULL,
  `ct_ref` varchar(100) collate utf8_unicode_ci NOT NULL,
  `agents_sup_id` int(11) NOT NULL,
  `ct_name` varchar(200) collate utf8_unicode_ci NOT NULL,
  `area_id` int(11) NOT NULL,
  `area_to_id` int(11) NOT NULL,
  `ct_hot` int(1) NOT NULL,
  PRIMARY KEY  (`ct_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=12 ;

-- 
-- Dumping data for table `cartransfers`
-- 

INSERT INTO `cartransfers` VALUES (8, 0, 11, 'ประมาณ 4 ชม.', 7, 14, 8, 2, '2014-07-02 14:25:56', 'Bus001', 1, 'test Bus Transfer01', 24, 24, 1);
INSERT INTO `cartransfers` VALUES (9, 0, 14, '4 HOURS', 8, 16, 12, 2, '2014-07-16 16:49:36', 'KBVURT 12.30PM', 1, 'Krabi to Surat Thani (Dep. 12.30PM)', 29, 27, 1);
INSERT INTO `cartransfers` VALUES (10, 2, 13, '4 HOURS', 0, 16, 12, 2, '2014-07-18 16:25:13', 'KBVURT 07.00', 2, 'Krabi to Surat Thani (Dep. 07.00AM)', 29, 27, 0);
INSERT INTO `cartransfers` VALUES (11, 2, 13, '4.5 HOURS', 0, 16, 12, 2, '2014-07-26 14:02:16', 'KBV-URT TRAIN (07.00)', 2, 'Krabi to Surat Thani Train Station (Dep. 07.00AM)', 29, 26, 0);

-- --------------------------------------------------------

-- 
-- Table structure for table `cartransfer_allocation`
-- 

CREATE TABLE `cartransfer_allocation` (
  `catallo_id` int(11) NOT NULL auto_increment,
  `cartransfers_id` int(11) NOT NULL,
  `catallo_datefrom` date NOT NULL,
  `catallo_dateto` date NOT NULL,
  `catallo_global` int(11) NOT NULL,
  `last_edit_time` datetime NOT NULL,
  `cartransferratetype_id` int(11) NOT NULL,
  PRIMARY KEY  (`catallo_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

-- 
-- Dumping data for table `cartransfer_allocation`
-- 

INSERT INTO `cartransfer_allocation` VALUES (1, 1, '2014-01-01', '2014-02-02', 22, '2014-05-23 11:07:09', 4);
INSERT INTO `cartransfer_allocation` VALUES (2, 6, '2014-06-18', '2014-10-31', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocation` VALUES (3, 8, '2014-07-14', '2014-07-31', 50, '2014-07-14 15:53:16', 11);
INSERT INTO `cartransfer_allocation` VALUES (4, 10, '2014-07-17', '2014-07-31', 0, '2014-07-17 11:33:31', 12);

-- --------------------------------------------------------

-- 
-- Table structure for table `cartransfer_allocationdaily`
-- 

CREATE TABLE `cartransfer_allocationdaily` (
  `catalloday_id` int(11) NOT NULL auto_increment,
  `cartransfers_id` int(11) NOT NULL,
  `catalloday_date` date NOT NULL,
  `catalloday_value` int(11) NOT NULL,
  `last_edit_time` datetime NOT NULL,
  `cartransferratetype_id` int(11) NOT NULL,
  PRIMARY KEY  (`catalloday_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=313 ;

-- 
-- Dumping data for table `cartransfer_allocationdaily`
-- 

INSERT INTO `cartransfer_allocationdaily` VALUES (1, 1, '2014-01-01', 22, '2014-05-23 11:07:09', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (2, 1, '2014-01-02', 22, '2014-05-23 11:07:09', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (3, 1, '2014-01-03', 22, '2014-05-23 11:07:09', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (4, 1, '2014-01-04', 22, '2014-05-23 11:07:09', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (5, 1, '2014-01-05', 22, '2014-05-23 11:07:09', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (6, 1, '2014-01-06', 22, '2014-05-23 11:07:09', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (7, 1, '2014-01-07', 22, '2014-05-23 11:07:09', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (8, 1, '2014-01-08', 22, '2014-05-23 11:07:09', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (9, 1, '2014-01-09', 22, '2014-05-23 11:07:09', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (10, 1, '2014-01-10', 22, '2014-05-23 11:07:09', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (11, 1, '2014-01-11', 22, '2014-05-23 11:07:09', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (12, 1, '2014-01-12', 22, '2014-05-23 11:07:09', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (13, 1, '2014-01-13', 22, '2014-05-23 11:07:09', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (14, 1, '2014-01-14', 22, '2014-05-23 11:07:09', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (15, 1, '2014-01-15', 22, '2014-05-23 11:07:09', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (16, 1, '2014-01-16', 22, '2014-05-23 11:07:09', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (17, 1, '2014-01-17', 22, '2014-05-23 11:07:09', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (18, 1, '2014-01-18', 22, '2014-05-23 11:07:09', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (19, 1, '2014-01-19', 22, '2014-05-23 11:07:09', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (20, 1, '2014-01-20', 22, '2014-05-23 11:07:09', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (21, 1, '2014-01-21', 22, '2014-05-23 11:07:09', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (22, 1, '2014-01-22', 22, '2014-05-23 11:07:09', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (23, 1, '2014-01-23', 23, '2014-05-23 14:38:15', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (24, 1, '2014-01-24', 24, '2014-05-23 14:38:15', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (25, 1, '2014-01-25', 25, '2014-05-23 14:38:15', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (26, 1, '2014-01-26', 26, '2014-05-23 14:38:15', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (27, 1, '2014-01-27', 27, '2014-05-23 14:38:15', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (28, 1, '2014-01-28', 288, '2014-05-23 14:38:15', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (29, 1, '2014-01-29', 299, '2014-05-23 14:38:15', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (30, 1, '2014-01-30', 300, '2014-05-23 14:38:15', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (31, 1, '2014-01-31', 311, '2014-05-23 14:38:15', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (32, 1, '2014-02-01', 22, '2014-05-23 14:38:15', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (33, 1, '2014-02-02', 22, '2014-05-23 14:38:15', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (34, 1, '2014-02-03', 0, '2014-05-23 14:38:15', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (35, 1, '2014-02-04', 0, '2014-05-23 14:38:15', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (36, 1, '2014-02-05', 0, '2014-05-23 14:38:15', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (37, 1, '2014-02-06', 0, '2014-05-23 14:38:15', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (38, 1, '2014-02-07', 0, '2014-05-23 14:38:15', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (39, 1, '2014-02-08', 0, '2014-05-23 14:38:15', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (40, 1, '2014-02-09', 0, '2014-05-23 14:38:15', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (41, 1, '2014-02-10', 0, '2014-05-23 14:38:15', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (42, 1, '2014-02-11', 0, '2014-05-23 14:38:15', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (43, 1, '2014-02-12', 0, '2014-05-23 14:38:15', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (44, 1, '2014-02-13', 0, '2014-05-23 14:38:15', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (45, 1, '2014-02-14', 0, '2014-05-23 14:38:15', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (46, 1, '2014-02-15', 0, '2014-05-23 14:38:15', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (47, 1, '2014-02-16', 0, '2014-05-23 14:38:15', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (48, 1, '2014-02-17', 0, '2014-05-23 14:38:15', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (49, 1, '2014-02-18', 0, '2014-05-23 14:38:15', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (50, 1, '2014-02-19', 0, '2014-05-23 14:38:15', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (51, 1, '2014-02-20', 0, '2014-05-23 14:38:15', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (52, 1, '2014-02-21', 0, '2014-05-23 14:38:15', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (53, 1, '2014-02-22', 0, '2014-05-23 14:38:15', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (54, 1, '2014-02-23', 0, '2014-05-23 14:38:15', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (55, 1, '2014-02-24', 0, '2014-05-23 14:38:15', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (56, 1, '2014-02-25', 0, '2014-05-23 14:38:15', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (57, 1, '2014-02-26', 0, '2014-05-23 14:38:15', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (58, 1, '2014-02-27', 0, '2014-05-23 14:38:15', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (59, 1, '2014-02-28', 0, '2014-05-23 14:38:15', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (60, 1, '2014-03-01', 0, '2014-05-23 14:38:15', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (61, 1, '2014-03-02', 0, '2014-05-23 14:38:15', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (62, 1, '2014-03-03', 0, '2014-05-23 14:38:15', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (63, 1, '2014-03-04', 0, '2014-05-23 14:38:15', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (64, 1, '2014-03-05', 0, '2014-05-23 14:38:15', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (65, 1, '2014-03-06', 0, '2014-05-23 14:38:15', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (66, 1, '2014-03-07', 0, '2014-05-23 14:38:15', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (67, 1, '2014-03-08', 0, '2014-05-23 14:38:15', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (68, 1, '2014-03-09', 0, '2014-05-23 14:38:15', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (69, 1, '2014-03-10', 0, '2014-05-23 14:38:15', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (70, 1, '2014-03-11', 0, '2014-05-23 14:38:15', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (71, 1, '2014-03-12', 0, '2014-05-23 14:38:15', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (72, 1, '2014-03-13', 0, '2014-05-23 14:38:15', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (73, 1, '2014-03-14', 0, '2014-05-23 14:38:15', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (74, 1, '2014-03-15', 0, '2014-05-23 14:38:15', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (75, 1, '2014-03-16', 0, '2014-05-23 14:38:15', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (76, 1, '2014-03-17', 0, '2014-05-23 14:38:15', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (77, 1, '2014-03-18', 0, '2014-05-23 14:38:15', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (78, 1, '2014-03-19', 0, '2014-05-23 14:38:15', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (79, 1, '2014-03-20', 0, '2014-05-23 14:38:15', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (80, 1, '2014-03-21', 0, '2014-05-23 14:38:15', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (81, 1, '2014-03-22', 0, '2014-05-23 14:38:15', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (82, 1, '2014-03-23', 0, '2014-05-23 14:38:15', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (83, 1, '2014-03-24', 0, '2014-05-23 14:38:15', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (84, 1, '2014-03-25', 0, '2014-05-23 14:38:15', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (85, 1, '2014-03-26', 0, '2014-05-23 14:38:15', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (86, 1, '2014-03-27', 0, '2014-05-23 14:38:15', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (87, 1, '2014-03-28', 0, '2014-05-23 14:38:15', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (88, 1, '2014-03-29', 0, '2014-05-23 14:38:15', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (89, 1, '2014-03-30', 0, '2014-05-23 14:38:15', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (90, 1, '2014-03-31', 0, '2014-05-23 14:38:15', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (91, 1, '2014-04-01', 0, '2014-05-23 14:38:15', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (92, 1, '2014-04-02', 0, '2014-05-23 14:38:15', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (93, 1, '2014-04-03', 0, '2014-05-23 14:38:15', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (94, 1, '2014-04-04', 0, '2014-05-23 14:38:15', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (95, 1, '2014-04-05', 0, '2014-05-23 14:38:15', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (96, 1, '2014-04-06', 0, '2014-05-23 14:38:15', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (97, 1, '2014-04-07', 0, '2014-05-23 14:38:15', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (98, 1, '2014-04-08', 0, '2014-05-23 14:38:15', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (99, 1, '2014-04-09', 0, '2014-05-23 14:38:15', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (100, 1, '2014-04-10', 0, '2014-05-23 14:38:15', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (101, 1, '2014-04-11', 0, '2014-05-23 14:38:15', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (102, 1, '2014-04-12', 0, '2014-05-23 14:38:15', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (103, 1, '2014-04-13', 0, '2014-05-23 14:38:15', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (104, 1, '2014-04-14', 0, '2014-05-23 14:38:15', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (105, 1, '2014-04-15', 0, '2014-05-23 14:38:15', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (106, 1, '2014-04-16', 0, '2014-05-23 14:38:15', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (107, 1, '2014-04-17', 0, '2014-05-23 14:38:15', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (108, 1, '2014-04-18', 0, '2014-05-23 14:38:15', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (109, 1, '2014-04-19', 0, '2014-05-23 14:38:15', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (110, 1, '2014-04-20', 0, '2014-05-23 14:38:15', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (111, 1, '2014-04-21', 0, '2014-05-23 14:38:15', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (112, 1, '2014-04-22', 0, '2014-05-23 14:38:15', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (113, 1, '2014-04-23', 0, '2014-05-23 14:38:15', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (114, 1, '2014-04-24', 0, '2014-05-23 14:38:15', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (115, 1, '2014-04-25', 0, '2014-05-23 14:38:15', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (116, 1, '2014-04-26', 0, '2014-05-23 14:38:15', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (117, 1, '2014-04-27', 0, '2014-05-23 14:38:15', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (118, 1, '2014-04-28', 0, '2014-05-23 14:38:15', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (119, 1, '2014-04-29', 0, '2014-05-23 14:38:15', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (120, 1, '2014-04-30', 0, '2014-05-23 14:38:15', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (121, 1, '2014-05-01', 0, '2014-05-23 14:38:15', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (122, 1, '2014-05-02', 0, '2014-05-23 14:38:15', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (123, 1, '2014-05-03', 0, '2014-05-23 14:38:15', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (124, 1, '2014-05-04', 0, '2014-05-23 14:38:15', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (125, 1, '2014-05-05', 0, '2014-05-23 14:38:15', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (126, 1, '2014-05-06', 0, '2014-05-23 14:38:15', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (127, 1, '2014-05-07', 0, '2014-05-23 14:38:15', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (128, 1, '2014-05-08', 0, '2014-05-23 14:38:15', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (129, 1, '2014-05-09', 0, '2014-05-23 14:38:15', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (130, 1, '2014-05-10', 0, '2014-05-23 14:38:15', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (131, 1, '2014-05-11', 0, '2014-05-23 14:38:15', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (132, 1, '2014-05-12', 0, '2014-05-23 14:38:15', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (133, 1, '2014-05-13', 0, '2014-05-23 14:38:15', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (134, 1, '2014-05-14', 0, '2014-05-23 14:38:15', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (135, 1, '2014-05-15', 0, '2014-05-23 14:38:15', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (136, 1, '2014-05-16', 0, '2014-05-23 14:38:15', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (137, 1, '2014-05-17', 0, '2014-05-23 14:38:15', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (138, 1, '2014-05-18', 0, '2014-05-23 14:38:15', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (139, 1, '2014-05-19', 0, '2014-05-23 14:38:15', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (140, 1, '2014-05-20', 0, '2014-05-23 14:38:15', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (141, 1, '2014-05-21', 0, '2014-05-23 14:38:15', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (142, 1, '2014-05-22', 0, '2014-05-23 14:38:15', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (143, 1, '2014-05-23', 0, '2014-05-23 14:38:15', 4);
INSERT INTO `cartransfer_allocationdaily` VALUES (144, 6, '2014-06-18', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (145, 6, '2014-06-19', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (146, 6, '2014-06-20', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (147, 6, '2014-06-21', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (148, 6, '2014-06-22', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (149, 6, '2014-06-23', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (150, 6, '2014-06-24', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (151, 6, '2014-06-25', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (152, 6, '2014-06-26', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (153, 6, '2014-06-27', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (154, 6, '2014-06-28', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (155, 6, '2014-06-29', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (156, 6, '2014-06-30', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (157, 6, '2014-07-01', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (158, 6, '2014-07-02', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (159, 6, '2014-07-03', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (160, 6, '2014-07-04', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (161, 6, '2014-07-05', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (162, 6, '2014-07-06', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (163, 6, '2014-07-07', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (164, 6, '2014-07-08', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (165, 6, '2014-07-09', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (166, 6, '2014-07-10', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (167, 6, '2014-07-11', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (168, 6, '2014-07-12', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (169, 6, '2014-07-13', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (170, 6, '2014-07-14', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (171, 6, '2014-07-15', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (172, 6, '2014-07-16', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (173, 6, '2014-07-17', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (174, 6, '2014-07-18', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (175, 6, '2014-07-19', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (176, 6, '2014-07-20', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (177, 6, '2014-07-21', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (178, 6, '2014-07-22', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (179, 6, '2014-07-23', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (180, 6, '2014-07-24', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (181, 6, '2014-07-25', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (182, 6, '2014-07-26', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (183, 6, '2014-07-27', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (184, 6, '2014-07-28', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (185, 6, '2014-07-29', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (186, 6, '2014-07-30', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (187, 6, '2014-07-31', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (188, 6, '2014-08-01', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (189, 6, '2014-08-02', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (190, 6, '2014-08-03', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (191, 6, '2014-08-04', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (192, 6, '2014-08-05', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (193, 6, '2014-08-06', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (194, 6, '2014-08-07', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (195, 6, '2014-08-08', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (196, 6, '2014-08-09', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (197, 6, '2014-08-10', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (198, 6, '2014-08-11', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (199, 6, '2014-08-12', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (200, 6, '2014-08-13', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (201, 6, '2014-08-14', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (202, 6, '2014-08-15', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (203, 6, '2014-08-16', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (204, 6, '2014-08-17', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (205, 6, '2014-08-18', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (206, 6, '2014-08-19', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (207, 6, '2014-08-20', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (208, 6, '2014-08-21', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (209, 6, '2014-08-22', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (210, 6, '2014-08-23', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (211, 6, '2014-08-24', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (212, 6, '2014-08-25', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (213, 6, '2014-08-26', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (214, 6, '2014-08-27', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (215, 6, '2014-08-28', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (216, 6, '2014-08-29', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (217, 6, '2014-08-30', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (218, 6, '2014-08-31', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (219, 6, '2014-09-01', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (220, 6, '2014-09-02', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (221, 6, '2014-09-03', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (222, 6, '2014-09-04', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (223, 6, '2014-09-05', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (224, 6, '2014-09-06', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (225, 6, '2014-09-07', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (226, 6, '2014-09-08', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (227, 6, '2014-09-09', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (228, 6, '2014-09-10', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (229, 6, '2014-09-11', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (230, 6, '2014-09-12', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (231, 6, '2014-09-13', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (232, 6, '2014-09-14', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (233, 6, '2014-09-15', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (234, 6, '2014-09-16', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (235, 6, '2014-09-17', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (236, 6, '2014-09-18', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (237, 6, '2014-09-19', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (238, 6, '2014-09-20', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (239, 6, '2014-09-21', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (240, 6, '2014-09-22', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (241, 6, '2014-09-23', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (242, 6, '2014-09-24', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (243, 6, '2014-09-25', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (244, 6, '2014-09-26', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (245, 6, '2014-09-27', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (246, 6, '2014-09-28', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (247, 6, '2014-09-29', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (248, 6, '2014-09-30', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (249, 6, '2014-10-01', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (250, 6, '2014-10-02', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (251, 6, '2014-10-03', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (252, 6, '2014-10-04', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (253, 6, '2014-10-05', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (254, 6, '2014-10-06', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (255, 6, '2014-10-07', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (256, 6, '2014-10-08', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (257, 6, '2014-10-09', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (258, 6, '2014-10-10', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (259, 6, '2014-10-11', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (260, 6, '2014-10-12', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (261, 6, '2014-10-13', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (262, 6, '2014-10-14', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (263, 6, '2014-10-15', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (264, 6, '2014-10-16', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (265, 6, '2014-10-17', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (266, 6, '2014-10-18', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (267, 6, '2014-10-19', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (268, 6, '2014-10-20', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (269, 6, '2014-10-21', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (270, 6, '2014-10-22', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (271, 6, '2014-10-23', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (272, 6, '2014-10-24', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (273, 6, '2014-10-25', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (274, 6, '2014-10-26', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (275, 6, '2014-10-27', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (276, 6, '2014-10-28', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (277, 6, '2014-10-29', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (278, 6, '2014-10-30', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (279, 6, '2014-10-31', 100, '2014-06-18 09:43:56', 6);
INSERT INTO `cartransfer_allocationdaily` VALUES (280, 8, '2014-07-14', 50, '2014-07-14 15:53:16', 11);
INSERT INTO `cartransfer_allocationdaily` VALUES (281, 8, '2014-07-15', 50, '2014-07-14 15:53:16', 11);
INSERT INTO `cartransfer_allocationdaily` VALUES (282, 8, '2014-07-16', 50, '2014-07-14 15:53:16', 11);
INSERT INTO `cartransfer_allocationdaily` VALUES (283, 8, '2014-07-17', 50, '2014-07-14 15:53:16', 11);
INSERT INTO `cartransfer_allocationdaily` VALUES (284, 8, '2014-07-18', 50, '2014-07-14 15:53:16', 11);
INSERT INTO `cartransfer_allocationdaily` VALUES (285, 8, '2014-07-19', 50, '2014-07-14 15:53:16', 11);
INSERT INTO `cartransfer_allocationdaily` VALUES (286, 8, '2014-07-20', 50, '2014-07-14 15:53:16', 11);
INSERT INTO `cartransfer_allocationdaily` VALUES (287, 8, '2014-07-21', 50, '2014-07-14 15:53:16', 11);
INSERT INTO `cartransfer_allocationdaily` VALUES (288, 8, '2014-07-22', 50, '2014-07-14 15:53:16', 11);
INSERT INTO `cartransfer_allocationdaily` VALUES (289, 8, '2014-07-23', 50, '2014-07-14 15:53:16', 11);
INSERT INTO `cartransfer_allocationdaily` VALUES (290, 8, '2014-07-24', 50, '2014-07-14 15:53:16', 11);
INSERT INTO `cartransfer_allocationdaily` VALUES (291, 8, '2014-07-25', 50, '2014-07-14 15:53:16', 11);
INSERT INTO `cartransfer_allocationdaily` VALUES (292, 8, '2014-07-26', 50, '2014-07-14 15:53:16', 11);
INSERT INTO `cartransfer_allocationdaily` VALUES (293, 8, '2014-07-27', 50, '2014-07-14 15:53:16', 11);
INSERT INTO `cartransfer_allocationdaily` VALUES (294, 8, '2014-07-28', 50, '2014-07-14 15:53:16', 11);
INSERT INTO `cartransfer_allocationdaily` VALUES (295, 8, '2014-07-29', 50, '2014-07-14 15:53:16', 11);
INSERT INTO `cartransfer_allocationdaily` VALUES (296, 8, '2014-07-30', 50, '2014-07-14 15:53:16', 11);
INSERT INTO `cartransfer_allocationdaily` VALUES (297, 8, '2014-07-31', 50, '2014-07-14 15:53:16', 11);
INSERT INTO `cartransfer_allocationdaily` VALUES (298, 10, '2014-07-17', 10, '2014-07-17 11:34:57', 12);
INSERT INTO `cartransfer_allocationdaily` VALUES (299, 10, '2014-07-18', 10, '2014-07-17 11:34:57', 12);
INSERT INTO `cartransfer_allocationdaily` VALUES (300, 10, '2014-07-19', 10, '2014-07-17 11:34:57', 12);
INSERT INTO `cartransfer_allocationdaily` VALUES (301, 10, '2014-07-20', 10, '2014-07-17 11:34:57', 12);
INSERT INTO `cartransfer_allocationdaily` VALUES (302, 10, '2014-07-21', 10, '2014-07-17 11:34:57', 12);
INSERT INTO `cartransfer_allocationdaily` VALUES (303, 10, '2014-07-22', 10, '2014-07-17 11:34:57', 12);
INSERT INTO `cartransfer_allocationdaily` VALUES (304, 10, '2014-07-23', 10, '2014-07-17 11:34:57', 12);
INSERT INTO `cartransfer_allocationdaily` VALUES (305, 10, '2014-07-24', 10, '2014-07-17 11:34:57', 12);
INSERT INTO `cartransfer_allocationdaily` VALUES (306, 10, '2014-07-25', 10, '2014-07-17 11:34:57', 12);
INSERT INTO `cartransfer_allocationdaily` VALUES (307, 10, '2014-07-26', 10, '2014-07-17 11:34:57', 12);
INSERT INTO `cartransfer_allocationdaily` VALUES (308, 10, '2014-07-27', 10, '2014-07-17 11:34:57', 12);
INSERT INTO `cartransfer_allocationdaily` VALUES (309, 10, '2014-07-28', 10, '2014-07-17 11:34:57', 12);
INSERT INTO `cartransfer_allocationdaily` VALUES (310, 10, '2014-07-29', 10, '2014-07-17 11:34:57', 12);
INSERT INTO `cartransfer_allocationdaily` VALUES (311, 10, '2014-07-30', 10, '2014-07-17 11:34:57', 12);
INSERT INTO `cartransfer_allocationdaily` VALUES (312, 10, '2014-07-31', 10, '2014-07-17 11:34:57', 12);

-- --------------------------------------------------------

-- 
-- Table structure for table `cartransfer_con_area`
-- 

CREATE TABLE `cartransfer_con_area` (
  `con_id` int(3) unsigned zerofill NOT NULL auto_increment,
  `con_name` varchar(200) collate utf8_unicode_ci NOT NULL,
  `photo1` varchar(100) collate utf8_unicode_ci NOT NULL,
  `last_edit_time` datetime NOT NULL,
  `province_id` int(11) NOT NULL,
  PRIMARY KEY  (`con_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=32 ;

-- 
-- Dumping data for table `cartransfer_con_area`
-- 

INSERT INTO `cartransfer_con_area` VALUES (022, 'Bus Transfer Area 01', '', '2014-07-02 12:17:21', 7);
INSERT INTO `cartransfer_con_area` VALUES (023, 'Bus Transfer Area 02', '', '2014-07-02 12:17:30', 7);
INSERT INTO `cartransfer_con_area` VALUES (024, 'Bus Transfer Area 03', '', '2014-07-02 12:17:41', 8);
INSERT INTO `cartransfer_con_area` VALUES (025, 'Bus Transfer Area 04', '', '2014-07-02 12:17:47', 9);
INSERT INTO `cartransfer_con_area` VALUES (026, 'Surat Thani Train Station', '', '2014-07-16 16:34:01', 11);
INSERT INTO `cartransfer_con_area` VALUES (027, 'Surat Thani Town', '', '2014-07-16 16:39:27', 11);
INSERT INTO `cartransfer_con_area` VALUES (028, 'Phuket Town', '', '2014-07-17 10:47:30', 13);
INSERT INTO `cartransfer_con_area` VALUES (029, 'Krabi Town', '', '2014-07-16 16:43:32', 12);
INSERT INTO `cartransfer_con_area` VALUES (030, 'Don Sak Pier (Songserm Pier)', '', '2014-07-17 10:47:53', 11);
INSERT INTO `cartransfer_con_area` VALUES (031, 'Ao Nang Beach', '', '2014-07-17 10:48:21', 12);

-- --------------------------------------------------------

-- 
-- Table structure for table `cartransfer_con_car`
-- 

CREATE TABLE `cartransfer_con_car` (
  `con_id` int(3) unsigned zerofill NOT NULL auto_increment,
  `con_name` varchar(200) collate utf8_unicode_ci NOT NULL,
  `photo1` varchar(100) collate utf8_unicode_ci NOT NULL,
  `last_edit_time` datetime NOT NULL,
  `car_registration` varchar(100) collate utf8_unicode_ci NOT NULL,
  `car_seat_num` int(5) NOT NULL,
  `car_brand` varchar(100) collate utf8_unicode_ci NOT NULL,
  `car_model` varchar(100) collate utf8_unicode_ci NOT NULL,
  `car_driver_name` varchar(200) collate utf8_unicode_ci NOT NULL,
  `car_driver_tel` varchar(100) collate utf8_unicode_ci NOT NULL,
  `car_driver_license` text collate utf8_unicode_ci NOT NULL,
  PRIMARY KEY  (`con_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=9 ;

-- 
-- Dumping data for table `cartransfer_con_car`
-- 

INSERT INTO `cartransfer_con_car` VALUES (008, '', '', '2014-07-02 12:21:48', 'BB-0002', 30, 'Toyota', '', 'TestDriver02', '081-0000002', 'ข้อมูลใบขับขี่  02');
INSERT INTO `cartransfer_con_car` VALUES (007, '', '', '2014-07-02 12:21:55', 'BB-0001', 35, 'Toyota', '', 'TestDriver01', '081-0000001', 'ข้อมูลใบขับขี่ 01');

-- --------------------------------------------------------

-- 
-- Table structure for table `cartransfer_con_country`
-- 

CREATE TABLE `cartransfer_con_country` (
  `con_id` int(3) unsigned zerofill NOT NULL auto_increment,
  `con_name` varchar(200) collate utf8_unicode_ci NOT NULL,
  `photo1` varchar(100) collate utf8_unicode_ci NOT NULL,
  `last_edit_time` datetime NOT NULL,
  PRIMARY KEY  (`con_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=17 ;

-- 
-- Dumping data for table `cartransfer_con_country`
-- 

INSERT INTO `cartransfer_con_country` VALUES (015, 'test Bus Transfer Country 02', '', '2014-07-02 12:12:22');
INSERT INTO `cartransfer_con_country` VALUES (014, 'test Bus Transfer Country 01', '', '2014-07-02 12:11:50');
INSERT INTO `cartransfer_con_country` VALUES (016, 'Thailand', '', '2014-07-16 15:43:05');

-- --------------------------------------------------------

-- 
-- Table structure for table `cartransfer_con_destination`
-- 

CREATE TABLE `cartransfer_con_destination` (
  `con_id` int(3) unsigned zerofill NOT NULL auto_increment,
  `con_name` varchar(200) collate utf8_unicode_ci NOT NULL,
  `photo1` varchar(100) collate utf8_unicode_ci NOT NULL,
  `last_edit_time` datetime NOT NULL,
  `country_id` int(11) NOT NULL,
  `province_id` int(11) NOT NULL,
  `area_id` int(11) NOT NULL,
  PRIMARY KEY  (`con_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=20 ;

-- 
-- Dumping data for table `cartransfer_con_destination`
-- 

INSERT INTO `cartransfer_con_destination` VALUES (017, 'Bus Transfer Destination 03', '', '2014-07-02 12:19:23', 14, 8, 24);
INSERT INTO `cartransfer_con_destination` VALUES (018, 'Bus Transfer Destination 04', '', '2014-07-02 12:19:39', 14, 8, 24);
INSERT INTO `cartransfer_con_destination` VALUES (015, 'Bus Transfer Destination 01', '', '2014-07-02 12:19:05', 14, 7, 22);
INSERT INTO `cartransfer_con_destination` VALUES (016, 'Bus Transfer Destination 02', '', '2014-07-02 12:19:14', 14, 7, 22);

-- --------------------------------------------------------

-- 
-- Table structure for table `cartransfer_con_province`
-- 

CREATE TABLE `cartransfer_con_province` (
  `con_id` int(3) unsigned zerofill NOT NULL auto_increment,
  `con_name` varchar(200) collate utf8_unicode_ci NOT NULL,
  `photo1` varchar(100) collate utf8_unicode_ci NOT NULL,
  `last_edit_time` datetime NOT NULL,
  `country_id` int(11) NOT NULL,
  PRIMARY KEY  (`con_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=15 ;

-- 
-- Dumping data for table `cartransfer_con_province`
-- 

INSERT INTO `cartransfer_con_province` VALUES (009, 'test Bus Transfer Province 03', '', '2014-07-02 12:13:12', 15);
INSERT INTO `cartransfer_con_province` VALUES (007, 'test Bus Transfer Province 01', '', '2014-07-02 12:12:51', 14);
INSERT INTO `cartransfer_con_province` VALUES (008, 'test Bus Transfer Province 02', '', '2014-07-02 12:13:08', 14);
INSERT INTO `cartransfer_con_province` VALUES (010, 'test Bus Transfer Province 04', '', '2014-07-02 12:13:20', 15);
INSERT INTO `cartransfer_con_province` VALUES (011, 'Surat Thani', '', '2014-07-16 16:14:16', 16);
INSERT INTO `cartransfer_con_province` VALUES (012, 'Krabi', '', '2014-07-16 16:14:32', 16);
INSERT INTO `cartransfer_con_province` VALUES (013, 'Phuket', '', '2014-07-16 16:17:58', 16);
INSERT INTO `cartransfer_con_province` VALUES (014, 'Bangkok', '', '2014-07-16 16:31:53', 16);

-- --------------------------------------------------------

-- 
-- Table structure for table `cartransfer_con_time`
-- 

CREATE TABLE `cartransfer_con_time` (
  `con_id` int(3) unsigned zerofill NOT NULL auto_increment,
  `con_name` varchar(100) collate utf8_unicode_ci NOT NULL,
  `photo1` varchar(100) collate utf8_unicode_ci NOT NULL,
  `last_edit_time` datetime NOT NULL,
  PRIMARY KEY  (`con_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=15 ;

-- 
-- Dumping data for table `cartransfer_con_time`
-- 

INSERT INTO `cartransfer_con_time` VALUES (010, '08:00', '', '2014-07-02 12:20:04');
INSERT INTO `cartransfer_con_time` VALUES (011, '10:00', '', '2014-07-02 12:20:09');
INSERT INTO `cartransfer_con_time` VALUES (012, '11:00', '', '2014-07-02 12:20:14');
INSERT INTO `cartransfer_con_time` VALUES (013, '07:00', '', '2014-07-16 16:47:11');
INSERT INTO `cartransfer_con_time` VALUES (014, '12:30', '', '2014-07-16 16:47:29');

-- --------------------------------------------------------

-- 
-- Table structure for table `cartransfer_period`
-- 

CREATE TABLE `cartransfer_period` (
  `catpe_id` int(11) NOT NULL auto_increment,
  `cartransfers_id` int(11) NOT NULL,
  `catpe_name` varchar(200) collate utf8_unicode_ci NOT NULL,
  `catpe_datefrom` date NOT NULL,
  `catpe_dateto` date NOT NULL,
  `arrange` int(11) NOT NULL,
  `last_edit_time` datetime NOT NULL,
  PRIMARY KEY  (`catpe_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=8 ;

-- 
-- Dumping data for table `cartransfer_period`
-- 

INSERT INTO `cartransfer_period` VALUES (1, 1, 'Season_1', '2014-01-01', '2014-07-31', 0, '2014-05-23 10:14:27');
INSERT INTO `cartransfer_period` VALUES (2, 1, 'Season_2', '2014-08-01', '2014-12-31', 0, '2014-05-23 10:14:31');
INSERT INTO `cartransfer_period` VALUES (3, 2, 'Season 1', '2014-01-01', '2014-12-31', 0, '2014-05-23 10:51:15');
INSERT INTO `cartransfer_period` VALUES (4, 6, 'NOW TILL 31 OCT ''14', '2014-06-18', '2014-10-31', 0, '2014-06-18 09:42:26');
INSERT INTO `cartransfer_period` VALUES (5, 4, 'NOW TILL 31 OCT ''14', '2014-06-18', '2014-10-31', 0, '2014-06-18 10:31:23');
INSERT INTO `cartransfer_period` VALUES (6, 8, 'Test Period July ''14', '2014-07-14', '2014-07-31', 0, '2014-07-14 15:52:31');
INSERT INTO `cartransfer_period` VALUES (7, 10, '', '2014-07-17', '2014-10-31', 0, '2014-07-17 11:30:06');

-- --------------------------------------------------------

-- 
-- Table structure for table `cartransfer_price_type`
-- 

CREATE TABLE `cartransfer_price_type` (
  `c_id` int(11) NOT NULL auto_increment,
  `c_name` varchar(200) collate utf8_unicode_ci NOT NULL,
  `photo1` varchar(100) collate utf8_unicode_ci NOT NULL,
  `last_edit_time` datetime NOT NULL,
  PRIMARY KEY  (`c_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

-- 
-- Dumping data for table `cartransfer_price_type`
-- 

INSERT INTO `cartransfer_price_type` VALUES (1, 'แบบกำหนดเปอร์เซ็นต์ส่วนลด', '', '2014-05-15 15:23:28');
INSERT INTO `cartransfer_price_type` VALUES (2, 'แบบกำหนดราคาเอง', '', '2014-05-15 15:23:24');

-- --------------------------------------------------------

-- 
-- Table structure for table `cartransfer_ratepercents`
-- 

CREATE TABLE `cartransfer_ratepercents` (
  `catrateper_id` int(11) NOT NULL auto_increment,
  `cartransfers_id` int(11) NOT NULL,
  `agentgrade_id` int(11) NOT NULL,
  `catrateper_discount` int(3) NOT NULL,
  `last_edit_time` datetime NOT NULL,
  PRIMARY KEY  (`catrateper_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=11 ;

-- 
-- Dumping data for table `cartransfer_ratepercents`
-- 

INSERT INTO `cartransfer_ratepercents` VALUES (1, 2, 1, 90, '2014-05-23 10:56:46');
INSERT INTO `cartransfer_ratepercents` VALUES (2, 2, 2, 80, '2014-05-23 10:56:46');
INSERT INTO `cartransfer_ratepercents` VALUES (3, 2, 3, 70, '2014-05-23 10:56:46');
INSERT INTO `cartransfer_ratepercents` VALUES (4, 2, 4, 60, '2014-05-23 10:56:46');
INSERT INTO `cartransfer_ratepercents` VALUES (5, 2, 5, 55, '2014-05-23 10:56:46');
INSERT INTO `cartransfer_ratepercents` VALUES (6, 6, 1, 0, '2014-06-18 09:43:19');
INSERT INTO `cartransfer_ratepercents` VALUES (7, 6, 2, 0, '2014-06-18 09:43:19');
INSERT INTO `cartransfer_ratepercents` VALUES (8, 6, 3, 0, '2014-06-18 09:43:19');
INSERT INTO `cartransfer_ratepercents` VALUES (9, 6, 4, 0, '2014-06-18 09:43:19');
INSERT INTO `cartransfer_ratepercents` VALUES (10, 6, 5, 0, '2014-06-18 09:43:19');

-- --------------------------------------------------------

-- 
-- Table structure for table `cartransfer_rates`
-- 

CREATE TABLE `cartransfer_rates` (
  `catrate_id` int(11) NOT NULL auto_increment,
  `cartransfers_id` int(11) NOT NULL,
  `agentgrade_id` int(11) NOT NULL,
  `rate_1` double(15,2) NOT NULL,
  `rate_2` double(15,2) NOT NULL,
  `rate_3` double(15,2) NOT NULL,
  `rate_4` double(15,2) NOT NULL,
  `rate_5` double(15,2) NOT NULL,
  `last_edit_time` datetime NOT NULL,
  `cartransferratetype_id` int(11) NOT NULL,
  `cartransferperiod_id` int(11) NOT NULL,
  PRIMARY KEY  (`catrate_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=18 ;

-- 
-- Dumping data for table `cartransfer_rates`
-- 

INSERT INTO `cartransfer_rates` VALUES (1, 1, 0, 1000.00, 1001.00, 1002.00, 0.00, 0.00, '2014-05-23 10:27:58', 3, 1);
INSERT INTO `cartransfer_rates` VALUES (2, 1, 0, 2000.00, 2001.00, 2002.00, 0.00, 0.00, '2014-05-23 10:27:58', 3, 2);
INSERT INTO `cartransfer_rates` VALUES (3, 1, 0, 3000.00, 3001.00, 3002.00, 0.00, 0.00, '2014-05-23 10:27:58', 4, 1);
INSERT INTO `cartransfer_rates` VALUES (4, 1, 0, 4000.00, 4001.00, 4002.00, 0.00, 0.00, '2014-05-23 10:27:58', 4, 2);
INSERT INTO `cartransfer_rates` VALUES (5, 1, 1, 1010.00, 1011.00, 1012.00, 0.00, 0.00, '2014-05-23 10:29:21', 3, 1);
INSERT INTO `cartransfer_rates` VALUES (6, 1, 1, 2010.00, 2011.00, 2012.00, 0.00, 0.00, '2014-05-23 10:29:21', 3, 2);
INSERT INTO `cartransfer_rates` VALUES (7, 1, 1, 3010.00, 3011.00, 3012.00, 0.00, 0.00, '2014-05-23 10:29:21', 4, 1);
INSERT INTO `cartransfer_rates` VALUES (8, 1, 1, 4010.00, 4011.00, 4012.00, 0.00, 0.00, '2014-05-23 10:29:21', 4, 2);
INSERT INTO `cartransfer_rates` VALUES (9, 1, 2, 1020.00, 1021.00, 1022.00, 0.00, 0.00, '2014-05-23 10:38:13', 3, 1);
INSERT INTO `cartransfer_rates` VALUES (10, 1, 2, 2020.00, 2021.00, 2022.00, 0.00, 0.00, '2014-05-23 10:38:13', 3, 2);
INSERT INTO `cartransfer_rates` VALUES (11, 1, 2, 3020.00, 3021.00, 3022.00, 0.00, 0.00, '2014-05-23 10:38:13', 4, 1);
INSERT INTO `cartransfer_rates` VALUES (12, 1, 2, 4020.00, 4021.00, 4022.00, 0.00, 0.00, '2014-05-23 10:38:13', 4, 2);
INSERT INTO `cartransfer_rates` VALUES (13, 2, 0, 8000.00, 7000.00, 6000.00, 0.00, 0.00, '2014-05-23 10:51:32', 5, 3);
INSERT INTO `cartransfer_rates` VALUES (14, 6, 0, 0.00, 0.00, 0.00, 0.00, 0.00, '2014-06-18 09:42:59', 6, 4);
INSERT INTO `cartransfer_rates` VALUES (15, 8, 0, 500.00, 500.00, 0.00, 0.00, 0.00, '2014-07-14 15:52:53', 11, 6);
INSERT INTO `cartransfer_rates` VALUES (16, 10, 0, 400.00, 400.00, 0.00, 0.00, 0.00, '2014-07-17 11:33:16', 12, 7);
INSERT INTO `cartransfer_rates` VALUES (17, 10, 1, 300.00, 300.00, 0.00, 0.00, 0.00, '2014-07-17 11:35:29', 12, 7);

-- --------------------------------------------------------

-- 
-- Table structure for table `cartransfer_ratetypes`
-- 

CREATE TABLE `cartransfer_ratetypes` (
  `catrt_id` int(11) NOT NULL auto_increment,
  `cartransfers_id` int(11) NOT NULL,
  `catrt_name` varchar(200) collate utf8_unicode_ci NOT NULL,
  `catrt_detail` text collate utf8_unicode_ci NOT NULL,
  `photo1` varchar(100) collate utf8_unicode_ci NOT NULL,
  `last_edit_time` datetime NOT NULL,
  PRIMARY KEY  (`catrt_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=13 ;

-- 
-- Dumping data for table `cartransfer_ratetypes`
-- 

INSERT INTO `cartransfer_ratetypes` VALUES (1, 3, 'Rate Type01', '', '', '2014-05-22 16:52:54');
INSERT INTO `cartransfer_ratetypes` VALUES (2, 3, 'Rate Type02', '', '', '2014-05-22 16:52:41');
INSERT INTO `cartransfer_ratetypes` VALUES (3, 1, 'Rate Type01', '', '', '2014-05-22 17:05:30');
INSERT INTO `cartransfer_ratetypes` VALUES (4, 1, 'Rate Type02', '', '', '2014-05-22 17:05:31');
INSERT INTO `cartransfer_ratetypes` VALUES (5, 2, 'Rate Type 1', '', '', '2014-05-23 10:50:54');
INSERT INTO `cartransfer_ratetypes` VALUES (6, 6, 'JOIN PICK KATA-PIER', '', '', '2014-06-18 09:41:40');
INSERT INTO `cartransfer_ratetypes` VALUES (7, 4, 'APT-PTG (PVT) 07.00 - 19.00', '', '', '2014-06-18 10:30:16');
INSERT INTO `cartransfer_ratetypes` VALUES (8, 4, 'APT-PTG (PVT) 20.00 - 24.00', '', '', '2014-06-18 10:30:34');
INSERT INTO `cartransfer_ratetypes` VALUES (9, 4, 'APT-PTG (PVT) 24.00 - 06.00', '', '', '2014-06-18 10:30:58');
INSERT INTO `cartransfer_ratetypes` VALUES (10, 7, '800', '', '', '2014-06-18 12:57:17');
INSERT INTO `cartransfer_ratetypes` VALUES (11, 8, 'Krabi to Surat Thani', '', '', '2014-07-14 15:51:47');
INSERT INTO `cartransfer_ratetypes` VALUES (12, 10, 'Krabi to Surat Thani Town', '', '', '2014-07-17 11:29:51');

-- --------------------------------------------------------

-- 
-- Table structure for table `cartransfer_type`
-- 

CREATE TABLE `cartransfer_type` (
  `c_id` int(11) NOT NULL auto_increment,
  `c_name` varchar(200) collate utf8_unicode_ci NOT NULL,
  `photo1` varchar(100) collate utf8_unicode_ci NOT NULL,
  `last_edit_time` datetime NOT NULL,
  PRIMARY KEY  (`c_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

-- 
-- Dumping data for table `cartransfer_type`
-- 

INSERT INTO `cartransfer_type` VALUES (2, 'Join', '', '2014-05-15 14:52:08');

-- --------------------------------------------------------

-- 
-- Table structure for table `clients`
-- 

CREATE TABLE `clients` (
  `cl_id` int(5) unsigned zerofill NOT NULL auto_increment,
  `cl_name` varchar(200) collate utf8_unicode_ci NOT NULL,
  `cl_lname` varchar(200) collate utf8_unicode_ci NOT NULL,
  `cl_nationality_id` int(11) NOT NULL,
  `cl_category` varchar(100) collate utf8_unicode_ci NOT NULL,
  `cl_passport` varchar(200) collate utf8_unicode_ci NOT NULL,
  `cl_passportuntil_date` date NOT NULL,
  `cl_addr1` text collate utf8_unicode_ci NOT NULL,
  `cl_addr2` text collate utf8_unicode_ci NOT NULL,
  `country_id` int(11) NOT NULL,
  `province_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `cl_zipcode` varchar(100) collate utf8_unicode_ci NOT NULL,
  `cl_phone` varchar(100) collate utf8_unicode_ci NOT NULL,
  `cl_mobile` varchar(100) collate utf8_unicode_ci NOT NULL,
  `cl_occup` varchar(200) collate utf8_unicode_ci NOT NULL,
  `cl_busiphone` varchar(100) collate utf8_unicode_ci NOT NULL,
  `cl_emerg_num` varchar(100) collate utf8_unicode_ci NOT NULL,
  `cl_fax` varchar(100) collate utf8_unicode_ci NOT NULL,
  `cl_email` varchar(100) collate utf8_unicode_ci NOT NULL,
  `cl_password` varchar(100) collate utf8_unicode_ci NOT NULL,
  `cl_birth_date` date NOT NULL,
  `cl_age` varchar(100) collate utf8_unicode_ci NOT NULL,
  `last_edit_time` datetime NOT NULL,
  `category_id` int(11) NOT NULL,
  `cl_create_date` date NOT NULL,
  PRIMARY KEY  (`cl_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

-- 
-- Dumping data for table `clients`
-- 

INSERT INTO `clients` VALUES (00001, 'TFClientname', 'TLClientname', 211, '', 'ID Card  test', '2014-01-01', 'ที่อยู่ Client 1 test', 'ที่อยู่ Client 2 test', 12, 3, 9, 'Zip Codetest test', '025555555', 'Mobile test', 'Occupation test', 'Business test', 'Emergency test', 'Fax test', 'client01@mail.com', '4d157573', '2010-07-06', 'Age test', '2014-05-26 15:26:50', 2, '2014-01-09');

-- --------------------------------------------------------

-- 
-- Table structure for table `client_con_category`
-- 

CREATE TABLE `client_con_category` (
  `con_id` int(3) unsigned zerofill NOT NULL auto_increment,
  `con_name` varchar(200) collate utf8_unicode_ci NOT NULL,
  `photo1` varchar(100) collate utf8_unicode_ci NOT NULL,
  `last_edit_time` datetime NOT NULL,
  PRIMARY KEY  (`con_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=9 ;

-- 
-- Dumping data for table `client_con_category`
-- 

INSERT INTO `client_con_category` VALUES (001, 'Test Client Category 01', '', '2014-05-08 11:47:47');
INSERT INTO `client_con_category` VALUES (002, 'Test Client Category 02', '', '2014-05-08 11:48:11');
INSERT INTO `client_con_category` VALUES (003, 'Test Client Category 03', '', '2014-05-08 11:48:06');
INSERT INTO `client_con_category` VALUES (004, 'WALK-IN (ONLINE)', '', '2014-07-26 15:53:06');
INSERT INTO `client_con_category` VALUES (005, 'WALK-IN', '', '2014-07-26 15:45:18');
INSERT INTO `client_con_category` VALUES (006, 'V.I.P.', '', '2014-07-26 15:45:27');
INSERT INTO `client_con_category` VALUES (007, 'REPEATER', '', '2014-07-26 15:45:37');

-- --------------------------------------------------------

-- 
-- Table structure for table `client_con_city`
-- 

CREATE TABLE `client_con_city` (
  `con_id` int(3) unsigned zerofill NOT NULL auto_increment,
  `con_name` varchar(200) collate utf8_unicode_ci NOT NULL,
  `photo1` varchar(100) collate utf8_unicode_ci NOT NULL,
  `last_edit_time` datetime NOT NULL,
  `province_id` int(11) NOT NULL,
  PRIMARY KEY  (`con_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=12 ;

-- 
-- Dumping data for table `client_con_city`
-- 

INSERT INTO `client_con_city` VALUES (007, 'Kata', '', '2014-04-03 16:50:49', 1);
INSERT INTO `client_con_city` VALUES (008, 'Patong', '', '2014-04-03 16:50:45', 1);
INSERT INTO `client_con_city` VALUES (009, 'City_Client', '', '2014-05-02 16:37:25', 3);
INSERT INTO `client_con_city` VALUES (010, 'Phuket Town', '', '2014-07-26 14:07:04', 1);
INSERT INTO `client_con_city` VALUES (011, 'Kamala', '', '2014-07-26 14:07:31', 1);

-- --------------------------------------------------------

-- 
-- Table structure for table `client_con_country`
-- 

CREATE TABLE `client_con_country` (
  `con_id` int(3) unsigned zerofill NOT NULL auto_increment,
  `con_name` varchar(200) collate utf8_unicode_ci NOT NULL,
  `photo1` varchar(100) collate utf8_unicode_ci NOT NULL,
  `last_edit_time` datetime NOT NULL,
  PRIMARY KEY  (`con_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=144 ;

-- 
-- Dumping data for table `client_con_country`
-- 

INSERT INTO `client_con_country` VALUES (001, 'Thailand', '', '2014-04-03 15:58:12');
INSERT INTO `client_con_country` VALUES (011, 'Vietnam', '', '2014-04-22 10:35:14');
INSERT INTO `client_con_country` VALUES (012, 'Country_Client', '', '2014-05-02 16:26:13');
INSERT INTO `client_con_country` VALUES (013, 'Australia', '', '2014-07-26 14:10:13');
INSERT INTO `client_con_country` VALUES (014, 'China', '', '2014-07-26 14:10:27');
INSERT INTO `client_con_country` VALUES (015, 'Singapore', '', '2014-07-26 14:10:34');
INSERT INTO `client_con_country` VALUES (016, 'Taiwan', '', '2014-07-26 14:10:43');
INSERT INTO `client_con_country` VALUES (017, 'Japan', '', '2014-07-26 14:10:52');
INSERT INTO `client_con_country` VALUES (018, 'India', '', '2014-07-26 14:10:59');
INSERT INTO `client_con_country` VALUES (019, 'Indonesia', '', '2014-07-26 14:11:06');
INSERT INTO `client_con_country` VALUES (020, 'Malaysia', '', '2014-07-26 14:11:13');
INSERT INTO `client_con_country` VALUES (021, 'Pakistan', '', '2014-07-26 14:11:20');
INSERT INTO `client_con_country` VALUES (022, 'Korea', '', '2014-07-26 14:11:26');
INSERT INTO `client_con_country` VALUES (023, 'Russian Federation', '', '2014-07-26 14:41:28');
INSERT INTO `client_con_country` VALUES (024, 'France', '', '2014-07-26 14:12:16');
INSERT INTO `client_con_country` VALUES (025, 'Germany', '', '2014-07-26 14:12:24');
INSERT INTO `client_con_country` VALUES (027, 'Argentina', '', '2014-07-26 14:24:23');
INSERT INTO `client_con_country` VALUES (028, 'Austria', '', '2014-07-26 14:24:45');
INSERT INTO `client_con_country` VALUES (029, 'Bangladesh', '', '2014-07-26 14:24:59');
INSERT INTO `client_con_country` VALUES (030, 'Belgium', '', '2014-07-26 14:25:40');
INSERT INTO `client_con_country` VALUES (031, 'Brazil', '', '2014-07-26 14:25:56');
INSERT INTO `client_con_country` VALUES (032, 'Brunei Darussalam', '', '2014-07-26 14:41:02');
INSERT INTO `client_con_country` VALUES (033, 'Bulgaria', '', '2014-07-26 14:26:33');
INSERT INTO `client_con_country` VALUES (034, 'Cambodia', '', '2014-07-26 14:26:48');
INSERT INTO `client_con_country` VALUES (035, 'Canada', '', '2014-07-26 14:29:15');
INSERT INTO `client_con_country` VALUES (036, 'Chile', '', '2014-07-26 14:29:29');
INSERT INTO `client_con_country` VALUES (037, 'Cyprus', '', '2014-07-26 14:29:52');
INSERT INTO `client_con_country` VALUES (038, 'Czech Republic', '', '2014-07-26 14:30:10');
INSERT INTO `client_con_country` VALUES (039, 'Denmark', '', '2014-07-26 14:30:20');
INSERT INTO `client_con_country` VALUES (040, 'Egypt', '', '2014-07-26 14:30:31');
INSERT INTO `client_con_country` VALUES (041, 'Finland', '', '2014-07-26 14:30:58');
INSERT INTO `client_con_country` VALUES (042, 'Greece', '', '2014-07-26 14:31:27');
INSERT INTO `client_con_country` VALUES (043, 'Hong Kong (China)', '', '2014-07-26 14:31:53');
INSERT INTO `client_con_country` VALUES (044, 'Hungary', '', '2014-07-26 14:32:01');
INSERT INTO `client_con_country` VALUES (045, 'Iran', '', '2014-07-26 14:35:05');
INSERT INTO `client_con_country` VALUES (046, 'Iraq', '', '2014-07-26 14:35:12');
INSERT INTO `client_con_country` VALUES (047, 'Ireland', '', '2014-07-26 14:35:24');
INSERT INTO `client_con_country` VALUES (048, 'Israel', '', '2014-07-26 14:35:34');
INSERT INTO `client_con_country` VALUES (049, 'Italy', '', '2014-07-26 14:35:42');
INSERT INTO `client_con_country` VALUES (050, 'Jamaica', '', '2014-07-26 14:35:53');
INSERT INTO `client_con_country` VALUES (051, 'Bosnia and Herzegovina', '', '2014-07-26 14:38:32');
INSERT INTO `client_con_country` VALUES (052, 'Belarus', '', '2014-07-26 14:39:14');
INSERT INTO `client_con_country` VALUES (053, 'Bahrain', '', '2014-07-26 14:39:29');
INSERT INTO `client_con_country` VALUES (054, 'Colombia', '', '2014-07-26 14:40:07');
INSERT INTO `client_con_country` VALUES (055, 'Croatia', '', '2014-07-26 14:42:16');
INSERT INTO `client_con_country` VALUES (056, 'Estonia', '', '2014-07-26 14:42:43');
INSERT INTO `client_con_country` VALUES (057, 'Eritrea', '', '2014-07-26 14:43:06');
INSERT INTO `client_con_country` VALUES (058, 'Honduras', '', '2014-07-26 14:43:50');
INSERT INTO `client_con_country` VALUES (059, 'Jordan', '', '2014-07-26 14:44:30');
INSERT INTO `client_con_country` VALUES (060, 'Guatemala', '', '2014-07-26 14:45:22');
INSERT INTO `client_con_country` VALUES (061, 'Iceland', '', '2014-07-26 14:45:58');
INSERT INTO `client_con_country` VALUES (062, 'Kenya', '', '2014-07-26 14:47:04');
INSERT INTO `client_con_country` VALUES (063, 'Kosovo', '', '2014-07-26 14:48:01');
INSERT INTO `client_con_country` VALUES (064, 'Kuwait', '', '2014-07-26 14:48:09');
INSERT INTO `client_con_country` VALUES (065, 'Kyrgyzstan', '', '2014-07-26 14:49:10');
INSERT INTO `client_con_country` VALUES (066, 'Laos', '', '2014-07-26 14:49:41');
INSERT INTO `client_con_country` VALUES (067, 'Latvia', '', '2014-07-26 14:49:52');
INSERT INTO `client_con_country` VALUES (068, 'Lebanon', '', '2014-07-26 14:50:03');
INSERT INTO `client_con_country` VALUES (069, 'Liechtenstein', '', '2014-07-26 14:50:33');
INSERT INTO `client_con_country` VALUES (070, 'Lithuania', '', '2014-07-26 14:51:21');
INSERT INTO `client_con_country` VALUES (071, 'Luxembourg', '', '2014-07-26 14:51:47');
INSERT INTO `client_con_country` VALUES (072, 'Macao (China)', '', '2014-07-26 14:52:25');
INSERT INTO `client_con_country` VALUES (073, 'Macedonia', '', '2014-07-26 14:52:45');
INSERT INTO `client_con_country` VALUES (074, 'Madagascar', '', '2014-07-26 14:53:02');
INSERT INTO `client_con_country` VALUES (075, 'Maldives', '', '2014-07-26 14:53:12');
INSERT INTO `client_con_country` VALUES (076, 'Malta', '', '2014-07-26 14:53:23');
INSERT INTO `client_con_country` VALUES (077, 'Mauritius', '', '2014-07-26 14:53:51');
INSERT INTO `client_con_country` VALUES (078, 'Mexico', '', '2014-07-26 14:54:03');
INSERT INTO `client_con_country` VALUES (079, 'Moldova', '', '2014-07-26 14:54:41');
INSERT INTO `client_con_country` VALUES (080, 'Monaco', '', '2014-07-26 14:55:53');
INSERT INTO `client_con_country` VALUES (081, 'Mongolia', '', '2014-07-26 14:56:04');
INSERT INTO `client_con_country` VALUES (082, 'Montenegro', '', '2014-07-26 14:56:15');
INSERT INTO `client_con_country` VALUES (083, 'Morocco', '', '2014-07-26 15:02:43');
INSERT INTO `client_con_country` VALUES (084, 'Mozambique', '', '2014-07-26 15:03:03');
INSERT INTO `client_con_country` VALUES (085, 'Myanmar', '', '2014-07-26 15:03:17');
INSERT INTO `client_con_country` VALUES (086, 'Namibia', '', '2014-07-26 15:03:26');
INSERT INTO `client_con_country` VALUES (087, 'Nepal', '', '2014-07-26 15:03:39');
INSERT INTO `client_con_country` VALUES (088, 'Netherlands', '', '2014-07-26 15:03:50');
INSERT INTO `client_con_country` VALUES (089, 'New Caledonia', '', '2014-07-26 15:04:03');
INSERT INTO `client_con_country` VALUES (090, 'New Zealand', '', '2014-07-26 15:04:12');
INSERT INTO `client_con_country` VALUES (091, 'Nicaragua', '', '2014-07-26 15:04:23');
INSERT INTO `client_con_country` VALUES (092, 'Nigeria', '', '2014-07-26 15:04:38');
INSERT INTO `client_con_country` VALUES (093, 'Niger', '', '2014-07-26 15:04:45');
INSERT INTO `client_con_country` VALUES (094, 'Northern Mariana Island', '', '2014-07-26 15:08:59');
INSERT INTO `client_con_country` VALUES (095, 'Norway', '', '2014-07-26 15:09:53');
INSERT INTO `client_con_country` VALUES (096, 'Oman', '', '2014-07-26 15:10:18');
INSERT INTO `client_con_country` VALUES (097, 'Palau', '', '2014-07-26 15:10:31');
INSERT INTO `client_con_country` VALUES (098, 'Palesinian Territory', '', '2014-07-26 15:10:48');
INSERT INTO `client_con_country` VALUES (099, 'Panama', '', '2014-07-26 15:11:01');
INSERT INTO `client_con_country` VALUES (100, 'Papua New Guinea', '', '2014-07-26 15:11:19');
INSERT INTO `client_con_country` VALUES (101, 'Paraguay', '', '2014-07-26 15:11:29');
INSERT INTO `client_con_country` VALUES (102, 'Peru', '', '2014-07-26 15:12:07');
INSERT INTO `client_con_country` VALUES (103, 'Philippines', '', '2014-07-26 15:12:37');
INSERT INTO `client_con_country` VALUES (104, 'Poland', '', '2014-07-26 15:13:21');
INSERT INTO `client_con_country` VALUES (105, 'Portugal', '', '2014-07-26 15:13:36');
INSERT INTO `client_con_country` VALUES (106, 'Puerto Rico', '', '2014-07-26 15:13:49');
INSERT INTO `client_con_country` VALUES (107, 'Qatar', '', '2014-07-26 15:14:00');
INSERT INTO `client_con_country` VALUES (108, 'Romania', '', '2014-07-26 15:23:56');
INSERT INTO `client_con_country` VALUES (109, 'Saint Lucia', '', '2014-07-26 15:24:34');
INSERT INTO `client_con_country` VALUES (110, 'Cuba', '', '2014-07-26 15:26:00');
INSERT INTO `client_con_country` VALUES (111, 'San Marino', '', '2014-07-26 15:27:27');
INSERT INTO `client_con_country` VALUES (112, 'Saudi Arabia', '', '2014-07-26 15:27:42');
INSERT INTO `client_con_country` VALUES (113, 'Senegal', '', '2014-07-26 15:27:53');
INSERT INTO `client_con_country` VALUES (114, 'Serbia', '', '2014-07-26 15:28:00');
INSERT INTO `client_con_country` VALUES (115, 'Seychelles', '', '2014-07-26 15:28:54');
INSERT INTO `client_con_country` VALUES (116, 'Slovakia', '', '2014-07-26 15:29:02');
INSERT INTO `client_con_country` VALUES (117, 'Slovenia', '', '2014-07-26 15:29:13');
INSERT INTO `client_con_country` VALUES (118, 'South Africa', '', '2014-07-26 15:29:30');
INSERT INTO `client_con_country` VALUES (119, 'Spain', '', '2014-07-26 15:30:02');
INSERT INTO `client_con_country` VALUES (120, 'Sri Lanka', '', '2014-07-26 15:30:13');
INSERT INTO `client_con_country` VALUES (121, 'Sudan', '', '2014-07-26 15:30:38');
INSERT INTO `client_con_country` VALUES (122, 'Suriname', '', '2014-07-26 15:32:38');
INSERT INTO `client_con_country` VALUES (123, 'Sweden', '', '2014-07-26 15:33:04');
INSERT INTO `client_con_country` VALUES (124, 'Switzerland', '', '2014-07-26 15:33:15');
INSERT INTO `client_con_country` VALUES (125, 'Syrian Arab Republic', '', '2014-07-26 15:33:35');
INSERT INTO `client_con_country` VALUES (126, 'Tajikistan', '', '2014-07-26 15:33:49');
INSERT INTO `client_con_country` VALUES (127, 'Tanzania', '', '2014-07-26 15:33:59');
INSERT INTO `client_con_country` VALUES (128, 'Trinidad & Tobago', '', '2014-07-26 15:34:53');
INSERT INTO `client_con_country` VALUES (129, 'Tunisia', '', '2014-07-26 15:35:03');
INSERT INTO `client_con_country` VALUES (130, 'Turkey', '', '2014-07-26 15:35:14');
INSERT INTO `client_con_country` VALUES (131, 'Uganda', '', '2014-07-26 15:36:22');
INSERT INTO `client_con_country` VALUES (132, 'Ukraine', '', '2014-07-26 15:36:45');
INSERT INTO `client_con_country` VALUES (133, 'United Arab Emirates', '', '2014-07-26 15:36:59');
INSERT INTO `client_con_country` VALUES (134, 'United Kingdom', '', '2014-07-26 15:37:10');
INSERT INTO `client_con_country` VALUES (135, 'United States', '', '2014-07-26 15:37:21');
INSERT INTO `client_con_country` VALUES (136, 'Uruguay', '', '2014-07-26 15:37:32');
INSERT INTO `client_con_country` VALUES (137, 'Uzbekistan', '', '2014-07-26 15:37:43');
INSERT INTO `client_con_country` VALUES (138, 'Venezuela', '', '2014-07-26 15:38:08');
INSERT INTO `client_con_country` VALUES (140, 'Viet Nam', '', '2014-07-26 15:43:01');
INSERT INTO `client_con_country` VALUES (141, 'Yemen', '', '2014-07-26 15:43:17');
INSERT INTO `client_con_country` VALUES (142, 'Zambia', '', '2014-07-26 15:43:26');
INSERT INTO `client_con_country` VALUES (143, 'Zimbabwe', '', '2014-07-26 15:43:34');

-- --------------------------------------------------------

-- 
-- Table structure for table `client_con_province`
-- 

CREATE TABLE `client_con_province` (
  `con_id` int(3) unsigned zerofill NOT NULL auto_increment,
  `con_name` varchar(200) collate utf8_unicode_ci NOT NULL,
  `photo1` varchar(100) collate utf8_unicode_ci NOT NULL,
  `last_edit_time` datetime NOT NULL,
  `country_id` int(11) NOT NULL,
  PRIMARY KEY  (`con_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

-- 
-- Dumping data for table `client_con_province`
-- 

INSERT INTO `client_con_province` VALUES (001, 'Phuket', '', '2014-04-03 16:31:43', 1);
INSERT INTO `client_con_province` VALUES (002, 'Krabi', '', '2014-04-03 16:16:21', 1);
INSERT INTO `client_con_province` VALUES (003, 'Province_Client', '', '2014-05-02 16:32:06', 12);
INSERT INTO `client_con_province` VALUES (004, 'Bangkok', '', '2014-07-26 14:08:48', 1);

-- --------------------------------------------------------

-- 
-- Table structure for table `member`
-- 

CREATE TABLE `member` (
  `member_id` int(11) NOT NULL auto_increment,
  `member_name` text collate utf8_unicode_ci NOT NULL,
  `member_username` varchar(100) collate utf8_unicode_ci NOT NULL default '',
  `member_password` varchar(100) collate utf8_unicode_ci NOT NULL default '',
  `permission` varchar(50) collate utf8_unicode_ci NOT NULL default '',
  PRIMARY KEY  (`member_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

-- 
-- Dumping data for table `member`
-- 

INSERT INTO `member` VALUES (1, 'Administrator', 'solution', 'F:]^=yjo', 'Admin');
INSERT INTO `member` VALUES (3, 'Admin A', 'admin', '123', 'Admin');

-- --------------------------------------------------------

-- 
-- Table structure for table `nametitles`
-- 

CREATE TABLE `nametitles` (
  `nt_id` int(11) NOT NULL auto_increment,
  `nt_name` varchar(100) collate utf8_unicode_ci NOT NULL,
  `last_edit_time` datetime NOT NULL,
  PRIMARY KEY  (`nt_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

-- 
-- Dumping data for table `nametitles`
-- 

INSERT INTO `nametitles` VALUES (1, 'Mr', '2014-04-28 16:57:59');
INSERT INTO `nametitles` VALUES (2, 'Mrs ', '2014-04-28 16:58:02');
INSERT INTO `nametitles` VALUES (3, 'Miss', '2014-04-28 16:58:12');

-- --------------------------------------------------------

-- 
-- Table structure for table `nationalities`
-- 

CREATE TABLE `nationalities` (
  `country_id` int(11) NOT NULL auto_increment,
  `name` text NOT NULL,
  PRIMARY KEY  (`country_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=tis620 AUTO_INCREMENT=240 ;

-- 
-- Dumping data for table `nationalities`
-- 

INSERT INTO `nationalities` VALUES (1, 'Afghanistan');
INSERT INTO `nationalities` VALUES (2, 'Albania');
INSERT INTO `nationalities` VALUES (3, 'Algeria');
INSERT INTO `nationalities` VALUES (4, 'American Samoa');
INSERT INTO `nationalities` VALUES (5, 'Andorra');
INSERT INTO `nationalities` VALUES (6, 'Angola');
INSERT INTO `nationalities` VALUES (7, 'Anguilla');
INSERT INTO `nationalities` VALUES (8, 'Antarctica');
INSERT INTO `nationalities` VALUES (9, 'Antigua and Barbuda');
INSERT INTO `nationalities` VALUES (10, 'Argentina');
INSERT INTO `nationalities` VALUES (11, 'Armenia');
INSERT INTO `nationalities` VALUES (12, 'Aruba');
INSERT INTO `nationalities` VALUES (13, 'Australia');
INSERT INTO `nationalities` VALUES (14, 'Austria');
INSERT INTO `nationalities` VALUES (15, 'Azerbaijan');
INSERT INTO `nationalities` VALUES (16, 'Bahamas');
INSERT INTO `nationalities` VALUES (17, 'Bahrain');
INSERT INTO `nationalities` VALUES (18, 'Bangladesh');
INSERT INTO `nationalities` VALUES (19, 'Barbados');
INSERT INTO `nationalities` VALUES (20, 'Belarus');
INSERT INTO `nationalities` VALUES (21, 'Belgium');
INSERT INTO `nationalities` VALUES (22, 'Belize');
INSERT INTO `nationalities` VALUES (23, 'Benin');
INSERT INTO `nationalities` VALUES (24, 'Bermuda');
INSERT INTO `nationalities` VALUES (25, 'Bhutan');
INSERT INTO `nationalities` VALUES (26, 'Bolivia');
INSERT INTO `nationalities` VALUES (27, 'Bosnia and Herzegovina');
INSERT INTO `nationalities` VALUES (28, 'Botswana');
INSERT INTO `nationalities` VALUES (29, 'Bouvet Island');
INSERT INTO `nationalities` VALUES (30, 'Brazil');
INSERT INTO `nationalities` VALUES (31, 'British Indian Ocean Territory');
INSERT INTO `nationalities` VALUES (32, 'British Virgin Islands');
INSERT INTO `nationalities` VALUES (33, 'Brunei');
INSERT INTO `nationalities` VALUES (34, 'Bulgaria');
INSERT INTO `nationalities` VALUES (35, 'Burkina Faso');
INSERT INTO `nationalities` VALUES (36, 'Burundi');
INSERT INTO `nationalities` VALUES (37, 'Cambodia');
INSERT INTO `nationalities` VALUES (38, 'Cameroon');
INSERT INTO `nationalities` VALUES (39, 'Canada');
INSERT INTO `nationalities` VALUES (40, 'Cape Verde');
INSERT INTO `nationalities` VALUES (41, 'Cayman Islands');
INSERT INTO `nationalities` VALUES (42, 'Central African Republic');
INSERT INTO `nationalities` VALUES (43, 'Chad');
INSERT INTO `nationalities` VALUES (44, 'Chile');
INSERT INTO `nationalities` VALUES (45, 'China');
INSERT INTO `nationalities` VALUES (46, 'Christmas Island');
INSERT INTO `nationalities` VALUES (47, 'Cocos (Keeling) Islands');
INSERT INTO `nationalities` VALUES (48, 'Colombia');
INSERT INTO `nationalities` VALUES (49, 'Comoros');
INSERT INTO `nationalities` VALUES (50, 'Congo');
INSERT INTO `nationalities` VALUES (51, 'Congo, Republic of (Zaire)');
INSERT INTO `nationalities` VALUES (52, 'Cook Islands');
INSERT INTO `nationalities` VALUES (53, 'Costa Rica');
INSERT INTO `nationalities` VALUES (54, 'Cote D Ivoire');
INSERT INTO `nationalities` VALUES (55, 'Croatia');
INSERT INTO `nationalities` VALUES (56, 'Cuba');
INSERT INTO `nationalities` VALUES (57, 'Cyprus');
INSERT INTO `nationalities` VALUES (58, 'Czech Republic');
INSERT INTO `nationalities` VALUES (59, 'Denmark');
INSERT INTO `nationalities` VALUES (60, 'Djibouti');
INSERT INTO `nationalities` VALUES (61, 'Dominica');
INSERT INTO `nationalities` VALUES (62, 'Dominican Republic');
INSERT INTO `nationalities` VALUES (63, 'East Timor');
INSERT INTO `nationalities` VALUES (64, 'Ecuador');
INSERT INTO `nationalities` VALUES (65, 'Egypt');
INSERT INTO `nationalities` VALUES (66, 'El Salvador');
INSERT INTO `nationalities` VALUES (67, 'Equatorial Guinea');
INSERT INTO `nationalities` VALUES (68, 'Eritrea');
INSERT INTO `nationalities` VALUES (69, 'Estonia');
INSERT INTO `nationalities` VALUES (70, 'Ethiopia');
INSERT INTO `nationalities` VALUES (71, 'Falkland Islands (Malvinas)');
INSERT INTO `nationalities` VALUES (72, 'Faroe Islands');
INSERT INTO `nationalities` VALUES (73, 'Federated States of Micronesia');
INSERT INTO `nationalities` VALUES (74, 'Fiji');
INSERT INTO `nationalities` VALUES (75, 'Finland');
INSERT INTO `nationalities` VALUES (76, 'France');
INSERT INTO `nationalities` VALUES (77, 'French Guiana');
INSERT INTO `nationalities` VALUES (78, 'French Polynesia');
INSERT INTO `nationalities` VALUES (79, 'French Southern Territories');
INSERT INTO `nationalities` VALUES (80, 'Gabon');
INSERT INTO `nationalities` VALUES (81, 'Gambia');
INSERT INTO `nationalities` VALUES (82, 'Georgia');
INSERT INTO `nationalities` VALUES (83, 'Germany');
INSERT INTO `nationalities` VALUES (84, 'Ghana');
INSERT INTO `nationalities` VALUES (85, 'Gibraltar');
INSERT INTO `nationalities` VALUES (86, 'Greece');
INSERT INTO `nationalities` VALUES (87, 'Greenland');
INSERT INTO `nationalities` VALUES (88, 'Grenada');
INSERT INTO `nationalities` VALUES (89, 'Guadeloupe');
INSERT INTO `nationalities` VALUES (90, 'Guam');
INSERT INTO `nationalities` VALUES (91, 'Guatemala');
INSERT INTO `nationalities` VALUES (92, 'Guinea');
INSERT INTO `nationalities` VALUES (93, 'Guinea-Bissau');
INSERT INTO `nationalities` VALUES (94, 'Guyana');
INSERT INTO `nationalities` VALUES (95, 'Haiti');
INSERT INTO `nationalities` VALUES (96, 'Heard Island & McDonald Isls');
INSERT INTO `nationalities` VALUES (97, 'Honduras');
INSERT INTO `nationalities` VALUES (98, 'Hong Kong, China');
INSERT INTO `nationalities` VALUES (99, 'Hungary');
INSERT INTO `nationalities` VALUES (100, 'Iceland');
INSERT INTO `nationalities` VALUES (101, 'India');
INSERT INTO `nationalities` VALUES (102, 'Indonesia');
INSERT INTO `nationalities` VALUES (103, 'Iran');
INSERT INTO `nationalities` VALUES (104, 'Iraq');
INSERT INTO `nationalities` VALUES (105, 'Ireland');
INSERT INTO `nationalities` VALUES (106, 'Israel');
INSERT INTO `nationalities` VALUES (107, 'Italy');
INSERT INTO `nationalities` VALUES (108, 'Jamaica');
INSERT INTO `nationalities` VALUES (109, 'Japan');
INSERT INTO `nationalities` VALUES (110, 'Jordan');
INSERT INTO `nationalities` VALUES (111, 'Kazakhstan');
INSERT INTO `nationalities` VALUES (112, 'Kenya');
INSERT INTO `nationalities` VALUES (113, 'Kiribati');
INSERT INTO `nationalities` VALUES (114, 'Kuwait');
INSERT INTO `nationalities` VALUES (115, 'Kyrgyzstan');
INSERT INTO `nationalities` VALUES (116, 'Laos');
INSERT INTO `nationalities` VALUES (117, 'Latvia');
INSERT INTO `nationalities` VALUES (118, 'Lebanon');
INSERT INTO `nationalities` VALUES (119, 'Lesotho');
INSERT INTO `nationalities` VALUES (120, 'Liberia');
INSERT INTO `nationalities` VALUES (121, 'Libya');
INSERT INTO `nationalities` VALUES (122, 'Liechtenstein');
INSERT INTO `nationalities` VALUES (123, 'Lithuania');
INSERT INTO `nationalities` VALUES (124, 'Luxembourg');
INSERT INTO `nationalities` VALUES (125, 'Macau');
INSERT INTO `nationalities` VALUES (126, 'Macedonia');
INSERT INTO `nationalities` VALUES (127, 'Madagascar');
INSERT INTO `nationalities` VALUES (128, 'Malawi');
INSERT INTO `nationalities` VALUES (129, 'Malaysia');
INSERT INTO `nationalities` VALUES (130, 'Maldives');
INSERT INTO `nationalities` VALUES (131, 'Mali');
INSERT INTO `nationalities` VALUES (132, 'Malta');
INSERT INTO `nationalities` VALUES (133, 'Marshall Islands');
INSERT INTO `nationalities` VALUES (134, 'Martinique');
INSERT INTO `nationalities` VALUES (135, 'Mauritania');
INSERT INTO `nationalities` VALUES (136, 'Mauritius');
INSERT INTO `nationalities` VALUES (137, 'Mayotte');
INSERT INTO `nationalities` VALUES (138, 'Metropolitan France');
INSERT INTO `nationalities` VALUES (139, 'Mexico');
INSERT INTO `nationalities` VALUES (140, 'Moldova');
INSERT INTO `nationalities` VALUES (141, 'Monaco');
INSERT INTO `nationalities` VALUES (142, 'Mongolia');
INSERT INTO `nationalities` VALUES (143, 'Montserrat');
INSERT INTO `nationalities` VALUES (144, 'Morocco');
INSERT INTO `nationalities` VALUES (145, 'Mozambique');
INSERT INTO `nationalities` VALUES (146, 'Myanmar');
INSERT INTO `nationalities` VALUES (147, 'Namibia');
INSERT INTO `nationalities` VALUES (148, 'Nauru');
INSERT INTO `nationalities` VALUES (149, 'Nepal');
INSERT INTO `nationalities` VALUES (150, 'Netherlands');
INSERT INTO `nationalities` VALUES (151, 'Netherlands Antilles');
INSERT INTO `nationalities` VALUES (152, 'New Caledonia');
INSERT INTO `nationalities` VALUES (153, 'New Zealand');
INSERT INTO `nationalities` VALUES (154, 'Nicaragua');
INSERT INTO `nationalities` VALUES (155, 'Niger');
INSERT INTO `nationalities` VALUES (156, 'Nigeria');
INSERT INTO `nationalities` VALUES (157, 'Niue');
INSERT INTO `nationalities` VALUES (158, 'Norfolk Island');
INSERT INTO `nationalities` VALUES (159, 'North Korea');
INSERT INTO `nationalities` VALUES (160, 'Northern Mariana Islands');
INSERT INTO `nationalities` VALUES (161, 'Norway');
INSERT INTO `nationalities` VALUES (162, 'Oman');
INSERT INTO `nationalities` VALUES (163, 'Pakistan');
INSERT INTO `nationalities` VALUES (164, 'Palau');
INSERT INTO `nationalities` VALUES (165, 'Panama');
INSERT INTO `nationalities` VALUES (166, 'Papua New Guinea');
INSERT INTO `nationalities` VALUES (167, 'Paraguay');
INSERT INTO `nationalities` VALUES (168, 'Peru');
INSERT INTO `nationalities` VALUES (169, 'Philippines');
INSERT INTO `nationalities` VALUES (170, 'Pitcairn');
INSERT INTO `nationalities` VALUES (171, 'Poland');
INSERT INTO `nationalities` VALUES (172, 'Portugal');
INSERT INTO `nationalities` VALUES (173, 'Puerto Rico');
INSERT INTO `nationalities` VALUES (174, 'Qatar');
INSERT INTO `nationalities` VALUES (175, 'Reunion');
INSERT INTO `nationalities` VALUES (176, 'Romania');
INSERT INTO `nationalities` VALUES (177, 'Russia');
INSERT INTO `nationalities` VALUES (178, 'Rwanda');
INSERT INTO `nationalities` VALUES (179, 'Samoa');
INSERT INTO `nationalities` VALUES (180, 'San Marino');
INSERT INTO `nationalities` VALUES (181, 'Sao Tome and Principe');
INSERT INTO `nationalities` VALUES (182, 'Saudia Arabia');
INSERT INTO `nationalities` VALUES (183, 'Senegal');
INSERT INTO `nationalities` VALUES (184, 'Seychelles');
INSERT INTO `nationalities` VALUES (185, 'Sierra Leone');
INSERT INTO `nationalities` VALUES (186, 'Singapore');
INSERT INTO `nationalities` VALUES (187, 'Slovakia');
INSERT INTO `nationalities` VALUES (188, 'Slovenia');
INSERT INTO `nationalities` VALUES (189, 'Solomon Islands');
INSERT INTO `nationalities` VALUES (190, 'Somalia');
INSERT INTO `nationalities` VALUES (191, 'South Africa');
INSERT INTO `nationalities` VALUES (192, 'South Korea');
INSERT INTO `nationalities` VALUES (193, 'Spain');
INSERT INTO `nationalities` VALUES (194, 'Sri Lanka');
INSERT INTO `nationalities` VALUES (195, 'St. Helena');
INSERT INTO `nationalities` VALUES (196, 'St. Kitts and Nevis');
INSERT INTO `nationalities` VALUES (197, 'St. Lucia');
INSERT INTO `nationalities` VALUES (198, 'St. Pierre and Miquelon');
INSERT INTO `nationalities` VALUES (199, 'St. Vincent & the Grenadines');
INSERT INTO `nationalities` VALUES (200, 'Sth Georgia & Sth Sandwich Isl');
INSERT INTO `nationalities` VALUES (201, 'Sudan');
INSERT INTO `nationalities` VALUES (202, 'Suriname');
INSERT INTO `nationalities` VALUES (203, 'Svalbard & Jan Mayen Isl');
INSERT INTO `nationalities` VALUES (204, 'Swaziland');
INSERT INTO `nationalities` VALUES (205, 'Sweden');
INSERT INTO `nationalities` VALUES (206, 'Switzerland');
INSERT INTO `nationalities` VALUES (207, 'Syria');
INSERT INTO `nationalities` VALUES (208, 'Taiwan');
INSERT INTO `nationalities` VALUES (209, 'Tajikistan');
INSERT INTO `nationalities` VALUES (210, 'Tanzania');
INSERT INTO `nationalities` VALUES (211, 'Thailand');
INSERT INTO `nationalities` VALUES (212, 'Togo');
INSERT INTO `nationalities` VALUES (213, 'Tokelau');
INSERT INTO `nationalities` VALUES (214, 'Tonga');
INSERT INTO `nationalities` VALUES (215, 'Trinidad and Tobago');
INSERT INTO `nationalities` VALUES (216, 'Tunisia');
INSERT INTO `nationalities` VALUES (217, 'Turkey');
INSERT INTO `nationalities` VALUES (218, 'Turkmenistan');
INSERT INTO `nationalities` VALUES (219, 'Turks and Caicos Islands');
INSERT INTO `nationalities` VALUES (220, 'Tuvalu');
INSERT INTO `nationalities` VALUES (221, 'U.S. Virgin Islands');
INSERT INTO `nationalities` VALUES (222, 'Uganda');
INSERT INTO `nationalities` VALUES (223, 'Ukraine');
INSERT INTO `nationalities` VALUES (224, 'United Arab Emirates');
INSERT INTO `nationalities` VALUES (225, 'United Kingdom');
INSERT INTO `nationalities` VALUES (226, 'United States of America');
INSERT INTO `nationalities` VALUES (227, 'Uruguay');
INSERT INTO `nationalities` VALUES (228, 'US Minor Outlying Islands');
INSERT INTO `nationalities` VALUES (229, 'Uzbekistan');
INSERT INTO `nationalities` VALUES (230, 'Vanuatu');
INSERT INTO `nationalities` VALUES (231, 'Vatican City');
INSERT INTO `nationalities` VALUES (232, 'Venezuela');
INSERT INTO `nationalities` VALUES (233, 'Vietnam');
INSERT INTO `nationalities` VALUES (234, 'Wallis and Futuna Islands');
INSERT INTO `nationalities` VALUES (235, 'Western Sahara');
INSERT INTO `nationalities` VALUES (236, 'Yemen');
INSERT INTO `nationalities` VALUES (237, 'Yugoslavia');
INSERT INTO `nationalities` VALUES (238, 'Zambia');
INSERT INTO `nationalities` VALUES (239, 'Zimbabwe');

-- --------------------------------------------------------

-- 
-- Table structure for table `pickuptransfers`
-- 

CREATE TABLE `pickuptransfers` (
  `put_id` int(11) NOT NULL auto_increment,
  `pickuptransfer_type_id` int(11) NOT NULL,
  `time_id` int(11) NOT NULL,
  `put_time` varchar(100) collate utf8_unicode_ci NOT NULL,
  `car_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `province_id` int(11) NOT NULL,
  `pickuptransfer_price_type_id` int(11) NOT NULL,
  `last_edit_time` datetime NOT NULL,
  `put_ref` varchar(100) collate utf8_unicode_ci NOT NULL,
  `agents_sup_id` int(11) NOT NULL,
  `put_name` varchar(200) collate utf8_unicode_ci NOT NULL,
  `area_id` int(11) NOT NULL,
  `area_to_id` int(11) NOT NULL,
  `put_hot` int(1) NOT NULL,
  PRIMARY KEY  (`put_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

-- 
-- Dumping data for table `pickuptransfers`
-- 

INSERT INTO `pickuptransfers` VALUES (2, 1, 1, 'ประมาณ 1 ชม.', 1, 1, 2, 1, '2014-07-18 17:29:45', 'PUF001', 2, 'Pick Up Transfer 01', 3, 4, 1);
INSERT INTO `pickuptransfers` VALUES (3, 2, 3, 'ประมาณ 2 ชม.', 2, 1, 1, 2, '2014-07-18 17:35:41', 'PUF002', 5, 'Pick Up Transfer 02', 1, 2, 1);
INSERT INTO `pickuptransfers` VALUES (4, 2, 5, '50 MINUTES', 0, 4, 4, 2, '2014-07-19 11:08:28', 'PTG-PIER (JOIN / 08.30)', 0, 'Patong Beach to Rassada Pier (JOIN / 08.30)', 6, 12, 0);
INSERT INTO `pickuptransfers` VALUES (5, 1, 5, '40 MINUTES', 3, 4, 4, 1, '2014-07-29 15:33:40', 'PTG-PIER (PVT/08.30)', 0, 'Patong Beach to Rassada Pier (Private / 08.30)', 6, 12, 0);

-- --------------------------------------------------------

-- 
-- Table structure for table `pickuptransfer_con_area`
-- 

CREATE TABLE `pickuptransfer_con_area` (
  `con_id` int(3) unsigned zerofill NOT NULL auto_increment,
  `con_name` varchar(200) collate utf8_unicode_ci NOT NULL,
  `photo1` varchar(100) collate utf8_unicode_ci NOT NULL,
  `last_edit_time` datetime NOT NULL,
  `province_id` int(11) NOT NULL,
  PRIMARY KEY  (`con_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=39 ;

-- 
-- Dumping data for table `pickuptransfer_con_area`
-- 

INSERT INTO `pickuptransfer_con_area` VALUES (001, 'Pick Up Transfer Area 01', '', '2014-07-18 14:03:57', 1);
INSERT INTO `pickuptransfer_con_area` VALUES (002, 'Pick Up Transfer Area 02', '', '2014-07-18 14:04:08', 1);
INSERT INTO `pickuptransfer_con_area` VALUES (003, 'Pick Up Transfer Area 03', '', '2014-07-18 14:04:15', 2);
INSERT INTO `pickuptransfer_con_area` VALUES (004, 'Pick Up Transfer Area 04', '', '2014-07-18 14:04:20', 2);
INSERT INTO `pickuptransfer_con_area` VALUES (005, 'Pick Up Transfer Area 05', '', '2014-07-18 14:04:29', 3);
INSERT INTO `pickuptransfer_con_area` VALUES (006, 'Patong Beach', '', '2014-07-19 10:46:53', 4);
INSERT INTO `pickuptransfer_con_area` VALUES (007, 'Kata Beach', '', '2014-07-19 10:47:05', 4);
INSERT INTO `pickuptransfer_con_area` VALUES (008, 'Karon Beach', '', '2014-07-19 10:47:16', 4);
INSERT INTO `pickuptransfer_con_area` VALUES (009, 'Phuket Town', '', '2014-07-19 10:47:27', 4);
INSERT INTO `pickuptransfer_con_area` VALUES (010, 'Chalong', '', '2014-07-19 10:47:38', 4);
INSERT INTO `pickuptransfer_con_area` VALUES (011, 'Krabi Town', '', '2014-07-19 10:48:26', 5);
INSERT INTO `pickuptransfer_con_area` VALUES (012, 'Rassada Pier', '', '2014-07-19 10:48:39', 4);
INSERT INTO `pickuptransfer_con_area` VALUES (013, 'Phuket Airport', '', '2014-07-19 10:48:51', 4);
INSERT INTO `pickuptransfer_con_area` VALUES (014, 'Klong Jilad Pier', '', '2014-07-19 10:49:02', 5);
INSERT INTO `pickuptransfer_con_area` VALUES (015, 'Ao Nang Beach', '', '2014-07-19 10:49:18', 5);
INSERT INTO `pickuptransfer_con_area` VALUES (016, 'Ao Nam Mao Pier', '', '2014-07-19 10:49:31', 5);
INSERT INTO `pickuptransfer_con_area` VALUES (017, 'Krabi Airport', '', '2014-07-19 10:49:48', 5);
INSERT INTO `pickuptransfer_con_area` VALUES (018, 'Surat Thani Town', '', '2014-07-19 10:50:28', 6);
INSERT INTO `pickuptransfer_con_area` VALUES (019, 'Don Sak Pier (Songserm Pier)', '', '2014-07-19 10:50:42', 6);
INSERT INTO `pickuptransfer_con_area` VALUES (020, 'Surat Thani Train Station', '', '2014-07-19 10:50:55', 6);
INSERT INTO `pickuptransfer_con_area` VALUES (021, 'Maikhao Beach', '', '2014-07-19 16:16:20', 4);
INSERT INTO `pickuptransfer_con_area` VALUES (022, 'Naithon Beach', '', '2014-07-19 16:16:33', 4);
INSERT INTO `pickuptransfer_con_area` VALUES (023, 'Layan Beach', '', '2014-07-19 16:16:58', 4);
INSERT INTO `pickuptransfer_con_area` VALUES (024, 'Ao Po / Pa Klok', '', '2014-07-19 16:20:05', 4);
INSERT INTO `pickuptransfer_con_area` VALUES (026, 'Nai Yang Beach', '', '2014-07-19 16:20:36', 4);
INSERT INTO `pickuptransfer_con_area` VALUES (027, 'Bangtao Beach', '', '2014-07-19 16:20:48', 4);
INSERT INTO `pickuptransfer_con_area` VALUES (028, 'Kamala Beach', '', '2014-07-19 16:21:02', 4);
INSERT INTO `pickuptransfer_con_area` VALUES (029, 'Surin Beach', '', '2014-07-19 16:21:23', 4);
INSERT INTO `pickuptransfer_con_area` VALUES (030, 'Nakalae Beach', '', '2014-07-19 16:47:32', 4);
INSERT INTO `pickuptransfer_con_area` VALUES (031, 'Tri Trang Beach', '', '2014-07-19 16:48:13', 4);
INSERT INTO `pickuptransfer_con_area` VALUES (032, 'Naiharn Beach', '', '2014-07-19 16:49:06', 4);
INSERT INTO `pickuptransfer_con_area` VALUES (033, 'Kata Noi Beach', '', '2014-07-19 16:49:21', 4);
INSERT INTO `pickuptransfer_con_area` VALUES (034, 'Rawai Beach', '', '2014-07-19 16:49:35', 4);
INSERT INTO `pickuptransfer_con_area` VALUES (035, 'Koh Siray ', '', '2014-07-19 16:50:35', 4);
INSERT INTO `pickuptransfer_con_area` VALUES (036, 'By-Pass Road / Bang Chee Lou', '', '2014-07-19 16:55:25', 4);
INSERT INTO `pickuptransfer_con_area` VALUES (037, 'Phuket New Bus Terminal', '', '2014-07-19 16:51:14', 4);
INSERT INTO `pickuptransfer_con_area` VALUES (038, 'Panwa Beach', '', '2014-07-19 16:55:57', 4);

-- --------------------------------------------------------

-- 
-- Table structure for table `pickuptransfer_con_car`
-- 

CREATE TABLE `pickuptransfer_con_car` (
  `con_id` int(3) unsigned zerofill NOT NULL auto_increment,
  `con_name` varchar(200) collate utf8_unicode_ci NOT NULL,
  `photo1` varchar(100) collate utf8_unicode_ci NOT NULL,
  `last_edit_time` datetime NOT NULL,
  `car_registration` varchar(100) collate utf8_unicode_ci NOT NULL,
  `car_seat_num` int(5) NOT NULL,
  `car_brand` varchar(100) collate utf8_unicode_ci NOT NULL,
  `car_model` varchar(100) collate utf8_unicode_ci NOT NULL,
  `car_driver_name` varchar(200) collate utf8_unicode_ci NOT NULL,
  `car_driver_tel` varchar(100) collate utf8_unicode_ci NOT NULL,
  `car_driver_license` text collate utf8_unicode_ci NOT NULL,
  PRIMARY KEY  (`con_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=8 ;

-- 
-- Dumping data for table `pickuptransfer_con_car`
-- 

INSERT INTO `pickuptransfer_con_car` VALUES (001, '', '', '2014-07-18 14:24:42', 'Pick Up Transfer Car01', 1, 'ยี่ห้อรถ 01', 'รุ่นรถ 01', 'ชื่อคนขับรถ 01', 'เบอร์โทรศัพท์คนขับรถ 01', 'ข้อมูลใบขับขี่ ข้อมูลใบขับขี่ ข้อมูลใบขับขี่ ข้อมูลใบขับขี่ 01');
INSERT INTO `pickuptransfer_con_car` VALUES (002, '', '', '2014-07-18 14:25:12', 'Pick Up Transfer Car02 ', 2, 'ยี่ห้อรถ 02', 'รุ่นรถ 02', 'ชื่อคนขับรถ 02', 'เบอร์โทรศัพท์คนขับรถ 02', 'ข้อมูลใบขับขี่ ข้อมูลใบขับขี่ ข้อมูลใบขับขี่ ข้อมูลใบขับขี่ ข้อมูลใบขับขี่ 02');
INSERT INTO `pickuptransfer_con_car` VALUES (003, '', '', '2014-07-19 11:06:22', 'VAN (8)', 8, 'TOYOTA', 'COMMUTER', '', '', '');
INSERT INTO `pickuptransfer_con_car` VALUES (004, '', '', '2014-07-19 11:06:12', 'VAN (10)', 10, 'TOYOTA', 'COMMUTER', '', '', '');
INSERT INTO `pickuptransfer_con_car` VALUES (006, '', '', '2014-07-19 11:07:49', 'CAR (2)', 2, 'TOYOTA', 'CAMRY', '', '', '');
INSERT INTO `pickuptransfer_con_car` VALUES (007, '', '', '2014-07-19 11:07:38', 'CAR (3)', 3, 'TOYOTA', 'FORTUNER', '', '', '');

-- --------------------------------------------------------

-- 
-- Table structure for table `pickuptransfer_con_country`
-- 

CREATE TABLE `pickuptransfer_con_country` (
  `con_id` int(3) unsigned zerofill NOT NULL auto_increment,
  `con_name` varchar(200) collate utf8_unicode_ci NOT NULL,
  `photo1` varchar(100) collate utf8_unicode_ci NOT NULL,
  `last_edit_time` datetime NOT NULL,
  PRIMARY KEY  (`con_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

-- 
-- Dumping data for table `pickuptransfer_con_country`
-- 

INSERT INTO `pickuptransfer_con_country` VALUES (001, 'Pick Up Transfer Country 01', '', '2014-07-18 13:47:46');
INSERT INTO `pickuptransfer_con_country` VALUES (002, 'Pick Up Transfer Country 02', '', '2014-07-18 13:47:54');
INSERT INTO `pickuptransfer_con_country` VALUES (003, 'Pick Up Transfer Country 03', '', '2014-07-18 13:48:01');
INSERT INTO `pickuptransfer_con_country` VALUES (004, 'Thailand', '', '2014-07-19 10:44:38');

-- --------------------------------------------------------

-- 
-- Table structure for table `pickuptransfer_con_destination`
-- 

CREATE TABLE `pickuptransfer_con_destination` (
  `con_id` int(3) unsigned zerofill NOT NULL auto_increment,
  `con_name` varchar(200) collate utf8_unicode_ci NOT NULL,
  `photo1` varchar(100) collate utf8_unicode_ci NOT NULL,
  `last_edit_time` datetime NOT NULL,
  `country_id` int(11) NOT NULL,
  `province_id` int(11) NOT NULL,
  `area_id` int(11) NOT NULL,
  PRIMARY KEY  (`con_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

-- 
-- Dumping data for table `pickuptransfer_con_destination`
-- 

INSERT INTO `pickuptransfer_con_destination` VALUES (001, 'Pick Up Transfer Destination 01', '', '2014-07-18 14:13:04', 1, 1, 1);
INSERT INTO `pickuptransfer_con_destination` VALUES (002, 'Pick Up Transfer Destination 02', '', '2014-07-18 14:13:34', 1, 2, 3);
INSERT INTO `pickuptransfer_con_destination` VALUES (003, 'Pick Up Transfer Destination 03', '', '2014-07-18 14:13:46', 1, 2, 3);

-- --------------------------------------------------------

-- 
-- Table structure for table `pickuptransfer_con_province`
-- 

CREATE TABLE `pickuptransfer_con_province` (
  `con_id` int(3) unsigned zerofill NOT NULL auto_increment,
  `con_name` varchar(200) collate utf8_unicode_ci NOT NULL,
  `photo1` varchar(100) collate utf8_unicode_ci NOT NULL,
  `last_edit_time` datetime NOT NULL,
  `country_id` int(11) NOT NULL,
  PRIMARY KEY  (`con_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

-- 
-- Dumping data for table `pickuptransfer_con_province`
-- 

INSERT INTO `pickuptransfer_con_province` VALUES (001, 'Pick Up Transfer Province 01', '', '2014-07-18 13:56:09', 1);
INSERT INTO `pickuptransfer_con_province` VALUES (002, 'Pick Up Transfer Province 02', '', '2014-07-18 13:56:20', 1);
INSERT INTO `pickuptransfer_con_province` VALUES (003, 'Pick Up Transfer Province 03', '', '2014-07-18 13:56:27', 2);
INSERT INTO `pickuptransfer_con_province` VALUES (004, 'Phuket', '', '2014-07-19 10:45:03', 4);
INSERT INTO `pickuptransfer_con_province` VALUES (005, 'Krabi', '', '2014-07-19 10:46:21', 4);
INSERT INTO `pickuptransfer_con_province` VALUES (006, 'Surat Thani', '', '2014-07-19 10:48:02', 4);

-- --------------------------------------------------------

-- 
-- Table structure for table `pickuptransfer_con_time`
-- 

CREATE TABLE `pickuptransfer_con_time` (
  `con_id` int(3) unsigned zerofill NOT NULL auto_increment,
  `con_name` varchar(100) collate utf8_unicode_ci NOT NULL,
  `photo1` varchar(100) collate utf8_unicode_ci NOT NULL,
  `last_edit_time` datetime NOT NULL,
  PRIMARY KEY  (`con_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=23 ;

-- 
-- Dumping data for table `pickuptransfer_con_time`
-- 

INSERT INTO `pickuptransfer_con_time` VALUES (001, '10:00', '', '2014-07-18 14:18:22');
INSERT INTO `pickuptransfer_con_time` VALUES (002, '10:30', '', '2014-07-18 14:18:38');
INSERT INTO `pickuptransfer_con_time` VALUES (003, '11:00', '', '2014-07-18 14:18:43');
INSERT INTO `pickuptransfer_con_time` VALUES (004, '07:00', '', '2014-07-19 10:54:00');
INSERT INTO `pickuptransfer_con_time` VALUES (005, '07:10', '', '2014-07-19 10:54:09');
INSERT INTO `pickuptransfer_con_time` VALUES (006, '07:30', '', '2014-07-19 10:54:22');
INSERT INTO `pickuptransfer_con_time` VALUES (007, '07:40', '', '2014-07-19 10:54:33');
INSERT INTO `pickuptransfer_con_time` VALUES (008, '07:50', '', '2014-07-19 10:54:45');
INSERT INTO `pickuptransfer_con_time` VALUES (009, '08:00', '', '2014-07-19 10:54:52');
INSERT INTO `pickuptransfer_con_time` VALUES (010, '12:00', '', '2014-07-19 10:54:59');
INSERT INTO `pickuptransfer_con_time` VALUES (011, '12:10', '', '2014-07-19 10:55:08');
INSERT INTO `pickuptransfer_con_time` VALUES (012, '12:15', '', '2014-07-19 10:55:17');
INSERT INTO `pickuptransfer_con_time` VALUES (013, '12:30', '', '2014-07-19 10:55:28');
INSERT INTO `pickuptransfer_con_time` VALUES (014, '12:50', '', '2014-07-19 10:55:38');
INSERT INTO `pickuptransfer_con_time` VALUES (015, '13:00', '', '2014-07-19 10:55:45');
INSERT INTO `pickuptransfer_con_time` VALUES (016, '11:00', '', '2014-07-19 10:55:58');
INSERT INTO `pickuptransfer_con_time` VALUES (017, '16:30', '', '2014-07-19 10:56:11');
INSERT INTO `pickuptransfer_con_time` VALUES (018, '11:30', '', '2014-07-19 10:56:27');
INSERT INTO `pickuptransfer_con_time` VALUES (019, '17:00', '', '2014-07-19 10:56:35');
INSERT INTO `pickuptransfer_con_time` VALUES (020, '17:45', '', '2014-07-19 10:57:07');
INSERT INTO `pickuptransfer_con_time` VALUES (021, '17:30', '', '2014-07-19 10:57:21');
INSERT INTO `pickuptransfer_con_time` VALUES (022, '06:00', '', '2014-07-19 10:57:35');

-- --------------------------------------------------------

-- 
-- Table structure for table `pickuptransfer_period`
-- 

CREATE TABLE `pickuptransfer_period` (
  `putpe_id` int(11) NOT NULL auto_increment,
  `pickuptransfers_id` int(11) NOT NULL,
  `putpe_name` varchar(200) collate utf8_unicode_ci NOT NULL,
  `putpe_datefrom` date NOT NULL,
  `putpe_dateto` date NOT NULL,
  `arrange` int(11) NOT NULL,
  `last_edit_time` datetime NOT NULL,
  PRIMARY KEY  (`putpe_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

-- 
-- Dumping data for table `pickuptransfer_period`
-- 

INSERT INTO `pickuptransfer_period` VALUES (1, 2, 'Pick Up Transfer Period 01', '2014-01-01', '2014-07-31', 0, '2014-07-18 16:48:49');
INSERT INTO `pickuptransfer_period` VALUES (2, 2, 'Pick Up Transfer Period 02', '2014-08-01', '2014-12-31', 0, '2014-07-18 16:48:39');
INSERT INTO `pickuptransfer_period` VALUES (3, 3, 'Pick Up Transfer Period 03', '2014-01-01', '2014-12-31', 0, '2014-07-18 17:36:29');
INSERT INTO `pickuptransfer_period` VALUES (4, 4, '', '2014-07-19', '2014-10-31', 0, '2014-07-19 10:59:29');
INSERT INTO `pickuptransfer_period` VALUES (5, 5, 'LOW 2014', '2014-07-19', '2014-10-31', 0, '2014-07-19 11:04:15');

-- --------------------------------------------------------

-- 
-- Table structure for table `pickuptransfer_price_type`
-- 

CREATE TABLE `pickuptransfer_price_type` (
  `c_id` int(11) NOT NULL auto_increment,
  `c_name` varchar(200) collate utf8_unicode_ci NOT NULL,
  `photo1` varchar(100) collate utf8_unicode_ci NOT NULL,
  `last_edit_time` datetime NOT NULL,
  PRIMARY KEY  (`c_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

-- 
-- Dumping data for table `pickuptransfer_price_type`
-- 

INSERT INTO `pickuptransfer_price_type` VALUES (1, 'แบบกำหนดเปอร์เซ็นต์ส่วนลด', '', '2014-05-15 15:23:28');
INSERT INTO `pickuptransfer_price_type` VALUES (2, 'แบบกำหนดราคาเอง', '', '2014-05-15 15:23:24');

-- --------------------------------------------------------

-- 
-- Table structure for table `pickuptransfer_ratepercents`
-- 

CREATE TABLE `pickuptransfer_ratepercents` (
  `putrateper_id` int(11) NOT NULL auto_increment,
  `pickuptransfers_id` int(11) NOT NULL,
  `agentgrade_id` int(11) NOT NULL,
  `putrateper_discount` int(3) NOT NULL,
  `last_edit_time` datetime NOT NULL,
  PRIMARY KEY  (`putrateper_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

-- 
-- Dumping data for table `pickuptransfer_ratepercents`
-- 

INSERT INTO `pickuptransfer_ratepercents` VALUES (1, 2, 1, 55, '2014-07-18 17:33:53');
INSERT INTO `pickuptransfer_ratepercents` VALUES (2, 2, 2, 44, '2014-07-18 17:33:53');
INSERT INTO `pickuptransfer_ratepercents` VALUES (3, 2, 3, 33, '2014-07-18 17:33:53');
INSERT INTO `pickuptransfer_ratepercents` VALUES (4, 2, 4, 22, '2014-07-18 17:33:53');
INSERT INTO `pickuptransfer_ratepercents` VALUES (5, 2, 5, 11, '2014-07-18 17:33:53');

-- --------------------------------------------------------

-- 
-- Table structure for table `pickuptransfer_rates`
-- 

CREATE TABLE `pickuptransfer_rates` (
  `putrate_id` int(11) NOT NULL auto_increment,
  `pickuptransfers_id` int(11) NOT NULL,
  `agentgrade_id` int(11) NOT NULL,
  `rate_1` double(15,2) NOT NULL,
  `rate_2` double(15,2) NOT NULL,
  `rate_3` double(15,2) NOT NULL,
  `rate_4` double(15,2) NOT NULL,
  `rate_5` double(15,2) NOT NULL,
  `last_edit_time` datetime NOT NULL,
  `pickuptransferratetype_id` int(11) NOT NULL,
  `pickuptransferperiod_id` int(11) NOT NULL,
  PRIMARY KEY  (`putrate_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=19 ;

-- 
-- Dumping data for table `pickuptransfer_rates`
-- 

INSERT INTO `pickuptransfer_rates` VALUES (1, 2, 0, 0.00, 0.00, 0.00, 1004.00, 0.00, '2014-07-18 17:26:18', 1, 1);
INSERT INTO `pickuptransfer_rates` VALUES (2, 2, 0, 0.00, 0.00, 0.00, 2004.00, 0.00, '2014-07-18 17:26:18', 1, 2);
INSERT INTO `pickuptransfer_rates` VALUES (3, 2, 0, 0.00, 0.00, 0.00, 3004.00, 0.00, '2014-07-18 17:26:18', 2, 1);
INSERT INTO `pickuptransfer_rates` VALUES (4, 2, 0, 0.00, 0.00, 0.00, 5004.00, 0.00, '2014-07-18 17:26:18', 2, 2);
INSERT INTO `pickuptransfer_rates` VALUES (5, 2, 1, 0.00, 0.00, 0.00, 1014.00, 0.00, '2014-07-18 17:26:47', 1, 1);
INSERT INTO `pickuptransfer_rates` VALUES (6, 2, 1, 0.00, 0.00, 0.00, 2014.00, 0.00, '2014-07-18 17:26:47', 1, 2);
INSERT INTO `pickuptransfer_rates` VALUES (7, 2, 1, 0.00, 0.00, 0.00, 3014.00, 0.00, '2014-07-18 17:26:47', 2, 1);
INSERT INTO `pickuptransfer_rates` VALUES (8, 2, 1, 0.00, 0.00, 0.00, 4014.00, 0.00, '2014-07-18 17:26:47', 2, 2);
INSERT INTO `pickuptransfer_rates` VALUES (9, 2, 2, 0.00, 0.00, 0.00, 1024.00, 0.00, '2014-07-18 17:27:51', 1, 1);
INSERT INTO `pickuptransfer_rates` VALUES (10, 2, 2, 0.00, 0.00, 0.00, 2024.00, 0.00, '2014-07-18 17:27:51', 1, 2);
INSERT INTO `pickuptransfer_rates` VALUES (11, 2, 2, 0.00, 0.00, 0.00, 3024.00, 0.00, '2014-07-18 17:27:51', 2, 1);
INSERT INTO `pickuptransfer_rates` VALUES (12, 2, 2, 0.00, 0.00, 0.00, 4024.00, 0.00, '2014-07-18 17:27:51', 2, 2);
INSERT INTO `pickuptransfer_rates` VALUES (13, 3, 0, 101.00, 102.00, 103.00, 0.00, 0.00, '2014-07-18 17:37:01', 3, 3);
INSERT INTO `pickuptransfer_rates` VALUES (14, 3, 0, 201.00, 202.00, 203.00, 0.00, 0.00, '2014-07-18 17:37:01', 4, 3);
INSERT INTO `pickuptransfer_rates` VALUES (15, 3, 1, 111.00, 112.00, 113.00, 0.00, 0.00, '2014-07-18 17:37:33', 3, 3);
INSERT INTO `pickuptransfer_rates` VALUES (16, 3, 1, 211.00, 212.00, 213.00, 0.00, 0.00, '2014-07-18 17:37:33', 4, 3);
INSERT INTO `pickuptransfer_rates` VALUES (17, 4, 0, 250.00, 250.00, 0.00, 0.00, 0.00, '2014-07-19 11:01:54', 5, 4);
INSERT INTO `pickuptransfer_rates` VALUES (18, 5, 0, 0.00, 0.00, 0.00, 800.00, 0.00, '2014-07-30 09:05:29', 6, 5);

-- --------------------------------------------------------

-- 
-- Table structure for table `pickuptransfer_ratetypes`
-- 

CREATE TABLE `pickuptransfer_ratetypes` (
  `putrt_id` int(11) NOT NULL auto_increment,
  `pickuptransfers_id` int(11) NOT NULL,
  `putrt_name` varchar(200) collate utf8_unicode_ci NOT NULL,
  `putrt_detail` text collate utf8_unicode_ci NOT NULL,
  `photo1` varchar(100) collate utf8_unicode_ci NOT NULL,
  `last_edit_time` datetime NOT NULL,
  PRIMARY KEY  (`putrt_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

-- 
-- Dumping data for table `pickuptransfer_ratetypes`
-- 

INSERT INTO `pickuptransfer_ratetypes` VALUES (1, 2, 'Pick Up Transfer Rate Type 01', '', '', '2014-07-18 16:33:27');
INSERT INTO `pickuptransfer_ratetypes` VALUES (2, 2, 'Pick Up Transfer Rate Type 02', '', '', '2014-07-18 16:33:22');
INSERT INTO `pickuptransfer_ratetypes` VALUES (3, 3, 'Pick Up Transfer Rate Type 03', '', '', '2014-07-18 17:35:55');
INSERT INTO `pickuptransfer_ratetypes` VALUES (4, 3, 'Pick Up Transfer Rate Type 04', '', '', '2014-07-18 17:36:00');
INSERT INTO `pickuptransfer_ratetypes` VALUES (5, 4, 'PTG-PIER (AM JOIN)', '', '', '2014-07-19 10:59:12');
INSERT INTO `pickuptransfer_ratetypes` VALUES (6, 5, 'PTG-PIER (AM PVT)', '', '', '2014-07-19 11:03:52');

-- --------------------------------------------------------

-- 
-- Table structure for table `pickuptransfer_type`
-- 

CREATE TABLE `pickuptransfer_type` (
  `c_id` int(11) NOT NULL auto_increment,
  `c_name` varchar(200) collate utf8_unicode_ci NOT NULL,
  `photo1` varchar(100) collate utf8_unicode_ci NOT NULL,
  `last_edit_time` datetime NOT NULL,
  PRIMARY KEY  (`c_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

-- 
-- Dumping data for table `pickuptransfer_type`
-- 

INSERT INTO `pickuptransfer_type` VALUES (1, 'Private', '', '2014-05-15 14:51:58');
INSERT INTO `pickuptransfer_type` VALUES (2, 'Join', '', '2014-05-15 14:52:08');

-- --------------------------------------------------------

-- 
-- Table structure for table `privatelandtransfers`
-- 

CREATE TABLE `privatelandtransfers` (
  `plt_id` int(11) NOT NULL auto_increment,
  `privatelandtransfer_type_id` int(11) NOT NULL,
  `time_id` int(11) NOT NULL,
  `plt_time` varchar(100) collate utf8_unicode_ci NOT NULL,
  `car_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `province_id` int(11) NOT NULL,
  `privatelandtransfer_price_type_id` int(11) NOT NULL,
  `last_edit_time` datetime NOT NULL,
  `plt_ref` varchar(100) collate utf8_unicode_ci NOT NULL,
  `agents_sup_id` int(11) NOT NULL,
  `plt_name` varchar(200) collate utf8_unicode_ci NOT NULL,
  `area_id` int(11) NOT NULL,
  `area_to_id` int(11) NOT NULL,
  `plt_hot` int(1) NOT NULL,
  PRIMARY KEY  (`plt_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=10 ;

-- 
-- Dumping data for table `privatelandtransfers`
-- 

INSERT INTO `privatelandtransfers` VALUES (1, 1, 2, 'ประมาณ 1 ชม.', 1, 1, 1, 1, '2014-07-24 16:40:24', 'PLT001', 4, 'Private Land Transfer 01', 1, 2, 1);
INSERT INTO `privatelandtransfers` VALUES (2, 1, 5, '1 HOUR', 3, 5, 6, 1, '2014-07-26 10:43:25', 'HKTAPT-PIER (6.00-19.00)', 4, 'Phuket Airport to Rassada Pier (06.00-19.00)', 6, 7, 0);
INSERT INTO `privatelandtransfers` VALUES (5, 1, 5, '1 HOUR', 3, 5, 6, 1, '2014-07-26 10:43:56', 'HKTAPT-TOWN (06.00-19.00)', 5, 'Phuket Airport - Phuket Town (06.00-19.00)', 6, 9, 0);
INSERT INTO `privatelandtransfers` VALUES (6, 1, 6, '1 HOUR', 3, 5, 6, 1, '2014-07-29 15:17:06', 'HKTAPT-TOWN(19.00-21.00)', 5, 'Phuket Airport - Phuket Town (19.00-21.00)', 6, 9, 0);
INSERT INTO `privatelandtransfers` VALUES (7, 1, 7, '1 HOUR', 3, 5, 6, 1, '2014-07-29 15:17:34', 'HKTAPT-TOWN(21.00-00.00)', 5, 'Phuket Airport - Phuket Town (21.00-00.00)', 6, 9, 0);
INSERT INTO `privatelandtransfers` VALUES (8, 1, 5, '1.20 HOURS', 3, 5, 6, 1, '2014-07-29 15:24:58', 'HKTAPT-PTG(06.00-19.00)', 5, 'Phuket Airport - Patong Beach (06.00-19.00)', 6, 8, 0);
INSERT INTO `privatelandtransfers` VALUES (9, 1, 6, '1.20 HOURS', 3, 5, 6, 1, '2014-07-30 09:40:09', 'HKTAPT-PTG (19.00-21.00)', 5, 'Phuket Airport - Patong Beach (19.00-21.00)', 6, 8, 0);

-- --------------------------------------------------------

-- 
-- Table structure for table `privatelandtransfer_con_area`
-- 

CREATE TABLE `privatelandtransfer_con_area` (
  `con_id` int(3) unsigned zerofill NOT NULL auto_increment,
  `con_name` varchar(200) collate utf8_unicode_ci NOT NULL,
  `photo1` varchar(100) collate utf8_unicode_ci NOT NULL,
  `last_edit_time` datetime NOT NULL,
  `province_id` int(11) NOT NULL,
  PRIMARY KEY  (`con_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=25 ;

-- 
-- Dumping data for table `privatelandtransfer_con_area`
-- 

INSERT INTO `privatelandtransfer_con_area` VALUES (001, 'Private Land Transfer Area 01', '', '2014-07-24 14:18:05', 1);
INSERT INTO `privatelandtransfer_con_area` VALUES (002, 'Private Land Transfer Area 02', '', '2014-07-24 14:18:10', 1);
INSERT INTO `privatelandtransfer_con_area` VALUES (003, 'Private Land Transfer Area 03', '', '2014-07-24 14:18:15', 2);
INSERT INTO `privatelandtransfer_con_area` VALUES (004, 'Private Land Transfer Area 04', '', '2014-07-24 14:18:20', 3);
INSERT INTO `privatelandtransfer_con_area` VALUES (005, 'Private Land Transfer Area 05', '', '2014-07-24 14:18:41', 3);
INSERT INTO `privatelandtransfer_con_area` VALUES (006, 'Phuket Airport', '', '2014-07-25 09:22:00', 6);
INSERT INTO `privatelandtransfer_con_area` VALUES (007, 'Rassada Pier', '', '2014-07-25 09:22:14', 6);
INSERT INTO `privatelandtransfer_con_area` VALUES (008, 'Patong Beach', '', '2014-07-25 09:22:27', 6);
INSERT INTO `privatelandtransfer_con_area` VALUES (009, 'Phuket Town', '', '2014-07-25 09:22:39', 6);
INSERT INTO `privatelandtransfer_con_area` VALUES (010, 'Kata Beach', '', '2014-07-25 09:22:53', 6);
INSERT INTO `privatelandtransfer_con_area` VALUES (011, 'Karon Beach', '', '2014-07-25 09:23:04', 6);
INSERT INTO `privatelandtransfer_con_area` VALUES (012, 'Rawai Beach', '', '2014-07-25 09:23:17', 6);
INSERT INTO `privatelandtransfer_con_area` VALUES (013, 'Chalong', '', '2014-07-25 09:23:28', 6);
INSERT INTO `privatelandtransfer_con_area` VALUES (014, 'Kalim Beach', '', '2014-07-25 09:23:40', 6);
INSERT INTO `privatelandtransfer_con_area` VALUES (015, 'Nakalae Beach', '', '2014-07-25 09:24:44', 6);
INSERT INTO `privatelandtransfer_con_area` VALUES (016, 'Kamala Beach', '', '2014-07-25 09:24:55', 6);
INSERT INTO `privatelandtransfer_con_area` VALUES (017, 'Surin Beach', '', '2014-07-25 09:25:06', 6);
INSERT INTO `privatelandtransfer_con_area` VALUES (018, 'Bangtao Beach', '', '2014-07-25 09:25:20', 6);
INSERT INTO `privatelandtransfer_con_area` VALUES (019, 'Maikhao Beach', '', '2014-07-25 09:25:33', 6);
INSERT INTO `privatelandtransfer_con_area` VALUES (020, 'Nai Yang Beach', '', '2014-07-25 09:25:43', 6);
INSERT INTO `privatelandtransfer_con_area` VALUES (021, 'Naithon Beach', '', '2014-07-25 09:25:55', 6);
INSERT INTO `privatelandtransfer_con_area` VALUES (022, 'Layan Beach', '', '2014-07-25 09:26:11', 6);
INSERT INTO `privatelandtransfer_con_area` VALUES (023, 'Ao Po / Pa Klok', '', '2014-07-25 09:26:23', 6);
INSERT INTO `privatelandtransfer_con_area` VALUES (024, 'Panwa Beach', '', '2014-07-25 09:26:37', 6);

-- --------------------------------------------------------

-- 
-- Table structure for table `privatelandtransfer_con_car`
-- 

CREATE TABLE `privatelandtransfer_con_car` (
  `con_id` int(3) unsigned zerofill NOT NULL auto_increment,
  `con_name` varchar(200) collate utf8_unicode_ci NOT NULL,
  `photo1` varchar(100) collate utf8_unicode_ci NOT NULL,
  `last_edit_time` datetime NOT NULL,
  `car_registration` varchar(100) collate utf8_unicode_ci NOT NULL,
  `car_seat_num` int(5) NOT NULL,
  `car_brand` varchar(100) collate utf8_unicode_ci NOT NULL,
  `car_model` varchar(100) collate utf8_unicode_ci NOT NULL,
  `car_driver_name` varchar(200) collate utf8_unicode_ci NOT NULL,
  `car_driver_tel` varchar(100) collate utf8_unicode_ci NOT NULL,
  `car_driver_license` text collate utf8_unicode_ci NOT NULL,
  PRIMARY KEY  (`con_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

-- 
-- Dumping data for table `privatelandtransfer_con_car`
-- 

INSERT INTO `privatelandtransfer_con_car` VALUES (001, '', '', '2014-07-24 14:47:18', 'Private Land Transfer Car 001', 1, 'ยี่ห้อรถ 01', 'รุ่นรถ 01', 'ชื่อคนขับรถ 01', 'เบอร์โทรศัพท์คนขับรถ 01', 'Private Land Transfer Car\r\n	 001');
INSERT INTO `privatelandtransfer_con_car` VALUES (002, '', '', '2014-07-24 14:47:40', 'Private Land Transfer Car 002', 2, 'ยี่ห้อรถ 02', 'รุ่นรถ 02', 'ชื่อคนขับรถ 02', 'เบอร์โทรศัพท์คนขับรถ 02', 'ข้อมูลใบขับขี่  0202');
INSERT INTO `privatelandtransfer_con_car` VALUES (003, '', '', '2014-07-25 09:39:18', 'VAN (8)', 8, 'TOYOTA', 'COMMUTER', '', '', '');
INSERT INTO `privatelandtransfer_con_car` VALUES (004, '', '', '2014-07-25 09:39:37', 'VAN (10)', 10, 'TOYOTA', 'COMMUTER', '', '', '');
INSERT INTO `privatelandtransfer_con_car` VALUES (005, '', '', '2014-07-25 09:39:53', 'CAR (2)', 2, '', '', '', '', '');

-- --------------------------------------------------------

-- 
-- Table structure for table `privatelandtransfer_con_country`
-- 

CREATE TABLE `privatelandtransfer_con_country` (
  `con_id` int(3) unsigned zerofill NOT NULL auto_increment,
  `con_name` varchar(200) collate utf8_unicode_ci NOT NULL,
  `photo1` varchar(100) collate utf8_unicode_ci NOT NULL,
  `last_edit_time` datetime NOT NULL,
  PRIMARY KEY  (`con_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

-- 
-- Dumping data for table `privatelandtransfer_con_country`
-- 

INSERT INTO `privatelandtransfer_con_country` VALUES (001, 'Private Land Transfer Country 01', '', '2014-07-24 11:49:59');
INSERT INTO `privatelandtransfer_con_country` VALUES (002, 'Private Land Transfer Country 02', '', '2014-07-24 11:50:06');
INSERT INTO `privatelandtransfer_con_country` VALUES (003, 'Private Land Transfer Country 03', '', '2014-07-24 11:50:10');
INSERT INTO `privatelandtransfer_con_country` VALUES (004, 'Private Land Transfer Country 04', '', '2014-07-24 11:51:47');
INSERT INTO `privatelandtransfer_con_country` VALUES (005, 'Thailand', '', '2014-07-25 09:20:42');

-- --------------------------------------------------------

-- 
-- Table structure for table `privatelandtransfer_con_destination`
-- 

CREATE TABLE `privatelandtransfer_con_destination` (
  `con_id` int(3) unsigned zerofill NOT NULL auto_increment,
  `con_name` varchar(200) collate utf8_unicode_ci NOT NULL,
  `photo1` varchar(100) collate utf8_unicode_ci NOT NULL,
  `last_edit_time` datetime NOT NULL,
  `country_id` int(11) NOT NULL,
  `province_id` int(11) NOT NULL,
  `area_id` int(11) NOT NULL,
  PRIMARY KEY  (`con_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

-- 
-- Dumping data for table `privatelandtransfer_con_destination`
-- 

INSERT INTO `privatelandtransfer_con_destination` VALUES (001, 'Private Land Transfer Destination 01', '', '2014-07-24 14:27:18', 1, 1, 1);
INSERT INTO `privatelandtransfer_con_destination` VALUES (002, 'Private Land Transfer Destination 02', '', '2014-07-24 14:27:43', 1, 2, 3);
INSERT INTO `privatelandtransfer_con_destination` VALUES (003, 'Private Land Transfer Destination 03', '', '2014-07-24 14:27:53', 1, 2, 3);
INSERT INTO `privatelandtransfer_con_destination` VALUES (004, 'Private Land Transfer Destination 04', '', '2014-07-24 14:27:59', 1, 2, 3);

-- --------------------------------------------------------

-- 
-- Table structure for table `privatelandtransfer_con_province`
-- 

CREATE TABLE `privatelandtransfer_con_province` (
  `con_id` int(3) unsigned zerofill NOT NULL auto_increment,
  `con_name` varchar(200) collate utf8_unicode_ci NOT NULL,
  `photo1` varchar(100) collate utf8_unicode_ci NOT NULL,
  `last_edit_time` datetime NOT NULL,
  `country_id` int(11) NOT NULL,
  PRIMARY KEY  (`con_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=9 ;

-- 
-- Dumping data for table `privatelandtransfer_con_province`
-- 

INSERT INTO `privatelandtransfer_con_province` VALUES (001, 'Private Land Transfer Province 01', '', '2014-07-24 14:10:54', 1);
INSERT INTO `privatelandtransfer_con_province` VALUES (002, 'Private Land Transfer Province 02', '', '2014-07-24 14:10:59', 1);
INSERT INTO `privatelandtransfer_con_province` VALUES (003, 'Private Land Transfer Province 03', '', '2014-07-24 14:11:05', 2);
INSERT INTO `privatelandtransfer_con_province` VALUES (004, 'Private Land Transfer Province 04', '', '2014-07-24 14:11:10', 2);
INSERT INTO `privatelandtransfer_con_province` VALUES (005, 'Private Land Transfer Province 05', '', '2014-07-24 14:11:17', 3);
INSERT INTO `privatelandtransfer_con_province` VALUES (006, 'Phuket', '', '2014-07-25 09:21:07', 5);
INSERT INTO `privatelandtransfer_con_province` VALUES (007, 'Krabi', '', '2014-07-25 09:21:21', 5);
INSERT INTO `privatelandtransfer_con_province` VALUES (008, 'Bangkok', '', '2014-07-25 09:21:31', 5);

-- --------------------------------------------------------

-- 
-- Table structure for table `privatelandtransfer_con_time`
-- 

CREATE TABLE `privatelandtransfer_con_time` (
  `con_id` int(3) unsigned zerofill NOT NULL auto_increment,
  `con_name` varchar(100) collate utf8_unicode_ci NOT NULL,
  `photo1` varchar(100) collate utf8_unicode_ci NOT NULL,
  `last_edit_time` datetime NOT NULL,
  PRIMARY KEY  (`con_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=9 ;

-- 
-- Dumping data for table `privatelandtransfer_con_time`
-- 

INSERT INTO `privatelandtransfer_con_time` VALUES (001, '08:00-10:00', '', '2014-07-24 15:06:40');
INSERT INTO `privatelandtransfer_con_time` VALUES (002, '09:00-11:00', '', '2014-07-24 14:42:36');
INSERT INTO `privatelandtransfer_con_time` VALUES (003, '12:30-14:40', '', '2014-07-24 15:06:08');
INSERT INTO `privatelandtransfer_con_time` VALUES (004, '14:00-16:10', '', '2014-07-24 15:06:21');
INSERT INTO `privatelandtransfer_con_time` VALUES (005, '06:00-19:00', '', '2014-07-25 09:31:05');
INSERT INTO `privatelandtransfer_con_time` VALUES (006, '19:00-21:00', '', '2014-07-25 09:33:56');
INSERT INTO `privatelandtransfer_con_time` VALUES (007, '21:00-00:00', '', '2014-07-25 09:34:15');
INSERT INTO `privatelandtransfer_con_time` VALUES (008, '00:00-06:00', '', '2014-07-25 09:34:25');

-- --------------------------------------------------------

-- 
-- Table structure for table `privatelandtransfer_period`
-- 

CREATE TABLE `privatelandtransfer_period` (
  `pltpe_id` int(11) NOT NULL auto_increment,
  `privatelandtransfers_id` int(11) NOT NULL,
  `pltpe_name` varchar(200) collate utf8_unicode_ci NOT NULL,
  `pltpe_datefrom` date NOT NULL,
  `pltpe_dateto` date NOT NULL,
  `arrange` int(11) NOT NULL,
  `last_edit_time` datetime NOT NULL,
  PRIMARY KEY  (`pltpe_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=19 ;

-- 
-- Dumping data for table `privatelandtransfer_period`
-- 

INSERT INTO `privatelandtransfer_period` VALUES (1, 1, 'Private Land Transfer Period 01', '2014-01-01', '2014-10-31', 0, '2014-07-24 15:57:49');
INSERT INTO `privatelandtransfer_period` VALUES (2, 1, 'Private Land Transfer Period 02', '2014-11-01', '2014-12-31', 0, '2014-07-24 15:58:07');
INSERT INTO `privatelandtransfer_period` VALUES (3, 2, '2013-2014', '2014-07-25', '2014-10-31', 0, '2014-07-25 09:43:05');
INSERT INTO `privatelandtransfer_period` VALUES (4, 2, '2014-2015', '2014-11-01', '2015-10-31', 0, '2014-07-25 09:42:54');
INSERT INTO `privatelandtransfer_period` VALUES (5, 3, '2013-2014', '2014-07-25', '2014-10-31', 0, '2014-07-25 09:47:19');
INSERT INTO `privatelandtransfer_period` VALUES (6, 3, '2014-2015', '2014-11-01', '2014-10-31', 0, '2014-07-25 10:07:48');
INSERT INTO `privatelandtransfer_period` VALUES (7, 4, '2013-2014', '2014-07-25', '2014-10-31', 0, '2014-07-25 10:11:48');
INSERT INTO `privatelandtransfer_period` VALUES (8, 4, '2014-2015', '2014-11-01', '2015-10-31', 0, '2014-07-25 10:12:11');
INSERT INTO `privatelandtransfer_period` VALUES (9, 5, '2013-2014', '2014-07-25', '2014-10-31', 0, '2014-07-25 10:17:14');
INSERT INTO `privatelandtransfer_period` VALUES (10, 5, '2014-2015', '2014-11-01', '2015-10-31', 0, '2014-07-25 10:17:38');
INSERT INTO `privatelandtransfer_period` VALUES (11, 6, '2013-2014', '2014-07-26', '2014-10-31', 0, '2014-07-26 10:52:01');
INSERT INTO `privatelandtransfer_period` VALUES (12, 6, '2014-2015', '2014-11-01', '2015-10-31', 0, '2014-07-26 10:52:26');
INSERT INTO `privatelandtransfer_period` VALUES (13, 7, '2013-2014', '2014-07-29', '2014-10-31', 0, '2014-07-29 15:14:24');
INSERT INTO `privatelandtransfer_period` VALUES (14, 7, '2014-2015', '2014-11-01', '2015-10-31', 0, '2014-07-29 15:14:50');
INSERT INTO `privatelandtransfer_period` VALUES (15, 8, '2013-2014', '2014-07-29', '2014-10-31', 0, '2014-07-29 15:25:49');
INSERT INTO `privatelandtransfer_period` VALUES (16, 8, '2014-2015', '2014-11-01', '2015-10-31', 0, '2014-07-29 15:26:38');
INSERT INTO `privatelandtransfer_period` VALUES (17, 9, '2013-2014', '2014-07-30', '2014-10-31', 0, '2014-07-30 09:44:18');
INSERT INTO `privatelandtransfer_period` VALUES (18, 9, '2014-2015', '2014-11-01', '2015-10-31', 0, '2014-07-30 09:44:55');

-- --------------------------------------------------------

-- 
-- Table structure for table `privatelandtransfer_price_type`
-- 

CREATE TABLE `privatelandtransfer_price_type` (
  `c_id` int(11) NOT NULL auto_increment,
  `c_name` varchar(200) collate utf8_unicode_ci NOT NULL,
  `photo1` varchar(100) collate utf8_unicode_ci NOT NULL,
  `last_edit_time` datetime NOT NULL,
  PRIMARY KEY  (`c_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

-- 
-- Dumping data for table `privatelandtransfer_price_type`
-- 

INSERT INTO `privatelandtransfer_price_type` VALUES (1, 'แบบกำหนดเปอร์เซ็นต์ส่วนลด', '', '2014-05-15 15:23:28');
INSERT INTO `privatelandtransfer_price_type` VALUES (2, 'แบบกำหนดราคาเอง', '', '2014-05-15 15:23:24');

-- --------------------------------------------------------

-- 
-- Table structure for table `privatelandtransfer_ratepercents`
-- 

CREATE TABLE `privatelandtransfer_ratepercents` (
  `pltrateper_id` int(11) NOT NULL auto_increment,
  `privatelandtransfers_id` int(11) NOT NULL,
  `agentgrade_id` int(11) NOT NULL,
  `pltrateper_discount` int(3) NOT NULL,
  `last_edit_time` datetime NOT NULL,
  PRIMARY KEY  (`pltrateper_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=41 ;

-- 
-- Dumping data for table `privatelandtransfer_ratepercents`
-- 

INSERT INTO `privatelandtransfer_ratepercents` VALUES (1, 1, 1, 77, '2014-07-24 16:45:22');
INSERT INTO `privatelandtransfer_ratepercents` VALUES (2, 1, 2, 66, '2014-07-24 16:45:22');
INSERT INTO `privatelandtransfer_ratepercents` VALUES (3, 1, 3, 55, '2014-07-24 16:45:22');
INSERT INTO `privatelandtransfer_ratepercents` VALUES (4, 1, 4, 44, '2014-07-24 16:45:22');
INSERT INTO `privatelandtransfer_ratepercents` VALUES (5, 1, 5, 33, '2014-07-24 16:45:22');
INSERT INTO `privatelandtransfer_ratepercents` VALUES (6, 2, 1, 0, '2014-07-25 09:44:44');
INSERT INTO `privatelandtransfer_ratepercents` VALUES (7, 2, 2, 0, '2014-07-25 09:44:44');
INSERT INTO `privatelandtransfer_ratepercents` VALUES (8, 2, 3, 0, '2014-07-25 09:44:44');
INSERT INTO `privatelandtransfer_ratepercents` VALUES (9, 2, 4, 0, '2014-07-25 09:44:44');
INSERT INTO `privatelandtransfer_ratepercents` VALUES (10, 2, 5, 0, '2014-07-25 09:44:44');
INSERT INTO `privatelandtransfer_ratepercents` VALUES (11, 3, 1, 0, '2014-07-25 10:08:37');
INSERT INTO `privatelandtransfer_ratepercents` VALUES (12, 3, 2, 0, '2014-07-25 10:08:37');
INSERT INTO `privatelandtransfer_ratepercents` VALUES (13, 3, 3, 0, '2014-07-25 10:08:37');
INSERT INTO `privatelandtransfer_ratepercents` VALUES (14, 3, 4, 0, '2014-07-25 10:08:37');
INSERT INTO `privatelandtransfer_ratepercents` VALUES (15, 3, 5, 0, '2014-07-25 10:08:37');
INSERT INTO `privatelandtransfer_ratepercents` VALUES (16, 4, 1, 0, '2014-07-25 10:12:53');
INSERT INTO `privatelandtransfer_ratepercents` VALUES (17, 4, 2, 0, '2014-07-25 10:12:53');
INSERT INTO `privatelandtransfer_ratepercents` VALUES (18, 4, 3, 0, '2014-07-25 10:12:53');
INSERT INTO `privatelandtransfer_ratepercents` VALUES (19, 4, 4, 0, '2014-07-25 10:12:53');
INSERT INTO `privatelandtransfer_ratepercents` VALUES (20, 4, 5, 0, '2014-07-25 10:12:53');
INSERT INTO `privatelandtransfer_ratepercents` VALUES (21, 5, 1, 0, '2014-07-25 10:18:17');
INSERT INTO `privatelandtransfer_ratepercents` VALUES (22, 5, 2, 0, '2014-07-25 10:18:17');
INSERT INTO `privatelandtransfer_ratepercents` VALUES (23, 5, 3, 0, '2014-07-25 10:18:17');
INSERT INTO `privatelandtransfer_ratepercents` VALUES (24, 5, 4, 0, '2014-07-25 10:18:17');
INSERT INTO `privatelandtransfer_ratepercents` VALUES (25, 5, 5, 0, '2014-07-25 10:18:17');
INSERT INTO `privatelandtransfer_ratepercents` VALUES (26, 6, 1, 0, '2014-07-26 10:55:13');
INSERT INTO `privatelandtransfer_ratepercents` VALUES (27, 6, 2, 0, '2014-07-26 10:55:13');
INSERT INTO `privatelandtransfer_ratepercents` VALUES (28, 6, 3, 0, '2014-07-26 10:55:13');
INSERT INTO `privatelandtransfer_ratepercents` VALUES (29, 6, 4, 0, '2014-07-26 10:55:13');
INSERT INTO `privatelandtransfer_ratepercents` VALUES (30, 6, 5, 0, '2014-07-26 10:55:13');
INSERT INTO `privatelandtransfer_ratepercents` VALUES (31, 7, 1, 0, '2014-07-29 15:16:35');
INSERT INTO `privatelandtransfer_ratepercents` VALUES (32, 7, 2, 0, '2014-07-29 15:16:35');
INSERT INTO `privatelandtransfer_ratepercents` VALUES (33, 7, 3, 0, '2014-07-29 15:16:35');
INSERT INTO `privatelandtransfer_ratepercents` VALUES (34, 7, 4, 0, '2014-07-29 15:16:35');
INSERT INTO `privatelandtransfer_ratepercents` VALUES (35, 7, 5, 0, '2014-07-29 15:16:35');
INSERT INTO `privatelandtransfer_ratepercents` VALUES (36, 9, 1, 0, '2014-07-30 10:00:35');
INSERT INTO `privatelandtransfer_ratepercents` VALUES (37, 9, 2, 0, '2014-07-30 10:00:35');
INSERT INTO `privatelandtransfer_ratepercents` VALUES (38, 9, 3, 0, '2014-07-30 10:00:35');
INSERT INTO `privatelandtransfer_ratepercents` VALUES (39, 9, 4, 0, '2014-07-30 10:00:35');
INSERT INTO `privatelandtransfer_ratepercents` VALUES (40, 9, 5, 0, '2014-07-30 10:00:35');

-- --------------------------------------------------------

-- 
-- Table structure for table `privatelandtransfer_rates`
-- 

CREATE TABLE `privatelandtransfer_rates` (
  `pltrate_id` int(11) NOT NULL auto_increment,
  `privatelandtransfers_id` int(11) NOT NULL,
  `agentgrade_id` int(11) NOT NULL,
  `rate_1` double(15,2) NOT NULL,
  `rate_2` double(15,2) NOT NULL,
  `rate_3` double(15,2) NOT NULL,
  `rate_4` double(15,2) NOT NULL,
  `rate_5` double(15,2) NOT NULL,
  `last_edit_time` datetime NOT NULL,
  `privatelandtransferratetype_id` int(11) NOT NULL,
  `privatelandtransferperiod_id` int(11) NOT NULL,
  PRIMARY KEY  (`pltrate_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=29 ;

-- 
-- Dumping data for table `privatelandtransfer_rates`
-- 

INSERT INTO `privatelandtransfer_rates` VALUES (1, 1, 0, 0.00, 0.00, 0.00, 1004.00, 0.00, '2014-07-24 16:39:29', 1, 1);
INSERT INTO `privatelandtransfer_rates` VALUES (2, 1, 0, 0.00, 0.00, 0.00, 1014.00, 0.00, '2014-07-24 16:39:29', 1, 2);
INSERT INTO `privatelandtransfer_rates` VALUES (3, 1, 0, 0.00, 0.00, 0.00, 1024.00, 0.00, '2014-07-24 16:39:29', 2, 1);
INSERT INTO `privatelandtransfer_rates` VALUES (4, 1, 0, 0.00, 0.00, 0.00, 1034.00, 0.00, '2014-07-24 16:39:29', 2, 2);
INSERT INTO `privatelandtransfer_rates` VALUES (5, 1, 1, 0.00, 0.00, 0.00, 2004.00, 0.00, '2014-07-24 16:39:34', 1, 1);
INSERT INTO `privatelandtransfer_rates` VALUES (6, 1, 1, 0.00, 0.00, 0.00, 2014.00, 0.00, '2014-07-24 16:39:34', 1, 2);
INSERT INTO `privatelandtransfer_rates` VALUES (7, 1, 1, 0.00, 0.00, 0.00, 2024.00, 0.00, '2014-07-24 16:39:34', 2, 1);
INSERT INTO `privatelandtransfer_rates` VALUES (8, 1, 1, 0.00, 0.00, 0.00, 2034.00, 0.00, '2014-07-24 16:39:34', 2, 2);
INSERT INTO `privatelandtransfer_rates` VALUES (9, 1, 2, 0.00, 0.00, 0.00, 3004.00, 0.00, '2014-07-24 16:39:37', 1, 1);
INSERT INTO `privatelandtransfer_rates` VALUES (10, 1, 2, 0.00, 0.00, 0.00, 3014.00, 0.00, '2014-07-24 16:39:37', 1, 2);
INSERT INTO `privatelandtransfer_rates` VALUES (11, 1, 2, 0.00, 0.00, 0.00, 3024.00, 0.00, '2014-07-24 16:39:37', 2, 1);
INSERT INTO `privatelandtransfer_rates` VALUES (12, 1, 2, 0.00, 0.00, 0.00, 3034.00, 0.00, '2014-07-24 16:39:37', 2, 2);
INSERT INTO `privatelandtransfer_rates` VALUES (13, 2, 0, 0.00, 0.00, 0.00, 700.00, 0.00, '2014-07-25 09:43:40', 3, 3);
INSERT INTO `privatelandtransfer_rates` VALUES (14, 2, 0, 0.00, 0.00, 0.00, 800.00, 0.00, '2014-07-25 09:43:40', 3, 4);
INSERT INTO `privatelandtransfer_rates` VALUES (15, 3, 0, 0.00, 0.00, 0.00, 900.00, 0.00, '2014-07-25 10:08:21', 4, 5);
INSERT INTO `privatelandtransfer_rates` VALUES (16, 3, 0, 0.00, 0.00, 0.00, 1000.00, 0.00, '2014-07-25 10:08:21', 4, 6);
INSERT INTO `privatelandtransfer_rates` VALUES (17, 4, 0, 0.00, 0.00, 0.00, 1100.00, 0.00, '2014-07-25 10:12:34', 5, 7);
INSERT INTO `privatelandtransfer_rates` VALUES (18, 4, 0, 0.00, 0.00, 0.00, 1200.00, 0.00, '2014-07-25 10:12:34', 5, 8);
INSERT INTO `privatelandtransfer_rates` VALUES (19, 5, 0, 0.00, 0.00, 0.00, 700.00, 0.00, '2014-07-25 10:18:04', 6, 9);
INSERT INTO `privatelandtransfer_rates` VALUES (20, 5, 0, 0.00, 0.00, 0.00, 800.00, 0.00, '2014-07-25 10:18:04', 6, 10);
INSERT INTO `privatelandtransfer_rates` VALUES (21, 6, 0, 0.00, 0.00, 0.00, 900.00, 0.00, '2014-07-26 10:55:01', 7, 11);
INSERT INTO `privatelandtransfer_rates` VALUES (22, 6, 0, 0.00, 0.00, 0.00, 1000.00, 0.00, '2014-07-26 10:55:01', 7, 12);
INSERT INTO `privatelandtransfer_rates` VALUES (23, 7, 0, 0.00, 0.00, 0.00, 1100.00, 0.00, '2014-07-29 15:16:10', 8, 13);
INSERT INTO `privatelandtransfer_rates` VALUES (24, 7, 0, 0.00, 0.00, 0.00, 1200.00, 0.00, '2014-07-29 15:16:10', 8, 14);
INSERT INTO `privatelandtransfer_rates` VALUES (25, 8, 0, 0.00, 0.00, 0.00, 800.00, 0.00, '2014-07-29 15:29:42', 9, 15);
INSERT INTO `privatelandtransfer_rates` VALUES (26, 8, 0, 0.00, 0.00, 0.00, 900.00, 0.00, '2014-07-29 15:29:42', 9, 16);
INSERT INTO `privatelandtransfer_rates` VALUES (27, 9, 0, 0.00, 0.00, 0.00, 900.00, 0.00, '2014-07-30 09:59:32', 10, 17);
INSERT INTO `privatelandtransfer_rates` VALUES (28, 9, 0, 0.00, 0.00, 0.00, 1000.00, 0.00, '2014-07-30 09:59:32', 10, 18);

-- --------------------------------------------------------

-- 
-- Table structure for table `privatelandtransfer_ratetypes`
-- 

CREATE TABLE `privatelandtransfer_ratetypes` (
  `pltrt_id` int(11) NOT NULL auto_increment,
  `privatelandtransfers_id` int(11) NOT NULL,
  `pltrt_name` varchar(200) collate utf8_unicode_ci NOT NULL,
  `pltrt_detail` text collate utf8_unicode_ci NOT NULL,
  `photo1` varchar(100) collate utf8_unicode_ci NOT NULL,
  `last_edit_time` datetime NOT NULL,
  PRIMARY KEY  (`pltrt_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=11 ;

-- 
-- Dumping data for table `privatelandtransfer_ratetypes`
-- 

INSERT INTO `privatelandtransfer_ratetypes` VALUES (1, 1, 'Private Land Transfer Rate Type 01', '', '', '2014-07-24 15:50:59');
INSERT INTO `privatelandtransfer_ratetypes` VALUES (2, 1, 'Private Land Transfer Rate Type 02', '', '', '2014-07-24 15:51:02');
INSERT INTO `privatelandtransfer_ratetypes` VALUES (3, 2, 'HKTAPT-PIER (DAY)', '', '', '2014-07-25 09:37:08');
INSERT INTO `privatelandtransfer_ratetypes` VALUES (4, 3, 'HKTAPT-PIER (19.00-21.00)', '', '', '2014-07-25 09:46:48');
INSERT INTO `privatelandtransfer_ratetypes` VALUES (5, 4, 'HKTAPT-PIER (21.00-00.00)', '', '', '2014-07-25 10:11:30');
INSERT INTO `privatelandtransfer_ratetypes` VALUES (6, 5, 'HKTAPT-TOWN(06.00-19.00)', '', '', '2014-07-25 10:16:55');
INSERT INTO `privatelandtransfer_ratetypes` VALUES (7, 6, 'HKTAPT-TOWN(19.00-21.00)', '', '', '2014-07-26 10:51:40');
INSERT INTO `privatelandtransfer_ratetypes` VALUES (8, 7, 'HKTAPT-TOWN(21.00-00.00)', '', '', '2014-07-29 15:12:25');
INSERT INTO `privatelandtransfer_ratetypes` VALUES (9, 8, 'HKTAPT-PTG(06.00-19.00)', '', '', '2014-07-29 15:25:24');
INSERT INTO `privatelandtransfer_ratetypes` VALUES (10, 9, 'HKTAPT-PTG (19.00-21.00)', '', '', '2014-07-30 09:41:01');

-- --------------------------------------------------------

-- 
-- Table structure for table `privatelandtransfer_type`
-- 

CREATE TABLE `privatelandtransfer_type` (
  `c_id` int(11) NOT NULL auto_increment,
  `c_name` varchar(200) collate utf8_unicode_ci NOT NULL,
  `photo1` varchar(100) collate utf8_unicode_ci NOT NULL,
  `last_edit_time` datetime NOT NULL,
  PRIMARY KEY  (`c_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

-- 
-- Dumping data for table `privatelandtransfer_type`
-- 

INSERT INTO `privatelandtransfer_type` VALUES (1, 'Private', '', '2014-05-15 14:51:58');

-- --------------------------------------------------------

-- 
-- Table structure for table `suppliers`
-- 

CREATE TABLE `suppliers` (
  `sp_id` int(5) unsigned zerofill NOT NULL auto_increment,
  `sp_cname` varchar(200) collate utf8_unicode_ci NOT NULL,
  `servicetype_id` int(11) NOT NULL,
  `sp_pyamenttermtype` varchar(50) collate utf8_unicode_ci NOT NULL,
  `sp_paymentterm_num` int(3) NOT NULL,
  `sp_addr1` text collate utf8_unicode_ci NOT NULL,
  `sp_addr2` text collate utf8_unicode_ci NOT NULL,
  `country_id` int(11) NOT NULL,
  `province_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `sp_zipcode` varchar(100) collate utf8_unicode_ci NOT NULL,
  `sp_phone1` varchar(100) collate utf8_unicode_ci NOT NULL,
  `sp_phone2` varchar(100) collate utf8_unicode_ci NOT NULL,
  `sp_mobile` varchar(100) collate utf8_unicode_ci NOT NULL,
  `sp_fax` varchar(100) collate utf8_unicode_ci NOT NULL,
  `sp_email` varchar(100) collate utf8_unicode_ci NOT NULL,
  `sp_password` varchar(100) collate utf8_unicode_ci NOT NULL,
  `sp_email_reser` varchar(100) collate utf8_unicode_ci NOT NULL,
  `sp_email_account` varchar(100) collate utf8_unicode_ci NOT NULL,
  `sp_url` varchar(200) collate utf8_unicode_ci NOT NULL,
  `file1` varchar(100) collate utf8_unicode_ci NOT NULL,
  `file2` varchar(100) collate utf8_unicode_ci NOT NULL,
  `file3` varchar(100) collate utf8_unicode_ci NOT NULL,
  `file4` varchar(100) collate utf8_unicode_ci NOT NULL,
  `file5` varchar(100) collate utf8_unicode_ci NOT NULL,
  `sp_memassociation` varchar(200) collate utf8_unicode_ci NOT NULL,
  `sp_title_id` int(11) NOT NULL,
  `sp_fname` varchar(200) collate utf8_unicode_ci NOT NULL,
  `sp_lname` varchar(200) collate utf8_unicode_ci NOT NULL,
  `sp_department` varchar(200) collate utf8_unicode_ci NOT NULL,
  `sp_position` varchar(200) collate utf8_unicode_ci NOT NULL,
  `sp_phone3` varchar(100) collate utf8_unicode_ci NOT NULL,
  `sp_fax2` varchar(100) collate utf8_unicode_ci NOT NULL,
  `sp_mobile2` varchar(100) collate utf8_unicode_ci NOT NULL,
  `sp_email_c` varchar(100) collate utf8_unicode_ci NOT NULL,
  `photo1` varchar(100) collate utf8_unicode_ci NOT NULL,
  `photo2` varchar(100) collate utf8_unicode_ci NOT NULL,
  `photo3` varchar(100) collate utf8_unicode_ci NOT NULL,
  `photo4` varchar(100) collate utf8_unicode_ci NOT NULL,
  `photo5` varchar(100) collate utf8_unicode_ci NOT NULL,
  `sp_bankname` varchar(200) collate utf8_unicode_ci NOT NULL,
  `sp_bankbranch` varchar(200) collate utf8_unicode_ci NOT NULL,
  `sp_bankbenef` varchar(200) collate utf8_unicode_ci NOT NULL,
  `sp_bankaccount` varchar(200) collate utf8_unicode_ci NOT NULL,
  `sp_bankswift` varchar(200) collate utf8_unicode_ci NOT NULL,
  `last_edit_time` datetime NOT NULL,
  `sp_ref` varchar(100) collate utf8_unicode_ci NOT NULL,
  `sp_create_date` date NOT NULL,
  PRIMARY KEY  (`sp_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

-- 
-- Dumping data for table `suppliers`
-- 

INSERT INTO `suppliers` VALUES (00001, 'Company Name test ', 12, 'prepayment', 0, 'ที่อยู่ Supplier 1test ', 'ที่อยู่ Supplier 2test ', 12, 3, 9, 'Zip Code test', 'เบอร์โทรศัพท์ Supplier 1 test', 'เบอร์โทรศัพท์ Supplier 2test', 'เบอร์โทรศัพท์มือถือ Supplier test', 'Fax test', 'yajok.jojo@gmail.com99', '2cc129d93', 'Email Reservatintest', 'Account test', 'URL test', '1399457690_1.pdf', '1399457690_2.pdf', '1399457690_3.pdf', '1399457690_4.pdf', '1399457690_5.pdf', 'Member Association test', 1, 'ชื่อ ผู้ติดต่อ test', 'สกุล ผู้ติดต่อtest', 'Department test', 'Position test', 'เบอร์โทรศัพท์ ผู้ติดต่อtest', 'Fax ผู้ติดต่อ test', 'เบอร์โทรศัพท์มือถือ ผู้ติดต่อ test', 'Email ผู้ติดต่อtest', '1399457852_1.jpg', '', '', '', '', 'Bank Name test', 'Bank Branchtest', 'Beneficiary test', 'Account Numbertest', 'Swift Code test', '2014-05-26 15:46:12', 'SP001', '2014-01-02');
INSERT INTO `suppliers` VALUES (00002, 'ADV TRAVEL', 13, 'credit', 30, '', '', 1, 2, 0, '', '', '', '', '', 'andamaboat@gmail.com', '46ebbfc0', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2014-07-16 15:54:09', 'BKK0001', '2014-07-16');
INSERT INTO `suppliers` VALUES (00003, 'ISLAND HOPPER', 14, 'prepayment', 0, '', '', 1, 1, 0, '', '', '', '', '', 'naoko@andamanwavemaster.com', '491f5330', 'andamanboat@gmail.com', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2014-07-17 11:24:25', 'HKT0001', '2014-07-17');
INSERT INTO `suppliers` VALUES (00004, 'S.S.ADV.', 14, '', 0, '', '', 0, 0, 0, '', '', '', '', '', 'awmnaoko@hotmail.com', 'd371dd24', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2014-07-17 11:25:31', 'HKT0002', '2014-07-17');
INSERT INTO `suppliers` VALUES (00005, 'S.S.ADV.', 13, 'credit', 30, '', '', 0, 0, 0, '', '', '', '', '', '123@GMAIL.COM', '63788a1a', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2014-07-17 11:28:37', 'HKT0003', '2014-07-17');

-- --------------------------------------------------------

-- 
-- Table structure for table `supplier_con_city`
-- 

CREATE TABLE `supplier_con_city` (
  `con_id` int(3) unsigned zerofill NOT NULL auto_increment,
  `con_name` varchar(200) collate utf8_unicode_ci NOT NULL,
  `photo1` varchar(100) collate utf8_unicode_ci NOT NULL,
  `last_edit_time` datetime NOT NULL,
  `province_id` int(11) NOT NULL,
  PRIMARY KEY  (`con_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=10 ;

-- 
-- Dumping data for table `supplier_con_city`
-- 

INSERT INTO `supplier_con_city` VALUES (007, 'Kata', '', '2014-04-03 16:50:49', 1);
INSERT INTO `supplier_con_city` VALUES (008, 'Patong', '', '2014-04-03 16:50:45', 1);
INSERT INTO `supplier_con_city` VALUES (009, 'City_Supplier ', '', '2014-05-07 15:32:04', 3);

-- --------------------------------------------------------

-- 
-- Table structure for table `supplier_con_country`
-- 

CREATE TABLE `supplier_con_country` (
  `con_id` int(3) unsigned zerofill NOT NULL auto_increment,
  `con_name` varchar(200) collate utf8_unicode_ci NOT NULL,
  `photo1` varchar(100) collate utf8_unicode_ci NOT NULL,
  `last_edit_time` datetime NOT NULL,
  PRIMARY KEY  (`con_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=13 ;

-- 
-- Dumping data for table `supplier_con_country`
-- 

INSERT INTO `supplier_con_country` VALUES (001, 'Thailand', '', '2014-04-03 15:58:12');
INSERT INTO `supplier_con_country` VALUES (011, 'Vietnam', '', '2014-04-22 10:35:14');
INSERT INTO `supplier_con_country` VALUES (012, 'Country_Supplier ', '', '2014-05-07 15:22:04');

-- --------------------------------------------------------

-- 
-- Table structure for table `supplier_con_province`
-- 

CREATE TABLE `supplier_con_province` (
  `con_id` int(3) unsigned zerofill NOT NULL auto_increment,
  `con_name` varchar(200) collate utf8_unicode_ci NOT NULL,
  `photo1` varchar(100) collate utf8_unicode_ci NOT NULL,
  `last_edit_time` datetime NOT NULL,
  `country_id` int(11) NOT NULL,
  PRIMARY KEY  (`con_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

-- 
-- Dumping data for table `supplier_con_province`
-- 

INSERT INTO `supplier_con_province` VALUES (001, 'Phuket', '', '2014-04-03 16:31:43', 1);
INSERT INTO `supplier_con_province` VALUES (002, 'Krabi', '', '2014-04-03 16:16:21', 1);
INSERT INTO `supplier_con_province` VALUES (003, 'Province_Supplier ', '', '2014-05-07 15:28:10', 12);
INSERT INTO `supplier_con_province` VALUES (004, 'Bangkok', '', '2014-07-16 15:55:04', 1);

-- --------------------------------------------------------

-- 
-- Table structure for table `supplier_con_servicetype`
-- 

CREATE TABLE `supplier_con_servicetype` (
  `con_id` int(3) unsigned zerofill NOT NULL auto_increment,
  `con_name` varchar(200) collate utf8_unicode_ci NOT NULL,
  `photo1` varchar(100) collate utf8_unicode_ci NOT NULL,
  `last_edit_time` datetime NOT NULL,
  PRIMARY KEY  (`con_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=19 ;

-- 
-- Dumping data for table `supplier_con_servicetype`
-- 

INSERT INTO `supplier_con_servicetype` VALUES (012, 'Test Type Of Service 01', '', '2014-05-07 15:38:10');
INSERT INTO `supplier_con_servicetype` VALUES (010, 'Hotel', '', '2014-04-28 17:12:33');
INSERT INTO `supplier_con_servicetype` VALUES (014, 'TOUR / TRANSFER', '', '2014-07-26 14:03:40');
INSERT INTO `supplier_con_servicetype` VALUES (018, 'Restaurant / Cafe', '', '2014-07-26 14:04:35');
INSERT INTO `supplier_con_servicetype` VALUES (016, 'Entertainment / Show', '', '2014-07-17 11:26:56');
INSERT INTO `supplier_con_servicetype` VALUES (017, 'Spa / Massage', '', '2014-07-17 11:27:07');

-- --------------------------------------------------------

-- 
-- Table structure for table `tours`
-- 

CREATE TABLE `tours` (
  `tou_id` int(11) NOT NULL auto_increment,
  `category_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `province_id` int(11) NOT NULL,
  `area_id` int(11) NOT NULL,
  `tou_name` varchar(200) collate utf8_unicode_ci NOT NULL,
  `tou_detail` text collate utf8_unicode_ci NOT NULL,
  `photo1` varchar(100) collate utf8_unicode_ci NOT NULL,
  `photo2` varchar(100) collate utf8_unicode_ci NOT NULL,
  `photo3` varchar(100) collate utf8_unicode_ci NOT NULL,
  `photo4` varchar(100) collate utf8_unicode_ci NOT NULL,
  `photo5` varchar(100) collate utf8_unicode_ci NOT NULL,
  `photo6` varchar(100) collate utf8_unicode_ci NOT NULL,
  `photo7` varchar(100) collate utf8_unicode_ci NOT NULL,
  `photo8` varchar(100) collate utf8_unicode_ci NOT NULL,
  `photo9` varchar(100) collate utf8_unicode_ci NOT NULL,
  `photo10` varchar(100) collate utf8_unicode_ci NOT NULL,
  `photo11` varchar(100) collate utf8_unicode_ci NOT NULL,
  `photo12` varchar(100) collate utf8_unicode_ci NOT NULL,
  `photo13` varchar(100) collate utf8_unicode_ci NOT NULL,
  `photo14` varchar(100) collate utf8_unicode_ci NOT NULL,
  `photo15` varchar(100) collate utf8_unicode_ci NOT NULL,
  `photo16` varchar(100) collate utf8_unicode_ci NOT NULL,
  `photo17` varchar(100) collate utf8_unicode_ci NOT NULL,
  `photo18` varchar(100) collate utf8_unicode_ci NOT NULL,
  `photo19` varchar(100) collate utf8_unicode_ci NOT NULL,
  `photo20` varchar(100) collate utf8_unicode_ci NOT NULL,
  `tou_ref` varchar(100) collate utf8_unicode_ci NOT NULL,
  `tou_pro_time_arr` text collate utf8_unicode_ci NOT NULL,
  `tou_pro_des_arr` text collate utf8_unicode_ci NOT NULL,
  `tou_remark` text collate utf8_unicode_ci NOT NULL,
  `tou_condition` text collate utf8_unicode_ci NOT NULL,
  `tou_shortdetail` text collate utf8_unicode_ci NOT NULL,
  `tourpricetype_id` int(11) NOT NULL,
  `last_edit_time` datetime NOT NULL,
  `agents_sup_id` int(11) NOT NULL,
  `tou_hot` int(1) NOT NULL,
  PRIMARY KEY  (`tou_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

-- 
-- Dumping data for table `tours`
-- 

INSERT INTO `tours` VALUES (1, 7, 1, 1, 7, 'เดินเขารังชมวิวภูเก็ต', 'test Description Description Description Description ', '1397036367_1.jpg', '1396594823_2.jpg', '1396594823_3.jpg', '1396594823_4.jpg', '1396594823_5.jpg', '1396594823_6.jpg', '1396594823_7.jpg', '1396594823_8.jpg', '1396594823_9.jpg', '', '1396594823_11.jpg', '1396594933_12.jpg', '1396594933_13.jpg', '1396594933_14.jpg', '1396594933_15.jpg', '1396594933_16.jpg', '1396594933_17.jpg', '', '', '', 'TPK001', '~07:30 - 08:00 am.~09:00 am.~12:30 pm.~13:30 - 16:00 pm.~16.00 - 16:30 pm.~t6~t7~t8~t9~t10~', '~Description1~Description2~Description3~Description4~Description5~Description6~Description7~Description8~Description9~Description10~', 'Remark Remark Remark Remark Remark Remark Remark ', 'Condition Condition Condition Condition \r\nCondition Condition Condition Condition ', 'test Short DescriptionShort DescriptionShort Description', 2, '2014-04-09 16:39:27', 13, 0);
INSERT INTO `tours` VALUES (2, 10, 1, 1, 9, 'Phi Phi Island Tour (by Island Hopper)', 'This Phi Phi Island tour by speedboat\r\nThis Phi Phi Island tour by speedboat\r\nThis Phi Phi Island tour by speedboat\r\nThis Phi Phi Island tour by speedboat\r\nThis Phi Phi Island tour by speedboat\r\nThis Phi Phi Island tour by speedboat\r\nThis Phi Phi Island tour by speedboat\r\nThis Phi Phi Island tour by speedboat', '1397037267_1.jpg', '1397037267_2.jpg', '1397037267_3.jpg', '1397037267_4.jpg', '1397037267_5.jpg', '1397037267_6.jpg', '1397037267_7.jpg', '', '', '', '', '', '', '', '', '', '', '', '', '', 'HKTSP0001', '~07.45-08.00~08.00-08.30~08.30-11.30~11.30-15.00~~~~~~~', '~Pick-up from your hotel and transfer to Port Island Hopper.~Arrive at Port for breakfast (coffee, tea and pineapple biscuits).~Khai Nui island where are very exciting such as the clear sea water with tropical fishes and many kind of coral reefs. Enjoy snorkelling where you will discover sea secret world,also feeding fishes.~Enjoy set menu luncluded easy B.B.Q. Seafood.~~~~~~~', 'กรุณาอย่านำโทรศัพท์มาเนื่องจากมีการสูญหายบ่อยครั้ง', 'หากโทรศัพท์ของท่านสูญหายหรือเปียกน้ำ ทางบริษัทจะไม่รับผิดชอบ', 'This Phi Phi Island tour by speedboat\r\nThis Phi Phi Island tour by speedboat', 1, '2014-07-17 11:22:15', 1, 0);
INSERT INTO `tours` VALUES (3, 1, 1, 1, 8, 'James Bond Tour', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'JB0002', '~08.00~09.00~~~~~~~~~', '~Pick up from Hotel~Departure from Ao-Por pier~~~~~~~~~', '', '', '', 2, '2014-05-26 16:52:16', 0, 0);
INSERT INTO `tours` VALUES (4, 1, 1, 1, 8, 'PP Day Trip with Lunch ', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'PP0002', '~~~~~~~~~~~', '~~~~~~~~~~~', '', '', '', 2, '2014-05-09 17:34:15', 1, 0);

-- --------------------------------------------------------

-- 
-- Table structure for table `tour_allocation`
-- 

CREATE TABLE `tour_allocation` (
  `touallo_id` int(11) NOT NULL auto_increment,
  `tours_id` int(11) NOT NULL,
  `tourratetype_id` int(11) NOT NULL,
  `touallo_datefrom` date NOT NULL,
  `touallo_dateto` date NOT NULL,
  `touallo_global` int(11) NOT NULL,
  `last_edit_time` datetime NOT NULL,
  PRIMARY KEY  (`touallo_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=11 ;

-- 
-- Dumping data for table `tour_allocation`
-- 

INSERT INTO `tour_allocation` VALUES (1, 1, 3, '2014-01-01', '2014-03-21', 77, '2014-04-08 17:43:09');
INSERT INTO `tour_allocation` VALUES (2, 1, 2, '2014-01-01', '2014-01-20', 88, '2014-04-08 17:43:44');
INSERT INTO `tour_allocation` VALUES (3, 1, 1, '2014-01-01', '2014-01-18', 100, '2014-04-08 17:55:01');
INSERT INTO `tour_allocation` VALUES (4, 1, 1, '2014-01-19', '2014-01-24', 120, '2014-04-08 17:57:21');
INSERT INTO `tour_allocation` VALUES (5, 2, 8, '2014-01-01', '2014-04-30', 999, '2014-04-09 17:21:05');
INSERT INTO `tour_allocation` VALUES (6, 2, 9, '2014-01-01', '2014-04-30', 9999, '2014-04-09 17:22:22');
INSERT INTO `tour_allocation` VALUES (7, 2, 8, '2014-05-01', '2014-05-31', 8888, '2014-04-09 17:28:24');
INSERT INTO `tour_allocation` VALUES (8, 3, 11, '2014-04-23', '2014-04-23', 5, '2014-04-23 16:41:02');
INSERT INTO `tour_allocation` VALUES (9, 4, 12, '2014-04-24', '2014-10-24', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocation` VALUES (10, 3, 11, '2014-01-24', '2014-12-24', 0, '2014-04-24 13:12:41');

-- --------------------------------------------------------

-- 
-- Table structure for table `tour_allocationdaily`
-- 

CREATE TABLE `tour_allocationdaily` (
  `toualloday_id` int(11) NOT NULL auto_increment,
  `tours_id` int(11) NOT NULL,
  `tourratetype_id` int(11) NOT NULL,
  `toualloday_date` date NOT NULL,
  `toualloday_value` int(11) NOT NULL,
  `last_edit_time` datetime NOT NULL,
  PRIMARY KEY  (`toualloday_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1050 ;

-- 
-- Dumping data for table `tour_allocationdaily`
-- 

INSERT INTO `tour_allocationdaily` VALUES (1, 1, 3, '2014-01-01', 77, '2014-04-08 17:44:36');
INSERT INTO `tour_allocationdaily` VALUES (2, 1, 3, '2014-01-02', 77, '2014-04-08 17:44:36');
INSERT INTO `tour_allocationdaily` VALUES (3, 1, 3, '2014-01-03', 77, '2014-04-08 17:44:36');
INSERT INTO `tour_allocationdaily` VALUES (4, 1, 3, '2014-01-04', 77, '2014-04-08 17:44:36');
INSERT INTO `tour_allocationdaily` VALUES (5, 1, 3, '2014-01-05', 77, '2014-04-08 17:44:36');
INSERT INTO `tour_allocationdaily` VALUES (6, 1, 3, '2014-01-06', 77, '2014-04-08 17:44:36');
INSERT INTO `tour_allocationdaily` VALUES (7, 1, 3, '2014-01-07', 77, '2014-04-08 17:44:36');
INSERT INTO `tour_allocationdaily` VALUES (8, 1, 3, '2014-01-08', 77, '2014-04-08 17:44:36');
INSERT INTO `tour_allocationdaily` VALUES (9, 1, 3, '2014-01-09', 77, '2014-04-08 17:44:36');
INSERT INTO `tour_allocationdaily` VALUES (10, 1, 3, '2014-01-10', 77, '2014-04-08 17:44:36');
INSERT INTO `tour_allocationdaily` VALUES (11, 1, 3, '2014-01-11', 77, '2014-04-08 17:44:36');
INSERT INTO `tour_allocationdaily` VALUES (12, 1, 3, '2014-01-12', 77, '2014-04-08 17:44:36');
INSERT INTO `tour_allocationdaily` VALUES (13, 1, 3, '2014-01-13', 77, '2014-04-08 17:44:36');
INSERT INTO `tour_allocationdaily` VALUES (14, 1, 3, '2014-01-14', 77, '2014-04-08 17:44:36');
INSERT INTO `tour_allocationdaily` VALUES (15, 1, 3, '2014-01-15', 77, '2014-04-08 17:44:36');
INSERT INTO `tour_allocationdaily` VALUES (16, 1, 3, '2014-01-16', 77, '2014-04-08 17:44:36');
INSERT INTO `tour_allocationdaily` VALUES (17, 1, 3, '2014-01-17', 77, '2014-04-08 17:44:36');
INSERT INTO `tour_allocationdaily` VALUES (18, 1, 3, '2014-01-18', 77, '2014-04-08 17:44:36');
INSERT INTO `tour_allocationdaily` VALUES (19, 1, 3, '2014-01-19', 77, '2014-04-08 17:44:36');
INSERT INTO `tour_allocationdaily` VALUES (20, 1, 3, '2014-01-20', 77, '2014-04-08 17:44:36');
INSERT INTO `tour_allocationdaily` VALUES (21, 1, 3, '2014-01-21', 77, '2014-04-08 17:44:36');
INSERT INTO `tour_allocationdaily` VALUES (22, 1, 3, '2014-01-22', 77, '2014-04-08 17:44:36');
INSERT INTO `tour_allocationdaily` VALUES (23, 1, 3, '2014-01-23', 77, '2014-04-08 17:44:36');
INSERT INTO `tour_allocationdaily` VALUES (24, 1, 3, '2014-01-24', 77, '2014-04-08 17:44:36');
INSERT INTO `tour_allocationdaily` VALUES (25, 1, 3, '2014-01-25', 77, '2014-04-08 17:44:36');
INSERT INTO `tour_allocationdaily` VALUES (26, 1, 3, '2014-01-26', 77, '2014-04-08 17:44:36');
INSERT INTO `tour_allocationdaily` VALUES (27, 1, 3, '2014-01-27', 77, '2014-04-08 17:44:36');
INSERT INTO `tour_allocationdaily` VALUES (28, 1, 3, '2014-01-28', 77, '2014-04-08 17:44:36');
INSERT INTO `tour_allocationdaily` VALUES (29, 1, 3, '2014-01-29', 77, '2014-04-08 17:44:36');
INSERT INTO `tour_allocationdaily` VALUES (30, 1, 3, '2014-01-30', 77, '2014-04-08 17:44:36');
INSERT INTO `tour_allocationdaily` VALUES (31, 1, 3, '2014-01-31', 77, '2014-04-08 17:44:36');
INSERT INTO `tour_allocationdaily` VALUES (32, 1, 3, '2014-02-01', 77, '2014-04-08 17:44:36');
INSERT INTO `tour_allocationdaily` VALUES (33, 1, 3, '2014-02-02', 77, '2014-04-08 17:44:36');
INSERT INTO `tour_allocationdaily` VALUES (34, 1, 3, '2014-02-03', 77, '2014-04-08 17:44:36');
INSERT INTO `tour_allocationdaily` VALUES (35, 1, 3, '2014-02-04', 77, '2014-04-08 17:44:36');
INSERT INTO `tour_allocationdaily` VALUES (36, 1, 3, '2014-02-05', 77, '2014-04-08 17:44:36');
INSERT INTO `tour_allocationdaily` VALUES (37, 1, 3, '2014-02-06', 77, '2014-04-08 17:44:36');
INSERT INTO `tour_allocationdaily` VALUES (38, 1, 3, '2014-02-07', 77, '2014-04-08 17:44:36');
INSERT INTO `tour_allocationdaily` VALUES (39, 1, 3, '2014-02-08', 77, '2014-04-08 17:44:36');
INSERT INTO `tour_allocationdaily` VALUES (40, 1, 3, '2014-02-09', 77, '2014-04-08 17:44:36');
INSERT INTO `tour_allocationdaily` VALUES (41, 1, 3, '2014-02-10', 77, '2014-04-08 17:44:36');
INSERT INTO `tour_allocationdaily` VALUES (42, 1, 3, '2014-02-11', 77, '2014-04-08 17:44:36');
INSERT INTO `tour_allocationdaily` VALUES (43, 1, 3, '2014-02-12', 77, '2014-04-08 17:44:36');
INSERT INTO `tour_allocationdaily` VALUES (44, 1, 3, '2014-02-13', 77, '2014-04-08 17:44:36');
INSERT INTO `tour_allocationdaily` VALUES (45, 1, 3, '2014-02-14', 77, '2014-04-08 17:44:36');
INSERT INTO `tour_allocationdaily` VALUES (46, 1, 3, '2014-02-15', 77, '2014-04-08 17:44:36');
INSERT INTO `tour_allocationdaily` VALUES (47, 1, 3, '2014-02-16', 77, '2014-04-08 17:44:36');
INSERT INTO `tour_allocationdaily` VALUES (48, 1, 3, '2014-02-17', 77, '2014-04-08 17:44:36');
INSERT INTO `tour_allocationdaily` VALUES (49, 1, 3, '2014-02-18', 77, '2014-04-08 17:44:36');
INSERT INTO `tour_allocationdaily` VALUES (50, 1, 3, '2014-02-19', 77, '2014-04-08 17:44:36');
INSERT INTO `tour_allocationdaily` VALUES (51, 1, 3, '2014-02-20', 77, '2014-04-08 17:44:36');
INSERT INTO `tour_allocationdaily` VALUES (52, 1, 3, '2014-02-21', 77, '2014-04-08 17:44:36');
INSERT INTO `tour_allocationdaily` VALUES (53, 1, 3, '2014-02-22', 77, '2014-04-08 17:44:36');
INSERT INTO `tour_allocationdaily` VALUES (54, 1, 3, '2014-02-23', 77, '2014-04-08 17:44:36');
INSERT INTO `tour_allocationdaily` VALUES (55, 1, 3, '2014-02-24', 77, '2014-04-08 17:44:36');
INSERT INTO `tour_allocationdaily` VALUES (56, 1, 3, '2014-02-25', 77, '2014-04-08 17:44:36');
INSERT INTO `tour_allocationdaily` VALUES (57, 1, 3, '2014-02-26', 77, '2014-04-08 17:44:36');
INSERT INTO `tour_allocationdaily` VALUES (58, 1, 3, '2014-02-27', 77, '2014-04-08 17:44:36');
INSERT INTO `tour_allocationdaily` VALUES (59, 1, 3, '2014-02-28', 77, '2014-04-08 17:44:36');
INSERT INTO `tour_allocationdaily` VALUES (60, 1, 3, '2014-03-01', 77, '2014-04-08 17:45:29');
INSERT INTO `tour_allocationdaily` VALUES (61, 1, 3, '2014-03-02', 77, '2014-04-08 17:45:29');
INSERT INTO `tour_allocationdaily` VALUES (62, 1, 3, '2014-03-03', 77, '2014-04-08 17:45:29');
INSERT INTO `tour_allocationdaily` VALUES (63, 1, 3, '2014-03-04', 77, '2014-04-08 17:45:29');
INSERT INTO `tour_allocationdaily` VALUES (64, 1, 3, '2014-03-05', 77, '2014-04-08 17:45:29');
INSERT INTO `tour_allocationdaily` VALUES (65, 1, 3, '2014-03-06', 77, '2014-04-08 17:45:29');
INSERT INTO `tour_allocationdaily` VALUES (66, 1, 3, '2014-03-07', 77, '2014-04-08 17:45:29');
INSERT INTO `tour_allocationdaily` VALUES (67, 1, 3, '2014-03-08', 77, '2014-04-08 17:45:29');
INSERT INTO `tour_allocationdaily` VALUES (68, 1, 3, '2014-03-09', 77, '2014-04-08 17:45:29');
INSERT INTO `tour_allocationdaily` VALUES (69, 1, 3, '2014-03-10', 77, '2014-04-08 17:45:29');
INSERT INTO `tour_allocationdaily` VALUES (70, 1, 3, '2014-03-11', 77, '2014-04-08 17:45:29');
INSERT INTO `tour_allocationdaily` VALUES (71, 1, 3, '2014-03-12', 77, '2014-04-08 17:45:29');
INSERT INTO `tour_allocationdaily` VALUES (72, 1, 3, '2014-03-13', 77, '2014-04-08 17:45:29');
INSERT INTO `tour_allocationdaily` VALUES (73, 1, 3, '2014-03-14', 77, '2014-04-08 17:45:29');
INSERT INTO `tour_allocationdaily` VALUES (74, 1, 3, '2014-03-15', 77, '2014-04-08 17:45:29');
INSERT INTO `tour_allocationdaily` VALUES (75, 1, 3, '2014-03-16', 77, '2014-04-08 17:45:29');
INSERT INTO `tour_allocationdaily` VALUES (76, 1, 3, '2014-03-17', 77, '2014-04-08 17:45:29');
INSERT INTO `tour_allocationdaily` VALUES (77, 1, 3, '2014-03-18', 77, '2014-04-08 17:45:29');
INSERT INTO `tour_allocationdaily` VALUES (78, 1, 3, '2014-03-19', 77, '2014-04-08 17:45:29');
INSERT INTO `tour_allocationdaily` VALUES (79, 1, 3, '2014-03-20', 77, '2014-04-08 17:45:29');
INSERT INTO `tour_allocationdaily` VALUES (80, 1, 3, '2014-03-21', 77, '2014-04-08 17:45:29');
INSERT INTO `tour_allocationdaily` VALUES (81, 1, 2, '2014-01-01', 88, '2014-04-08 17:43:44');
INSERT INTO `tour_allocationdaily` VALUES (82, 1, 2, '2014-01-02', 88, '2014-04-08 17:43:44');
INSERT INTO `tour_allocationdaily` VALUES (83, 1, 2, '2014-01-03', 88, '2014-04-08 17:43:44');
INSERT INTO `tour_allocationdaily` VALUES (84, 1, 2, '2014-01-04', 88, '2014-04-08 17:43:44');
INSERT INTO `tour_allocationdaily` VALUES (85, 1, 2, '2014-01-05', 88, '2014-04-08 17:43:44');
INSERT INTO `tour_allocationdaily` VALUES (86, 1, 2, '2014-01-06', 88, '2014-04-08 17:43:44');
INSERT INTO `tour_allocationdaily` VALUES (87, 1, 2, '2014-01-07', 88, '2014-04-08 17:43:44');
INSERT INTO `tour_allocationdaily` VALUES (88, 1, 2, '2014-01-08', 88, '2014-04-08 18:04:34');
INSERT INTO `tour_allocationdaily` VALUES (89, 1, 2, '2014-01-09', 88, '2014-04-08 18:04:34');
INSERT INTO `tour_allocationdaily` VALUES (90, 1, 2, '2014-01-10', 88, '2014-04-08 18:04:34');
INSERT INTO `tour_allocationdaily` VALUES (91, 1, 2, '2014-01-11', 88, '2014-04-08 18:04:34');
INSERT INTO `tour_allocationdaily` VALUES (92, 1, 2, '2014-01-12', 88, '2014-04-08 18:04:34');
INSERT INTO `tour_allocationdaily` VALUES (93, 1, 2, '2014-01-13', 88, '2014-04-08 18:04:34');
INSERT INTO `tour_allocationdaily` VALUES (94, 1, 2, '2014-01-14', 88, '2014-04-08 18:04:34');
INSERT INTO `tour_allocationdaily` VALUES (95, 1, 2, '2014-01-15', 88, '2014-04-08 18:04:34');
INSERT INTO `tour_allocationdaily` VALUES (96, 1, 2, '2014-01-16', 88, '2014-04-08 18:04:34');
INSERT INTO `tour_allocationdaily` VALUES (97, 1, 2, '2014-01-17', 88, '2014-04-08 18:04:34');
INSERT INTO `tour_allocationdaily` VALUES (98, 1, 2, '2014-01-18', 88, '2014-04-08 18:04:34');
INSERT INTO `tour_allocationdaily` VALUES (99, 1, 2, '2014-01-19', 88, '2014-04-08 18:04:34');
INSERT INTO `tour_allocationdaily` VALUES (100, 1, 2, '2014-01-20', 88, '2014-04-08 18:04:34');
INSERT INTO `tour_allocationdaily` VALUES (101, 1, 3, '2014-03-22', 999, '2014-04-08 17:45:29');
INSERT INTO `tour_allocationdaily` VALUES (102, 1, 3, '2014-03-23', 999, '2014-04-08 17:45:29');
INSERT INTO `tour_allocationdaily` VALUES (103, 1, 3, '2014-03-24', 999, '2014-04-08 17:45:29');
INSERT INTO `tour_allocationdaily` VALUES (104, 1, 3, '2014-03-25', 999, '2014-04-08 17:45:29');
INSERT INTO `tour_allocationdaily` VALUES (105, 1, 3, '2014-03-27', 27, '2014-04-08 17:45:29');
INSERT INTO `tour_allocationdaily` VALUES (106, 1, 1, '2014-01-01', 0, '2014-08-14 10:50:28');
INSERT INTO `tour_allocationdaily` VALUES (107, 1, 1, '2014-01-02', 100, '2014-08-14 10:51:12');
INSERT INTO `tour_allocationdaily` VALUES (108, 1, 1, '2014-01-03', 100, '2014-08-14 10:51:12');
INSERT INTO `tour_allocationdaily` VALUES (109, 1, 1, '2014-01-04', 100, '2014-08-14 10:51:12');
INSERT INTO `tour_allocationdaily` VALUES (110, 1, 1, '2014-01-05', 100, '2014-08-14 10:51:12');
INSERT INTO `tour_allocationdaily` VALUES (111, 1, 1, '2014-01-06', 100, '2014-08-14 10:51:12');
INSERT INTO `tour_allocationdaily` VALUES (112, 1, 1, '2014-01-07', 100, '2014-08-14 10:51:12');
INSERT INTO `tour_allocationdaily` VALUES (113, 1, 1, '2014-01-08', 100, '2014-08-14 10:51:12');
INSERT INTO `tour_allocationdaily` VALUES (114, 1, 1, '2014-01-09', 100, '2014-08-14 10:51:12');
INSERT INTO `tour_allocationdaily` VALUES (115, 1, 1, '2014-01-10', 100, '2014-08-14 10:51:12');
INSERT INTO `tour_allocationdaily` VALUES (116, 1, 1, '2014-01-11', 100, '2014-08-14 10:51:12');
INSERT INTO `tour_allocationdaily` VALUES (117, 1, 1, '2014-01-12', 100, '2014-08-14 10:51:12');
INSERT INTO `tour_allocationdaily` VALUES (118, 1, 1, '2014-01-13', 100, '2014-08-14 10:51:12');
INSERT INTO `tour_allocationdaily` VALUES (119, 1, 1, '2014-01-14', 100, '2014-08-14 10:51:12');
INSERT INTO `tour_allocationdaily` VALUES (120, 1, 1, '2014-01-15', 100, '2014-08-14 10:51:12');
INSERT INTO `tour_allocationdaily` VALUES (121, 1, 1, '2014-01-16', 100, '2014-08-14 10:50:28');
INSERT INTO `tour_allocationdaily` VALUES (122, 1, 1, '2014-01-17', 100, '2014-08-14 10:50:28');
INSERT INTO `tour_allocationdaily` VALUES (123, 1, 1, '2014-01-18', 100, '2014-08-14 10:50:28');
INSERT INTO `tour_allocationdaily` VALUES (124, 1, 1, '2014-01-19', 120, '2014-08-14 10:50:28');
INSERT INTO `tour_allocationdaily` VALUES (125, 1, 1, '2014-01-20', 120, '2014-08-14 10:50:28');
INSERT INTO `tour_allocationdaily` VALUES (126, 1, 1, '2014-01-21', 120, '2014-08-14 10:50:28');
INSERT INTO `tour_allocationdaily` VALUES (127, 1, 1, '2014-01-22', 120, '2014-08-14 10:50:28');
INSERT INTO `tour_allocationdaily` VALUES (128, 1, 1, '2014-01-23', 0, '2014-08-14 10:50:28');
INSERT INTO `tour_allocationdaily` VALUES (129, 1, 1, '2014-01-24', 0, '2014-08-14 10:50:28');
INSERT INTO `tour_allocationdaily` VALUES (130, 1, 2, '2014-02-10', 10, '2014-04-08 18:03:30');
INSERT INTO `tour_allocationdaily` VALUES (131, 1, 2, '2014-02-11', 11, '2014-04-08 18:03:30');
INSERT INTO `tour_allocationdaily` VALUES (132, 1, 2, '2014-02-12', 12, '2014-04-08 18:03:30');
INSERT INTO `tour_allocationdaily` VALUES (133, 1, 2, '2014-02-13', 133, '2014-04-08 18:03:30');
INSERT INTO `tour_allocationdaily` VALUES (134, 1, 2, '2014-03-11', 111, '2014-04-08 18:03:30');
INSERT INTO `tour_allocationdaily` VALUES (135, 1, 2, '2014-01-25', 255, '2014-04-08 18:04:34');
INSERT INTO `tour_allocationdaily` VALUES (136, 1, 2, '2014-01-26', 266, '2014-04-08 18:04:34');
INSERT INTO `tour_allocationdaily` VALUES (137, 1, 2, '2014-01-27', 277, '2014-04-08 18:04:34');
INSERT INTO `tour_allocationdaily` VALUES (138, 1, 2, '2014-02-07', 77, '2014-04-08 18:04:34');
INSERT INTO `tour_allocationdaily` VALUES (139, 1, 2, '2014-02-08', 88, '2014-04-08 18:04:34');
INSERT INTO `tour_allocationdaily` VALUES (140, 2, 8, '2014-01-01', 999, '2014-04-09 17:21:05');
INSERT INTO `tour_allocationdaily` VALUES (141, 2, 8, '2014-01-02', 999, '2014-04-09 17:21:05');
INSERT INTO `tour_allocationdaily` VALUES (142, 2, 8, '2014-01-03', 999, '2014-04-09 17:21:05');
INSERT INTO `tour_allocationdaily` VALUES (143, 2, 8, '2014-01-04', 999, '2014-04-09 17:21:05');
INSERT INTO `tour_allocationdaily` VALUES (144, 2, 8, '2014-01-05', 999, '2014-04-09 17:21:05');
INSERT INTO `tour_allocationdaily` VALUES (145, 2, 8, '2014-01-06', 999, '2014-04-09 17:21:05');
INSERT INTO `tour_allocationdaily` VALUES (146, 2, 8, '2014-01-07', 999, '2014-04-09 17:21:05');
INSERT INTO `tour_allocationdaily` VALUES (147, 2, 8, '2014-01-08', 999, '2014-04-09 17:21:05');
INSERT INTO `tour_allocationdaily` VALUES (148, 2, 8, '2014-01-09', 999, '2014-04-09 17:21:05');
INSERT INTO `tour_allocationdaily` VALUES (149, 2, 8, '2014-01-10', 999, '2014-04-09 17:21:05');
INSERT INTO `tour_allocationdaily` VALUES (150, 2, 8, '2014-01-11', 999, '2014-04-09 17:21:05');
INSERT INTO `tour_allocationdaily` VALUES (151, 2, 8, '2014-01-12', 999, '2014-04-09 17:21:05');
INSERT INTO `tour_allocationdaily` VALUES (152, 2, 8, '2014-01-13', 999, '2014-04-09 17:21:05');
INSERT INTO `tour_allocationdaily` VALUES (153, 2, 8, '2014-01-14', 999, '2014-04-09 17:21:05');
INSERT INTO `tour_allocationdaily` VALUES (154, 2, 8, '2014-01-15', 999, '2014-04-09 17:21:05');
INSERT INTO `tour_allocationdaily` VALUES (155, 2, 8, '2014-01-16', 999, '2014-04-09 17:21:05');
INSERT INTO `tour_allocationdaily` VALUES (156, 2, 8, '2014-01-17', 999, '2014-04-09 17:21:05');
INSERT INTO `tour_allocationdaily` VALUES (157, 2, 8, '2014-01-18', 999, '2014-04-09 17:21:05');
INSERT INTO `tour_allocationdaily` VALUES (158, 2, 8, '2014-01-19', 999, '2014-04-09 17:21:05');
INSERT INTO `tour_allocationdaily` VALUES (159, 2, 8, '2014-01-20', 999, '2014-04-09 17:21:05');
INSERT INTO `tour_allocationdaily` VALUES (160, 2, 8, '2014-01-21', 999, '2014-04-09 17:21:05');
INSERT INTO `tour_allocationdaily` VALUES (161, 2, 8, '2014-01-22', 999, '2014-04-09 17:21:05');
INSERT INTO `tour_allocationdaily` VALUES (162, 2, 8, '2014-01-23', 999, '2014-04-09 17:21:05');
INSERT INTO `tour_allocationdaily` VALUES (163, 2, 8, '2014-01-24', 999, '2014-04-09 17:21:05');
INSERT INTO `tour_allocationdaily` VALUES (164, 2, 8, '2014-01-25', 999, '2014-04-09 17:21:05');
INSERT INTO `tour_allocationdaily` VALUES (165, 2, 8, '2014-01-26', 999, '2014-04-09 17:21:05');
INSERT INTO `tour_allocationdaily` VALUES (166, 2, 8, '2014-01-27', 999, '2014-04-09 17:21:05');
INSERT INTO `tour_allocationdaily` VALUES (167, 2, 8, '2014-01-28', 999, '2014-04-09 17:21:05');
INSERT INTO `tour_allocationdaily` VALUES (168, 2, 8, '2014-01-29', 999, '2014-04-09 17:21:05');
INSERT INTO `tour_allocationdaily` VALUES (169, 2, 8, '2014-01-30', 999, '2014-04-09 17:21:05');
INSERT INTO `tour_allocationdaily` VALUES (170, 2, 8, '2014-01-31', 999, '2014-04-09 17:21:05');
INSERT INTO `tour_allocationdaily` VALUES (171, 2, 8, '2014-02-01', 999, '2014-04-09 17:21:05');
INSERT INTO `tour_allocationdaily` VALUES (172, 2, 8, '2014-02-02', 999, '2014-04-09 17:21:05');
INSERT INTO `tour_allocationdaily` VALUES (173, 2, 8, '2014-02-03', 999, '2014-04-09 17:21:05');
INSERT INTO `tour_allocationdaily` VALUES (174, 2, 8, '2014-02-04', 999, '2014-04-09 17:21:05');
INSERT INTO `tour_allocationdaily` VALUES (175, 2, 8, '2014-02-05', 999, '2014-04-09 17:21:05');
INSERT INTO `tour_allocationdaily` VALUES (176, 2, 8, '2014-02-06', 999, '2014-04-09 17:21:05');
INSERT INTO `tour_allocationdaily` VALUES (177, 2, 8, '2014-02-07', 999, '2014-04-09 17:21:05');
INSERT INTO `tour_allocationdaily` VALUES (178, 2, 8, '2014-02-08', 999, '2014-04-09 17:21:05');
INSERT INTO `tour_allocationdaily` VALUES (179, 2, 8, '2014-02-09', 999, '2014-04-09 17:21:05');
INSERT INTO `tour_allocationdaily` VALUES (180, 2, 8, '2014-02-10', 999, '2014-04-09 17:21:05');
INSERT INTO `tour_allocationdaily` VALUES (181, 2, 8, '2014-02-11', 999, '2014-04-09 17:21:05');
INSERT INTO `tour_allocationdaily` VALUES (182, 2, 8, '2014-02-12', 999, '2014-04-09 17:21:05');
INSERT INTO `tour_allocationdaily` VALUES (183, 2, 8, '2014-02-13', 999, '2014-04-09 17:21:05');
INSERT INTO `tour_allocationdaily` VALUES (184, 2, 8, '2014-02-14', 999, '2014-04-09 17:21:05');
INSERT INTO `tour_allocationdaily` VALUES (185, 2, 8, '2014-02-15', 999, '2014-04-09 17:21:05');
INSERT INTO `tour_allocationdaily` VALUES (186, 2, 8, '2014-02-16', 999, '2014-04-09 17:21:05');
INSERT INTO `tour_allocationdaily` VALUES (187, 2, 8, '2014-02-17', 999, '2014-04-09 17:21:05');
INSERT INTO `tour_allocationdaily` VALUES (188, 2, 8, '2014-02-18', 999, '2014-04-09 17:21:05');
INSERT INTO `tour_allocationdaily` VALUES (189, 2, 8, '2014-02-19', 999, '2014-04-09 17:21:05');
INSERT INTO `tour_allocationdaily` VALUES (190, 2, 8, '2014-02-20', 999, '2014-04-09 17:21:05');
INSERT INTO `tour_allocationdaily` VALUES (191, 2, 8, '2014-02-21', 999, '2014-04-09 17:21:05');
INSERT INTO `tour_allocationdaily` VALUES (192, 2, 8, '2014-02-22', 999, '2014-04-09 17:21:05');
INSERT INTO `tour_allocationdaily` VALUES (193, 2, 8, '2014-02-23', 999, '2014-04-09 17:21:05');
INSERT INTO `tour_allocationdaily` VALUES (194, 2, 8, '2014-02-24', 999, '2014-04-09 17:21:05');
INSERT INTO `tour_allocationdaily` VALUES (195, 2, 8, '2014-02-25', 999, '2014-04-09 17:21:05');
INSERT INTO `tour_allocationdaily` VALUES (196, 2, 8, '2014-02-26', 999, '2014-04-09 17:21:05');
INSERT INTO `tour_allocationdaily` VALUES (197, 2, 8, '2014-02-27', 999, '2014-04-09 17:21:05');
INSERT INTO `tour_allocationdaily` VALUES (198, 2, 8, '2014-02-28', 999, '2014-04-09 17:21:05');
INSERT INTO `tour_allocationdaily` VALUES (199, 2, 8, '2014-03-01', 999, '2014-04-09 17:21:05');
INSERT INTO `tour_allocationdaily` VALUES (200, 2, 8, '2014-03-02', 999, '2014-04-09 17:21:05');
INSERT INTO `tour_allocationdaily` VALUES (201, 2, 8, '2014-03-03', 999, '2014-04-09 17:21:05');
INSERT INTO `tour_allocationdaily` VALUES (202, 2, 8, '2014-03-04', 999, '2014-04-09 17:21:05');
INSERT INTO `tour_allocationdaily` VALUES (203, 2, 8, '2014-03-05', 999, '2014-04-09 17:21:05');
INSERT INTO `tour_allocationdaily` VALUES (204, 2, 8, '2014-03-06', 999, '2014-04-09 17:21:05');
INSERT INTO `tour_allocationdaily` VALUES (205, 2, 8, '2014-03-07', 999, '2014-04-09 17:21:05');
INSERT INTO `tour_allocationdaily` VALUES (206, 2, 8, '2014-03-08', 999, '2014-04-09 17:21:05');
INSERT INTO `tour_allocationdaily` VALUES (207, 2, 8, '2014-03-09', 999, '2014-04-09 17:21:05');
INSERT INTO `tour_allocationdaily` VALUES (208, 2, 8, '2014-03-10', 999, '2014-04-09 17:21:05');
INSERT INTO `tour_allocationdaily` VALUES (209, 2, 8, '2014-03-11', 999, '2014-04-09 17:21:05');
INSERT INTO `tour_allocationdaily` VALUES (210, 2, 8, '2014-03-12', 999, '2014-04-09 17:21:05');
INSERT INTO `tour_allocationdaily` VALUES (211, 2, 8, '2014-03-13', 999, '2014-04-09 17:21:05');
INSERT INTO `tour_allocationdaily` VALUES (212, 2, 8, '2014-03-14', 999, '2014-04-09 17:21:05');
INSERT INTO `tour_allocationdaily` VALUES (213, 2, 8, '2014-03-15', 999, '2014-04-09 17:21:05');
INSERT INTO `tour_allocationdaily` VALUES (214, 2, 8, '2014-03-16', 999, '2014-04-09 17:21:05');
INSERT INTO `tour_allocationdaily` VALUES (215, 2, 8, '2014-03-17', 999, '2014-04-09 17:21:05');
INSERT INTO `tour_allocationdaily` VALUES (216, 2, 8, '2014-03-18', 999, '2014-04-09 17:21:05');
INSERT INTO `tour_allocationdaily` VALUES (217, 2, 8, '2014-03-19', 999, '2014-04-09 17:21:05');
INSERT INTO `tour_allocationdaily` VALUES (218, 2, 8, '2014-03-20', 999, '2014-04-09 17:21:05');
INSERT INTO `tour_allocationdaily` VALUES (219, 2, 8, '2014-03-21', 999, '2014-04-09 17:21:05');
INSERT INTO `tour_allocationdaily` VALUES (220, 2, 8, '2014-03-22', 999, '2014-04-09 17:21:05');
INSERT INTO `tour_allocationdaily` VALUES (221, 2, 8, '2014-03-23', 999, '2014-04-09 17:21:05');
INSERT INTO `tour_allocationdaily` VALUES (222, 2, 8, '2014-03-24', 999, '2014-04-09 17:21:05');
INSERT INTO `tour_allocationdaily` VALUES (223, 2, 8, '2014-03-25', 999, '2014-04-09 17:21:05');
INSERT INTO `tour_allocationdaily` VALUES (224, 2, 8, '2014-03-26', 999, '2014-04-09 17:21:05');
INSERT INTO `tour_allocationdaily` VALUES (225, 2, 8, '2014-03-27', 999, '2014-04-09 17:21:05');
INSERT INTO `tour_allocationdaily` VALUES (226, 2, 8, '2014-03-28', 999, '2014-04-09 17:21:05');
INSERT INTO `tour_allocationdaily` VALUES (227, 2, 8, '2014-03-29', 999, '2014-04-09 17:21:05');
INSERT INTO `tour_allocationdaily` VALUES (228, 2, 8, '2014-03-30', 999, '2014-04-09 17:21:05');
INSERT INTO `tour_allocationdaily` VALUES (229, 2, 8, '2014-03-31', 999, '2014-04-09 17:21:05');
INSERT INTO `tour_allocationdaily` VALUES (230, 2, 8, '2014-04-01', 999, '2014-04-09 17:21:05');
INSERT INTO `tour_allocationdaily` VALUES (231, 2, 8, '2014-04-02', 999, '2014-04-09 17:21:05');
INSERT INTO `tour_allocationdaily` VALUES (232, 2, 8, '2014-04-03', 999, '2014-04-09 17:21:05');
INSERT INTO `tour_allocationdaily` VALUES (233, 2, 8, '2014-04-04', 999, '2014-04-09 17:21:05');
INSERT INTO `tour_allocationdaily` VALUES (234, 2, 8, '2014-04-05', 999, '2014-04-09 17:21:05');
INSERT INTO `tour_allocationdaily` VALUES (235, 2, 8, '2014-04-06', 999, '2014-04-09 17:21:05');
INSERT INTO `tour_allocationdaily` VALUES (236, 2, 8, '2014-04-07', 999, '2014-04-09 17:21:05');
INSERT INTO `tour_allocationdaily` VALUES (237, 2, 8, '2014-04-08', 999, '2014-04-09 17:21:05');
INSERT INTO `tour_allocationdaily` VALUES (238, 2, 8, '2014-04-09', 999, '2014-04-09 17:21:05');
INSERT INTO `tour_allocationdaily` VALUES (239, 2, 8, '2014-04-10', 999, '2014-04-09 17:21:05');
INSERT INTO `tour_allocationdaily` VALUES (240, 2, 8, '2014-04-11', 999, '2014-04-09 17:21:05');
INSERT INTO `tour_allocationdaily` VALUES (241, 2, 8, '2014-04-12', 999, '2014-04-09 17:21:05');
INSERT INTO `tour_allocationdaily` VALUES (242, 2, 8, '2014-04-13', 999, '2014-04-09 17:21:05');
INSERT INTO `tour_allocationdaily` VALUES (243, 2, 8, '2014-04-14', 999, '2014-04-09 17:21:05');
INSERT INTO `tour_allocationdaily` VALUES (244, 2, 8, '2014-04-15', 999, '2014-04-09 17:21:05');
INSERT INTO `tour_allocationdaily` VALUES (245, 2, 8, '2014-04-16', 999, '2014-04-09 17:21:05');
INSERT INTO `tour_allocationdaily` VALUES (246, 2, 8, '2014-04-17', 999, '2014-04-09 17:21:05');
INSERT INTO `tour_allocationdaily` VALUES (247, 2, 8, '2014-04-18', 999, '2014-04-09 17:21:05');
INSERT INTO `tour_allocationdaily` VALUES (248, 2, 8, '2014-04-19', 999, '2014-04-09 17:21:05');
INSERT INTO `tour_allocationdaily` VALUES (249, 2, 8, '2014-04-20', 999, '2014-04-09 17:21:05');
INSERT INTO `tour_allocationdaily` VALUES (250, 2, 8, '2014-04-21', 999, '2014-04-09 17:21:05');
INSERT INTO `tour_allocationdaily` VALUES (251, 2, 8, '2014-04-22', 999, '2014-04-09 17:21:05');
INSERT INTO `tour_allocationdaily` VALUES (252, 2, 8, '2014-04-23', 999, '2014-04-09 17:21:05');
INSERT INTO `tour_allocationdaily` VALUES (253, 2, 8, '2014-04-24', 999, '2014-04-09 17:21:05');
INSERT INTO `tour_allocationdaily` VALUES (254, 2, 8, '2014-04-25', 999, '2014-04-09 17:21:05');
INSERT INTO `tour_allocationdaily` VALUES (255, 2, 8, '2014-04-26', 999, '2014-04-09 17:21:05');
INSERT INTO `tour_allocationdaily` VALUES (256, 2, 8, '2014-04-27', 999, '2014-04-09 17:21:05');
INSERT INTO `tour_allocationdaily` VALUES (257, 2, 8, '2014-04-28', 999, '2014-04-09 17:21:05');
INSERT INTO `tour_allocationdaily` VALUES (258, 2, 8, '2014-04-29', 999, '2014-04-09 17:21:05');
INSERT INTO `tour_allocationdaily` VALUES (259, 2, 8, '2014-04-30', 999, '2014-04-09 17:21:05');
INSERT INTO `tour_allocationdaily` VALUES (260, 2, 9, '2014-01-01', 9999, '2014-04-09 17:22:22');
INSERT INTO `tour_allocationdaily` VALUES (261, 2, 9, '2014-01-02', 9999, '2014-04-09 17:22:22');
INSERT INTO `tour_allocationdaily` VALUES (262, 2, 9, '2014-01-03', 9999, '2014-04-09 17:22:22');
INSERT INTO `tour_allocationdaily` VALUES (263, 2, 9, '2014-01-04', 9999, '2014-04-09 17:22:22');
INSERT INTO `tour_allocationdaily` VALUES (264, 2, 9, '2014-01-05', 9999, '2014-04-09 17:22:22');
INSERT INTO `tour_allocationdaily` VALUES (265, 2, 9, '2014-01-06', 9999, '2014-04-09 17:22:22');
INSERT INTO `tour_allocationdaily` VALUES (266, 2, 9, '2014-01-07', 9999, '2014-04-09 17:22:22');
INSERT INTO `tour_allocationdaily` VALUES (267, 2, 9, '2014-01-08', 9999, '2014-04-09 17:22:22');
INSERT INTO `tour_allocationdaily` VALUES (268, 2, 9, '2014-01-09', 9999, '2014-04-09 17:22:22');
INSERT INTO `tour_allocationdaily` VALUES (269, 2, 9, '2014-01-10', 9999, '2014-04-09 17:22:22');
INSERT INTO `tour_allocationdaily` VALUES (270, 2, 9, '2014-01-11', 9999, '2014-04-09 17:22:22');
INSERT INTO `tour_allocationdaily` VALUES (271, 2, 9, '2014-01-12', 9999, '2014-04-09 17:22:22');
INSERT INTO `tour_allocationdaily` VALUES (272, 2, 9, '2014-01-13', 9999, '2014-04-09 17:22:22');
INSERT INTO `tour_allocationdaily` VALUES (273, 2, 9, '2014-01-14', 9999, '2014-04-09 17:22:22');
INSERT INTO `tour_allocationdaily` VALUES (274, 2, 9, '2014-01-15', 9999, '2014-04-09 17:22:22');
INSERT INTO `tour_allocationdaily` VALUES (275, 2, 9, '2014-01-16', 9999, '2014-04-09 17:22:22');
INSERT INTO `tour_allocationdaily` VALUES (276, 2, 9, '2014-01-17', 9999, '2014-04-09 17:22:22');
INSERT INTO `tour_allocationdaily` VALUES (277, 2, 9, '2014-01-18', 9999, '2014-04-09 17:22:22');
INSERT INTO `tour_allocationdaily` VALUES (278, 2, 9, '2014-01-19', 9999, '2014-04-09 17:22:22');
INSERT INTO `tour_allocationdaily` VALUES (279, 2, 9, '2014-01-20', 9999, '2014-04-09 17:22:22');
INSERT INTO `tour_allocationdaily` VALUES (280, 2, 9, '2014-01-21', 9999, '2014-04-09 17:22:22');
INSERT INTO `tour_allocationdaily` VALUES (281, 2, 9, '2014-01-22', 9999, '2014-04-09 17:22:22');
INSERT INTO `tour_allocationdaily` VALUES (282, 2, 9, '2014-01-23', 9999, '2014-04-09 17:22:22');
INSERT INTO `tour_allocationdaily` VALUES (283, 2, 9, '2014-01-24', 9999, '2014-04-09 17:22:22');
INSERT INTO `tour_allocationdaily` VALUES (284, 2, 9, '2014-01-25', 9999, '2014-04-09 17:22:22');
INSERT INTO `tour_allocationdaily` VALUES (285, 2, 9, '2014-01-26', 9999, '2014-04-09 17:22:22');
INSERT INTO `tour_allocationdaily` VALUES (286, 2, 9, '2014-01-27', 9999, '2014-04-09 17:22:22');
INSERT INTO `tour_allocationdaily` VALUES (287, 2, 9, '2014-01-28', 9999, '2014-04-09 17:22:22');
INSERT INTO `tour_allocationdaily` VALUES (288, 2, 9, '2014-01-29', 9999, '2014-04-09 17:22:22');
INSERT INTO `tour_allocationdaily` VALUES (289, 2, 9, '2014-01-30', 9999, '2014-04-09 17:22:22');
INSERT INTO `tour_allocationdaily` VALUES (290, 2, 9, '2014-01-31', 9999, '2014-04-09 17:22:22');
INSERT INTO `tour_allocationdaily` VALUES (291, 2, 9, '2014-02-01', 9999, '2014-04-09 17:22:22');
INSERT INTO `tour_allocationdaily` VALUES (292, 2, 9, '2014-02-02', 9999, '2014-04-09 17:22:22');
INSERT INTO `tour_allocationdaily` VALUES (293, 2, 9, '2014-02-03', 9999, '2014-04-09 17:22:22');
INSERT INTO `tour_allocationdaily` VALUES (294, 2, 9, '2014-02-04', 9999, '2014-04-09 17:22:22');
INSERT INTO `tour_allocationdaily` VALUES (295, 2, 9, '2014-02-05', 9999, '2014-04-09 17:22:22');
INSERT INTO `tour_allocationdaily` VALUES (296, 2, 9, '2014-02-06', 9999, '2014-04-09 17:22:22');
INSERT INTO `tour_allocationdaily` VALUES (297, 2, 9, '2014-02-07', 9999, '2014-04-09 17:22:22');
INSERT INTO `tour_allocationdaily` VALUES (298, 2, 9, '2014-02-08', 9999, '2014-04-09 17:22:22');
INSERT INTO `tour_allocationdaily` VALUES (299, 2, 9, '2014-02-09', 9999, '2014-04-09 17:22:22');
INSERT INTO `tour_allocationdaily` VALUES (300, 2, 9, '2014-02-10', 9999, '2014-04-09 17:22:22');
INSERT INTO `tour_allocationdaily` VALUES (301, 2, 9, '2014-02-11', 9999, '2014-04-09 17:22:22');
INSERT INTO `tour_allocationdaily` VALUES (302, 2, 9, '2014-02-12', 9999, '2014-04-09 17:22:22');
INSERT INTO `tour_allocationdaily` VALUES (303, 2, 9, '2014-02-13', 9999, '2014-04-09 17:22:22');
INSERT INTO `tour_allocationdaily` VALUES (304, 2, 9, '2014-02-14', 9999, '2014-04-09 17:22:22');
INSERT INTO `tour_allocationdaily` VALUES (305, 2, 9, '2014-02-15', 9999, '2014-04-09 17:22:22');
INSERT INTO `tour_allocationdaily` VALUES (306, 2, 9, '2014-02-16', 9999, '2014-04-09 17:22:22');
INSERT INTO `tour_allocationdaily` VALUES (307, 2, 9, '2014-02-17', 9999, '2014-04-09 17:22:22');
INSERT INTO `tour_allocationdaily` VALUES (308, 2, 9, '2014-02-18', 9999, '2014-04-09 17:22:22');
INSERT INTO `tour_allocationdaily` VALUES (309, 2, 9, '2014-02-19', 9999, '2014-04-09 17:22:22');
INSERT INTO `tour_allocationdaily` VALUES (310, 2, 9, '2014-02-20', 9999, '2014-04-09 17:22:22');
INSERT INTO `tour_allocationdaily` VALUES (311, 2, 9, '2014-02-21', 9999, '2014-04-09 17:22:22');
INSERT INTO `tour_allocationdaily` VALUES (312, 2, 9, '2014-02-22', 9999, '2014-04-09 17:22:22');
INSERT INTO `tour_allocationdaily` VALUES (313, 2, 9, '2014-02-23', 9999, '2014-04-09 17:22:22');
INSERT INTO `tour_allocationdaily` VALUES (314, 2, 9, '2014-02-24', 9999, '2014-04-09 17:22:22');
INSERT INTO `tour_allocationdaily` VALUES (315, 2, 9, '2014-02-25', 9999, '2014-04-09 17:22:22');
INSERT INTO `tour_allocationdaily` VALUES (316, 2, 9, '2014-02-26', 9999, '2014-04-09 17:22:22');
INSERT INTO `tour_allocationdaily` VALUES (317, 2, 9, '2014-02-27', 9999, '2014-04-09 17:22:22');
INSERT INTO `tour_allocationdaily` VALUES (318, 2, 9, '2014-02-28', 9999, '2014-04-09 17:22:22');
INSERT INTO `tour_allocationdaily` VALUES (319, 2, 9, '2014-03-01', 9999, '2014-04-09 17:22:22');
INSERT INTO `tour_allocationdaily` VALUES (320, 2, 9, '2014-03-02', 9999, '2014-04-09 17:22:22');
INSERT INTO `tour_allocationdaily` VALUES (321, 2, 9, '2014-03-03', 9999, '2014-04-09 17:22:22');
INSERT INTO `tour_allocationdaily` VALUES (322, 2, 9, '2014-03-04', 9999, '2014-04-09 17:22:22');
INSERT INTO `tour_allocationdaily` VALUES (323, 2, 9, '2014-03-05', 9999, '2014-04-09 17:22:22');
INSERT INTO `tour_allocationdaily` VALUES (324, 2, 9, '2014-03-06', 9999, '2014-04-09 17:22:22');
INSERT INTO `tour_allocationdaily` VALUES (325, 2, 9, '2014-03-07', 9999, '2014-04-09 17:22:22');
INSERT INTO `tour_allocationdaily` VALUES (326, 2, 9, '2014-03-08', 9999, '2014-04-09 17:22:22');
INSERT INTO `tour_allocationdaily` VALUES (327, 2, 9, '2014-03-09', 9999, '2014-04-09 17:22:22');
INSERT INTO `tour_allocationdaily` VALUES (328, 2, 9, '2014-03-10', 9999, '2014-04-09 17:22:22');
INSERT INTO `tour_allocationdaily` VALUES (329, 2, 9, '2014-03-11', 9999, '2014-04-09 17:22:22');
INSERT INTO `tour_allocationdaily` VALUES (330, 2, 9, '2014-03-12', 9999, '2014-04-09 17:22:22');
INSERT INTO `tour_allocationdaily` VALUES (331, 2, 9, '2014-03-13', 9999, '2014-04-09 17:22:22');
INSERT INTO `tour_allocationdaily` VALUES (332, 2, 9, '2014-03-14', 9999, '2014-04-09 17:22:22');
INSERT INTO `tour_allocationdaily` VALUES (333, 2, 9, '2014-03-15', 9999, '2014-04-09 17:22:22');
INSERT INTO `tour_allocationdaily` VALUES (334, 2, 9, '2014-03-16', 9999, '2014-04-09 17:22:22');
INSERT INTO `tour_allocationdaily` VALUES (335, 2, 9, '2014-03-17', 9999, '2014-04-09 17:22:22');
INSERT INTO `tour_allocationdaily` VALUES (336, 2, 9, '2014-03-18', 9999, '2014-04-09 17:22:22');
INSERT INTO `tour_allocationdaily` VALUES (337, 2, 9, '2014-03-19', 9999, '2014-04-09 17:22:22');
INSERT INTO `tour_allocationdaily` VALUES (338, 2, 9, '2014-03-20', 9999, '2014-04-09 17:22:22');
INSERT INTO `tour_allocationdaily` VALUES (339, 2, 9, '2014-03-21', 9999, '2014-04-09 17:22:22');
INSERT INTO `tour_allocationdaily` VALUES (340, 2, 9, '2014-03-22', 9999, '2014-04-09 17:22:22');
INSERT INTO `tour_allocationdaily` VALUES (341, 2, 9, '2014-03-23', 9999, '2014-04-09 17:22:22');
INSERT INTO `tour_allocationdaily` VALUES (342, 2, 9, '2014-03-24', 9999, '2014-04-09 17:22:22');
INSERT INTO `tour_allocationdaily` VALUES (343, 2, 9, '2014-03-25', 9999, '2014-04-09 17:22:22');
INSERT INTO `tour_allocationdaily` VALUES (344, 2, 9, '2014-03-26', 9999, '2014-04-09 17:22:22');
INSERT INTO `tour_allocationdaily` VALUES (345, 2, 9, '2014-03-27', 9999, '2014-04-09 17:22:22');
INSERT INTO `tour_allocationdaily` VALUES (346, 2, 9, '2014-03-28', 9999, '2014-04-09 17:22:22');
INSERT INTO `tour_allocationdaily` VALUES (347, 2, 9, '2014-03-29', 9999, '2014-04-09 17:22:22');
INSERT INTO `tour_allocationdaily` VALUES (348, 2, 9, '2014-03-30', 9999, '2014-04-09 17:22:22');
INSERT INTO `tour_allocationdaily` VALUES (349, 2, 9, '2014-03-31', 9999, '2014-04-09 17:22:22');
INSERT INTO `tour_allocationdaily` VALUES (350, 2, 9, '2014-04-01', 9999, '2014-04-09 17:22:22');
INSERT INTO `tour_allocationdaily` VALUES (351, 2, 9, '2014-04-02', 9999, '2014-04-09 17:22:22');
INSERT INTO `tour_allocationdaily` VALUES (352, 2, 9, '2014-04-03', 9999, '2014-04-09 17:22:22');
INSERT INTO `tour_allocationdaily` VALUES (353, 2, 9, '2014-04-04', 9999, '2014-04-09 17:22:22');
INSERT INTO `tour_allocationdaily` VALUES (354, 2, 9, '2014-04-05', 9999, '2014-04-09 17:22:22');
INSERT INTO `tour_allocationdaily` VALUES (355, 2, 9, '2014-04-06', 9999, '2014-04-09 17:22:22');
INSERT INTO `tour_allocationdaily` VALUES (356, 2, 9, '2014-04-07', 9999, '2014-04-09 17:22:22');
INSERT INTO `tour_allocationdaily` VALUES (357, 2, 9, '2014-04-08', 9999, '2014-04-09 17:22:22');
INSERT INTO `tour_allocationdaily` VALUES (358, 2, 9, '2014-04-09', 9999, '2014-04-09 17:22:22');
INSERT INTO `tour_allocationdaily` VALUES (359, 2, 9, '2014-04-10', 9999, '2014-04-09 17:22:22');
INSERT INTO `tour_allocationdaily` VALUES (360, 2, 9, '2014-04-11', 9999, '2014-04-09 17:22:22');
INSERT INTO `tour_allocationdaily` VALUES (361, 2, 9, '2014-04-12', 9999, '2014-04-09 17:22:22');
INSERT INTO `tour_allocationdaily` VALUES (362, 2, 9, '2014-04-13', 9999, '2014-04-09 17:22:22');
INSERT INTO `tour_allocationdaily` VALUES (363, 2, 9, '2014-04-14', 9999, '2014-04-09 17:22:22');
INSERT INTO `tour_allocationdaily` VALUES (364, 2, 9, '2014-04-15', 9999, '2014-04-09 17:22:22');
INSERT INTO `tour_allocationdaily` VALUES (365, 2, 9, '2014-04-16', 9999, '2014-04-09 17:22:22');
INSERT INTO `tour_allocationdaily` VALUES (366, 2, 9, '2014-04-17', 9999, '2014-04-09 17:22:22');
INSERT INTO `tour_allocationdaily` VALUES (367, 2, 9, '2014-04-18', 9999, '2014-04-09 17:22:22');
INSERT INTO `tour_allocationdaily` VALUES (368, 2, 9, '2014-04-19', 9999, '2014-04-09 17:22:22');
INSERT INTO `tour_allocationdaily` VALUES (369, 2, 9, '2014-04-20', 9999, '2014-04-09 17:22:22');
INSERT INTO `tour_allocationdaily` VALUES (370, 2, 9, '2014-04-21', 9999, '2014-04-09 17:22:22');
INSERT INTO `tour_allocationdaily` VALUES (371, 2, 9, '2014-04-22', 9999, '2014-04-09 17:22:22');
INSERT INTO `tour_allocationdaily` VALUES (372, 2, 9, '2014-04-23', 9999, '2014-04-09 17:22:22');
INSERT INTO `tour_allocationdaily` VALUES (373, 2, 9, '2014-04-24', 9999, '2014-04-09 17:22:22');
INSERT INTO `tour_allocationdaily` VALUES (374, 2, 9, '2014-04-25', 9999, '2014-04-09 17:22:22');
INSERT INTO `tour_allocationdaily` VALUES (375, 2, 9, '2014-04-26', 9999, '2014-04-09 17:22:22');
INSERT INTO `tour_allocationdaily` VALUES (376, 2, 9, '2014-04-27', 9999, '2014-04-09 17:22:22');
INSERT INTO `tour_allocationdaily` VALUES (377, 2, 9, '2014-04-28', 9999, '2014-04-09 17:22:22');
INSERT INTO `tour_allocationdaily` VALUES (378, 2, 9, '2014-04-29', 9999, '2014-04-09 17:22:22');
INSERT INTO `tour_allocationdaily` VALUES (379, 2, 9, '2014-04-30', 9999, '2014-04-09 17:22:22');
INSERT INTO `tour_allocationdaily` VALUES (380, 2, 8, '2014-05-01', 8888, '2014-04-09 17:28:24');
INSERT INTO `tour_allocationdaily` VALUES (381, 2, 8, '2014-05-02', 8888, '2014-04-09 17:28:24');
INSERT INTO `tour_allocationdaily` VALUES (382, 2, 8, '2014-05-03', 8888, '2014-04-09 17:28:24');
INSERT INTO `tour_allocationdaily` VALUES (383, 2, 8, '2014-05-04', 8888, '2014-04-09 17:28:24');
INSERT INTO `tour_allocationdaily` VALUES (384, 2, 8, '2014-05-05', 8888, '2014-04-09 17:28:24');
INSERT INTO `tour_allocationdaily` VALUES (385, 2, 8, '2014-05-06', 8888, '2014-04-09 17:28:24');
INSERT INTO `tour_allocationdaily` VALUES (386, 2, 8, '2014-05-07', 8888, '2014-04-09 17:28:24');
INSERT INTO `tour_allocationdaily` VALUES (387, 2, 8, '2014-05-08', 8888, '2014-04-09 17:28:24');
INSERT INTO `tour_allocationdaily` VALUES (388, 2, 8, '2014-05-09', 8888, '2014-04-09 17:28:24');
INSERT INTO `tour_allocationdaily` VALUES (389, 2, 8, '2014-05-10', 8888, '2014-04-09 17:28:24');
INSERT INTO `tour_allocationdaily` VALUES (390, 2, 8, '2014-05-11', 8888, '2014-04-09 17:28:24');
INSERT INTO `tour_allocationdaily` VALUES (391, 2, 8, '2014-05-12', 8888, '2014-04-09 17:28:24');
INSERT INTO `tour_allocationdaily` VALUES (392, 2, 8, '2014-05-13', 8888, '2014-04-09 17:28:24');
INSERT INTO `tour_allocationdaily` VALUES (393, 2, 8, '2014-05-14', 8888, '2014-04-09 17:28:24');
INSERT INTO `tour_allocationdaily` VALUES (394, 2, 8, '2014-05-15', 8888, '2014-04-09 17:28:24');
INSERT INTO `tour_allocationdaily` VALUES (395, 2, 8, '2014-05-16', 8888, '2014-04-09 17:28:24');
INSERT INTO `tour_allocationdaily` VALUES (396, 2, 8, '2014-05-17', 8888, '2014-04-09 17:28:24');
INSERT INTO `tour_allocationdaily` VALUES (397, 2, 8, '2014-05-18', 8888, '2014-04-09 17:28:24');
INSERT INTO `tour_allocationdaily` VALUES (398, 2, 8, '2014-05-19', 8888, '2014-04-09 17:28:24');
INSERT INTO `tour_allocationdaily` VALUES (399, 2, 8, '2014-05-20', 8888, '2014-04-09 17:28:24');
INSERT INTO `tour_allocationdaily` VALUES (400, 2, 8, '2014-05-21', 8888, '2014-04-09 17:28:24');
INSERT INTO `tour_allocationdaily` VALUES (401, 2, 8, '2014-05-22', 8888, '2014-04-09 17:28:24');
INSERT INTO `tour_allocationdaily` VALUES (402, 2, 8, '2014-05-23', 8888, '2014-04-09 17:28:24');
INSERT INTO `tour_allocationdaily` VALUES (403, 2, 8, '2014-05-24', 8888, '2014-04-09 17:28:24');
INSERT INTO `tour_allocationdaily` VALUES (404, 2, 8, '2014-05-25', 8888, '2014-04-09 17:28:24');
INSERT INTO `tour_allocationdaily` VALUES (405, 2, 8, '2014-05-26', 8888, '2014-04-09 17:28:24');
INSERT INTO `tour_allocationdaily` VALUES (406, 2, 8, '2014-05-27', 8888, '2014-04-09 17:28:24');
INSERT INTO `tour_allocationdaily` VALUES (407, 2, 8, '2014-05-28', 8888, '2014-04-09 17:28:24');
INSERT INTO `tour_allocationdaily` VALUES (408, 2, 8, '2014-05-29', 8888, '2014-04-09 17:28:24');
INSERT INTO `tour_allocationdaily` VALUES (409, 2, 8, '2014-05-30', 8888, '2014-04-09 17:28:24');
INSERT INTO `tour_allocationdaily` VALUES (410, 2, 8, '2014-05-31', 8888, '2014-04-09 17:28:24');
INSERT INTO `tour_allocationdaily` VALUES (596, 3, 11, '2014-01-24', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (412, 4, 12, '2014-04-24', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (413, 4, 12, '2014-04-25', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (414, 4, 12, '2014-04-26', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (415, 4, 12, '2014-04-27', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (416, 4, 12, '2014-04-28', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (417, 4, 12, '2014-04-29', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (418, 4, 12, '2014-04-30', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (419, 4, 12, '2014-05-01', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (420, 4, 12, '2014-05-02', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (421, 4, 12, '2014-05-03', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (422, 4, 12, '2014-05-04', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (423, 4, 12, '2014-05-05', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (424, 4, 12, '2014-05-06', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (425, 4, 12, '2014-05-07', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (426, 4, 12, '2014-05-08', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (427, 4, 12, '2014-05-09', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (428, 4, 12, '2014-05-10', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (429, 4, 12, '2014-05-11', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (430, 4, 12, '2014-05-12', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (431, 4, 12, '2014-05-13', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (432, 4, 12, '2014-05-14', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (433, 4, 12, '2014-05-15', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (434, 4, 12, '2014-05-16', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (435, 4, 12, '2014-05-17', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (436, 4, 12, '2014-05-18', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (437, 4, 12, '2014-05-19', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (438, 4, 12, '2014-05-20', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (439, 4, 12, '2014-05-21', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (440, 4, 12, '2014-05-22', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (441, 4, 12, '2014-05-23', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (442, 4, 12, '2014-05-24', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (443, 4, 12, '2014-05-25', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (444, 4, 12, '2014-05-26', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (445, 4, 12, '2014-05-27', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (446, 4, 12, '2014-05-28', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (447, 4, 12, '2014-05-29', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (448, 4, 12, '2014-05-30', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (449, 4, 12, '2014-05-31', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (450, 4, 12, '2014-06-01', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (451, 4, 12, '2014-06-02', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (452, 4, 12, '2014-06-03', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (453, 4, 12, '2014-06-04', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (454, 4, 12, '2014-06-05', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (455, 4, 12, '2014-06-06', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (456, 4, 12, '2014-06-07', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (457, 4, 12, '2014-06-08', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (458, 4, 12, '2014-06-09', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (459, 4, 12, '2014-06-10', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (460, 4, 12, '2014-06-11', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (461, 4, 12, '2014-06-12', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (462, 4, 12, '2014-06-13', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (463, 4, 12, '2014-06-14', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (464, 4, 12, '2014-06-15', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (465, 4, 12, '2014-06-16', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (466, 4, 12, '2014-06-17', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (467, 4, 12, '2014-06-18', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (468, 4, 12, '2014-06-19', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (469, 4, 12, '2014-06-20', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (470, 4, 12, '2014-06-21', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (471, 4, 12, '2014-06-22', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (472, 4, 12, '2014-06-23', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (473, 4, 12, '2014-06-24', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (474, 4, 12, '2014-06-25', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (475, 4, 12, '2014-06-26', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (476, 4, 12, '2014-06-27', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (477, 4, 12, '2014-06-28', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (478, 4, 12, '2014-06-29', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (479, 4, 12, '2014-06-30', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (480, 4, 12, '2014-07-01', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (481, 4, 12, '2014-07-02', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (482, 4, 12, '2014-07-03', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (483, 4, 12, '2014-07-04', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (484, 4, 12, '2014-07-05', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (485, 4, 12, '2014-07-06', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (486, 4, 12, '2014-07-07', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (487, 4, 12, '2014-07-08', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (488, 4, 12, '2014-07-09', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (489, 4, 12, '2014-07-10', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (490, 4, 12, '2014-07-11', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (491, 4, 12, '2014-07-12', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (492, 4, 12, '2014-07-13', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (493, 4, 12, '2014-07-14', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (494, 4, 12, '2014-07-15', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (495, 4, 12, '2014-07-16', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (496, 4, 12, '2014-07-17', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (497, 4, 12, '2014-07-18', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (498, 4, 12, '2014-07-19', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (499, 4, 12, '2014-07-20', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (500, 4, 12, '2014-07-21', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (501, 4, 12, '2014-07-22', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (502, 4, 12, '2014-07-23', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (503, 4, 12, '2014-07-24', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (504, 4, 12, '2014-07-25', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (505, 4, 12, '2014-07-26', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (506, 4, 12, '2014-07-27', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (507, 4, 12, '2014-07-28', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (508, 4, 12, '2014-07-29', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (509, 4, 12, '2014-07-30', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (510, 4, 12, '2014-07-31', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (511, 4, 12, '2014-08-01', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (512, 4, 12, '2014-08-02', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (513, 4, 12, '2014-08-03', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (514, 4, 12, '2014-08-04', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (515, 4, 12, '2014-08-05', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (516, 4, 12, '2014-08-06', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (517, 4, 12, '2014-08-07', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (518, 4, 12, '2014-08-08', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (519, 4, 12, '2014-08-09', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (520, 4, 12, '2014-08-10', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (521, 4, 12, '2014-08-11', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (522, 4, 12, '2014-08-12', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (523, 4, 12, '2014-08-13', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (524, 4, 12, '2014-08-14', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (525, 4, 12, '2014-08-15', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (526, 4, 12, '2014-08-16', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (527, 4, 12, '2014-08-17', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (528, 4, 12, '2014-08-18', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (529, 4, 12, '2014-08-19', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (530, 4, 12, '2014-08-20', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (531, 4, 12, '2014-08-21', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (532, 4, 12, '2014-08-22', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (533, 4, 12, '2014-08-23', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (534, 4, 12, '2014-08-24', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (535, 4, 12, '2014-08-25', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (536, 4, 12, '2014-08-26', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (537, 4, 12, '2014-08-27', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (538, 4, 12, '2014-08-28', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (539, 4, 12, '2014-08-29', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (540, 4, 12, '2014-08-30', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (541, 4, 12, '2014-08-31', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (542, 4, 12, '2014-09-01', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (543, 4, 12, '2014-09-02', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (544, 4, 12, '2014-09-03', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (545, 4, 12, '2014-09-04', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (546, 4, 12, '2014-09-05', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (547, 4, 12, '2014-09-06', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (548, 4, 12, '2014-09-07', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (549, 4, 12, '2014-09-08', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (550, 4, 12, '2014-09-09', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (551, 4, 12, '2014-09-10', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (552, 4, 12, '2014-09-11', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (553, 4, 12, '2014-09-12', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (554, 4, 12, '2014-09-13', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (555, 4, 12, '2014-09-14', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (556, 4, 12, '2014-09-15', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (557, 4, 12, '2014-09-16', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (558, 4, 12, '2014-09-17', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (559, 4, 12, '2014-09-18', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (560, 4, 12, '2014-09-19', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (561, 4, 12, '2014-09-20', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (562, 4, 12, '2014-09-21', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (563, 4, 12, '2014-09-22', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (564, 4, 12, '2014-09-23', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (565, 4, 12, '2014-09-24', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (566, 4, 12, '2014-09-25', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (567, 4, 12, '2014-09-26', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (568, 4, 12, '2014-09-27', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (569, 4, 12, '2014-09-28', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (570, 4, 12, '2014-09-29', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (571, 4, 12, '2014-09-30', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (572, 4, 12, '2014-10-01', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (573, 4, 12, '2014-10-02', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (574, 4, 12, '2014-10-03', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (575, 4, 12, '2014-10-04', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (576, 4, 12, '2014-10-05', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (577, 4, 12, '2014-10-06', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (578, 4, 12, '2014-10-07', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (579, 4, 12, '2014-10-08', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (580, 4, 12, '2014-10-09', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (581, 4, 12, '2014-10-10', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (582, 4, 12, '2014-10-11', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (583, 4, 12, '2014-10-12', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (584, 4, 12, '2014-10-13', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (585, 4, 12, '2014-10-14', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (586, 4, 12, '2014-10-15', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (587, 4, 12, '2014-10-16', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (588, 4, 12, '2014-10-17', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (589, 4, 12, '2014-10-18', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (590, 4, 12, '2014-10-19', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (591, 4, 12, '2014-10-20', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (592, 4, 12, '2014-10-21', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (593, 4, 12, '2014-10-22', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (594, 4, 12, '2014-10-23', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (595, 4, 12, '2014-10-24', 5, '2014-04-24 11:04:23');
INSERT INTO `tour_allocationdaily` VALUES (597, 3, 11, '2014-01-25', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (598, 3, 11, '2014-01-26', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (599, 3, 11, '2014-01-27', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (600, 3, 11, '2014-01-28', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (601, 3, 11, '2014-01-29', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (602, 3, 11, '2014-01-30', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (603, 3, 11, '2014-01-31', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (604, 3, 11, '2014-02-01', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (605, 3, 11, '2014-02-02', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (606, 3, 11, '2014-02-03', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (607, 3, 11, '2014-02-04', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (608, 3, 11, '2014-02-05', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (609, 3, 11, '2014-02-06', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (610, 3, 11, '2014-02-07', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (611, 3, 11, '2014-02-08', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (612, 3, 11, '2014-02-09', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (613, 3, 11, '2014-02-10', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (614, 3, 11, '2014-02-11', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (615, 3, 11, '2014-02-12', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (616, 3, 11, '2014-02-13', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (617, 3, 11, '2014-02-14', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (618, 3, 11, '2014-02-15', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (619, 3, 11, '2014-02-16', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (620, 3, 11, '2014-02-17', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (621, 3, 11, '2014-02-18', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (622, 3, 11, '2014-02-19', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (623, 3, 11, '2014-02-20', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (624, 3, 11, '2014-02-21', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (625, 3, 11, '2014-02-22', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (626, 3, 11, '2014-02-23', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (627, 3, 11, '2014-02-24', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (628, 3, 11, '2014-02-25', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (629, 3, 11, '2014-02-26', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (630, 3, 11, '2014-02-27', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (631, 3, 11, '2014-02-28', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (632, 3, 11, '2014-03-01', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (633, 3, 11, '2014-03-02', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (634, 3, 11, '2014-03-03', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (635, 3, 11, '2014-03-04', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (636, 3, 11, '2014-03-05', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (637, 3, 11, '2014-03-06', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (638, 3, 11, '2014-03-07', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (639, 3, 11, '2014-03-08', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (640, 3, 11, '2014-03-09', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (641, 3, 11, '2014-03-10', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (642, 3, 11, '2014-03-11', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (643, 3, 11, '2014-03-12', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (644, 3, 11, '2014-03-13', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (645, 3, 11, '2014-03-14', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (646, 3, 11, '2014-03-15', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (647, 3, 11, '2014-03-16', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (648, 3, 11, '2014-03-17', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (649, 3, 11, '2014-03-18', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (650, 3, 11, '2014-03-19', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (651, 3, 11, '2014-03-20', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (652, 3, 11, '2014-03-21', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (653, 3, 11, '2014-03-22', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (654, 3, 11, '2014-03-23', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (655, 3, 11, '2014-03-24', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (656, 3, 11, '2014-03-25', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (657, 3, 11, '2014-03-26', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (658, 3, 11, '2014-03-27', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (659, 3, 11, '2014-03-28', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (660, 3, 11, '2014-03-29', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (661, 3, 11, '2014-03-30', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (662, 3, 11, '2014-03-31', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (663, 3, 11, '2014-04-01', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (664, 3, 11, '2014-04-02', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (665, 3, 11, '2014-04-03', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (666, 3, 11, '2014-04-04', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (667, 3, 11, '2014-04-05', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (668, 3, 11, '2014-04-06', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (669, 3, 11, '2014-04-07', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (670, 3, 11, '2014-04-08', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (671, 3, 11, '2014-04-09', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (672, 3, 11, '2014-04-10', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (673, 3, 11, '2014-04-11', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (674, 3, 11, '2014-04-12', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (675, 3, 11, '2014-04-13', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (676, 3, 11, '2014-04-14', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (677, 3, 11, '2014-04-15', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (678, 3, 11, '2014-04-16', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (679, 3, 11, '2014-04-17', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (680, 3, 11, '2014-04-18', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (681, 3, 11, '2014-04-19', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (682, 3, 11, '2014-04-20', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (683, 3, 11, '2014-04-21', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (684, 3, 11, '2014-04-22', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (685, 3, 11, '2014-04-23', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (686, 3, 11, '2014-04-24', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (687, 3, 11, '2014-04-25', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (688, 3, 11, '2014-04-26', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (689, 3, 11, '2014-04-27', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (690, 3, 11, '2014-04-28', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (691, 3, 11, '2014-04-29', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (692, 3, 11, '2014-04-30', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (693, 3, 11, '2014-05-01', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (694, 3, 11, '2014-05-02', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (695, 3, 11, '2014-05-03', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (696, 3, 11, '2014-05-04', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (697, 3, 11, '2014-05-05', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (698, 3, 11, '2014-05-06', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (699, 3, 11, '2014-05-07', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (700, 3, 11, '2014-05-08', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (701, 3, 11, '2014-05-09', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (702, 3, 11, '2014-05-10', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (703, 3, 11, '2014-05-11', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (704, 3, 11, '2014-05-12', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (705, 3, 11, '2014-05-13', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (706, 3, 11, '2014-05-14', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (707, 3, 11, '2014-05-15', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (708, 3, 11, '2014-05-16', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (709, 3, 11, '2014-05-17', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (710, 3, 11, '2014-05-18', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (711, 3, 11, '2014-05-19', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (712, 3, 11, '2014-05-20', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (713, 3, 11, '2014-05-21', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (714, 3, 11, '2014-05-22', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (715, 3, 11, '2014-05-23', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (716, 3, 11, '2014-05-24', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (717, 3, 11, '2014-05-25', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (718, 3, 11, '2014-05-26', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (719, 3, 11, '2014-05-27', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (720, 3, 11, '2014-05-28', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (721, 3, 11, '2014-05-29', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (722, 3, 11, '2014-05-30', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (723, 3, 11, '2014-05-31', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (724, 3, 11, '2014-06-01', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (725, 3, 11, '2014-06-02', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (726, 3, 11, '2014-06-03', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (727, 3, 11, '2014-06-04', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (728, 3, 11, '2014-06-05', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (729, 3, 11, '2014-06-06', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (730, 3, 11, '2014-06-07', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (731, 3, 11, '2014-06-08', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (732, 3, 11, '2014-06-09', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (733, 3, 11, '2014-06-10', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (734, 3, 11, '2014-06-11', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (735, 3, 11, '2014-06-12', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (736, 3, 11, '2014-06-13', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (737, 3, 11, '2014-06-14', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (738, 3, 11, '2014-06-15', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (739, 3, 11, '2014-06-16', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (740, 3, 11, '2014-06-17', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (741, 3, 11, '2014-06-18', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (742, 3, 11, '2014-06-19', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (743, 3, 11, '2014-06-20', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (744, 3, 11, '2014-06-21', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (745, 3, 11, '2014-06-22', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (746, 3, 11, '2014-06-23', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (747, 3, 11, '2014-06-24', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (748, 3, 11, '2014-06-25', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (749, 3, 11, '2014-06-26', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (750, 3, 11, '2014-06-27', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (751, 3, 11, '2014-06-28', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (752, 3, 11, '2014-06-29', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (753, 3, 11, '2014-06-30', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (754, 3, 11, '2014-07-01', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (755, 3, 11, '2014-07-02', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (756, 3, 11, '2014-07-03', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (757, 3, 11, '2014-07-04', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (758, 3, 11, '2014-07-05', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (759, 3, 11, '2014-07-06', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (760, 3, 11, '2014-07-07', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (761, 3, 11, '2014-07-08', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (762, 3, 11, '2014-07-09', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (763, 3, 11, '2014-07-10', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (764, 3, 11, '2014-07-11', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (765, 3, 11, '2014-07-12', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (766, 3, 11, '2014-07-13', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (767, 3, 11, '2014-07-14', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (768, 3, 11, '2014-07-15', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (769, 3, 11, '2014-07-16', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (770, 3, 11, '2014-07-17', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (771, 3, 11, '2014-07-18', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (772, 3, 11, '2014-07-19', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (773, 3, 11, '2014-07-20', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (774, 3, 11, '2014-07-21', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (775, 3, 11, '2014-07-22', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (776, 3, 11, '2014-07-23', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (777, 3, 11, '2014-07-24', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (778, 3, 11, '2014-07-25', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (779, 3, 11, '2014-07-26', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (780, 3, 11, '2014-07-27', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (781, 3, 11, '2014-07-28', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (782, 3, 11, '2014-07-29', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (783, 3, 11, '2014-07-30', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (784, 3, 11, '2014-07-31', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (785, 3, 11, '2014-08-01', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (786, 3, 11, '2014-08-02', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (787, 3, 11, '2014-08-03', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (788, 3, 11, '2014-08-04', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (789, 3, 11, '2014-08-05', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (790, 3, 11, '2014-08-06', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (791, 3, 11, '2014-08-07', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (792, 3, 11, '2014-08-08', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (793, 3, 11, '2014-08-09', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (794, 3, 11, '2014-08-10', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (795, 3, 11, '2014-08-11', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (796, 3, 11, '2014-08-12', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (797, 3, 11, '2014-08-13', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (798, 3, 11, '2014-08-14', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (799, 3, 11, '2014-08-15', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (800, 3, 11, '2014-08-16', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (801, 3, 11, '2014-08-17', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (802, 3, 11, '2014-08-18', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (803, 3, 11, '2014-08-19', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (804, 3, 11, '2014-08-20', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (805, 3, 11, '2014-08-21', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (806, 3, 11, '2014-08-22', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (807, 3, 11, '2014-08-23', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (808, 3, 11, '2014-08-24', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (809, 3, 11, '2014-08-25', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (810, 3, 11, '2014-08-26', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (811, 3, 11, '2014-08-27', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (812, 3, 11, '2014-08-28', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (813, 3, 11, '2014-08-29', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (814, 3, 11, '2014-08-30', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (815, 3, 11, '2014-08-31', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (816, 3, 11, '2014-09-01', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (817, 3, 11, '2014-09-02', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (818, 3, 11, '2014-09-03', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (819, 3, 11, '2014-09-04', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (820, 3, 11, '2014-09-05', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (821, 3, 11, '2014-09-06', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (822, 3, 11, '2014-09-07', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (823, 3, 11, '2014-09-08', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (824, 3, 11, '2014-09-09', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (825, 3, 11, '2014-09-10', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (826, 3, 11, '2014-09-11', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (827, 3, 11, '2014-09-12', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (828, 3, 11, '2014-09-13', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (829, 3, 11, '2014-09-14', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (830, 3, 11, '2014-09-15', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (831, 3, 11, '2014-09-16', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (832, 3, 11, '2014-09-17', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (833, 3, 11, '2014-09-18', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (834, 3, 11, '2014-09-19', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (835, 3, 11, '2014-09-20', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (836, 3, 11, '2014-09-21', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (837, 3, 11, '2014-09-22', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (838, 3, 11, '2014-09-23', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (839, 3, 11, '2014-09-24', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (840, 3, 11, '2014-09-25', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (841, 3, 11, '2014-09-26', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (842, 3, 11, '2014-09-27', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (843, 3, 11, '2014-09-28', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (844, 3, 11, '2014-09-29', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (845, 3, 11, '2014-09-30', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (846, 3, 11, '2014-10-01', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (847, 3, 11, '2014-10-02', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (848, 3, 11, '2014-10-03', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (849, 3, 11, '2014-10-04', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (850, 3, 11, '2014-10-05', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (851, 3, 11, '2014-10-06', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (852, 3, 11, '2014-10-07', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (853, 3, 11, '2014-10-08', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (854, 3, 11, '2014-10-09', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (855, 3, 11, '2014-10-10', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (856, 3, 11, '2014-10-11', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (857, 3, 11, '2014-10-12', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (858, 3, 11, '2014-10-13', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (859, 3, 11, '2014-10-14', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (860, 3, 11, '2014-10-15', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (861, 3, 11, '2014-10-16', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (862, 3, 11, '2014-10-17', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (863, 3, 11, '2014-10-18', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (864, 3, 11, '2014-10-19', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (865, 3, 11, '2014-10-20', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (866, 3, 11, '2014-10-21', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (867, 3, 11, '2014-10-22', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (868, 3, 11, '2014-10-23', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (869, 3, 11, '2014-10-24', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (870, 3, 11, '2014-10-25', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (871, 3, 11, '2014-10-26', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (872, 3, 11, '2014-10-27', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (873, 3, 11, '2014-10-28', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (874, 3, 11, '2014-10-29', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (875, 3, 11, '2014-10-30', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (876, 3, 11, '2014-10-31', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (877, 3, 11, '2014-11-01', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (878, 3, 11, '2014-11-02', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (879, 3, 11, '2014-11-03', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (880, 3, 11, '2014-11-04', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (881, 3, 11, '2014-11-05', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (882, 3, 11, '2014-11-06', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (883, 3, 11, '2014-11-07', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (884, 3, 11, '2014-11-08', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (885, 3, 11, '2014-11-09', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (886, 3, 11, '2014-11-10', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (887, 3, 11, '2014-11-11', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (888, 3, 11, '2014-11-12', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (889, 3, 11, '2014-11-13', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (890, 3, 11, '2014-11-14', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (891, 3, 11, '2014-11-15', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (892, 3, 11, '2014-11-16', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (893, 3, 11, '2014-11-17', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (894, 3, 11, '2014-11-18', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (895, 3, 11, '2014-11-19', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (896, 3, 11, '2014-11-20', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (897, 3, 11, '2014-11-21', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (898, 3, 11, '2014-11-22', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (899, 3, 11, '2014-11-23', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (900, 3, 11, '2014-11-24', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (901, 3, 11, '2014-11-25', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (902, 3, 11, '2014-11-26', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (903, 3, 11, '2014-11-27', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (904, 3, 11, '2014-11-28', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (905, 3, 11, '2014-11-29', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (906, 3, 11, '2014-11-30', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (907, 3, 11, '2014-12-01', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (908, 3, 11, '2014-12-02', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (909, 3, 11, '2014-12-03', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (910, 3, 11, '2014-12-04', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (911, 3, 11, '2014-12-05', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (912, 3, 11, '2014-12-06', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (913, 3, 11, '2014-12-07', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (914, 3, 11, '2014-12-08', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (915, 3, 11, '2014-12-09', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (916, 3, 11, '2014-12-10', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (917, 3, 11, '2014-12-11', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (918, 3, 11, '2014-12-12', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (919, 3, 11, '2014-12-13', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (920, 3, 11, '2014-12-14', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (921, 3, 11, '2014-12-15', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (922, 3, 11, '2014-12-16', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (923, 3, 11, '2014-12-17', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (924, 3, 11, '2014-12-18', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (925, 3, 11, '2014-12-19', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (926, 3, 11, '2014-12-20', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (927, 3, 11, '2014-12-21', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (928, 3, 11, '2014-12-22', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (929, 3, 11, '2014-12-23', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (930, 3, 11, '2014-12-24', 0, '2014-04-24 13:12:41');
INSERT INTO `tour_allocationdaily` VALUES (931, 1, 1, '2014-01-25', 0, '2014-08-14 10:50:28');
INSERT INTO `tour_allocationdaily` VALUES (932, 1, 1, '2014-01-26', 0, '2014-08-14 10:50:28');
INSERT INTO `tour_allocationdaily` VALUES (933, 1, 1, '2014-01-27', 0, '2014-08-14 10:50:28');
INSERT INTO `tour_allocationdaily` VALUES (934, 1, 1, '2014-01-28', 0, '2014-08-14 10:50:28');
INSERT INTO `tour_allocationdaily` VALUES (935, 1, 1, '2014-01-29', 0, '2014-05-23 12:07:59');
INSERT INTO `tour_allocationdaily` VALUES (936, 1, 1, '2014-01-30', 0, '2014-05-23 12:07:59');
INSERT INTO `tour_allocationdaily` VALUES (937, 1, 1, '2014-01-31', 0, '2014-05-23 12:07:59');
INSERT INTO `tour_allocationdaily` VALUES (938, 1, 1, '2014-02-01', 0, '2014-05-23 12:07:59');
INSERT INTO `tour_allocationdaily` VALUES (939, 1, 1, '2014-02-02', 0, '2014-05-23 12:07:59');
INSERT INTO `tour_allocationdaily` VALUES (940, 1, 1, '2014-02-03', 0, '2014-05-23 12:07:59');
INSERT INTO `tour_allocationdaily` VALUES (941, 1, 1, '2014-02-04', 0, '2014-05-23 12:07:59');
INSERT INTO `tour_allocationdaily` VALUES (942, 1, 1, '2014-02-05', 0, '2014-05-23 12:07:59');
INSERT INTO `tour_allocationdaily` VALUES (943, 1, 1, '2014-02-06', 0, '2014-05-23 12:07:59');
INSERT INTO `tour_allocationdaily` VALUES (944, 1, 1, '2014-02-07', 0, '2014-05-23 12:07:59');
INSERT INTO `tour_allocationdaily` VALUES (945, 1, 1, '2014-02-08', 0, '2014-05-23 12:07:59');
INSERT INTO `tour_allocationdaily` VALUES (946, 1, 1, '2014-02-09', 0, '2014-05-23 12:07:59');
INSERT INTO `tour_allocationdaily` VALUES (947, 1, 1, '2014-02-10', 0, '2014-05-23 12:07:59');
INSERT INTO `tour_allocationdaily` VALUES (948, 1, 1, '2014-02-11', 0, '2014-05-23 12:07:59');
INSERT INTO `tour_allocationdaily` VALUES (949, 1, 1, '2014-02-12', 0, '2014-05-23 12:07:59');
INSERT INTO `tour_allocationdaily` VALUES (950, 1, 1, '2014-02-13', 0, '2014-05-23 12:07:59');
INSERT INTO `tour_allocationdaily` VALUES (951, 1, 1, '2014-02-14', 0, '2014-05-23 12:07:59');
INSERT INTO `tour_allocationdaily` VALUES (952, 1, 1, '2014-02-15', 0, '2014-05-23 12:07:59');
INSERT INTO `tour_allocationdaily` VALUES (953, 1, 1, '2014-02-16', 0, '2014-05-23 12:07:59');
INSERT INTO `tour_allocationdaily` VALUES (954, 1, 1, '2014-02-17', 0, '2014-05-23 12:07:59');
INSERT INTO `tour_allocationdaily` VALUES (955, 1, 1, '2014-02-18', 0, '2014-05-23 12:07:59');
INSERT INTO `tour_allocationdaily` VALUES (956, 1, 1, '2014-02-19', 0, '2014-05-23 12:07:59');
INSERT INTO `tour_allocationdaily` VALUES (957, 1, 1, '2014-02-20', 0, '2014-05-23 12:07:59');
INSERT INTO `tour_allocationdaily` VALUES (958, 1, 1, '2014-02-21', 0, '2014-05-23 12:07:59');
INSERT INTO `tour_allocationdaily` VALUES (959, 1, 1, '2014-02-22', 0, '2014-05-23 12:07:59');
INSERT INTO `tour_allocationdaily` VALUES (960, 1, 1, '2014-02-23', 0, '2014-05-23 12:07:59');
INSERT INTO `tour_allocationdaily` VALUES (961, 1, 1, '2014-02-24', 0, '2014-05-23 12:07:59');
INSERT INTO `tour_allocationdaily` VALUES (962, 1, 1, '2014-02-25', 0, '2014-05-23 12:07:59');
INSERT INTO `tour_allocationdaily` VALUES (963, 1, 1, '2014-02-26', 0, '2014-05-23 12:07:59');
INSERT INTO `tour_allocationdaily` VALUES (964, 1, 1, '2014-02-27', 0, '2014-05-23 12:07:59');
INSERT INTO `tour_allocationdaily` VALUES (965, 1, 1, '2014-02-28', 0, '2014-05-23 12:07:59');
INSERT INTO `tour_allocationdaily` VALUES (966, 1, 1, '2014-03-01', 0, '2014-05-23 12:07:59');
INSERT INTO `tour_allocationdaily` VALUES (967, 1, 1, '2014-03-02', 0, '2014-05-23 12:07:59');
INSERT INTO `tour_allocationdaily` VALUES (968, 1, 1, '2014-03-03', 0, '2014-05-23 12:07:59');
INSERT INTO `tour_allocationdaily` VALUES (969, 1, 1, '2014-03-04', 0, '2014-05-23 12:07:59');
INSERT INTO `tour_allocationdaily` VALUES (970, 1, 1, '2014-03-05', 0, '2014-05-23 12:07:59');
INSERT INTO `tour_allocationdaily` VALUES (971, 1, 1, '2014-03-06', 0, '2014-05-23 12:07:59');
INSERT INTO `tour_allocationdaily` VALUES (972, 1, 1, '2014-03-07', 0, '2014-05-23 12:07:59');
INSERT INTO `tour_allocationdaily` VALUES (973, 1, 1, '2014-03-08', 0, '2014-05-23 12:07:59');
INSERT INTO `tour_allocationdaily` VALUES (974, 1, 1, '2014-03-09', 0, '2014-05-23 12:07:59');
INSERT INTO `tour_allocationdaily` VALUES (975, 1, 1, '2014-03-10', 0, '2014-05-23 12:07:59');
INSERT INTO `tour_allocationdaily` VALUES (976, 1, 1, '2014-03-11', 0, '2014-05-23 12:07:59');
INSERT INTO `tour_allocationdaily` VALUES (977, 1, 1, '2014-03-12', 0, '2014-05-23 12:07:59');
INSERT INTO `tour_allocationdaily` VALUES (978, 1, 1, '2014-03-13', 0, '2014-05-23 12:07:59');
INSERT INTO `tour_allocationdaily` VALUES (979, 1, 1, '2014-03-14', 0, '2014-05-23 12:07:59');
INSERT INTO `tour_allocationdaily` VALUES (980, 1, 1, '2014-03-15', 0, '2014-05-23 12:07:59');
INSERT INTO `tour_allocationdaily` VALUES (981, 1, 1, '2014-03-16', 0, '2014-05-23 12:07:59');
INSERT INTO `tour_allocationdaily` VALUES (982, 1, 1, '2014-03-17', 0, '2014-05-23 12:07:59');
INSERT INTO `tour_allocationdaily` VALUES (983, 1, 1, '2014-03-18', 0, '2014-05-23 12:07:59');
INSERT INTO `tour_allocationdaily` VALUES (984, 1, 1, '2014-03-19', 0, '2014-05-23 12:07:59');
INSERT INTO `tour_allocationdaily` VALUES (985, 1, 1, '2014-03-20', 0, '2014-05-23 12:07:59');
INSERT INTO `tour_allocationdaily` VALUES (986, 1, 1, '2014-03-21', 0, '2014-05-23 12:07:59');
INSERT INTO `tour_allocationdaily` VALUES (987, 1, 1, '2014-03-22', 0, '2014-05-23 12:07:59');
INSERT INTO `tour_allocationdaily` VALUES (988, 1, 1, '2014-03-23', 0, '2014-05-23 12:07:59');
INSERT INTO `tour_allocationdaily` VALUES (989, 1, 1, '2014-03-24', 0, '2014-05-23 12:07:59');
INSERT INTO `tour_allocationdaily` VALUES (990, 1, 1, '2014-03-25', 0, '2014-05-23 12:07:59');
INSERT INTO `tour_allocationdaily` VALUES (991, 1, 1, '2014-03-26', 0, '2014-05-23 12:07:59');
INSERT INTO `tour_allocationdaily` VALUES (992, 1, 1, '2014-03-27', 0, '2014-05-23 12:07:59');
INSERT INTO `tour_allocationdaily` VALUES (993, 1, 1, '2014-03-28', 0, '2014-05-23 12:07:59');
INSERT INTO `tour_allocationdaily` VALUES (994, 1, 1, '2014-03-29', 0, '2014-05-23 12:07:59');
INSERT INTO `tour_allocationdaily` VALUES (995, 1, 1, '2014-03-30', 0, '2014-05-23 12:07:59');
INSERT INTO `tour_allocationdaily` VALUES (996, 1, 1, '2014-03-31', 0, '2014-05-23 12:07:59');
INSERT INTO `tour_allocationdaily` VALUES (997, 1, 1, '2014-04-01', 0, '2014-05-23 12:07:59');
INSERT INTO `tour_allocationdaily` VALUES (998, 1, 1, '2014-04-02', 0, '2014-05-23 12:07:59');
INSERT INTO `tour_allocationdaily` VALUES (999, 1, 1, '2014-04-03', 0, '2014-05-23 12:07:59');
INSERT INTO `tour_allocationdaily` VALUES (1000, 1, 1, '2014-04-04', 0, '2014-05-23 12:07:59');
INSERT INTO `tour_allocationdaily` VALUES (1001, 1, 1, '2014-04-05', 0, '2014-05-23 12:07:59');
INSERT INTO `tour_allocationdaily` VALUES (1002, 1, 1, '2014-04-06', 0, '2014-05-23 12:07:59');
INSERT INTO `tour_allocationdaily` VALUES (1003, 1, 1, '2014-04-07', 0, '2014-05-23 12:07:59');
INSERT INTO `tour_allocationdaily` VALUES (1004, 1, 1, '2014-04-08', 0, '2014-05-23 12:07:59');
INSERT INTO `tour_allocationdaily` VALUES (1005, 1, 1, '2014-04-09', 0, '2014-05-23 12:07:59');
INSERT INTO `tour_allocationdaily` VALUES (1006, 1, 1, '2014-04-10', 0, '2014-05-23 12:07:59');
INSERT INTO `tour_allocationdaily` VALUES (1007, 1, 1, '2014-04-11', 0, '2014-05-23 12:07:59');
INSERT INTO `tour_allocationdaily` VALUES (1008, 1, 1, '2014-04-12', 0, '2014-05-23 12:07:59');
INSERT INTO `tour_allocationdaily` VALUES (1009, 1, 1, '2014-04-13', 0, '2014-05-23 12:07:59');
INSERT INTO `tour_allocationdaily` VALUES (1010, 1, 1, '2014-04-14', 0, '2014-05-23 12:07:59');
INSERT INTO `tour_allocationdaily` VALUES (1011, 1, 1, '2014-04-15', 0, '2014-05-23 12:07:59');
INSERT INTO `tour_allocationdaily` VALUES (1012, 1, 1, '2014-04-16', 0, '2014-05-23 12:07:59');
INSERT INTO `tour_allocationdaily` VALUES (1013, 1, 1, '2014-04-17', 0, '2014-05-23 12:07:59');
INSERT INTO `tour_allocationdaily` VALUES (1014, 1, 1, '2014-04-18', 0, '2014-05-23 12:07:59');
INSERT INTO `tour_allocationdaily` VALUES (1015, 1, 1, '2014-04-19', 0, '2014-05-23 12:07:59');
INSERT INTO `tour_allocationdaily` VALUES (1016, 1, 1, '2014-04-20', 0, '2014-05-23 12:07:59');
INSERT INTO `tour_allocationdaily` VALUES (1017, 1, 1, '2014-04-21', 0, '2014-05-23 12:07:59');
INSERT INTO `tour_allocationdaily` VALUES (1018, 1, 1, '2014-04-22', 0, '2014-05-23 12:07:59');
INSERT INTO `tour_allocationdaily` VALUES (1019, 1, 1, '2014-04-23', 0, '2014-05-23 12:07:59');
INSERT INTO `tour_allocationdaily` VALUES (1020, 1, 1, '2014-04-24', 0, '2014-05-23 12:07:59');
INSERT INTO `tour_allocationdaily` VALUES (1021, 1, 1, '2014-04-25', 0, '2014-05-23 12:07:59');
INSERT INTO `tour_allocationdaily` VALUES (1022, 1, 1, '2014-04-26', 0, '2014-05-23 12:07:59');
INSERT INTO `tour_allocationdaily` VALUES (1023, 1, 1, '2014-04-27', 0, '2014-05-23 12:07:59');
INSERT INTO `tour_allocationdaily` VALUES (1024, 1, 1, '2014-04-28', 0, '2014-05-23 12:07:59');
INSERT INTO `tour_allocationdaily` VALUES (1025, 1, 1, '2014-04-29', 0, '2014-05-23 12:07:59');
INSERT INTO `tour_allocationdaily` VALUES (1026, 1, 1, '2014-04-30', 0, '2014-05-23 12:07:59');
INSERT INTO `tour_allocationdaily` VALUES (1027, 1, 1, '2014-05-01', 0, '2014-05-23 12:07:59');
INSERT INTO `tour_allocationdaily` VALUES (1028, 1, 1, '2014-05-02', 0, '2014-05-23 12:07:59');
INSERT INTO `tour_allocationdaily` VALUES (1029, 1, 1, '2014-05-03', 0, '2014-05-23 12:07:59');
INSERT INTO `tour_allocationdaily` VALUES (1030, 1, 1, '2014-05-04', 0, '2014-05-23 12:07:59');
INSERT INTO `tour_allocationdaily` VALUES (1031, 1, 1, '2014-05-05', 0, '2014-05-23 12:07:59');
INSERT INTO `tour_allocationdaily` VALUES (1032, 1, 1, '2014-05-06', 0, '2014-05-23 12:07:59');
INSERT INTO `tour_allocationdaily` VALUES (1033, 1, 1, '2014-05-07', 0, '2014-05-23 12:07:59');
INSERT INTO `tour_allocationdaily` VALUES (1034, 1, 1, '2014-05-08', 0, '2014-05-23 12:07:59');
INSERT INTO `tour_allocationdaily` VALUES (1035, 1, 1, '2014-05-09', 0, '2014-05-23 12:07:59');
INSERT INTO `tour_allocationdaily` VALUES (1036, 1, 1, '2014-05-10', 0, '2014-05-23 12:07:59');
INSERT INTO `tour_allocationdaily` VALUES (1037, 1, 1, '2014-05-11', 0, '2014-05-23 12:07:59');
INSERT INTO `tour_allocationdaily` VALUES (1038, 1, 1, '2014-05-12', 0, '2014-05-23 12:07:59');
INSERT INTO `tour_allocationdaily` VALUES (1039, 1, 1, '2014-05-13', 0, '2014-05-23 12:07:59');
INSERT INTO `tour_allocationdaily` VALUES (1040, 1, 1, '2014-05-14', 0, '2014-05-23 12:07:59');
INSERT INTO `tour_allocationdaily` VALUES (1041, 1, 1, '2014-05-15', 0, '2014-05-23 12:07:59');
INSERT INTO `tour_allocationdaily` VALUES (1042, 1, 1, '2014-05-16', 0, '2014-05-23 12:07:59');
INSERT INTO `tour_allocationdaily` VALUES (1043, 1, 1, '2014-05-17', 0, '2014-05-23 12:07:59');
INSERT INTO `tour_allocationdaily` VALUES (1044, 1, 1, '2014-05-18', 0, '2014-05-23 12:07:59');
INSERT INTO `tour_allocationdaily` VALUES (1045, 1, 1, '2014-05-19', 0, '2014-05-23 12:07:59');
INSERT INTO `tour_allocationdaily` VALUES (1046, 1, 1, '2014-05-20', 0, '2014-05-23 12:07:59');
INSERT INTO `tour_allocationdaily` VALUES (1047, 1, 1, '2014-05-21', 0, '2014-05-23 12:07:59');
INSERT INTO `tour_allocationdaily` VALUES (1048, 1, 1, '2014-05-22', 0, '2014-05-23 12:07:59');
INSERT INTO `tour_allocationdaily` VALUES (1049, 1, 1, '2014-05-23', 0, '2014-05-23 12:07:59');

-- --------------------------------------------------------

-- 
-- Table structure for table `tour_con_area`
-- 

CREATE TABLE `tour_con_area` (
  `con_id` int(3) unsigned zerofill NOT NULL auto_increment,
  `con_name` varchar(200) collate utf8_unicode_ci NOT NULL,
  `photo1` varchar(100) collate utf8_unicode_ci NOT NULL,
  `last_edit_time` datetime NOT NULL,
  `province_id` int(11) NOT NULL,
  PRIMARY KEY  (`con_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=16 ;

-- 
-- Dumping data for table `tour_con_area`
-- 

INSERT INTO `tour_con_area` VALUES (001, 'Area01', '', '2014-04-03 16:55:10', 8);
INSERT INTO `tour_con_area` VALUES (002, 'Area02', '', '2014-04-03 16:25:12', 2);
INSERT INTO `tour_con_area` VALUES (003, 'Area03', '', '2014-04-03 16:25:20', 2);
INSERT INTO `tour_con_area` VALUES (007, 'Kata', '', '2014-04-03 16:50:49', 1);
INSERT INTO `tour_con_area` VALUES (008, 'Patong', '', '2014-04-03 16:50:45', 1);
INSERT INTO `tour_con_area` VALUES (009, 'Phi Phi Island', '', '2014-07-16 16:53:25', 1);
INSERT INTO `tour_con_area` VALUES (010, 'Racha Island', '', '2014-07-16 16:53:38', 1);
INSERT INTO `tour_con_area` VALUES (011, 'Coral Island', '', '2014-07-16 16:53:51', 1);
INSERT INTO `tour_con_area` VALUES (012, 'Phang Nga Bay', '', '2014-07-16 16:54:08', 1);
INSERT INTO `tour_con_area` VALUES (013, 'Khai Island', '', '2014-07-16 16:54:27', 1);
INSERT INTO `tour_con_area` VALUES (014, 'Kamala Beach', '', '2014-07-16 16:54:57', 1);
INSERT INTO `tour_con_area` VALUES (015, 'Chalong', '', '2014-07-16 16:55:34', 1);

-- --------------------------------------------------------

-- 
-- Table structure for table `tour_con_category`
-- 

CREATE TABLE `tour_con_category` (
  `con_id` int(3) unsigned zerofill NOT NULL auto_increment,
  `con_name` varchar(200) collate utf8_unicode_ci NOT NULL,
  `photo1` varchar(100) collate utf8_unicode_ci NOT NULL,
  `last_edit_time` datetime NOT NULL,
  PRIMARY KEY  (`con_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=14 ;

-- 
-- Dumping data for table `tour_con_category`
-- 

INSERT INTO `tour_con_category` VALUES (001, 'Island Tour', '', '2014-04-03 16:57:50');
INSERT INTO `tour_con_category` VALUES (003, 'Phuket Adventure Tour ', '', '2014-04-03 16:57:45');
INSERT INTO `tour_con_category` VALUES (007, 'City Tour', '', '2014-04-03 17:07:07');
INSERT INTO `tour_con_category` VALUES (009, 'Canoe/Kayak Tour', '', '2014-07-16 16:56:10');
INSERT INTO `tour_con_category` VALUES (010, 'Speed Boat Tour', '', '2014-07-16 16:56:22');
INSERT INTO `tour_con_category` VALUES (011, 'Big Boat Tour', '', '2014-07-16 16:56:33');
INSERT INTO `tour_con_category` VALUES (012, 'Fishing Tour', '', '2014-07-16 16:56:47');
INSERT INTO `tour_con_category` VALUES (013, 'Sightseeing Tour', '', '2014-07-16 16:57:03');

-- --------------------------------------------------------

-- 
-- Table structure for table `tour_con_country`
-- 

CREATE TABLE `tour_con_country` (
  `con_id` int(3) unsigned zerofill NOT NULL auto_increment,
  `con_name` varchar(200) collate utf8_unicode_ci NOT NULL,
  `photo1` varchar(100) collate utf8_unicode_ci NOT NULL,
  `last_edit_time` datetime NOT NULL,
  PRIMARY KEY  (`con_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=11 ;

-- 
-- Dumping data for table `tour_con_country`
-- 

INSERT INTO `tour_con_country` VALUES (001, 'Thailand', '', '2014-04-03 15:58:12');
INSERT INTO `tour_con_country` VALUES (002, 'Malaysia', '', '2014-04-03 15:55:01');
INSERT INTO `tour_con_country` VALUES (010, 'Indonesia', '', '2014-04-03 16:31:06');

-- --------------------------------------------------------

-- 
-- Table structure for table `tour_con_province`
-- 

CREATE TABLE `tour_con_province` (
  `con_id` int(3) unsigned zerofill NOT NULL auto_increment,
  `con_name` varchar(200) collate utf8_unicode_ci NOT NULL,
  `photo1` varchar(100) collate utf8_unicode_ci NOT NULL,
  `last_edit_time` datetime NOT NULL,
  `country_id` int(11) NOT NULL,
  PRIMARY KEY  (`con_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=9 ;

-- 
-- Dumping data for table `tour_con_province`
-- 

INSERT INTO `tour_con_province` VALUES (001, 'Phuket', '', '2014-04-03 16:31:43', 1);
INSERT INTO `tour_con_province` VALUES (002, 'Krabi', '', '2014-04-03 16:16:21', 1);
INSERT INTO `tour_con_province` VALUES (008, 'Kuala Lumpur', '', '2014-04-03 16:36:31', 2);
INSERT INTO `tour_con_province` VALUES (007, 'Jakarta', '', '2014-04-03 16:35:53', 10);

-- --------------------------------------------------------

-- 
-- Table structure for table `tour_period`
-- 

CREATE TABLE `tour_period` (
  `toupe_id` int(11) NOT NULL auto_increment,
  `tours_id` int(11) NOT NULL,
  `toupe_name` varchar(200) collate utf8_unicode_ci NOT NULL,
  `toupe_datefrom` date NOT NULL,
  `toupe_dateto` date NOT NULL,
  `last_edit_time` datetime NOT NULL,
  `arrange` int(11) NOT NULL,
  PRIMARY KEY  (`toupe_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=15 ;

-- 
-- Dumping data for table `tour_period`
-- 

INSERT INTO `tour_period` VALUES (6, 1, 'Season02', '2014-04-01', '2014-06-30', '2014-04-07 10:20:02', 0);
INSERT INTO `tour_period` VALUES (2, 1, 'Season01', '2014-01-01', '2014-03-30', '2014-04-07 10:16:38', 0);
INSERT INTO `tour_period` VALUES (10, 1, 'Season04', '2014-10-01', '2014-12-31', '2014-04-07 10:25:46', 0);
INSERT INTO `tour_period` VALUES (9, 1, 'Season03', '2014-07-01', '2014-09-30', '2014-04-07 10:25:24', 0);
INSERT INTO `tour_period` VALUES (11, 2, 'Period 2014', '2014-01-01', '2014-12-31', '2014-04-09 17:18:27', 0);
INSERT INTO `tour_period` VALUES (12, 2, 'Period 2015', '2015-01-01', '2015-12-31', '2014-04-21 12:14:50', 0);
INSERT INTO `tour_period` VALUES (13, 3, 'Period 2014', '2014-04-23', '2014-10-31', '2014-04-23 16:39:56', 0);
INSERT INTO `tour_period` VALUES (14, 4, 'Period 2014', '2014-04-24', '2014-10-31', '2014-04-24 11:03:21', 0);

-- --------------------------------------------------------

-- 
-- Table structure for table `tour_price_type`
-- 

CREATE TABLE `tour_price_type` (
  `t_id` int(11) NOT NULL auto_increment,
  `t_name` varchar(200) collate utf8_unicode_ci NOT NULL,
  `last_edit_time` datetime NOT NULL,
  `photo1` varchar(100) collate utf8_unicode_ci NOT NULL,
  PRIMARY KEY  (`t_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

-- 
-- Dumping data for table `tour_price_type`
-- 

INSERT INTO `tour_price_type` VALUES (1, 'แบบกำหนดเปอร์เซ็นต์ส่วนลด', '2014-04-03 15:20:41', '');
INSERT INTO `tour_price_type` VALUES (2, 'แบบกำหนดราคาเอง', '2014-04-03 15:20:45', '');

-- --------------------------------------------------------

-- 
-- Table structure for table `tour_ratepercents`
-- 

CREATE TABLE `tour_ratepercents` (
  `tourateper_id` int(11) NOT NULL auto_increment,
  `tours_id` int(11) NOT NULL,
  `agentgrade_id` int(11) NOT NULL,
  `tourateper_discount` int(3) NOT NULL,
  `last_edit_time` datetime NOT NULL,
  PRIMARY KEY  (`tourateper_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=11 ;

-- 
-- Dumping data for table `tour_ratepercents`
-- 

INSERT INTO `tour_ratepercents` VALUES (1, 1, 1, 50, '2014-04-07 15:17:46');
INSERT INTO `tour_ratepercents` VALUES (2, 1, 2, 40, '2014-04-07 15:17:46');
INSERT INTO `tour_ratepercents` VALUES (3, 1, 3, 30, '2014-04-07 15:17:46');
INSERT INTO `tour_ratepercents` VALUES (4, 1, 4, 20, '2014-04-07 15:17:46');
INSERT INTO `tour_ratepercents` VALUES (5, 1, 5, 10, '2014-04-07 15:17:46');
INSERT INTO `tour_ratepercents` VALUES (6, 2, 1, 30, '2014-04-09 17:20:31');
INSERT INTO `tour_ratepercents` VALUES (7, 2, 2, 25, '2014-04-09 17:20:31');
INSERT INTO `tour_ratepercents` VALUES (8, 2, 3, 20, '2014-04-09 17:20:31');
INSERT INTO `tour_ratepercents` VALUES (9, 2, 4, 10, '2014-04-09 17:20:31');
INSERT INTO `tour_ratepercents` VALUES (10, 2, 5, 0, '2014-04-09 17:20:31');

-- --------------------------------------------------------

-- 
-- Table structure for table `tour_rates`
-- 

CREATE TABLE `tour_rates` (
  `tourate_id` int(11) NOT NULL auto_increment,
  `tours_id` int(11) NOT NULL,
  `tourratetype_id` int(11) NOT NULL,
  `tourperiod_id` int(11) NOT NULL,
  `agentgrade_id` int(11) NOT NULL,
  `rate_1` double(15,2) NOT NULL,
  `rate_2` double(15,2) NOT NULL,
  `rate_3` double(15,2) NOT NULL,
  `rate_4` double(15,2) NOT NULL,
  `rate_5` double(15,2) NOT NULL,
  `last_edit_time` datetime NOT NULL,
  PRIMARY KEY  (`tourate_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=54 ;

-- 
-- Dumping data for table `tour_rates`
-- 

INSERT INTO `tour_rates` VALUES (1, 1, 1, 2, 0, 1000.00, 1100.00, 1200.00, 0.00, 0.00, '2014-04-07 12:18:01');
INSERT INTO `tour_rates` VALUES (2, 1, 1, 6, 0, 2000.00, 2100.00, 2200.00, 0.00, 0.00, '2014-04-07 12:18:01');
INSERT INTO `tour_rates` VALUES (3, 1, 1, 9, 0, 3000.00, 3100.00, 3200.00, 0.00, 0.00, '2014-04-07 12:18:01');
INSERT INTO `tour_rates` VALUES (4, 1, 1, 10, 0, 4000.00, 4100.00, 4200.00, 0.00, 0.00, '2014-04-07 12:18:01');
INSERT INTO `tour_rates` VALUES (5, 1, 2, 2, 0, 5000.00, 5100.00, 5200.00, 0.00, 0.00, '2014-04-07 12:18:01');
INSERT INTO `tour_rates` VALUES (6, 1, 2, 6, 0, 6000.00, 6100.00, 6200.00, 0.00, 0.00, '2014-04-07 12:18:01');
INSERT INTO `tour_rates` VALUES (7, 1, 2, 9, 0, 7000.00, 7100.00, 7200.00, 0.00, 0.00, '2014-04-07 12:18:01');
INSERT INTO `tour_rates` VALUES (8, 1, 2, 10, 0, 8000.00, 8100.00, 8200.00, 0.00, 0.00, '2014-04-07 12:18:01');
INSERT INTO `tour_rates` VALUES (9, 1, 3, 2, 0, 9000.00, 9100.00, 9200.00, 0.00, 0.00, '2014-04-07 12:18:01');
INSERT INTO `tour_rates` VALUES (10, 1, 3, 6, 0, 10000.00, 10100.00, 10200.00, 0.00, 0.00, '2014-04-07 12:18:01');
INSERT INTO `tour_rates` VALUES (11, 1, 3, 9, 0, 11000.00, 11100.00, 11200.00, 0.00, 0.00, '2014-04-07 12:18:01');
INSERT INTO `tour_rates` VALUES (12, 1, 3, 10, 0, 12000.00, 12100.00, 12200.00, 0.00, 0.00, '2014-04-07 12:18:01');
INSERT INTO `tour_rates` VALUES (13, 1, 1, 2, 1, 1001.00, 1101.00, 3201.00, 0.00, 0.00, '2014-04-07 12:34:17');
INSERT INTO `tour_rates` VALUES (14, 1, 1, 6, 1, 2001.00, 2101.00, 3201.00, 0.00, 0.00, '2014-04-07 12:34:17');
INSERT INTO `tour_rates` VALUES (15, 1, 1, 9, 1, 3001.00, 3101.00, 3201.00, 0.00, 0.00, '2014-04-07 12:34:17');
INSERT INTO `tour_rates` VALUES (16, 1, 1, 10, 1, 4001.00, 4101.00, 4201.00, 0.00, 0.00, '2014-04-07 12:34:17');
INSERT INTO `tour_rates` VALUES (17, 1, 2, 2, 1, 5001.00, 5101.00, 5201.00, 0.00, 0.00, '2014-04-07 12:34:17');
INSERT INTO `tour_rates` VALUES (18, 1, 2, 6, 1, 6001.00, 6101.00, 6201.00, 0.00, 0.00, '2014-04-07 12:34:17');
INSERT INTO `tour_rates` VALUES (19, 1, 2, 9, 1, 7001.00, 7101.00, 7201.00, 0.00, 0.00, '2014-04-07 12:34:17');
INSERT INTO `tour_rates` VALUES (20, 1, 2, 10, 1, 8001.00, 8101.00, 8201.00, 0.00, 0.00, '2014-04-07 12:34:17');
INSERT INTO `tour_rates` VALUES (21, 1, 3, 2, 1, 9001.00, 9101.00, 9201.00, 0.00, 0.00, '2014-04-07 12:34:17');
INSERT INTO `tour_rates` VALUES (22, 1, 3, 6, 1, 10001.00, 10101.00, 10201.00, 0.00, 0.00, '2014-04-07 12:34:17');
INSERT INTO `tour_rates` VALUES (23, 1, 3, 9, 1, 11001.00, 11101.00, 11201.00, 0.00, 0.00, '2014-04-07 12:34:17');
INSERT INTO `tour_rates` VALUES (24, 1, 3, 10, 1, 12001.00, 12101.00, 12201.00, 0.00, 0.00, '2014-04-07 12:34:17');
INSERT INTO `tour_rates` VALUES (25, 1, 1, 2, 2, 1002.00, 1102.00, 1202.00, 0.00, 0.00, '2014-04-07 13:21:24');
INSERT INTO `tour_rates` VALUES (26, 1, 1, 6, 2, 2002.00, 2102.00, 2202.00, 0.00, 0.00, '2014-04-07 13:21:24');
INSERT INTO `tour_rates` VALUES (27, 1, 1, 9, 2, 3002.00, 3102.00, 3202.00, 0.00, 0.00, '2014-04-07 13:21:24');
INSERT INTO `tour_rates` VALUES (28, 1, 1, 10, 2, 4002.00, 4102.00, 4202.00, 0.00, 0.00, '2014-04-07 13:21:24');
INSERT INTO `tour_rates` VALUES (29, 1, 2, 2, 2, 5002.00, 5102.00, 5202.00, 0.00, 0.00, '2014-04-07 13:21:24');
INSERT INTO `tour_rates` VALUES (30, 1, 2, 6, 2, 6002.00, 6102.00, 6202.00, 0.00, 0.00, '2014-04-07 13:21:24');
INSERT INTO `tour_rates` VALUES (31, 1, 2, 9, 2, 7002.00, 7102.00, 7202.00, 0.00, 0.00, '2014-04-07 13:21:24');
INSERT INTO `tour_rates` VALUES (32, 1, 2, 10, 2, 8002.00, 8102.00, 8202.00, 0.00, 0.00, '2014-04-07 13:21:24');
INSERT INTO `tour_rates` VALUES (33, 1, 3, 2, 2, 9002.00, 9102.00, 9202.00, 0.00, 0.00, '2014-04-07 13:21:24');
INSERT INTO `tour_rates` VALUES (34, 1, 3, 6, 2, 10002.00, 10102.00, 10202.00, 0.00, 0.00, '2014-04-07 13:21:24');
INSERT INTO `tour_rates` VALUES (35, 1, 3, 9, 2, 11002.00, 11102.00, 11202.00, 0.00, 0.00, '2014-04-07 13:21:24');
INSERT INTO `tour_rates` VALUES (36, 1, 3, 10, 2, 12002.00, 12102.00, 12202.00, 0.00, 0.00, '2014-04-07 13:21:24');
INSERT INTO `tour_rates` VALUES (37, 1, 1, 2, 3, 1003.00, 1103.00, 1203.00, 0.00, 0.00, '2014-04-07 13:25:39');
INSERT INTO `tour_rates` VALUES (38, 1, 1, 6, 3, 2003.00, 2103.00, 2203.00, 0.00, 0.00, '2014-04-07 13:25:39');
INSERT INTO `tour_rates` VALUES (39, 1, 1, 9, 3, 3003.00, 3103.00, 3203.00, 0.00, 0.00, '2014-04-07 13:25:39');
INSERT INTO `tour_rates` VALUES (40, 1, 1, 10, 3, 4003.00, 4103.00, 4203.00, 0.00, 0.00, '2014-04-07 13:25:39');
INSERT INTO `tour_rates` VALUES (41, 1, 2, 2, 3, 5003.00, 5103.00, 5203.00, 0.00, 0.00, '2014-04-07 13:25:39');
INSERT INTO `tour_rates` VALUES (42, 1, 2, 6, 3, 6003.00, 6103.00, 6203.00, 0.00, 0.00, '2014-04-07 13:25:39');
INSERT INTO `tour_rates` VALUES (43, 1, 2, 9, 3, 7003.00, 7103.00, 7203.00, 0.00, 0.00, '2014-04-07 13:25:39');
INSERT INTO `tour_rates` VALUES (44, 1, 2, 10, 3, 8003.00, 8103.00, 8203.00, 0.00, 0.00, '2014-04-07 13:25:39');
INSERT INTO `tour_rates` VALUES (45, 1, 3, 2, 3, 9003.00, 9103.00, 9203.00, 0.00, 0.00, '2014-04-07 13:25:39');
INSERT INTO `tour_rates` VALUES (46, 1, 3, 6, 3, 10003.00, 10103.00, 10203.00, 0.00, 0.00, '2014-04-07 13:25:39');
INSERT INTO `tour_rates` VALUES (47, 1, 3, 9, 3, 11003.00, 11103.00, 11203.00, 0.00, 0.00, '2014-04-07 13:25:39');
INSERT INTO `tour_rates` VALUES (48, 1, 3, 10, 3, 12003.00, 12103.00, 12203.00, 0.00, 0.00, '2014-04-07 13:25:39');
INSERT INTO `tour_rates` VALUES (49, 2, 8, 11, 0, 8000.00, 7000.00, 0.00, 0.00, 0.00, '2014-04-09 17:19:13');
INSERT INTO `tour_rates` VALUES (50, 2, 9, 11, 0, 8500.00, 7500.00, 0.00, 0.00, 0.00, '2014-04-09 17:19:13');
INSERT INTO `tour_rates` VALUES (51, 3, 11, 13, 0, 1800.00, 1500.00, 0.00, 0.00, 0.00, '2014-04-23 16:40:27');
INSERT INTO `tour_rates` VALUES (52, 3, 11, 13, 1, 1200.00, 900.00, 0.00, 0.00, 0.00, '2014-04-23 16:40:47');
INSERT INTO `tour_rates` VALUES (53, 4, 12, 14, 0, 900.00, 700.00, 0.00, 0.00, 0.00, '2014-05-20 14:54:17');

-- --------------------------------------------------------

-- 
-- Table structure for table `tour_ratetypes`
-- 

CREATE TABLE `tour_ratetypes` (
  `tourt_id` int(11) NOT NULL auto_increment,
  `tours_id` int(11) NOT NULL,
  `tourt_name` varchar(200) collate utf8_unicode_ci NOT NULL,
  `photo1` varchar(100) collate utf8_unicode_ci NOT NULL,
  `tourt_detail` text collate utf8_unicode_ci NOT NULL,
  `last_edit_time` datetime NOT NULL,
  PRIMARY KEY  (`tourt_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=13 ;

-- 
-- Dumping data for table `tour_ratetypes`
-- 

INSERT INTO `tour_ratetypes` VALUES (1, 1, 'Tour ครึ่งวัน (บ่าย)', '', '', '2014-04-07 12:34:50');
INSERT INTO `tour_ratetypes` VALUES (2, 1, 'Tour ครึ่งวัน (เช้า)', '', '', '2014-04-07 12:34:46');
INSERT INTO `tour_ratetypes` VALUES (3, 1, 'Tour เต็มวัน', '', '', '2014-04-07 12:34:41');
INSERT INTO `tour_ratetypes` VALUES (8, 2, 'Phi Phi Island Tour', '', '', '2014-04-09 17:16:16');
INSERT INTO `tour_ratetypes` VALUES (10, 2, 'Phi Phi Island Tour + Lunch + Transfer', '', '', '2014-04-15 10:22:06');
INSERT INTO `tour_ratetypes` VALUES (11, 3, 'James Bonds Tour with Lunch', '', '', '2014-04-23 16:20:22');
INSERT INTO `tour_ratetypes` VALUES (12, 4, 'PP Day Trip ', '', '', '2014-04-24 11:02:52');
