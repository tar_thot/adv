<?php session_start();
error_reporting(E_ALL & ~E_NOTICE); ?>

<?php require("lib/connect.php") ?>
<?php require("lib/function.php") ?>
<?php require("lib/constant.php") ?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <!--<meta http-equiv="Content-Type" content="text/html; charset=tis-620"> -->
    <title>Admin S.S.ADV.</title>
    <link href="styles/font.css" rel="stylesheet" type="text/css">
    <!-- Add jQuery -->
    <!--<script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script> -->
    <script type="text/javascript" src="./javascript/jquery/jquery-1.5.2.min.js"></script>
    <script type="text/javascript" src="./javascript/calendar/calendarDateInput.js"></script>
</head>
<body>
<div style="width:100%; height:100%; margin:0px; margin-left:auto; margin-right:auto; position:absolute; padding:0px;"
     align="center">
    <?php

    // Convert Variable Array To Variable

    while (list($xVarName, $xVarvalue) = each($_GET)) {
        ${$xVarName} = $xVarvalue;
    }


    while (list($xVarName, $xVarvalue) = each($_POST)) {
        ${$xVarName} = $xVarvalue;
    }

    while (list($xVarName, $xVarvalue) = each($_FILES)) {
        ${$xVarName . "_name"} = $xVarvalue['name'];
        ${$xVarName . "_type"} = $xVarvalue['type'];
        ${$xVarName . "_size"} = $xVarvalue['size'];
        ${$xVarName . "_error"} = $xVarvalue['error'];
        ${$xVarName} = $xVarvalue['tmp_name'];
    }

    $perms = $_SESSION['perms'];

    if (isset($mode) && isset($perms["ID"])) {

        ?>
        <table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td height="126" valign="top" align="left">
                    <?php include("./lib/inc.top.php") ?>
                    <?php include("./lib/inc.topbar.php") ?>
                </td>
            </tr>
            <tr>
                <td valign="top" height="100%">
                    <table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td width="200" bgcolor="#DDDDDD" valign="top"
                                align="left"><?php include("./lib/inc.leftmenu.php") ?></td>
                            <td valign="top">&nbsp;</td>
                            <td valign="top" height="100%">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td valign="top" height="74">
                                            <table width="98%" height="39" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td width="400"><img src="images/sub-wel.jpg" width="400"
                                                                         height="39" border="0"></td>
                                                    <td align="right"><?= DateFormat($today, f) ?></td>
                                                </tr>
                                            </table>
                                            <table width="98%" height="35" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td width="300"><img src="images/pic-quick.jpg" width="300"
                                                                         height="35" border="0"></td>
                                                    <td background="images/bg-top.jpg">&nbsp;</td>
                                                    <td width="24"><img src="images/top-right.jpg" width="24"
                                                                        height="35" border="0"></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top" height="100%">
                                            <table width="98%" height="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td width="20" background="images/left.jpg"><img
                                                            src="images/left.jpg" width="20" height="244" border="0">
                                                    </td>
                                                    <td height="100%" align="left"
                                                        valign="top"><?php include("sections/form/$mode.php") ?></td>
                                                    <td width="24" background="images/right.jpg"><img
                                                            src="images/right.jpg" width="24" height="244" border="0">
                                                    </td>
                                                </tr>
                                                <!--<tr>
                                                    <td width="20" background="images/left.jpg">&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td width="24" background="images/right.jpg">&nbsp;</td>
                                                </tr> -->
                                                <tr>
                                                    <td width="20"><img src="images/un-left.jpg" width="20" height="20"
                                                                        border="0"></td>
                                                    <td height="20" background="images/un-q.jpg"><img
                                                            src="images/blank.gif" width="20" height="20" border="0">
                                                    </td>
                                                    <td width="24"><img src="images/un-right.jpg" width="24" height="20"
                                                                        border="0"></td>
                                                </tr>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <?php
    } else {
        include("sections/form/login/index.php");
    }
    ?>
    <?php include("lib/error.php") ?>
</div>
</body>
</html>