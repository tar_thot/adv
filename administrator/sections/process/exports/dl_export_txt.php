<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <!--<meta http-equiv="Content-Type" content="text/html; charset=tis-620"> -->
    <title>Admin Phuket Solution</title>
    <link href="styles/font.css" rel="stylesheet" type="text/css">
</head>
<body>

<?php

function setNumberLength($num, $length)
{
    $sumstr = strlen($num);
    $zero = str_repeat("0", $length - $sumstr);
    $results = $zero . $num;

    return $results;
}

function DateDiff($strDate1, $strDate2)
{
    return (strtotime($strDate2) - strtotime($strDate1)) / (60 * 60 * 24);  // 1 day = 60*60*24
}

$str_cddate = "$cddate";
$str_date = date("ymd", strtotime($str_cddate));
$str_date_show = date("d/m/Y", strtotime($str_cddate));

$fileName = "DL" . $str_date . ".txt";
$objWrite = fopen("../textfile/$fileName", "w") or die("can't open file");

?>

<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td valign="top">
            <!--<table width="100%" border="0" cellspacing="0" cellpadding="3">
                <tr>
                    <td class="txt_big_gray">DL List</td>
                    <td width="500" align="right"><a href="./index.php?mode=exports/dl_st1" style="background-color:#ffffff; color:#000000"><<< Back</a></td>
                </tr>
            </table>-->
            <!---- Listing Body ---->
            <table width="100%" border="0" cellspacing="0" cellpadding="3">
                <tr>
                    <td>
                        <!---- Search Box ---->
                        <br/>

                        <? //$strNewDate = date("Y-m-d", strtotime("+3 day", strtotime($_POST[to_date]))); ?>

                        <!--<table width="100%" align="center">
	<tr>
    	<td class="txt_bold_gray" align="center">Confirm Date: <?= DateFormat($_POST[from_date], "f") ?></td>
        
    </tr>
    <tr>
    	<td class="txt_bold_gray" align="right"><input type="button" name="export" id="export" value="Export .txt" onclick="window.location='process.php?mode=exports/dl_export_txt&cddate=<?= $_POST[from_date] ?>'" /></td>
        
    </tr>
</table>-->


                        <? // DL ?>

                        <br/>

                        <table width="50%" border="1" cellspacing="0" cellpadding="3" bgcolor="#FFFFFF"
                               bordercolor="#000000" align="center">
                            <tr bgcolor="#CCCCCC">
                                <td width="50" align="center" class="txt_bold_gray">Confirm Date</td>
                                <td width="50" align="center" class="txt_bold_gray">Payment Code</td>
                                <td width="100" align="center" class="txt_bold_gray">Payment Description</td>
                                <td width="50" align="center" class="txt_bold_gray">Qty</td>
                                <td width="100" align="center" class="txt_bold_gray">Amount</td>

                            </tr>

                            <? // Query Reservations

                            $sql = "SELECT * ";
                            $sql .= "FROM reservations ";
                            $sql .= "WHERE confirm_date = '$cddate' ";
                            $sql .= "AND bookingstatus_id = '3' ";
                            $sql .= "AND res_voucher_status = '1' ";
                            $sql .= "ORDER BY res_id ASC ";

                            //echo $sql;
                            //exit();

                            $result = mysql_query($sql);
                            while ($row = mysql_fetch_array($result)) {

                                // Get Value

                                $total_qty = 0;
                                $total_price = 0;

                                $code_agent = get_value(agents, ag_id, ag_ref, $row[agents_id]);
                                $agent_name = get_value(agents, ag_id, ag_name, $row[agents_id]);
                                $agentfor_id = get_value(agents, ag_id, agentfor_id, $row[agents_id]);
                                $agentfor_name = get_value(agent_for, a_id, a_name, $agentfor_id);
                                $lis_name = get_value(lis_titlename, lis_id, lis_name, $row[titlename_id]);
                                $comfirm_payment = get_value(lis_comfirm_payment, lis_id, lis_name, $row[confirm_pay_id]);

                                // Query Reservations Boat Transfer

                                $sql_boat = "SELECT * ";
                                $sql_boat .= "FROM reservation_boattransfer_items ";
                                $sql_boat .= "WHERE reservations_id = '$row[res_id]' ";
                                //$sql_boat .= "AND rbt_travel_date = '$_POST[from_date]' ";
                                $sql_boat .= "ORDER BY rbt_id ASC ";

                                $result_boat = mysql_query($sql_boat);
                                while ($row_boat = mysql_fetch_array($result_boat)) {

                                    $pax = "";
                                    $pax = $row_boat[rbt_adult_num] + $row_boat[rbt_child_num];

                                    $product_name = get_value(boattransfers, bot_id, bot_name, $row_boat[boattransfers_id]);

                                    $ac_andaman_adult = get_value(boattransfers, bot_id, ac_andaman_adult, $row_boat[boattransfers_id]);
                                    $ac_andaman_child = get_value(boattransfers, bot_id, ac_andaman_child, $row_boat[boattransfers_id]);
                                    $ac_dotcom_adult = get_value(boattransfers, bot_id, ac_dotcom_adult, $row_boat[boattransfers_id]);
                                    $ac_dotcom_child = get_value(boattransfers, bot_id, ac_dotcom_child, $row_boat[boattransfers_id]);
                                    $ac_btoc_adult = get_value(boattransfers, bot_id, ac_btoc_adult, $row_boat[boattransfers_id]);
                                    $ac_btoc_child = get_value(boattransfers, bot_id, ac_btoc_child, $row_boat[boattransfers_id]);

                                    $sql_vo = "SELECT * ";
                                    $sql_vo .= "FROM voucher ";
                                    $sql_vo .= "WHERE vo_res_id = '$row[res_id_str]' ";
                                    $sql_vo .= "AND vo_item_id = '$product_name' ";
                                    $sql_vo .= "AND vo_status = '2' ";
                                    $sql_vo .= "ORDER BY vo_id ASC ";

                                    $result_vo = mysql_query($sql_vo);
                                    $row_vo = mysql_fetch_array($result_vo);

                                    if ($agentfor_id == 1) {
                                        $code_adult = $ac_andaman_adult;
                                        $code_child = $ac_andaman_child;

                                    } else if ($agentfor_id == 2) {
                                        $code_adult = $ac_dotcom_adult;
                                        $code_child = $ac_dotcom_child;

                                    } else {
                                        $code_adult = $ac_btoc_adult;
                                        $code_child = $ac_btoc_child;

                                    }

                                    $price_adult = @($row_boat[rbt_adult_prices] / $row_boat[rbt_adult_num]);
                                    $price_child = @($row_boat[rbt_child_prices] / $row_boat[rbt_child_num]);
                                    ?>

                                    <!--<tr>
                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?= $_POST[from_date] ?></td>
                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?= "00" . $row[confirm_pay_id]; ?></td>
                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?= $comfirm_payment ?></td>
                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?= $pax ?></td>
                        <td class="txt_bold_gray" align="right" bgcolor="#F0F0F0"><?= number_format($row_boat[rbt_prices], 2) ?></td>
                        
                    </tr>-->

                                    <? $total_qty = $total_qty + $pax;
                                    $total_price = $total_price + $row_boat[rbt_prices];
                                } // END while($row_boat = mysql_fetch_array($result_boat)){
                                ?>


                                <?
                                // Query Reservations Pick up Transfer

                                $sql_pickup = "SELECT * ";
                                $sql_pickup .= "FROM reservation_pickuptransfer_items ";
                                $sql_pickup .= "WHERE reservations_id = '$row[res_id]' ";
                                //$sql_pickup .= "AND rpt_travel_date = '$_POST[from_date]' ";
                                $sql_pickup .= "ORDER BY rpt_id ASC ";

                                $result_pickup = mysql_query($sql_pickup);
                                while ($row_pickup = mysql_fetch_array($result_pickup)) {

                                    $pax = "";
                                    $pax = $row_pickup[rpt_adult_num] + $row_pickup[rpt_child_num];

                                    $product_name = get_value(pickuptransfers, put_id, put_name, $row_pickup[pickuptransfers_id]);

                                    $ac_andaman_adult = get_value(pickuptransfers, put_id, ac_andaman_adult, $row_pickup[pickuptransfers_id]);
                                    $ac_andaman_child = get_value(pickuptransfers, put_id, ac_andaman_child, $row_pickup[pickuptransfers_id]);
                                    $ac_dotcom_adult = get_value(pickuptransfers, put_id, ac_dotcom_adult, $row_pickup[pickuptransfers_id]);
                                    $ac_dotcom_child = get_value(pickuptransfers, put_id, ac_dotcom_child, $row_pickup[pickuptransfers_id]);
                                    $ac_btoc_adult = get_value(pickuptransfers, put_id, ac_btoc_adult, $row_pickup[pickuptransfers_id]);
                                    $ac_btoc_child = get_value(pickuptransfers, put_id, ac_btoc_child, $row_pickup[pickuptransfers_id]);

                                    $sql_vo = "SELECT * ";
                                    $sql_vo .= "FROM voucher ";
                                    $sql_vo .= "WHERE vo_res_id = '$row[res_id_str]' ";
                                    $sql_vo .= "AND vo_item_id = '$product_name' ";
                                    $sql_vo .= "AND vo_status = '2' ";
                                    $sql_vo .= "ORDER BY vo_id ASC ";

                                    $result_vo = mysql_query($sql_vo);
                                    $row_vo = mysql_fetch_array($result_vo);

                                    if ($agentfor_id == 1) {
                                        $code_adult = $ac_andaman_adult;
                                        $code_child = $ac_andaman_child;

                                    } else if ($agentfor_id == 2) {
                                        $code_adult = $ac_dotcom_adult;
                                        $code_child = $ac_dotcom_child;

                                    } else {
                                        $code_adult = $ac_btoc_adult;
                                        $code_child = $ac_btoc_child;

                                    }

                                    $price_adult = @($row_pickup[rpt_adult_prices] / $row_pickup[rpt_adult_num]);
                                    $price_child = @($row_pickup[rpt_child_prices] / $row_pickup[rpt_child_num]);
                                    ?>

                                    <!--<tr>
                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?= $_POST[from_date] ?></td>
                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?= "00" . $row[confirm_pay_id]; ?></td>
                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?= $comfirm_payment ?></td>
                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?= $pax ?></td>
                        <td class="txt_bold_gray" align="right" bgcolor="#F0F0F0"><?= number_format($row_pickup[rpt_prices], 2) ?></td>
                        
                    </tr>-->

                                    <? $total_qty = $total_qty + $pax;
                                    $total_price = $total_price + $row_pickup[rpt_prices];
                                } // END while($row_pickup = mysql_fetch_array($result_pickup)){
                                ?>


                                <?
                                // Query Reservations Tour

                                $sql_tour = "SELECT * ";
                                $sql_tour .= "FROM reservation_tour_items ";
                                $sql_tour .= "WHERE reservations_id = '$row[res_id]' ";
                                //$sql_tour .= "AND rtt_travel_date = '$_POST[from_date]' ";
                                $sql_tour .= "ORDER BY rtt_id ASC ";

                                $result_tour = mysql_query($sql_tour);
                                while ($row_tour = mysql_fetch_array($result_tour)) {

                                    $pax = "";
                                    $pax = $row_tour[rtt_adult_num] + $row_tour[rtt_child_num];

                                    $product_name = get_value(tours, tou_id, tou_name, $row_tour[tours_id]);

                                    $ac_andaman_adult = get_value(tours, tou_id, ac_andaman_adult, $row_tour[tours_id]);
                                    $ac_andaman_child = get_value(tours, tou_id, ac_andaman_child, $row_tour[tours_id]);
                                    $ac_dotcom_adult = get_value(tours, tou_id, ac_dotcom_adult, $row_tour[tours_id]);
                                    $ac_dotcom_child = get_value(tours, tou_id, ac_dotcom_child, $row_tour[tours_id]);
                                    $ac_btoc_adult = get_value(tours, tou_id, ac_btoc_adult, $row_tour[tours_id]);
                                    $ac_btoc_child = get_value(tours, tou_id, ac_btoc_child, $row_tour[tours_id]);

                                    $sql_vo = "SELECT * ";
                                    $sql_vo .= "FROM voucher ";
                                    $sql_vo .= "WHERE vo_res_id = '$row[res_id_str]' ";
                                    $sql_vo .= "AND vo_item_id = '$product_name' ";
                                    $sql_vo .= "AND vo_status = '2' ";
                                    $sql_vo .= "ORDER BY vo_id ASC ";

                                    $result_vo = mysql_query($sql_vo);
                                    $row_vo = mysql_fetch_array($result_vo);

                                    if ($agentfor_id == 1) {
                                        $code_adult = $ac_andaman_adult;
                                        $code_child = $ac_andaman_child;

                                    } else if ($agentfor_id == 2) {
                                        $code_adult = $ac_dotcom_adult;
                                        $code_child = $ac_dotcom_child;

                                    } else {
                                        $code_adult = $ac_btoc_adult;
                                        $code_child = $ac_btoc_child;

                                    }

                                    $price_adult = @($row_tour[rtt_adult_prices] / $row_tour[rtt_adult_num]);
                                    $price_child = @($row_tour[rtt_child_prices] / $row_tour[rtt_child_num]);
                                    ?>

                                    <!--<tr>
                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?= $_POST[from_date] ?></td>
                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?= "00" . $row[confirm_pay_id]; ?></td>
                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?= $comfirm_payment ?></td>
                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?= $pax ?></td>
                        <td class="txt_bold_gray" align="right" bgcolor="#F0F0F0"><?= number_format($row_tour[rtt_prices], 2) ?></td>
                        
                    </tr>-->

                                    <? $total_qty = $total_qty + $pax;
                                    $total_price = $total_price + $row_tour[rtt_prices];
                                } // END while($row_tour = mysql_fetch_array($result_tour)){
                                ?>


                                <?
                                // Query Reservations Activity

                                $sql_activity = "SELECT * ";
                                $sql_activity .= "FROM reservation_activity_items ";
                                $sql_activity .= "WHERE reservations_id = '$row[res_id]' ";
                                //$sql_activity .= "AND rat_travel_date = '$_POST[from_date]' ";
                                $sql_activity .= "ORDER BY rat_id ASC ";

                                $result_activity = mysql_query($sql_activity);
                                while ($row_activity = mysql_fetch_array($result_activity)) {

                                    $pax = "";
                                    $pax = $row_activity[rat_adult_num] + $row_activity[rat_child_num];

                                    $product_name = get_value(activities, act_id, act_name, $row_activity[activities_id]);

                                    $ac_andaman_adult = get_value(activities, act_id, ac_andaman_adult, $row_activity[activities_id]);
                                    $ac_andaman_child = get_value(activities, act_id, ac_andaman_child, $row_activity[activities_id]);
                                    $ac_dotcom_adult = get_value(activities, act_id, ac_dotcom_adult, $row_activity[activities_id]);
                                    $ac_dotcom_child = get_value(activities, act_id, ac_dotcom_child, $row_activity[activities_id]);
                                    $ac_btoc_adult = get_value(activities, act_id, ac_btoc_adult, $row_activity[activities_id]);
                                    $ac_btoc_child = get_value(activities, act_id, ac_btoc_child, $row_activity[activities_id]);

                                    $sql_vo = "SELECT * ";
                                    $sql_vo .= "FROM voucher ";
                                    $sql_vo .= "WHERE vo_res_id = '$row[res_id_str]' ";
                                    $sql_vo .= "AND vo_item_id = '$product_name' ";
                                    $sql_vo .= "AND vo_status = '2' ";
                                    $sql_vo .= "ORDER BY vo_id ASC ";

                                    $result_vo = mysql_query($sql_vo);
                                    $row_vo = mysql_fetch_array($result_vo);

                                    if ($agentfor_id == 1) {
                                        $code_adult = $ac_andaman_adult;
                                        $code_child = $ac_andaman_child;

                                    } else if ($agentfor_id == 2) {
                                        $code_adult = $ac_dotcom_adult;
                                        $code_child = $ac_dotcom_child;

                                    } else {
                                        $code_adult = $ac_btoc_adult;
                                        $code_child = $ac_btoc_child;

                                    }

                                    $price_adult = @($row_activity[rat_adult_prices] / $row_activity[rat_adult_num]);
                                    $price_child = @($row_activity[rat_child_prices] / $row_activity[rat_child_num]);
                                    ?>

                                    <!--<tr>
                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?= $_POST[from_date] ?></td>
                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?= "00" . $row[confirm_pay_id]; ?></td>
                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?= $comfirm_payment ?></td>
                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?= $pax ?></td>
                        <td class="txt_bold_gray" align="right" bgcolor="#F0F0F0"><?= number_format($row_activity[rat_prices], 2) ?></td>
                        
                    </tr>-->

                                    <? $total_qty = $total_qty + $pax;
                                    $total_price = $total_price + $row_activity[rat_prices];
                                } // END while($row_activity = mysql_fetch_array($result_activity)){
                                ?>


                                <?
                                // Query Reservations Bus Transfer

                                $sql_bus = "SELECT * ";
                                $sql_bus .= "FROM reservation_bustransfer_items ";
                                $sql_bus .= "WHERE reservations_id = '$row[res_id]' ";
                                //$sql_bus .= "AND rct_travel_date = '$_POST[from_date]' ";
                                $sql_bus .= "ORDER BY rct_id ASC ";

                                $result_bus = mysql_query($sql_bus);
                                while ($row_bus = mysql_fetch_array($result_bus)) {

                                    $pax = "";
                                    $pax = $row_bus[rct_adult_num] + $row_bus[rct_child_num];

                                    $product_name = get_value(cartransfers, ct_id, ct_name, $row_bus[bustransfers_id]);

                                    $ac_andaman_adult = get_value(cartransfers, ct_id, ac_andaman_adult, $row_bus[bustransfers_id]);
                                    $ac_andaman_child = get_value(cartransfers, ct_id, ac_andaman_child, $row_bus[bustransfers_id]);
                                    $ac_dotcom_adult = get_value(cartransfers, ct_id, ac_dotcom_adult, $row_bus[bustransfers_id]);
                                    $ac_dotcom_child = get_value(cartransfers, ct_id, ac_dotcom_child, $row_bus[bustransfers_id]);
                                    $ac_btoc_adult = get_value(cartransfers, ct_id, ac_btoc_adult, $row_bus[bustransfers_id]);
                                    $ac_btoc_child = get_value(cartransfers, ct_id, ac_btoc_child, $row_bus[bustransfers_id]);

                                    $sql_vo = "SELECT * ";
                                    $sql_vo .= "FROM voucher ";
                                    $sql_vo .= "WHERE vo_res_id = '$row[res_id_str]' ";
                                    $sql_vo .= "AND vo_item_id = '$product_name' ";
                                    $sql_vo .= "AND vo_status = '2' ";
                                    $sql_vo .= "ORDER BY vo_id ASC ";

                                    $result_vo = mysql_query($sql_vo);
                                    $row_vo = mysql_fetch_array($result_vo);

                                    if ($agentfor_id == 1) {
                                        $code_adult = $ac_andaman_adult;
                                        $code_child = $ac_andaman_child;

                                    } else if ($agentfor_id == 2) {
                                        $code_adult = $ac_dotcom_adult;
                                        $code_child = $ac_dotcom_child;

                                    } else {
                                        $code_adult = $ac_btoc_adult;
                                        $code_child = $ac_btoc_child;

                                    }

                                    $price_adult = @($row_bus[rct_adult_prices] / $row_bus[rct_adult_num]);
                                    $price_child = @($row_bus[rct_child_prices] / $row_bus[rct_child_num]);
                                    ?>

                                    <!--<tr>
                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?= $_POST[from_date] ?></td>
                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?= "00" . $row[confirm_pay_id]; ?></td>
                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?= $comfirm_payment ?></td>
                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?= $pax ?></td>
                        <td class="txt_bold_gray" align="right" bgcolor="#F0F0F0"><?= number_format($row_bus[rct_prices], 2) ?></td>
                        
                    </tr>-->

                                    <? $total_qty = $total_qty + $pax;
                                    $total_price = $total_price + $row_bus[rct_prices];
                                } // END while($row_bus = mysql_fetch_array($result_bus)){
                                ?>


                                <?
                                // Query Reservations Private Land Transfer

                                $sql_private = "SELECT * ";
                                $sql_private .= "FROM reservation_privatelandtransfer_items ";
                                $sql_private .= "WHERE reservations_id = '$row[res_id]' ";
                                //$sql_private .= "AND rplt_travel_date = '$_POST[from_date]' ";
                                $sql_private .= "ORDER BY rplt_id ASC ";

                                $result_private = mysql_query($sql_private);
                                while ($row_private = mysql_fetch_array($result_private)) {

                                    $pax = "";
                                    $pax = $row_private[rplt_adult_num] + $row_private[rplt_child_num];

                                    $product_name = get_value(privatelandtransfers, plt_id, plt_name, $row_private[privatelandtransfers_id]);

                                    $ac_andaman_adult = get_value(privatelandtransfers, plt_id, ac_andaman_adult, $row_private[privatelandtransfers_id]);
                                    $ac_andaman_child = get_value(privatelandtransfers, plt_id, ac_andaman_child, $row_private[privatelandtransfers_id]);
                                    $ac_dotcom_adult = get_value(privatelandtransfers, plt_id, ac_dotcom_adult, $row_private[privatelandtransfers_id]);
                                    $ac_dotcom_child = get_value(privatelandtransfers, plt_id, ac_dotcom_child, $row_private[privatelandtransfers_id]);
                                    $ac_btoc_adult = get_value(privatelandtransfers, plt_id, ac_btoc_adult, $row_private[privatelandtransfers_id]);
                                    $ac_btoc_child = get_value(privatelandtransfers, plt_id, ac_btoc_child, $row_private[privatelandtransfers_id]);

                                    $sql_vo = "SELECT * ";
                                    $sql_vo .= "FROM voucher ";
                                    $sql_vo .= "WHERE vo_res_id = '$row[res_id_str]' ";
                                    $sql_vo .= "AND vo_item_id = '$product_name' ";
                                    $sql_vo .= "AND vo_status = '2' ";
                                    $sql_vo .= "ORDER BY vo_id ASC ";

                                    $result_vo = mysql_query($sql_vo);
                                    $row_vo = mysql_fetch_array($result_vo);

                                    if ($agentfor_id == 1) {
                                        $code_adult = $ac_andaman_adult;
                                        $code_child = $ac_andaman_child;

                                    } else if ($agentfor_id == 2) {
                                        $code_adult = $ac_dotcom_adult;
                                        $code_child = $ac_dotcom_child;

                                    } else {
                                        $code_adult = $ac_btoc_adult;
                                        $code_child = $ac_btoc_child;

                                    }

                                    $price_adult = @($row_private[rplt_adult_prices] / $row_private[rplt_adult_num]);
                                    $price_child = @($row_private[rplt_child_prices] / $row_private[rplt_child_num]);
                                    ?>

                                    <!--<tr>
                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?= $_POST[from_date] ?></td>
                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?= "00" . $row[confirm_pay_id]; ?></td>
                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?= $comfirm_payment ?></td>
                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?= $pax ?></td>
                        <td class="txt_bold_gray" align="right" bgcolor="#F0F0F0"><?= number_format($row_private[rplt_prices], 2) ?></td>
                        
                    </tr>-->

                                    <? $total_qty = $total_qty + $pax;
                                    $total_price = $total_price + $row_private[rplt_prices];
                                } // END while($row_private = mysql_fetch_array($result_private)){
                                ?>


                                <?
                                // Query Reservations Hotel

                                $sql_hotel = "SELECT * ";
                                $sql_hotel .= "FROM reservation_hotel_items ";
                                $sql_hotel .= "WHERE reservations_id = '$row[res_id]' ";
                                //$sql_hotel .= "AND rht_check_in = '$_POST[from_date]' ";
                                $sql_hotel .= "ORDER BY rht_id ASC ";

                                $result_hotel = mysql_query($sql_hotel);
                                while ($row_hotel = mysql_fetch_array($result_hotel)) {

                                    $pax = "";
                                    $pax2 = "";

                                    $pax = $row_hotel[rht_room_num] * 1;
                                    $pax2 = $row_hotel[rht_adult_num] + $row_hotel[rht_child_num];

                                    $product_name = get_value(hotels, hot_id, hot_name, $row_hotel[hotels_id]);

                                    $ac_andaman_adult = get_value(hotels, hot_id, ac_andaman_adult, $row_hotel[hotels_id]);
                                    $ac_andaman_child = get_value(hotels, hot_id, ac_andaman_child, $row_hotel[hotels_id]);
                                    $ac_dotcom_adult = get_value(hotels, hot_id, ac_dotcom_adult, $row_hotel[hotels_id]);
                                    $ac_dotcom_child = get_value(hotels, hot_id, ac_dotcom_child, $row_hotel[hotels_id]);
                                    $ac_btoc_adult = get_value(hotels, hot_id, ac_btoc_adult, $row_hotel[hotels_id]);
                                    $ac_btoc_child = get_value(hotels, hot_id, ac_btoc_child, $row_hotel[hotels_id]);

                                    $sql_vo = "SELECT * ";
                                    $sql_vo .= "FROM voucher ";
                                    $sql_vo .= "WHERE vo_res_id = '$row[res_id_str]' ";
                                    $sql_vo .= "AND vo_item_id = '$product_name' ";
                                    $sql_vo .= "AND vo_status = '2' ";
                                    $sql_vo .= "ORDER BY vo_id ASC ";

                                    $result_vo = mysql_query($sql_vo);
                                    $row_vo = mysql_fetch_array($result_vo);

                                    if ($agentfor_id == 1) {
                                        $code_adult = $ac_andaman_adult;
                                        $code_child = $ac_andaman_child;

                                    } else if ($agentfor_id == 2) {
                                        $code_adult = $ac_dotcom_adult;
                                        $code_child = $ac_dotcom_child;

                                    } else {
                                        $code_adult = $ac_btoc_adult;
                                        $code_child = $ac_btoc_child;

                                    }

                                    //$price_adult = @($row_hotel[rht_adult_prices] / $row_hotel[rht_adult_num]);
                                    //$price_child = @($row_hotel[rht_child_prices] / $row_hotel[rht_child_num]);
                                    $price_adult = 0;
                                    $price_child = 0;
                                    ?>

                                    <!--<tr>
                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?= $_POST[from_date] ?></td>
                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?= "00" . $row[confirm_pay_id]; ?></td>
                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?= $comfirm_payment ?></td>
                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?= $pax2 ?></td>
                        <td class="txt_bold_gray" align="right" bgcolor="#F0F0F0"><?= number_format($row_hotel[rht_prices], 2) ?></td>
                        
                    </tr>-->

                                    <? $total_qty = $total_qty + $pax2;
                                    $total_price = $total_price + $row_hotel[rht_prices];
                                } // END while($row_hotel = mysql_fetch_array($result_hotel)){
                                ?>


                                <?
                                // Query Reservations Train Transfer

                                $sql_train = "SELECT * ";
                                $sql_train .= "FROM reservation_traintransfer_items ";
                                $sql_train .= "WHERE reservations_id = '$row[res_id]' ";
                                //$sql_train .= "AND rbt_travel_date = '$_POST[from_date]' ";
                                $sql_train .= "ORDER BY rrt_id ASC ";

                                $result_train = mysql_query($sql_train);
                                while ($row_train = mysql_fetch_array($result_train)) {

                                    $pax = "";
                                    $pax = $row_train[rrt_adult_num] + $row_train[rrt_child_num];

                                    $product_name = get_value(traintransfers, train_id, train_name, $row_train[traintransfers_id]);

                                    $ac_andaman_adult = get_value(traintransfers, train_id, ac_andaman_adult, $row_train[traintransfers_id]);
                                    $ac_andaman_child = get_value(traintransfers, train_id, ac_andaman_child, $row_train[traintransfers_id]);
                                    $ac_dotcom_adult = get_value(traintransfers, train_id, ac_dotcom_adult, $row_train[traintransfers_id]);
                                    $ac_dotcom_child = get_value(traintransfers, train_id, ac_dotcom_child, $row_train[traintransfers_id]);
                                    $ac_btoc_adult = get_value(traintransfers, train_id, ac_btoc_adult, $row_train[traintransfers_id]);
                                    $ac_btoc_child = get_value(traintransfers, train_id, ac_btoc_child, $row_train[traintransfers_id]);

                                    $sql_vo = "SELECT * ";
                                    $sql_vo .= "FROM voucher ";
                                    $sql_vo .= "WHERE vo_res_id = '$row[res_id_str]' ";
                                    $sql_vo .= "AND vo_item_id = '$product_name' ";
                                    $sql_vo .= "AND vo_status = '2' ";
                                    $sql_vo .= "ORDER BY vo_id ASC ";

                                    $result_vo = mysql_query($sql_vo);
                                    $row_vo = mysql_fetch_array($result_vo);

                                    if ($agentfor_id == 1) {
                                        $code_adult = $ac_andaman_adult;
                                        $code_child = $ac_andaman_child;

                                    } else if ($agentfor_id == 2) {
                                        $code_adult = $ac_dotcom_adult;
                                        $code_child = $ac_dotcom_child;

                                    } else {
                                        $code_adult = $ac_btoc_adult;
                                        $code_child = $ac_btoc_child;

                                    }

                                    $price_adult = @($row_train[rrt_adult_prices] / $row_train[rrt_adult_num]);
                                    $price_child = @($row_train[rrt_child_prices] / $row_train[rrt_child_num]);
                                    ?>

                                    <!--<tr>
                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?= $_POST[from_date] ?></td>
                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?= "00" . $row[confirm_pay_id]; ?></td>
                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?= $comfirm_payment ?></td>
                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?= $pax ?></td>
                        <td class="txt_bold_gray" align="right" bgcolor="#F0F0F0"><?= number_format($row_train[rrt_prices], 2) ?></td>
                        
                    </tr>-->

                                    <? $total_qty = $total_qty + $pax;
                                    $total_price = $total_price + $row_train[rrt_prices];
                                } // END while($row_train = mysql_fetch_array($result_train)){
                                ?>


                                <?
                                // Check Pay id
                                if ($row[confirm_pay_id] == 1) {
                                    $total_pay_qty1 = $total_qty;
                                    $total_pay_pr1 = $total_price;

                                } else if ($row[confirm_pay_id] == 2) {
                                    $total_pay_qty2 = $total_qty;
                                    $total_pay_pr2 = $total_price;

                                } else if ($row[confirm_pay_id] == 3) {
                                    $total_pay_qty3 = $total_qty;
                                    $total_pay_pr3 = $total_price;

                                } else {
                                    $total_pay_qty4 = $total_qty;
                                    $total_pay_pr4 = $total_price;

                                }
                                // END Check Pay id


                            } // END while($row = mysql_fetch_array($result)){	?>





                            <? // Query Reservations Combo Product

                            $sql_combo = "SELECT * ";
                            $sql_combo .= "FROM reservation_packages ";
                            $sql_combo .= "WHERE confirm_date = '$cddate' ";
                            $sql_combo .= "AND bookingstatus_id = '3' ";
                            $sql_combo .= "AND rpa_v_status = '1' ";
                            $sql_combo .= "ORDER BY rpa_id ASC ";

                            //echo $sql_combo;

                            $result_combo = mysql_query($sql_combo);
                            while ($row_combo = mysql_fetch_array($result_combo)) {

                                // Get Value

                                $total_qty_cb = 0;
                                $total_price_cb = 0;

                                $code_agent = get_value(agents, ag_id, ag_ref, $row_combo[agents_id]);
                                $agent_name = get_value(agents, ag_id, ag_name, $row_combo[agents_id]);
                                $agentfor_id = get_value(agents, ag_id, agentfor_id, $row_combo[agents_id]);
                                $agentfor_name = get_value(agent_for, a_id, a_name, $agentfor_id);
                                $lis_name = get_value(lis_titlename, lis_id, lis_name, $row_combo[titlename_id]);
                                $comfirm_payment = get_value(lis_comfirm_payment, lis_id, lis_name, $row_combo[confirm_pay_id]);

                                //$product_name = get_value(boattransfers,bot_id,bot_name,$row_boat[boattransfers_id]);

                                $sql_vo = "SELECT * ";
                                $sql_vo .= "FROM voucher ";
                                $sql_vo .= "WHERE vo_res_id = '$row_combo[rpa_id_str]' ";
                                //$sql_vo .= "AND vo_item_id = '$product_name' ";
                                $sql_vo .= "AND vo_status = '2' ";
                                $sql_vo .= "ORDER BY vo_id ASC ";

                                $result_vo = mysql_query($sql_vo);
                                $row_vo = mysql_fetch_array($result_vo);

                                // Query Reservations Combo Product items

                                $sql_product = "SELECT * ";
                                $sql_product .= "FROM reservationpackage_item ";
                                $sql_product .= "WHERE reservationpackages_id = '$row_combo[rpa_id]' ";
                                $sql_product .= "ORDER BY rpt_id ASC ";

                                //echo $sql_product;

                                $result_product = mysql_query($sql_product);
                                while ($row_product = mysql_fetch_array($result_product)) {

                                    $pax = "";
                                    $pax = $row_product[rpt_adult_num] + $row_product[rpt_child_num];

                                    $ac_andaman_adult = get_value(packages, pac_id, ac_andaman_adult, $row_product[packages_id]);
                                    $ac_andaman_child = get_value(packages, pac_id, ac_andaman_child, $row_product[packages_id]);
                                    $ac_dotcom_adult = get_value(packages, pac_id, ac_dotcom_adult, $row_product[packages_id]);
                                    $ac_dotcom_child = get_value(packages, pac_id, ac_dotcom_child, $row_product[packages_id]);
                                    $ac_btoc_adult = get_value(packages, pac_id, ac_btoc_adult, $row_product[packages_id]);
                                    $ac_btoc_child = get_value(packages, pac_id, ac_btoc_child, $row_product[packages_id]);

                                    if ($agentfor_id == 1) {
                                        $code_adult = $ac_andaman_adult;
                                        $code_child = $ac_andaman_child;

                                    } else if ($agentfor_id == 2) {
                                        $code_adult = $ac_dotcom_adult;
                                        $code_child = $ac_dotcom_child;

                                    } else {
                                        $code_adult = $ac_btoc_adult;
                                        $code_child = $ac_btoc_child;

                                    }

                                    $price_adult = @($row_product[rpt_adult_prices] / $row_product[rpt_adult_num]);
                                    $price_child = @($row_product[rpt_child_prices] / $row_product[rpt_child_num]);

                                    $rpt_item_travel_date = explode("~", $row_product[rpt_item_travel_date_arr]);
                                    //$rpt_item_producttype_id = explode("~",$row_product[rpt_item_producttype_id_arr]);
                                    //$rpt_item_id = explode("~",$row_product[rpt_item_id_arr]);

                                    $count_item = "";
                                    $count_item = count($rpt_item_travel_date);
                                    $count_item = $count_item - 1;

                                    $ar_date = 0;

                                    /*for($i=1;$i<$count_item;$i++){

                                        if($rpt_item_travel_date[$i] == $_POST[from_date]){

                                            $ar_date = 1;

                                        }else{

                                            if($ar_date != 1){ $ar_date = 0; }else{ $ar_date = 1; }

                                        } // END if($rpt_item_travel_date[$i] == $_POST[from_date]){

                                    } // END for($i=1;$i<count($rpt_item_travel_date);$i++){*/

                                    //if($ar_date == 1){
                                    ?>

                                    <!--<tr>
                            <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?= $_POST[from_date] ?></td>
                            <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?= "00" . $row_combo[confirm_pay_id]; ?></td>
                            <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?= $comfirm_payment ?></td>
                            <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?= $pax ?></td>
                            <td class="txt_bold_gray" align="right" bgcolor="#F0F0F0"><?= number_format($row_product[rpt_prices], 2) ?></td>
                            
                        </tr>-->

                                    <?
                                    //} // END if($ar_date == 1){

                                    $total_qty_cb = $total_qty_cb + $pax;
                                    $total_price_cb = $total_price_cb + $row_product[rpt_prices];
                                } // END while($row_product = mysql_fetch_array($result_product)){


                                // Check Pay id
                                if ($row_combo[confirm_pay_id] == 1) {
                                    $total_pay_qty1_cb = $total_qty_cb;
                                    $total_pay_pr1_cb = $total_price_cb;

                                } else if ($row_combo[confirm_pay_id] == 2) {
                                    $total_pay_qty2_cb = $total_qty_cb;
                                    $total_pay_pr2_cb = $total_price_cb;

                                } else if ($row_combo[confirm_pay_id] == 3) {
                                    $total_pay_qty3_cb = $total_qty_cb;
                                    $total_pay_pr3_cb = $total_price_cb;

                                } else {
                                    $total_pay_qty4_cb = $total_qty_cb;
                                    $total_pay_pr4_cb = $total_price_cb;

                                }
                                // END Check Pay id


                            } // END while($row_combo = mysql_fetch_array($result_combo)){	?>


                            <!--<tr bgcolor="#CCCCCC">
                                <td colspan="5" class="txt_bold_gray" align="center">&nbsp;</td>
                            </tr>-->

                            <!-- Total -->
                            <? $sql_pay = "SELECT * ";
                            $sql_pay .= "FROM lis_comfirm_payment ";
                            $sql_pay .= "WHERE lis_id > 0 ";
                            $sql_pay .= "ORDER BY lis_id ASC ";

                            //echo $sql_product;

                            $result_pay = mysql_query($sql_pay);
                            while ($row_pay = mysql_fetch_array($result_pay)) { ?>
                                <tr>
                                    <td class="txt_bold_gray" align="center"
                                        bgcolor="#F0F0F0"><?= $str_date_show ?></td>
                                    <td class="txt_bold_gray" align="center"
                                        bgcolor="#F0F0F0"><?= "P00" . $row_pay[lis_id]; ?></td>
                                    <td class="txt_bold_gray" align="center"
                                        bgcolor="#F0F0F0"><?= $row_pay[lis_name]; ?></td>

                                    <?
                                    //$show_date = date("d/m/Y",strtotime($row[confirm_date]));
                                    //$show_price = number_format($row_boat[rbt_prices], 0);
                                    $show_pay = "P00" . $row_pay[lis_id];

                                    fwrite($objWrite, "$str_date_show;$show_pay;$row_pay[lis_name];");
                                    ?>

                                    <? if ($row_pay[lis_id] == 1) {
                                        $total_pay_qty1_num = $total_pay_qty1 + $total_pay_qty1_cb;
                                        $total_pay_pr1_num = $total_pay_pr1 + $total_pay_pr1_cb;

                                        ?>
                                        <td class="txt_bold_gray" align="center"
                                            bgcolor="#F0F0F0"><?= $total_pay_qty1_num ?></td>
                                        <td class="txt_bold_gray" align="right"
                                            bgcolor="#F0F0F0"><?= number_format($total_pay_pr1_num, 2) ?></td>

                                        <?
                                        //$show_price = number_format($total_pay_pr1_num, 0);
                                        $show_price = floor($total_pay_pr1_num);
                                        fwrite($objWrite, "$total_pay_qty1_num;$show_price; \r\n");
                                        ?>

                                    <? } else if ($row_pay[lis_id] == 2) {
                                        $total_pay_qty2_num = $total_pay_qty2 + $total_pay_qty2_cb;
                                        $total_pay_pr2_num = $total_pay_pr2 + $total_pay_pr2_cb;

                                        ?>
                                        <td class="txt_bold_gray" align="center"
                                            bgcolor="#F0F0F0"><?= $total_pay_qty2_num ?></td>
                                        <td class="txt_bold_gray" align="right"
                                            bgcolor="#F0F0F0"><?= number_format($total_pay_pr2_num, 2) ?></td>

                                        <?
                                        //$show_price = number_format($total_pay_pr2_num, 0);
                                        $show_price = floor($total_pay_pr2_num);
                                        fwrite($objWrite, "$total_pay_qty2_num;$show_price; \r\n");
                                        ?>

                                    <? } else if ($row_pay[lis_id] == 3) {
                                        $total_pay_qty3_num = $total_pay_qty3 + $total_pay_qty3_cb;
                                        $total_pay_pr3_num = $total_pay_pr3 + $total_pay_pr3_cb;

                                        ?>
                                        <td class="txt_bold_gray" align="center"
                                            bgcolor="#F0F0F0"><?= $total_pay_qty3_num ?></td>
                                        <td class="txt_bold_gray" align="right"
                                            bgcolor="#F0F0F0"><?= number_format($total_pay_pr3_num, 2) ?></td>

                                        <?
                                        //$show_price = number_format($total_pay_pr3_num, 0);
                                        $show_price = floor($total_pay_pr3_num);
                                        fwrite($objWrite, "$total_pay_qty3_num;$show_price; \r\n");
                                        ?>

                                    <? } else {
                                        $total_pay_qty4_num = $total_pay_qty4 + $total_pay_qty4_cb;
                                        $total_pay_pr4_num = $total_pay_pr4 + $total_pay_pr4_cb;

                                        ?>
                                        <td class="txt_bold_gray" align="center"
                                            bgcolor="#F0F0F0"><?= $total_pay_qty4_num ?></td>
                                        <td class="txt_bold_gray" align="right"
                                            bgcolor="#F0F0F0"><?= number_format($total_pay_pr4_num, 2) ?></td>

                                        <?
                                        //$show_price = number_format($total_pay_pr4_num, 0);
                                        $show_price = floor($total_pay_pr4_num);
                                        fwrite($objWrite, "$total_pay_qty4_num;$show_price; \r\n");
                                        ?>

                                    <? } ?>

                                </tr>
                            <? } // END while($row_pay = mysql_fetch_array($result_pay)){ ?>
                            <!-- END Total -->

                            <?
                            fclose($objWrite);
                            echo "<meta http-equiv=\"refresh\" content=\"0; url = '../textfile/$fileName'\" >";
                            echo '<script>alert("Download this file(Right click, Save target as)")</script> ';
                            ?>

                        </table>

                        <!---- Search Box ---->
                    </td>
                    <td align="right" valign="bottom"></td>
                </tr>
            </table>

            <!---- Listing Body ---->
        </td>
    </tr>
</table>

</body>
</html>