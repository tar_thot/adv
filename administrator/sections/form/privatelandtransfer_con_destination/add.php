<?php
// Convert Variable Array To Variable

while (list($xVarName, $xVarvalue) = each($_GET)) {
    ${$xVarName} = $xVarvalue;
}


while (list($xVarName, $xVarvalue) = each($_POST)) {
    ${$xVarName} = $xVarvalue;
}

while (list($xVarName, $xVarvalue) = each($_FILES)) {
    ${$xVarName . "_name"} = $xVarvalue['name'];
    ${$xVarName . "_type"} = $xVarvalue['type'];
    ${$xVarName . "_size"} = $xVarvalue['size'];
    ${$xVarName . "_error"} = $xVarvalue['error'];
    ${$xVarName} = $xVarvalue['tmp_name'];
}
//������� 21/07/58======================================
if ($id) {
    $page_title = "Update Private Land Transfer Destination";
    $sql = "select * from privatelandtransfer_con_destination where con_id = $id";
    $result = mysql_query($sql);
    $row = mysql_fetch_array($result);


} else {
    $page_title = "Create New Private Land Transfer Destination";
}

?>

<script language="JavaScript" type="text/JavaScript">
    <!--
    function MM_findObj(n, d) { //v4.01
        var p, i, x;
        if (!d) d = document;
        if ((p = n.indexOf("?")) > 0 && parent.frames.length) {
            d = parent.frames[n.substring(p + 1)].document;
            n = n.substring(0, p);
        }
        if (!(x = d[n]) && d.all) x = d.all[n];
        for (i = 0; !x && i < d.forms.length; i++) x = d.forms[i][n];
        for (i = 0; !x && d.layers && i < d.layers.length; i++) x = MM_findObj(n, d.layers[i].document);
        if (!x && d.getElementById) x = d.getElementById(n);
        return x;
    }
    function MM_validateForm() { //v4.0
        var i, p, q, nm, test, num, min, max, errors = '', args = MM_validateForm.arguments;
        for (i = 0; i < (args.length - 2); i += 3) {
            test = args[i + 2];
            val = MM_findObj(args[i]);
            if (val) {
                nm = val.name;
                if ((val = val.value) != "") {
                    if (test.indexOf('isEmail') != -1) {
                        p = val.indexOf('@');
                        if (p < 1 || p == (val.length - 1)) errors += '- E-mail must contain an e-mail address.\n';
                    } else if (test != 'R') {
                        num = parseFloat(val);
                        if (isNaN(val)) {
                            if (nm == 'ag_balance_credit') nm = 'Credit Balance';
                            if (nm == 'ag_min_credit') nm = 'Minimum of Credit available';
                            errors += '- ' + nm + ' must contain a number.\n';
                        }
                        if (test.indexOf('inRange') != -1) {
                            p = test.indexOf(':');
                            min = test.substring(8, p);
                            max = test.substring(p + 1);
                            if (num < min || max < num) errors += '- ' + nm + ' must contain a number between ' + min + ' and ' + max + '.\n';
                        }
                    }
                } else if (test.charAt(0) == 'R') {
                    if (nm == 'con_name') nm = 'Name';
                    if (nm == 'ag_name') nm = 'Name';
                    if (nm == 'ag_email') nm = 'Email';

                    errors += '- ' + nm + ' is required.\n';
                }
            }
        }
        if (errors) alert('The following error(s) occurred:\n' + errors);
        document.MM_returnValue = (errors == '');
    }
    //-->
</script>

<script language="JavaScript" type="text/JavaScript">
    function checkCountryFroShowProvince() {


        //alert('country_id : ' + $("#country_id").val());
        $("#td_province_id").prepend("<img src='../admin_ajax/images/icon/ajax-loader.gif' /> ");

        $.post("../admin_ajax/ajx_privatelandtransfer_provincelist.php", {
                data1: $("#country_id").val(),
                data2: $("#province_id").val()
            },
            function (result) {
                $("#td_province_id").html(result);
                checkProvinceFroShowArea();
            }
        );


    }

    function checkProvinceFroShowArea() {
        //alert('province_id : ' + $("#province_id").val());
        $("#td_area_id").prepend("<img src='../admin_ajax/images/icon/ajax-loader.gif' /> ");

        $.post("../admin_ajax/ajx_privatelandtransfer_arealist.php", {
                data1: $("#province_id").val(),
                data2: $("#area_id").val()
            },
            function (result) {
                $("#td_area_id").html(result);
            }
        );
    }


</script>

<script language="JavaScript" type="text/JavaScript">
    $(document).ready(function () {
        checkCountryFroShowProvince();
        checkProvinceFroShowArea();

    });
</script>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td class="txt_big_gray"><?= $page_title ?></td>
    </tr>
</table>
<form action="process.php?mode=privatelandtransfer_con_destination/save&id=<?= $id ?>" onsubmit="MM_validateForm(
'con_name','','R'
);return document.MM_returnValue" name="form" id="form1" method="post" enctype="multipart/form-data">
    <input type="hidden" name="id" id="id" value="<?= $id ?>"/>
    <table width="100%" border="0" cellspacing="0" cellpadding="3">
        <tr>
            <td align="right" class="txt_bold_gray" width="250"></td>
            <td width="1126" align="right"><a href="./?mode=privatelandtransfer_con_destination/index"
                                              style="background-color:#ffffff; color:#000000"><<< Back</a></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray">Country :</td>
            <td><select name="country_id" id="country_id" style="width:200px"
                        onchange="checkCountryFroShowProvince();">
                    <option value="">-- Select --</option>
                    <?php listbox('privatelandtransfer_con_country', 'con_id', 'con_name', $row['country_id'], 'N'); ?>
                </select></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray">Province :</td>
            <td id="td_province_id"><select name="province_id" id="province_id" style="width:200px"
                                            onchange="checkProvinceFroShowArea();">
                    <option value="">-- Select --</option>
                    <?php listbox('privatelandtransfer_con_province', 'con_id', 'con_name', $row['province_id'], 'N'); ?>
                </select></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray">Area :</td>
            <td id="td_area_id"><select name="area_id" id="area_id" style="width:200px">
                    <option value="">-- Select --</option>
                    <?php listbox('privatelandtransfer_con_area', 'con_id', 'con_name', $row['area_id'], 'N'); ?>
                </select></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="250">Name :</td>
            <td width="1126"><input type="text" name="con_name" value="<?= $row['con_name'] ?>" size="50"><span
                    class="remark"> * </span></td>
        </tr>


        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td><input type="submit" name="submit" value="SUBMIT" style="width:75px;"><?php
                if ($id) echo " <input type=\"submit\" name=\"del\" value=\"DELETE\" style=\"width:75px;\" onClick=\"return confirm('Do you want to delete this record ? ')\">";
                else echo " <input type=\"reset\" name=\"Reset\" value=\"RESET\" style=\"width:75px;\">";
                ?></td>
        </tr>
    </table>
</form>


<script type="text/javascript">
    checkPayType();
</script>