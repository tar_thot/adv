<?php
// Convert Variable Array To Variable

while (list($xVarName, $xVarvalue) = each($_GET)) {
    ${$xVarName} = $xVarvalue;
}


while (list($xVarName, $xVarvalue) = each($_POST)) {
    ${$xVarName} = $xVarvalue;
}

while (list($xVarName, $xVarvalue) = each($_FILES)) {
    ${$xVarName . "_name"} = $xVarvalue['name'];
    ${$xVarName . "_type"} = $xVarvalue['type'];
    ${$xVarName . "_size"} = $xVarvalue['size'];
    ${$xVarName . "_error"} = $xVarvalue['error'];
    ${$xVarName} = $xVarvalue['tmp_name'];
}
if (isset($id)) {
    $page_title = "Update Client";
    $sql = "select * from clients where cl_id = $id";
    $result = mysql_query($sql);
    $row = mysql_fetch_array($result);
} else {
    $page_title = "Create New Client";
}

?>

<script language="JavaScript" type="text/JavaScript">
    <!--
    function MM_findObj(n, d) { //v4.01
        var p, i, x;
        if (!d) d = document;
        if ((p = n.indexOf("?")) > 0 && parent.frames.length) {
            d = parent.frames[n.substring(p + 1)].document;
            n = n.substring(0, p);
        }
        if (!(x = d[n]) && d.all) x = d.all[n];
        for (i = 0; !x && i < d.forms.length; i++) x = d.forms[i][n];
        for (i = 0; !x && d.layers && i < d.layers.length; i++) x = MM_findObj(n, d.layers[i].document);
        if (!x && d.getElementById) x = d.getElementById(n);
        return x;
    }
    function MM_validateForm() { //v4.0
        var i, p, q, nm, test, num, min, max, errors = '', args = MM_validateForm.arguments;
        for (i = 0; i < (args.length - 2); i += 3) {
            test = args[i + 2];
            val = MM_findObj(args[i]);
            if (val) {
                nm = val.name;
                if ((val = val.value) != "") {
                    if (test.indexOf('isEmail') != -1) {
                        p = val.indexOf('@');
                        if (p < 1 || p == (val.length - 1)) errors += '- E-mail must contain an e-mail address.\n';
                    } else if (test != 'R') {
                        num = parseFloat(val);
                        if (isNaN(val)) {
                            if (nm == 'cl_balance_credit') nm = 'Credit Balance';
                            if (nm == 'cl_min_credit') nm = 'Minimum of Credit available';
                            errors += '- ' + nm + ' must contain a number.\n';
                        }
                        if (test.indexOf('inRange') != -1) {
                            p = test.indexOf(':');
                            min = test.substring(8, p);
                            max = test.substring(p + 1);
                            if (num < min || max < num) errors += '- ' + nm + ' must contain a number between ' + min + ' and ' + max + '.\n';
                        }
                    }
                } else if (test.charAt(0) == 'R') {
                    if (nm == 'cl_name') nm = 'First Name';
                    if (nm == 'cl_lname') nm = 'Last Name';
                    if (nm == 'cl_email') nm = 'Email';
                    if (nm == 'check_cl_email') nm = 'Email';

                    errors += '- ' + nm + ' is required.\n';
                }
            }
        }
        if (errors) alert('The following error(s) occurred:\n' + errors);
        document.MM_returnValue = (errors == '');
    }
    //-->
</script>

<script language="JavaScript" type="text/JavaScript">
    function checkCountryFroShowProvince() {


        //alert('country_id : ' + $("#country_id").val());
        $("#td_province_id").prepend("<img src='../admin_ajax/images/icon/ajax-loader.gif' /> ");

        $.post("../admin_ajax/ajx_client_provincelist.php", {
                data1: $("#country_id").val(),
                data2: $("#province_id").val()
            },
            function (result) {
                $("#td_province_id").html(result);
                checkProvinceFroShowArea();
            }
        );


    }

    function checkProvinceFroShowCity() {
        //alert('province_id : ' + $("#province_id").val());
        $("#td_city_id").prepend("<img src='../admin_ajax/images/icon/ajax-loader.gif' /> ");

        $.post("../admin_ajax/ajx_client_citylist.php", {
                data1: $("#province_id").val(),
                data2: $("#city_id").val()
            },
            function (result) {
                $("#td_city_id").html(result);
            }
        );
    }


</script>

<script language="JavaScript" type="text/JavaScript">
    $(document).ready(function () {
        checkCountryFroShowProvince();
        checkProvinceFroShowCity();

    });
</script>


<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td class="txt_big_gray"><?= $page_title ?></td>
    </tr>
</table>
<form action="process.php?mode=clients/save&id=<?= $id ?>" onsubmit="MM_validateForm(
'cl_name','','R',
'cl_lname','','R',
'cl_email','','R',
'check_cl_email','','R',
'cl_email','','isEmail'
);return document.MM_returnValue" name="form" id="form1" method="post" enctype="multipart/form-data">
    <input type="hidden" name="id" id="id" value="<?= $id ?>"/>
    <table width="100%" border="0" cellspacing="0" cellpadding="3">

        <tr>
            <td align="right" class="txt_bold_gray" width="250"></td>
            <td width="1126"><b style="font-size:18px">ข้อมูล Client</b></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="250">Client ID :</td>
            <td width="1126"><input type="text" name="cl_id_auto" value="<?php if ($row['cl_id']) {
                    echo "C" . $row['cl_id'] . "";
                } ?>" size="50" readonly="readonly" style="background-color:#EAEAEA;"></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="250">Title Name :</td>
            <td>
                <select name="titlename_id">
                    <?php listbox('lis_titlename', 'lis_id', 'lis_name', $row['titlename_id'], 'N'); ?>
                </select>
            </td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="250">First Name :</td>
            <td><input type="text" name="cl_name" value="<?= $row['cl_name'] ?>" size="50"><span
                    class="remark"> * </span></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="250">Last Name :</td>
            <td><input type="text" name="cl_lname" value="<?= $row['cl_lname'] ?>" size="50"><span
                    class="remark"> * </span></td>
        </tr>

        <script type="text/javascript">
            function checkEmailRepeatedly() {
                //alert('checkEmailRepeatedly : ' + $("#cl_email").val());
                $("#spn_cl_email").html("<img src='../admin_ajax/images/icon/ajax-loader.gif' /> ");

                $.post("../admin_ajax/ajx_email_repeatedly.php", {
                        data1: $("#cl_email").val(),
                        data3: 'forclients',
                        data2: '<?=$row[0]?>'
                    },
                    function (result) {
                        $("#spn_cl_email").html(result);
                    }
                );
            }
        </script>


        <tr>
            <td align="right" class="txt_bold_gray" width="250">Email (User Name) :</td>
            <td><input type="text" name="cl_email" id="cl_email" value="<?= $row['cl_email'] ?>" size="50"
                       onkeyup="checkEmailRepeatedly();">
                <span id="spn_cl_email" class="remark"> * <input type="hidden" name="check_cl_email"
                                                                 value="<?= $row['cl_email'] ?>"></span></td>
        </tr>


        <script type="text/javascript">
            function reSetPasswordClient() {

                var new_pass = '';

                if (new_pass = prompt('กรุณาป้อนรหัสผ่านใหม่')) {
                    $("#cl_password").val(new_pass);
                }

            }
        </script>
        <tr>
            <td align="right" class="txt_bold_gray" width="250">Password :</td>
            <td><input type="text" name="cl_password" id="cl_password" value="<?= $row['cl_password'] ?>" size="50"
                       readonly="readonly" style="background-color:#EAEAEA;">
                <?php
                if (isset($id)) {
                    echo '<input type="button" name="repass" value="เปลี่ยนรหัสผ่าน" onclick="reSetPasswordClient();" />';
                }

                ?>      </td>
        </tr>


        <tr>
            <td align="right" class="txt_bold_gray">Country :</td>
            <td><select name="country_id" id="country_id" style="width:200px" onchange="checkCountryFroShowProvince();">
                    <option value="">-- Select --</option>
                    <?php listbox('client_con_country', 'con_id', 'con_name', $row['country_id'], 'N'); ?>
                </select></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray">Province :</td>
            <td id="td_province_id"><select name="province_id" id="province_id" style="width:200px"
                                            onchange="checkProvinceFroShowCity();">
                    <option value="">-- Select --</option>
                    <?php listbox('client_con_province', 'con_id', 'con_name', $row['province_id'], 'N'); ?>
                </select></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray">City :</td>
            <td id="td_city_id"><select name="city_id" id="city_id" style="width:200px">
                    <option value="">-- Select --</option>
                    <?php listbox('client_con_city', 'con_id', 'con_name', $row['city_id'], 'N'); ?>
                </select></td>
        </tr>


        <tr>
            <td align="right" class="txt_bold_gray">Nationality :</td>
            <td><select name="cl_nationality_id" id="cl_nationality_id" style="width:200px">
                    <option value="">-- Select --</option>
                    <?php listbox('nationalities', 'country_id', 'name', $row['cl_nationality_id'], 'N'); ?>
                </select></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="250"> Client Category :</td>
            <td><select name="category_id" id="category_id" style="width:200px">
                    <option value="">-- Select --</option>
                    <?php listbox('client_con_category', 'con_id', 'con_name', $row['category_id'], 'N'); ?>
                </select>
            </td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="250"> Passport Number / ID Card :</td>
            <td><input type="text" name="cl_passport" value="<?= $row['cl_passport'] ?>" size="50"></td>
        </tr>


        <tr>
            <?php
            if ($row['cl_passportuntil_date'] > 0) {
                $cl_passportuntil_date = $row['cl_passportuntil_date'];
            } else {
                $cl_passportuntil_date = $today;
            }
            ?>
            <td align="right" class="txt_bold_gray" width="250"> Passport Until Date :</td>
            <td>
                <script>DateInput('cl_passportuntil_date', false, 'yyyy-mm-dd', '<?= $cl_passportuntil_date?>')</script>
            </td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="250" valign="top">ที่อยู่ Client 1 :</td>
            <td>
                <textarea name="cl_addr1" cols="50"><?= $row['cl_addr1'] ?></textarea>
            </td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="250" valign="top">ที่อยู่ Client 2 :</td>
            <td>
                <textarea name="cl_addr2" cols="50"><?= $row['cl_addr2'] ?></textarea>
            </td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="250"> Zip Code :</td>
            <td><input type="text" name="cl_zipcode" value="<?= $row['cl_zipcode'] ?>" size="50"></td>
        </tr>


        <tr>
            <td align="right" class="txt_bold_gray" width="250"> Phone :</td>
            <td><input type="text" name="cl_phone" value="<?= $row['cl_phone'] ?>" size="50"></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="250"> Mobile :</td>
            <td><input type="text" name="cl_mobile" value="<?= $row['cl_mobile'] ?>" size="50"></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="250"> Occupation :</td>
            <td><input type="text" name="cl_occup" value="<?= $row['cl_occup'] ?>" size="50"></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="250"> Business Phone :</td>
            <td><input type="text" name="cl_busiphone" value="<?= $row['cl_busiphone'] ?>" size="50"></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="250"> Emergency Number :</td>
            <td><input type="text" name="cl_emerg_num" value="<?= $row['cl_emerg_num'] ?>" size="50"></td>
        </tr>


        <tr>
            <td align="right" class="txt_bold_gray" width="250"> Fax :</td>
            <td><input type="text" name="cl_fax" value="<?= $row['cl_fax'] ?>" size="50"></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="250"> Age :</td>
            <td><input type="text" name="cl_age" value="<?= $row['cl_age'] ?>" size="50"></td>
        </tr>

        <tr>
            <?php
            if ($row['cl_birth_date'] > 0) {
                $cl_birth_date = $row['cl_birth_date'];
            } else {
                $cl_birth_date = $today;
            }
            ?>
            <td align="right" class="txt_bold_gray" width="250"> Birth Date :</td>
            <td>
                <script>DateInput('cl_birth_date', false, 'yyyy-mm-dd', '<?= $cl_birth_date?>')</script>
            </td>
        </tr>


        <tr>
            <?php
            if ($row['cl_create_date'] > 0) {
                $cl_create_date = $row['cl_create_date'];
            } else {
                $cl_create_date = $today;
            }
            ?>
            <td align="right" class="txt_bold_gray" width="250"> Create Date :</td>
            <td>
                <script>DateInput('cl_create_date', false, 'yyyy-mm-dd', '<?= $cl_create_date?>')</script>
            </td>
        </tr>


        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td><input type="submit" name="submit" value="SUBMIT" style="width:75px;"><?php
                if (isset($id)) echo " <input type=\"submit\" name=\"del\" value=\"DELETE\" style=\"width:75px;\" onClick=\"return confirm('Do you want to delete this record ?')\">";
                else echo " <input type=\"reset\" name=\"Reset\" value=\"RESET\" style=\"width:75px;\">";
                ?></td>
        </tr>
    </table>
</form>


