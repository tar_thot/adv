<?php
// Convert Variable Array To Variable

while (list($xVarName, $xVarvalue) = each($_GET)) {
    ${$xVarName} = $xVarvalue;
}


while (list($xVarName, $xVarvalue) = each($_POST)) {
    ${$xVarName} = $xVarvalue;
}

while (list($xVarName, $xVarvalue) = each($_FILES)) {
    ${$xVarName . "_name"} = $xVarvalue['name'];
    ${$xVarName . "_type"} = $xVarvalue['type'];
    ${$xVarName . "_size"} = $xVarvalue['size'];
    ${$xVarName . "_error"} = $xVarvalue['error'];
    ${$xVarName} = $xVarvalue['tmp_name'];
}
//เพิ่มเติม 21/07/58======================================
if (isset($id)) {
    $page_title = "Update Agent";
    $sql = "select * from agents where ag_id = $id";
    $result = mysql_query($sql);
    $row = mysql_fetch_array($result);
} else {
    $page_title = "Create New Agent";
}

?>

<script language="JavaScript" type="text/JavaScript">
    <!--
    function MM_findObj(n, d) { //v4.01
        var p, i, x;
        if (!d) d = document;
        if ((p = n.indexOf("?")) > 0 && parent.frames.length) {
            d = parent.frames[n.substring(p + 1)].document;
            n = n.substring(0, p);
        }
        if (!(x = d[n]) && d.all) x = d.all[n];
        for (i = 0; !x && i < d.forms.length; i++) x = d.forms[i][n];
        for (i = 0; !x && d.layers && i < d.layers.length; i++) x = MM_findObj(n, d.layers[i].document);
        if (!x && d.getElementById) x = d.getElementById(n);
        return x;
    }
    function MM_validateForm() { //v4.0
        var i, p, q, nm, test, num, min, max, errors = '', args = MM_validateForm.arguments;
        for (i = 0; i < (args.length - 2); i += 3) {
            test = args[i + 2];
            val = MM_findObj(args[i]);
            if (val) {
                nm = val.name;
                if ((val = val.value) != "") {
                    if (test.indexOf('isEmail') != -1) {
                        p = val.indexOf('@');
                        if (p < 1 || p == (val.length - 1)) errors += '- E-mail must contain an e-mail address.\n';
                    } else if (test != 'R') {
                        num = parseFloat(val);
                        if (isNaN(val)) {
                            if (nm == 'ag_balance_credit') nm = 'Credit Balance';
                            if (nm == 'ag_min_credit') nm = 'Minimum of Credit available';
                            errors += '- ' + nm + ' must contain a number.\n';
                        }
                        if (test.indexOf('inRange') != -1) {
                            p = test.indexOf(':');
                            min = test.substring(8, p);
                            max = test.substring(p + 1);
                            if (num < min || max < num) errors += '- ' + nm + ' must contain a number between ' + min + ' and ' + max + '.\n';
                        }
                    }
                } else if (test.charAt(0) == 'R') {
                    if (nm == 'ag_ref') nm = 'Agent Ref';
                    if (nm == 'ag_name') nm = 'Name';
                    if (nm == 'ag_email') nm = 'Email';
                    if (nm == 'check_ag_email') nm = 'Email';
                    if (nm == 'check_ag_ref') nm = 'Agent Ref';


                    errors += '- ' + nm + ' is required.\n';
                }
            }
        }
        if (errors) alert('The following error(s) occurred:\n' + errors);
        document.MM_returnValue = (errors == '');
    }
    //-->
</script>

<script language="JavaScript" type="text/JavaScript">
    function checkCountryFroShowProvince() {


        //alert('country_id : ' + $("#country_id").val());
        $("#td_province_id").prepend("<img src='../admin_ajax/images/icon/ajax-loader.gif' /> ");

        $.post("../admin_ajax/ajx_agent_provincelist.php", {
                data1: $("#country_id").val(),
                data2: $("#province_id").val()
            },
            function (result) {
                $("#td_province_id").html(result);
                checkProvinceFroShowArea();
            }
        );


    }

    function checkProvinceFroShowCity() {
        //alert('province_id : ' + $("#province_id").val());
        $("#td_city_id").prepend("<img src='../admin_ajax/images/icon/ajax-loader.gif' /> ");

        $.post("../admin_ajax/ajx_agent_citylist.php", {
                data1: $("#province_id").val(),
                data2: $("#city_id").val()
            },
            function (result) {
                $("#td_city_id").html(result);
            }
        );
    }


</script>

<script language="JavaScript" type="text/JavaScript">
    $(document).ready(function () {
        checkCountryFroShowProvince();
        checkProvinceFroShowCity();

    });
</script>


<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td class="txt_big_gray"><?= $page_title ?></td>
    </tr>
</table>
<form action="process.php?mode=agents/save&id=<?= $id ?>" onsubmit="MM_validateForm(
'ag_ref','','R',
'ag_name','','R',
'ag_balance_credit','','isNaN',
'ag_min_credit','','isNaN',
'ag_email','','R',
'check_ag_email','','R',
'check_ag_ref','','R',
'ag_email','','isEmail'
);return document.MM_returnValue" name="form" id="form1" method="post" enctype="multipart/form-data">
    <input type="hidden" name="id" id="id" value="<?= $id ?>"/>
    <table width="100%" border="0" cellspacing="0" cellpadding="3">

        <tr>
            <td align="right" class="txt_bold_gray" width="317"></td>
            <td width="1059"><b style="font-size:18px">ข้อมูล Agent</b></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="317">Agent ID :</td>
            <td width="1059"><input type="text" name="ag_id_auto" value="<?php if ($row['ag_id']) {
                    echo "A" . $row['ag_id'] . "";
                } ?>" size="50" readonly="readonly" style="background-color:#EAEAEA;"></td>
        </tr>

        <script type="text/javascript">
            function checkRefRepeatedly() {
                //alert('checkEmailRepeatedly : ' + $("#ag_email").val());
                $("#spn_ag_ref").html("<img src='../admin_ajax/images/icon/ajax-loader.gif' /> ");

                $.post("../admin_ajax/ajx_ref_repeatedly.php", {
                        data1: $("#ag_ref").val(),
                        data2: 'ag_',
                        data3: 'agents',
                        data4: '<?=$row[0]?>'
                    },
                    function (result) {
                        $("#spn_ag_ref").html(result);
                    }
                );
            }
        </script>

        <tr>
            <td align="right" class="txt_bold_gray" width="317">Agent Ref :</td>
            <td width="1059"><input type="text" name="ag_ref" id="ag_ref" value="<?= $row['ag_ref'] ?>" size="50"
                                    onkeyup="checkRefRepeatedly();">
                <span id="spn_ag_ref" class="remark"> * <input type="hidden" name="check_ag_ref"
                                                               value="<?= $row['ag_ref'] ?>"></span>
            </td>
        </tr>
        <tr>
            <td align="right" class="txt_bold_gray" width="317">Name :</td>
            <td><input type="text" name="ag_name" value="<?= $row['ag_name'] ?>" size="50"><span
                    class="remark"> * </span></td>
        </tr>

        <script type="text/javascript">
            function checkEmailRepeatedly() {
                //alert('checkEmailRepeatedly : ' + $("#ag_email").val());
                $("#spn_ag_email").html("<img src='../admin_ajax/images/icon/ajax-loader.gif' /> ");

                $.post("../admin_ajax/ajx_email_repeatedly.php", {
                        data1: $("#ag_email").val(),
                        data3: 'foragents',
                        data2: '<?=$row[0]?>'
                    },
                    function (result) {
                        $("#spn_ag_email").html(result);
                    }
                );
            }
        </script>


        <tr>
            <td align="right" class="txt_bold_gray" width="317">Email (User Name) :</td>
            <td><input type="text" name="ag_email" id="ag_email" value="<?= $row['ag_email'] ?>" size="50"
                       onkeyup="checkEmailRepeatedly();">
                <span id="spn_ag_email" class="remark"> * <input type="hidden" name="check_ag_email"
                                                                 value="<?= $row['ag_email'] ?>"></span></td>
        </tr>


        <script type="text/javascript">
            function reSetPasswordAgent() {

                var new_pass = '';

                if (new_pass = prompt('กรุณาป้อนรหัสผ่านใหม่')) {
                    $("#ag_password").val(new_pass);
                }

            }
        </script>
        <tr>
            <td align="right" class="txt_bold_gray" width="317">Password :</td>
            <td><input type="text" name="ag_password" id="ag_password" value="<?= $row['ag_password'] ?>" size="50"
                       readonly="readonly" style="background-color:#EAEAEA;">
                <?php
                if (isset($id)) {
                    echo '<input type="button" name="repass" value="เปลี่ยนรหัสผ่าน" onclick="reSetPasswordAgent();" />';
                }

                ?>

            </td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="317">Email 2 :</td>
            <td><input type="text" name="ag_email2" value="<?= $row['ag_email2'] ?>" size="50">
            </td>
        </tr>


        <tr>
            <td align="right" class="txt_bold_gray" width="317" valign="top">ที่อยู่ Agent 1 :</td>
            <td>
                <textarea name="ag_address" cols="50"><?= $row['ag_address'] ?></textarea>
            </td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="317" valign="top">ที่อยู่ Agent 2 :</td>
            <td>
                <textarea name="ag_address2" cols="50"><?= $row['ag_address2'] ?></textarea>
            </td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray">Country :</td>
            <td><select name="country_id" id="country_id" style="width:200px" onchange="checkCountryFroShowProvince();">
                    <option value="">-- Select --</option>
                    <?php listbox('agent_con_country', 'con_id', 'con_name', $row['country_id'], 'N'); ?>
                </select></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray">Province :</td>
            <td id="td_province_id"><select name="province_id" id="province_id" style="width:200px"
                                            onchange="checkProvinceFroShowCity();">
                    <option value="">-- Select --</option>
                    <?php listbox('agent_con_province', 'con_id', 'con_name', $row['province_id'], 'N'); ?>
                </select></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray">City :</td>
            <td id="td_city_id"><select name="city_id" id="city_id" style="width:200px">
                    <option value="">-- Select --</option>
                    <?php listbox('agent_con_city', 'con_id', 'con_name', $row['city_id'], 'N'); ?>
                </select></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="317"> Zip Code :</td>
            <td><input type="text" name="ag_zipcode" value="<?= $row['ag_zipcode'] ?>" size="50"></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="317">เบอร์โทรศัพท์ Agent 1 :</td>
            <td><input type="text" name="ag_tel" value="<?= $row['ag_tel'] ?>" size="50"></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="317">เบอร์โทรศัพท์ Agent 2 :</td>
            <td><input type="text" name="ag_tel2" value="<?= $row['ag_tel2'] ?>" size="50"></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="317">เบอร์โทรศัพท์ Agent 3 :</td>
            <td><input type="text" name="ag_tel3" value="<?= $row['ag_tel3'] ?>" size="50"></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="317">Fax :</td>
            <td><input type="text" name="ag_fax" value="<?= $row['ag_fax'] ?>" size="50"></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="317"> License Number :</td>
            <td><input type="text" name="ag_licen_num" value="<?= $row['ag_licen_num'] ?>" size="50"></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="317"> License Upload 1 :</td>
            <td><?php $img_tmp = $row['file1'];
                if ($img_tmp) { ?>
                    <a href="../files_upload/agents/<?= $img_tmp ?>" target="_blank">Download file 1</a><br>
                    <label><input type="checkbox" name="del_file1" value="<?= $img_tmp ?>"><span class="remark">Delete File</span></label>
                    <br>
                <?php } ?>
                <input name="file1" id="file1" type="file"><input name="tmp_file1" type="hidden"
                                                                  value="<?= $img_tmp ?>">
            </td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="317"> License Upload 2 :</td>
            <td><?php $img_tmp = $row['file2'];
                if ($img_tmp) { ?>
                    <a href="../files_upload/agents/<?= $img_tmp ?>" target="_blank">Download file 2</a><br>
                    <label><input type="checkbox" name="del_file2" value="<?= $img_tmp ?>"><span class="remark">Delete File</span></label>
                    <br>
                <?php } ?>
                <input name="file2" id="file2" type="file"><input name="tmp_file2" type="hidden"
                                                                  value="<?= $img_tmp ?>">
            </td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="317"> License Upload 3 :</td>
            <td><?php $img_tmp = $row['file3'];
                if ($img_tmp) { ?>
                    <a href="../files_upload/agents/<?= $img_tmp ?>" target="_blank">Download file 3</a><br>
                    <label><input type="checkbox" name="del_file3" value="<?= $img_tmp ?>"><span class="remark">Delete File</span></label>
                    <br>
                <?php } ?>
                <input name="file3" id="file3" type="file"><input name="tmp_file3" type="hidden"
                                                                  value="<?= $img_tmp ?>">
            </td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="317"> License Upload 4 :</td>
            <td><?php $img_tmp = $row['file4'];
                if ($img_tmp) { ?>
                    <a href="../files_upload/agents/<?= $img_tmp ?>" target="_blank">Download file 4</a><br>
                    <label><input type="checkbox" name="del_file4" value="<?= $img_tmp ?>"><span class="remark">Delete File</span></label>
                    <br>
                <?php } ?>
                <input name="file4" id="file4" type="file"><input name="tmp_file4" type="hidden"
                                                                  value="<?= $img_tmp ?>">
            </td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="317"> License Upload 5 :</td>
            <td><?php $img_tmp = $row['file5'];
                if ($img_tmp) { ?>
                    <a href="../files_upload/agents/<?= $img_tmp ?>" target="_blank">Download file 5</a><br>
                    <label><input type="checkbox" name="del_file5" value="<?= $img_tmp ?>"><span class="remark">Delete File</span></label>
                    <br>
                <?php } ?>
                <input name="file5" id="file5" type="file"><input name="tmp_file5" type="hidden"
                                                                  value="<?= $img_tmp ?>">
            </td>
        </tr>

        <tr>
            <td></td>
            <td bgcolor="#FFFFFF">
                <div
                    style="font-size:0.9em; color:#C00; padding:5px; width:300px; background-color:#FFE0DD; position:relative;">
                    - Please use file size less than 2MB.<br/>
                </div>
            </td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="317"> URL 1 :</td>
            <td><input type="text" name="ag_url1" value="<?= $row['ag_url1'] ?>" size="50"></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="317"> URL 2 :</td>
            <td><input type="text" name="ag_url2" value="<?= $row['ag_url2'] ?>" size="50"></td>
        </tr>


        <tr>
            <?php
            if ($row['ag_combirth_date'] > 0) {
                $ag_combirth_date = $row['ag_combirth_date'];
            } else {
                $ag_combirth_date = $today;
            }
            ?>
            <td align="right" class="txt_bold_gray" width="317"> Company Birth Date :</td>
            <td>
                <script>DateInput('ag_combirth_date', false, 'yyyy-mm-dd', '<?= $ag_combirth_date?>')</script>
            </td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray">Logo Photo :</td>
            <td><?php $img_tmp = $row['photo1'];
                if ($img_tmp) { ?>
                    <img src="./resizer.php?imgfile=../photo/agents/<?= $img_tmp ?>&size=200" border="0"><br>
                    <label><input type="checkbox" name="del_photo1" value="<?= $img_tmp ?>"><span class="remark">Delete Photo</span></label>
                    <br>
                <?php } ?>
                <input name="photo1" type="file"><input name="tmp_photo1" type="hidden" value="<?= $img_tmp ?>"></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray">Voucher Photo :</td>
            <td><?php $img_tmp = $row['photo2'];
                if ($img_tmp) { ?>
                    <img src="./resizer.php?imgfile=../photo/agents/<?= $img_tmp ?>&size=200" border="0"><br>
                    <label><input type="checkbox" name="del_photo2" value="<?= $img_tmp ?>"><span class="remark">Delete Photo</span></label>
                    <br>
                <?php } ?>
                <input name="photo2" type="file"><input name="tmp_photo2" type="hidden" value="<?= $img_tmp ?>"></td>
        </tr>


        <tr>
            <td></td>
            <td bgcolor="#FFFFFF">
                <div
                    style="font-size:0.9em; color:#C00; padding:5px; width:300px; background-color:#FFE0DD; position:relative;">
                    - Support JPG Format Only<br>
                    - Please use file size less than 2MB.<br/>
                </div>
            </td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="317" valign="top">Remark For Invoice :</td>
            <td>
                <textarea name="ag_remarkinvoice" cols="50"><?= $row['ag_remarkinvoice'] ?></textarea>
            </td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="317"> Invoice Show Vat :</td>
            <td><input type="checkbox" name="ag_invoice_vat" value="1" size="50" <?php if ($row['ag_invoice_vat']) {
                    echo 'checked';
                } ?> ></td>
        </tr>

        <tr>
            <?php
            if ($row['ag_contactstart_date'] > 0) {
                $ag_contactstart_date = $row['ag_contactstart_date'];
            } else {
                $ag_contactstart_date = $today;
            }
            ?>
            <td align="right" class="txt_bold_gray" width="317"> Contact Started On :</td>
            <td>
                <script>DateInput('ag_contactstart_date', false, 'yyyy-mm-dd', '<?= $ag_contactstart_date?>')</script>
            </td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray">Business :</td>
            <td><select name="businesstype_id" id="businesstype_id" style="width:200px">
                    <option value="">-- Select --</option>
                    <?php listbox('agent_con_businesstype', 'con_id', 'con_name', $row['businesstype_id'], 'N'); ?>
                </select></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="317"> Line :</td>
            <td><input type="text" name="ag_line" value="<?= $row['ag_line'] ?>" size="50"></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="317"> Skype :</td>
            <td><input type="text" name="ag_skype" value="<?= $row['ag_skype'] ?>" size="50"></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="317">คำนำหน้าชื่อ ผู้ติดต่อ :</td>
            <td><select name="ag_contact_title_id">
                    <option value="">-- Select --</option>
                    <?php listbox('nametitles', 'nt_id', 'nt_name', $row['ag_contact_title_id'], 'N'); ?>
                </select></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="317"> ชื่อ ผู้ติดต่อ :</td>
            <td><input type="text" name="ag_contact_name" value="<?= $row['ag_contact_name'] ?>" size="50"></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="317"> สกุล ผู้ติดต่อ :</td>
            <td><input type="text" name="ag_contact_lname" value="<?= $row['ag_contact_lname'] ?>" size="50"></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="317">เบอร์โทรศัพท์ ผู้ติดต่อ :</td>
            <td><input type="text" name="ag_contact_tel" value="<?= $row['ag_contact_tel'] ?>" size="50"></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="317"> Email ผู้ติดต่อ :</td>
            <td><input type="text" name="ag_contact_email" value="<?= $row['ag_contact_email'] ?>" size="50"></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="317">คำนำหน้าชื่อ ฝ่ายบัญชี :</td>
            <td><select name="ag_account_title_id">
                    <option value="">-- Select --</option>
                    <?php listbox('nametitles', 'nt_id', 'nt_name', $row['ag_account_title_id'], 'N'); ?>
                </select></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="317">ชื่อ ฝ่ายบัญชี :</td>
            <td><input type="text" name="ag_account_name" value="<?= $row['ag_account_name'] ?>" size="50"></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="317"> สกุล ฝ่ายบัญชี :</td>
            <td><input type="text" name="ag_account_lname" value="<?= $row['ag_account_lname'] ?>" size="50"></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="317">เบอร์โทรศัพท์ ฝ่ายบัญชี :</td>
            <td><input type="text" name="ag_account_tel" value="<?= $row['ag_account_tel'] ?>" size="50"></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="317"> Email ฝ่ายบัญชี :</td>
            <td><input type="text" name="ag_account_email" value="<?= $row['ag_account_email'] ?>" size="50"></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="317">คำนำหน้าชื่อ ฝ่าย Reservation :</td>
            <td><select name="ag_reser_title_id">
                    <option value="">-- Select --</option>
                    <?php listbox('nametitles', 'nt_id', 'nt_name', $row['ag_reser_title_id'], 'N'); ?>
                </select></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="317">ชื่อ ฝ่าย Reservation :</td>
            <td><input type="text" name="ag_reser_name" value="<?= $row['ag_reser_name'] ?>" size="50"></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="317"> สกุล ฝ่ายบัญชี :</td>
            <td><input type="text" name="ag_reser_lname" value="<?= $row['ag_reser_lname'] ?>" size="50"></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="317">เบอร์โทรศัพท์ ฝ่าย Reservation :</td>
            <td><input type="text" name="ag_reser_tel" value="<?= $row['ag_reser_tel'] ?>" size="50"></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="317"> Email ฝ่าย Reservation :</td>
            <td><input type="text" name="ag_reser_email" value="<?= $row['ag_reser_email'] ?>" size="50"></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="317">ระดับความสำพันธ์ :</td>
            <td><select name="agentrelation_id">
                    <option value="">-- Select --</option>
                    <?php listbox('agent_relation', 'a_id', 'a_name', $row['agentrelation_id'], 'N'); ?>
                </select></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="317" valign="top">บันทึกภายใน 1:</td>
            <td>
                <textarea name="ag_note" cols="50"><?= $row['ag_note'] ?></textarea>
            </td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="317" valign="top">บันทึกภายใน 2:</td>
            <td>
                <textarea name="ag_remark2" cols="50"><?= $row['ag_remark2'] ?></textarea>
            </td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="317">Company :</td>
            <td><select name="agentfor_id">
                    <option value="">-- Select --</option>
                    <?php listbox('agent_for', 'a_id', 'a_name', $row['agentfor_id'], 'N'); ?>
                </select></td>
        </tr>

        <tr>
            <?php
            if ($row['ag_create_date'] > 0) {
                $ag_create_date = $row['ag_create_date'];
            } else {
                $ag_create_date = $today;
            }
            ?>
            <td align="right" class="txt_bold_gray" width="317"> Create Date :</td>
            <td>
                <script>DateInput('ag_create_date', false, 'yyyy-mm-dd', '<?= $ag_create_date?>')</script>
            </td>
        </tr>

        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="317"></td>
            <td width="1059"><b style="font-size:18px">Market Segment</b></td>
        </tr>
        <tr>
            <td align="right" class="txt_bold_gray">Type Of Client :</td>
            <td><select name="agenttypeofclient_id">
                    <option value="">-- Select --</option>
                    <?php listbox('agent_typeofclient', 'a_id', 'a_name', $row['agenttypeofclient_id'], 'N'); ?>
                </select></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="317"> Channel :</td>
            <td><input type="text" name="ag_channel" value="<?= $row['ag_channel'] ?>" size="50"></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="317"> Region 1 :</td>
            <td><input type="text" name="ag_region1" value="<?= $row['ag_region1'] ?>" size="50"></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="317"> Region 2 :</td>
            <td><input type="text" name="ag_region2" value="<?= $row['ag_region2'] ?>" size="50"></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="317"> Target Country 1 :</td>
            <td><input type="text" name="ag_targetcountry1" value="<?= $row['ag_targetcountry1'] ?>" size="50"></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="317"> Target Country 2 :</td>
            <td><input type="text" name="ag_targetcountry2" value="<?= $row['ag_targetcountry2'] ?>" size="50"></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="317"> Target Country 3 :</td>
            <td><input type="text" name="ag_targetcountry3" value="<?= $row['ag_targetcountry3'] ?>" size="50"></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="317"> Seasonality 1 :</td>
            <td><input type="text" name="ag_seasonality1" value="<?= $row['ag_seasonality1'] ?>" size="50"></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="317"> Seasonality 2 :</td>
            <td><input type="text" name="ag_seasonality2" value="<?= $row['ag_seasonality2'] ?>" size="50"></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="317"> Seasonality 3 :</td>
            <td><input type="text" name="ag_seasonality3" value="<?= $row['ag_seasonality3'] ?>" size="50"></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="317"> Competitor 1 :</td>
            <td><input type="text" name="ag_competitor1" value="<?= $row['ag_competitor1'] ?>" size="50"></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="317"> Competitor 2 :</td>
            <td><input type="text" name="ag_competitor2" value="<?= $row['ag_competitor2'] ?>" size="50"></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="317"> Competitor 3 :</td>
            <td><input type="text" name="ag_competitor3" value="<?= $row['ag_competitor3'] ?>" size="50"></td>
        </tr>


        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="317"></td>
            <td width="1059"><b style="font-size:18px">เกรดของ Agent สินค้า</b></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="317">เกรดสำหรับซื้อ Tour :</td>
            <td><select name="agentgrade_id_tour">
                    <option value="">-- Select --</option>
                    <?php listbox('agent_grade', 'a_id', 'a_name', $row['agentgrade_id_tour'], 'N'); ?>
                </select></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="317">เกรดสำหรับซื้อ Activity :</td>
            <td><select name="agentgrade_id_activity">
                    <option value="">-- Select --</option>
                    <?php listbox('agent_grade', 'a_id', 'a_name', $row['agentgrade_id_activity'], 'N'); ?>
                </select></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="317">เกรดสำหรับซื้อ Hotel :</td>
            <td><select name="agentgrade_id_hotel">
                    <option value="">-- Select --</option>
                    <?php listbox('agent_grade', 'a_id', 'a_name', $row['agentgrade_id_hotel'], 'N'); ?>
                </select></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="317">เกรดสำหรับซื้อ Boat Transfer :</td>
            <td><select name="agentgrade_id_boat">
                    <option value="">-- Select --</option>
                    <?php listbox('agent_grade', 'a_id', 'a_name', $row['agentgrade_id_boat'], 'N'); ?>
                </select></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="317">เกรดสำหรับซื้อ Bus Transfer :</td>
            <td><select name="agentgrade_id_bustran">
                    <option value="">-- Select --</option>
                    <?php listbox('agent_grade', 'a_id', 'a_name', $row['agentgrade_id_bustran'], 'N'); ?>
                </select></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="317">เกรดสำหรับซื้อ Pick Up Transfer :</td>
            <td><select name="agentgrade_id_pickuptran">
                    <option value="">-- Select --</option>
                    <?php listbox('agent_grade', 'a_id', 'a_name', $row['agentgrade_id_pickuptran'], 'N'); ?>
                </select></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="317">เกรดสำหรับซื้อ Private Land Transfer :</td>
            <td><select name="agentgrade_id_privatetran">
                    <option value="">-- Select --</option>
                    <?php listbox('agent_grade', 'a_id', 'a_name', $row['agentgrade_id_privatetran'], 'N'); ?>
                </select></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="317">เกรดสำหรับซื้อ Train Transfer :</td>
            <td><select name="agentgrade_id_train">
                    <option value="">-- Select --</option>
                    <?php listbox('agent_grade', 'a_id', 'a_name', $row['agentgrade_id_train'], 'N'); ?>
                </select></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="317">เกรดสำหรับซื้อ Air Ticket :</td>
            <td><select name="agentgrade_id_air">
                    <option value="">-- Select --</option>
                    <?php listbox('agent_grade', 'a_id', 'a_name', $row['agentgrade_id_air'], 'N'); ?>
                </select></td>
        </tr>


        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="317"></td>
            <td width="1059"><b style="font-size:18px">เกรดของ Agent สำหรับ Combo Product</b></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="317">เกรดสำหรับซื้อ Combo Product :</td>
            <td><select name="agentgrade_id_package">
                    <option value="">-- Select --</option>
                    <?php listbox('agent_grade', 'a_id', 'a_name', $row['agentgrade_id_package'], 'N'); ?>
                </select></td>
        </tr>

        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="317"></td>
            <td width="1059"><b style="font-size:18px">การชำระเงินของ Agent</b></td>
        </tr>


        <script type="text/javascript">
            function checkPayType() {
                var agentpaytype_id = $("#agentpaytype_id").val();
                //alert('agentpaytype_id : ' + agentpaytype_id);
                //alert('555');
                if (agentpaytype_id == '002') {
                    //alert('Credit');
                    $("#tr_credit_detail").show();
                } else {
                    $("#tr_credit_detail").hide();
                }
            }
        </script>


        <tr>
            <td align="right" class="txt_bold_gray" width="317">ประเภทการชำระเงิน :</td>
            <td><select name="agentpaytype_id" id="agentpaytype_id" onchange="checkPayType();">
                    <option value="">-- Select --</option>
                    <?php listbox('agent_paytype', 'a_id', 'a_name', $row['agentpaytype_id'], 'N'); ?>
                </select></td>
        </tr>

        <tr id="tr_credit_detail">
            <td align="right" class="txt_bold_gray" width="317"></td>
            <td>
                <table width="450" border="0" cellspacing="0" cellpadding="3" style="border:1px solid blue;">

                    <tr>
                        <td colspan="2" align="center"><b style="font-size:18px">ข้อมูล Credit</b></td>
                    </tr>

                    <tr>
                        <td align="right" class="txt_bold_gray" width="200">ประเภท Credit :</td>
                        <td><select name="agentcredittype_id">
                                <option value="">-- Select --</option>
                                <?php listbox('agent_credittype', 'a_id', 'a_name', $row['agentcredittype_id'], 'N'); ?>
                            </select></td>
                    </tr>

                    <tr>
                        <td align="right" class="txt_bold_gray" width="250">จำนวน Credit คงเหลือ :</td>
                        <td><input type="text" name="ag_balance_credit" value="<?= $row['ag_balance_credit'] ?>"
                                   size="20"></td>
                    </tr>

                    <tr>
                        <td align="right" class="txt_bold_gray" width="250">จำนวน Credit ขั้นต่ำที่สามารถใช้ได้ :</td>
                        <td><input type="text" name="ag_min_credit" value="<?= $row['ag_min_credit'] ?>" size="20"></td>
                    </tr>

                    <tr>
                        <td align="right" class="txt_bold_gray" width="250"> Payment Term :</td>
                        <td><select name="ag_paymentterm_num">
                                <option value="">-- Select --</option>
                                <?php list_number($row['ag_paymentterm_num'], 1, 60); ?>
                            </select></td>
                    </tr>


                    <tr>
                        <td align="right" class="txt_bold_gray" width="250"> วิธีการชำระเงิน :</td>
                        <td><input type="text" name="ag_howtobuycredit" value="<?= $row['ag_howtobuycredit'] ?>"
                                   size="20"></td>
                    </tr>


                </table>
            </td>
        </tr>


        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td><input type="submit" name="submit" value="SUBMIT" style="width:75px;"><?php
                if (isset($id)) echo " <input type=\"submit\" name=\"del\" value=\"DELETE\" style=\"width:75px;\" onClick=\"return confirm('Do you want to delete this record ?')\">";
                else echo " <input type=\"reset\" name=\"Reset\" value=\"RESET\" style=\"width:75px;\">";
                ?></td>
        </tr>
    </table>
</form>


<script type="text/javascript">
    checkPayType();
</script>