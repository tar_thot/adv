<?php
include("value_get.php");
if (isset($id)) {
    $sql = "select * from hotels where hot_id = $id";
    $result = mysql_query($sql);
    $row = mysql_fetch_array($result);
    include("inc.top.php");
    if ($hotpe_id) {
        $page_title = "Update Period";
        $sql = "select * from hotel_period where hotpe_id = $hotpe_id";
        $result = mysql_query($sql);
        $row = mysql_fetch_array($result);

        //echo "sql : $sql";
    } else {
        $row = "";
        $page_title = "Create New Period";
    }
    ?>
    <script language="JavaScript" type="text/JavaScript">
        <!--
        function MM_findObj(n, d) { //v4.01
            var p, i, x;
            if (!d) d = document;
            if ((p = n.indexOf("?")) > 0 && parent.frames.length) {
                d = parent.frames[n.substring(p + 1)].document;
                n = n.substring(0, p);
            }
            if (!(x = d[n]) && d.all) x = d.all[n];
            for (i = 0; !x && i < d.forms.length; i++) x = d.forms[i][n];
            for (i = 0; !x && d.layers && i < d.layers.length; i++) x = MM_findObj(n, d.layers[i].document);
            if (!x && d.getElementById) x = d.getElementById(n);
            return x;
        }
        function MM_validateForm() { //v4.0
            var i, p, q, nm, test, num, min, max, errors = '', args = MM_validateForm.arguments;
            for (i = 0; i < (args.length - 2); i += 3) {
                test = args[i + 2];
                val = MM_findObj(args[i]);
                if (val) {
                    nm = val.name;
                    if ((val = val.value) != "") {
                        if (test.indexOf('isEmail') != -1) {
                            p = val.indexOf('@');
                            if (p < 1 || p == (val.length - 1)) errors += '- E-mail must contain an e-mail address.\n';
                        } else if (test != 'R') {
                            num = parseFloat(val);
                            if (isNaN(val)) {
                                if (nm == 'name') nm = 'Room Type';
                                errors += '- ' + nm + ' must contain a number.\n';
                            }
                            if (test.indexOf('inRange') != -1) {
                                p = test.indexOf(':');
                                min = test.substring(8, p);
                                max = test.substring(p + 1);
                                if (num < min || max < num) errors += '- ' + nm + ' must contain a number between ' + min + ' and ' + max + '.\n';
                            }
                        }
                    } else if (test.charAt(0) == 'R') {
                        if (nm == 'tourt_name') nm = 'Period';
                        errors += '- ' + nm + ' is required.\n';
                    }
                }
            }
            if (errors) alert('The following error(s) occurred:\n' + errors);
            document.MM_returnValue = (errors == '');
        }
        //-->
    </script>

    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="txt_big_gray"><?= $page_title ?></td>
        </tr>
    </table>
    <form action="process.php?mode=hotels/period_save&id=<?= $id ?>&hotpe_id=<?= $hotpe_id ?>" name="form" method="post"
          enctype="multipart/form-data">
        <table width="100%" border="0" cellspacing="0" cellpadding="3">
            <tr>
                <td align="right" class="txt_bold_gray" width="200">Period Name :</td>
                <td><input type="text" name="hotpe_name" value="<?= $row['hotpe_name'] ?>" size="50"/></td>
            </tr>
            <?php
            if ($row['hotpe_datefrom']) {
                $hotpe_datefrom = $row['hotpe_datefrom'];
            } else {
                $hotpe_datefrom = $today;
            }

            if ($row['hotpe_dateto']) {
                $hotpe_dateto = $row['hotpe_dateto'];
            } else {
                $hotpe_dateto = $today;
            }
            ?>

            <tr>
                <td align="right" class="txt_bold_gray" width="200">Date From :</td>
                <td>
                    <script>DateInput('hotpe_datefrom', false, 'yyyy-mm-dd', '<?= $hotpe_datefrom?>')</script>
                </td>
            </tr>

            <tr>
                <td align="right" class="txt_bold_gray" width="200">Date To :</td>
                <td>
                    <script>DateInput('hotpe_dateto', false, 'yyyy-mm-dd', '<?= $hotpe_dateto?>')</script>
                </td>
            </tr>


            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td><input type="submit" name="submit" value="SUBMIT" style="width:75px;"><?php
                    if ($botpe_id) echo " <input type=\"submit\" name=\"del\" value=\"DELETE\" style=\"width:75px;\" onClick=\"return confirm('Do you want to delete this record ?')\">";
                    else echo " <input type=\"reset\" name=\"Reset\" value=\"RESET\" style=\"width:75px;\">";
                    ?></td>
            </tr>
        </table>
    </form>
    <?php
}
?>
