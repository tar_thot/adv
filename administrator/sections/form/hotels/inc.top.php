<table width="100%" border="0" cellspacing="0" cellpadding="3">
    <tr>
        <td>
            <table border="0" cellspacing="1" cellpadding="3" bgcolor="<?= $color_1 ?>">
                <tr bgcolor="<?= $color_3 ?>">
                    <td bgcolor="<?= $color_2 ?>" align="right" class="txt_bold_gray">Name :</td>
                    <td><?= $row['hot_name'] ?></td>
                </tr>
                <tr bgcolor="<?= $color_3 ?>">
                    <td bgcolor="<?= $color_2 ?>" align="right" class="txt_bold_gray">Country :</td>
                    <td><?= get_value('hotel_con_country', 'con_id', 'con_name', $row['country_id']) ?></td>
                </tr>
                <tr bgcolor="<?= $color_3 ?>">
                    <td bgcolor="<?= $color_2 ?>" align="right" class="txt_bold_gray">Province/State :</td>
                    <td><?= get_value('hotel_con_province', 'con_id', 'con_name', $row['province_id']) ?></td>
                </tr>
                <tr bgcolor="<?= $color_3 ?>">
                    <td bgcolor="<?= $color_2 ?>" align="right" class="txt_bold_gray">Area :</td>
                    <td><?= get_value('hotel_con_area', 'con_id', 'con_name', $row['area_id']) ?></td>
                </tr>
                <tr bgcolor="<?= $color_3 ?>">
                    <td bgcolor="<?= $color_2 ?>" align="right" class="txt_bold_gray">Room Type :</td>
                    <td><?= get_value('hotel_con_room_type', 'con_id', 'con_name', $row['room_type_id']) ?></td>
                </tr>


                <tr bgcolor="<?= $color_3 ?>">
                    <td bgcolor="<?= $color_2 ?>" align="right" class="txt_bold_gray">ประเภทการกำหนดราคา :</td>
                    <td><?= get_value('hotel_price_type', 'c_id', 'c_name', $row['hotelpricetype_id']) ?></td>
                </tr>

                <tr bgcolor="<?= $color_3 ?>">
                    <td bgcolor="<?= $color_2 ?>" align="right" class="txt_bold_gray">การคำนวณราคา :</td>
                    <td>รายห้อง</td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<table width="100%" border="0" cellspacing="1" cellpadding="3" bgcolor="<?= $color_inctop1 ?>">
    <tr bgcolor="<?= $color_inctop2 ?>">
        <td><span class="txt_bold_white">
        <a href="./?mode=hotels/add&id=<?= $id ?>" class="txt_bold_white">Edit</a><strong
                    style="color:<?= $color_inctop1 ?>;"> | </strong>
        <a href="./?mode=hotels/ratetypes_index&id=<?= $id ?>" class="txt_bold_white">Rate Type</a><strong
                    style="color:<?= $color_inctop1 ?>;"> | </strong>
        <a href="./?mode=hotels/period_index&id=<?= $id ?>" class="txt_bold_white">Period</a><strong
                    style="color:<?= $color_inctop1 ?>;"> | </strong>
        <a href="./?mode=hotels/rate_index&id=<?= $id ?>" class="txt_bold_white">Rate Setting</a><strong
                    style="color:<?= $color_inctop1 ?>;"> | </strong>
        <a href="./?mode=hotels/allocation_index&id=<?= $id ?>" class="txt_bold_white">Allocation Setting</a><strong
                    style="color:<?= $color_inctop1 ?>;"> | </strong>
        </span></td>
    </tr>
</table>
<br/>
