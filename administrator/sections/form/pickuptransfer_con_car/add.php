<?php
// Convert Variable Array To Variable

while (list($xVarName, $xVarvalue) = each($_GET)) {
    ${$xVarName} = $xVarvalue;
}


while (list($xVarName, $xVarvalue) = each($_POST)) {
    ${$xVarName} = $xVarvalue;
}

while (list($xVarName, $xVarvalue) = each($_FILES)) {
    ${$xVarName . "_name"} = $xVarvalue['name'];
    ${$xVarName . "_type"} = $xVarvalue['type'];
    ${$xVarName . "_size"} = $xVarvalue['size'];
    ${$xVarName . "_error"} = $xVarvalue['error'];
    ${$xVarName} = $xVarvalue['tmp_name'];
}
//เพิ่มเติม 21/07/58======================================
if (isset($id)) {
    $page_title = "Update Pick Up Transfer Car";
    $sql = "select * from pickuptransfer_con_car where con_id = $id";
    $result = mysql_query($sql);
    $row = mysql_fetch_array($result);


} else {
    $page_title = "Create New Pick Up Transfer Car";
}

?>

<script language="JavaScript" type="text/JavaScript">
    <!--
    function MM_findObj(n, d) { //v4.01
        var p, i, x;
        if (!d) d = document;
        if ((p = n.indexOf("?")) > 0 && parent.frames.length) {
            d = parent.frames[n.substring(p + 1)].document;
            n = n.substring(0, p);
        }
        if (!(x = d[n]) && d.all) x = d.all[n];
        for (i = 0; !x && i < d.forms.length; i++) x = d.forms[i][n];
        for (i = 0; !x && d.layers && i < d.layers.length; i++) x = MM_findObj(n, d.layers[i].document);
        if (!x && d.getElementById) x = d.getElementById(n);
        return x;
    }
    function MM_validateForm() { //v4.0
        var i, p, q, nm, test, num, min, max, errors = '', args = MM_validateForm.arguments;
        for (i = 0; i < (args.length - 2); i += 3) {
            test = args[i + 2];
            val = MM_findObj(args[i]);
            if (val) {
                nm = val.name;
                if ((val = val.value) != "") {
                    if (test.indexOf('isEmail') != -1) {
                        p = val.indexOf('@');
                        if (p < 1 || p == (val.length - 1)) errors += '- E-mail must contain an e-mail address.\n';
                    } else if (test != 'R') {
                        num = parseFloat(val);
                        if (isNaN(val)) {
                            if (nm == 'ag_balance_credit') nm = 'Credit Balance';
                            if (nm == 'ag_min_credit') nm = 'Minimum of Credit available';
                            errors += '- ' + nm + ' must contain a number.\n';
                        }
                        if (test.indexOf('inRange') != -1) {
                            p = test.indexOf(':');
                            min = test.substring(8, p);
                            max = test.substring(p + 1);
                            if (num < min || max < num) errors += '- ' + nm + ' must contain a number between ' + min + ' and ' + max + '.\n';
                        }
                    }
                } else if (test.charAt(0) == 'R') {
                    if (nm == 'con_name') nm = 'Name';
                    if (nm == 'ag_name') nm = 'Name';
                    if (nm == 'ag_email') nm = 'Email';

                    errors += '- ' + nm + ' is required.\n';
                }
            }
        }
        if (errors) alert('The following error(s) occurred:\n' + errors);
        document.MM_returnValue = (errors == '');
    }
    //-->
</script>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td class="txt_big_gray"><?= $page_title ?></td>
    </tr>
</table>
<form action="process.php?mode=pickuptransfer_con_car/save&id=<?= $id ?>" onsubmit="MM_validateForm(
'car_registration','','R'
);return document.MM_returnValue" name="form" id="form1" method="post" enctype="multipart/form-data">
    <input type="hidden" name="id" id="id" value="<?= $id ?>"/>
    <table width="100%" border="0" cellspacing="0" cellpadding="3">
        <tr>
            <td align="right" class="txt_bold_gray" width="250"></td>
            <td width="1126" align="right"><a href="./?mode=pickuptransfer_con_car/index"
                                              style="background-color:#ffffff; color:#000000"><<< Back</a></td>
        </tr>
        <tr>
            <td align="right" class="txt_bold_gray" width="250">ทะเบียนรถ :</td>
            <td width="1126"><input type="text" name="car_registration" value="<?= $row['car_registration'] ?>"
                                    size="50"><span class="remark"> * </span></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="250">จำนวนที่นั่ง :</td>
            <td width="1126">
                <select name="car_seat_num">
                    <option value="">-- Select --</option>
                    <?php list_number($row['car_seat_num'], 1, 500); ?>
                </select>
            </td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="250">ยี่ห้อรถ :</td>
            <td width="1126"><input type="text" name="car_brand" value="<?= $row['car_brand'] ?>" size="50"></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="250">รุ่นรถ :</td>
            <td width="1126"><input type="text" name="car_model" value="<?= $row['car_model'] ?>" size="50"></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="250">ชื่อคนขับรถ :</td>
            <td width="1126"><input type="text" name="car_driver_name" value="<?= $row['car_driver_name'] ?>" size="50">
            </td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="250">เบอร์โทรศัพท์คนขับรถ :</td>
            <td width="1126"><input type="text" name="car_driver_tel" value="<?= $row['car_driver_tel'] ?>" size="50">
            </td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="250">ข้อมูลใบขับขี่ :</td>
            <td width="1126"><textarea name="car_driver_license"
                                       style="width:325px; height:100px;"><?= $row['car_driver_license'] ?></textarea>
            </td>
        </tr>


        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td><input type="submit" name="submit" value="SUBMIT" style="width:75px;"><?php
                if (isset($id)) echo " <input type=\"submit\" name=\"del\" value=\"DELETE\" style=\"width:75px;\" onClick=\"return confirm('Do you want to delete this record ?')\">";
                else echo " <input type=\"reset\" name=\"Reset\" value=\"RESET\" style=\"width:75px;\">";
                ?></td>
        </tr>
    </table>
</form>


<script type="text/javascript">
    checkPayType();
</script>