<table width="100%" height="100%" border="0" cellpadding="3" cellspacing="0">
    <tr>
        <td align="center">
            <table border="0" cellspacing="0" cellpadding="3">
                <tr>
                    <td><img src="images/icon_menu/settings-48x48.png" width="48" height="48"/></td>
                    <td class="txt_big_gray">CONFIGURATION SYSTEM</td>
                    <td><img src="images/icon_menu/settings-48x48.png" width="48" height="48"/></td>
                </tr>

            </table>
            <table width="650" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td align="center" valign="top">
                        &nbsp;

                        <table border="0" width="600" cellspacing="0" cellpadding="3">

                            <tr>
                                <td colspan="5">
                                    <hr/>
                                </td>
                            </tr>
                            <tr>
                                <th width="127" valign="top" align="center">&nbsp;Agent Config</th>
                                <td width="26"><img src="images/icon_menu/Nero Smart Start.png" width="25" height="25"/>
                                </td>
                                <td width="144"><a href="./?mode=agent_con_country/index"
                                                   class="txt_bold_gray">Country</a></td>
                                <td width="26"><img src="images/icon_menu/Nero Smart Start.png" width="25" height="25"/>
                                </td>
                                <td width="139"><a href="./?mode=agent_con_province/index" class="txt_bold_gray">Province/State</a>
                                </td>
                            </tr>
                            <tr>
                                <th width="127" valign="middle">&nbsp;</th>
                                <td width="26"><img src="images/icon_menu/Nero Smart Start.png" width="25" height="25"/>
                                </td>
                                <td width="144"><a href="./?mode=agent_con_city/index" class="txt_bold_gray">City</a>
                                </td>
                                <td width="26"><img src="images/icon_menu/Nero Smart Start.png" width="25" height="25"/>
                                </td>
                                <td width="139"><a href="./?mode=agent_con_businesstype/index" class="txt_bold_gray">Business
                                        Type</a></td>
                            </tr>

                            <tr>
                                <td colspan="5">
                                    <hr/>
                                </td>
                            </tr>
                            <tr>
                                <th width="127" valign="top" align="center">&nbsp;Client Config</th>
                                <td width="26"><img src="images/icon_menu/Nero Smart Start.png" width="25" height="25"/>
                                </td>
                                <td width="144"><a href="./?mode=client_con_country/index"
                                                   class="txt_bold_gray">Country</a></td>
                                <td width="26"><img src="images/icon_menu/Nero Smart Start.png" width="25" height="25"/>
                                </td>
                                <td width="139"><a href="./?mode=client_con_province/index" class="txt_bold_gray">Province/State</a>
                                </td>
                            </tr>
                            <tr>
                                <th width="127" valign="middle">&nbsp;</th>
                                <td width="26"><img src="images/icon_menu/Nero Smart Start.png" width="25" height="25"/>
                                </td>
                                <td width="144"><a href="./?mode=client_con_city/index" class="txt_bold_gray">City</a>
                                </td>
                                <td width="26"><img src="images/icon_menu/Nero Smart Start.png" width="25" height="25"/>
                                </td>
                                <td width="139"><a href="./?mode=client_con_category/index" class="txt_bold_gray">Client
                                        Category</a></td>
                            </tr>


                            <tr>
                                <td colspan="5">
                                    <hr/>
                                </td>
                            </tr>
                            <tr>
                                <th width="127" valign="top" align="center">&nbsp;Supplier Config</th>
                                <td width="26"><img src="images/icon_menu/Nero Smart Start.png" width="25" height="25"/>
                                </td>
                                <td width="144"><a href="./?mode=supplier_con_country/index" class="txt_bold_gray">Country</a>
                                </td>
                                <td width="26"><img src="images/icon_menu/Nero Smart Start.png" width="25" height="25"/>
                                </td>
                                <td width="139"><a href="./?mode=supplier_con_province/index" class="txt_bold_gray">Province/State</a>
                                </td>
                            </tr>
                            <tr>
                                <th width="127" valign="middle">&nbsp;</th>
                                <td width="26"><img src="images/icon_menu/Nero Smart Start.png" width="25" height="25"/>
                                </td>
                                <td width="144"><a href="./?mode=supplier_con_city/index" class="txt_bold_gray">City</a>
                                </td>
                                <td width="26"><img src="images/icon_menu/Nero Smart Start.png" width="25" height="25"/>
                                </td>
                                <td width="144"><a href="./?mode=supplier_con_servicetype/index" class="txt_bold_gray">Type
                                        Of Service</a></td>
                            </tr>


                            <tr>
                                <td colspan="5">
                                    <hr/>
                                </td>
                            </tr>
                            <tr>
                                <th width="127" valign="top" align="center">&nbsp;Tour Config</th>
                                <td width="26"><img src="images/icon_menu/Nero Smart Start.png" width="25" height="25"/>
                                </td>
                                <td width="144"><a href="./?mode=tour_con_country/index"
                                                   class="txt_bold_gray">Country</a></td>
                                <td width="26"><img src="images/icon_menu/Nero Smart Start.png" width="25" height="25"/>
                                </td>
                                <td width="139"><a href="./?mode=tour_con_province/index" class="txt_bold_gray">Province/State</a>
                                </td>
                            </tr>
                            <tr>
                                <th width="127" valign="middle">&nbsp;</th>
                                <td width="26"><img src="images/icon_menu/Nero Smart Start.png" width="25" height="25"/>
                                </td>
                                <td width="144"><a href="./?mode=tour_con_area/index" class="txt_bold_gray">Area</a>
                                </td>
                                <td width="26"><img src="images/icon_menu/Nero Smart Start.png" width="25" height="25"/>
                                </td>
                                <td width="139"><a href="./?mode=tour_con_category/index"
                                                   class="txt_bold_gray">Category</a></td>
                            </tr>


                            <tr>
                                <td colspan="5">
                                    <hr/>
                                </td>
                            </tr>
                            <tr>
                                <th width="127" valign="top" align="center">&nbsp;Activity Config</th>
                                <td width="26"><img src="images/icon_menu/Nero Smart Start.png" width="25" height="25"/>
                                </td>
                                <td width="144"><a href="./?mode=activity_con_country/index" class="txt_bold_gray">Country</a>
                                </td>
                                <td width="26"><img src="images/icon_menu/Nero Smart Start.png" width="25" height="25"/>
                                </td>
                                <td width="139"><a href="./?mode=activity_con_province/index" class="txt_bold_gray">Province/State</a>
                                </td>
                            </tr>
                            <tr>
                                <th width="127" valign="middle">&nbsp;</th>
                                <td width="26"><img src="images/icon_menu/Nero Smart Start.png" width="25" height="25"/>
                                </td>
                                <td width="144"><a href="./?mode=activity_con_area/index" class="txt_bold_gray">Area</a>
                                </td>
                                <td width="26"><img src="images/icon_menu/Nero Smart Start.png" width="25" height="25"/>
                                </td>
                                <td width="139"><a href="./?mode=activity_con_category/index" class="txt_bold_gray">Category</a>
                                </td>
                            </tr>


                            <tr>
                                <td colspan="5">
                                    <hr/>
                                </td>
                            </tr>
                            <tr>
                                <th width="127" valign="top" align="center">&nbsp;Hotel Config</th>
                                <td width="26"><img src="images/icon_menu/Nero Smart Start.png" width="25" height="25"/>
                                </td>
                                <td width="144"><a href="./?mode=hotel_con_country/index"
                                                   class="txt_bold_gray">Country</a></td>
                                <td width="26"><img src="images/icon_menu/Nero Smart Start.png" width="25" height="25"/>
                                </td>
                                <td width="139"><a href="./?mode=hotel_con_province/index" class="txt_bold_gray">Province/State</a>
                                </td>
                            </tr>
                            <tr>
                                <th width="127" valign="middle">&nbsp;</th>
                                <td width="26"><img src="images/icon_menu/Nero Smart Start.png" width="25" height="25"/>
                                </td>
                                <td width="144"><a href="./?mode=hotel_con_area/index" class="txt_bold_gray">Area</a>
                                </td>
                                <td width="26"><img src="images/icon_menu/Nero Smart Start.png" width="25" height="25"/>
                                </td>
                                <td width="139"><a href="./?mode=hotel_con_optional/index" class="txt_bold_gray">Optional</a>
                                </td>
                            </tr>
                            <tr>
                                <th width="127" valign="middle">&nbsp;</th>
                                <td width="26"><img src="images/icon_menu/Nero Smart Start.png" width="25" height="25"/>
                                </td>
                                <td width="144"><a href="./?mode=hotel_con_facility/index" class="txt_bold_gray">Hotel
                                        Facility</a></td>
                                <td width="26"><img src="images/icon_menu/Nero Smart Start.png" width="25" height="25"/>
                                </td>
                                <td width="139"><a href="./?mode=hotel_con_room_facility/index" class="txt_bold_gray">Room
                                        Facility</a></td>
                            </tr>
                            <tr>
                                <th width="127" valign="middle">&nbsp;</th>
                                <td width="26"><img src="images/icon_menu/Nero Smart Start.png" width="25" height="25"/>
                                </td>
                                <td width="144"><a href="./?mode=hotel_con_room_type/index" class="txt_bold_gray">Room
                                        Type</a></td>
                                <td width="26">
                                    <!--<img src="images/icon_menu/Nero Smart Start.png" width="25" height="25" />--></td>
                                <td width="139"><a href="./?mode=underconstruction/index" class="txt_bold_gray">
                                        &nbsp;</a></td>
                            </tr>


                            <tr>
                                <td colspan="5">
                                    <hr/>
                                </td>
                            </tr>
                            <tr>
                                <th width="127" valign="top" align="center">&nbsp;Bus Transfer Config</th>
                                <td width="26"><img src="images/icon_menu/Nero Smart Start.png" width="25" height="25"/>
                                </td>
                                <td width="144"><a href="./?mode=cartransfer_con_country/index" class="txt_bold_gray">Country</a>
                                </td>
                                <td width="26"><img src="images/icon_menu/Nero Smart Start.png" width="25" height="25"/>
                                </td>
                                <td width="139"><a href="./?mode=cartransfer_con_province/index" class="txt_bold_gray">Province/State</a>
                                </td>
                            </tr>
                            <tr>
                                <th width="127" valign="middle">&nbsp;</th>
                                <td width="26"><img src="images/icon_menu/Nero Smart Start.png" width="25" height="25"/>
                                </td>
                                <td width="144"><a href="./?mode=cartransfer_con_area/index"
                                                   class="txt_bold_gray">Area</a></td>
                                <td width="26"><img src="images/icon_menu/Nero Smart Start.png" width="25" height="25"/>
                                </td>
                                <td width="139"><a href="./?mode=cartransfer_con_destination/index"
                                                   class="txt_bold_gray">Destination</a></td>
                            </tr>
                            <tr>
                                <th width="127" valign="middle">&nbsp;</th>
                                <td width="26"><img src="images/icon_menu/Nero Smart Start.png" width="25" height="25"/>
                                </td>
                                <td width="144"><a href="./?mode=cartransfer_con_time/index"
                                                   class="txt_bold_gray">Time</a></td>
                                <td width="26"><img src="images/icon_menu/Nero Smart Start.png" width="25" height="25"/>
                                </td>
                                <td width="139"><a href="./?mode=cartransfer_con_car/index"
                                                   class="txt_bold_gray">Car</a></td>
                            </tr>


                            <tr>
                                <td colspan="5">
                                    <hr/>
                                </td>
                            </tr>
                            <tr>
                                <th width="127" valign="top" align="center">&nbsp;Pick Up Transfer Config</th>
                                <td width="26"><img src="images/icon_menu/Nero Smart Start.png" width="25" height="25"/>
                                </td>
                                <td width="144"><a href="./?mode=pickuptransfer_con_country/index"
                                                   class="txt_bold_gray">Country</a></td>
                                <td width="26"><img src="images/icon_menu/Nero Smart Start.png" width="25" height="25"/>
                                </td>
                                <td width="139"><a href="./?mode=pickuptransfer_con_province/index"
                                                   class="txt_bold_gray">Province/State</a></td>
                            </tr>
                            <tr>
                                <th width="127" valign="middle">&nbsp;</th>
                                <td width="26"><img src="images/icon_menu/Nero Smart Start.png" width="25" height="25"/>
                                </td>
                                <td width="144"><a href="./?mode=pickuptransfer_con_area/index" class="txt_bold_gray">Area</a>
                                </td>
                                <td width="26"><img src="images/icon_menu/Nero Smart Start.png" width="25" height="25"/>
                                </td>
                                <td width="139"><a href="./?mode=pickuptransfer_con_destination/index"
                                                   class="txt_bold_gray">Destination</a></td>
                            </tr>
                            <tr>
                                <th width="127" valign="middle">&nbsp;</th>
                                <td width="26"><img src="images/icon_menu/Nero Smart Start.png" width="25" height="25"/>
                                </td>
                                <td width="144"><a href="./?mode=pickuptransfer_con_time/index" class="txt_bold_gray">Time</a>
                                </td>
                                <td width="26"><img src="images/icon_menu/Nero Smart Start.png" width="25" height="25"/>
                                </td>
                                <td width="139"><a href="./?mode=pickuptransfer_con_car/index"
                                                   class="txt_bold_gray">Car</a></td>
                            </tr>
                            <tr>
                                <th width="127" valign="middle">&nbsp;</th>
                                <td width="26"><img src="images/icon_menu/Nero Smart Start.png" width="25" height="25"/>
                                </td>
                                <td width="144"><a href="./?mode=pickuptransfer_con_forsearch/index"
                                                   class="txt_bold_gray">For Search</a></td>
                                <td width="26"></td>
                                <td width="139"></td>
                            </tr>

                            <tr>
                                <td colspan="5">
                                    <hr/>
                                </td>
                            </tr>
                            <tr>
                                <th width="127" valign="top" align="center">&nbsp;Private Land Transfer Config</th>
                                <td width="26"><img src="images/icon_menu/Nero Smart Start.png" width="25" height="25"/>
                                </td>
                                <td width="144"><a href="./?mode=privatelandtransfer_con_country/index"
                                                   class="txt_bold_gray">Country</a></td>
                                <td width="26"><img src="images/icon_menu/Nero Smart Start.png" width="25" height="25"/>
                                </td>
                                <td width="139"><a href="./?mode=privatelandtransfer_con_province/index"
                                                   class="txt_bold_gray">Province/State</a></td>
                            </tr>
                            <tr>
                                <th width="127" valign="middle">&nbsp;</th>
                                <td width="26"><img src="images/icon_menu/Nero Smart Start.png" width="25" height="25"/>
                                </td>
                                <td width="144"><a href="./?mode=privatelandtransfer_con_area/index"
                                                   class="txt_bold_gray">Area</a></td>
                                <td width="26"><img src="images/icon_menu/Nero Smart Start.png" width="25" height="25"/>
                                </td>
                                <td width="139"><a href="./?mode=privatelandtransfer_con_destination/index"
                                                   class="txt_bold_gray">Destination</a></td>
                            </tr>
                            <tr>
                                <th width="127" valign="middle">&nbsp;</th>
                                <td width="26"><img src="images/icon_menu/Nero Smart Start.png" width="25" height="25"/>
                                </td>
                                <td width="144"><a href="./?mode=privatelandtransfer_con_time/index"
                                                   class="txt_bold_gray">Time</a></td>
                                <td width="26"><img src="images/icon_menu/Nero Smart Start.png" width="25" height="25"/>
                                </td>
                                <td width="139"><a href="./?mode=privatelandtransfer_con_car/index"
                                                   class="txt_bold_gray">Car</a></td>
                            </tr>

                            <tr>
                                <td colspan="5">
                                    <hr/>
                                </td>
                            </tr>
                            <tr>
                                <th width="127" valign="top" align="center">&nbsp;Boat Transfer Config</th>
                                <td width="26"><img src="images/icon_menu/Nero Smart Start.png" width="25" height="25"/>
                                </td>
                                <td width="144"><a href="./?mode=boattransfer_con_country/index" class="txt_bold_gray">Country</a>
                                </td>
                                <td width="26"><img src="images/icon_menu/Nero Smart Start.png" width="25" height="25"/>
                                </td>
                                <td width="139"><a href="./?mode=boattransfer_con_province/index" class="txt_bold_gray">Province/State</a>
                                </td>
                            </tr>
                            <tr>
                                <th width="127" valign="middle">&nbsp;</th>
                                <td width="26"><img src="images/icon_menu/Nero Smart Start.png" width="25" height="25"/>
                                </td>
                                <td width="144"><a href="./?mode=boattransfer_con_area/index"
                                                   class="txt_bold_gray">Area</a></td>
                                <td width="26"><img src="images/icon_menu/Nero Smart Start.png" width="25" height="25"/>
                                </td>
                                <td width="139"><a href="./?mode=boattransfer_con_destination/index"
                                                   class="txt_bold_gray">Destination</a></td>
                            </tr>
                            <tr>
                                <th width="127" valign="middle">&nbsp;</th>
                                <td width="26"><img src="images/icon_menu/Nero Smart Start.png" width="25" height="25"/>
                                </td>
                                <td width="144"><a href="./?mode=boattransfer_con_time/index"
                                                   class="txt_bold_gray">Time</a></td>
                                <td width="26"><img src="images/icon_menu/Nero Smart Start.png" width="25" height="25"/>
                                </td>
                                <td width="139"><a href="./?mode=boattransfer_con_boat/index"
                                                   class="txt_bold_gray">Boat</a></td>
                            </tr>
                            <tr>
                                <th width="127" valign="middle">&nbsp;</th>
                                <td width="26"><img src="images/icon_menu/Nero Smart Start.png" width="25" height="25"/>
                                </td>
                                <td width="144"><a href="./?mode=boattransfer_con_forsearch/index"
                                                   class="txt_bold_gray">For Search</a></td>
                                <td width="26"></td>
                                <td width="139"></td>
                            </tr>


                            <tr>
                                <td colspan="5">
                                    <hr/>
                                </td>
                            </tr>
                            <tr>
                                <th width="127" valign="top" align="center">&nbsp;Train Transfer Config</th>
                                <td width="26"><img src="images/icon_menu/Nero Smart Start.png" width="25" height="25"/>
                                </td>
                                <td width="144"><a href="./?mode=traintransfer_con_country/index" class="txt_bold_gray">Country</a>
                                </td>
                                <td width="26"><img src="images/icon_menu/Nero Smart Start.png" width="25" height="25"/>
                                </td>
                                <!--<td width="139"><a href="./?mode=traintransfer_con_province/index" class="txt_bold_gray">Province/State</a></td>-->
                                <td width="139"><a href="./?mode=traintransfer_con_province/index"
                                                   class="txt_bold_gray">Destination</a></td>
                            </tr>
                            <!--<tr>
                              <th width="127" valign="middle">&nbsp;</th>
                              <td width="26"><img src="images/icon_menu/Nero Smart Start.png" width="25" height="25" /></td>
                              <td width="144"><a href="./?mode=underconstruction/index" class="txt_bold_gray">Area</a></td>
                              <td width="26"><img src="images/icon_menu/Nero Smart Start.png" width="25" height="25" /></td>
                              <td width="139"><a href="./?mode=underconstruction/index" class="txt_bold_gray">Destination</a></td>
                            </tr>-->
                            <tr>
                                <th width="127" valign="middle">&nbsp;</th>
                                <td width="26"><img src="images/icon_menu/Nero Smart Start.png" width="25" height="25"/>
                                </td>
                                <td width="144"><a href="./?mode=traintransfer_con_time/index" class="txt_bold_gray">Time</a>
                                </td>
                                <td width="26"><img src="images/icon_menu/Nero Smart Start.png" width="25" height="25"/>
                                </td>
                                <td width="139"><a href="./?mode=traintransfer_con_train/index" class="txt_bold_gray">Train</a>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="5">
                                    <hr/>
                                </td>
                            </tr>
                            <tr>
                                <th width="127" valign="top" align="center">&nbsp;Combo Product Config</th>
                                <!--<td width="26"><img src="images/icon_menu/Nero Smart Start.png" width="25" height="25" /></td>
                                <td width="139"><a href="./?mode=package_con_category/index" class="txt_bold_gray">Combo Product Category</a></td>-->
                                <td width="26"><img src="images/icon_menu/Nero Smart Start.png" width="25" height="25"/>
                                </td>
                                <td width="144"><a href="./?mode=package_con_forsearch/index" class="txt_bold_gray">For
                                        Search</a></td>
                                <td width="26">&nbsp;</td>
                                <td width="144">&nbsp;</td>
                            </tr>

                            <tr>
                                <td colspan="5">
                                    <hr/>
                                </td>
                            </tr>

                            <tr>
                                <td colspan="1"></td>
                                <td colspan="1">&nbsp;</td>
                                <td colspan="1">&nbsp;</td>
                                <td colspan="2"></td>
                            </tr>
                        </table>


                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
