<?php

function setNumberLength($num, $length)
{
    $sumstr = strlen($num);
    $zero = str_repeat("0", $length - $sumstr);
    $results = $zero . $num;

    return $results;
}

function DateDiff($strDate1, $strDate2)
{
    return (strtotime($strDate2) - strtotime($strDate1)) / (60 * 60 * 24);  // 1 day = 60*60*24
}

?>

<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td valign="top">
            <table width="100%" border="0" cellspacing="0" cellpadding="3">
                <tr>
                    <td class="txt_big_gray">Drop-off Transfer List</td>
                    <td width="500" align="right"><a href="./index.php?mode=reports/dropoff_transfer_st1"
                                                     style="background-color:#ffffff; color:#000000"><<< Back</a></td>
                </tr>
            </table>
            <!---- Listing Body ---->
            <table width="100%" border="0" cellspacing="0" cellpadding="3">
                <tr>
                    <td>
                        <!---- Search Box ---->
                        <br/>

                        <?php //$strNewDate = date("Y-m-d", strtotime("+3 day", strtotime($_POST[to_date]))); ?>

                        <table align="center">
                            <tr>
                                <td class="txt_bold_gray" align="center">
                                    <?php if (isset($_POST['area_id'])) {
                                        echo get_value('pickuptransfer_con_area', 'con_id', 'con_name', $_POST['area_id']);
                                    } ?> - (<?= DateFormat($_POST['from_dat'], "f") ?>)
                                </td>
                            </tr>
                        </table>


                        <?php // Reservations Pick Up Transfer ?>

                        <br/>

                        <table border="1" cellspacing="0" cellpadding="3" bgcolor="#FFFFFF" bordercolor="#000000">
                            <tr bgcolor="#CCCCCC">
                                <td width="20" align="center" class="txt_bold_gray">CODE</td>
                                <td width="500" class="txt_bold_gray" align="center">AGENT</td>
                                <td width="500" class="txt_bold_gray" align="center">PASSENGER'S NAME</td>
                                <td width="500" class="txt_bold_gray" align="center">PASSENGER'S EMAIL</td>
                                <td width="20" class="txt_bold_gray" align="center">PAX</td>
                                <td width="50" class="txt_bold_gray" align="center">CLASS</td>
                                <td width="50" class="txt_bold_gray" align="center">TYPE</td>
                                <td width="500" class="txt_bold_gray" align="center">PICK UP FROM</td>
                                <td width="500" class="txt_bold_gray" align="center">DROP-OFF TO</td>
                                <td width="200" class="txt_bold_gray" align="center">REMARK</td>
                            </tr>


                            <?php // Query Reservations Pick UP Transfer

                            $sql = "SELECT * FROM pickuptransfers WHERE area_id = " . $_POST['area_id'] . " ORDER BY put_id ASC";

                            //echo $sql;

                            $results = mysql_query($sql);
                            while ($row = mysql_fetch_array($results)) {

                                //for( $i = 1 ; $i <= $item_num ; $i++ ){

                                $sql_res = "SELECT * ";
                                $sql_res .= "FROM reservation_pickuptransfer_items ";
                                $sql_res .= "WHERE pickuptransfers_id = '" . $row['put_id'] . " ";
                                $sql_res .= "AND rpt_travel_date = '" . $_POST['from_date'] . " ";

                                $sql_res .= "ORDER BY rpt_id ASC ";

                                $result_res = mysql_query($sql_res);
                                while ($row_res = mysql_fetch_array($result_res)) {
                                    ?>
                                    <?php
                                    $agent_id = get_value('reservations', 'res_id', 'agents_id', $row_res['reservations_id']);
                                    $code_agent = get_value('agents', 'ag_id', 'ag_ref', $agent_id);
                                    $agent_name = get_value('agents', 'ag_id', 'ag_name', $agent_id);
                                    $agentpaytype_id = get_value('agents', 'ag_id', 'agentpaytype_id', $agent_id);
                                    $res_email = get_value('reservations', 'res_id', 'res_email', $row_res['reservations_id']);

                                    $res_book_by = get_value('reservations', 'res_id', 'res_book_by', $row_res['reservations_id']);

                                    $titlename_id = get_value('reservations', 'res_id', 'titlename_id', $row_res['reservations_id']);
                                    $lis_name = get_value('lis_titlename', 'lis_id', 'lis_name', $titlename_id);
                                    $res_fname = get_value('reservations', 'res_id', 'res_fname', $row_res['reservations_id']);
                                    $res_lname = get_value('reservations', 'res_id', 'res_lname', $row_res['reservations_id']);

                                    $bookingstatus_id = get_value('reservations', 'res_id', 'bookingstatus_id', $row_res['reservations_id']);

                                    $res_cash_collection = get_value('reservations', 'res_id', 'res_cash_collection', $row_res['reservations_id']);
                                    $res_agent_voucher = get_value('reservations', 'res_id', 'res_agent_voucher', $row_res['reservations_id']);
                                    $res_request = get_value('reservations', 'res_id', 'res_request', $row_res['reservations_id']);

                                    $pax = $row_res['rpt_adult_num'] + $row_res['rpt_child_num'];

                                    $res_id_str = get_value('reservations', 'res_id', 'res_id_str', $row_res['reservations_id']);

                                    if ($bookingstatus_id == 3) {
                                        ?>

                                        <tr>
                                            <td class="txt_bold_gray" align="center"
                                                bgcolor="#F0F0F0"><?= $res_id_str ?></td>
                                            <td class="txt_bold_gray"
                                                bgcolor="#F0F0F0"><?= $agent_name ?><?php if (isset($agent_name)) { ?>
                                                    <br/>(<?= $code_agent ?>) <?php } ?></td>
                                            <!--<td class="txt_bold_gray" bgcolor="#F0F0F0"><?= $res_book_by ?></td>-->
                                            <td class="txt_bold_gray"
                                                bgcolor="#F0F0F0"><?= $lis_name ?> <?= $res_fname ?> <?= $res_lname ?></td>
                                            <td class="txt_bold_gray" bgcolor="#F0F0F0"><?= $res_email ?></td>
                                            <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?= $pax ?></td>
                                            <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"></td>
                                            <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"></td>
                                            <td class="txt_bold_gray"
                                                bgcolor="#F0F0F0"><?= $row_res['rpt_pickupfrom'] ?></td>
                                            <td class="txt_bold_gray"
                                                bgcolor="#F0F0F0"><?= $row_res['rpt_pickupto'] ?></td>
                                            <!--<td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?= $row_res['rpt_room'] ?></td>
                <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?= $time_pickup ?></td>
                <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?= number_format($res_cash_collection) ?></td>
				<td class="txt_bold_gray" bgcolor="#F0F0F0"><?= $res_agent_voucher ?></td>-->
                                            <td class="txt_bold_gray" bgcolor="#F0F0F0"><?= nl2br($res_request) ?></td>

                                            <?php //if($agentpaytype_id != 2 || $agent_id == 0){
                                            ?>

                                            <!--<td class="txt_bold_gray" bgcolor="#F0F0F0"><?= number_format($row_res['rpt_prices']) ?></td>
                <td class="txt_bold_gray" bgcolor="#F0F0F0">&nbsp;</td>
                
                <?php //}else{
                                            ?>
                
                <td class="txt_bold_gray" bgcolor="#F0F0F0">&nbsp;</td>
                <td class="txt_bold_gray" bgcolor="#F0F0F0"><?= number_format($row_res['rpt_prices']) ?></td>
                
                <?php //}
                                            ?>
                
                <td class="txt_bold_gray" bgcolor="#F0F0F0">&nbsp;</td>-->
                                        </tr>

                                        <?php
                                        $total_pax = $total_pax + $pax;

                                    } // end if($bookingstatus_id == 3){ ?>

                                <?php } // end while($row_res = mysql_fetch_array($result_res)){

                            } // end while($row = mysql_fetch_array($results)){
                            ?>


                            <?php // Query Reservations Pick UP Transfer (Combo Product)

                            //for( $i = 1 ; $i <= $item_num ; $i++ ){

                            $sql_combo = "SELECT * FROM pickuptransfers WHERE area_id = '" . $_POST['area_id'] . "' ORDER BY put_id ASC";

                            $results_combo = mysql_query($sql_combo);
                            while ($row_combo = mysql_fetch_array($results_combo)) {

                                $sql_resc = "SELECT * ";
                                $sql_resc .= "FROM reservationpackage_item ";
                                $sql_resc .= "WHERE rpt_item_producttype_id_arr LIKE '%~2~%' ";
                                $sql_resc .= "AND rpt_item_id_arr LIKE '%~" . $row_combo['put_id'] . "~%' ";
                                $sql_resc .= "AND rpt_item_travel_date_arr LIKE '%~" . $_POST['from_date'] . "~%' ";
                                $sql_resc .= "ORDER BY rpt_id ASC ";

                                //echo $sql_resc;

                                $result_resc = mysql_query($sql_resc);
                                while ($row_resc = mysql_fetch_array($result_resc)) {
                                    ?>
                                    <?php
                                    $agent_id = get_value('reservation_packages', 'rpa_id', 'agents_id', $row_resc['reservationpackages_id']);
                                    $code_agent = get_value('agents', 'ag_id', 'ag_ref', $agent_id);
                                    $agent_name = get_value('agents', 'ag_id', 'ag_name', $agent_id);
                                    $rpa_email = get_value('reservation_packages', 'rpa_id', 'rpa_email', $row_resc['reservationpackages_id']);

                                    $res_book_by = get_value('reservation_packages', 'rpa_id', 'rpa_bookby', $row_resc['reservationpackages_id']);

                                    $titlename_id = get_value('reservation_packages', 'rpa_id', 'titlename_id', $row_resc['reservationpackages_id']);
                                    $lis_name = get_value('lis_titlename', 'lis_id', 'lis_name', $titlename_id);
                                    $rpa_fname = get_value('reservation_packages', 'rpa_id', 'rpa_fname', $row_resc['reservationpackages_id']);
                                    $rpa_lname = get_value('reservation_packages', 'rpa_id', 'rpa_lname', $row_resc['reservationpackages_id']);

                                    $bookingstatus_id = get_value('reservation_packages', 'rpa_id', 'bookingstatus_id', $row_resc['reservationpackages_id']);

                                    $ticket_type_id = get_value('packages', 'pac_id', 'ticket_type_id', $row_resc['packages_id']);
                                    $class_id = get_value('packages', 'pac_id', 'class_id', $row_resc['packages_id']);

                                    $ticket_type_name = get_value('lis_ticket_type', 'lis_id', 'lis_name', $ticket_type_id);
                                    $class_name = get_value('lis_class', 'lis_id', 'lis_name', $class_id);

                                    $rpa_cash_collection = get_value('reservation_packages', 'rpa_id', 'rpa_cash_collection', $row_resc['reservationpackages_id']);
                                    $rpa_agent_voucher = get_value('reservation_packages', 'rpa_id', 'rpa_agent_voucher', $row_resc['reservationpackages_id']);
                                    $rpa_request = get_value('reservation_packages', 'rpa_id', 'rpa_request', $row_resc['reservationpackages_id']);

                                    $pax = $row_resc['rpt_adult_num'] + $row_resc['rpt_child_num'];

                                    $rpa_id_str = get_value('reservation_packages', 'rpa_id', 'rpa_id_str', $row_resc['reservationpackages_id']);

                                    if ($bookingstatus_id == 3) {

                                        $sql_arr = "SELECT * ";
                                        $sql_arr .= "FROM reservationpackage_item ";
                                        $sql_arr .= "WHERE rpt_id = '" . $row_resc['rpt_id'] . "' ";
                                        $sql_arr .= "ORDER BY rpt_id ASC ";

                                        //echo $sql_arr;

                                        $result_arr = mysql_query($sql_arr);
                                        $row_arr = mysql_fetch_array($result_arr);

                                        $rpt_item_travel_date = explode("~", $row_arr['rpt_item_travel_date_arr']);
                                        $rpt_item_producttype_id = explode("~", $row_arr['rpt_item_producttype_id_arr']);
                                        $rpt_item_id = explode("~", $row_arr['rpt_item_id_arr']);

                                        $rpt_pickup_from = explode("~", $row_arr['rpt_pickup_from_arr']);
                                        $rpt_pickup_to = explode("~", $row_arr['rpt_pickup_to_arr']);
                                        $rpt_room_no = explode("~", $row_arr['rpt_room_no_arr']);

                                        for ($i = 1; $i < count($rpt_item_travel_date); $i++) {

                                            if ($rpt_item_travel_date[$i] == $_POST['from_date'] && $rpt_item_producttype_id[$i] == 2 && $rpt_item_id[$i] == $row_combo['put_id']) {
                                                //echo "from : ".$rpt_pickup_from[$i]."<br>";
                                                $pickup_from = $rpt_pickup_from[$i];

                                                //echo "to : ".$rpt_pickup_to[$i]."<br>";
                                                $pickup_to = $rpt_pickup_to[$i];

                                                //echo "room no. : ".$rpt_room_no[$i]."<br>";
                                                $room_no = $rpt_room_no[$i];
                                                ?>
                                                <tr>
                                                    <td class="txt_bold_gray" align="center"
                                                        bgcolor="#F0F0F0"><?= $rpa_id_str ?></td>
                                                    <td class="txt_bold_gray"
                                                        bgcolor="#F0F0F0"><?= $agent_name ?><?php if (isset($agent_name)) { ?>
                                                            <br/>(<?= $code_agent ?>) <?php } ?></td>
                                                    <!--<td class="txt_bold_gray" bgcolor="#F0F0F0"><?= $res_book_by ?></td>-->
                                                    <td class="txt_bold_gray"
                                                        bgcolor="#F0F0F0"><?= $lis_name ?> <?= $rpa_fname ?> <?= $rpa_lname ?></td>
                                                    <td class="txt_bold_gray" bgcolor="#F0F0F0"><?= $rpa_email ?></td>
                                                    <td class="txt_bold_gray" align="center"
                                                        bgcolor="#F0F0F0"><?= $pax ?></td>
                                                    <td class="txt_bold_gray" align="center"
                                                        bgcolor="#F0F0F0"><?= $class_name ?></td>
                                                    <td class="txt_bold_gray" align="center"
                                                        bgcolor="#F0F0F0"><?= $ticket_type_name ?></td>
                                                    <td class="txt_bold_gray" bgcolor="#F0F0F0"><?= $pickup_from ?></td>
                                                    <td class="txt_bold_gray" bgcolor="#F0F0F0"><?= $pickup_to ?></td>
                                                    <!--<td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?= $room_no ?></td>
							<td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?= $time_pickup ?></td>
							<td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?= number_format($rpa_cash_collection) ?></td>
							<td class="txt_bold_gray" bgcolor="#F0F0F0"><?= $rpa_agent_voucher ?></td>-->
                                                    <td class="txt_bold_gray"
                                                        bgcolor="#F0F0F0"><?= nl2br($rpa_request) ?></td>
                                                    <!--<td class="txt_bold_gray" bgcolor="#F0F0F0">&nbsp;</td>
                            <td class="txt_bold_gray" bgcolor="#F0F0F0">&nbsp;</td>
                            <td class="txt_bold_gray" bgcolor="#F0F0F0"><?= number_format($row_arr['rpt_prices']) ?></td>-->
                                                </tr>
                                                <?php
                                            }
                                        }

                                        ?>
                                        <?php
                                        $total_pax = $total_pax + $pax;

                                    } ?>

                                <?php }

                            }
                            ?>

                            <tr>
                                <td colspan="4" class="txt_bold_gray" align="right"><b>TOTAL</b></td>
                                <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?= $total_pax ?></td>
                                <td colspan="5" class="txt_bold_gray" align="center">&nbsp;</td>
                            </tr>

                        </table>

                        <!---- Search Box ---->
                    </td>
                    <td align="right" valign="bottom"></td>
                </tr>
            </table>

            <!---- Listing Body ---->
        </td>
    </tr>
</table>