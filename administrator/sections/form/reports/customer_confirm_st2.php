<?php

function setNumberLength($num, $length)
{
    $sumstr = strlen($num);
    $zero = str_repeat("0", $length - $sumstr);
    $results = $zero . $num;

    return $results;
}

?>

<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td valign="top">
            <table width="100%" border="0" cellspacing="0" cellpadding="3">
                <tr>
                    <td class="txt_big_gray">Customer Confirm</td>
                    <td width="500" align="right"><a href="./index.php?mode=reports/customer_confirm_st1"
                                                     style="background-color:#ffffff; color:#000000"><<< Back</a></td>
                </tr>
            </table>
            <!---- Listing Body ---->
            <table width="100%" border="0" cellspacing="0" cellpadding="3">
                <tr>
                    <td>
                        <!---- Search Box ---->

                        <table border="0" cellspacing="3" cellpadding="3" style="border: solid #CCCCCC 1px;">
                            <form name="formSt2" method="post" action="./index.php?mode=reports/customer_confirm_list">
                                <input type="hidden" name="producttype" value="<?= $_POST['producttype'] ?>"/>

                                <tr>
                                    <td class="txt_bold_gray" align="right">Product Type :</td>
                                    <td align="left"><?= $_POST['producttype'] ?></td>
                                </tr>

                                <tr>
                                    <td class="txt_bold_gray" align="right">Product :</td>
                                    <td align="left">
                                        <?php if ($_POST['producttype'] == "Boat Transfer") { ?>
                                            <select name="product_id">
                                                <option value="">-- Select --</option>
                                                <?php listbox('boattransfers', 'bot_id', 'bot_name', 'N', 'N'); ?>
                                            </select>
                                        <?php } ?>
                                        <?php if ($_POST['producttype'] == "Pick Up Transfer") { ?>
                                            <select name="product_id">
                                                <option value="">-- Select --</option>
                                                <?php listbox('pickuptransfers', 'put_id', 'put_name', 'N', 'N'); ?>
                                            </select>
                                        <?php } ?>
                                        <?php if ($_POST['producttype'] == "Tour") { ?>
                                            <select name="product_id">
                                                <option value="">-- Select --</option>
                                                <?php listbox('tours', 'tou_id', 'tou_name', 'N', 'N'); ?>
                                            </select>
                                        <?php } ?>
                                        <?php if ($_POST['producttype'] == "Activity") { ?>
                                            <select name="product_id">
                                                <option value="">-- Select --</option>
                                                <?php listbox('activities', 'act_id', 'act_name', 'N', 'N'); ?>
                                            </select>
                                        <?php } ?>
                                        <?php if ($_POST['producttype'] == "Bus Transfer") { ?>
                                            <select name="product_id">
                                                <option value="">-- Select --</option>
                                                <?php listbox('cartransfers', 'ct_id', 'ct_name', 'N', 'N'); ?>
                                            </select>
                                        <?php } ?>
                                        <?php if ($_POST['producttype'] == "Private Land Transfer") { ?>
                                            <select name="product_id">
                                                <option value="">-- Select --</option>
                                                <?php listbox('privatelandtransfers', 'plt_id', 'plt_name', 'N', 'N'); ?>
                                            </select>
                                        <?php } ?>
                                        <?php if ($_POST['producttype'] == "Hotel") { ?>
                                            <select name="product_id">
                                                <option value="">-- Select --</option>
                                                <?php listbox('hotels', 'hot_id', 'hot_name', 'N', 'N'); ?>
                                            </select>
                                        <?php } ?>
                                        <?php if ($_POST['producttype'] == "Train Transfer") { ?>
                                            <select name="product_id">
                                                <option value="">-- Select --</option>
                                                <?php listbox('traintransfers', 'train_id', 'train_name', 'N', 'N'); ?>
                                            </select>
                                        <?php } ?>
                                    </td>
                                </tr>

                                <tr>
                                    <td class="txt_bold_gray" align="right">From Date :</td>
                                    <td align="left">
                                        <script>DateInput('from_date', true, 'yyyy-mm-dd' <?php if (isset($from_date)) {
                                                echo ",'$from_date'";
                                            }?>)</script>
                                    </td>
                                </tr>

                                <tr>
                                    <td class="txt_bold_gray" align="right">To Date :</td>
                                    <td align="left">
                                        <script>DateInput('to_date', true, 'yyyy-mm-dd' <?php if (isset($to_date)) {
                                                echo ",'$to_date'";
                                            }?>)</script>
                                    </td>
                                </tr>

                                <tr>
                                    <td class="txt_bold_gray" align="right">Payment/Booking :</td>
                                    <td align="left">
                                        <input type="radio" name="status" value="1" checked="checked">Payment Status :
                                        Paid/Invoice<br/>
                                        <input type="radio" name="status" value="2">Booking Status : Confirm
                                    </td>
                                </tr>

                                <tr>
                                    <td class="txt_bold_gray" align="right">&nbsp;</td>
                                    <td align="right"><input type="submit" name="Submit" value="SEARCH"
                                                             style="height:25px;"></td>
                                </tr>
                            </form>
                        </table>
                        <!---- Search Box ---->
                    </td>
                    <td align="right" valign="bottom"></td>
                </tr>
            </table>

            <!---- Listing Body ---->
        </td>
    </tr>
</table>