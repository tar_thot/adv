<?php

function setNumberLength($num, $length)
{
    $sumstr = strlen($num);
    $zero = str_repeat("0", $length - $sumstr);
    $results = $zero . $num;

    return $results;
}

$sql = "SELECT * ";
$sql .= "FROM boattransfers ";
$sql .= "WHERE bot_id = '$_POST[product_id]' ";

$sql .= "ORDER BY bot_id ASC ";

$results = mysql_query($sql);
$row = mysql_fetch_array($results);

?>

<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td valign="top">
            <table width="100%" border="0" cellspacing="0" cellpadding="3">
                <tr>
                    <td class="txt_big_gray">Pax by Rate Type</td>
                    <td width="500" align="right"><a href="./index.php?mode=reports/pax_by_ratetype_st1"
                                                     style="background-color:#ffffff; color:#000000"><<< Back</a></td>
                </tr>
            </table>
            <!---- Listing Body ---->
            <table width="100%" border="0" cellspacing="0" cellpadding="3">
                <tr>
                    <td>
                        <!---- Search Box ---->

                        <table border="0" cellspacing="3" cellpadding="3" style="border: solid #CCCCCC 1px;">
                            <form name="formSt2" method="post" action="./index.php?mode=reports/pax_by_ratetype_list">
                                <input type="hidden" name="product_id" value="<?= $_POST['product_id'] ?>"/>

                                <tr>
                                    <td class="txt_bold_gray" align="right">Product :</td>
                                    <td align="left"><?= $row['bot_name'] ?></td>
                                </tr>

                                <tr>
                                    <td class="txt_bold_gray" align="right">Rate type :</td>
                                    <td align="left">
                                        <select name="botrt_id" style="width:200px;">
                                            <option value="">-- Select --</option>
                                            <?php listbox('boattransfer_ratetypes', 'botrt_id', 'botrt_name', 'N', "boattransfers_id = '" . $row['bot_id'] . "'"); ?>
                                        </select>
                                    </td>
                                </tr>

                                <tr>
                                    <td class="txt_bold_gray" align="right">From Date :</td>
                                    <td align="left">
                                        <script>DateInput('from_date', true, 'yyyy-mm-dd' <?php if (isset($from_date)) {
                                                echo ",'$from_date'";
                                            }?>)</script>
                                    </td>
                                </tr>


                                <tr>
                                    <td class="txt_bold_gray" align="right">&nbsp;</td>
                                    <td align="right"><input type="submit" name="Submit" value="SEARCH"
                                                             style="height:25px;"></td>
                                </tr>
                            </form>
                        </table>
                        <!---- Search Box ---->
                    </td>
                    <td align="right" valign="bottom"></td>
                </tr>
            </table>

            <!---- Listing Body ---->
        </td>
    </tr>
</table>