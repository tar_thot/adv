<?php

function setNumberLength($num, $length)
{
    $sumstr = strlen($num);
    $zero = str_repeat("0", $length - $sumstr);
    $results = $zero . $num;

    return $results;
}

?>

<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td valign="top">
            <table width="100%" border="0" cellspacing="0" cellpadding="3">
                <tr>
                    <td class="txt_big_gray">Daily Passenger</td>
                </tr>
            </table>
            <!---- Listing Body ---->
            <table width="100%" border="0" cellspacing="0" cellpadding="3">
                <tr>
                    <td>
                        <!---- Search Box ---->

                        <table border="0" cellspacing="3" cellpadding="3" style="border: solid #CCCCCC 1px;">
                            <form name="formSt1" method="post" action="./index.php?mode=reports/daily_passenger_list">

                                <tr>
                                    <td class="txt_bold_gray" align="right">Boat Transfer (Standard Class) :</td>
                                    <td align="left">
                                        <select name="product_sd_id">
                                            <option value="">-- Select --</option>
                                            <?php listbox('boattransfers', 'bot_id', 'bot_name', 'N', 'N'); ?>
                                        </select>
                                    </td>
                                </tr>

                                <tr>
                                    <td class="txt_bold_gray" align="right">Boat Transfer (Premium Class) :</td>
                                    <td align="left">
                                        <select name="product_pr_id">
                                            <option value="">-- Select --</option>
                                            <?php listbox('boattransfers', 'bot_id', 'bot_name', 'N', 'N'); ?>
                                        </select>
                                    </td>
                                </tr>

                                <tr>
                                    <td class="txt_bold_gray" align="right">Boat Transfer (First Class) :</td>
                                    <td align="left">
                                        <select name="product_fc_id">
                                            <option value="">-- Select --</option>
                                            <?php listbox('boattransfers', 'bot_id', 'bot_name', 'N', 'N'); ?>
                                        </select>
                                    </td>
                                </tr>

                                <tr>
                                    <td class="txt_bold_gray" align="right">Date :</td>
                                    <td align="left">
                                        <script>DateInput('from_date', true, 'yyyy-mm-dd' <?php if (isset($from_date)) {
                                                echo ",'$from_date'";
                                            }?>)</script>
                                    </td>
                                </tr>

                                <tr>
                                    <td class="txt_bold_gray" align="right">Company :</td>
                                    <td align="left">
                                        <select name="company">
                                            <option value="">-- Select --</option>
                                            <?php listbox('lis_company', 'lis_id', 'lis_name', 'N', 'N'); ?>
                                        </select>
                                    </td>
                                </tr>

                                <tr>
                                    <td class="txt_bold_gray" align="right">&nbsp;</td>
                                    <td align="right"><input type="submit" name="Submit" value="SEARCH"
                                                             style="height:25px;"></td>
                                </tr>
                            </form>
                        </table>
                        <!---- Search Box ---->
                    </td>
                    <td align="right" valign="bottom"></td>
                </tr>
            </table>

            <!---- Listing Body ---->
        </td>
    </tr>
</table>