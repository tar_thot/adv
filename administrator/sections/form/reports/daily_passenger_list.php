<?php

function setNumberLength($num, $length)
{
    $sumstr = strlen($num);
    $zero = str_repeat("0", $length - $sumstr);
    $results = $zero . $num;

    return $results;
}

function DateDiff($strDate1, $strDate2)
{
    return (strtotime($strDate2) - strtotime($strDate1)) / (60 * 60 * 24);  // 1 day = 60*60*24
}

?>

<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td valign="top">
            <table width="100%" border="0" cellspacing="0" cellpadding="3">
                <tr>
                    <td class="txt_big_gray">Daily Passenger List</td>
                    <td width="500" align="right"><a href="./index.php?mode=reports/daily_passenger_st1"
                                                     style="background-color:#ffffff; color:#000000"><<< Back</a></td>
                </tr>
            </table>
            <!---- Listing Body ---->
            <table width="100%" border="0" cellspacing="0" cellpadding="3">
                <tr>
                    <td>
                        <!---- Search Box ---->
                        <br/>

                        <table align="center">
                            <tr>
                                <td class="txt_bold_gray" align="center">
                                    <?php if ($_POST['product_sd_id']) {
                                        echo get_value('boattransfers', 'bot_id', 'bot_name', $_POST['product_sd_id']);
                                    } ?>
                                    <?php if ($_POST['product_sd_id'] && $_POST['product_pr_id']) {
                                        echo " , ";
                                    } ?>
                                    <?php if ($_POST['product_pr_id']) {
                                        echo get_value('boattransfers', 'bot_id', 'bot_name', $_POST['product_pr_id']);
                                    } ?>
                                    <?php if ($_POST['product_pr_id'] && $_POST['product_fc_id']) {
                                        echo " , ";
                                    } ?>
                                    <?php if ($_POST['product_fc_id']) {
                                        echo get_value('boattransfers', 'bot_id', 'bot_name', $_POST['product_fc_id']);
                                    } ?></td>
                            </tr>
                        </table>

                        <br/>

                        <table border="1" cellspacing="0" cellpadding="3" bgcolor="#FFFFFF" bordercolor="#000000">
                            <tr bgcolor="#CCCCCC">
                                <td width="20" rowspan="2" align="center" class="txt_bold_gray">CODE</td>
                                <td width="500" rowspan="2" class="txt_bold_gray" align="center">AGENT</td>
                                <td width="500" rowspan="2" class="txt_bold_gray" align="center">BY</td>
                                <td width="500" rowspan="2" class="txt_bold_gray" align="center">PASSENGER'S NAME</td>
                                <td width="500" rowspan="2" class="txt_bold_gray" align="center">PASSENGER'S EMAIL</td>
                                <td width="50" rowspan="2" class="txt_bold_gray" align="center">TYPE</td>
                                <td width="50" rowspan="2" class="txt_bold_gray" align="center">CLASS</td>
                                <td width="100" rowspan="2" class="txt_bold_gray" align="center">TICKET NO.</td>
                                <td width="20" rowspan="2" class="txt_bold_gray" align="center">ADULT</td>
                                <td width="20" rowspan="2" class="txt_bold_gray" align="center">CHILD</td>
                                <td width="20" rowspan="2" class="txt_bold_gray" align="center">INFANT</td>
                                <td width="100" rowspan="2" class="txt_bold_gray" align="center">TRAVEL DATE</td>
                                <td width="100" rowspan="2" class="txt_bold_gray" align="center">VOUCHER</td>
                                <td width="100" rowspan="2" class="txt_bold_gray" align="center">CASH COLLECTION</td>
                                <td width="200" rowspan="2" class="txt_bold_gray" align="center">REMARK</td>
                                <td width="200" colspan="3" class="txt_bold_gray" align="center">PRICE</td>
                            </tr>
                            <tr bgcolor="#CCCCCC">
                                <td width="100" class="txt_bold_gray" align="center">Cash</td>
                                <td width="100" class="txt_bold_gray" align="center">Credit</td>
                                <td width="100" class="txt_bold_gray" align="center">Combo</td>
                            </tr>

                            <?php // Query Reservations Boat Transfer

                            $sql_res = "SELECT * ";
                            $sql_res .= "FROM reservation_boattransfer_items ";
                            $sql_res .= "WHERE rbt_travel_date = '" . $_POST['from_date'] . "' ";
                            $sql_res .= "AND (boattransfers_id = '" . $_POST['product_sd_id'] . "' ";
                            $sql_res .= "OR boattransfers_id = '" . $_POST['product_pr_id'] . "' ";
                            $sql_res .= "OR boattransfers_id = '" . $_POST['product_fc_id'] . "' ";

                            $sql_res .= "ORDER BY rbt_id ASC ";

                            $result_res = mysql_query($sql_res);
                            while ($row_res = mysql_fetch_array($result_res)) {
                                ?>
                                <?php
                                $bot_name = get_value('boattransfers', 'bot_id', 'bot_name', $row_res['boattransfers_id']);
                                $bot_class = get_value('boattransfers', 'bot_id', 'boattransfer_class_id', $row_res['boattransfers_id']);
                                $class = get_value('boattransfer_class', 'c_id', 'c_name', $bot_class);

                                $bookingstatus_id = get_value('reservations', 'res_id', 'bookingstatus_id', $row_res['reservations_id']);
                                $company = get_value('reservations', 'res_id', 'company', $row_res['reservations_id']);

                                $agent_id = get_value('reservations', 'res_id', 'agents_id', $row_res['reservations_id']);
                                $code_agent = get_value('agents', 'ag_id', 'ag_ref', $agent_id);
                                $agent_name = get_value('agents', 'ag_id', 'ag_name', $agent_id);

                                $agentpaytype_id = get_value('agents', 'ag_id', 'agentpaytype_id', $agent_id);
                                $res_email = get_value('reservations', 'res_id', 'res_email', $row_res['reservations_id']);

                                $res_book_by = get_value('reservations', 'res_id', 'res_book_by', $row_res['reservations_id']);

                                $titlename_id = get_value('reservations', 'res_id', 'titlename_id', $row_res['reservations_id']);
                                $lis_name = get_value('lis_titlename', 'lis_id', 'lis_name', $titlename_id);
                                $res_fname = get_value('reservations', 'res_id', 'res_fname', $row_res['reservations_id']);
                                $res_lname = get_value('reservations', 'res_id', 'res_lname', $row_res['reservations_id']);

                                $bookingstatus_id = get_value('reservations', 'res_id', 'bookingstatus_id', $row_res['reservations_id']);

                                $res_cash_collection = get_value('reservations', 'res_id', 'res_cash_collection', $row_res['reservations_id']);
                                $res_agent_voucher = get_value('reservations', 'res_id', 'res_agent_voucher', $row_res['reservations_id']);
                                $res_request = get_value('reservations', 'res_id', 'res_request', $row_res['reservations_id']);

                                $res_id_str = get_value('reservations', 'res_id', 'res_id_str', $row_res['reservations_id']);

                                # Voucher

                                $sql_vo = "SELECT * ";
                                $sql_vo .= "FROM voucher ";
                                $sql_vo .= "WHERE vo_res_id = '$res_id_str' ";

                                $sql_vo .= "AND vo_status = '2' ";
                                $sql_vo .= "AND vo_item_id = '$bot_name' ";

                                $sql_vo .= "ORDER BY vo_id ASC ";

                                $result_vo = mysql_query($sql_vo);
                                $row_vo = mysql_fetch_array($result_vo);

                                if ($bookingstatus_id == 3 && $company == $_POST['company']) {
                                    ?>

                                    <tr>
                                        <td class="txt_bold_gray" align="center"
                                            bgcolor="#F0F0F0"><?= $res_id_str ?></td>
                                        <td class="txt_bold_gray"
                                            bgcolor="#F0F0F0"><?= $agent_name ?><?php if ($agent_name) { ?>
                                                <br/>(<?= $code_agent ?>) <?php } ?></td>
                                        <td class="txt_bold_gray" bgcolor="#F0F0F0"><?= $res_book_by ?></td>
                                        <td class="txt_bold_gray"
                                            bgcolor="#F0F0F0"><?= $lis_name ?> <?= $res_fname ?> <?= $res_lname ?></td>
                                        <td class="txt_bold_gray" bgcolor="#F0F0F0"><?= $res_email ?></td>
                                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0">OW</td>
                                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0">
                                            <?php if ($class == "Standard Class") {
                                                echo "SD";
                                            } ?>
                                            <?php if ($class == "First Class") {
                                                echo "FC";
                                            } ?>
                                            <?php if ($class == "Premium Class") {
                                                echo "PR";
                                            } ?>    </td>
                                        <td class="txt_bold_gray" align="center"
                                            bgcolor="#F0F0F0"><?= $row_res['rbt_ticket'] ?></td>
                                        <td class="txt_bold_gray" align="center"
                                            bgcolor="#F0F0F0"><?= $row_res['rbt_adult_num'] ?></td>
                                        <td class="txt_bold_gray" align="center"
                                            bgcolor="#F0F0F0"><?= $row_res['rbt_child_num'] ?></td>
                                        <td class="txt_bold_gray" align="center"
                                            bgcolor="#F0F0F0"><?= $row_res['rbt_infant_num'] ?></td>
                                        <td class="txt_bold_gray" align="center"
                                            bgcolor="#F0F0F0"><?= DateFormat($row_res['rbt_travel_date'], "s") ?></td>
                                        <!--<td class="txt_bold_gray" bgcolor="#F0F0F0"><?= $row_res['rpt_pickupfrom'] ?></td>-->

                                        <?php if ($agentpaytype_id != 2) { ?>
                                            <td class="txt_bold_gray" bgcolor="#F0F0F0"><?= $row_vo['vo_no'] ?></td>
                                        <?php } else { ?>
                                            <td class="txt_bold_gray"
                                                bgcolor="#F0F0F0"><?= $res_agent_voucher ?></td> <?php } ?>

                                        <td class="txt_bold_gray" align="center"
                                            bgcolor="#F0F0F0"><?= number_format($res_cash_collection) ?></td>
                                        <td class="txt_bold_gray" bgcolor="#F0F0F0"><?= nl2br($res_request) ?></td>

                                        <?php if ($agentpaytype_id != 2 || $agent_id == 0) { ?>

                                            <td class="txt_bold_gray"
                                                bgcolor="#F0F0F0"><?= number_format($row_res['rbt_prices']) ?></td>
                                            <td class="txt_bold_gray" bgcolor="#F0F0F0">&nbsp;</td>

                                            <?php $total_cash = $total_cash + $row_res['rbt_prices']; ?>

                                        <?php } else { ?>

                                            <td class="txt_bold_gray" bgcolor="#F0F0F0">&nbsp;</td>
                                            <td class="txt_bold_gray"
                                                bgcolor="#F0F0F0"><?= number_format($row_res['rbt_prices']) ?></td>

                                            <?php $total_credit = $total_credit + $row_res['rbt_prices']; ?>

                                        <?php } ?>

                                        <td class="txt_bold_gray" bgcolor="#F0F0F0">&nbsp;</td>
                                    </tr>

                                    <?php
                                    $total_adult = $total_adult + $row_res['rbt_adult_num'];
                                    $total_child = $total_child + $row_res['rbt_child_num'];
                                    $total_infant = $total_infant + $row_res['rbt_infant_num'];
                                    $total_collection = $total_collection + $res_cash_collection;

                                } // END if($bookingstatus_id == 3 && $company == $_POST['company']){
                                ?>

                            <?php } // END while($row_res = mysql_fetch_array($result_res)){ ?>


                            <?php // Query Reservations Boat Transfer (Combo Product)

                            $sql_resc = "SELECT * ";
                            $sql_resc .= "FROM reservationpackage_item ";
                            $sql_resc .= "WHERE rpt_item_producttype_id_arr LIKE '%~1~%' ";
                            $sql_resc .= "AND rpt_item_travel_date_arr LIKE '%~" . $_POST['from_date'] . "~%' ";
                            $sql_resc .= "AND (rpt_item_id_arr LIKE '%~" . $_POST['product_sd_id'] . "~%' ";
                            $sql_resc .= "OR rpt_item_id_arr LIKE '%~" . $_POST['product_pr_id'] . "~%' ";
                            $sql_resc .= "OR rpt_item_id_arr LIKE '%~" . $_POST['product_fc_id'] . "~%') ";

                            $sql_resc .= "ORDER BY rpt_id ASC ";

                            //echo $sql_resc;

                            $result_resc = mysql_query($sql_resc);
                            while ($row_resc = mysql_fetch_array($result_resc)) {
                                ?>
                                <?php
                                $company = get_value('reservation_packages', 'rpa_id', 'company', $row_resc['reservationpackages_id']);

                                $ticket_type_id = get_value('packages', 'pac_id', 'ticket_type_id', $row_resc['packages_id']);

                                $ticket_type = get_value('lis_ticket_type', 'lis_id', 'lis_name', $ticket_type_id);

                                $agent_id = get_value('reservation_packages', 'rpa_id', 'agents_id', $row_resc['reservationpackages_id']);
                                $code_agent = get_value('agents', 'ag_id', 'ag_ref', $agent_id);
                                $agent_name = get_value('agents', 'ag_id', 'ag_name', $agent_id);
                                $agentpaytype_id = get_value('agents', 'ag_id', 'agentpaytype_id', $agent_id);
                                $rpa_email = get_value('reservation_packages', 'rpa_id', 'rpa_email', $row_resc['reservationpackages_id']);

                                $res_book_by = get_value('reservation_packages', 'rpa_id', 'rpa_bookby', $row_resc['reservationpackages_id']);

                                $titlename_id = get_value('reservation_packages', 'rpa_id', 'titlename_id', $row_resc['reservationpackages_id']);
                                $lis_name = get_value('lis_titlename', 'lis_id', 'lis_name', $titlename_id);
                                $rpa_fname = get_value('reservation_packages', 'rpa_id', 'rpa_fname', $row_resc['reservationpackages_id']);
                                $rpa_lname = get_value('reservation_packages', 'rpa_id', 'rpa_lname', $row_resc['reservationpackages_id']);

                                $bookingstatus_id = get_value('reservation_packages', 'rpa_id', 'bookingstatus_id', $row_resc['reservationpackages_id']);

                                $class_id = get_value('packages', 'pac_id', 'class_id', $row_resc['packages_id']);

                                $ticket_type_name = get_value('lis_ticket_type', 'lis_id', 'lis_name', $ticket_type_id);
                                $class_name = get_value('lis_class', 'lis_id', 'lis_name', $class_id);

                                $rpa_cash_collection = get_value('reservation_packages', 'rpa_id', 'rpa_cash_collection', $row_resc['reservationpackages_id']);
                                $rpa_agent_voucher = get_value('reservation_packages', 'rpa_id', 'rpa_agent_voucher', $row_resc['reservationpackages_id']);
                                $rpa_request = get_value('reservation_packages', 'rpa_id', 'rpa_request', $row_resc['reservationpackages_id']);

                                $rpa_id_str = get_value('reservation_packages', 'rpa_id', 'rpa_id_str', $row_resc['reservationpackages_id']);

                                if ($bookingstatus_id == 3 && $company == $_POST['company']) {

                                    $sql_arr = "SELECT * ";
                                    $sql_arr .= "FROM reservationpackage_item ";
                                    $sql_arr .= "WHERE rpt_id = '" . $row_resc['rpt_id'] . "' ";
                                    $sql_arr .= "ORDER BY rpt_id ASC ";

                                    //echo $sql_arr;

                                    $result_arr = mysql_query($sql_arr);
                                    $row_arr = mysql_fetch_array($result_arr);

                                    $rpt_item_name = explode("~", $row_arr['rpt_item_name_arr']);
                                    $rpt_item_travel_date = explode("~", $row_arr['rpt_item_travel_date_arr']);
                                    $rpt_item_producttype_id = explode("~", $row_arr['rpt_item_producttype_id_arr']);
                                    $rpt_item_id = explode("~", $row_arr['rpt_item_id_arr']);

                                    //$rpt_pickup_from = explode("~",'$row_arr['rpt_pickup_from_arr]);
                                    //$rpt_pickup_to = explode("~",$row_arr['rpt_pickup_to_arr]);
                                    //$rpt_room_no = explode("~",$row_arr['rpt_room_no_arr]);

                                    for ($i = 1; $i < count($rpt_item_travel_date); $i++) {

                                        if ($rpt_item_travel_date[$i] == $_POST['from_date'] && $rpt_item_producttype_id[$i] == 1) {
                                            if ($rpt_item_id[$i] == $_POST['product_sd_id'] || $rpt_item_id[$i] == $_POST['product_pr_id'] || $rpt_item_id[$i] == $_POST['product_fc_id']) {

                                                $bot_class = get_value('boattransfers', 'bot_id', 'boattransfer_class_id', $rpt_item_id[$i]);
                                                $class = get_value('boattransfer_class', 'c_id', 'c_name', $bot_class);

                                                $item_name = $rpt_item_name[$i];

                                                # Voucher

                                                $sql_vo = "SELECT * ";
                                                $sql_vo .= "FROM voucher ";
                                                $sql_vo .= "WHERE vo_res_id = '" . $rpa_id_str . "' ";

                                                $sql_vo .= "AND vo_status = '2' ";
                                                $sql_vo .= "AND vo_item_id = '" . $item_name . "' ";

                                                $sql_vo .= "ORDER BY vo_id ASC ";

                                                $result_vo = mysql_query($sql_vo);
                                                $row_vo = mysql_fetch_array($result_vo);

                                                # end Voucher
                                                ?>
                                                <tr>
                                                    <td class="txt_bold_gray" align="center"
                                                        bgcolor="#F0F0F0"><?= $rpa_id_str ?></td>
                                                    <td class="txt_bold_gray"
                                                        bgcolor="#F0F0F0"><?= $agent_name ?><?php if ($agent_name) { ?>
                                                            <br/>(<?= $code_agent ?>) <?php } ?></td>
                                                    <td class="txt_bold_gray" bgcolor="#F0F0F0"><?= $res_book_by ?></td>
                                                    <td class="txt_bold_gray"
                                                        bgcolor="#F0F0F0"><?= $lis_name ?> <?= $rpa_fname ?> <?= $rpa_lname ?></td>
                                                    <td class="txt_bold_gray" bgcolor="#F0F0F0"><?= $rpa_email ?></td>
                                                    <td class="txt_bold_gray" align="center"
                                                        bgcolor="#F0F0F0"><?= $ticket_type ?></td>
                                                    <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0">
                                                        <?php if ($class == "Standard Class") {
                                                            echo "SD";
                                                        } ?>
                                                        <?php if ($class == "First Class") {
                                                            echo "FC";
                                                        } ?>
                                                        <?php if ($class == "Premium Class") {
                                                            echo "PR";
                                                        } ?>    </td>
                                                    <td class="txt_bold_gray" align="center"
                                                        bgcolor="#F0F0F0"><?= $row_arr['rpt_ticket_no'] ?></td>
                                                    <td class="txt_bold_gray" align="center"
                                                        bgcolor="#F0F0F0"><?= $row_arr['rpt_adult_num'] ?></td>
                                                    <td class="txt_bold_gray" align="center"
                                                        bgcolor="#F0F0F0"><?= $row_arr['rpt_child_num'] ?></td>
                                                    <td class="txt_bold_gray" align="center"
                                                        bgcolor="#F0F0F0"><?= $row_arr['rpt_infant_num'] ?></td>
                                                    <td class="txt_bold_gray" align="center"
                                                        bgcolor="#F0F0F0"><?= DateFormat($rpt_item_travel_date[$i], "s") ?></td>

                                                    <?php if ($agentpaytype_id != 2) { ?>
                                                        <td class="txt_bold_gray"
                                                            bgcolor="#F0F0F0"><?= $row_vo['vo_no'] ?></td>
                                                    <?php } else { ?>
                                                        <td class="txt_bold_gray"
                                                            bgcolor="#F0F0F0"><?= $rpa_agent_voucher ?></td> <?php } ?>

                                                    <td class="txt_bold_gray" align="center"
                                                        bgcolor="#F0F0F0"><?= number_format($rpa_cash_collection) ?></td>
                                                    <td class="txt_bold_gray"
                                                        bgcolor="#F0F0F0"><?= nl2br($rpa_request) ?></td>
                                                    <td class="txt_bold_gray" bgcolor="#F0F0F0">&nbsp;</td>
                                                    <td class="txt_bold_gray" bgcolor="#F0F0F0">&nbsp;</td>
                                                    <td class="txt_bold_gray"
                                                        bgcolor="#F0F0F0"><?= number_format($row_arr['rpt_prices']) ?></td>
                                                </tr>
                                                <?php
                                            }
                                        }
                                    }
                                    ?>

                                    <?php
                                    $total_adult = $total_adult + $row_arr['rpt_adult_num'];
                                    $total_child = $total_child + $row_arr['rpt_child_num'];
                                    $total_infant = $total_infant + $row_arr['rpt_infant_num'];
                                    $total_collection = $total_collection + $rpa_cash_collection;
                                    $total_combo = $total_combo + $row_arr['rpt_prices'];

                                } ?>

                            <?php } ?>

                            <tr>
                                <td colspan="8" class="txt_bold_gray" align="right"><b>TOTAL</b></td>
                                <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?= $total_adult ?></td>
                                <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?= $total_child ?></td>
                                <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?= $total_infant ?></td>
                                <td colspan="2" class="txt_bold_gray" align="center">&nbsp;</td>
                                <td class="txt_bold_gray" align="center"
                                    bgcolor="#F0F0F0"><?= number_format($total_collection) ?>
                                <td class="txt_bold_gray" align="center">&nbsp;</td>
                                <td class="txt_bold_gray" align="center"
                                    bgcolor="#F0F0F0"><?= number_format($total_cash) ?></td>
                                <td class="txt_bold_gray" align="center"
                                    bgcolor="#F0F0F0"><?= number_format($total_credit) ?></td>
                                <td class="txt_bold_gray" align="center"
                                    bgcolor="#F0F0F0"><?= number_format($total_combo) ?>
                            </tr>

                        </table>

                        <!---- Search Box ---->
                    </td>
                    <td align="right" valign="bottom"></td>
                </tr>
            </table>

            <!---- Listing Body ---->
        </td>
    </tr>
</table>