<?php

function setNumberLength($num, $length)
{
    $sumstr = strlen($num);
    $zero = str_repeat("0", $length - $sumstr);
    $results = $zero . $num;

    return $results;
}

?>

<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td valign="top">
            <table width="100%" border="0" cellspacing="0" cellpadding="3">
                <tr>
                    <td class="txt_big_gray">Customer Confirm</td>
                </tr>
            </table>
            <!---- Listing Body ---->
            <table width="100%" border="0" cellspacing="0" cellpadding="3">
                <tr>
                    <td>
                        <!---- Search Box ---->
                        <form name="formSt1" method="post" action="./index.php?mode=reports/customer_confirm_st2">
                            <table border="0" cellspacing="3" cellpadding="3" style="border: solid #CCCCCC 1px;">
                                <tr>
                                    <td class="txt_bold_gray" align="right">Product Type<br/>
                                        <select name="producttype" style="width:155px;">
                                            <option value="">-- Select --</option>
                                            <option value="Boat Transfer">Boat Transfer</option>
                                            <option value="Pick Up Transfer">Pick Up Transfer</option>
                                            <option value="Tour">Tour</option>
                                        </select></td>
                                    <td class="txt_bold_gray" align="right">&nbsp;</td>
                                    <td class="txt_bold_gray" align="right">&nbsp;</td>
                                </tr>

                                <tr>
                                    <td class="txt_bold_gray" align="right">&nbsp;</td>
                                    <td class="txt_bold_gray" align="right">&nbsp;</td>
                                    <td class="txt_bold_gray" align="right">&nbsp;</td>
                                    <td class="txt_bold_gray" align="right"><input type="submit" name="Submit"
                                                                                   value="Continue"
                                                                                   style="height:25px;"></td>
                                </tr>
                            </table>
                        </form>

                        <!---- Search Box ---->
                    </td>
                    <td align="right" valign="bottom"></td>
                </tr>
            </table>

            <!---- Listing Body ---->
        </td>
    </tr>
</table>