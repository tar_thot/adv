<?php

function setNumberLength($num, $length)
{
    $sumstr = strlen($num);
    $zero = str_repeat("0", $length - $sumstr);
    $results = $zero . $num;

    return $results;
}

function DateDiff($strDate1, $strDate2)
{
    return (strtotime($strDate2) - strtotime($strDate1)) / (60 * 60 * 24);  // 1 day = 60*60*24
}

?>

<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td valign="top">
            <table width="100%" border="0" cellspacing="0" cellpadding="3">
                <tr>
                    <td class="txt_big_gray">Customer Confirm List</td>
                    <td width="500" align="right"><a href="./index.php?mode=reports/customer_confirm_st1"
                                                     style="background-color:#ffffff; color:#000000"><<< Back</a></td>
                </tr>
            </table>
            <!---- Listing Body ---->
            <table width="100%" border="0" cellspacing="0" cellpadding="3">
                <tr>
                    <td>
                        <!---- Search Box ---->
                        <br/>

                        <?php //$strNewDate = date("Y-m-d", strtotime("+3 day", strtotime($_POST[to_date]))); ?>

                        <table align="center">
                            <tr>
                                <?php if ($_POST[producttype] == "Boat Transfer") {
                                    $producttype = 1;
                                    $sql = "SELECT * FROM boattransfers WHERE bot_id = '$_POST[product_id]' ORDER BY bot_id ASC";
                                    $results = mysql_query($sql);
                                    $row = mysql_fetch_array($results);
                                    ?>
                                    <td class="txt_bold_gray" align="center"><?= $row[bot_name] ?> -
                                        (<?= DateFormat($_POST[from_date], "f") ?>
                                        - <?= DateFormat($_POST[to_date], "f") ?>)
                                    </td>    <?php } ?>

                                <?php if ($_POST[producttype] == "Pick Up Transfer") {
                                    $producttype = 2;
                                    $sql = "SELECT * FROM pickuptransfers WHERE put_id = '$_POST[product_id]' ORDER BY put_id ASC";
                                    $results = mysql_query($sql);
                                    $row = mysql_fetch_array($results);
                                    ?>
                                    <td class="txt_bold_gray" align="center"><?= $row[put_name] ?> -
                                        (<?= DateFormat($_POST[from_date], "f") ?>
                                        - <?= DateFormat($_POST[to_date], "f") ?>)
                                    </td>    <?php } ?>

                                <?php if ($_POST[producttype] == "Tour") {
                                    $producttype = 3;
                                    $sql = "SELECT * FROM tours WHERE tou_id = '$_POST[product_id]' ORDER BY tou_id ASC";
                                    $results = mysql_query($sql);
                                    $row = mysql_fetch_array($results);
                                    ?>
                                    <td class="txt_bold_gray" align="center"><?= $row[tou_name] ?> -
                                        (<?= DateFormat($_POST[from_date], "f") ?>
                                        - <?= DateFormat($_POST[to_date], "f") ?>)
                                    </td>    <?php } ?>

                            </tr>
                        </table>


                        <?php // Reservation Combo Product ?>

                        <br/>


                        <table border="0" cellspacing="0" cellpadding="3" bgcolor="#FFFFFF" bordercolor="#000000">
                            <tr bgcolor="#CCCCCC">
                                <td width="20" class="txt_bold_gray" align="center">No.</td>
                                <td width="500" class="txt_bold_gray" align="center">AGENT</td>

                                <?php $strnodate = $_POST[from_date]; ?>

                                <?php for ($i = 0; $i <= DateDiff("$_POST[from_date]", "$_POST[to_date]"); $i++) { ?>

                                    <td width="40" class="txt_bold_gray"
                                        align="center"><?= date("j", strtotime($strnodate)) ?></td>

                                    <?php $strnodate = date("Y-m-d", strtotime("+1 day", strtotime($strnodate))); ?>

                                <?php } ?>

                            </tr>

                            <?php
                            $i = 1;
                            $x = 1;
                            $y = 1;

                            $strfromdate = $_POST[from_date];
                            $strtodate = $_POST[to_date];

                            // Query Agents
                            $sql = "SELECT * ";
                            $sql .= "FROM agents ";
                            $sql .= "ORDER BY ag_name ASC ";

                            $results = mysql_query($sql);
                            while ($row = mysql_fetch_array($results)) {

                                ?>

                                <tr>
                                    <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?= $i ?></td>
                                    <td class="txt_bold_gray" bgcolor="#F0F0F0"><?= $row[ag_name] ?></td>

                                    <?php // Date
                                    $c_date = $_POST[from_date];
                                    $x = 1;

                                    while (date($c_date <= $_POST[to_date])) {

                                        $sum_people = 0;
                                        ?>
                                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0">

                                            <?php // Query Reservations

                                            if ($_POST[status] == 1) {
                                                $sql_res = "SELECT * ";
                                                $sql_res .= "FROM reservation_packages ";
                                                $sql_res .= "WHERE agents_id = '$row[ag_id]' ";
                                                $sql_res .= "AND paymentstatus_id != '1' ";
                                                $sql_res .= "ORDER BY rpa_id ASC ";
                                            }

                                            if ($_POST[status] == 2) {
                                                $sql_res = "SELECT * ";
                                                $sql_res .= "FROM reservation_packages ";
                                                $sql_res .= "WHERE agents_id = '$row[ag_id]' ";
                                                $sql_res .= "AND bookingstatus_id = '3' ";
                                                $sql_res .= "ORDER BY rpa_id ASC ";
                                            }

                                            $result_res = mysql_query($sql_res);
                                            while ($row_res = mysql_fetch_array($result_res)) {

                                                if ($producttype) {

                                                    // Query item in Combo Product
                                                    $sql_rpt = "SELECT * ";
                                                    $sql_rpt .= "FROM reservationpackage_item ";
                                                    $sql_rpt .= "WHERE reservationpackages_id = '$row_res[rpa_id]' ";
                                                    $sql_rpt .= "AND rpt_item_producttype_id_arr LIKE '%~$producttype~%' ";
                                                    $sql_rpt .= "AND rpt_item_id_arr LIKE '%~$_POST[product_id]~%' ";
                                                    $sql_rpt .= "AND rpt_item_travel_date_arr LIKE '%~$c_date~%' ";
                                                    $sql_rpt .= "ORDER BY rpt_id ASC ";

                                                    $result_rpt = mysql_query($sql_rpt);
                                                    while ($row_rpt = mysql_fetch_array($result_rpt)) { ?>

                                                        <?php $sum_people = $sum_people + ($row_rpt[rpt_adult_num] + $row_rpt[rpt_child_num]) ?>

                                                        <?php
                                                    }  //while($row_rpt = mysql_fetch_array($result_rpt)){

                                                }  //if($producttype){

                                            }  //while($row_res = mysql_fetch_array($result_res)){

                                            $c_date = date("Y-m-d", strtotime("+1 day", strtotime($c_date)));

                                            echo $sum_people;
                                            echo $sql_rpt;
                                            $sum_people_combo_arr[$x][$y] = $sum_people;
                                            ?>
                                        </td>
                                        <?php
                                        $x++;
                                    }  //while(date($c_date <= $_POST[to_date])){
                                    ?>

                                </tr>

                                <?php $i++;
                                $y++;
                            } //while($row = mysql_fetch_array($results)){ ?>

                        </table>


                        <?php // Reservations ?>

                        <br/>

                        <table border="1" cellspacing="0" cellpadding="3" bgcolor="#FFFFFF" bordercolor="#000000">
                            <tr bgcolor="#CCCCCC">
                                <td width="20" class="txt_bold_gray" align="center">No.</td>
                                <td width="500" class="txt_bold_gray" align="center">AGENT</td>

                                <?php $strnodate = $_POST[from_date]; ?>

                                <?php for ($i = 0; $i <= DateDiff("$_POST[from_date]", "$_POST[to_date]"); $i++) { ?>

                                    <td width="40" class="txt_bold_gray"
                                        align="center"><?= date("j", strtotime($strnodate)) ?></td>

                                    <?php $strnodate = date("Y-m-d", strtotime("+1 day", strtotime($strnodate))); ?>

                                <?php } ?>

                            </tr>

                            <?php
                            $i = 1;
                            $x = 1;
                            $y = 1;

                            $strfromdate = $_POST[from_date];
                            $strtodate = $_POST[to_date];

                            // Query Agents
                            $sql = "SELECT * ";
                            $sql .= "FROM agents ";
                            $sql .= "ORDER BY ag_name ASC ";

                            $results = mysql_query($sql);
                            while ($row = mysql_fetch_array($results)) {

                                ?>
                                <tr>
                                    <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?= $i ?></td>
                                    <td class="txt_bold_gray" bgcolor="#F0F0F0"><?= $row[ag_name] ?></td>

                                    <?php // Date
                                    $c_date = $_POST[from_date];
                                    $x = 1;

                                    while (date($c_date <= $_POST[to_date])) {

                                        $sum_people = 0;
                                        ?>
                                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0">

                                            <?php // Query Reservations

                                            if ($_POST[status] == 1) {
                                                $sql_res = "SELECT * ";
                                                $sql_res .= "FROM reservations ";
                                                $sql_res .= "WHERE agents_id = '$row[ag_id]' ";
                                                $sql_res .= "AND paymentstatus_id != '1' ";
                                                $sql_res .= "ORDER BY res_id ASC ";
                                            }

                                            if ($_POST[status] == 2) {
                                                $sql_res = "SELECT * ";
                                                $sql_res .= "FROM reservations ";
                                                $sql_res .= "WHERE agents_id = '$row[ag_id]' ";
                                                $sql_res .= "AND bookingstatus_id = '3' ";
                                                $sql_res .= "ORDER BY res_id ASC ";
                                            }

                                            $result_res = mysql_query($sql_res);
                                            while ($row_res = mysql_fetch_array($result_res)) {

                                                if ($_POST[producttype] == "Boat Transfer") {

                                                    // Query Reservations Boattransfer item
                                                    $sql_rbt = "SELECT * ";
                                                    $sql_rbt .= "FROM reservation_boattransfer_items ";
                                                    $sql_rbt .= "WHERE reservations_id = '$row_res[res_id]' ";
                                                    $sql_rbt .= "AND boattransfers_id = '$_POST[product_id]' ";
                                                    $sql_rbt .= "AND rbt_travel_date = '$c_date' ";
                                                    $sql_rbt .= "ORDER BY rbt_id ASC ";

                                                    $result_rbt = mysql_query($sql_rbt);
                                                    while ($row_rbt = mysql_fetch_array($result_rbt)) { ?>

                                                        <?php $sum_people = $sum_people + ($row_rbt[rbt_adult_num] + $row_rbt[rbt_child_num]) ?>

                                                        <?php
                                                    }  //while($row_rbt = mysql_fetch_array($result_rbt)){

                                                }  //if($_POST[producttype]=="Boat Transfer"){


                                                if ($_POST[producttype] == "Pick Up Transfer") {

                                                    // Query Reservations Pickuptransfer item
                                                    $sql_rpt = "SELECT * ";
                                                    $sql_rpt .= "FROM reservation_pickuptransfer_items ";
                                                    $sql_rpt .= "WHERE reservations_id = '$row_res[res_id]' ";
                                                    $sql_rpt .= "AND pickuptransfers_id = '$_POST[product_id]' ";
                                                    $sql_rpt .= "AND rpt_travel_date = '$c_date' ";
                                                    $sql_rpt .= "ORDER BY rpt_id ASC ";

                                                    $result_rpt = mysql_query($sql_rpt);
                                                    while ($row_rpt = mysql_fetch_array($result_rpt)) { ?>

                                                        <?php $sum_people = $sum_people + ($row_rpt[rpt_adult_num] + $row_rpt[rpt_child_num]) ?>

                                                        <?php
                                                    }  //while($row_rpt = mysql_fetch_array($result_rpt)){

                                                }  //if($_POST[producttype]=="Pick Up Transfer"){


                                                if ($_POST[producttype] == "Tour") {

                                                    // Query Reservations Tour item
                                                    $sql_rtt = "SELECT * ";
                                                    $sql_rtt .= "FROM reservation_tour_items ";
                                                    $sql_rtt .= "WHERE reservations_id = '$row_res[res_id]' ";
                                                    $sql_rtt .= "AND tours_id = '$_POST[product_id]' ";
                                                    $sql_rtt .= "AND rtt_travel_date = '$c_date' ";
                                                    $sql_rtt .= "ORDER BY rtt_id ASC ";

                                                    $result_rtt = mysql_query($sql_rtt);
                                                    while ($row_rtt = mysql_fetch_array($result_rtt)) { ?>

                                                        <?php $sum_people = $sum_people + ($row_rtt[rtt_adult_num] + $row_rtt[rtt_child_num]) ?>

                                                        <?php
                                                    }  //while($row_rtt = mysql_fetch_array($result_rtt)){

                                                }  //if($_POST[producttype]=="Tour"){

                                            }  //while($row_res = mysql_fetch_array($result_res)){

                                            $c_date = date("Y-m-d", strtotime("+1 day", strtotime($c_date)));

                                            //echo $sum_people
                                            $sum_people_arr[$x][$y] = $sum_people;

                                            $sum_people = $sum_people_arr[$x][$y] + $sum_people_combo_arr[$x][$y];

                                            echo $sum_people;
                                            ?>
                                        </td>
                                        <?php
                                        $x++;
                                    }  //while(date($c_date <= $_POST[to_date])){
                                    ?>

                                </tr>

                                <?php $i++;
                                $y++;
                            } //while($row = mysql_fetch_array($results)){ ?>

                        </table>

                        <!---- Search Box ---->
                    </td>
                    <td align="right" valign="bottom"></td>
                </tr>
            </table>

            <!---- Listing Body ---->
        </td>
    </tr>
</table>