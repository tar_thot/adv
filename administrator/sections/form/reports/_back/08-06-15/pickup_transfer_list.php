<?php

function setNumberLength($num, $length)
{
    $sumstr = strlen($num);
    $zero = str_repeat("0", $length - $sumstr);
    $results = $zero . $num;

    return $results;
}

function DateDiff($strDate1, $strDate2)
{
    return (strtotime($strDate2) - strtotime($strDate1)) / (60 * 60 * 24);  // 1 day = 60*60*24
}

?>

<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td valign="top">
            <table width="100%" border="0" cellspacing="0" cellpadding="3">
                <tr>
                    <td class="txt_big_gray">Pick Up Transfer List</td>
                    <td width="500" align="right"><a href="./index.php?mode=reports/pickup_transfer_st1"
                                                     style="background-color:#ffffff; color:#000000"><<< Back</a></td>
                </tr>
            </table>
            <!---- Listing Body ---->
            <table width="100%" border="0" cellspacing="0" cellpadding="3">
                <tr>
                    <td>
                        <!---- Search Box ---->
                        <br/>

                        <?php //$strNewDate = date("Y-m-d", strtotime("+3 day", strtotime($_POST[to_date]))); ?>

                        <table align="center">
                            <tr>
                                <?php
                                if ($_POST[product_id]) {
                                    $item_num = 1;

                                    $sql = "SELECT * FROM pickuptransfers WHERE put_id = '$_POST[product_id]' ORDER BY put_id ASC";
                                    $results = mysql_query($sql);
                                    $row = mysql_fetch_array($results);

                                    $time_pickup = get_value(pickuptransfer_con_time, con_id, con_name, $row[time_id]);
                                }

                                if ($_POST[product_id2]) {
                                    $item_num = 2;

                                    $sql2 = "SELECT * FROM pickuptransfers WHERE put_id = '$_POST[product_id2]' ORDER BY put_id ASC";
                                    $results2 = mysql_query($sql2);
                                    $row2 = mysql_fetch_array($results2);

                                    $time_pickup2 = get_value(pickuptransfer_con_time, con_id, con_name, $row2[time_id]);
                                }

                                if ($_POST[product_id3]) {
                                    $item_num = 3;

                                    $sql3 = "SELECT * FROM pickuptransfers WHERE put_id = '$_POST[product_id3]' ORDER BY put_id ASC";
                                    $results3 = mysql_query($sql3);
                                    $row3 = mysql_fetch_array($results3);

                                    $time_pickup3 = get_value(pickuptransfer_con_time, con_id, con_name, $row3[time_id]);
                                }

                                if ($_POST[product_id4]) {
                                    $item_num = 4;

                                    $sql4 = "SELECT * FROM pickuptransfers WHERE put_id = '$_POST[product_id4]' ORDER BY put_id ASC";
                                    $results4 = mysql_query($sql4);
                                    $row4 = mysql_fetch_array($results4);

                                    $time_pickup4 = get_value(pickuptransfer_con_time, con_id, con_name, $row4[time_id]);
                                }

                                if ($_POST[product_id5]) {
                                    $item_num = 5;

                                    $sql5 = "SELECT * FROM pickuptransfers WHERE put_id = '$_POST[product_id5]' ORDER BY put_id ASC";
                                    $results5 = mysql_query($sql5);
                                    $row5 = mysql_fetch_array($results5);

                                    $time_pickup5 = get_value(pickuptransfer_con_time, con_id, con_name, $row5[time_id]);
                                }

                                //echo $item_num;

                                ?>
                                <td class="txt_bold_gray" align="center">
                                    <?php if ($_POST[product_id]) {
                                        echo $row[put_name];
                                    } ?>
                                    <?php if ($_POST[product_id2]) {
                                        echo ", " . $row2[put_name];
                                    } ?>
                                    <?php if ($_POST[product_id3]) {
                                        echo ", " . $row3[put_name];
                                    } ?>
                                    <?php if ($_POST[product_id4]) {
                                        echo ", " . $row4[put_name];
                                    } ?>
                                    <?php if ($_POST[product_id5]) {
                                        echo ", " . $row5[put_name];
                                    } ?>
                                    - (<?= DateFormat($_POST[from_date], "f") ?>)
                                </td>

                            </tr>
                        </table>


                        <?php // Reservations Pick Up Transfer ?>

                        <br/>

                        <table border="1" cellspacing="0" cellpadding="3" bgcolor="#FFFFFF" bordercolor="#000000">
                            <tr bgcolor="#CCCCCC">
                                <td width="20" rowspan="2" align="center" class="txt_bold_gray">CODE</td>
                                <td width="500" rowspan="2" class="txt_bold_gray" align="center">AGENT</td>
                                <td width="500" rowspan="2" class="txt_bold_gray" align="center">BY</td>
                                <td width="500" rowspan="2" class="txt_bold_gray" align="center">PASSENGER'S NAME</td>
                                <td width="500" rowspan="2" class="txt_bold_gray" align="center">PASSENGER'S EMAIL</td>
                                <td width="20" rowspan="2" class="txt_bold_gray" align="center">PAX</td>
                                <td width="50" rowspan="2" class="txt_bold_gray" align="center">CLASS</td>
                                <td width="50" rowspan="2" class="txt_bold_gray" align="center">TYPE</td>
                                <td width="500" rowspan="2" class="txt_bold_gray" align="center">PICK UP FROM</td>
                                <td width="500" rowspan="2" class="txt_bold_gray" align="center">DROP-OFF TO</td>
                                <td width="100" rowspan="2" class="txt_bold_gray" align="center">ROOM NO.</td>
                                <td width="100" rowspan="2" class="txt_bold_gray" align="center">TIME</td>
                                <td width="100" rowspan="2" class="txt_bold_gray" align="center">CASH COLLECTION</td>
                                <td width="100" rowspan="2" class="txt_bold_gray" align="center">VOUCHER</td>
                                <td width="200" rowspan="2" class="txt_bold_gray" align="center">REMARK</td>
                                <td width="200" colspan="3" class="txt_bold_gray" align="center">PRICE</td>
                            </tr>
                            <tr bgcolor="#CCCCCC">
                                <td width="100" class="txt_bold_gray" align="center">Cash</td>
                                <td width="100" class="txt_bold_gray" align="center">Credit</td>
                                <td width="100" class="txt_bold_gray" align="center">Combo</td>
                            </tr>

                            <?php // Query Reservations Pick UP Transfer

                            for ($i = 1; $i <= $item_num; $i++) {

                                $sql_res = "SELECT * ";
                                $sql_res .= "FROM reservation_pickuptransfer_items ";

                                if ($i == 1) {
                                    $sql_res .= "WHERE pickuptransfers_id = '$_POST[product_id]' ";
                                }
                                if ($i == 2) {
                                    $sql_res .= "WHERE pickuptransfers_id = '$_POST[product_id2]' ";
                                }
                                if ($i == 3) {
                                    $sql_res .= "WHERE pickuptransfers_id = '$_POST[product_id3]' ";
                                }
                                if ($i == 4) {
                                    $sql_res .= "WHERE pickuptransfers_id = '$_POST[product_id4]' ";
                                }
                                if ($i == 5) {
                                    $sql_res .= "WHERE pickuptransfers_id = '$_POST[product_id5]' ";
                                }


                                $sql_res .= "AND rpt_travel_date = '$_POST[from_date]' ";
                                //$sql_res .= "AND bookingstatus_id = '3' ";
                                $sql_res .= "ORDER BY rpt_id ASC ";

                                $result_res = mysql_query($sql_res);
                                while ($row_res = mysql_fetch_array($result_res)) {
                                    ?>
                                    <?php
                                    $put_name = get_value(pickuptransfers, put_id, put_name, $row_res[pickuptransfers_id]);

                                    $agent_id = get_value(reservations, res_id, agents_id, $row_res[reservations_id]);
                                    $code_agent = get_value(agents, ag_id, ag_ref, $agent_id);
                                    $agent_name = get_value(agents, ag_id, ag_name, $agent_id);
                                    $agentpaytype_id = get_value(agents, ag_id, agentpaytype_id, $agent_id);
                                    $res_email = get_value(reservations, res_id, res_email, $row_res[reservations_id]);

                                    $res_book_by = get_value(reservations, res_id, res_book_by, $row_res[reservations_id]);

                                    $titlename_id = get_value(reservations, res_id, titlename_id, $row_res[reservations_id]);
                                    $lis_name = get_value(lis_titlename, lis_id, lis_name, $titlename_id);
                                    $res_fname = get_value(reservations, res_id, res_fname, $row_res[reservations_id]);
                                    $res_lname = get_value(reservations, res_id, res_lname, $row_res[reservations_id]);

                                    $bookingstatus_id = get_value(reservations, res_id, bookingstatus_id, $row_res[reservations_id]);

                                    $res_cash_collection = get_value(reservations, res_id, res_cash_collection, $row_res[reservations_id]);
                                    $res_agent_voucher = get_value(reservations, res_id, res_agent_voucher, $row_res[reservations_id]);
                                    $res_request = get_value(reservations, res_id, res_request, $row_res[reservations_id]);

                                    $pax = $row_res[rpt_adult_num] + $row_res[rpt_child_num];

                                    $res_id_str = get_value(reservations, res_id, res_id_str, $row_res[reservations_id]);

                                    # Voucher

                                    $sql_vo = "SELECT * ";
                                    $sql_vo .= "FROM voucher ";
                                    $sql_vo .= "WHERE vo_res_id = '$res_id_str' ";

                                    $sql_vo .= "AND vo_status = '2' ";
                                    $sql_vo .= "AND vo_item_id = '$put_name' ";

                                    $sql_vo .= "ORDER BY vo_id ASC ";

                                    $result_vo = mysql_query($sql_vo);
                                    $row_vo = mysql_fetch_array($result_vo);

                                    # end Voucher

                                    if ($bookingstatus_id == 3) {
                                        ?>

                                        <tr>
                                            <td class="txt_bold_gray" align="center"
                                                bgcolor="#F0F0F0"><?= $res_id_str ?></td>
                                            <td class="txt_bold_gray"
                                                bgcolor="#F0F0F0"><?= $agent_name ?><?php if ($agent_name) { ?>
                                                    <br/>(<?= $code_agent ?>) <?php } ?></td>
                                            <td class="txt_bold_gray" bgcolor="#F0F0F0"><?= $res_book_by ?></td>
                                            <td class="txt_bold_gray"
                                                bgcolor="#F0F0F0"><?= $lis_name ?> <?= $res_fname ?> <?= $res_lname ?></td>
                                            <td class="txt_bold_gray" bgcolor="#F0F0F0"><?= $res_email ?></td>
                                            <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?= $pax ?></td>
                                            <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"></td>
                                            <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"></td>
                                            <td class="txt_bold_gray"
                                                bgcolor="#F0F0F0"><?= $row_res[rpt_pickupfrom] ?></td>
                                            <td class="txt_bold_gray"
                                                bgcolor="#F0F0F0"><?= $row_res[rpt_pickupto] ?></td>
                                            <td class="txt_bold_gray" align="center"
                                                bgcolor="#F0F0F0"><?= $row_res[rpt_room] ?></td>
                                            <td class="txt_bold_gray" align="center"
                                                bgcolor="#F0F0F0"><?= $time_pickup ?></td>
                                            <td class="txt_bold_gray" align="center"
                                                bgcolor="#F0F0F0"><?= number_format($res_cash_collection) ?></td>

                                            <?php if ($agentpaytype_id != 2) { ?>
                                                <td class="txt_bold_gray" bgcolor="#F0F0F0"><?= $row_vo[vo_no] ?></td>
                                            <?php } else { ?>
                                                <td class="txt_bold_gray"
                                                    bgcolor="#F0F0F0"><?= $res_agent_voucher ?></td> <?php } ?>

                                            <td class="txt_bold_gray" bgcolor="#F0F0F0"><?= nl2br($res_request) ?></td>

                                            <?php if ($agentpaytype_id != 2 || $agent_id == 0) { ?>

                                                <td class="txt_bold_gray"
                                                    bgcolor="#F0F0F0"><?= number_format($row_res[rpt_prices]) ?></td>
                                                <td class="txt_bold_gray" bgcolor="#F0F0F0">&nbsp;</td>

                                            <?php } else { ?>

                                                <td class="txt_bold_gray" bgcolor="#F0F0F0">&nbsp;</td>
                                                <td class="txt_bold_gray"
                                                    bgcolor="#F0F0F0"><?= number_format($row_res[rpt_prices]) ?></td>

                                            <?php } ?>

                                            <td class="txt_bold_gray" bgcolor="#F0F0F0">&nbsp;</td>
                                        </tr>

                                    <?php } // end if($bookingstatus_id == 3){
                                    ?>

                                <?php } // end while($row_res = mysql_fetch_array($result_res)){

                            } // end for( $i = 1 ; $i <= $item_num ; $i++ ){
                            ?>


                            <?php // Query Reservations Pick UP Transfer (Combo Product)

                            for ($i = 1; $i <= $item_num; $i++) {

                                $sql_resc = "SELECT * ";
                                $sql_resc .= "FROM reservationpackage_item ";
                                $sql_resc .= "WHERE rpt_item_producttype_id_arr LIKE '%~2~%' ";

                                if ($i == 1) {
                                    $sql_resc .= "AND rpt_item_id_arr LIKE '%~$_POST[product_id]~%' ";
                                }
                                if ($i == 2) {
                                    $sql_resc .= "AND rpt_item_id_arr LIKE '%~$_POST[product_id2]~%' ";
                                }
                                if ($i == 3) {
                                    $sql_resc .= "AND rpt_item_id_arr LIKE '%~$_POST[product_id3]~%' ";
                                }
                                if ($i == 4) {
                                    $sql_resc .= "AND rpt_item_id_arr LIKE '%~$_POST[product_id4]~%' ";
                                }
                                if ($i == 5) {
                                    $sql_resc .= "AND rpt_item_id_arr LIKE '%~$_POST[product_id5]~%' ";
                                }


                                $sql_resc .= "AND rpt_item_travel_date_arr LIKE '%~$_POST[from_date]~%' ";
                                $sql_resc .= "ORDER BY rpt_id ASC ";

                                //echo $sql_resc;

                                $result_resc = mysql_query($sql_resc);
                                while ($row_resc = mysql_fetch_array($result_resc)) {
                                    ?>
                                    <?php
                                    $agent_id = get_value(reservation_packages, rpa_id, agents_id, $row_resc[reservationpackages_id]);
                                    $code_agent = get_value(agents, ag_id, ag_ref, $agent_id);
                                    $agent_name = get_value(agents, ag_id, ag_name, $agent_id);
                                    $agentpaytype_id = get_value(agents, ag_id, agentpaytype_id, $agent_id);
                                    $rpa_email = get_value(reservation_packages, rpa_id, rpa_email, $row_resc[reservationpackages_id]);

                                    $res_book_by = get_value(reservation_packages, rpa_id, rpa_bookby, $row_resc[reservationpackages_id]);

                                    $titlename_id = get_value(reservation_packages, rpa_id, titlename_id, $row_resc[reservationpackages_id]);
                                    $lis_name = get_value(lis_titlename, lis_id, lis_name, $titlename_id);
                                    $rpa_fname = get_value(reservation_packages, rpa_id, rpa_fname, $row_resc[reservationpackages_id]);
                                    $rpa_lname = get_value(reservation_packages, rpa_id, rpa_lname, $row_resc[reservationpackages_id]);

                                    $bookingstatus_id = get_value(reservation_packages, rpa_id, bookingstatus_id, $row_resc[reservationpackages_id]);

                                    $ticket_type_id = get_value(packages, pac_id, ticket_type_id, $row_resc[packages_id]);
                                    $class_id = get_value(packages, pac_id, class_id, $row_resc[packages_id]);

                                    $ticket_type_name = get_value(lis_ticket_type, lis_id, lis_name, $ticket_type_id);
                                    $class_name = get_value(lis_class, lis_id, lis_name, $class_id);

                                    $rpa_cash_collection = get_value(reservation_packages, rpa_id, rpa_cash_collection, $row_resc[reservationpackages_id]);
                                    $rpa_agent_voucher = get_value(reservation_packages, rpa_id, rpa_agent_voucher, $row_resc[reservationpackages_id]);
                                    $rpa_request = get_value(reservation_packages, rpa_id, rpa_request, $row_resc[reservationpackages_id]);

                                    $pax = $row_resc[rpt_adult_num] + $row_resc[rpt_child_num];

                                    $rpa_id_str = get_value(reservation_packages, rpa_id, rpa_id_str, $row_resc[reservationpackages_id]);

                                    if ($bookingstatus_id == 3) {

                                        $sql_arr = "SELECT * ";
                                        $sql_arr .= "FROM reservationpackage_item ";
                                        $sql_arr .= "WHERE rpt_id = '$row_resc[rpt_id]' ";
                                        $sql_arr .= "ORDER BY rpt_id ASC ";

                                        //echo $sql_arr;

                                        $result_arr = mysql_query($sql_arr);
                                        $row_arr = mysql_fetch_array($result_arr);

                                        $rpt_item_name = explode("~", $row_arr[rpt_item_name_arr]);
                                        $rpt_item_travel_date = explode("~", $row_arr[rpt_item_travel_date_arr]);
                                        $rpt_item_producttype_id = explode("~", $row_arr[rpt_item_producttype_id_arr]);
                                        $rpt_item_id = explode("~", $row_arr[rpt_item_id_arr]);

                                        $rpt_pickup_from = explode("~", $row_arr[rpt_pickup_from_arr]);
                                        $rpt_pickup_to = explode("~", $row_arr[rpt_pickup_to_arr]);
                                        $rpt_room_no = explode("~", $row_arr[rpt_room_no_arr]);

                                        for ($i = 1; $i < count($rpt_item_travel_date); $i++) {

                                            if ($rpt_item_travel_date[$i] == $_POST[from_date] && $rpt_item_producttype_id[$i] == 2 && $rpt_item_id[$i] == $_POST[product_id]) {
                                                //echo "from : ".$rpt_pickup_from[$i]."<br>";
                                                $pickup_from = $rpt_pickup_from[$i];

                                                //echo "to : ".$rpt_pickup_to[$i]."<br>";
                                                $pickup_to = $rpt_pickup_to[$i];

                                                //echo "room no. : ".$rpt_room_no[$i]."<br>";
                                                $room_no = $rpt_room_no[$i];

                                                //echo "item_name : ".$rpt_item_name[$i]."<br>";
                                                $item_name = $rpt_item_name[$i];


                                                # Voucher

                                                $sql_vo = "SELECT * ";
                                                $sql_vo .= "FROM voucher ";
                                                $sql_vo .= "WHERE vo_res_id = '$rpa_id_str' ";

                                                $sql_vo .= "AND vo_status = '2' ";
                                                $sql_vo .= "AND vo_item_id = '$item_name' ";

                                                $sql_vo .= "ORDER BY vo_id ASC ";

                                                $result_vo = mysql_query($sql_vo);
                                                $row_vo = mysql_fetch_array($result_vo);

                                                # end Voucher


                                                ?>
                                                <tr>
                                                    <td class="txt_bold_gray" align="center"
                                                        bgcolor="#F0F0F0"><?= $rpa_id_str ?></td>
                                                    <td class="txt_bold_gray"
                                                        bgcolor="#F0F0F0"><?= $agent_name ?><?php if ($agent_name) { ?>
                                                            <br/>(<?= $code_agent ?>) <?php } ?></td>
                                                    <td class="txt_bold_gray" bgcolor="#F0F0F0"><?= $res_book_by ?></td>
                                                    <td class="txt_bold_gray"
                                                        bgcolor="#F0F0F0"><?= $lis_name ?> <?= $rpa_fname ?> <?= $rpa_lname ?></td>
                                                    <td class="txt_bold_gray" bgcolor="#F0F0F0"><?= $rpa_email ?></td>
                                                    <td class="txt_bold_gray" align="center"
                                                        bgcolor="#F0F0F0"><?= $pax ?></td>
                                                    <td class="txt_bold_gray" align="center"
                                                        bgcolor="#F0F0F0"><?= $class_name ?></td>
                                                    <td class="txt_bold_gray" align="center"
                                                        bgcolor="#F0F0F0"><?= $ticket_type_name ?></td>
                                                    <td class="txt_bold_gray" bgcolor="#F0F0F0"><?= $pickup_from ?></td>
                                                    <td class="txt_bold_gray" bgcolor="#F0F0F0"><?= $pickup_to ?></td>
                                                    <td class="txt_bold_gray" align="center"
                                                        bgcolor="#F0F0F0"><?= $room_no ?></td>
                                                    <td class="txt_bold_gray" align="center"
                                                        bgcolor="#F0F0F0"><?= $time_pickup ?></td>
                                                    <td class="txt_bold_gray" align="center"
                                                        bgcolor="#F0F0F0"><?= number_format($rpa_cash_collection) ?></td>

                                                    <?php if ($agentpaytype_id != 2) { ?>
                                                        <td class="txt_bold_gray"
                                                            bgcolor="#F0F0F0"><?= $row_vo[vo_no] ?></td>
                                                    <?php } else { ?>
                                                        <td class="txt_bold_gray"
                                                            bgcolor="#F0F0F0"><?= $rpa_agent_voucher ?></td> <?php } ?>

                                                    <td class="txt_bold_gray"
                                                        bgcolor="#F0F0F0"><?= nl2br($rpa_request) ?></td>
                                                    <td class="txt_bold_gray" bgcolor="#F0F0F0">&nbsp;</td>
                                                    <td class="txt_bold_gray" bgcolor="#F0F0F0">&nbsp;</td>
                                                    <td class="txt_bold_gray"
                                                        bgcolor="#F0F0F0"><?= number_format($row_arr[rpt_prices]) ?></td>
                                                </tr>
                                                <?php
                                            } //if($rpt_item_travel_date[$i] == $_POST[from_date] && $rpt_item_producttype_id[$i] == 2 && $rpt_item_id[$i] == $_POST[product_id]){

                                        } //for($i=1;$i<count($rpt_item_travel_date);$i++){

                                        ?>
                                        <!--
			<tr>
				<td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?= $code_agent ?></td>
                <td class="txt_bold_gray" bgcolor="#F0F0F0"><?= $agent_name ?></td>
                <td class="txt_bold_gray" bgcolor="#F0F0F0"><?= $res_book_by ?></td>
                <td class="txt_bold_gray" bgcolor="#F0F0F0"><?= $lis_name ?> <?= $rpa_fname ?> <?= $rpa_lname ?></td>
                <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?= $pax ?></td>
                <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?= $class_name ?></td>
                <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?= $ticket_type_name ?></td>
                <td class="txt_bold_gray" bgcolor="#F0F0F0"><?= $pickup_from ?></td>
                <td class="txt_bold_gray" bgcolor="#F0F0F0"><?= $pickup_to ?></td>
                <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?= $room_no ?></td>
                <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?= $time_pickup ?></td>
                <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?= number_format($rpa_cash_collection) ?></td>
				<td class="txt_bold_gray" bgcolor="#F0F0F0"><?= $rpa_agent_voucher ?></td>
                <td class="txt_bold_gray" bgcolor="#F0F0F0"><?= nl2br($rpa_request) ?></td>
			</tr>
            -->
                                    <?php } // end if($bookingstatus_id == 3){
                                    ?>

                                <?php } // end while($row_resc = mysql_fetch_array($result_resc)){

                            } // end for( $i = 1 ; $i <= $item_num ; $i++ ){
                            ?>

                        </table>

                        <!---- Search Box ---->
                    </td>
                    <td align="right" valign="bottom"></td>
                </tr>
            </table>

            <!---- Listing Body ---->
        </td>
    </tr>
</table>