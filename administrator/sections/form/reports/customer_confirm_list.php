<?php

function setNumberLength($num, $length)
{
    $sumstr = strlen($num);
    $zero = str_repeat("0", $length - $sumstr);
    $results = $zero . $num;

    return $results;
}

function DateDiff($strDate1, $strDate2)
{
    return (strtotime($strDate2) - strtotime($strDate1)) / (60 * 60 * 24);  // 1 day = 60*60*24
}

?>

<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td valign="top">
            <table width="100%" border="0" cellspacing="0" cellpadding="3">
                <tr>
                    <td class="txt_big_gray">Customer Confirm List</td>
                    <td width="500" align="right"><a href="./index.php?mode=reports/customer_confirm_st1"
                                                     style="background-color:#ffffff; color:#000000"><<< Back</a></td>
                </tr>
            </table>
            <!---- Listing Body ---->
            <table width="100%" border="0" cellspacing="0" cellpadding="3">
                <tr>
                    <td>
                        <!---- Search Box ---->
                        <br/>

                        <?php //$strNewDate = date("Y-m-d", strtotime("+3 day", strtotime($_POST[to_date]))); ?>

                        <table align="center">
                            <tr>
                                <?php if ($_POST['producttype'] == "Boat Transfer") {
                                    $producttype = 1;
                                    $sql = "SELECT * FROM boattransfers WHERE bot_id = '" . $_POST['product_id'] . "' ORDER BY bot_id ASC";
                                    $results = mysql_query($sql);
                                    $row = mysql_fetch_array($results);
                                    ?>
                                    <td class="txt_bold_gray" align="center"><?= $row['bot_name'] ?> -
                                        (<?= DateFormat($_POST['from_date'], "f") ?>
                                        - <?= DateFormat($_POST['to_date'], "f") ?>)
                                    </td>    <?php } ?>

                                <?php if ($_POST['producttype'] == "Pick Up Transfer") {
                                    $producttype = 2;
                                    $sql = "SELECT * FROM pickuptransfers WHERE put_id = '" . $_POST['product_id'] . "' ORDER BY put_id ASC";
                                    $results = mysql_query($sql);
                                    $row = mysql_fetch_array($results);
                                    ?>
                                    <td class="txt_bold_gray" align="center"><?= $row['put_name'] ?> -
                                        (<?= DateFormat($_POST['from_date'], "f") ?>
                                        - <?= DateFormat($_POST['to_date'], "f") ?>)
                                    </td>    <?php } ?>

                                <?php if ($_POST['producttype'] == "Tour") {
                                    $producttype = 3;
                                    $sql = "SELECT * FROM tours WHERE tou_id = '" . $_POST['product_id'] . "' ORDER BY tou_id ASC";
                                    $results = mysql_query($sql);
                                    $row = mysql_fetch_array($results);
                                    ?>
                                    <td class="txt_bold_gray" align="center"><?= $row['tou_name'] ?> -
                                        (<?= DateFormat($_POST['from_date'], "f") ?>
                                        - <?= DateFormat($_POST['to_date'], "f") ?>)
                                    </td>    <?php } ?>

                                <?php if ($_POST['producttype'] == "Activity") {
                                    $producttype = 4;
                                    $sql = "SELECT * FROM activities WHERE act_id = '" . $_POST['product_id'] . "' ORDER BY act_id ASC";
                                    $results = mysql_query($sql);
                                    $row = mysql_fetch_array($results);
                                    ?>
                                    <td class="txt_bold_gray" align="center"><?= $row['act_name'] ?> -
                                        (<?= DateFormat($_POST['from_date'], "f") ?>
                                        - <?= DateFormat($_POST['to_date'], "f") ?>)
                                    </td>    <?php } ?>

                                <?php if ($_POST['producttype'] == "Bus Transfer") {
                                    $producttype = 5;
                                    $sql = "SELECT * FROM cartransfers WHERE ct_id = '" . $_POST['product_id'] . "' ORDER BY ct_id ASC";
                                    $results = mysql_query($sql);
                                    $row = mysql_fetch_array($results);
                                    ?>
                                    <td class="txt_bold_gray" align="center"><?= $row['ct_name'] ?> -
                                        (<?= DateFormat($_POST['from_date'], "f") ?>
                                        - <?= DateFormat($_POST['to_date'], "f") ?>)
                                    </td>    <?php } ?>

                                <?php if ($_POST['producttype'] == "Private Land Transfer") {
                                    $producttype = 6;
                                    $sql = "SELECT * FROM privatelandtransfers WHERE plt_id = '" . $_POST['product_id'] . "' ORDER BY plt_id ASC";
                                    $results = mysql_query($sql);
                                    $row = mysql_fetch_array($results);
                                    ?>
                                    <td class="txt_bold_gray" align="center"><?= $row['plt_name'] ?> -
                                        (<?= DateFormat($_POST['from_date'], "f") ?>
                                        - <?= DateFormat($_POST['to_date'], "f") ?>)
                                    </td>    <?php } ?>

                                <?php if ($_POST['producttype'] == "Hotel") {
                                    $producttype = 7;
                                    $sql = "SELECT * FROM hotels WHERE hot_id = '" . $_POST['product_id'] . "' ORDER BY hot_id ASC";
                                    $results = mysql_query($sql);
                                    $row = mysql_fetch_array($results);
                                    ?>
                                    <td class="txt_bold_gray" align="center"><?= $row['hot_name'] ?> -
                                        (<?= DateFormat($_POST['from_date'], "f") ?>
                                        - <?= DateFormat($_POST['to_date'], "f") ?>)
                                    </td>    <?php } ?>

                                <?php if ($_POST['producttype'] == "Train Transfer") {
                                    $producttype = 8;
                                    $sql = "SELECT * FROM traintransfers WHERE train_id = '" . $_POST['product_id'] . "' ORDER BY train_id ASC";
                                    $results = mysql_query($sql);
                                    $row = mysql_fetch_array($results);
                                    ?>
                                    <td class="txt_bold_gray" align="center"><?= $row['train_name'] ?> -
                                        (<?= DateFormat($_POST['from_date'], "f") ?>
                                        - <?= DateFormat($_POST['to_date'], "f") ?>)
                                    </td>    <?php } ?>

                            </tr>
                        </table>


                        <!-- Start Hide Combo Product Table

<?php // Reservation Combo Product ?>

<br />

<table border="0" cellspacing="0" cellpadding="3" bgcolor="#FFFFFF" bordercolor="#000000">
	<tr bgcolor="#CCCCCC">
    	<td width="20" class="txt_bold_gray" align="center">No.</td>
        <td width="500" class="txt_bold_gray" align="center">AGENT</td>
        
        <?php $strnodate = $_POST['from_date']; ?>

        <?php for ($i = 0; $i <= DateDiff($_POST['from_date'], $_POST['to_date']); $i++) { ?>
        
        	<td width="40" class="txt_bold_gray" align="center"><?= date("j", strtotime($strnodate)) ?></td>

        	<?php $strnodate = date("Y-m-d", strtotime("+1 day", strtotime($strnodate))); ?>

		<?php } ?>
        
        <td width="40" class="txt_bold_gray" align="center">TOTAL</td>

    </tr>

<?php
                        $i = 1;
                        $x = 1;
                        $y = 1;

                        $strfromdate = $_POST['from_date'];
                        $strtodate = $_POST['to_date'];

                        // Query Agents
                        $sql = "SELECT * ";
                        $sql .= "FROM agents ";
                        $sql .= "ORDER BY ag_name ASC ";

                        $results = mysql_query($sql);
                        while ($row = mysql_fetch_array($results)) {

                            $total = 0;
                            ?>

	<tr>
		<td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?= $i ?></td>
        <td class="txt_bold_gray" bgcolor="#F0F0F0"><?= $row['ag_name'] ?></td>
		
<?php // Date
                            $c_date = $_POST['from_date'];
                            $t_date = $_POST['to_date'];
                            $x = 1;

                            while (date($c_date <= $_POST['to_date'])) {

                                $sum_people = 0;
                                ?>
		<td class="txt_bold_gray" align="center" bgcolor="#F0F0F0">
			
<?php // Query Reservations

                                if ($_POST['status'] == 1) {
                                    $sql_res = "SELECT * ";
                                    $sql_res .= "FROM reservation_packages ";
                                    $sql_res .= "WHERE agents_id = '" . $row['ag_id'] . "' ";
                                    $sql_res .= "AND paymentstatus_id != '1' ";
                                    $sql_res .= "ORDER BY rpa_id ASC ";
                                }

                                if ($_POST['status'] == 2) {
                                    $sql_res = "SELECT * ";
                                    $sql_res .= "FROM reservation_packages ";
                                    $sql_res .= "WHERE agents_id = '" . $row['ag_id'] . "' ";
                                    $sql_res .= " AND bookingstatus_id = '3' ";
                                    $sql_res .= "ORDER BY rpa_id ASC ";
                                }

                                $result_res = mysql_query($sql_res);
                                while ($row_res = mysql_fetch_array($result_res)) {

                                    if ($producttype) {

                                        // Query item in Combo Product
                                        $sql_rpt = "SELECT * ";
                                        $sql_rpt .= "FROM reservationpackage_item ";
                                        $sql_rpt .= "WHERE reservationpackages_id = '$row_res[rpa_id]' ";
                                        $sql_rpt .= " AND rpt_item_producttype_id_arr LIKE '%~$producttype~%' ";
                                        $sql_rpt .= " AND rpt_item_id_arr LIKE '%~" . $_POST['product_id'] . "~%' ";
                                        $sql_rpt .= " AND rpt_item_travel_date_arr LIKE '%~$c_date~%' ";
                                        $sql_rpt .= "ORDER BY rpt_id ASC ";

                                        $result_rpt = mysql_query($sql_rpt);
                                        while ($row_rpt = mysql_fetch_array($result_rpt)) {

                                            $sql_arr = "SELECT * ";
                                            $sql_arr .= "FROM reservationpackage_item ";
                                            $sql_arr .= "WHERE rpt_id = '" . $row_rpt['rpt_id'] . "' ";
                                            $sql_arr .= "ORDER BY rpt_id ASC ";

                                            //echo $sql_arr;

                                            $result_arr = mysql_query($sql_arr);
                                            $row_arr = mysql_fetch_array($result_arr);

                                            $rpt_item_travel_date = explode("~", $row_arr['rpt_item_travel_date_arr']);
                                            $rpt_item_producttype_id = explode("~", $row_arr['rpt_item_producttype_id_arr']);
                                            $rpt_item_id = explode("~", $row_arr['rpt_item_id_arr']);

                                            for ($j = 1; $j < count($rpt_item_travel_date); $j++) {

                                                if ($rpt_item_travel_date[$j] == $c_date && $rpt_item_producttype_id[$j] == $producttype && $rpt_item_id[$j] == $_POST['product_id']) {

                                                    $sum_people = $sum_people + ($row_rpt['rpt_adult_num'] + $row_rpt['rpt_child_num']);

                                                } //if($rpt_item_travel_date[$i] == $_POST[from_date] && $rpt_item_producttype_id[$i] == 2 && $rpt_item_id[$i] == $_POST[product_id]){

                                            } //for($j=1;$j<count($rpt_item_travel_date);$j++){
                                            ?>
                    
                    
						<?php //$sum_people = $sum_people + ($row_rpt[rpt_adult_num] + $row_rpt[rpt_child_num]);
                                            ?>
						
<?php
                                        }  //while($row_rpt = mysql_fetch_array($result_rpt)){

                                    }  //if($producttype){

                                }  //while($row_res = mysql_fetch_array($result_res)){

                                $c_date = date("Y-m-d", strtotime("+1 day", strtotime($c_date)));

                                echo $sum_people; //echo "<br>".$x." ".$y;

                                $total = $total + $sum_people;

                                $sum_people_combo_arr[$x][$y] = $sum_people;
                                ?>
		</td>
<?php
                                $x++;
                            }  //while(date($c_date <= $_POST[to_date])){

                            ?>
    	<td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?= $total ?></td>
    </tr>

<?php $i++;
                            $y++;
                        } //while($row = mysql_fetch_array($results)){ ?>



<?php // B to C (Internet User) ?>

	<tr>
		<td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?= $i ?></td>
        <td class="txt_bold_gray" bgcolor="#F0F0F0">B to C</td>
        
        <?php // Date
                        $c_date = $_POST['from_date'];
                        $x = 1;
                        $total = 0;

                        while (date($c_date <= $_POST['to_date'])) {

                            $sum_people = 0; ?>
        
        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0">
        
        <?php // Query Reservations (B to C)

                            if ($_POST['status'] == 1) {
                                $sql_res = "SELECT * ";
                                $sql_res .= "FROM reservation_packages ";
                                $sql_res .= "WHERE agents_id = '0' ";
                                $sql_res .= "AND paymentstatus_id != '1' ";
                                $sql_res .= "ORDER BY rpa_id ASC ";
                            }

                            if ($_POST['status'] == 2) {
                                $sql_res = "SELECT * ";
                                $sql_res .= "FROM reservation_packages ";
                                $sql_res .= "WHERE agents_id = '0' ";
                                $sql_res .= "AND bookingstatus_id = '3' ";
                                $sql_res .= "ORDER BY rpa_id ASC ";
                            }

                            $result_res = mysql_query($sql_res);
                            while ($row_res = mysql_fetch_array($result_res)) {

                                if (isset($producttype)) {

                                    // Query item in Combo Product (B to C)
                                    $sql_rpt = "SELECT * ";
                                    $sql_rpt .= "FROM reservationpackage_item ";
                                    $sql_rpt .= "WHERE reservationpackages_id = '" . $row_res['rpa_id'] . "' ";
                                    $sql_rpt .= " AND rpt_item_producttype_id_arr LIKE '%~$producttype~%' ";
                                    $sql_rpt .= " AND rpt_item_id_arr LIKE '%~" . $_POST['product_id'] . "~%' ";
                                    $sql_rpt .= " AND rpt_item_travel_date_arr LIKE '%~$c_date~%' ";
                                    $sql_rpt .= "ORDER BY rpt_id ASC ";

                                    $result_rpt = mysql_query($sql_rpt);
                                    while ($row_rpt = mysql_fetch_array($result_rpt)) {

                                        $sql_arr = "SELECT * ";
                                        $sql_arr .= "FROM reservationpackage_item ";
                                        $sql_arr .= "WHERE rpt_id = '" . $row_rpt['rpt_id'] . "' ";
                                        $sql_arr .= "ORDER BY rpt_id ASC ";

                                        //echo $sql_arr;

                                        $result_arr = mysql_query($sql_arr);
                                        $row_arr = mysql_fetch_array($result_arr);

                                        $rpt_item_travel_date = explode("~", $row_arr['rpt_item_travel_date_arr']);
                                        $rpt_item_producttype_id = explode("~", $row_arr['rpt_item_producttype_id_arr']);
                                        $rpt_item_id = explode("~", $row_arr['rpt_item_id_arr']);

                                        for ($j = 1; $j < count($rpt_item_travel_date); $j++) {

                                            if ($rpt_item_travel_date[$j] == $c_date && $rpt_item_producttype_id[$j] == $producttype && $rpt_item_id[$j] == $_POST['product_id']) {

                                                $sum_people = $sum_people + ($row_rpt['rpt_adult_num'] + $row_rpt['rpt_child_num']);

                                            } //if($rpt_item_travel_date[$i'] == $_POST['from_date'] && $rpt_item_producttype_id[$i'] == 2 && $rpt_item_id[$i'] == $_POST['product_id']){

                                        } //for($j=1;$j<count($rpt_item_travel_date);$j++){
                                        ?>
                    
<?php } //while($row_rpt = mysql_fetch_array($result_rpt)){

                                } //if($producttype){

                            } //while($row_res = mysql_fetch_array($result_res)){

                            $c_date = date("Y-m-d", strtotime("+1 day", strtotime($c_date)));
                            echo $sum_people; //echo "<br>".$x." ".$y;

                            $total = $total + $sum_people;

                            $sum_people_combo_arr[$x][$y] = $sum_people; ?>

        </td>

<?php $x++;
                        } //while(date($c_date <= $_POST[to_date'])){ ?>
        
        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?= $total ?></td>
        
	</tr>

<?php // End B2C (Internet User) ?>

</table>

End Hide Combo Product Table -->


                        <!-- Start Hide Product Table

<?php // Reservations ?>

<br />

<table border="1" cellspacing="0" cellpadding="3" bgcolor="#FFFFFF" bordercolor="#000000">
	<tr bgcolor="#CCCCCC">
    	<td width="20" class="txt_bold_gray" align="center">No.</td>
        <td width="500" class="txt_bold_gray" align="center">AGENT</td>
        <td width="100" class="txt_bold_gray" align="center">Company</td>
        
        <?php $strnodate = $_POST['from_date']; ?>

        <?php for ($i = 0; $i <= DateDiff($_POST['from_date'], $_POST['to_date']); $i++) { ?>
        
        	<td width="40" class="txt_bold_gray" align="center"><?= date("j", strtotime($strnodate)) ?></td>

        	<?php $strnodate = date("Y-m-d", strtotime("+1 day", strtotime($strnodate))); ?>

		<?php } ?>
        
        <td width="40" class="txt_bold_gray" align="center">TOTAL</td>

    </tr>
    
<?php
                        $i = 1;
                        $x = 1;
                        $y = 1;

                        $strfromdate = $_POST['from_date'];
                        $strtodate = $_POST['to_date'];

                        // Query Agents
                        $sql = "SELECT * ";
                        $sql .= "FROM agents ";
                        $sql .= "ORDER BY ag_name ASC ";

                        $results = mysql_query($sql);
                        while ($row = mysql_fetch_array($results)) {

                            $ag_company = get_value('agent_for', 'a_id', 'a_name', $row['agentfor_id']);

                            $ag_name = $row['ag_name'];

                            $total = 0;
                            ?>
	<tr>
		<td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?= $i ?></td>
        <td class="txt_bold_gray" bgcolor="#F0F0F0"><?= $row['ag_name'] ?></td>
        <td class="txt_bold_gray" bgcolor="#F0F0F0"><?= $ag_company ?></td>
		
<?php // Date
                            $c_date = $_POST['from_date'];
                            $x = 1;

                            while (date($c_date <= $_POST['to_date'])) {

                                $sum_people = 0;

                                ?>
		<td class="txt_bold_gray" align="center" bgcolor="#F0F0F0">
			
<?php // Query Reservations

                                if ($_POST['status'] == 1) {
                                    $sql_res = "SELECT * ";
                                    $sql_res .= "FROM reservations ";
                                    $sql_res .= "WHERE agents_id = '" . $row['ag_id'] . "' ";
                                    $sql_res .= "AND paymentstatus_id != '1' ";
                                    $sql_res .= "ORDER BY res_id ASC ";
                                }

                                if ($_POST['status'] == 2) {
                                    $sql_res = "SELECT * ";
                                    $sql_res .= "FROM reservations ";
                                    $sql_res .= "WHERE agents_id = '" . $row['ag_id'] . "' ";
                                    $sql_res .= " AND bookingstatus_id = '3' ";
                                    $sql_res .= "ORDER BY res_id ASC ";
                                }

                                $result_res = mysql_query($sql_res);
                                while ($row_res = mysql_fetch_array($result_res)) {

                                    if ($_POST['producttype'] == "Boat Transfer") {

                                        // Query Reservations Boattransfer item
                                        $sql_rbt = "SELECT * ";
                                        $sql_rbt .= "FROM reservation_boattransfer_items ";
                                        $sql_rbt .= "WHERE reservations_id = '$row_res[res_id]' ";
                                        $sql_rbt .= " AND boattransfers_id = '" . $_POST['product_id'] . "' ";
                                        $sql_rbt .= " AND rbt_travel_date = '$c_date' ";
                                        $sql_rbt .= "ORDER BY rbt_id ASC ";

                                        $result_rbt = mysql_query($sql_rbt);
                                        while ($row_rbt = mysql_fetch_array($result_rbt)) {

                                            $sum_people = $sum_people + ($row_rbt['rbt_adult_num'] + $row_rbt['rbt_child_num']);

                                        }  //while($row_rbt = mysql_fetch_array($result_rbt)){

                                    }  //if($_POST['producttype']=="Boat Transfer"){


                                    if ($_POST['producttype'] == "Pick Up Transfer") {

                                        // Query Reservations Pickuptransfer item
                                        $sql_rpt = "SELECT * ";
                                        $sql_rpt .= "FROM reservation_pickuptransfer_items ";
                                        $sql_rpt .= "WHERE reservations_id = '" . $row_res['res_id'] . "' ";
                                        $sql_rpt .= "AND pickuptransfers_id = '" . $_POST['product_id'] . "' ";
                                        $sql_rpt .= "AND rpt_travel_date = '$c_date' ";
                                        $sql_rpt .= "ORDER BY rpt_id ASC ";

                                        $result_rpt = mysql_query($sql_rpt);
                                        while ($row_rpt = mysql_fetch_array($result_rpt)) {

                                            $sum_people = $sum_people + ($row_rpt['rpt_adult_num'] + $row_rpt['rpt_child_num']);

                                        }  //while($row_rpt = mysql_fetch_array($result_rpt)){

                                    }  //if($_POST[producttype]=="Pick Up Transfer"){


                                    if ($_POST['producttype'] == "Tour") {

                                        // Query Reservations Tour item
                                        $sql_rtt = "SELECT * ";
                                        $sql_rtt .= "FROM reservation_tour_items ";
                                        $sql_rtt .= "WHERE reservations_id = '" . $row_res['res_id'] . "' ";
                                        $sql_rtt .= " AND tours_id = '" . $_POST['product_id'] . "' ";
                                        $sql_rtt .= " AND rtt_travel_date = '$c_date' ";
                                        $sql_rtt .= "ORDER BY rtt_id ASC ";

                                        $result_rtt = mysql_query($sql_rtt);
                                        while ($row_rtt = mysql_fetch_array($result_rtt)) {

                                            $sum_people = $sum_people + ($row_rtt['rtt_adult_num'] + $row_rtt['rtt_child_num']);

                                        }  //while($row_rtt = mysql_fetch_array($result_rtt)){

                                    }  //if($_POST['producttype']=="Tour"){


                                    if ($_POST['producttype'] == "Activity") {

                                        // Query Reservations Activity item
                                        $sql_rat = "SELECT * ";
                                        $sql_rat .= "FROM reservation_activity_items ";
                                        $sql_rat .= "WHERE reservations_id = '" . $row_res['res_id'] . "' ";
                                        $sql_rat .= "AND activities_id = '" . $_POST['product_id'] . "' ";
                                        $sql_rat .= "AND rat_travel_date = '$c_date' ";
                                        $sql_rat .= "ORDER BY rat_id ASC ";

                                        $result_rat = mysql_query($sql_rat);
                                        while ($row_rat = mysql_fetch_array($result_rat)) {

                                            $sum_people = $sum_people + ($row_rat['rat_adult_num'] + $row_rat['rat_child_num']);

                                        }  //while($row_rat = mysql_fetch_array($result_rat)){

                                    }  //if($_POST[producttype]=="Activity"){


                                    if ($_POST['producttype'] == "Bus Transfer") {

                                        // Query Reservations Bus Transfer item
                                        $sql_rct = "SELECT * ";
                                        $sql_rct .= "FROM reservation_bustransfer_items ";
                                        $sql_rct .= "WHERE reservations_id = '" . $row_res['res_id'] . "' ";
                                        $sql_rct .= " AND bustransfers_id = '" . $_POST['product_id'] . "' ";
                                        $sql_rct .= " AND rct_travel_date = '$c_date' ";
                                        $sql_rct .= "ORDER BY rct_id ASC ";

                                        $result_rct = mysql_query($sql_rct);
                                        while ($row_rct = mysql_fetch_array($result_rct)) {

                                            $sum_people = $sum_people + ($row_rct['rct_adult_num'] + $row_rct['rct_child_num']);

                                        }  //while($row_rct = mysql_fetch_array($result_rct)){

                                    }  //if($_POST['producttype] == "Bus Transfer"){


                                    if ($_POST['producttype'] == "Private Land Transfer") {

                                        // Query Reservations Private Land Transfer item
                                        $sql_rplt = "SELECT * ";
                                        $sql_rplt .= "FROM reservation_privatelandtransfer_items ";
                                        $sql_rplt .= "WHERE reservations_id = '" . $row_res['res_id'] . "' ";
                                        $sql_rplt .= "AND privatelandtransfers_id = '" . $_POST['product_id'] . "' ";
                                        $sql_rplt .= "AND rplt_travel_date = '$c_date' ";
                                        $sql_rplt .= "ORDER BY rplt_id ASC ";

                                        $result_rplt = mysql_query($sql_rplt);
                                        while ($row_rplt = mysql_fetch_array($result_rplt)) {

                                            $sum_people = $sum_people + ($row_rplt['rplt_adult_num'] + $row_rplt['rplt_child_num']);

                                        }  //while($row_rplt = mysql_fetch_array($result_rplt)){

                                    }  //if($_POST[producttype'] == "Private Land Transfer"){


                                    if ($_POST['producttype'] == "Hotel") {

                                        // Query Reservations Hotel item
                                        $sql_rht = "SELECT * ";
                                        $sql_rht .= "FROM reservation_hotel_items ";
                                        $sql_rht .= "WHERE reservations_id = '" . $row_res['res_id'] . "' ";
                                        $sql_rht .= "AND hotels_id = '" . $_POST['product_id'] . "' ";
                                        $sql_rht .= "AND rht_check_in = '$c_date' ";
                                        $sql_rht .= "ORDER BY rht_id ASC ";

                                        $result_rht = mysql_query($sql_rht);
                                        while ($row_rht = mysql_fetch_array($result_rht)) {

                                            $sum_people = $sum_people + ($row_rht['rht_adult_num'] + $row_rht['rht_child_num']);

                                        }  //while($row_rht = mysql_fetch_array($result_rht)){

                                    }  //if($_POST[producttype] == "Hotel"){


                                    if ($_POST['producttype'] == "Train Transfer") {

                                        // Query Reservations Train Transfer item
                                        $sql_rrt = "SELECT * ";
                                        $sql_rrt .= "FROM reservation_traintransfer_items ";
                                        $sql_rrt .= "WHERE reservations_id = '" . $row_res['res_id'] . "' ";
                                        $sql_rrt .= "AND traintransfers_id = '" . $_POST['product_id'] . "' ";
                                        $sql_rrt .= "AND rrt_travel_date = '$c_date' ";
                                        $sql_rrt .= "ORDER BY rrt_id ASC ";

                                        $result_rrt = mysql_query($sql_rrt);
                                        while ($row_rrt = mysql_fetch_array($result_rrt)) {

                                            $sum_people = $sum_people + ($row_rrt['rrt_adult_num'] + $row_rrt['rrt_child_num']);

                                        }  //while($row_rrt = mysql_fetch_array($result_rrt)){

                                    }  //if($_POST[producttype] == "Train Transfer"){


                                }  //while($row_res = mysql_fetch_array($result_res)){


                                $c_date = date("Y-m-d", strtotime("+1 day", strtotime($c_date)));

                                $sum_people_arr[$x][$y] = $sum_people;

                                $sum_people = $sum_people_arr[$x][$y] + $sum_people_combo_arr[$x][$y];

                                echo $sum_people;

                                $total = $total + $sum_people;
                                ?>
		</td>
<?php
                                $x++;
                            }  //while(date($c_date <= $_POST[to_date])){

                            $total_arr[$ag_name][$y] = $total;
                            ?>
		<td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?= $total ?></td>
    </tr>
    
<?php $i++;
                            $y++;
                        } //while($row = mysql_fetch_array($results)){ ?>



<?php // B to C (Internet User) ?>

	<tr>
		<td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?= $i ?></td>
        <td class="txt_bold_gray" bgcolor="#F0F0F0">B to C</td>
        <td class="txt_bold_gray" bgcolor="#F0F0F0">&nbsp;</td>
        
<?php // Date
                        $c_date = $_POST['from_date'];
                        $x = 1;
                        $total = 0;

                        while (date($c_date <= $_POST['to_date'])) {

                            $sum_people = 0; ?>
        
		<td class="txt_bold_gray" align="center" bgcolor="#F0F0F0">
        
        <?php // Query Reservations

                            if ($_POST['status'] == 1) {
                                $sql_res = "SELECT * ";
                                $sql_res .= "FROM reservations ";
                                $sql_res .= "WHERE agents_id = '0' ";
                                $sql_res .= "AND paymentstatus_id != '1' ";
                                $sql_res .= "ORDER BY res_id ASC ";
                            }

                            if ($_POST['status'] == 2) {
                                $sql_res = "SELECT * ";
                                $sql_res .= "FROM reservations ";
                                $sql_res .= "WHERE agents_id = '0' ";
                                $sql_res .= "AND bookingstatus_id = '3' ";
                                $sql_res .= "ORDER BY res_id ASC ";
                            }

                            $result_res = mysql_query($sql_res);
                            while ($row_res = mysql_fetch_array($result_res)) {

                                if ($_POST['producttype'] == "Boat Transfer") {

                                    // Query Reservations Boattransfer item
                                    $sql_rbt = "SELECT * ";
                                    $sql_rbt .= "FROM reservation_boattransfer_items ";
                                    $sql_rbt .= "WHERE reservations_id = '" . $row_res['res_id'] . "' ";
                                    $sql_rbt .= "AND boattransfers_id = '" . $_POST['product_id'] . "' ";
                                    $sql_rbt .= "AND rbt_travel_date = '$c_date' ";
                                    $sql_rbt .= "ORDER BY rbt_id ASC ";

                                    $result_rbt = mysql_query($sql_rbt);
                                    while ($row_rbt = mysql_fetch_array($result_rbt)) {

                                        $sum_people = $sum_people + ($row_rbt['rbt_adult_num'] + $row_rbt['rbt_child_num']);

                                    }  //while($row_rbt = mysql_fetch_array($result_rbt)){

                                }  //if($_POST['producttype']=="Boat Transfer"){


                                if ($_POST['producttype'] == "Pick Up Transfer") {

                                    // Query Reservations Pickuptransfer item
                                    $sql_rpt = "SELECT * ";
                                    $sql_rpt .= "FROM reservation_pickuptransfer_items ";
                                    $sql_rpt .= "WHERE reservations_id = '" . $row_res['res_id'] . "' ";
                                    $sql_rpt .= "AND pickuptransfers_id = '" . $_POST['product_id'] . "' ";
                                    $sql_rpt .= "AND rpt_travel_date = '$c_date' ";
                                    $sql_rpt .= "ORDER BY rpt_id ASC ";

                                    $result_rpt = mysql_query($sql_rpt);
                                    while ($row_rpt = mysql_fetch_array($result_rpt)) {

                                        $sum_people = $sum_people + ($row_rpt['rpt_adult_num'] + $row_rpt['rpt_child_num']);

                                    }  //while($row_rpt = mysql_fetch_array($result_rpt)){

                                }  //if($_POST[producttype]=="Pick Up Transfer"){


                                if ($_POST['producttype'] == "Tour") {

                                    // Query Reservations Tour item
                                    $sql_rtt = "SELECT * ";
                                    $sql_rtt .= "FROM reservation_tour_items ";
                                    $sql_rtt .= "WHERE reservations_id = '" . $row_res['res_id'] . "' ";
                                    $sql_rtt .= "AND tours_id = '" . $_POST['product_id'] . "' ";
                                    $sql_rtt .= "AND rtt_travel_date = '$c_date' ";
                                    $sql_rtt .= "ORDER BY rtt_id ASC ";

                                    $result_rtt = mysql_query($sql_rtt);
                                    while ($row_rtt = mysql_fetch_array($result_rtt)) {

                                        $sum_people = $sum_people + ($row_rtt['rtt_adult_num'] + $row_rtt['rtt_child_num']);

                                    }  //while($row_rtt = mysql_fetch_array($result_rtt)){

                                }  //if($_POST[producttype]=="Tour"){


                                if ($_POST['producttype'] == "Activity") {

                                    // Query Reservations Activity item
                                    $sql_rat = "SELECT * ";
                                    $sql_rat .= "FROM reservation_activity_items ";
                                    $sql_rat .= "WHERE reservations_id = '" . $row_res['res_id'] . "' ";
                                    $sql_rat .= " AND activities_id = '" . $_POST['product_id'] . "' ";
                                    $sql_rat .= " AND rat_travel_date = '$c_date' ";
                                    $sql_rat .= "ORDER BY rat_id ASC ";

                                    $result_rat = mysql_query($sql_rat);
                                    while ($row_rat = mysql_fetch_array($result_rat)) {

                                        $sum_people = $sum_people + ($row_rat['rat_adult_num'] + $row_rat['rat_child_num']);

                                    }  //while($row_rat = mysql_fetch_array($result_rat)){

                                }  //if($_POST[''producttype']=="Activity"){


                                if ($_POST['producttype'] == "Bus Transfer") {

                                    // Query Reservations Bus Transfer item
                                    $sql_rct = "SELECT * ";
                                    $sql_rct .= "FROM reservation_bustransfer_items ";
                                    $sql_rct .= "WHERE reservations_id = '" . $row_res['res_id'] . "' ";
                                    $sql_rct .= "AND bustransfers_id = '" . $_POST['product_id'] . "' ";
                                    $sql_rct .= "AND rct_travel_date = '$c_date' ";
                                    $sql_rct .= "ORDER BY rct_id ASC ";

                                    $result_rct = mysql_query($sql_rct);
                                    while ($row_rct = mysql_fetch_array($result_rct)) {

                                        $sum_people = $sum_people + ($row_rct['rct_adult_num'] + $row_rct['rct_child_num']);

                                    }  //while($row_rct = mysql_fetch_array($result_rct)){

                                }  //if($_POST[producttype] == "Bus Transfer"){


                                if ($_POST['producttype'] == "Private Land Transfer") {

                                    // Query Reservations Private Land Transfer item
                                    $sql_rplt = "SELECT * ";
                                    $sql_rplt .= "FROM reservation_privatelandtransfer_items ";
                                    $sql_rplt .= "WHERE reservations_id = '" . $row_res['res_id'] . "' ";
                                    $sql_rplt .= " AND privatelandtransfers_id = '" . $_POST['product_id'] . "' ";
                                    $sql_rplt .= " AND rplt_travel_date = '$c_date' ";
                                    $sql_rplt .= "ORDER BY rplt_id ASC ";

                                    $result_rplt = mysql_query($sql_rplt);
                                    while ($row_rplt = mysql_fetch_array($result_rplt)) {

                                        $sum_people = $sum_people + ($row_rplt['rplt_adult_num'] + $row_rplt['rplt_child_num']);

                                    }  //while($row_rplt = mysql_fetch_array($result_rplt)){

                                }  //if($_POST['producttype] == "Private Land Transfer"){


                                if ($_POST['producttype'] == "Hotel") {

                                    // Query Reservations Hotel item
                                    $sql_rht = "SELECT * ";
                                    $sql_rht .= "FROM reservation_hotel_items ";
                                    $sql_rht .= "WHERE reservations_id = '" . $row_res['res_id'] . "' ";
                                    $sql_rht .= "AND hotels_id = '" . $_POST['product_id'] . "' ";
                                    $sql_rht .= "AND rht_check_in = '$c_date' ";
                                    //$sql_rht .= "AND rht_check_out = '$t_date' ";
                                    $sql_rht .= "ORDER BY rht_id ASC ";

                                    $result_rht = mysql_query($sql_rht);
                                    while ($row_rht = mysql_fetch_array($result_rht)) {

                                        $sum_people = $sum_people + ($row_rht['rht_adult_num'] + $row_rht['rht_child_num']);

                                    }  //while($row_rht = mysql_fetch_array($result_rht)){

                                }  //if($_POST[producttype'] == "Hotel"){


                                if ($_POST['producttype'] == "Train Transfer") {

                                    // Query Reservations Train Transfer item
                                    $sql_rrt = "SELECT * ";
                                    $sql_rrt .= "FROM reservation_traintransfer_items ";
                                    $sql_rrt .= "WHERE reservations_id = '" . $row_res['res_id'] . "' ";
                                    $sql_rrt .= "AND traintransfers_id = '" . $_POST['product_id'] . "' ";
                                    $sql_rrt .= "AND rrt_travel_date = '$c_date' ";
                                    $sql_rrt .= "ORDER BY rrt_id ASC ";

                                    $result_rrt = mysql_query($sql_rrt);
                                    while ($row_rrt = mysql_fetch_array($result_rrt)) {

                                        $sum_people = $sum_people + ($row_rrt['rrt_adult_num'] + $row_rrt['rrt_child_num']);

                                    }  //while($row_rrt = mysql_fetch_array($result_rrt)){

                                }  //if($_POST[producttype] == "Train Transfer"){


                            } //while($row_res = mysql_fetch_array($result_res)){

                            $c_date = date("Y-m-d", strtotime("+1 day", strtotime($c_date)));

                            $sum_people_arr[$x][$y] = $sum_people;

                            $sum_people = $sum_people_arr[$x][$y] + $sum_people_combo_arr[$x][$y];

                            echo $sum_people;

                            $total = $total + $sum_people; ?>
        
        </td>
        
<?php $x++;
                        } //while(date($c_date <= $_POST[to_date])){ ?>
        
        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?= $total ?></td>
	</tr>

<?php // End B to C (Internet User) ?>

</table>

End Hide Product Table -->


                        <?php // Reservations ?>

                        <br/>

                        <table border="1" cellspacing="0" cellpadding="3" bgcolor="#FFFFFF" bordercolor="#000000">
                            <tr bgcolor="#CCCCCC">
                                <td width="20" class="txt_bold_gray" align="center">No.</td>
                                <td width="500" class="txt_bold_gray" align="center">AGENT</td>
                                <td width="100" class="txt_bold_gray" align="center">Company</td>

                                <?php $strnodate = $_POST['from_date']; ?>

                                <?php for ($i = 0;
                                           $i <= DateDiff($_POST['from_date'], $_POST['to_date']);
                                           $i++) { ?>

                                    <td width="40" class="txt_bold_gray"
                                        align="center"><?= date("j/n", strtotime($strnodate)) ?></td>

                                    <?php $strnodate = date("Y-m-d", strtotime("+1 day", strtotime($strnodate))); ?>

                                <?php } ?>

                                <td width="40" class="txt_bold_gray" align="center">TOTAL</td>

                            </tr>

                            <?php
                            $i = 1;
                            $x = 1;
                            $y = 1;

                            $total_sum = 0;

                            $strfromdate = $_POST['from_date'];
                            $strtodate = $_POST['to_date'];

                            // Query Agents
                            $sql = "SELECT * ";
                            $sql .= "FROM agents ";
                            $sql .= "ORDER BY ag_name ASC ";

                            $results = mysql_query($sql);
                            while ($row = mysql_fetch_array($results)) {

                                $ag_company = get_value('agent_for', 'a_id', 'a_name', $row['agentfor_id']);

                                $ag_name = $row['ag_name'];

                                $total = 0;

                                if ($total_arr[$ag_name][$y] != 0) {

                                    ?>
                                    <tr>
                                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?= $i ?></td>
                                        <td class="txt_bold_gray" bgcolor="#F0F0F0"><?= $row['ag_name'] ?></td>
                                        <td class="txt_bold_gray" bgcolor="#F0F0F0"><?= $ag_company ?></td>

                                        <?php // Date
                                        $c_date = $_POST['from_date'];
                                        $x = 1;

                                        while (date($c_date <= $_POST['to_date'])) {

                                            $sum_people = 0;

                                            ?>
                                            <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0">

                                                <?php // Query Reservations

                                                if ($_POST['status'] == 1) {
                                                    $sql_res = "SELECT * ";
                                                    $sql_res .= "FROM reservations ";
                                                    $sql_res .= "WHERE agents_id = '" . $row['ag_id'] . "' ";
                                                    $sql_res .= "AND paymentstatus_id != '1' ";
                                                    $sql_res .= "ORDER BY res_id ASC ";
                                                }

                                                if ($_POST['status'] == 2) {
                                                    $sql_res = "SELECT * ";
                                                    $sql_res .= "FROM reservations ";
                                                    $sql_res .= "WHERE agents_id = '" . $row['ag_id'] . "' ";
                                                    $sql_res .= "AND bookingstatus_id = '3' ";
                                                    $sql_res .= "ORDER BY res_id ASC ";
                                                }

                                                $result_res = mysql_query($sql_res);
                                                while ($row_res = mysql_fetch_array($result_res)) {

                                                    if ($_POST['producttype'] == "Boat Transfer") {

                                                        // Query Reservations Boattransfer item
                                                        $sql_rbt = "SELECT * ";
                                                        $sql_rbt .= "FROM reservation_boattransfer_items ";
                                                        $sql_rbt .= "WHERE reservations_id = '" . $row_res['res_id'] . "' ";
                                                        $sql_rbt .= " AND boattransfers_id = '" . $_POST['product_id'] . "' ";
                                                        $sql_rbt .= "AND rbt_travel_date = '$c_date' ";
                                                        $sql_rbt .= "ORDER BY rbt_id ASC ";

                                                        $result_rbt = mysql_query($sql_rbt);
                                                        while ($row_rbt = mysql_fetch_array($result_rbt)) {

                                                            $sum_people = $sum_people + ($row_rbt['rbt_adult_num'] + $row_rbt['rbt_child_num']);

                                                        }  //while($row_rbt = mysql_fetch_array($result_rbt)){

                                                    }  //if($_POST['producttype']=="Boat Transfer"){


                                                    if ($_POST['producttype'] == "Pick Up Transfer") {

                                                        // Query Reservations Pickuptransfer item
                                                        $sql_rpt = "SELECT * ";
                                                        $sql_rpt .= "FROM reservation_pickuptransfer_items ";
                                                        $sql_rpt .= "WHERE reservations_id = '" . $row_res['res_id'] . "' ";
                                                        $sql_rpt .= "AND pickuptransfers_id = '" . $_POST['product_id'] . "' ";
                                                        $sql_rpt .= "AND rpt_travel_date = '$c_date' ";
                                                        $sql_rpt .= "ORDER BY rpt_id ASC ";

                                                        $result_rpt = mysql_query($sql_rpt);
                                                        while ($row_rpt = mysql_fetch_array($result_rpt)) {

                                                            $sum_people = $sum_people + ($row_rpt['rpt_adult_num'] + $row_rpt['rpt_child_num']);

                                                        }  //while($row_rpt = mysql_fetch_array($result_rpt)){

                                                    }  //if($_POST['producttype']=="Pick Up Transfer"){


                                                    if ($_POST['producttype'] == "Tour") {

                                                        // Query Reservations Tour item
                                                        $sql_rtt = "SELECT * ";
                                                        $sql_rtt .= "FROM reservation_tour_items ";
                                                        $sql_rtt .= "WHERE reservations_id = '" . $row_res['res_id'] . "' ";
                                                        $sql_rtt .= " AND tours_id = '" . $_POST['product_id'] . "' ";
                                                        $sql_rtt .= "AND rtt_travel_date = '$c_date' ";
                                                        $sql_rtt .= "ORDER BY rtt_id ASC ";

                                                        $result_rtt = mysql_query($sql_rtt);
                                                        while ($row_rtt = mysql_fetch_array($result_rtt)) {

                                                            $sum_people = $sum_people + ($row_rtt['rtt_adult_num'] + $row_rtt['rtt_child_num']);

                                                        }  //while($row_rtt = mysql_fetch_array($result_rtt)){

                                                    }  //if($_POST['producttype']=="Tour"){


                                                    if ($_POST['producttype'] == "Activity") {

                                                        // Query Reservations Activity item
                                                        $sql_rat = "SELECT * ";
                                                        $sql_rat .= "FROM reservation_activity_items ";
                                                        $sql_rat .= "WHERE reservations_id = '" . $row_res['res_id'] . "' ";
                                                        $sql_rat .= "AND activities_id = '" . $_POST['product_id'] . "' ";
                                                        $sql_rat .= "AND rat_travel_date = '$c_date' ";
                                                        $sql_rat .= "ORDER BY rat_id ASC ";

                                                        $result_rat = mysql_query($sql_rat);
                                                        while ($row_rat = mysql_fetch_array($result_rat)) {

                                                            $sum_people = $sum_people + ($row_rat['rat_adult_num'] + $row_rat['rat_child_num']);

                                                        }  //while($row_rat = mysql_fetch_array($result_rat)){

                                                    }  //if($_POST['producttype']=="Activity"){


                                                    if ($_POST['producttype'] == "Bus Transfer") {

                                                        // Query Reservations Bus Transfer item
                                                        $sql_rct = "SELECT * ";
                                                        $sql_rct .= "FROM reservation_bustransfer_items ";
                                                        $sql_rct .= "WHERE reservations_id = '" . $row_res['res_id'] . "' ";
                                                        $sql_rct .= " AND bustransfers_id = '" . $_POST['product_id'] . "' ";
                                                        $sql_rct .= "AND rct_travel_date = '$c_date' ";
                                                        $sql_rct .= "ORDER BY rct_id ASC ";

                                                        $result_rct = mysql_query($sql_rct);
                                                        while ($row_rct = mysql_fetch_array($result_rct)) {

                                                            $sum_people = $sum_people + ($row_rct['rct_adult_num'] + $row_rct['rct_child_num']);

                                                        }  //while($row_rct = mysql_fetch_array($result_rct)){

                                                    }  //if($_POST['producttype'] == "Bus Transfer"){


                                                    if ($_POST['producttype'] == "Private Land Transfer") {

                                                        // Query Reservations Private Land Transfer item
                                                        $sql_rplt = "SELECT * ";
                                                        $sql_rplt .= "FROM reservation_privatelandtransfer_items ";
                                                        $sql_rplt .= "WHERE reservations_id = '" . $row_res['res_id'] . "' ";
                                                        $sql_rplt .= "AND privatelandtransfers_id = '" . $_POST['product_id'] . "' ";
                                                        $sql_rplt .= "AND rplt_travel_date = '$c_date' ";
                                                        $sql_rplt .= "ORDER BY rplt_id ASC ";

                                                        $result_rplt = mysql_query($sql_rplt);
                                                        while ($row_rplt = mysql_fetch_array($result_rplt)) {

                                                            $sum_people = $sum_people + ($row_rplt['rplt_adult_num'] + $row_rplt['rplt_child_num']);

                                                        }  //while($row_rplt = mysql_fetch_array($result_rplt)){

                                                    }  //if($_POST['producttype'] == "Private Land Transfer"){


                                                    if ($_POST['producttype'] == "Hotel") {

                                                        // Query Reservations Hotel item
                                                        $sql_rht = "SELECT * ";
                                                        $sql_rht .= "FROM reservation_hotel_items ";
                                                        $sql_rht .= "WHERE reservations_id = '" . $row_res['res_id'] . "' ";
                                                        $sql_rht .= " AND hotels_id = '" . $_POST['product_id'] . "' ";
                                                        $sql_rht .= "AND rht_check_in = '$c_date' ";
                                                        $sql_rht .= "ORDER BY rht_id ASC ";

                                                        $result_rht = mysql_query($sql_rht);
                                                        while ($row_rht = mysql_fetch_array($result_rht)) {

                                                            $sum_people = $sum_people + ($row_rht['rht_adult_num'] + $row_rht['rht_child_num']);

                                                        }  //while($row_rht = mysql_fetch_array($result_rht)){

                                                    }  //if($_POST['producttype'] == "Hotel"){


                                                    if ($_POST['producttype'] == "Train Transfer") {

                                                        // Query Reservations Train Transfer item
                                                        $sql_rrt = "SELECT * ";
                                                        $sql_rrt .= "FROM reservation_traintransfer_items ";
                                                        $sql_rrt .= "WHERE reservations_id = '" . $row_res['res_id'] . "' ";
                                                        $sql_rrt .= "AND traintransfers_id = '" . $_POST['product_id'] . "' ";
                                                        $sql_rrt .= "AND rrt_travel_date = '$c_date' ";
                                                        $sql_rrt .= "ORDER BY rrt_id ASC ";

                                                        echo $sql_rrt;

                                                        $result_rrt = mysql_query($sql_rrt);
                                                        while ($row_rrt = mysql_fetch_array($result_rrt)) {

                                                            $sum_people = $sum_people + ($row_rrt['rrt_adult_num'] + $row_rrt['rrt_child_num']);

                                                        }  //while($row_rrt = mysql_fetch_array($result_rrt)){

                                                    }  //if($_POST['producttype'] == "Train Transfer"){


                                                }  //while($row_res = mysql_fetch_array($result_res)){


                                                $c_date = date("Y-m-d", strtotime("+1 day", strtotime($c_date)));

                                                $sum_people_arr[$x][$y] = $sum_people;

                                                $sum_people = $sum_people_arr[$x][$y] + $sum_people_combo_arr[$x][$y];

                                                $sum_col[$x][$y] = $sum_people_arr[$x][$y] + $sum_people_combo_arr[$x][$y];

                                                //echo $sum_people;
                                                echo $sum_col[$x][$y];

                                                $total = $total + $sum_people;
                                                ?>
                                            </td>
                                            <?php
                                            $x++;
                                        }  //while(date($c_date <= $_POST['to_date'])){

                                        $total_sum = $total_sum + $total_arr[$ag_name][$y];
                                        ?>
                                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?= $total ?></td>
                                    </tr>

                                    <?php $i++;
                                } // END if($total_arr[$ag_name][$y] == 0){

                                $y++;
                            } //while($row = mysql_fetch_array($results)){ ?>



                            <?php // B to C (Internet User) ?>

                            <tr>
                                <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?= $i ?></td>
                                <td class="txt_bold_gray" bgcolor="#F0F0F0">B to C</td>
                                <td class="txt_bold_gray" bgcolor="#F0F0F0">&nbsp;</td>

                                <?php // Date
                                $c_date = $_POST['from_date'];
                                $x = 1;
                                $total = 0;

                                while (date($c_date <= $_POST['to_date'])) {

                                    $sum_people = 0; ?>

                                    <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0">

                                        <?php // Query Reservations

                                        if ($_POST['status'] == 1) {
                                            $sql_res = "SELECT * ";
                                            $sql_res .= "FROM reservations ";
                                            $sql_res .= "WHERE agents_id = '0' ";
                                            $sql_res .= "AND paymentstatus_id != '1' ";
                                            $sql_res .= "ORDER BY res_id ASC ";
                                        }

                                        if ($_POST['status'] == 2) {
                                            $sql_res = "SELECT * ";
                                            $sql_res .= "FROM reservations ";
                                            $sql_res .= "WHERE agents_id = '0' ";
                                            $sql_res .= "AND bookingstatus_id = '3' ";
                                            $sql_res .= "ORDER BY res_id ASC ";
                                        }

                                        $result_res = mysql_query($sql_res);
                                        while ($row_res = mysql_fetch_array($result_res)) {

                                            if ($_POST['producttype'] == "Boat Transfer") {

                                                // Query Reservations Boattransfer item
                                                $sql_rbt = "SELECT * ";
                                                $sql_rbt .= "FROM reservation_boattransfer_items ";
                                                $sql_rbt .= "WHERE reservations_id = '" . $row_res['res_id'] . "' ";
                                                $sql_rbt .= "AND boattransfers_id = '" . $_POST['product_id'] . "' ";
                                                $sql_rbt .= "AND rbt_travel_date = '$c_date' ";
                                                $sql_rbt .= "ORDER BY rbt_id ASC ";

                                                $result_rbt = mysql_query($sql_rbt);
                                                while ($row_rbt = mysql_fetch_array($result_rbt)) {

                                                    $sum_people = $sum_people + ($row_rbt['rbt_adult_num'] + $row_rbt['rbt_child_num']);

                                                }  //while($row_rbt = mysql_fetch_array($result_rbt)){

                                            }  //if($_POST['producttype']=="Boat Transfer"){


                                            if ($_POST['producttype'] == "Pick Up Transfer") {

                                                // Query Reservations Pickuptransfer item
                                                $sql_rpt = "SELECT * ";
                                                $sql_rpt .= "FROM reservation_pickuptransfer_items ";
                                                $sql_rpt .= "WHERE reservations_id = '" . $row_res['res_id'] . "' ";
                                                $sql_rpt .= " AND pickuptransfers_id = '" . $_POST['product_id'] . "' ";
                                                $sql_rpt .= " AND rpt_travel_date = '$c_date' ";
                                                $sql_rpt .= "ORDER BY rpt_id ASC ";

                                                $result_rpt = mysql_query($sql_rpt);
                                                while ($row_rpt = mysql_fetch_array($result_rpt)) {

                                                    $sum_people = $sum_people + ($row_rpt['rpt_adult_num'] + $row_rpt['rpt_child_num']);

                                                }  //while($row_rpt = mysql_fetch_array($result_rpt)){

                                            }  //if($_POST['producttype]=="Pick Up Transfer"){


                                            if ($_POST['producttype'] == "Tour") {

                                                // Query Reservations Tour item
                                                $sql_rtt = "SELECT * ";
                                                $sql_rtt .= "FROM reservation_tour_items ";
                                                $sql_rtt .= "WHERE reservations_id = '" . $row_res['res_id'] . "' ";
                                                $sql_rtt .= "AND tours_id = '" . $_POST['product_id'] . "' ";
                                                $sql_rtt .= "AND rtt_travel_date = '$c_date' ";
                                                $sql_rtt .= "ORDER BY rtt_id ASC ";

                                                $result_rtt = mysql_query($sql_rtt);
                                                while ($row_rtt = mysql_fetch_array($result_rtt)) {

                                                    $sum_people = $sum_people + ($row_rtt['rtt_adult_num'] + $row_rtt['rtt_child_num']);

                                                }  //while($row_rtt = mysql_fetch_array($result_rtt)){

                                            }  //if($_POST['producttype']=="Tour"){


                                            if ($_POST['producttype'] == "Activity") {

                                                // Query Reservations Activity item
                                                $sql_rat = "SELECT * ";
                                                $sql_rat .= "FROM reservation_activity_items ";
                                                $sql_rat .= "WHERE reservations_id = '" . $row_res['res_id'] . "' ";
                                                $sql_rat .= " AND activities_id = '" . $_POST['product_id'] . "' ";
                                                $sql_rat .= " AND rat_travel_date = '$c_date' ";
                                                $sql_rat .= "ORDER BY rat_id ASC ";

                                                $result_rat = mysql_query($sql_rat);
                                                while ($row_rat = mysql_fetch_array($result_rat)) {

                                                    $sum_people = $sum_people + ($row_rat['rat_adult_num'] + $row_rat['rat_child_num']);

                                                }  //while($row_rat = mysql_fetch_array($result_rat)){

                                            }  //if($_POST['producttype]=="Activity"){


                                            if ($_POST['producttype'] == "Bus Transfer") {

                                                // Query Reservations Bus Transfer item
                                                $sql_rct = "SELECT * ";
                                                $sql_rct .= "FROM reservation_bustransfer_items ";
                                                $sql_rct .= "WHERE reservations_id = '" . $row_res['res_id'] . "' ";
                                                $sql_rct .= "AND bustransfers_id = '" . $_POST['product_id'] . "' ";
                                                $sql_rct .= "AND rct_travel_date = '$c_date' ";
                                                $sql_rct .= "ORDER BY rct_id ASC ";

                                                $result_rct = mysql_query($sql_rct);
                                                while ($row_rct = mysql_fetch_array($result_rct)) {

                                                    $sum_people = $sum_people + ($row_rct['rct_adult_num'] + $row_rct['rct_child_num']);

                                                }  //while($row_rct = mysql_fetch_array($result_rct)){

                                            }  //if($_POST['producttype''] == "Bus Transfer"){


                                            if ($_POST['producttype'] == "Private Land Transfer") {

                                                // Query Reservations Private Land Transfer item
                                                $sql_rplt = "SELECT * ";
                                                $sql_rplt .= "FROM reservation_privatelandtransfer_items ";
                                                $sql_rplt .= "WHERE reservations_id = '" . $row_res['res_id'] . "' ";
                                                $sql_rplt .= "AND privatelandtransfers_id = '" . $_POST['product_id'] . "' ";
                                                $sql_rplt .= "AND rplt_travel_date = '$c_date' ";
                                                $sql_rplt .= "ORDER BY rplt_id ASC ";

                                                $result_rplt = mysql_query($sql_rplt);
                                                while ($row_rplt = mysql_fetch_array($result_rplt)) {

                                                    $sum_people = $sum_people + ($row_rplt['rplt_adult_num'] + $row_rplt['rplt_child_num']);

                                                }  //while($row_rplt = mysql_fetch_array($result_rplt)){

                                            }  //if($_POST[producttype] == "Private Land Transfer"){


                                            if ($_POST['producttype'] == "Hotel") {

                                                // Query Reservations Hotel item
                                                $sql_rht = "SELECT * ";
                                                $sql_rht .= "FROM reservation_hotel_items ";
                                                $sql_rht .= "WHERE reservations_id = '" . $row_res['res_id'] . "' ";
                                                $sql_rht .= "AND hotels_id = '" . $_POST['product_id'] . "' ";
                                                $sql_rht .= "AND rht_check_in = '$c_date' ";
                                                $sql_rht .= "ORDER BY rht_id ASC ";

                                                $result_rht = mysql_query($sql_rht);
                                                while ($row_rht = mysql_fetch_array($result_rht)) {

                                                    $sum_people = $sum_people + ($row_rht['rht_adult_num'] + $row_rht['rht_child_num']);

                                                }  //while($row_rht = mysql_fetch_array($result_rht)){

                                            }  //if($_POST['producttype'] == "Hotel"){


                                            if ($_POST['producttype'] == "Train Transfer") {

                                                // Query Reservations Train Transfer item
                                                $sql_rrt = "SELECT * ";
                                                $sql_rrt .= "FROM reservation_traintransfer_items ";
                                                $sql_rrt .= "WHERE reservations_id = '" . $row_res['res_id'] . "' ";
                                                $sql_rrt .= " AND traintransfers_id = '" . $_POST['product_id'] . "' ";
                                                $sql_rrt .= " AND rrt_travel_date = '$c_date' ";
                                                $sql_rrt .= "ORDER BY rrt_id ASC ";

                                                $result_rrt = mysql_query($sql_rrt);
                                                while ($row_rrt = mysql_fetch_array($result_rrt)) {

                                                    $sum_people = $sum_people + ($row_rrt['rrt_adult_num'] + $row_rrt['rrt_child_num']);

                                                }  //while($row_rrt = mysql_fetch_array($result_rrt)){

                                            }  //if($_POST['producttype'] == "Train Transfer"){


                                        } //while($row_res = mysql_fetch_array($result_res)){

                                        $c_date = date("Y-m-d", strtotime("+1 day", strtotime($c_date)));

                                        $sum_people_arr[$x][$y] = $sum_people;

                                        $sum_people = $sum_people_arr[$x][$y] + $sum_people_combo_arr[$x][$y];

                                        $sum_col[$x][$y] = $sum_people_arr[$x][$y] + $sum_people_combo_arr[$x][$y];

                                        //echo $sum_people;
                                        echo $sum_col[$x][$y];

                                        $total = $total + $sum_people; ?>

                                    </td>

                                    <?php
                                    $x++;
                                } //while(date($c_date <= $_POST['to_date])){

                                $total_sum = $total_sum + $total;
                                ?>

                                <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?= $total ?></td>
                            </tr>

                            <?php // End B to C (Internet User) ?>

                            <?php
                            //$sum_td = $x + 2;
                            $sum_td = $x - 1;
                            ?>

                            <tr>
                                <td colspan="3" align="right" class="txt_bold_gray"><b>TOTAL</b></td>

                                <?php for ($i = 1;
                                           $i <= $sum_td;
                                           $i++) {

                                    $total_col = 0;

                                    for ($p = 1;
                                         $p <= $y;
                                         $p++) {

                                        $total_col = $total_col + $sum_col[$i][$p];

                                    } // END
                                    ?>

                                    <td align="center" class="txt_bold_gray" bgcolor="#F0F0F0"><b><?= $total_col ?></b>
                                    </td>

                                    <?php
                                } // END for($i=1;$i<=$sum_td;$i++) 
                                ?>

                                <td align="center" class="txt_bold_gray" bgcolor="#F0F0F0"><b><?= $total_sum ?></b></td>
                            </tr>

                        </table>


                        <!---- Search Box ---->
                    </td>
                    <td align="right" valign="bottom"></td>
                </tr>
            </table>

            <!---- Listing Body ---->
        </td>
    </tr>
</table>