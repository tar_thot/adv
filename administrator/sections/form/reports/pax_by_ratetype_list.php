<?php

function setNumberLength($num, $length)
{
    $sumstr = strlen($num);
    $zero = str_repeat("0", $length - $sumstr);
    $results = $zero . $num;

    return $results;
}

function DateDiff($strDate1, $strDate2)
{
    return (strtotime($strDate2) - strtotime($strDate1)) / (60 * 60 * 24);  // 1 day = 60*60*24
}

?>

<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td valign="top">
            <table width="100%" border="0" cellspacing="0" cellpadding="3">
                <tr>
                    <td class="txt_big_gray">Pax by Rate Type List</td>
                    <td width="500" align="right"><a href="./index.php?mode=reports/pax_by_ratetype_st1"
                                                     style="background-color:#ffffff; color:#000000"><<< Back</a></td>
                </tr>
            </table>
            <!---- Listing Body ---->
            <table width="100%" border="0" cellspacing="0" cellpadding="3">
                <tr>
                    <td>
                        <!---- Search Box ---->
                        <br/>

                        <?php //$strNewDate = date("Y-m-d", strtotime("+3 day", strtotime($_POST['to_date]))); ?>

                        <table align="center">
                            <tr>
                                <?php
                                $sql = "SELECT * FROM boattransfers WHERE bot_id = '" . $_POST['product_id'] . "' ORDER BY bot_id ASC";
                                $results = mysql_query($sql);
                                $row = mysql_fetch_array($results);

                                $sql_type = "SELECT * FROM boattransfer_ratetypes WHERE botrt_id = '" . $_POST['botrt_id'] . "' ORDER BY botrt_id ASC";
                                $results_type = mysql_query($sql_type);
                                $row_type = mysql_fetch_array($results_type);
                                ?>
                                <td class="txt_bold_gray" align="center"><?= $row['bot_name'] ?>
                                    [ <?= $row_type['botrt_name'] ?> ] -
                                    (<?= DateFormat($_POST['from_date'], "f") ?>)
                                </td>

                            </tr>
                        </table>


                        <?php // Reservations Boat Transfer ?>

                        <br/>

                        <table border="1" cellspacing="0" cellpadding="3" bgcolor="#FFFFFF" bordercolor="#000000">
                            <tr bgcolor="#CCCCCC">
                                <td width="50" align="center" class="txt_bold_gray">CODE</td>
                                <td width="200" class="txt_bold_gray" align="center">AGENT</td>
                                <td width="200" class="txt_bold_gray" align="center">PASSENGER'S NAME</td>
                                <td width="50" class="txt_bold_gray" align="center">TYPE</td>
                                <td width="50" class="txt_bold_gray" align="center">ADULT</td>
                                <td width="50" class="txt_bold_gray" align="center">CHILD</td>
                                <td width="20" class="txt_bold_gray" align="center">TOTAL</td>
                                <td width="300" class="txt_bold_gray" align="center">REMARK</td>
                            </tr>

                            <?php $total_adult = 0;
                            $total_child = 0;
                            $total_pax = 0;

                            // Query Reservations Boat Transfer

                            $sql_res = "SELECT * ";
                            $sql_res .= "FROM reservation_boattransfer_items ";
                            $sql_res .= "WHERE boattransfers_id = '" . $_POST['product_id'] . "' ";
                            $sql_res .= "AND boattransferratetypes_id = '" . $_POST['botrt_id'] . "' ";
                            $sql_res .= "AND rbt_travel_date = '" . $_POST['from_date'] . "' ";

                            $sql_res .= "ORDER BY rbt_id ASC ";

                            //echo $sql_res;

                            $result_res = mysql_query($sql_res);
                            while ($row_res = mysql_fetch_array($result_res)) {
                                ?>
                                <?php
                                $agent_id = get_value('reservations', 'res_id', 'agents_id', $row_res['reservations_id']);

                                $code_agent = get_value('agents', 'ag_id', 'ag_ref', $agent_id);
                                $agent_name = get_value('agents', 'ag_id', 'ag_name', $agent_id);
                                $agentpaytype_id = get_value('agents', 'ag_id', 'agentpaytype_id', $agent_id);
                                $res_email = get_value('reservations', 'res_id', 'res_email', $row_res['reservations_id']);

                                $res_book_by = get_value('reservations', 'res_id', 'res_book_by', $row_res['reservations_id']);

                                $titlename_id = get_value('reservations', 'res_id', 'titlename_id', $row_res['reservations_id']);
                                $lis_name = get_value('lis_titlename', 'lis_id', 'lis_name', $titlename_id);
                                $res_fname = get_value('reservations', 'res_id', 'res_fname', $row_res['reservations_id']);
                                $res_lname = get_value('reservations', 'res_id', 'res_lname', $row_res['reservations_id']);

                                $bookingstatus_id = get_value('reservations', 'res_id', 'bookingstatus_id', $row_res['reservations_id']);

                                $res_cash_collection = get_value('reservations', 'res_id', 'res_cash_collection', $row_res['reservations_id']);
                                $res_agent_voucher = get_value('reservations', 'res_id', 'res_agent_voucher', $row_res['reservations_id']);
                                $res_request = get_value('reservations', 'res_id', 'res_request', $row_res['reservations_id']);

                                $pax = $row_res['rbt_adult_num'] + $row_res['rbt_child_num'];

                                $res_id_str = get_value('reservations', 'res_id', 'res_id_str', $row_res['reservations_id']);

                                if ($bookingstatus_id == 3) {
                                    ?>

                                    <tr>
                                        <td class="txt_bold_gray" align="center"
                                            bgcolor="#F0F0F0"><?= $res_id_str ?></td>
                                        <td class="txt_bold_gray"
                                            bgcolor="#F0F0F0"><?= $agent_name ?><?php if (isset($agent_name)) { ?>
                                                <br/>(<?= $code_agent ?>) <?php } ?></td>
                                        <td class="txt_bold_gray"
                                            bgcolor="#F0F0F0"><?= $lis_name ?> <?= $res_fname ?> <?= $res_lname ?></td>
                                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0">OW</td>
                                        <td class="txt_bold_gray" align="center"
                                            bgcolor="#F0F0F0"><?= $row_res['rbt_adult_num'] ?></td>
                                        <td class="txt_bold_gray" align="center"
                                            bgcolor="#F0F0F0"><?= $row_res['rbt_child_num'] ?></td>
                                        <!--<td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?= $row_res['rbt_infant_num'] ?></td>-->
                                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?= $pax ?></td>
                                        <td class="txt_bold_gray" bgcolor="#F0F0F0"><?= nl2br($res_request) ?></td>

                                    </tr>

                                    <?php
                                    $total_adult = $total_adult + $row_res['rbt_adult_num'];
                                    $total_child = $total_child + $row_res['rbt_child_num'];
                                    $total_pax = $total_pax + $pax;

                                } //if($bookingstatus_id == 3){
                                ?>

                            <?php } ?>

                            <tr>
                                <td colspan="4" class="txt_bold_gray" align="right"><b>TOTAL</b></td>
                                <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?= $total_adult ?></td>
                                <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?= $total_child ?></td>
                                <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?= $total_pax ?></td>
                                <td class="txt_bold_gray" align="center">&nbsp;</td>
                            </tr>

                        </table>

                        <!---- Search Box ---->
                    </td>
                    <td align="right" valign="bottom"></td>
                </tr>
            </table>

            <!---- Listing Body ---->
        </td>
    </tr>
</table>