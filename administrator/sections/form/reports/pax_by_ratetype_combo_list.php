<?php

function setNumberLength($num, $length)
{
    $sumstr = strlen($num);
    $zero = str_repeat("0", $length - $sumstr);
    $results = $zero . $num;

    return $results;
}

function DateDiff($strDate1, $strDate2)
{
    return (strtotime($strDate2) - strtotime($strDate1)) / (60 * 60 * 24);  // 1 day = 60*60*24
}

?>

<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td valign="top">
            <table width="100%" border="0" cellspacing="0" cellpadding="3">
                <tr>
                    <td class="txt_big_gray">Pax by Rate Type List</td>
                    <td width="500" align="right"><a href="./index.php?mode=reports/pax_by_ratetype_st1"
                                                     style="background-color:#ffffff; color:#000000"><<< Back</a></td>
                </tr>
            </table>
            <!---- Listing Body ---->
            <table width="100%" border="0" cellspacing="0" cellpadding="3">
                <tr>
                    <td>
                        <!---- Search Box ---->
                        <br/>

                        <?php //$strNewDate = date("Y-m-d", strtotime("+3 day", strtotime($_POST[to_date]))); ?>

                        <table align="center">
                            <tr>
                                <?php
                                $sql = "SELECT * FROM packages WHERE pac_id = '" . $_POST['combo_id'] . "' ORDER BY pac_id ASC";
                                $results = mysql_query($sql);
                                $row = mysql_fetch_array($results);

                                $sql_type = "SELECT * FROM package_ratetypes WHERE pacrt_id = '" . $_POST['pacrt_id'] . "' ORDER BY pacrt_id ASC";
                                $results_type = mysql_query($sql_type);
                                $row_type = mysql_fetch_array($results_type);
                                ?>
                                <td class="txt_bold_gray" align="center"><?= $row['pac_name'] ?>
                                    [ <?= $row_type['pacrt_name'] ?> ] - (<?= DateFormat($_POST['from_date'], "f") ?>)
                                </td>

                            </tr>
                        </table>


                        <?php // Reservations Combo Product ?>

                        <br/>

                        <table border="1" cellspacing="0" cellpadding="3" bgcolor="#FFFFFF" bordercolor="#000000">
                            <tr bgcolor="#CCCCCC">
                                <td width="50" align="center" class="txt_bold_gray">CODE</td>
                                <td width="200" class="txt_bold_gray" align="center">AGENT</td>
                                <td width="200" class="txt_bold_gray" align="center">PASSENGER'S NAME</td>
                                <td width="50" class="txt_bold_gray" align="center">TYPE</td>
                                <td width="50" class="txt_bold_gray" align="center">ADULT</td>
                                <td width="50" class="txt_bold_gray" align="center">CHILD</td>
                                <td width="20" class="txt_bold_gray" align="center">TOTAL</td>
                                <td width="300" class="txt_bold_gray" align="center">REMARK</td>
                            </tr>


                            <?php $total_adult = 0;
                            $total_child = 0;
                            $total_pax = 0;

                            // Query Reservations Boat Transfer (Combo Product)

                            $pacrt_name = get_value('package_ratetypes', 'pacrt_id', 'pacrt_name', $_POST['pacrt_id']);

                            $sql_resc = "SELECT * ";
                            $sql_resc .= "FROM reservationpackage_item ";
                            $sql_resc .= "WHERE rpt_item_producttype_id_arr LIKE '%~1~%' ";
                            $sql_resc .= "AND rpt_item_id_arr LIKE '%~" . $_POST['product_id'] . "~%' ";
                            $sql_resc .= "AND rpt_ratetype = '$pacrt_name' ";
                            $sql_resc .= "AND rpt_item_travel_date_arr LIKE '%~" . $_POST['from_date'] . "~%' ";
                            $sql_resc .= "ORDER BY rpt_id ASC ";

                            //echo $sql_resc;

                            $result_resc = mysql_query($sql_resc);
                            while ($row_resc = mysql_fetch_array($result_resc)) {

                                $agent_id = get_value('reservation_packages', 'rpa_id', 'agents_id', $row_resc['reservationpackages_id']);
                                $code_agent = get_value('agents', 'ag_id', 'ag_ref', $agent_id);
                                $agent_name = get_value('agents', 'ag_id', 'ag_name', $agent_id);
                                $rpa_email = get_value('reservation_packages', 'rpa_id', 'rpa_email', $row_resc['reservationpackages_id']);

                                $res_book_by = get_value('reservation_packages', 'rpa_id', 'rpa_bookby', $row_resc['reservationpackages_id']);

                                $titlename_id = get_value('reservation_packages', 'rpa_id', 'titlename_id', $row_resc['reservationpackages_id']);
                                $lis_name = get_value('lis_titlename', 'lis_id', 'lis_name', $titlename_id);
                                $rpa_fname = get_value('reservation_packages', 'rpa_id', 'rpa_fname', $row_resc['reservationpackages_id']);
                                $rpa_lname = get_value('reservation_packages', 'rpa_id', 'rpa_lname', $row_resc['reservationpackages_id']);

                                $bookingstatus_id = get_value('reservation_packages', 'rpa_id', 'bookingstatus_id', $row_resc['reservationpackages_id']);

                                $ticket_type_id = get_value('packages', 'pac_id', 'ticket_type_id', $row_resc['packages_id']);
                                $class_id = get_value('packages', 'pac_id', 'class_id', $row_resc['packages_id']);

                                $ticket_type_name = get_value('lis_ticket_type', 'lis_id', 'lis_name', $ticket_type_id);
                                $class_name = get_value('lis_class', 'lis_id', 'lis_name', $class_id);

                                $rpa_cash_collection = get_value('reservation_packages', 'rpa_id', 'rpa_cash_collection', $row_resc['reservationpackages_id']);
                                $rpa_agent_voucher = get_value('reservation_packages', 'rpa_id', 'rpa_agent_voucher', $row_resc['reservationpackages_id']);
                                $rpa_request = get_value('reservation_packages', 'rpa_id', 'rpa_request', $row_resc['reservationpackages_id']);

                                $pax = $row_resc['rpt_adult_num'] + $row_resc['rpt_child_num'];

                                $ticket_type_id = get_value('packages', 'pac_id', 'ticket_type_id', $row_resc['packages_id']);

                                $ticket_type = get_value('lis_ticket_type', 'lis_id', 'lis_name', $ticket_type_id);

                                $rpa_id_str = get_value('reservation_packages', 'rpa_id', 'rpa_id_str', $row_resc['reservationpackages_id']);

                                if ($bookingstatus_id == 3) {

                                    $sql_arr = "SELECT * ";
                                    $sql_arr .= "FROM reservationpackage_item ";
                                    $sql_arr .= "WHERE rpt_id = " . $row_resc['rpt_id'] . " ";
                                    $sql_arr .= "ORDER BY rpt_id ASC ";

                                    //echo $sql_arr;

                                    $result_arr = mysql_query($sql_arr);
                                    $row_arr = mysql_fetch_array($result_arr);

                                    $rpt_item_travel_date = explode("~", $row_arr['rpt_item_travel_date_arr']);
                                    $rpt_item_producttype_id = explode("~", $row_arr['rpt_item_producttype_id_arr']);
                                    $rpt_item_id = explode("~", $row_arr['rpt_item_id_arr']);

                                    for ($i = 1; $i < count($rpt_item_travel_date); $i++) {

                                        if ($rpt_item_travel_date[$i] == $_POST['from_date'] && $rpt_item_producttype_id[$i] == 1 && $rpt_item_id[$i] == $_POST['product_id']) {
                                            ?>
                                            <tr>
                                                <td class="txt_bold_gray" align="center"
                                                    bgcolor="#F0F0F0"><?= $rpa_id_str ?></td>
                                                <td class="txt_bold_gray"
                                                    bgcolor="#F0F0F0"><?= $agent_name ?><?php if ($agent_name) { ?>
                                                        <br/>(<?= $code_agent ?>) <?php } ?></td>
                                                <td class="txt_bold_gray"
                                                    bgcolor="#F0F0F0"><?= $lis_name ?> <?= $rpa_fname ?> <?= $rpa_lname ?></td>
                                                <td class="txt_bold_gray" align="center"
                                                    bgcolor="#F0F0F0"><?= $ticket_type ?></td>
                                                <td class="txt_bold_gray" align="center"
                                                    bgcolor="#F0F0F0"><?= $row_arr['rpt_adult_num'] ?></td>
                                                <td class="txt_bold_gray" align="center"
                                                    bgcolor="#F0F0F0"><?= $row_arr['rpt_child_num'] ?></td>
                                                <td class="txt_bold_gray" align="center"
                                                    bgcolor="#F0F0F0"><?= $pax ?></td>
                                                <td class="txt_bold_gray"
                                                    bgcolor="#F0F0F0"><?= nl2br($rpa_request) ?></td>
                                            </tr>
                                            <?php
                                        }
                                    }
                                    ?>
                                    <?php $total_adult = $total_adult + $row_arr['rpt_adult_num'];
                                    $total_child = $total_child + $row_arr['rpt_child_num'];
                                    $total_pax = $total_pax + $pax;

                                } ?>

                            <?php } ?>

                            <tr>
                                <td colspan="4" class="txt_bold_gray" align="right"><b>TOTAL</b></td>
                                <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?= $total_adult ?></td>
                                <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?= $total_child ?></td>
                                <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?= $total_pax ?></td>
                                <td class="txt_bold_gray" align="center">&nbsp;</td>
                            </tr>

                        </table>

                        <!---- Search Box ---->
                    </td>
                    <td align="right" valign="bottom"></td>
                </tr>
            </table>

            <!---- Listing Body ---->
        </td>
    </tr>
</table>