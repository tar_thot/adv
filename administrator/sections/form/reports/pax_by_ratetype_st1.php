<?php

function setNumberLength($num, $length)
{
    $sumstr = strlen($num);
    $zero = str_repeat("0", $length - $sumstr);
    $results = $zero . $num;

    return $results;
}

?>

<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td valign="top">
            <table width="100%" border="0" cellspacing="0" cellpadding="3">
                <tr>
                    <td class="txt_big_gray">Pax by Rate Type</td>
                </tr>
            </table>
            <!---- Listing Body ---->
            <table width="100%" border="0" cellspacing="0" cellpadding="3">
                <tr>
                    <td>
                        <!---- Search Box ---->
                        <form name="formBoat" method="post" action="./index.php?mode=reports/pax_by_ratetype_st2">
                            <table border="0" cellspacing="3" cellpadding="3" style="border: solid #CCCCCC 1px;">
                                <tr>
                                    <td class="txt_bold_gray" align="right">Boat Transfer<br/>
                                        <select name="product_id" style="width:200px;">
                                            <option value="">-- Select --</option>
                                            <?php listbox('boattransfers', 'bot_id', 'bot_name', 'N', 'N'); ?>
                                        </select></td>
                                    <td class="txt_bold_gray" align="right">&nbsp;</td>
                                    <td class="txt_bold_gray" align="right">&nbsp;</td>
                                </tr>

                                <tr>
                                    <td class="txt_bold_gray" align="right">&nbsp;</td>
                                    <td class="txt_bold_gray" align="right">&nbsp;</td>
                                    <td class="txt_bold_gray" align="right">&nbsp;</td>
                                    <td class="txt_bold_gray" align="right"><input type="submit" name="Submit"
                                                                                   value="Continue"
                                                                                   style="height:25px;"></td>
                                </tr>

                            </table>
                        </form>

                        <br/>
                        <strong><font color="#FF0000">OR</font></strong>
                        <br/><br/>

                        <form name="formCombo" method="post"
                              action="./index.php?mode=reports/pax_by_ratetype_combo_st2">
                            <table border="0" cellspacing="3" cellpadding="3" style="border: solid #CCCCCC 1px;">
                                <tr>
                                    <td class="txt_bold_gray" align="right">Combo Product<br/>
                                        <select name="combo_id" style="width:200px;">
                                            <option value="">-- Select --</option>
                                            <?php listbox('packages', 'pac_id', 'pac_name', 'N', 'N'); ?>
                                        </select></td>
                                    <td class="txt_bold_gray" align="right">&nbsp;</td>
                                    <td class="txt_bold_gray" align="right">&nbsp;</td>
                                </tr>


                                <tr>
                                    <td class="txt_bold_gray" align="right">&nbsp;</td>
                                    <td class="txt_bold_gray" align="right">&nbsp;</td>
                                    <td class="txt_bold_gray" align="right">&nbsp;</td>
                                    <td class="txt_bold_gray" align="right"><input type="submit" name="Submit"
                                                                                   value="Continue"
                                                                                   style="height:25px;"></td>
                                </tr>

                            </table>
                        </form>

                        <!---- Search Box ---->
                    </td>
                    <td align="right" valign="bottom"></td>
                </tr>
            </table>

            <!---- Listing Body ---->
        </td>
    </tr>
</table>