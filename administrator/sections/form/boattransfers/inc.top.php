<table width="100%" border="0" cellspacing="0" cellpadding="3">
    <tr>
        <td>
            <table border="0" cellspacing="1" cellpadding="3" bgcolor="<?= $color_1 ?>">
                <tr bgcolor="<?= $color_3 ?>">
                    <td bgcolor="<?= $color_2 ?>" align="right" class="txt_bold_gray">Name :</td>
                    <td><?= $row['bot_name'] ?></td>
                </tr>
                <tr bgcolor="<?= $color_3 ?>">
                    <td bgcolor="<?= $color_2 ?>" align="right" class="txt_bold_gray">Country :</td>
                    <td><?= get_value('boattransfer_con_country', 'con_id', 'con_name', $row['country_id']) ?></td>
                </tr>
                <tr bgcolor="<?= $color_3 ?>">
                    <td bgcolor="<?= $color_2 ?>" align="right" class="txt_bold_gray">Province/State :</td>
                    <td><?= get_value('boattransfer_con_province', 'con_id', 'con_name', $row['province_id']) ?></td>
                </tr>


                <tr bgcolor="<?= $color_3 ?>">
                    <td bgcolor="<?= $color_2 ?>" align="right" class="txt_bold_gray">ประเภทการกำหนดราคา :</td>
                    <td><?= get_value('boattransfer_price_type', 'c_id', 'c_name', $row['boattransferpricetype_id']) ?></td>
                </tr>

                <tr bgcolor="<?= $color_3 ?>">
                    <td bgcolor="<?= $color_2 ?>" align="right" class="txt_bold_gray">การคำนวณราคา :</td>
                    <td>รายคน</td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<table width="100%" border="0" cellspacing="1" cellpadding="3" bgcolor="<?= $color_inctop1 ?>">
    <tr bgcolor="<?= $color_inctop2 ?>">
        <td><span class="txt_bold_white">
        <a href="./?mode=boattransfers/add&id=<?= $id ?>" class="txt_bold_white">Edit</a><strong
                    style="color:<?= $color_inctop1 ?>;"> | </strong>
        <a href="./?mode=boattransfers/ratetypes_index&id=<?= $id ?>" class="txt_bold_white">Rate Type</a><strong
                    style="color:<?= $color_inctop1 ?>;"> | </strong>
        <a href="./?mode=boattransfers/period_index&id=<?= $id ?>" class="txt_bold_white">Period</a><strong
                    style="color:<?= $color_inctop1 ?>;"> | </strong>
        <a href="./?mode=boattransfers/rate_index&id=<?= $id ?>" class="txt_bold_white">Rate Setting</a><strong
                    style="color:<?= $color_inctop1 ?>;"> | </strong>
        <a href="./?mode=boattransfers/allocation_index&id=<?= $id ?>" class="txt_bold_white">Allocation
            Setting</a><strong style="color:<?= $color_inctop1 ?>;"> | </strong>
        </span></td>
    </tr>
</table>
<br/>
