<?php
include("value_get.php");

if (isset($id)) {
    $sql = "select * from boattransfers where bot_id = $id";
    $result = mysql_query($sql);
    $row = mysql_fetch_array($result);
    include("inc.top.php");
    if ($botrt_id) {
        $page_title = "Update Rate Type";
        $sql = "select * from boattransfer_ratetypes where botrt_id = $botrt_id";
        $result = mysql_query($sql);
        $row = mysql_fetch_array($result);
    } else {
        $row = "";
        $page_title = "Create New Rate Type";
    }
    ?>
    <script language="JavaScript" type="text/JavaScript">
        <!--
        function MM_findObj(n, d) { //v4.01
            var p, i, x;
            if (!d) d = document;
            if ((p = n.indexOf("?")) > 0 && parent.frames.length) {
                d = parent.frames[n.substring(p + 1)].document;
                n = n.substring(0, p);
            }
            if (!(x = d[n]) && d.all) x = d.all[n];
            for (i = 0; !x && i < d.forms.length; i++) x = d.forms[i][n];
            for (i = 0; !x && d.layers && i < d.layers.length; i++) x = MM_findObj(n, d.layers[i].document);
            if (!x && d.getElementById) x = d.getElementById(n);
            return x;
        }
        function MM_validateForm() { //v4.0
            var i, p, q, nm, test, num, min, max, errors = '', args = MM_validateForm.arguments;
            for (i = 0; i < (args.length - 2); i += 3) {
                test = args[i + 2];
                val = MM_findObj(args[i]);
                if (val) {
                    nm = val.name;
                    if ((val = val.value) != "") {
                        if (test.indexOf('isEmail') != -1) {
                            p = val.indexOf('@');
                            if (p < 1 || p == (val.length - 1)) errors += '- E-mail must contain an e-mail address.\n';
                        } else if (test != 'R') {
                            num = parseFloat(val);
                            if (isNaN(val)) {
                                if (nm == 'name') nm = 'Room Type';
                                errors += '- ' + nm + ' must contain a number.\n';
                            }
                            if (test.indexOf('inRange') != -1) {
                                p = test.indexOf(':');
                                min = test.substring(8, p);
                                max = test.substring(p + 1);
                                if (num < min || max < num) errors += '- ' + nm + ' must contain a number between ' + min + ' and ' + max + '.\n';
                            }
                        }
                    } else if (test.charAt(0) == 'R') {
                        if (nm == 'botrt_name') nm = 'Rate Type';
                        errors += '- ' + nm + ' is required.\n';
                    }
                }
            }
            if (errors) alert('The following error(s) occurred:\n' + errors);
            document.MM_returnValue = (errors == '');
        }
        //-->
    </script>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="txt_big_gray"><?= $page_title ?></td>
        </tr>
    </table>
    <form action="process.php?mode=boattransfers/ratetypes_save&id=<?= $id ?>&botrt_id=<?= $botrt_id ?>" onsubmit="MM_validateForm(
'botrt_name','','R'
);return document.MM_returnValue" name="form" method="post" enctype="multipart/form-data">
        <table width="100%" border="0" cellspacing="0" cellpadding="3">
            <tr>
                <td align="right" class="txt_bold_gray" width="200">Rate Type :</td>
                <td><input type="text" name="botrt_name" value="<?= $row['botrt_name'] ?>" size="50"><span
                        class="remark"> * </span></td>
            </tr>


            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td><input type="submit" name="submit" value="SUBMIT" style="width:75px;"><?php
                    if (isset($botrt_id)) echo " <input type=\"submit\" name=\"del\" value=\"DELETE\" style=\"width:75px;\" onClick=\"return confirm('Do you want to delete this record ?')\">";
                    else echo " <input type=\"reset\" name=\"Reset\" value=\"RESET\" style=\"width:75px;\">";
                    ?></td>
            </tr>
        </table>
    </form>
    <?php
}
?>
