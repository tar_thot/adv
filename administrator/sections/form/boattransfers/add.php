<?php
include("value_get.php");

if ($id) {
    $page_title = "Update Boat Transfer";
    $sql = "select * from boattransfers where bot_id = $id";
    $result = mysql_query($sql);
    $row = mysql_fetch_array($result);
    include("inc.top.php");
} else {
    $page_title = "Create New Boat Transfer";
}
?>

<script language="JavaScript" type="text/JavaScript">
    <!--
    function MM_findObj(n, d) { //v4.01
        var p, i, x;
        if (!d) d = document;
        if ((p = n.indexOf("?")) > 0 && parent.frames.length) {
            d = parent.frames[n.substring(p + 1)].document;
            n = n.substring(0, p);
        }
        if (!(x = d[n]) && d.all) x = d.all[n];
        for (i = 0; !x && i < d.forms.length; i++) x = d.forms[i][n];
        for (i = 0; !x && d.layers && i < d.layers.length; i++) x = MM_findObj(n, d.layers[i].document);
        if (!x && d.getElementById) x = d.getElementById(n);
        return x;
    }
    function MM_validateForm() { //v4.0
        var i, p, q, nm, test, num, min, max, errors = '', args = MM_validateForm.arguments;
        for (i = 0; i < (args.length - 2); i += 3) {
            test = args[i + 2];
            val = MM_findObj(args[i]);
            if (val) {
                nm = val.name;
                if ((val = val.value) != "") {
                    if (test.indexOf('isEmail') != -1) {
                        p = val.indexOf('@');
                        if (p < 1 || p == (val.length - 1)) errors += '- E-mail must contain an e-mail address.\n';
                    } else if (test != 'R') {
                        num = parseFloat(val);
                        if (isNaN(val)) {
                            if (nm == 'category_id') nm = 'Category';
                            errors += '- ' + nm + ' must contain a number.\n';
                        }
                        if (test.indexOf('inRange') != -1) {
                            p = test.indexOf(':');
                            min = test.substring(8, p);
                            max = test.substring(p + 1);
                            if (num < min || max < num) errors += '- ' + nm + ' must contain a number between ' + min + ' and ' + max + '.\n';
                        }
                    }
                } else if (test.charAt(0) == 'R') {
                    if (nm == 'category_id') nm = 'Category';
                    if (nm == 'bot_name') nm = 'Name';
                    if (nm == 'bot_ref') nm = 'Ref';
                    if (nm == 'check_bot_ref') nm = 'Ref';
                    if (nm == 'activitypricetype_id') nm = 'Price Type';
                    errors += '- ' + nm + ' is required.\n';
                }
            }
        }
        if (errors) alert('The following error(s) occurred:\n' + errors);
        document.MM_returnValue = (errors == '');
    }
    //-->
</script>

<script language="JavaScript" type="text/JavaScript">
    function checkCountryFroShowProvince() {


        //alert('country_id : ' + $("#country_id").val());
        $("#td_province_id").prepend("<img src='../admin_ajax/images/icon/ajax-loader.gif' /> ");

        $.post("../admin_ajax/ajx_boattransfer_provincelist.php", {
                data1: $("#country_id").val(),
                data2: $("#province_id").val()
            },
            function (result) {
                $("#td_province_id").html(result);
                checkProvinceFroShowArea();
            }
        );


    }

    function checkProvinceFroShowArea() {
        //alert('province_id : ' + $("#province_id").val());
        $("#td_area_id").prepend("<img src='../admin_ajax/images/icon/ajax-loader.gif' /> ");

        $.post("../admin_ajax/ajx_boattransfer_arealist.php", {
                data1: $("#province_id").val(),
                data2: $("#area_id").val()
            },
            function (result) {
                $("#td_area_id").html(result);
            }
        );
    }


</script>

<script language="JavaScript" type="text/JavaScript">
    $(document).ready(function () {
        checkCountryFroShowProvince();
        checkProvinceFroShowArea();

    });
</script>

<script type="text/javascript" src="./javascript/ckeditor/ckeditor.js"></script>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td class="txt_big_gray"><?= $page_title ?></td>
    </tr>
</table>
<form action="process.php?mode=boattransfers/save&id=<?= $id ?>" onsubmit="MM_validateForm(
'check_bot_ref','','R',
'bot_name','','R',
'bot_ref','','R'
);return document.MM_returnValue" name="form" method="post" enctype="multipart/form-data">
    <table width="100%" border="0" cellspacing="0" cellpadding="3">

        <tr>
            <td align="right" class="txt_bold_gray" width="200">Name :</td>
            <td><input type="text" name="bot_name" id="bot_name" value="<?= $row['bot_name'] ?>" size="50"></td>
        </tr>

        <script type="text/javascript">
            function checkRefRepeatedly() {
                //alert('checkEmailRepeatedly : ' + $("#bot_email").val());
                $("#spn_bot_ref").html("<img src='../admin_ajax/images/icon/ajax-loader.gif' /> ");

                $.post("../admin_ajax/ajx_ref_repeatedly.php", {
                        data1: $("#bot_ref").val(),
                        data2: 'bot_',
                        data3: 'boattransfers',
                        data4: '<?=$row[0]?>'
                    },
                    function (result) {
                        $("#spn_bot_ref").html(result);
                    }
                );
            }
        </script>

        <tr>
            <td align="right" class="txt_bold_gray" width="200">Ref :</td>
            <td><input type="text" name="bot_ref" id="bot_ref" value="<?= $row['bot_ref'] ?>" size="50"
                       onkeyup="checkRefRepeatedly();">
                <span id="spn_bot_ref" class="remark"> * <input type="hidden" name="check_bot_ref"
                                                                value="<?= $row['bot_ref'] ?>"></span></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="200">Account Code AMW :</td>
            <td>Adult <input type="text" name="ac_andaman_adult" id="ac_andaman_adult"
                             value="<?= $row['ac_andaman_adult'] ?>" size="20">&nbsp;&nbsp;&nbsp;
                Child <input type="text" name="ac_andaman_child" id="ac_andaman_child"
                             value="<?= $row['ac_andaman_child'] ?>" size="20"></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="200">Account Code ppholiday.com :</td>
            <td>Adult <input type="text" name="ac_dotcom_adult" id="ac_dotcom_adult"
                             value="<?= $row['ac_dotcom_adult'] ?>" size="20">&nbsp;&nbsp;&nbsp;
                Child <input type="text" name="ac_dotcom_child" id="ac_dotcom_child"
                             value="<?= $row['ac_dotcom_child'] ?>" size="20"></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="200">Account Code B to C :</td>
            <td>Adult <input type="text" name="ac_btoc_adult" id="ac_btoc_adult" value="<?= $row['ac_btoc_adult'] ?>"
                             size="20">&nbsp;&nbsp;&nbsp;
                Child <input type="text" name="ac_btoc_child" id="ac_btoc_child" value="<?= $row['ac_btoc_child'] ?>"
                             size="20"></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="200">Boat Trip Number :</td>
            <td><input type="text" name="bot_trip_number" id="bot_trip_number" value="<?= $row['bot_trip_number'] ?>"
                       size="50"></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="200">&lt;Meta Title&gt; :</td>
            <td><textarea name="meta_title" rows="3" cols="68"><?= $row['meta_title'] ?></textarea></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="200">&lt;Meta Keywords&gt; :</td>
            <td><textarea name="meta_keywords" rows="3" cols="68"><?= $row['meta_keywords'] ?></textarea></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="200">&lt;Meta Description&gt; :</td>
            <td><textarea name="meta_description" rows="3" cols="68"><?= $row['meta_description'] ?></textarea></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray"> Transfer Type :</td>
            <td><select name="boattransfer_type_id" id="boattransfer_type_id" style="width:200px">
                    <?php listbox('boattransfer_type', 'c_id', 'c_name', $row['boattransfer_type_id'], 'N'); ?>
                </select></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray">Country :</td>
            <td><select name="country_id" id="country_id" style="width:200px" onchange="checkCountryFroShowProvince();">
                    <option value="">-- Select --</option>
                    <?php listbox('boattransfer_con_country', 'con_id', 'con_name', $row['country_id'], 'N'); ?>
                </select></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray">Province :</td>
            <td id="td_province_id"><select name="province_id" id="province_id" style="width:200px"
                                            onchange="checkProvinceFroShowArea();">
                    <option value="">-- Select --</option>
                    <?php listbox('boattransfer_con_province', 'con_id', 'con_name', $row['province_id'], 'N'); ?>
                </select></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray">From :</td>
            <td id="td_area_id"><select name="area_id" id="area_id" style="width:200px">
                    <option value="">-- Select --</option>
                    <?php listbox('boattransfer_con_area', 'con_id', 'con_name', $row['area_id'], 'N'); ?>
                </select></td>
        </tr>


        <tr>
            <td align="right" class="txt_bold_gray">To :</td>
            <td><select name="area_to_id" id="area_to_id" style="width:200px">
                    <option value="">-- Select --</option>
                    <?php listbox('boattransfer_con_area', 'con_id', 'con_name', $row['area_to_id'], 'N'); ?>
                </select></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray">Time :</td>
            <td><select name="time_id" id="time_id" style="width:200px;">
                    <option value="">-- Select --</option>
                    <?php listbox('boattransfer_con_time', 'con_id', 'con_name', $row['time_id'], 'N'); ?>
                </select></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray">เวลาที่ใช้ในการเดินทาง :</td>
            <td><input type="text" name="bot_time" id="bot_time" value="<?= $row['bot_time'] ?>" size="50"></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray">Boat :</td>
            <td><select name="boat_id" id="boat_id" style="width:200px;">
                    <option value="">-- Select --</option>
                    <?php listbox('boattransfer_con_boat', 'con_id', 'boa_registration', $row['boat_id'], 'N'); ?>
                </select></td>
        </tr>


        <tr>
            <td align="right" class="txt_bold_gray">ประเภทของการกำหนดราคา :</td>
            <td><select name="boattransferpricetype_id" id="boattransferpricetype_id" style="width:200px">
                    <?php listbox('boattransfer_price_type', 'c_id', 'c_name', $row['boattransferpricetype_id'], 'N'); ?>
                </select> <span class="remark"> * </span></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray">Agent Supplier :</td>
            <td><select name="agents_sup_id" id="agents_sup_id" style="width:200px">
                    <option value="">-- Select --</option>
                    <?php listbox('suppliers', 'sp_id', 'sp_cname', $row['agents_sup_id'], 'N'); ?>
                </select></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray">Hot Deal :</td>
            <td><input type="checkbox" name="bot_hot" value="1" <?php if ($row['bot_hot'] == 1) {
                    echo 'checked';
                } ?> /></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray">Class :</td>
            <td><select name="boattransfer_class_id" id="boattransfer_class_id" style="width:200px;">
                    <option value="">-- Select --</option>
                    <?php listbox('boattransfer_class', 'c_id', 'c_name', $row['boattransfer_class_id'], 'N'); ?>
                </select></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="200">Remark :</td>
            <!--<td>
        <textarea name="bot_remark" id="bot_remark" rows="7" style="width:500px;"><?= $row['bot_remark'] ?></textarea></td>-->
            <td>
                <div
                    style="font-size:0.9em; color:#C00; padding:5px; width:490px; background-color:#FFE0DD; position:relative;">
                    - Press "Enter" = New paragraph<br/>
                    - Press "Shift+Enter" = New line
                </div>
                <br/>
                <textarea name="bot_remark" id="bot_remark" rows="7"
                          style="width:500px;"><?= $row['bot_remark'] ?></textarea>
                <script type="text/javascript">//<![CDATA[
                    CKEDITOR.replace('bot_remark');
                    //]]></script>
            </td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="200">Name Link Recommended :</td>
            <td><input type="text" name="bot_name_link" id="bot_name_link" value="<?= $row['bot_name_link'] ?>"
                       size="50"></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="200">URL Link :</td>
            <td><input type="text" name="bot_url" id="bot_url" value="<?= $row['bot_url'] ?>" size="50"></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray">Photo :</td>
            <td>
                <table border="0" cellspacing="1" cellpadding="3" bgcolor="<?= $color_1 ?>">
                    <tr bgcolor="#FFFFFF">
                        <td valign="bottom" class="txt_bold_gray">#1<br/>
                            <?php $img_tmp = $row['photo2'];
                            if ($img_tmp) { ?>
                                <img src="./resizer.php?imgfile=../photo/boattransfers/<?= $img_tmp ?>&size=200"
                                     border="0"><br>
                                <label><input type="checkbox" name="del_photo2" value="<?= $img_tmp ?>"><span
                                        class="remark">Delete Photo</span></label><br>
                            <?php } ?>
                            <input name="photo2" type="file"><input name="tmp_photo2" type="hidden"
                                                                    value="<?= $img_tmp ?>"></td>
                        <td valign="bottom" class="txt_bold_gray">#2<br/>
                            <?php $img_tmp = $row['photo3'];
                            if ($img_tmp) { ?>
                                <img src="./resizer.php?imgfile=../photo/boattransfers/<?= $img_tmp ?>&size=200"
                                     border="0"><br>
                                <label><input type="checkbox" name="del_photo3" value="<?= $img_tmp ?>"><span
                                        class="remark">Delete Photo</span></label><br>
                            <?php } ?>
                            <input name="photo3" type="file"><input name="tmp_photo3" type="hidden"
                                                                    value="<?= $img_tmp ?>"></td>
                    </tr>
                </table>
            </td>
        </tr>

        <tr>
            <td>&nbsp;</td>
            <td><span class="remark">
        - Support JPG Format Only<br>
        - Recommend For Width 490px Height 355px
        </span></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" valign="top">For Search :</td>
            <td>
                <?php
                $bot_forsearch_id_arr = explode("~", $row['bot_forsearch_id_arr']);
                $con_forsearch_sql = "select * from boattransfer_con_forsearch order by con_name";
                $con_forsearch_results = mysql_query($con_forsearch_sql);
                while ($con_forsearch_rows = mysql_fetch_array($con_forsearch_results)) {
                    ?>
                    <label>
                        <input type="checkbox" name="bot_forsearch_id_arr[]"
                               value="<?= $con_forsearch_rows[0] ?>" <?php if (in_array($con_forsearch_rows[0], $bot_forsearch_id_arr)) echo "checked"; ?> />
                        &nbsp;<?= $con_forsearch_rows['con_name'] ?></label> <br/>
                    <?php
                } // while ($con_forsearch_rows = mysql_fetch_array($con_forsearch_results))
                ?>
            </td>
        </tr>

        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td><input type="submit" name="submit" value="SUBMIT" style="width:75px;"><?php
                if ($id) echo " <input type=\"submit\" name=\"del\" value=\"DELETE\" style=\"width:75px;\" onClick=\"return confirm('Do you want to delete this record ?')\">";
                else echo " <input type=\"reset\" name=\"Reset\" value=\"RESET\" style=\"width:75px;\">";
                ?></td>
        </tr>
    </table>
</form>