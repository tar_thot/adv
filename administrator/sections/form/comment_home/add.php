<?php
// Convert Variable Array To Variable

while (list($xVarName, $xVarvalue) = each($_GET)) {
    ${$xVarName} = $xVarvalue;
}


while (list($xVarName, $xVarvalue) = each($_POST)) {
    ${$xVarName} = $xVarvalue;
}

while (list($xVarName, $xVarvalue) = each($_FILES)) {
    ${$xVarName . "_name"} = $xVarvalue['name'];
    ${$xVarName . "_type"} = $xVarvalue['type'];
    ${$xVarName . "_size"} = $xVarvalue['size'];
    ${$xVarName . "_error"} = $xVarvalue['error'];
    ${$xVarName} = $xVarvalue['tmp_name'];
}
if ($id) {
    $page_title = "Update Comment";
    $sql = "select * from comments_home where com_id = $id";
    $result = mysql_query($sql);
    $row = mysql_fetch_array($result);
} else {
    $page_title = "Create New Comment";
}

?>

<script language="JavaScript" type="text/JavaScript">
    <!--
    function MM_findObj(n, d) { //v4.01
        var p, i, x;
        if (!d) d = document;
        if ((p = n.indexOf("?")) > 0 && parent.frames.length) {
            d = parent.frames[n.substring(p + 1)].document;
            n = n.substring(0, p);
        }
        if (!(x = d[n]) && d.all) x = d.all[n];
        for (i = 0; !x && i < d.forms.length; i++) x = d.forms[i][n];
        for (i = 0; !x && d.layers && i < d.layers.length; i++) x = MM_findObj(n, d.layers[i].document);
        if (!x && d.getElementById) x = d.getElementById(n);
        return x;
    }
    function MM_validateForm() { //v4.0
        var i, p, q, nm, test, num, min, max, errors = '', args = MM_validateForm.arguments;
        for (i = 0; i < (args.length - 2); i += 3) {
            test = args[i + 2];
            val = MM_findObj(args[i]);
            if (val) {
                nm = val.name;
                if ((val = val.value) != "") {
                    if (test.indexOf('isEmail') != -1) {
                        p = val.indexOf('@');
                        if (p < 1 || p == (val.length - 1)) errors += '- E-mail must contain an e-mail address.\n';
                    } else if (test != 'R') {
                        num = parseFloat(val);
                        if (isNaN(val)) {
                            if (nm == 'cl_balance_credit') nm = 'Credit Balance';
                            if (nm == 'cl_min_credit') nm = 'Minimum of Credit available';
                            errors += '- ' + nm + ' must contain a number.\n';
                        }
                        if (test.indexOf('inRange') != -1) {
                            p = test.indexOf(':');
                            min = test.substring(8, p);
                            max = test.substring(p + 1);
                            if (num < min || max < num) errors += '- ' + nm + ' must contain a number between ' + min + ' and ' + max + '.\n';
                        }
                    }
                } else if (test.charAt(0) == 'R') {
                    if (nm == 'cl_name') nm = 'First Name';
                    if (nm == 'cl_lname') nm = 'Last Name';
                    if (nm == 'cl_email') nm = 'Email';
                    if (nm == 'check_cl_email') nm = 'Email';

                    errors += '- ' + nm + ' is required.\n';
                }
            }
        }
        if (errors) alert('The following error(s) occurred:\n' + errors);
        document.MM_returnValue = (errors == '');
    }
    //-->
</script>

<script language="JavaScript" type="text/JavaScript">
    function checkCountryFroShowProvince() {


        //alert('country_id : ' + $("#country_id").val());
        $("#td_province_id").prepend("<img src='../admin_ajax/images/icon/ajax-loader.gif' /> ");

        $.post("../admin_ajax/ajx_client_provincelist.php", {
                data1: $("#country_id").val(),
                data2: $("#province_id").val()
            },
            function (result) {
                $("#td_province_id").html(result);
                checkProvinceFroShowArea();
            }
        );


    }

    function checkProvinceFroShowCity() {
        //alert('province_id : ' + $("#province_id").val());
        $("#td_city_id").prepend("<img src='../admin_ajax/images/icon/ajax-loader.gif' /> ");

        $.post("../admin_ajax/ajx_client_citylist.php", {
                data1: $("#province_id").val(),
                data2: $("#city_id").val()
            },
            function (result) {
                $("#td_city_id").html(result);
            }
        );
    }


</script>

<script language="JavaScript" type="text/JavaScript">
    $(document).ready(function () {
        checkCountryFroShowProvince();
        checkProvinceFroShowCity();

    });
</script>


<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td class="txt_big_gray"><?= $page_title ?></td>
    </tr>
</table>

<div align="right"><a href="./?mode=comments_home/index" class="txt_bold_gray">&laquo; Back</a></div>

<form action="process.php?mode=comment_home/save&id=<?= $id ?>" onsubmit="MM_validateForm(
'cl_name','','R',
'cl_lname','','R',
'cl_email','','R',
'check_cl_email','','R',
'cl_email','','isEmail'
);return document.MM_returnValue" name="form" id="form1" method="post" enctype="multipart/form-data">
    <input type="hidden" name="id" id="id" value="<?= $id ?>"/>
    <table width="100%" border="0" cellspacing="0" cellpadding="3">

        <tr>
            <td align="right" class="txt_bold_gray" width="250"></td>
            <td width="1126"><b style="font-size:18px">ข้อมูล Comment</b></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="250">Name :</td>
            <td width="1126">

                <?php if ($row['ag_id'] != 0 && $row['cl_id'] == 0) {
                    $ag_name = get_value('agents', 'ag_id', 'ag_name', $row['ag_id']); ?>
                    <input type="text" name="name" value="<?php if (isset($ag_name)) {
                        echo "$ag_name";
                    } ?>" size="40" readonly="readonly" style="background-color:#EAEAEA;">

                <?php } elseif ($row['ag_id'] == 0 && $row['cl_id'] != 0) {
                    $cl_name = get_value('clients', 'cl_id', 'cl_name', $row['cl_id']);
                    $cl_lname = get_value('clients', 'cl_id', 'cl_lname', $row['cl_id']); ?>
                    <input type="text" name="name" value="<?php if (isset($cl_name)) {
                        echo "$cl_name $cl_lname";
                    } ?>" size="40" readonly="readonly" style="background-color:#EAEAEA;">

                <?php } else { ?>
                    <input type="text" name="name" value="<?php if ($row['name']) {
                        echo $row['name'];
                    } ?>" size="40" readonly="readonly" style="background-color:#EAEAEA;">

                <?php } ?>

            </td>
        </tr>
        <?php if ($row['product_type'] == 1) {
            echo "Product";
        } else {
            echo "Combo Product";
        } ?>" size="40" readonly="readonly" style="background-color:#EAEAEA;"></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="250">Product Category :</td>
            <td width="1126">
                <input type="text" name="product_category" value="<?php
                if ($row['product_type'] == 1) {
                    $category_name = get_value('lis_package_producttype', 'lis_id', 'lis_name', $row['product_category']);
                    echo $category_name;
                } else {
                    $category_name = get_value('lis_combo_category', 'lis_id', 'lis_name', $row['product_category']);
                    echo $category_name;
                }
                ?>" size="40" readonly="readonly" style="background-color:#EAEAEA;"></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="250">Product :</td>
            <td width="1126">
                <input type="text" name="product_id" value="<?php
                if ($row['product_type'] == 1) {
                    $item_table = get_value('lis_package_producttype', 'lis_id', 'item_table', $row['product_category']);
                    $item_id = get_value('lis_package_producttype', 'lis_id', 'item_id', $row['product_category']);
                    $item_name = get_value('lis_package_producttype', 'lis_id', 'item_name', $row['product_category']);

                    $sql_product = "select * from $item_table where $item_id = " . $row['product_id'] . " ";
                    $result_product = mysql_query($sql_product);
                    $row_product = mysql_fetch_array($result_product);

                    echo $row_product[$item_name];

                } else {
                    $sql_product = "select * from packages where pac_id = " . $row['product_id'] . " ";
                    $result_product = mysql_query($sql_product);
                    $row_product = mysql_fetch_array($result_product);

                    echo $row_product['pac_name'];
                }
                ?>" size="40" readonly="readonly" style="background-color:#EAEAEA;"></td>
        </tr>
        -->

        <tr>
            <td align="right" class="txt_bold_gray" width="250">Comment :</td>
            <td><textarea name="comment" cols="50" rows="5" readonly="readonly"
                          style="background-color:#EAEAEA;"><?= $row['comment'] ?></textarea></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="250">Time :</td>
            <td width="1126"><input type="text" name="comment_date" value="<?php if ($row['comment_date']) {
                    echo $row['comment_date'];
                } ?>" size="20" readonly="readonly" style="background-color:#EAEAEA;"></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="250">Status :</td>
            <td width="1126">
                <input type="radio" name="status" value="0" <?php if ($row['status'] == 0) {
                    echo 'checked';
                } ?> /> Not Show &nbsp;
                <input type="radio" name="status" value="1" <?php if ($row['status'] == 1) {
                    echo 'checked';
                } ?> /> Show
            </td>
        </tr>


        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td><input type="submit" name="submit" value="SUBMIT" style="width:75px;"><?php
                if (isset($id)) echo " <input type=\"submit\" name=\"del\" value=\"DELETE\" style=\"width:75px;\" onClick=\"return confirm('Do you want to delete this record ?')\">";
                else echo " <input type=\"reset\" name=\"Reset\" value=\"RESET\" style=\"width:75px;\">";
                ?></td>
        </tr>
    </table>
</form>


