<?php
// Convert Variable Array To Variable

while (list($xVarName, $xVarvalue) = each($_GET)) {
    ${$xVarName} = $xVarvalue;
}


while (list($xVarName, $xVarvalue) = each($_POST)) {
    ${$xVarName} = $xVarvalue;
}

while (list($xVarName, $xVarvalue) = each($_FILES)) {
    ${$xVarName . "_name"} = $xVarvalue['name'];
    ${$xVarName . "_type"} = $xVarvalue['type'];
    ${$xVarName . "_size"} = $xVarvalue['size'];
    ${$xVarName . "_error"} = $xVarvalue['error'];
    ${$xVarName} = $xVarvalue['tmp_name'];
}
//เพิ่มเติม 21/07/58======================================
if ($id) {
    $page_title = "Update Activity";
    $sql = "select * from activities where act_id = $id";
    $result = mysql_query($sql);
    $row = mysql_fetch_array($result);
    include("inc.top.php");
} else {
    $page_title = "Create New Activity";
}
?>

<script language="JavaScript" type="text/JavaScript">
    <!--
    function MM_findObj(n, d) { //v4.01
        var p, i, x;
        if (!d) d = document;
        if ((p = n.indexOf("?")) > 0 && parent.frames.length) {
            d = parent.frames[n.substring(p + 1)].document;
            n = n.substring(0, p);
        }
        if (!(x = d[n]) && d.all) x = d.all[n];
        for (i = 0; !x && i < d.forms.length; i++) x = d.forms[i][n];
        for (i = 0; !x && d.layers && i < d.layers.length; i++) x = MM_findObj(n, d.layers[i].document);
        if (!x && d.getElementById) x = d.getElementById(n);
        return x;
    }
    function MM_validateForm() { //v4.0
        var i, p, q, nm, test, num, min, max, errors = '', args = MM_validateForm.arguments;
        for (i = 0; i < (args.length - 2); i += 3) {
            test = args[i + 2];
            val = MM_findObj(args[i]);
            if (val) {
                nm = val.name;
                if ((val = val.value) != "") {
                    if (test.indexOf('isEmail') != -1) {
                        p = val.indexOf('@');
                        if (p < 1 || p == (val.length - 1)) errors += '- E-mail must contain an e-mail address.\n';
                    } else if (test != 'R') {
                        num = parseFloat(val);
                        if (isNaN(val)) {
                            if (nm == 'category_id') nm = 'Category';
                            errors += '- ' + nm + ' must contain a number.\n';
                        }
                        if (test.indexOf('inRange') != -1) {
                            p = test.indexOf(':');
                            min = test.substring(8, p);
                            max = test.substring(p + 1);
                            if (num < min || max < num) errors += '- ' + nm + ' must contain a number between ' + min + ' and ' + max + '.\n';
                        }
                    }
                } else if (test.charAt(0) == 'R') {
                    if (nm == 'category_id') nm = 'Category';
                    if (nm == 'act_name') nm = 'Name';
                    if (nm == 'act_ref') nm = 'Ref';
                    if (nm == 'spn_act_ref') nm = 'Ref';
                    if (nm == 'activitypricetype_id') nm = 'Price Type';
                    errors += '- ' + nm + ' is required.\n';
                }
            }
        }
        if (errors) alert('The following error(s) occurred:\n' + errors);
        document.MM_returnValue = (errors == '');
    }
    //-->
</script>

<script language="JavaScript" type="text/JavaScript">
    function checkCountryFroShowProvince() {


        //alert('country_id : ' + $("#country_id").val());
        $("#td_province_id").prepend("<img src='../admin_ajax/images/icon/ajax-loader.gif' /> ");

        $.post("../admin_ajax/ajx_activity_provincelist.php", {
                data1: $("#country_id").val(),
                data2: $("#province_id").val()
            },
            function (result) {
                $("#td_province_id").html(result);
                checkProvinceFroShowArea();
            }
        );


    }

    function checkProvinceFroShowArea() {
        //alert('province_id : ' + $("#province_id").val());
        $("#td_area_id").prepend("<img src='../admin_ajax/images/icon/ajax-loader.gif' /> ");

        $.post("../admin_ajax/ajx_activity_arealist.php", {
                data1: $("#province_id").val(),
                data2: $("#area_id").val()
            },
            function (result) {
                $("#td_area_id").html(result);
            }
        );
    }


</script>

<script language="JavaScript" type="text/JavaScript">
    $(document).ready(function () {
        checkCountryFroShowProvince();
        checkProvinceFroShowArea();

    });
</script>

<script type="text/javascript" src="./javascript/ckeditor/ckeditor.js"></script>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td class="txt_big_gray"><?= $page_title ?></td>
    </tr>
</table>
<form action="process.php?mode=activities/save&id=<?= $id ?>" onsubmit="MM_validateForm(
'category_id','','R',
'act_name','','R',
'activitypricetype_id','','R',
'check_act_ref','','R',
'act_ref','','R'
);return document.MM_returnValue" name="form" method="post" enctype="multipart/form-data">
    <table width="100%" border="0" cellspacing="0" cellpadding="3">
        <tr>
            <td align="right" class="txt_bold_gray">Category :</td>
            <td><select name="category_id" id="category_id" style="width:200px">
                    <option value="">-- Select --</option>
                    <?php listbox('activity_con_category', 'con_id', 'con_name', $row['category_id'], 'N'); ?>
                </select> <span class="remark"> * </span></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray">Country :</td>
            <td><select name="country_id" id="country_id" style="width:200px" onchange="checkCountryFroShowProvince();">
                    <option value="">-- Select --</option>
                    <?php listbox('activity_con_country', 'con_id', 'con_name', $row['country_id'], 'N'); ?>
                </select></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray">Province :</td>
            <td id="td_province_id"><select name="province_id" id="province_id" style="width:200px"
                                            onchange="checkProvinceFroShowArea();">
                    <option value="">-- Select --</option>
                    <?php listbox('activity_con_province', 'con_id', 'con_name', $row['province_id'], 'N'); ?>
                </select></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray">Area :</td>
            <td id="td_area_id"><select name="area_id" id="area_id" style="width:200px">
                    <option value="">-- Select --</option>
                    <?php listbox('activity_con_area', 'con_id', 'con_name', $row['area_id'], 'N'); ?>
                </select></td>
        </tr>


        <tr>
            <td align="right" class="txt_bold_gray" width="200">Name :</td>
            <td><input type="text" name="act_name" id="act_name" value="<?= $row['act_name'] ?>" size="50"><span
                    class="remark"> * </span></td>
        </tr>


        <script type="text/javascript">
            function checkRefRepeatedly() {
                //alert('checkEmailRepeatedly : ' + $("#act_email").val());
                $("#spn_act_ref").html("<img src='../admin_ajax/images/icon/ajax-loader.gif' /> ");

                $.post("../admin_ajax/ajx_ref_repeatedly.php", {
                        data1: $("#act_ref").val(),
                        data2: 'act_',
                        data3: 'activities',
                        data4: '<?=$row[0]?>'
                    },
                    function (result) {
                        $("#spn_act_ref").html(result);
                    }
                );
            }
        </script>

        <tr>
            <td align="right" class="txt_bold_gray" width="200">Ref :</td>
            <td><input type="text" name="act_ref" id="act_ref" value="<?= $row['act_ref'] ?>" size="50"
                       onkeyup="checkRefRepeatedly();">
                <span id="spn_act_ref" class="remark"> * <input type="hidden" name="check_act_ref"
                                                                value="<?= $row['act_ref'] ?>"></span>
            </td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="200">Account Code AMW :</td>
            <td>Adult <input type="text" name="ac_andaman_adult" id="ac_andaman_adult"
                             value="<?= $row['ac_andaman_adult'] ?>" size="20">&nbsp;&nbsp;&nbsp;
                Child <input type="text" name="ac_andaman_child" id="ac_andaman_child"
                             value="<?= $row['ac_andaman_child'] ?>" size="20"></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="200">Account Code ppholiday.com :</td>
            <td>Adult <input type="text" name="ac_dotcom_adult" id="ac_dotcom_adult"
                             value="<?= $row['ac_dotcom_adult'] ?>" size="20">&nbsp;&nbsp;&nbsp;
                Child <input type="text" name="ac_dotcom_child" id="ac_dotcom_child"
                             value="<?= $row['ac_dotcom_child'] ?>" size="20"></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="200">Account Code B to C :</td>
            <td>Adult <input type="text" name="ac_btoc_adult" id="ac_btoc_adult" value="<?= $row['ac_btoc_adult'] ?>"
                             size="20">&nbsp;&nbsp;&nbsp;
                Child <input type="text" name="ac_btoc_child" id="ac_btoc_child" value="<?= $row['ac_btoc_child'] ?>"
                             size="20"></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="200">&lt;Meta Title&gt; :</td>
            <td><textarea name="meta_title" rows="3" cols="68"><?= $row['meta_title'] ?></textarea></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="200">&lt;Meta Keywords&gt; :</td>
            <td><textarea name="meta_keywords" rows="3" cols="68"><?= $row['meta_keywords'] ?></textarea></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="200">&lt;Meta Description&gt; :</td>
            <td><textarea name="meta_description" rows="3" cols="68"><?= $row['meta_description'] ?></textarea></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="200">Short Description :</td>
            <td>
                <textarea name="act_shortdetail" id="act_shortdetail" rows="7"
                          style="width:500px;"><?= $row['act_shortdetail'] ?></textarea></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="200">Description :</td>
            <td>
                <textarea name="act_detail" id="act_detail" rows="7"
                          style="width:500px;"><?= $row['act_detail'] ?></textarea></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray">Program Detail :</td>
            <td>
                <table border="0" cellspacing="1" cellpadding="3" bgcolor="<?= $color_1 ?>">
                    <tr bgcolor="<?= $color_2 ?>">
                        <td class="txt_bold_gray">Time</td>
                        <td class="txt_bold_gray">Description</td>
                    </tr>
                    <?php
                    $act_pro_time_arr = explode("~", $row['act_pro_time_arr']);
                    $act_pro_des_arr = explode("~", $row['act_pro_des_arr']);
                    $i = 0;
                    while ($i < 10) {
                        ?>
                        <tr bgcolor="<?= $color_3 ?>">
                            <td><textarea name="act_pro_time_arr[<?= $i ?>]" rows="2"
                                          cols="25"><?= $act_pro_time_arr[($i + 1)] ?></textarea></td>
                            <td><textarea name="act_pro_des_arr[<?= $i ?>]" rows="2"
                                          cols="75"><?= $act_pro_des_arr[($i + 1)] ?></textarea></td>
                        </tr>
                        <?php
                        $i++;
                    }
                    ?>
                </table>
            </td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="200">Remark :</td>
            <!--<td>
        <textarea name="act_remark" id="act_remark" rows="7" style="width:500px;"><?= $row['act_remark'] ?></textarea></td>-->
            <td>
                <div
                    style="font-size:0.9em; color:#C00; padding:5px; width:490px; background-color:#FFE0DD; position:relative;">
                    - Press "Enter" = New paragraph<br/>
                    - Press "Shift+Enter" = New line
                </div>
                <br/>
                <textarea name="act_remark" id="act_remark" rows="7"
                          style="width:500px;"><?= $row['act_remark'] ?></textarea>
                <script type="text/javascript">//<![CDATA[
                    CKEDITOR.replace('act_remark');
                    //]]></script>
            </td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="200">Name Link Recommended :</td>
            <td><input type="text" name="act_name_link" id="act_name_link" value="<?= $row['act_name_link'] ?>"
                       size="50"></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="200">URL Link :</td>
            <td><input type="text" name="act_url" id="act_url" value="<?= $row['act_url'] ?>" size="50"></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="200">Condition :</td>
            <td>
                <textarea name="act_condition" id="act_condition" rows="7"
                          style="width:500px;"><?= $row['act_condition'] ?></textarea></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray">Main photo :</td>
            <td><?php $img_tmp = $row['photo1'];
                if ($img_tmp) { ?>
                    <img src="./resizer.php?imgfile=../photo/activities/<?= $img_tmp ?>&size=200" border="0"><br>
                    <label><input type="checkbox" name="del_photo1" value="<?= $img_tmp ?>"><span class="remark">Delete Photo</span></label>
                    <br>
                <?php } ?>
                <input name="photo1" type="file"><input name="tmp_photo1" type="hidden" value="<?= $img_tmp ?>"></td>
        </tr>
        <tr>
            <td align="right" class="txt_bold_gray">Photo :</td>
            <td>
                <table border="0" cellspacing="1" cellpadding="3" bgcolor="<?= $color_1 ?>">
                    <tr bgcolor="#FFFFFF">
                        <td valign="bottom" class="txt_bold_gray">#1<br/>
                            <?php $img_tmp = $row['photo2'];
                            if ($img_tmp) { ?>
                                <img src="./resizer.php?imgfile=../photo/activities/<?= $img_tmp ?>&size=200"
                                     border="0"><br>
                                <label><input type="checkbox" name="del_photo2" value="<?= $img_tmp ?>"><span
                                        class="remark">Delete Photo</span></label><br>
                            <?php } ?>
                            <input name="photo2" type="file"><input name="tmp_photo2" type="hidden"
                                                                    value="<?= $img_tmp ?>"></td>
                        <td valign="bottom" class="txt_bold_gray">#2<br/>
                            <?php $img_tmp = $row['photo3'];
                            if ($img_tmp) { ?>
                                <img src="./resizer.php?imgfile=../photo/activities/<?= $img_tmp ?>&size=200"
                                     border="0"><br>
                                <label><input type="checkbox" name="del_photo3" value="<?= $img_tmp ?>"><span
                                        class="remark">Delete Photo</span></label><br>
                            <?php } ?>
                            <input name="photo3" type="file"><input name="tmp_photo3" type="hidden"
                                                                    value="<?= $img_tmp ?>"></td>
                        <td valign="bottom" class="txt_bold_gray">#3<br/>
                            <?php $img_tmp = $row['photo4'];
                            if ($img_tmp) { ?>
                                <img src="./resizer.php?imgfile=../photo/activities/<?= $img_tmp ?>&size=200"
                                     border="0"><br>
                                <label><input type="checkbox" name="del_photo4" value="<?= $img_tmp ?>"><span
                                        class="remark">Delete Photo</span></label><br>
                            <?php } ?>
                            <input name="photo4" type="file"><input name="tmp_photo4" type="hidden"
                                                                    value="<?= $img_tmp ?>"></td>
                    </tr>
                    <tr bgcolor="#FFFFFF">
                        <td valign="bottom" class="txt_bold_gray">#4<br/>
                            <?php $img_tmp = $row['photo5'];
                            if ($img_tmp) { ?>
                                <img src="./resizer.php?imgfile=../photo/activities/<?= $img_tmp ?>&size=200"
                                     border="0"><br>
                                <label><input type="checkbox" name="del_photo5" value="<?= $img_tmp ?>"><span
                                        class="remark">Delete Photo</span></label><br>
                            <?php } ?>
                            <input name="photo5" type="file"><input name="tmp_photo5" type="hidden"
                                                                    value="<?= $img_tmp ?>"></td>
                        <td valign="bottom" class="txt_bold_gray">#5<br/>
                            <?php $img_tmp = $row['photo6'];
                            if ($img_tmp) { ?>
                                <img src="./resizer.php?imgfile=../photo/activities/<?= $img_tmp ?>&size=200"
                                     border="0"><br>
                                <label><input type="checkbox" name="del_photo6" value="<?= $img_tmp ?>"><span
                                        class="remark">Delete Photo</span></label><br>
                            <?php } ?>
                            <input name="photo6" type="file"><input name="tmp_photo6" type="hidden"
                                                                    value="<?= $img_tmp ?>"></td>
                        <td valign="bottom" class="txt_bold_gray">#6<br/>
                            <?php $img_tmp = $row['photo7'];
                            if ($img_tmp) { ?>
                                <img src="./resizer.php?imgfile=../photo/activities/<?= $img_tmp ?>&size=200"
                                     border="0"><br>
                                <label><input type="checkbox" name="del_photo7" value="<?= $img_tmp ?>"><span
                                        class="remark">Delete Photo</span></label><br>
                            <?php } ?>
                            <input name="photo7" type="file"><input name="tmp_photo7" type="hidden"
                                                                    value="<?= $img_tmp ?>"></td>
                    </tr>
                    <tr bgcolor="#FFFFFF">
                        <td valign="bottom" class="txt_bold_gray">#7<br/>
                            <?php $img_tmp = $row['photo8'];
                            if ($img_tmp) { ?>
                                <img src="./resizer.php?imgfile=../photo/activities/<?= $img_tmp ?>&size=200"
                                     border="0"><br>
                                <label><input type="checkbox" name="del_photo8" value="<?= $img_tmp ?>"><span
                                        class="remark">Delete Photo</span></label><br>
                            <?php } ?>
                            <input name="photo8" type="file"><input name="tmp_photo8" type="hidden"
                                                                    value="<?= $img_tmp ?>"></td>
                        <td valign="bottom" class="txt_bold_gray">#8<br/>
                            <?php $img_tmp = $row['photo9'];
                            if ($img_tmp) { ?>
                                <img src="./resizer.php?imgfile=../photo/activities/<?= $img_tmp ?>&size=200"
                                     border="0"><br>
                                <label><input type="checkbox" name="del_photo9" value="<?= $img_tmp ?>"><span
                                        class="remark">Delete Photo</span></label><br>
                            <?php } ?>
                            <input name="photo9" type="file"><input name="tmp_photo9" type="hidden"
                                                                    value="<?= $img_tmp ?>"></td>
                        <td valign="bottom" class="txt_bold_gray">#9<br/>
                            <?php $img_tmp = $row['photo11'];
                            if ($img_tmp) { ?>
                                <img src="./resizer.php?imgfile=../photo/activities/<?= $img_tmp ?>&size=200"
                                     border="0"><br>
                                <label><input type="checkbox" name="del_photo11" value="<?= $img_tmp ?>"><span
                                        class="remark">Delete Photo</span></label><br>
                            <?php } ?>
                            <input name="photo11" type="file"><input name="tmp_photo11" type="hidden"
                                                                     value="<?= $img_tmp ?>"></td>
                    </tr>
                    <tr bgcolor="#FFFFFF">
                        <td valign="bottom" class="txt_bold_gray">#10<br/>
                            <?php $img_tmp = $row['photo12'];
                            if ($img_tmp) { ?>
                                <img src="./resizer.php?imgfile=../photo/activities/<?= $img_tmp ?>&size=200"
                                     border="0"><br>
                                <label><input type="checkbox" name="del_photo12" value="<?= $img_tmp ?>"><span
                                        class="remark">Delete Photo</span></label><br>
                            <?php } ?>
                            <input name="photo12" type="file"><input name="tmp_photo12" type="hidden"
                                                                     value="<?= $img_tmp ?>"></td>
                        <td valign="bottom" class="txt_bold_gray">#11<br/>
                            <?php $img_tmp = $row['photo13'];
                            if ($img_tmp) { ?>
                                <img src="./resizer.php?imgfile=../photo/activities/<?= $img_tmp ?>&size=200"
                                     border="0"><br>
                                <label><input type="checkbox" name="del_photo13" value="<?= $img_tmp ?>"><span
                                        class="remark">Delete Photo</span></label><br>
                            <?php } ?>
                            <input name="photo13" type="file"><input name="tmp_photo13" type="hidden"
                                                                     value="<?= $img_tmp ?>"></td>
                        <td valign="bottom" class="txt_bold_gray">#12<br/>
                            <?php $img_tmp = $row['photo14'];
                            if ($img_tmp) { ?>
                                <img src="./resizer.php?imgfile=../photo/activities/<?= $img_tmp ?>&size=200"
                                     border="0"><br>
                                <label><input type="checkbox" name="del_photo14" value="<?= $img_tmp ?>"><span
                                        class="remark">Delete Photo</span></label><br>
                            <?php } ?>
                            <input name="photo14" type="file"><input name="tmp_photo14" type="hidden"
                                                                     value="<?= $img_tmp ?>"></td>
                    </tr>
                    <tr bgcolor="#FFFFFF">
                        <td valign="bottom" class="txt_bold_gray">#13<br/>
                            <?php $img_tmp = $row['photo15'];
                            if ($img_tmp) { ?>
                                <img src="./resizer.php?imgfile=../photo/activities/<?= $img_tmp ?>&size=200"
                                     border="0"><br>
                                <label><input type="checkbox" name="del_photo15" value="<?= $img_tmp ?>"><span
                                        class="remark">Delete Photo</span></label><br>
                            <?php } ?>
                            <input name="photo15" type="file"><input name="tmp_photo15" type="hidden"
                                                                     value="<?= $img_tmp ?>"></td>
                        <td valign="bottom" class="txt_bold_gray">#14<br/>
                            <?php $img_tmp = $row['photo16'];
                            if ($img_tmp) { ?>
                                <img src="./resizer.php?imgfile=../photo/activities/<?= $img_tmp ?>&size=200"
                                     border="0"><br>
                                <label><input type="checkbox" name="del_photo16" value="<?= $img_tmp ?>"><span
                                        class="remark">Delete Photo</span></label><br>
                            <?php } ?>
                            <input name="photo16" type="file"><input name="tmp_photo16" type="hidden"
                                                                     value="<?= $img_tmp ?>"></td>
                        <td valign="bottom" class="txt_bold_gray">#15<br/>
                            <?php $img_tmp = $row['photo17'];
                            if ($img_tmp) { ?>
                                <img src="./resizer.php?imgfile=../photo/activities/<?= $img_tmp ?>&size=200"
                                     border="0"><br>
                                <label><input type="checkbox" name="del_photo17" value="<?= $img_tmp ?>"><span
                                        class="remark">Delete Photo</span></label><br>
                            <?php } ?>
                            <input name="photo17" type="file"><input name="tmp_photo17" type="hidden"
                                                                     value="<?= $img_tmp ?>"></td>
                    </tr>
                </table>
            </td>
        </tr>

        <tr>
            <td>&nbsp;</td>
            <td><span class="remark">
        - Support JPG Format Only<br>
        - Recommend For Width 640px Height 480px
        </span></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray">ประเภทของการกำหนดราคา :</td>
            <td><select name="activitypricetype_id" id="activitypricetype_id" style="width:200px">
                    <?php listbox('activity_price_type', 'a_id', 'a_name', $row['activitypricetype_id'], 'N'); ?>
                </select> <span class="remark"> * </span></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray">Agent Supplier :</td>
            <td><select name="agents_sup_id" id="agents_sup_id" style="width:200px">
                    <option value="">-- Select --</option>
                    <?php listbox('suppliers', 'sp_id', 'sp_cname', $row['agents_sup_id'], 'N'); ?>
                </select></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray">Hot Deal :</td>
            <td><input type="checkbox" name="act_hot" value="1" <?php if ($row['act_hot'] == 1) {
                    echo 'checked';
                } ?> /></td>
        </tr>

        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td><input type="submit" name="submit" value="SUBMIT" style="width:75px;"><?php
                if ($id) echo " <input type=\"submit\" name=\"del\" value=\"DELETE\" style=\"width:75px;\" onClick=\"return confirm('Do you want to delete this record ?')\">";
                else echo " <input type=\"reset\" name=\"Reset\" value=\"RESET\" style=\"width:75px;\">";
                ?></td>
        </tr>
    </table>
</form>