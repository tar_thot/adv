<table width="100%" border="0" cellspacing="0" cellpadding="3">
    <tr>
        <td>
            <table border="0" cellspacing="1" cellpadding="3" bgcolor="<?= $color_1 ?>">
                <tr bgcolor="<?= $color_3 ?>">
                    <td valign="top" width="150" rowspan="6"><?php $img_tmp = $row['photo1'];
                        if ($img_tmp) { ?>
                            <img src="./resizer.php?imgfile=../photo/activities/<?= $img_tmp ?>&size=150" border="0">
                            <br>
                        <?php } ?></td>
                    <td class="txt_bold_gray" colspan="2"><?= $row['act_name'] ?>                </td>
                </tr>
                <tr bgcolor="<?= $color_3 ?>">
                    <td bgcolor="<?= $color_2 ?>" align="right" class="txt_bold_gray">Country :</td>
                    <td><?= get_value('activity_con_country', 'con_id', 'con_name', $row['country_id']) ?></td>
                </tr>
                <tr bgcolor="<?= $color_3 ?>">
                    <td bgcolor="<?= $color_2 ?>" align="right" class="txt_bold_gray">Province/State :</td>
                    <td><?= get_value('activity_con_province', 'con_id', 'con_name', $row['province_id']) ?></td>
                </tr>
                <tr bgcolor="<?= $color_3 ?>">
                    <td bgcolor="<?= $color_2 ?>" align="right" class="txt_bold_gray">Area :</td>
                    <td><?= get_value('activity_con_area', 'con_id', 'con_name', $row['area_id']) ?></td>
                </tr>
                <tr bgcolor="<?= $color_3 ?>">
                    <td bgcolor="<?= $color_2 ?>" align="right" class="txt_bold_gray">Category :</td>
                    <td><?= get_value('activity_con_category', 'con_id', 'con_name', $row['category_id']) ?></td>
                </tr>
                <tr bgcolor="<?= $color_3 ?>">
                    <td bgcolor="<?= $color_2 ?>" align="right" class="txt_bold_gray">ประเภทการกำหนดราคา :</td>
                    <td><?= get_value('activity_price_type', 'a_id', 'a_name', $row['activitypricetype_id']) ?></td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<table width="100%" border="0" cellspacing="1" cellpadding="3" bgcolor="<?= $color_inctop1 ?>">
    <tr bgcolor="<?= $color_inctop2 ?>">
        <td><span class="txt_bold_white">
        <a href="./?mode=activities/add&id=<?= $id ?>" class="txt_bold_white">Edit</a><strong
                    style="color:<?= $color_inctop1 ?>;"> | </strong>
        <a href="./?mode=activities/ratetypes_index&id=<?= $id ?>" class="txt_bold_white">Rate Type</a><strong
                    style="color:<?= $color_inctop1 ?>;"> | </strong>
        <a href="./?mode=activities/period_index&id=<?= $id ?>" class="txt_bold_white">Period</a><strong
                    style="color:<?= $color_inctop1 ?>;"> | </strong>
        <a href="./?mode=activities/rate_index&id=<?= $id ?>" class="txt_bold_white">Rate Setting</a><strong
                    style="color:<?= $color_inctop1 ?>;"> | </strong>
        <a href="./?mode=activities/allocation_index&id=<?= $id ?>" class="txt_bold_white">Allocation Setting</a><strong
                    style="color:<?= $color_inctop1 ?>;"> | </strong>
        </span></td>
    </tr>
</table>
<br/>
