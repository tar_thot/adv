<?php
// Convert Variable Array To Variable

while (list($xVarName, $xVarvalue) = each($_GET)) {
    ${$xVarName} = $xVarvalue;
}


while (list($xVarName, $xVarvalue) = each($_POST)) {
    ${$xVarName} = $xVarvalue;
}

while (list($xVarName, $xVarvalue) = each($_FILES)) {
    ${$xVarName . "_name"} = $xVarvalue['name'];
    ${$xVarName . "_type"} = $xVarvalue['type'];
    ${$xVarName . "_size"} = $xVarvalue['size'];
    ${$xVarName . "_error"} = $xVarvalue['error'];
    ${$xVarName} = $xVarvalue['tmp_name'];
}
//������� 21/07/58======================================
if ($id) {
    $page_title = "Update Hotel Category";
    $sql = "select * from hotelcat where hotelcat_id = $id";
    $result = mysql_query($sql);
    $row = mysql_fetch_array($result);
} else {
    $page_title = "Create New Hotel Category";
}
?>
<script language="JavaScript" type="text/JavaScript">
    <!--
    function MM_findObj(n, d) { //v4.01
        var p, i, x;
        if (!d) d = document;
        if ((p = n.indexOf("?")) > 0 && parent.frames.length) {
            d = parent.frames[n.substring(p + 1)].document;
            n = n.substring(0, p);
        }
        if (!(x = d[n]) && d.all) x = d.all[n];
        for (i = 0; !x && i < d.forms.length; i++) x = d.forms[i][n];
        for (i = 0; !x && d.layers && i < d.layers.length; i++) x = MM_findObj(n, d.layers[i].document);
        if (!x && d.getElementById) x = d.getElementById(n);
        return x;
    }
    function MM_validateForm() { //v4.0
        var i, p, q, nm, test, num, min, max, errors = '', args = MM_validateForm.arguments;
        for (i = 0; i < (args.length - 2); i += 3) {
            test = args[i + 2];
            val = MM_findObj(args[i]);
            if (val) {
                nm = val.name;
                if ((val = val.value) != "") {
                    if (test.indexOf('isEmail') != -1) {
                        p = val.indexOf('@');
                        if (p < 1 || p == (val.length - 1)) errors += '- E-mail must contain an e-mail address.\n';
                    } else if (test != 'R') {
                        num = parseFloat(val);
                        if (isNaN(val)) {
                            if (nm == 'name') nm = 'Name';
                            errors += '- ' + nm + ' must contain a number.\n';
                        }
                        if (test.indexOf('inRange') != -1) {
                            p = test.indexOf(':');
                            min = test.substring(8, p);
                            max = test.substring(p + 1);
                            if (num < min || max < num) errors += '- ' + nm + ' must contain a number between ' + min + ' and ' + max + '.\n';
                        }
                    }
                } else if (test.charAt(0) == 'R') {
                    if (nm == 'name') nm = 'Name';
                    errors += '- ' + nm + ' is required.\n';
                }
            }
        }
        if (errors) alert('The following error(s) occurred:\n' + errors);
        document.MM_returnValue = (errors == '');
    }
    //-->
</script>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td class="txt_big_gray"><?= $page_title ?></td>
    </tr>
</table>
<form action="process.php?mode=hotel/cat_save&id=<?= $id ?>" onsubmit="MM_validateForm(
'name','','R'
);return document.MM_returnValue" name="form" method="post" enctype="multipart/form-data">
    <input type="hidden" name="permission" value="Admin">
    <table width="100%" border="0" cellspacing="0" cellpadding="3">
        <tr>
            <td align="right" class="txt_bold_gray" width="200">Name :</td>
            <td><input type="text" name="name" value="<?= $row['name'] ?>" size="50"><span class="remark"> * </span>
            </td>
        </tr>
        <tr>
            <td align="right" class="txt_bold_gray">Type :</td>
            <td>
                <label><input type="radio" name="type"
                              value="Interest" <?php if ($row['type'] == "Interest" || !$row['type']) echo "checked"; ?> />&nbsp;Interest</label>&nbsp;&nbsp;&nbsp;&nbsp;
                <label><input type="radio" name="type"
                              value="Destination" <?php if ($row['type'] == "Destination") echo "checked"; ?> />&nbsp;Destination</label><br/>
            </td>
        </tr>
        <tr>
            <td align="right" class="txt_bold_gray">Description :</td>
            <td><textarea name="description" rows="7" cols="75"><?= $row['description'] ?></textarea></td>
        </tr>
        <tr>
            <td align="right" class="txt_bold_gray">How to get there :</td>
            <td><textarea name="how_to_get_there" rows="7" cols="75"><?= $row['how_to_get_there'] ?></textarea></td>
        </tr>
        <tr>
            <td align="right" class="txt_bold_gray">How to getting around :</td>
            <td><textarea name="how_to_getting_around" rows="7"
                          cols="75"><?= $row['how_to_getting_around'] ?></textarea></td>
        </tr>
        <tr>
            <td align="right" class="txt_bold_gray">What to do & see :</td>
            <td><textarea name="what_to_do_see" rows="7" cols="75"><?= $row['what_to_do_see'] ?></textarea></td>
        </tr>
        <tr>
            <td align="right" class="txt_bold_gray">Which beach to go :</td>
            <td><textarea name="which_beach_to_go" rows="7" cols="75"><?= $row['which_beach_to_go'] ?></textarea></td>
        </tr>
        <tr>
            <td align="right" class="txt_bold_gray">Where to eat :</td>
            <td><textarea name="where_to_eat" rows="7" cols="75"><?= $row['where_to_eat'] ?></textarea></td>
        </tr>
        <tr>
            <td align="right" class="txt_bold_gray">Where to shopping :</td>
            <td><textarea name="where_to_shopping" rows="7" cols="75"><?= $row['where_to_shopping'] ?></textarea></td>
        </tr>
        <tr>
            <td align="right" class="txt_bold_gray">Where to party :</td>
            <td><textarea name="where_to_party" rows="7" cols="75"><?= $row['where_to_party'] ?></textarea></td>
        </tr>
        <tr>
            <td align="right" class="txt_bold_gray">Photo :</td>
            <td>
                <table border="0" cellspacing="1" cellpadding="3" bgcolor="<?= $color_1 ?>">
                    <tr bgcolor="#FFFFFF">
                        <td valign="bottom" class="txt_bold_gray">#1<br/>
                            <?php $img_tmp = $row['photo1'];
                            if ($img_tmp) { ?>
                                <img src="./resizer.php?imgfile=../photo/hotelcat/<?= $img_tmp ?>&size=200" border="0">
                                <br>
                                <label><input type="checkbox" name="del_photo1" value="<?= $img_tmp ?>"><span
                                        class="remark">Delete Photo</span></label><br>
                            <?php } ?>
                            <input name="photo1" type="file"><input name="tmp_photo1" type="hidden"
                                                                    value="<?= $img_tmp ?>"></td>
                        <td valign="bottom" class="txt_bold_gray">#2<br/>
                            <?php $img_tmp = $row['photo2'];
                            if ($img_tmp) { ?>
                                <img src="./resizer.php?imgfile=../photo/hotelcat/<?= $img_tmp ?>&size=200" border="0">
                                <br>
                                <label><input type="checkbox" name="del_photo2" value="<?= $img_tmp ?>"><span
                                        class="remark">Delete Photo</span></label><br>
                            <?php } ?>
                            <input name="photo2" type="file"><input name="tmp_photo2" type="hidden"
                                                                    value="<?= $img_tmp ?>"></td>
                        <td valign="bottom" class="txt_bold_gray">#3<br/>
                            <?php $img_tmp = $row['photo3'];
                            if ($img_tmp) { ?>
                                <img src="./resizer.php?imgfile=../photo/hotelcat/<?= $img_tmp ?>&size=200" border="0">
                                <br>
                                <label><input type="checkbox" name="del_photo3" value="<?= $img_tmp ?>"><span
                                        class="remark">Delete Photo</span></label><br>
                            <?php } ?>
                            <input name="photo3" type="file"><input name="tmp_photo3" type="hidden"
                                                                    value="<?= $img_tmp ?>"></td>
                    </tr>
                    <tr bgcolor="#FFFFFF">
                        <td valign="bottom" class="txt_bold_gray">#4<br/>
                            <?php $img_tmp = $row['photo4'];
                            if ($img_tmp) { ?>
                                <img src="./resizer.php?imgfile=../photo/hotelcat/<?= $img_tmp ?>&size=200" border="0">
                                <br>
                                <label><input type="checkbox" name="del_photo4" value="<?= $img_tmp ?>"><span
                                        class="remark">Delete Photo</span></label><br>
                            <?php } ?>
                            <input name="photo4" type="file"><input name="tmp_photo4" type="hidden"
                                                                    value="<?= $img_tmp ?>"></td>
                        <td valign="bottom" class="txt_bold_gray">#5<br/>
                            <?php $img_tmp = $row['photo5'];
                            if ($img_tmp) { ?>
                                <img src="./resizer.php?imgfile=../photo/hotelcat/<?= $img_tmp ?>&size=200" border="0">
                                <br>
                                <label><input type="checkbox" name="del_photo5" value="<?= $img_tmp ?>"><span
                                        class="remark">Delete Photo</span></label><br>
                            <?php } ?>
                            <input name="photo5" type="file"><input name="tmp_photo5" type="hidden"
                                                                    value="<?= $img_tmp ?>"></td>
                        <td valign="bottom" class="txt_bold_gray">#6<br/>
                            <?php $img_tmp = $row['photo6'];
                            if ($img_tmp) { ?>
                                <img src="./resizer.php?imgfile=../photo/hotelcat/<?= $img_tmp ?>&size=200" border="0">
                                <br>
                                <label><input type="checkbox" name="del_photo6" value="<?= $img_tmp ?>"><span
                                        class="remark">Delete Photo</span></label><br>
                            <?php } ?>
                            <input name="photo6" type="file"><input name="tmp_photo6" type="hidden"
                                                                    value="<?= $img_tmp ?>"></td>
                    </tr>
                    <tr bgcolor="#FFFFFF">
                        <td valign="bottom" class="txt_bold_gray">#7<br/>
                            <?php $img_tmp = $row['photo7'];
                            if ($img_tmp) { ?>
                                <img src="./resizer.php?imgfile=../photo/hotelcat/<?= $img_tmp ?>&size=200" border="0">
                                <br>
                                <label><input type="checkbox" name="del_photo7" value="<?= $img_tmp ?>"><span
                                        class="remark">Delete Photo</span></label><br>
                            <?php } ?>
                            <input name="photo7" type="file"><input name="tmp_photo7" type="hidden"
                                                                    value="<?= $img_tmp ?>"></td>
                        <td valign="bottom" class="txt_bold_gray">#8<br/>
                            <?php $img_tmp = $row['photo8'];
                            if ($img_tmp) { ?>
                                <img src="./resizer.php?imgfile=../photo/hotelcat/<?= $img_tmp ?>&size=200" border="0">
                                <br>
                                <label><input type="checkbox" name="del_photo8" value="<?= $img_tmp ?>"><span
                                        class="remark">Delete Photo</span></label><br>
                            <?php } ?>
                            <input name="photo8" type="file"><input name="tmp_photo8" type="hidden"
                                                                    value="<?= $img_tmp ?>"></td>
                        <td valign="bottom" class="txt_bold_gray">#9<br/>
                            <?php $img_tmp = $row['photo9'];
                            if ($img_tmp) { ?>
                                <img src="./resizer.php?imgfile=../photo/hotelcat/<?= $img_tmp ?>&size=200" border="0">
                                <br>
                                <label><input type="checkbox" name="del_photo9" value="<?= $img_tmp ?>"><span
                                        class="remark">Delete Photo</span></label><br>
                            <?php } ?>
                            <input name="photo9" type="file"><input name="tmp_photo9" type="hidden"
                                                                    value="<?= $img_tmp ?>"></td>
                    </tr>
                    <tr bgcolor="#FFFFFF">
                        <td valign="bottom" class="txt_bold_gray">#10<br/>
                            <?php $img_tmp = $row['photo10'];
                            if ($img_tmp) { ?>
                                <img src="./resizer.php?imgfile=../photo/hotelcat/<?= $img_tmp ?>&size=200" border="0">
                                <br>
                                <label><input type="checkbox" name="del_photo10" value="<?= $img_tmp ?>"><span
                                        class="remark">Delete Photo</span></label><br>
                            <?php } ?>
                            <input name="photo10" type="file"><input name="tmp_photo10" type="hidden"
                                                                     value="<?= $img_tmp ?>"></td>
                        <td valign="bottom" class="txt_bold_gray">#11<br/>
                            <?php $img_tmp = $row['photo11'];
                            if ($img_tmp) { ?>
                                <img src="./resizer.php?imgfile=../photo/hotelcat/<?= $img_tmp ?>&size=200" border="0">
                                <br>
                                <label><input type="checkbox" name="del_photo11" value="<?= $img_tmp ?>"><span
                                        class="remark">Delete Photo</span></label><br>
                            <?php } ?>
                            <input name="photo11" type="file"><input name="tmp_photo11" type="hidden"
                                                                     value="<?= $img_tmp ?>"></td>
                        <td valign="bottom" class="txt_bold_gray">#12<br/>
                            <?php $img_tmp = $row['photo12'];
                            if ($img_tmp) { ?>
                                <img src="./resizer.php?imgfile=../photo/hotelcat/<?= $img_tmp ?>&size=200" border="0">
                                <br>
                                <label><input type="checkbox" name="del_photo12" value="<?= $img_tmp ?>"><span
                                        class="remark">Delete Photo</span></label><br>
                            <?php } ?>
                            <input name="photo12" type="file"><input name="tmp_photo12" type="hidden"
                                                                     value="<?= $img_tmp ?>"></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td><span class="remark">
        - Support JPG Format Only<br>
        - Recommend For Width 640px Height 480px
        </span></td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td><input type="submit" name="submit" value="SUBMIT" style="width:75px;"><?php
                if ($id) echo " <input type=\"submit\" name=\"del\" value=\"DELETE\" style=\"width:75px;\" onClick=\"return confirm('Do you want to delete this record ?')\">";
                else echo " <input type=\"reset\" name=\"Reset\" value=\"RESET\" style=\"width:75px;\">";
                ?></td>
        </tr>
    </table>
</form>