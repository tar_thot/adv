<?php
include("value_get.php");
if ($id) {
    $page_title = "View Reservation Combo Product  ";
    $sql = "select * from reservation_packages where rpa_id = $id";
    $result = mysql_query($sql);
    $row = mysql_fetch_array($result);
    include("inc.top.php");
}

$total_prices = 0;
?>

<script language="JavaScript" type="text/JavaScript">
    <!--
    function MM_findObj(n, d) { //v4.01
        var p, i, x;
        if (!d) d = document;
        if ((p = n.indexOf("?")) > 0 && parent.frames.length) {
            d = parent.frames[n.substring(p + 1)].document;
            n = n.substring(0, p);
        }
        if (!(x = d[n]) && d.all) x = d.all[n];
        for (i = 0; !x && i < d.forms.length; i++) x = d.forms[i][n];
        for (i = 0; !x && d.layers && i < d.layers.length; i++) x = MM_findObj(n, d.layers[i].document);
        if (!x && d.getElementById) x = d.getElementById(n);
        return x;
    }
    function MM_validateForm() { //v4.0
        var i, p, q, nm, test, num, min, max, errors = '', args = MM_validateForm.arguments;
        for (i = 0; i < (args.length - 2); i += 3) {
            test = args[i + 2];
            val = MM_findObj(args[i]);
            if (val) {
                nm = val.name;
                if ((val = val.value) != "") {
                    if (test.indexOf('isEmail') != -1) {
                        p = val.indexOf('@');
                        if (p < 1 || p == (val.length - 1)) errors += '- E-mail must contain an e-mail address.\n';
                    } else if (test != 'R') {
                        num = parseFloat(val);
                        if (isNaN(val)) {
                            if (nm == 'category_id') nm = 'Category';
                            errors += '- ' + nm + ' must contain a number.\n';
                        }
                        if (test.indexOf('inRange') != -1) {
                            p = test.indexOf(':');
                            min = test.substring(8, p);
                            max = test.substring(p + 1);
                            if (num < min || max < num) errors += '- ' + nm + ' must contain a number between ' + min + ' and ' + max + '.\n';
                        }
                    }
                } else if (test.charAt(0) == 'R') {
                    if (nm == 'category_id') nm = 'Category';
                    if (nm == 'rpa_name') nm = 'Name';
                    if (nm == 'rpa_ref') nm = 'Ref';
                    if (nm == 'check_rpa_ref') nm = 'Ref';
                    if (nm == 'activitypricetype_id') nm = 'Price Type';
                    errors += '- ' + nm + ' is required.\n';
                }
            }
        }
        if (errors) alert('The following error(s) occurred:\n' + errors);
        document.MM_returnValue = (errors == '');
    }
    //-->
</script>


<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td class="txt_big_gray"><?= $page_title ?></td>
    </tr>
</table>


<table width="100%" border="0" cellspacing="1" cellpadding="3">
    <tr>
        <td colspan="5" bgcolor="#000000" height="1" style="padding:0px;"></td>
    </tr>
</table>
<br/>

<!-- Start Boat Transfer Items List -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td class="txt_big_gray">Reservation Combo Product</td>
    </tr>
</table>
<table width="100%" border="0" cellspacing="1" cellpadding="3" bgcolor="#FFFFFF">
    <?php

    $row = "";
    $sql = "select * from reservationpackage_item where reservationpackages_id = $id order by rpt_id ASC";
    $result = mysql_query($sql);
    $numrows = mysql_num_rows($result);
    $i = 0;
    if ($row = mysql_fetch_array($result)) {
        if ($color == $color_2) $color = $color_3; else $color = $color_2;
        if ($i == 0) {
            ?>

            <tr bgcolor="<?= $color_1 ?>">

                <td class="txt_bold_white">Combo Product Name</td>
                <td class="txt_bold_white">Ticket No.</td>
                <td class="txt_bold_white">Ref</td>
                <td class="txt_bold_white">Rate Type</td>
                <td class="txt_bold_white" align="center">Adult</td>
                <td class="txt_bold_white" align="center">Child</td>
                <td class="txt_bold_white" align="center">Infant</td>
                <td class="txt_bold_white" align="center">G</td>
                <td class="txt_bold_white" align="center">FOC</td>
                <td class="txt_bold_white" align="right">Price</td>
                <td class="txt_bold_white" align="right">Edit</td>


            </tr>
            <?php
        }
        ?>
        <tr bgcolor="<?= $color ?>">
            <td><?= $row['rpt_name'] ?></td>
            <td><?= $row['rpt_ticket_no'] ?></td>
            <td><?= $row['rpt_ref'] ?></td>
            <td><?= $row['rpt_ratetype'] ?></td>
            <td align="center"><?= $row['rpt_adult_num'] ?></td>
            <td align="center"><?= $row['rpt_child_num'] ?></td>
            <td align="center"><?= $row['rpt_infant_num'] ?></td>
            <td align="center"><?= $row['rpt_g_num'] ?></td>
            <td align="center"><?= $row['rpt_foc_num'] ?></td>
            <td align="right"><?= number_format($row['rpt_prices'], 0) ?>
                <?php
                $total_prices += $row['rpt_prices'];
                ?>
            </td>
            <td align="center"><a
                    href="./?mode=reservation_packages/item_add_st1&id=<?= $id ?>&rpt_id=<?= $row[0] ?>"><img
                        src="images/icon_menu/edit.png" border="0" width="20" height="20" align="absmiddle"/></a></td>

        </tr>
        <?php
        $i++;
    }
    if ($i != 0) {
        ?>
        <tr bgcolor="<?= $color_1 ?>">
            <td colspan="20">&nbsp;</td>
        </tr>

        <?php
    } else {
        ?>
        <tr bgcolor="<?= $color_3 ?>" height="100">
            <td align="center">Record Not Found!</td>
        </tr>
        <?php
    }
    ?>
</table>
<!-- END Boat Transfer Items List -->

<br/>
<table width="100%" border="0" cellspacing="1" cellpadding="3" bgcolor="#FFFFFF">
    <tr bgcolor="<?= $color_1 ?>">
        <td class="txt_bold_white">Items Include</td>
        <td class="txt_bold_white">Product Type</td>
        <td class="txt_bold_white">Pick Up From</td>
        <td class="txt_bold_white">Drop-off to</td>
        <td class="txt_bold_white">Room No.</td>
        <td class="txt_bold_white">Travel Date</td>
    </tr>
    <?php
    $rpt_item_name_arr = explode("~", $row['rpt_item_name_arr']);
    $rpt_item_travel_date_arr = explode("~", $row['rpt_item_travel_date_arr']);
    $rpt_item_producttype_id_arr = explode("~", $row['3rpt_item_producttype_id_arr']);
    $rpt_item_id_arr = explode("~", $row['rpt_item_id_arr']);
    $rpt_pickup_from_arr = explode("~", $row['3rpt_pickup_from_arr']);
    $rpt_pickup_to_arr = explode("~", $row['rpt_pickup_to_arr']);
    $rpt_room_no_arr = explode("~", $row['rpt_room_no_arr']);

    foreach ($rpt_item_id_arr as $ids => $value) {
        if ($value) {
            //echo $value." <br>";
            ?>
            <tr bgcolor="#cccccc">
                <td align="left"><?= $rpt_item_name_arr[$ids] ?> </td>
                <td align="left"><?= get_value('lis_reserv_producttype', 'lis_id', 'lis_name', $rpt_item_producttype_id_arr[$ids]) ?></td>
                <td align="left"><?= $rpt_pickup_from_arr[$ids] ?></td>
                <td align="left"><?= $rpt_pickup_to_arr[$ids] ?></td>
                <td align="left"><?= $rpt_room_no_arr[$ids] ?></td>
                <td align="left"><?= DateFormat($rpt_item_travel_date_arr[$ids], "s") ?></td>
            </tr>

            <?php
        } // END  if($value){
    } // END  foreach($rpt_item_id_arr as $ids => $value){
    ?>
</table>

<br/>


<table width="100%" border="0" cellspacing="1" cellpadding="3">
    <tr>
        <td colspan="5" bgcolor="#000000" height="1" style="padding:0px;"></td>
    </tr>
</table>

<?php
$row = "";
$sql = "select * from reservation_packages where rpa_id = $id";
$result = mysql_query($sql);
$row = mysql_fetch_array($result);
?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td class="txt_big_gray"><br/>History</td>
    </tr>
    <tr>
        <td class="txt_big_gray"><br/><textarea cols="155" rows="10" name="rpa_history"
                                                readonly="readonly"><?= $row['rpa_history'] ?></textarea></td>
    </tr>
</table>