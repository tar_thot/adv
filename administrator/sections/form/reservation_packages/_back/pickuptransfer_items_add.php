<?php
if ($id) {
    $sql = "select * from reservations where res_id = $id";
    $result = mysql_query($sql);
    $row = mysql_fetch_array($result);
    include("inc.top.php");
    if ($rpt_id) {
        $page_title = "Update Reservation Pick Up Transfer";
        $sql = "select * from reservation_pickuptransfer_items where rpt_id = $rpt_id";
        $result = mysql_query($sql);
        $row = mysql_fetch_array($result);

        //echo "sql : $sql";
    } else {
        $row = "";
        $page_title = "Create New Reservation Pick Up Transfer";
    }
    ?>
    <script language="JavaScript" type="text/JavaScript">
        <!--
        function MM_findObj(n, d) { //v4.01
            var p, i, x;
            if (!d) d = document;
            if ((p = n.indexOf("?")) > 0 && parent.frames.length) {
                d = parent.frames[n.substring(p + 1)].document;
                n = n.substring(0, p);
            }
            if (!(x = d[n]) && d.all) x = d.all[n];
            for (i = 0; !x && i < d.forms.length; i++) x = d.forms[i][n];
            for (i = 0; !x && d.layers && i < d.layers.length; i++) x = MM_findObj(n, d.layers[i].document);
            if (!x && d.getElementById) x = d.getElementById(n);
            return x;
        }
        function MM_validateForm() { //v4.0
            var i, p, q, nm, test, num, min, max, errors = '', args = MM_validateForm.arguments;
            for (i = 0; i < (args.length - 2); i += 3) {
                test = args[i + 2];
                val = MM_findObj(args[i]);
                if (val) {
                    nm = val.name;
                    if ((val = val.value) != "") {
                        if (test.indexOf('isEmail') != -1) {
                            p = val.indexOf('@');
                            if (p < 1 || p == (val.length - 1)) errors += '- E-mail must contain an e-mail address.\n';
                        } else if (test != 'R') {
                            num = parseFloat(val);
                            if (isNaN(val)) {
                                if (nm == 'name') nm = 'Room Type';
                                errors += '- ' + nm + ' must contain a number.\n';
                            }
                            if (test.indexOf('inRange') != -1) {
                                p = test.indexOf(':');
                                min = test.substring(8, p);
                                max = test.substring(p + 1);
                                if (num < min || max < num) errors += '- ' + nm + ' must contain a number between ' + min + ' and ' + max + '.\n';
                            }
                        }
                    } else if (test.charAt(0) == 'R') {
                        if (nm == 'tourt_name') nm = 'Reservation Pick Up Transfer';
                        errors += '- ' + nm + ' is required.\n';
                    }
                }
            }
            if (errors) alert('The following error(s) occurred:\n' + errors);
            document.MM_returnValue = (errors == '');
        }
        //-->
    </script>

    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="txt_big_gray"><?= $page_title ?></td>
        </tr>
    </table>
    <form action="process.php?mode=reservations/pickuptransfer_items_save&id=<?= $id ?>&rpt_id=<?= $rpt_id ?>"
          onsubmit="MM_validateForm(
'tourt_name','','R'
);return document.MM_returnValue" name="form" method="post" enctype="multipart/form-data">
        <input type="hidden" name="last_edit_by" value="<?= $perms["NAME"] ?>(Back Office System)"/>
        <table width="100%" border="0" cellspacing="0" cellpadding="3">
            <tr>
                <td align="right" class="txt_bold_gray" width="317">Name :</td>
                <td width="1076"><input type="text" name="rpt_name" value="<?= $row[rpt_name] ?>" size="50"/></td>
            </tr>


            <tr>
                <td align="right" class="txt_bold_gray">Ref :</td>
                <td><input type="text" name="rpt_ref" value="<?= $row[rpt_ref] ?>" size="50"/></td>
            </tr>

            <tr>
                <?php
                if ($row[rpt_travel_date] > 0) {
                    $rpt_travel_date = $row[rpt_travel_date];
                } else {
                    $rpt_travel_date = $today;
                }
                ?>
                <td align="right" class="txt_bold_gray"> Travel Date :</td>
                <td>
                    <script>DateInput('rpt_travel_date', false, 'yyyy-mm-dd', '<?= $rpt_travel_date?>')</script>
                </td>
            </tr>

            <tr>
                <td align="right" class="txt_bold_gray">Rate Type :</td>
                <td><input type="text" name="rpt_ratetype" value="<?= $row[rpt_ratetype] ?>" size="50"/></td>
            </tr>

            <tr>
                <td align="right" class="txt_bold_gray">Car :</td>
                <td><select name="rpt_car_num">
                        <option value="">--Select--</option>
                        <?php list_number($row[rpt_car_num], 1, 10); ?>
                    </select></td>
            </tr>

            <tr>
                <td align="right" class="txt_bold_gray">Audlt :</td>
                <td><select name="rpt_adult_num">

                        <?php list_number($row[rpt_adult_num], 1, 10); ?>
                    </select></td>
            </tr>

            <tr>
                <td align="right" class="txt_bold_gray">Child :</td>
                <td><select name="rpt_child_num">

                        <?php list_number($row[rpt_child_num], 0, 11); ?>
                    </select></td>
            </tr>

            <tr>
                <td align="right" class="txt_bold_gray">Infant :</td>
                <td><select name="rpt_infant_num">

                        <?php list_number($row[rpt_infant_num], 0, 11); ?>
                    </select></td>
            </tr>

            <tr>
                <td align="right" class="txt_bold_gray">Pick Up From :</td>
                <td><input type="text" name="rpt_pickupfrom" value="<?= $row[rpt_pickupfrom] ?>" size="50"/></td>
            </tr>

            <tr>
                <td align="right" class="txt_bold_gray">Pick Up Time :</td>
                <td><input type="text" name="rpt_pickuptime" value="<?= $row[rpt_pickuptime] ?>" size="50"/></td>
            </tr>

            <tr>
                <td align="right" class="txt_bold_gray">Price :</td>
                <td><input type="text" name="rpt_prices" value="<?= $row[rpt_prices] ?>" size="50"/></td>
            </tr>


            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td><input type="submit" name="submit" value="SUBMIT" style="width:75px;"><?php
                    if ($rpt_id) echo " <input type=\"submit\" name=\"del\" value=\"DELETE\" style=\"width:75;\" onClick=\"return confirm('Do you want to delete this record ?')\">";
                    else echo " <input type=\"reset\" name=\"Reset\" value=\"RESET\" style=\"width:75;\">";
                    ?></td>
            </tr>
        </table>
    </form>
    <?php
}
?>
