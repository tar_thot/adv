<?php
include("value_get.php");
if ($id) {
    $page_title = "Update Combo Product  ";
    $sql = "select * from reservation_packages where rpa_id = $id";
    $result = mysql_query($sql);
    $row = mysql_fetch_array($result);
    include("inc.top.php");
}

$total_prices = 0;
?>

<script language="JavaScript" type="text/JavaScript">
    <!--
    function MM_findObj(n, d) { //v4.01
        var p, i, x;
        if (!d) d = document;
        if ((p = n.indexOf("?")) > 0 && parent.frames.length) {
            d = parent.frames[n.substring(p + 1)].document;
            n = n.substring(0, p);
        }
        if (!(x = d[n]) && d.all) x = d.all[n];
        for (i = 0; !x && i < d.forms.length; i++) x = d.forms[i][n];
        for (i = 0; !x && d.layers && i < d.layers.length; i++) x = MM_findObj(n, d.layers[i].document);
        if (!x && d.getElementById) x = d.getElementById(n);
        return x;
    }
    function MM_validateForm() { //v4.0
        var i, p, q, nm, test, num, min, max, errors = '', args = MM_validateForm.arguments;
        for (i = 0; i < (args.length - 2); i += 3) {
            test = args[i + 2];
            val = MM_findObj(args[i]);
            if (val) {
                nm = val.name;
                if ((val = val.value) != "") {
                    if (test.indexOf('isEmail') != -1) {
                        p = val.indexOf('@');
                        if (p < 1 || p == (val.length - 1)) errors += '- E-mail must contain an e-mail address.\n';
                    } else if (test != 'R') {
                        num = parseFloat(val);
                        if (isNaN(val)) {
                            if (nm == 'category_id') nm = 'Category';
                            errors += '- ' + nm + ' must contain a number.\n';
                        }
                        if (test.indexOf('inRange') != -1) {
                            p = test.indexOf(':');
                            min = test.substring(8, p);
                            max = test.substring(p + 1);
                            if (num < min || max < num) errors += '- ' + nm + ' must contain a number between ' + min + ' and ' + max + '.\n';
                        }
                    }
                } else if (test.charAt(0) == 'R') {
                    if (nm == 'category_id') nm = 'Category';
                    if (nm == 'rpa_name') nm = 'Name';
                    if (nm == 'rpa_ref') nm = 'Ref';
                    if (nm == 'check_rpa_ref') nm = 'Ref';
                    if (nm == 'activitypricetype_id') nm = 'Price Type';
                    errors += '- ' + nm + ' is required.\n';
                }
            }
        }
        if (errors) alert('The following error(s) occurred:\n' + errors);
        document.MM_returnValue = (errors == '');
    }
    //-->
</script>


<?php


$sql_packages_items = "select * from packages_items Where packages_id = $packages_id ";

$sql_packages_items .= "order by pacitem_id ASC ";
$result_packages_items = mysql_query($sql_packages_items);
$numrows = mysql_num_rows($result_packages_items);

$i = 0;
$items_package_producttype_id = '';
$items_id = '';
$items_name = '';
while ($row_packages_items = mysql_fetch_array($result_packages_items)) {
    if ($row_packages_items['package_producttype_id'] == 1) {
        // Boattransfer
        $sql_boattransfers = "SELECT * ";
        $sql_boattransfers .= "FROM boattransfers ";
        $sql_boattransfers .= "where bot_id = " . $row_packages_items['product_id'] . " ";
        $result_boattransfers = mysql_query($sql_boattransfers);
        $row_boattransfers = mysql_fetch_array($result_boattransfers);


        $items_package_producttype_id[] = $row_packages_items['package_producttype_id'];
        $items_id[] = $row_boattransfers['bot_id'];
        $items_name[] = $row_boattransfers['bot_name'];
    }

    if ($row_packages_items['package_producttype_id'] == 2) {
        // Pick Up Transfer
        $sql_pickuptransfer = "SELECT * ";
        $sql_pickuptransfer .= "FROM pickuptransfers ";
        $sql_pickuptransfer .= "where put_id = " . $row_packages_items['product_id'] . " ";
        $result_pickuptransfer = mysql_query($sql_pickuptransfer);
        $row_pickuptransfer = mysql_fetch_array($result_pickuptransfer);


        $items_package_producttype_id[] = $row_packages_items['package_producttype_id'];
        $items_id[] = $row_pickuptransfer['put_id'];
        $items_name[] = $row_pickuptransfer['put_name'];
    }

    if ($row_packages_items['package_producttype_id'] == 3) {
        // Tour
        $sql_tours = "SELECT * ";
        $sql_tours .= "FROM tours ";
        $sql_tours .= "where tou_id = " . $row_packages_items['product_id'] . " ";
        $result_tours = mysql_query($sql_tours);
        $row_tours = mysql_fetch_array($result_tours);


        $items_package_producttype_id[] = $row_packages_items['package_producttype_id'];
        $items_id[] = $row_tours['tou_id'];
        $items_name[] = $row_tours['tou_name'];
    }

    if ($row_packages_items['package_producttype_id'] == 4) {
        // Activity
        $sql_activities = "SELECT * ";
        $sql_activities .= "FROM activities ";
        $sql_activities .= "where act_id = " . $row_packages_items['product_id'] . " ";
        $result_activities = mysql_query($sql_activities);
        $row_activities = mysql_fetch_array($result_activities);


        $items_package_producttype_id[] = $row_packages_items['package_producttype_id'];
        $items_id[] = $row_activities['act_id'];
        $items_name[] = $row_activities['act_name'];
    }

    if ($row_packages_items['package_producttype_id'] == 5) {
        // Bus Transfer
        $sql_cartransfers = "SELECT * ";
        $sql_cartransfers .= "FROM cartransfers ";
        $sql_cartransfers .= "where ct_id =" . $row_packages_items['product_id'] . " ";
        $result_cartransfers = mysql_query($sql_cartransfers);
        $row_cartransfers = mysql_fetch_array($result_cartransfers);


        $items_package_producttype_id[] = $row_packages_items['package_producttype_id'];
        $items_id[] = $row_cartransfers['ct_id'];
        $items_name[] = $row_cartransfers['ct_name'];
    }

    if ($row_packages_items['package_producttype_id'] == 6) {
        // Private Land Transfer
        $sql_privatelandtransfers = "SELECT * ";
        $sql_privatelandtransfers .= "FROM privatelandtransfers ";
        $sql_privatelandtransfers .= "where plt_id = " . $row_packages_items['product_id'] . " ";
        $result_privatelandtransfers = mysql_query($sql_privatelandtransfers);
        $row_privatelandtransfers = mysql_fetch_array($result_privatelandtransfers);


        $items_package_producttype_id[] = $row_packages_items['package_producttype_id'];
        $items_id[] = $row_privatelandtransfers['plt_id'];
        $items_name[] = $row_privatelandtransfers['plt_name'];
    }

    if ($row_packages_items['package_producttype_id'] == 7) {
        // Hotel
        $sql_hotels = "SELECT * ";
        $sql_hotels .= "FROM hotels ";
        $sql_hotels .= "where hot_id = " . $row_packages_items['product_id'];
        $result_hotels = mysql_query($sql_hotels);
        $row_hotels = mysql_fetch_array($result_hotels);


        $items_package_producttype_id[] = $row_packages_items['package_producttype_id'];
        $items_id[] = $row_hotels['hot_id'];
        $items_name[] = $row_hotels['hot_name'];
    }

    if ($row_packages_items['package_producttype_id'] == 8) {
        // Train Transfer
        $sql_traintransfers = "SELECT * ";
        $sql_traintransfers .= "FROM traintransfers ";
        $sql_traintransfers .= "where train_id = " . $row_packages_items['product_id'] . " ";
        $result_traintransfers = mysql_query($sql_traintransfers);
        $row_traintransfers = mysql_fetch_array($result_traintransfers);


        $items_package_producttype_id[] = $row_packages_items['package_producttype_id'];
        $items_id[] = $row_traintransfers['train_id'];
        $items_name[] = $row_traintransfers['train_name'];
    }

}


?>

<?php

if ($packages_id == $current_packages_id) {
    //echo "Same Package";

    $sql_reservationpackage_item = "select * from reservationpackage_item where reservationpackages_id = $id order by rpt_id ASC";
    $result_reservationpackage_item = mysql_query($sql_reservationpackage_item);
    $numrows_reservationpackage_item = mysql_num_rows($result_reservationpackage_item);
    $i = 0;
    $row_reservationpackage_item = mysql_fetch_array($result_reservationpackage_item);


    $rpt_item_name_arr = explode("~", $row_reservationpackage_item['rpt_item_name_arr']);
    $rpt_item_travel_date_arr = explode("~", $row_reservationpackage_item['rpt_item_travel_date_arr']);
    $rpt_item_producttype_id_arr = explode("~", $row_reservationpackage_item['rpt_item_producttype_id_arr']);
    $rpt_item_id_arr = explode("~", $row_reservationpackage_item['rpt_item_id_arr']);
    $rpt_pickup_from_arr = explode("~", $row_reservationpackage_item['rpt_pickup_from_arr']);
    $rpt_pickup_to_arr = explode("~", $row_reservationpackage_item['rpt_pickup_to_arr']);
    $rpt_room_no_arr = explode("~", $row_reservationpackage_item['rpt_room_no_arr']);

} else { // END if($packages_id == $current_packages_id){
    //echo "Not Same Package";	
} // END }else{ // END if($packages_id == $current_packages_id){
?>


<script type="text/javascript">
    function checkSubmitFormAddCart() {

        // Check Selecet Travel Date


        <?php
        foreach($items_name as $items_name_ids => $value){
        ?>
        var select_travel_date_str_<?= $items_name_ids?> = document.form.res_pac_travel_date_<?= $items_name_ids?>.value;

        document.getElementById("res_pac_travel_date_id_<?= $items_name_ids?>").value = select_travel_date_str_<?= $items_name_ids?> ;

        <?php
        } // END foreach($items_name as $items_name_ids => $value){
        ?>







        return true;
    }


</script>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td class="txt_big_gray"><?= $page_title ?></td>
    </tr>
</table>
<form action="process.php?mode=reservation_packages/item_save&id=<?= $id ?>&rpt_id=<?= $rpt_id ?>"
      onsubmit="return checkSubmitFormAddCart();" name="form" method="post" enctype="multipart/form-data">
    <input type="hidden" name="packages_id" value="<?= $packages_id ?>">

    <table width="100%" border="0" cellspacing="0" cellpadding="3">

        <tr>
            <td></td>
            <td></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray">Combo Product Name :</td>
            <td>
                <?= get_value('packages', 'pac_id', 'pac_name', $packages_id, 'N'); ?></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray">Ref :</td>
            <td>
                <?= get_value('packages', 'pac_id', 'pac_ref', $packages_id, 'N'); ?></td>
        </tr>


        <?php


        foreach ($items_name as $items_name_ids => $value) {
            //echo $value."<br>";
            $rpt_item_ids = $items_name_ids + 1;
            ?>

            <tr>
                <td align="right">Travel Date for <?= $value ?> :</td>
                <td>
                    <input type="hidden" name="items_package_producttype_id_arr[]"
                           value="<?= $items_package_producttype_id[$items_name_ids] ?>">
                    <input type="hidden" name="items_id_arr[]" value="<?= $items_id[$items_name_ids] ?>">
                    <input type="hidden" name="items_name_arr[]" value="<?= $items_name[$items_name_ids] ?>">
                    <input type="hidden" name="res_pac_travel_date_arr[]"
                           id="res_pac_travel_date_id_<?= $items_name_ids ?>" value="">
                    <?php
                    if (isset($rpt_item_travel_date_arr[$rpt_item_ids])) {
                        $travel_date_str = $rpt_item_travel_date_arr[$rpt_item_ids];
                    } else {
                        $travel_date_str = $today;
                    }


                    ?>
                    <script>DateInput('res_pac_travel_date_<?= $items_name_ids?>', false, 'yyyy-mm-dd', '<?= $travel_date_str?>')</script>
                </td>
            </tr>


            <?php
            if ($items_package_producttype_id[$items_name_ids] == 2) {
                // Pick Up Transfer
                ?>


                <tr>
                    <td align="right">Pick Up From for <?= $value ?>:</td>
                    <td>
                        <input type="text" name="res_pac_pickup_from_arr[]"
                               value="<?= $rpt_pickup_from_arr[$rpt_item_ids] ?>"></td>
                </tr>

                <tr>
                    <td align="right">Drop Off for <?= $value ?>:</td>
                    <td>
                        <input type="text" name="res_pac_pickup_to_arr[]"
                               value="<?= $rpt_pickup_to_arr[$rpt_item_ids] ?>"></td>
                </tr>

                <tr>
                    <td align="right">Room No. for <?= $value ?>:</td>
                    <td>
                        <input type="text" name="res_pac_room_no_arr[]" value="<?= $rpt_room_no_arr[$rpt_item_ids] ?>">
                    </td>
                </tr>

                <?php
            } else if ($items_package_producttype_id[$items_name_ids] == 5) {
                // Bus Transfer
                ?>

                <tr>
                    <td align="right">Pick Up From for <?= $value ?>:</td>
                    <td>
                        <input type="text" name="res_pac_pickup_from_arr[]"
                               value="<?= $rpt_pickup_from_arr[$rpt_item_ids] ?>"></td>
                </tr>

                <tr>
                    <td align="right">Drop Off for <?= $value ?>:</td>
                    <td>
                        <input type="text" name="res_pac_pickup_to_arr[]"
                               value="<?= $rpt_pickup_to_arr[$rpt_item_ids] ?>"></td>
                </tr>

                <tr>
                    <td align="right">Room No. for <?= $value ?>:</td>
                    <td>
                        <input type="text" name="res_pac_room_no_arr[]" value="<?= $rpt_room_no_arr[$rpt_item_ids] ?>">
                    </td>
                </tr>

                <?php
            } else if ($items_package_producttype_id[$items_name_ids] == 6) {
                // Bus Transfer
                ?>

                <tr>
                    <td align="right">Pick Up From for <?= $value ?>:</td>
                    <td>
                        <input type="text" name="res_pac_pickup_from_arr[]"
                               value="<?= $rpt_pickup_from_arr[$rpt_item_ids] ?>"></td>
                </tr>

                <tr>
                    <td align="right">Drop Off for <?= $value ?>:</td>
                    <td>
                        <input type="text" name="res_pac_pickup_to_arr[]"
                               value="<?= $rpt_pickup_to_arr[$rpt_item_ids] ?>"></td>
                </tr>

                <tr>
                    <td align="right">Room No. for <?= $value ?>:</td>
                    <td>
                        <input type="text" name="res_pac_room_no_arr[]" value="<?= $rpt_room_no_arr[$rpt_item_ids] ?>">
                    </td>
                </tr>


                <?php
            } else { // END if($items_package_producttype_id[$items_name_ids] == 2){
                ?>
                <input type="hidden" name="res_pac_pickup_from_arr[]" value="">
                <input type="hidden" name="res_pac_pickup_to_arr[]" value="">
                <input type="hidden" name="res_pac_room_no_arr[]" value="">
                <?php
            } // END }else{ // END if($items_package_producttype_id[$items_name_ids] == 2){
            ?>

            <?php
        } // END foreach($items_name as $items_name_ids => $value){

        ?>

        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>

        <tr>
            <td align="right">Rate Type :</td>
            <td>
                <select name="rpt_ratetype">

                    <?php listbox('package_ratetypes', 'pacrt_name', 'pacrt_name', $row_reservationpackage_item['rpt_ratetype'], "packages_id = $packages_id"); ?>

                </select></td>
        </tr>

        <tr>
            <td align="right">Adult :</td>
            <td>
                <select name="res_pac_adult_num">
                    <?php list_number($row_reservationpackage_item['rpt_adult_num'], 1, 999); ?>
                </select></td>
        </tr>

        <tr>
            <td align="right">Child :</td>
            <td>
                <select name="res_pac_child_num">
                    <?php list_number($row_reservationpackage_item['rpt_child_num'], 0, 1000); ?>
                </select></td>
        </tr>

        <tr>
            <td align="right">Infant :</td>
            <td>
                <select name="res_pac_infant_num">
                    <?php list_number($row_reservationpackage_item['rpt_infant_num'], 0, 1000); ?>
                </select></td>
        </tr>

        <tr>
            <td align="right">G :</td>
            <td>
                <select name="res_pac_g_num">
                    <?php list_number($row_reservationpackage_item['rpt_g_num'], 0, 1000); ?>
                </select></td>
        </tr>

        <tr>
            <td align="right">FOC :</td>
            <td>
                <select name="res_pac_foc_num">
                    <?php list_number($row_reservationpackage_item['rpt_foc_num'], 0, 1000); ?>
                </select></td>
        </tr>

        <tr>
            <td align="right">Price :</td>
            <td>
                <input type="text" name="rpt_prices" value="<?= $row_reservationpackage_item['rpt_prices'] ?>">
            </td>
        </tr>

        <tr>
            <td align="right">Ticket No. :</td>
            <td>
                <input type="text" name="rpt_ticket_no" value="<?= $row_reservationpackage_item['rpt_ticket_no'] ?>">
            </td>
        </tr>

        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td><input type="submit" name="submit" value="Submit" style="width:75px;"></td>
        </tr>
    </table>
</form>