<?php

function setNumberLength($num, $length)
{
    $sumstr = strlen($num);
    $zero = str_repeat("0", $length - $sumstr);
    $results = $zero . $num;

    return $results;
}

?>

<table width="100%" border="0" cellspacing="1" cellpadding="3" bgcolor="#ffffff">

    <tr bgcolor="#eeeeee">
        <td align="right" class="txt_bold_gray">Reservation Combo Product ID :</td>
        <td><?= $row['rpa_id_str'] ?>       </td>
        <td align="right" class="txt_bold_gray">Agent :</td>
        <td><?= get_value('agents', 'ag_id', 'ag_name', $row['agents_id']) ?>        </td>
    </tr>

    <tr bgcolor="#eeeeee">
        <td align="right" class="txt_bold_gray">Name :</td>
        <td>
            <?= get_value('lis_titlename', 'lis_id', 'lis_name', $row['titlename_id']) ?>
            <?= $row['rpa_fname'] ?>
            <?= $row['rpa_lname'] ?>       </td>
        <td align="right" class="txt_bold_gray">Email :</td>
        <td><?= $row['rpa_email'] ?>        </td>
    </tr>


    <tr bgcolor="#eeeeee">
        <td align="right" class="txt_bold_gray">Phone Number :</td>
        <td><?= $row['rpa_phone'] ?>        </td>
        <td align="right" class="txt_bold_gray">Fax Number :</td>
        <td><?= $row['rpa_fax'] ?>        </td>
    </tr>


    <tr bgcolor="#eeeeee">
        <td align="right" class="txt_bold_gray">Address :</td>
        <td><?= nl2br($row['rpa_address']) ?>        </td>
        <td align="right" class="txt_bold_gray">Remark :</td>
        <td><?= nl2br($row['rpa_request']) ?>        </td>
    </tr>


    <tr bgcolor="#eeeeee">
        <td align="right" class="txt_bold_gray" width="317"> Book Date :</td>
        <td><?= DateFormat($row['rpa_date'], "s") ?></td>
        <td align="right" class="txt_bold_gray">Booking Status :</td>
        <td><?= get_value('lis_booking_status', 'lis_id', 'lis_name', $row['bookingstatus_id']) ?>        </td>
    </tr>


    <tr bgcolor="#eeeeee">
        <td align="right" class="txt_bold_gray">Note :</td>
        <td><textarea cols="20" rows="2" name="rpa_note" readonly="readonly"><?= nl2br($row['rpa_note']) ?></textarea>
        </td>
        <td align="right" class="txt_bold_gray">Payment Status :</td>
        <td><?= get_value('lis_payment_status', 'lis_id', 'lis_name', $row['paymentstatus_id']) ?>        </td>
    </tr>

    <tr bgcolor="#eeeeee">
        <td align="right" class="txt_bold_gray">Agent Voucher :</td>
        <td><?= $row['rpa_agent_voucher'] ?>        </td>
        <td align="right" class="txt_bold_gray">Voucher Sent Status :</td>
        <td>
            <?php
            $i = 1;

            $sql_vo = "SELECT * FROM voucher WHERE vo_res_id = '" . $row['rpa_id_str'] . "' ";
            $sql_vo .= "AND vo_status = 2 ";
            $sql_vo .= "ORDER BY vo_id ASC ";

            //echo $sql_vo;

            $result_vo = mysql_query($sql_vo);
            $row_vo = mysql_fetch_array($result_vo);

            if (isset($row_vo['vo_no'])) {
                echo $row_vo['vo_no'];
            } else {
            }
            ?>

            <?php
            if ($row['rpa_v_status']) {
                echo "[Yes]";
            } else {
                echo "[No]";
            }
            ?>

            <?php
            if ($row['paymentstatus_id'] == 1) {
                // Status = Waiting
            } else {
                // Status = Paid, Invoice

                if ($row['bookingstatus_id'] == 3) {
                    ?>

                    <script type="text/javascript">
                        function clickBntSendVoucher() {

                            //alert('clickBntSendVoucher');
                            if (confirm('Are You Sure To Send Voucher ?')) {
                                window.location.replace("process.php?mode=voucher/send_voucher_save&id=<?= $id?>&type_id=<?php echo '2'; ?>&v_status=<?=$row['rpa_v_status'];?>");
                            } else {

                            }

                        }
                    </script>

                    <input type="button" name="bnt_send_voucher" value="Send Voucher" onclick="clickBntSendVoucher();">
                    <?php
                } // END if($row[bookingstatus_id] == 3){

            } // END }else{ if($row[paymentstatus_id] == 1){
            ?>


        </td>
    </tr>

    <tr bgcolor="#eeeeee">
        <td align="right" class="txt_bold_gray">Cash Collection :</td>
        <td><?= $row['rpa_cash_collection'] ?>        </td>
        <td align="right" class="txt_bold_gray">Booking By :</td>
        <td><?= $row['rpa_bookby'] ?>        </td>
    </tr>

    <tr bgcolor="#eeeeee">
        <td align="right" class="txt_bold_gray">Company :</td>
        <td><?= get_value('lis_company', 'lis_id', 'lis_name', $row['company']) ?>        </td>
        <td align="right" class="txt_bold_gray">Comfirmed By :</td>
        <td><?= $row['confirmed_by'] ?>        </td>
    </tr>

    <tr bgcolor="#eeeeee">
        <td align="right" class="txt_bold_gray">Prepared By :</td>
        <td><?= $row['prepared_by'] ?>        </td>
        <td align="right" class="txt_bold_gray">Client :</td>
        <td><?php if (isset($row['cl_id'])) {
                echo "Yes";
            } else {
                echo "No";
            } ?>        </td>
    </tr>

    <tr bgcolor="#eeeeee">
        <td align="right" class="txt_bold_gray">Confirm Payment :</td>
        <td><?= get_value('lis_comfirm_payment', 'lis_id', 'lis_name', $row['confirm_pay_id']) ?>        </td>
        <td align="right" class="txt_bold_gray">Confirm Date :</td>
        <td><?= $row['confirm_date'] ?>    </td>
    </tr>

    <tr bgcolor="#eeeeee">
        <td align="right" class="txt_bold_gray">Invoice :</td>
        <td>
            <?php
            $sql_in = "SELECT * FROM invoice WHERE in_res_id = '" . $row['rpa_id_str'] . "' ";
            $sql_in .= "AND in_status = 2 ";
            $sql_in .= "ORDER BY in_id ASC ";

            //echo $sql_in;

            $result_in = mysql_query($sql_in);
            $row_in = mysql_fetch_array($result_in);

            ?><a href="<?= $c_URL ?>/photo/invoice/<?= $row_in['in_pic'] ?>"
                 target="_blank"><?= $row_in['in_no'] ?></a><?php

            if ($row_in['in_no']) {
                echo " [Yes]";
            } else {
                echo " [No]";
            } ?>        </td>
        <td align="right" class="txt_bold_gray">&nbsp;</td>
        <td>&nbsp;</td>
    </tr>


</table>

<table width="100%" border="0" cellspacing="1" cellpadding="3" bgcolor="<?= $color_inctop1 ?>">
    <tr bgcolor="<?= $color_inctop2 ?>">
        <td><span class="txt_bold_white">
        <a href="./?mode=reservation_packages/view&id=<?= $id ?>" class="txt_bold_white">View</a><strong
                    style="color:<?= $color_inctop1 ?>;"> | </strong>
        <a href="./?mode=reservation_packages/add&id=<?= $id ?>" class="txt_bold_white">Edit</a><strong
                    style="color:<?= $color_inctop1 ?>;"> | </strong>

        </span></td>
    </tr>
</table>
<br/>
