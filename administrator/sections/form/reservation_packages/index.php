<?php

function setNumberLength($num, $length)
{
    $sumstr = strlen($num);
    $zero = str_repeat("0", $length - $sumstr);
    $results = $zero . $num;

    return $results;
}

?>

<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td valign="top">
            <table width="100%" border="0" cellspacing="0" cellpadding="3">
                <tr>
                    <td class="txt_big_gray">Reservation Combo Product List
                        <!--[ <a href="./?mode=reservation_packages/add">Create New Reservation </a> ]--></td>
                </tr>
            </table>
            <!---- Listing Body ---->
            <table width="100%" border="0" cellspacing="0" cellpadding="3">
                <?php
                include("value_get.php");

                $linkp = "&search=" . $search . "&plt_ref=" . $plt_ref . "";
                $linkp = '';


                $sql = "select * from reservation_packages ";
                $sql .= "where rpa_id > 0 ";

                if (isset($rpa_id_str)) {
                    $sql .= "AND rpa_id_str like '%" . $rpa_id_str . "%' ";
                    $linkp .= '&rpa_id_str=' . $rpa_id_str;
                }
                if (isset($agents_id)) {
                    $sql .= "AND agents_id = '" . $agents_id . "' ";
                    $linkp .= '&agents_id=' . $agents_id;
                }
                if (isset($rpa_fname)) {
                    $sql .= "AND rpa_fname like '%" . $rpa_fname . "%' ";
                    $linkp .= '&rpa_fname=' . $rpa_fname;
                }
                if (isset($rpa_lname)) {
                    $sql .= "AND rpa_lname like '%" . $rpa_lname . "%' ";
                    $linkp .= '&rpa_lname=' . $rpa_lname;
                }
                if (isset($bookingstatus_id)) {
                    $sql .= "AND bookingstatus_id = '" . $bookingstatus_id . "' ";
                    $linkp .= '&bookingstatus_id=' . $bookingstatus_id;
                }
                if (isset($paymentstatus_id)) {
                    $sql .= "AND paymentstatus_id = '" . $paymentstatus_id . "' ";
                    $linkp .= '&paymentstatus_id=' . $paymentstatus_id;
                }
                if (isset($sevice_name)) {
                    // In Item
                    $linkp .= '&sevice_name=' . $sevice_name;
                }
                if (isset($service_date)) {
                    // In Item
                    $linkp .= '&service_date=' . $service_date;
                }
                if (isset($reserv_producttype_id)) {
                    // In Item
                    $linkp .= '&reserv_producttype_id=' . $reserv_producttype_id;
                }

                if (isset($vo_no)) {

                    $sql_vo = "SELECT * FROM voucher WHERE vo_no = '$vo_no' ";
                    $sql_vo .= "AND vo_status = 2 ";
                    $sql_vo .= "ORDER BY vo_id ASC ";

                    $result_vo = mysql_query($sql_vo);
                    $row_vo = mysql_fetch_array($result_vo);

                    $sql .= "AND rpa_id_str = '" . $row_vo['vo_res_id'] . "' ";
                    $linkp .= '&voucher_no=' . $vo_no;
                }
                if (isset($rpa_agent_voucher)) {
                    $sql .= "AND rpa_agent_voucher = '" . $rpa_agent_voucher . "' ";
                    $linkp .= '&rpa_agent_voucher=' . $rpa_agent_voucher;
                }


                $sql .= " order by rpa_id DESC";

                //echo "sql : $sql";
                //echo "<br />";
                //echo "linkp : $linkp";

                $results = mysql_query($sql);
                $perpage = 15;
                $result = $perpage;
                $numrows = mysql_num_rows($results);
                $numpage = $numrows / $result;
                if (!$paper || $paper == 1) {
                    $start = 1;
                    $paper = 1;
                } else {
                    $start = (($paper - 1) * $result) + 1;
                    $result = $result * $paper;
                }
                if ($numrows != 0) $show_page .= "<strong>Pages : </strong>";
                for ($i = 1; $i < $numpage + 1; $i++) {
                    if ($paper == $i) $linkpage = "<strong style=\"color:#CCCCCC\">" . $i . "</strong>";
                    else $linkpage = "<a href='./?mode=reservation_packages/index&paper=" . $i . $linkp . "'><strong>" . $i . "</strong></a>";
                    $show_page .= $linkpage . " ";
                }
                if ($paper == $i - 1) $result = $numrows;

                if ($linkp == '') {
                    $show_row = "<b>Found : </b>" . $numrows . " Reservation(s)";
                }
                ?>
                <tr>
                    <td>
                        <!---- Search Box ---->

                        <table border="0" cellspacing="3" cellpadding="3" style="border: solid #CCCCCC 1px;">
                            <form name="formSearch" method="get" action="./">
                                <tr>
                                    <td class="txt_bold_gray" align="right">
                                        Reservation Combo Product ID<br/>
                                        <input type="text" name="rpa_id_str" value="<?= $rpa_id_str ?>"
                                               style="width:150px;"></td>
                                    <td class="txt_bold_gray" align="right">
                                        Agent<br/>
                                        <select name="agents_id" style="width:155px;">
                                            <option value="">-- Select --</option>
                                            <?php listbox('agents', 'ag_id', 'ag_name', $agents_id, 'N'); ?>
                                        </select></td>
                                    <td class="txt_bold_gray" align="right">
                                        First Name<br/>
                                        <input type="text" name="rpa_fname" value="<?= $rpa_fname ?>"
                                               style="width:150px;"></td>

                                </tr>


                                <tr>
                                    <td class="txt_bold_gray" align="right">
                                        Last Name<br/>
                                        <input type="text" name="rpa_lname" value="<?= $rpa_lname ?>"
                                               style="width:150px;"></td>
                                    <td class="txt_bold_gray" align="right">
                                        Booking Status<br/>
                                        <select name="bookingstatus_id" style="width:155px;">
                                            <option value="">-- Select --</option>
                                            <?php listbox('lis_booking_status', 'lis_id', 'lis_name', $bookingstatus_id, 'N'); ?>
                                        </select></td>
                                    <td class="txt_bold_gray" align="right">
                                        Payment Status<br/>
                                        <select name="paymentstatus_id" style="width:155px;">
                                            <option value="">-- Select --</option>
                                            <?php listbox('lis_payment_status', 'lis_id', 'lis_name', $paymentstatus_id, 'N'); ?>
                                        </select></td>

                                </tr>

                                <tr>
                                    <td class="txt_bold_gray" align="right">
                                        Voucher No.<br/>
                                        <input type="text" name="vo_no" value="<?= $vo_no ?>" style="width:150px;">
                                    </td>

                                    <td class="txt_bold_gray" align="right">
                                        Agent Voucher No.<br/>
                                        <input type="text" name="rpa_agent_voucher" value="<?= $rpa_agent_voucher ?>"
                                               style="width:150px;">
                                    </td>

                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td valign="bottom"><input type="hidden" name="mode" value="<?= $mode ?>"><input
                                            type="submit" name="Submit" value="SEARCH" style="height:25px;"></td>

                                </tr>

                            </form>
                        </table>
                        <!---- Search Box ---->
                    </td>
                    <td align="right" valign="bottom"></td>
                </tr>
                <tr>

                    <td align="right" valign="bottom" colspan="2"><?php
                        echo $show_row;
                        if ($show_page) echo "  " . $show_page;
                        ?></td>
                </tr>
                <?php
                if ($numrows > 0)
                {
                for ($a = 1; $a < $start; $a++) mysql_fetch_array($results);
                ?>
            </table>
            <table width="100%" border="0" cellspacing="1" cellpadding="3" bgcolor="#FFFFFF">
                <form name="form" action="process.php?mode=reservation_packages/save" method="post"
                      enctype="multipart/form-data">
                    <tr bgcolor="<?= $color_1 ?>">
                        <td class="txt_bold_white" align="center" width="45">Delete</td>
                        <td width="100" class="txt_bold_white">ID</td>
                        <td width="100" class="txt_bold_white">&nbsp;Book&nbsp;Date&nbsp;</td>
                        <td width="100" class="txt_bold_white">Agent&nbsp;Name</td>
                        <!--<td width="100" class="txt_bold_white" >Email</td>-->
                        <td width="100" class="txt_bold_white">First&nbsp;Name</td>
                        <td width="100" class="txt_bold_white">Last&nbsp;Name</td>
                        <td width="1000" class="txt_bold_white">Combo Product&nbsp;Name&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        <td width="100" class="txt_bold_white" align="center">Account Code (Adult)</td>
                        <td width="100" class="txt_bold_white" align="center">Account Code (Child)</td>
                        <td width="100" class="txt_bold_white">&nbsp;Travel&nbsp;Date&nbsp;</td>
                        <td width="100" class="txt_bold_white">Booking Status</td>
                        <td width="100" class="txt_bold_white">Payment</td>
                        <td width="100" class="txt_bold_white">Total Amount</td>


                        <td class="txt_bold_white" width="43">&nbsp;</td>
                    </tr>
                    <?php
                    for ($start; $start < $result + 1; $start++) {
                        $row = mysql_fetch_array($results);


                        ?>

                        <?php
                        # Loop For Package Item			
                        $sql_rpt = "SELECT * ";
                        $sql_rpt .= "FROM reservationpackage_item ";
                        $sql_rpt .= "where reservationpackages_id = $row[0] ";


                        if (isset($sevice_name)) {
                            $sql_rpt .= "AND rpt_name like '%" . $sevice_name . "%' ";;
                        }
                        if (isset($service_date)) {
                            $sql_rpt .= "AND rpt_travel_date = '" . $service_date . "' ";
                        }
                        if (isset($reserv_producttype_id) && $reserv_producttype_id != '1') {
                            $sql_rpt .= "AND rpt_id = '0' ";
                        }


                        $sql_rpt .= "order by rpt_id asc ";
                        //echo "sql_rpt : $sql_rpt";


                        $wlinks = "";
                        //if($p_id){$wlinks .= "&p_id=$p_id";}


                        $results_rpt = mysql_query($sql_rpt);
                        while ($rows_rpt = mysql_fetch_array($results_rpt)) {

                            // Select Travel Date

                            $travel_date = "9999-99-99";

                            $rpt_item_travel_date_arr = explode("~", $rows_rpt['rpt_item_travel_date_arr']);
                            $rpt_item_producttype_id_arr = explode("~", $rows_rpt['rpt_item_producttype_id_arr']);

                            foreach ($rpt_item_travel_date_arr as $ids => $value) {
                                if (isset($value)) {
                                    if ($rpt_item_producttype_id_arr[$ids] == 1) {
                                        if ($rpt_item_travel_date_arr[$ids] < $travel_date) {
                                            $travel_date = $rpt_item_travel_date_arr[$ids];
                                        };

                                    } // END if($rpt_item_producttype_id_arr[$ids] == 1){

                                    if ($rpt_item_producttype_id_arr[$ids] == 2) {
                                        if ($rpt_item_travel_date_arr[$ids] < $travel_date) {
                                            $travel_date = $rpt_item_travel_date_arr[$ids];
                                        };

                                    } // END if($rpt_item_producttype_id_arr[$ids] == 2){

                                    if ($rpt_item_producttype_id_arr[$ids] == 3) {
                                        if ($rpt_item_travel_date_arr[$ids] < $travel_date) {
                                            $travel_date = $rpt_item_travel_date_arr[$ids];
                                        };

                                    } // END if($rpt_item_producttype_id_arr[$ids] == 3){

                                    if ($rpt_item_producttype_id_arr[$ids] == 4) {
                                        if ($rpt_item_travel_date_arr[$ids] < $travel_date) {
                                            $travel_date = $rpt_item_travel_date_arr[$ids];
                                        };

                                    } // END if($rpt_item_producttype_id_arr[$ids] == 4){

                                    if ($rpt_item_producttype_id_arr[$ids] == 5) {
                                        if ($rpt_item_travel_date_arr[$ids] < $travel_date) {
                                            $travel_date = $rpt_item_travel_date_arr[$ids];
                                        };

                                    } // END if($rpt_item_producttype_id_arr[$ids] == 5){

                                    if ($rpt_item_producttype_id_arr[$ids] == 6) {
                                        if ($rpt_item_travel_date_arr[$ids] < $travel_date) {
                                            $travel_date = $rpt_item_travel_date_arr[$ids];
                                        };

                                    } // END if($rpt_item_producttype_id_arr[$ids] == 6){

                                    if ($rpt_item_producttype_id_arr[$ids] == 7) {
                                        if ($rpt_item_travel_date_arr[$ids] < $travel_date) {
                                            $travel_date = $rpt_item_travel_date_arr[$ids];
                                        };

                                    } // END if($rpt_item_producttype_id_arr[$ids] == 7){

                                    if ($rpt_item_producttype_id_arr[$ids] == 8) {
                                        if ($rpt_item_travel_date_arr[$ids] < $travel_date) {
                                            $travel_date = $rpt_item_travel_date_arr[$ids];
                                        };

                                    } // END if($rpt_item_producttype_id_arr[$ids] == 8){

                                } // END if($value){
                            } // END foreach($rpt_item_travel_date_arr as $ids => $value){

                            // END Select Travel Date

                            if ($color == $color_2) $color = $color_3; else $color = $color_2;
                            ?>
                            <tr bgcolor="<?= $color ?>">
                                <td align="center"><input type="checkbox" name="del_ids[]" value="<?= $row[0] ?>"/></td>
                                <td>
                                    <a href="./?mode=reservation_packages/view&id=<?= $row[0] ?>"><?= $row['rpa_id_str'] ?></a>
                                </td>
                                <td align="center"><?= DateFormat($row['rpa_date'], "s") ?></td>
                                <td><?= get_value('agents', 'ag_id', 'ag_name', $row['agents_id']) ?></td>
                                <!--<td><?= $row['rpa_email'] ?></td>-->
                                <td><?= $row['rpa_fname'] ?></td>
                                <td><?= $row['rpa_lname'] ?></td>
                                <td><?= $rows_rpt['rpt_name'] ?></td>
                                <td align="center">
                                    <?php if ($row['company'] == 1) { ?>
                                        <?= get_value('packages', 'pac_id', 'ac_andaman_adult', $rows_rpt['packages_id']) ?>
                                    <?php } elseif ($row['company'] == 2) { ?>
                                        <?= get_value('packages', 'pac_id', 'ac_dotcom_adult', $rows_rpt['packages_id']) ?>
                                    <?php } elseif ($row['company'] == 3) { ?>
                                        <?= get_value('packages', 'pac_id', 'ac_btoc_adult', $rows_rpt['packages_id']) ?>
                                    <?php } else { ?>

                                    <?php } ?>
                                </td>
                                <td align="center">
                                    <?php if ($row['company'] == 1) { ?>
                                        <?= get_value('packages', 'pac_id', 'ac_andaman_child', $rows_rpt['packages_id']) ?>
                                    <?php } elseif ($row['company'] == 2) { ?>
                                        <?= get_value('packages', 'pac_id', 'ac_dotcom_child', $rows_rpt['packages_id']) ?>
                                    <?php } elseif ($row['company'] == 3) { ?>
                                        <?= get_value('packages', 'pac_id', 'ac_btoc_child', $rows_rpt['packages_id']) ?>
                                    <?php } else { ?>

                                    <?php } ?>
                                </td>
                                <td align="center"><?= DateFormat($travel_date, "s") ?></td>
                                <td><?= get_value('lis_booking_status', 'lis_id', 'lis_name', $row['bookingstatus_id']) ?></td>
                                <td><?= get_value('lis_payment_status', 'lis_id', 'lis_name', $row['paymentstatus_id']) ?></td>
                                <td align="right"><?= number_format($rows_rpt['rpt_prices'], 0) ?></td>
                                <td align="center"><a href="./?mode=reservation_packages/view&id=<?= $row[0] ?>"><img
                                            src="images/icon_menu/edit.png" border="0" width="20" height="20"
                                            align="absmiddle"/></a></td>
                            </tr>
                            <?php
                        }// END while($rows_rpt = mysql_fetch_array($results_rpt)){		
                        ?>


                        <?php
                    } // END for ($start;$start<$result+1;$start++)
                    ?>
                    <tr bgcolor="<?= $color_1 ?>">
                        <td colspan="20"><input type="submit" name="submit_array" value="SAVE SETTING"
                                                style="width:125px;"
                                                onClick="return confirm('Do you want to do this ?')"></td>
                    </tr>
                </form>
                <?php
                }
                ?>
            </table>
            <!---- Listing Body ---->
        </td>
    </tr>
</table>