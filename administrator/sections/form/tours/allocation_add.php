<?php
include("value_get.php");
if (isset($id)) {
    $sql = "select * from tours where tou_id = $id";
    $result = mysql_query($sql);
    $row = mysql_fetch_array($result);
    include("inc.top.php");
    ?>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="txt_big_gray">Create New Allocation
            </td>
        </tr>
        <tr>
            <td class="txt_big_gray">&nbsp;
            </td>
        </tr>

        <script type="text/javascript">
            function checkSubmitForm() {
                var touallo_global = document.form.touallo_global.value;

                if (touallo_global == '') {
                    alert('Field : Number Of People To Allocate is request.');
                    return false;
                }
                return true;
            }

        </script>

        <tr>
            <td class="txt_big_gray">
                <form action="process.php?mode=tours/allocation_save&id=<?= $id ?>" onsubmit="return checkSubmitForm();"
                      name="form" method="post" enctype="multipart/form-data">
                    <table width="100%" border="0" cellspacing="0" cellpadding="3">

                        <tr>
                            <td align="right" class="txt_bold_gray" width="200">Date From :</td>
                            <td>
                                <script>DateInput('touallo_datefrom', true, 'yyyy-mm-dd')</script>
                            </td>
                        </tr>

                        <tr>
                            <td align="right" class="txt_bold_gray" width="200">Date To :</td>
                            <td>
                                <script>DateInput('touallo_dateto', true, 'yyyy-mm-dd')</script>
                            </td>
                        </tr>

                        <tr>
                            <td align="right" class="txt_bold_gray" width="200">Number Of People To Allocate :</td>
                            <td><input type="text" name="touallo_global" value="" style="width:30px; text-align:center;"
                                       maxlength="4"/><font color="#FF0000"> *</font></td>
                        </tr>


                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td><input type="submit" name="submit" value="SUBMIT" style="width:75px;"> <input
                                    type="reset" name="Reset" value="RESET" style="width:75;"></td>
                        </tr>
                    </table>
                </form>

            </td>
        </tr>

    </table>


    <?php
}
?>
