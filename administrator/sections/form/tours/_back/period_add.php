<?php
if ($id) {
    $sql = "select * from tours where tou_id = $id";
    $result = mysql_query($sql);
    $row = mysql_fetch_array($result);
    include("inc.top.php");
    $page_title = "Period Setting";
    ?>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="txt_big_gray"><?= $page_title ?></td>
        </tr>
    </table>
    <form action="process.php?mode=tours/period_save&id=<?= $id ?>" name="form" method="post"
          enctype="multipart/form-data">
        <table border="0" cellspacing="0" cellpadding="3">
            <tr>
                <td valign="top">
                    <table width="100%" border="0" cellspacing="1" cellpadding="3" bgcolor="<?= $color_1 ?>">
                        <tr bgcolor="<?= $color_2 ?>">
                            <td class="txt_bold_gray" align="center">Period No.</td>
                            <td class="txt_bold_gray" align="center">Date From</td>
                            <td class="txt_bold_gray" align="center">Date To</td>
                            <td class="txt_bold_gray" align="center">Auto Date</td>
                        </tr>
                        <?php
                        $auto_date_from[0] = date("Y") . "-01-16";
                        $auto_date_to[0] = date("Y") . "-04-30";
                        $auto_date_from[1] = date("Y") . "-05-01";
                        $auto_date_to[1] = date("Y") . "-10-31";
                        $auto_date_from[2] = date("Y") . "-11-01";
                        $auto_date_to[2] = date("Y") . "-12-19";
                        $auto_date_from[3] = date("Y") . "-12-20";
                        $auto_date_to[3] = (date("Y") + 1) . "-01-15";
                        $i = 0;
                        while ($i < 7) {
                            $row = "";
                            $sql = "select * from tour_period where tours_id = $id and arrange = $i";
                            $result = mysql_query($sql);
                            $row = mysql_fetch_array($result);
                            ?>
                            <tr bgcolor="<?= $color_3 ?>">
                                <td align="center"><?= ($i + 1) ?>
                                    <input type="hidden" name="toupe_id_<?= $i ?>" value="<?= $row[0] ?>"/>
                                    <input type="hidden" name="arrange_<?= $i ?>" value="<?= $i ?>"/>
                                </td>
                                <td><input class="plain" name="date_from_<?= $i ?>" id="date_from_<?= $i ?>"
                                           value="<?php $show = $row[toupe_datefrom];
                                           if ($show != "0000-00-00") echo $show; ?>" size="12" onFocus="this.blur()"
                                           readonly>&nbsp;<a href="javascript:void(0)"
                                                             onClick="if(self.gfPop)gfPop.fStartPop(document.form.date_from_<?= $i ?>,document.form.date_to_<?= $i ?>);return false;"><img
                                            src="javascript/DateRange/calbtn.gif" border="0" width="16" height="15"
                                            align="absmiddle" class="PopcalTrigger"></a></td>
                                <td><input class="plain" name="date_to_<?= $i ?>" id="date_to_<?= $i ?>"
                                           value="<?php $show = $row[toupe_dateto];
                                           if ($show != "0000-00-00") echo $show; ?>" size="12" onFocus="this.blur()"
                                           readonly>&nbsp;<a href="javascript:void(0)"
                                                             onClick="if(self.gfPop)gfPop.fEndPop(document.form.date_from_<?= $i ?>,document.form.date_to_<?= $i ?>);return false;"><img
                                            src="javascript/DateRange/calbtn.gif" border="0" width="16" height="15"
                                            align="absmiddle" class="PopcalTrigger"></a></td>
                                <td>
                                    <input type="button" value="Clear This Period"
                                           onclick="document.form.date_from_<?= $i ?>.value='';document.form.date_to_<?= $i ?>.value='';"/>
                                    <?php if ($auto_date_from[$i] && $auto_date_to[$i]) { ?>
                                        <input type="button" value="Auto This Period"
                                               onclick="document.form.date_from_<?= $i ?>.value='<?= $auto_date_from[$i] ?>';document.form.date_to_<?= $i ?>.value='<?= $auto_date_to[$i] ?>';"/>
                                        Auto This Period is <?= "<u class=\"remark\">" . Dateformat($auto_date_from[$i], s) . "</u> to <u class=\"remark\">" . Dateformat($auto_date_to[$i], s) . "</u>" ?>
                                    <?php } ?>
                                </td>
                            </tr>
                            <?php
                            $i++;
                        }
                        ?>
                    </table>
                </td>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td align="center"><input type="submit" name="submit" value="SUBMIT" style="width:75px;">&nbsp;<input
                        type="reset" name="Reset" value="RESET" style="width:75;"></td>
            </tr>
        </table>
    </form>
    <iframe width=132 height=142 name="gToday:contrast:agenda.js" id="gToday:contrast:agenda.js"
            src="javascript/DateRange/ipopeng.htm" scrolling="no" frameborder="0"
            style="visibility:visible; z-index:999; position:absolute; top:-500px; left:-500px;"></iframe>
    <?php
}
?>
