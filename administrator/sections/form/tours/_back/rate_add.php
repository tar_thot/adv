<?php
if ($id) {
    $sql = "select * from hotel where hotel_id = $id";
    $result = mysql_query($sql);
    $row = mysql_fetch_array($result);
    include("inc.top.php");
    $page_title = "Period Setting";
    ?>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="txt_big_gray"><?= $page_title ?></td>
        </tr>
    </table>
    <form action="process.php?mode=hotel/rate_save&id=<?= $id ?>" name="form" method="post"
          enctype="multipart/form-data">
        <table border="0" cellspacing="0" cellpadding="3">
            <tr>
                <td valign="top">
                    <table width="100%" border="0" cellspacing="1" cellpadding="3" bgcolor="<?= $color_1 ?>">
                        <tr bgcolor="<?= $color_2 ?>">
                            <td class="txt_bold_gray" align="center">Room Type</td>
                            <td class="txt_bold_gray" align="center" width="100">Single</td>
                            <td class="txt_bold_gray" align="center" width="100">TWN/DBL</td>
                            <td class="txt_bold_gray" align="center" width="100">Triple</td>
                            <td class="txt_bold_gray" align="center" width="100">Extra Bed</td>
                            <td class="txt_bold_gray" align="center" width="100">Include ABF</td>
                        </tr>
                        <?php
                        $i = 0;
                        $hotelroom = "select * from hotelroom where hotel_id = $id order by arrange";
                        $hotelroomhotelroom = mysql_query($hotelroom);
                        while ($hotelroom = mysql_fetch_array($hotelroomhotelroom)) {
                            ?>
                            <tr bgcolor="<?= $color_3 ?>">
                                <td class="txt_bold_gray" colspan="20"><?= $hotelroom[name] ?></td>
                            </tr>
                            <?php
                            $hotelperiod_status = "";
                            $hotelperiod = "select * from hotelperiod where hotel_id = $id and date_from != '0000-00-00' and date_to != '0000-00-00' order by arrange";
                            $hotelperiodhotelperiod = mysql_query($hotelperiod);
                            while ($hotelperiod = mysql_fetch_array($hotelperiodhotelperiod)) {
                                $hotelperiod_status = "Yes";
                                $row = "";
                                $sql = "select * from hotelrate where hotel_id = $id and hotelroom_id = $hotelroom[0] and hotelperiod_id = $hotelperiod[0]";
                                $result = mysql_query($sql);
                                $row = mysql_fetch_array($result);
                                ?>
                                <tr bgcolor="<?= $color_3 ?>">
                                    <td><?= DateFormat($hotelperiod[date_from], s) . " to " . DateFormat($hotelperiod[date_to], s) ?>
                                        <input type="hidden" name="hotelrate_id_<?= $i ?>" value="<?= $row[0] ?>"/>
                                        <input type="hidden" name="hotelroom_id_<?= $i ?>"
                                               value="<?= $hotelroom[0] ?>"/>
                                        <input type="hidden" name="hotelperiod_id_<?= $i ?>"
                                               value="<?= $hotelperiod[0] ?>"/>
                                    </td>
                                    <td align="center"><input type="text" name="rate_1_<?= $i ?>"
                                                              value="<?php if ($show = $row[rate_1]) echo number_format($show); else echo ""; ?>"
                                                              size="10"></td>
                                    <td align="center"><input type="text" name="rate_2_<?= $i ?>"
                                                              value="<?php if ($show = $row[rate_2]) echo number_format($show); else echo ""; ?>"
                                                              size="10"></td>
                                    <td align="center"><input type="text" name="rate_3_<?= $i ?>"
                                                              value="<?php if ($show = $row[rate_3]) echo number_format($show); else echo ""; ?>"
                                                              size="10"></td>
                                    <?php if ($hotelroom[extbed] == "Yes") { ?>
                                        <td align="center"><input type="text" name="rate_extbed_<?= $i ?>"
                                                                  value="<?php if ($show = $row[rate_extbed]) echo number_format($show); else echo ""; ?>"
                                                                  size="10"></td>
                                    <?php } else { ?>
                                        <td align="center"> -</td>
                                    <?php } ?>
                                    <td align="center"><?php if ($show = $hotelroom[incl_abf]) echo $show; else echo "-"; ?></td>
                                </tr>
                                <?php
                                $i++;
                            }
                            if ($hotelperiod_status == "") {
                                ?>
                                <tr bgcolor="<?= $color_3 ?>">
                                    <td align="center" colspan="20"><span class="remark">Not available, Please correct period first.</span>
                                    </td>
                                </tr>
                                <?php
                            }
                        }
                        ?>
                        <input type="hidden" name="count_i" value="<?= $i ?>"/>
                    </table>
                </td>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td align="center"><input type="submit" name="submit" value="SUBMIT" style="width:75px;">&nbsp;<input
                        type="reset" name="Reset" value="RESET" style="width:75;"></td>
            </tr>
        </table>
    </form>
    <iframe width=132 height=142 name="gToday:contrast:agenda.js" id="gToday:contrast:agenda.js"
            src="javascript/DateRange/ipopeng.htm" scrolling="no" frameborder="0"
            style="visibility:visible; z-index:999; position:absolute; top:-500px; left:-500px;"></iframe>
    <?php
}
?>
