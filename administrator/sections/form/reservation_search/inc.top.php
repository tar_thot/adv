<?php

function setNumberLength($num, $length)
{
    $sumstr = strlen($num);
    $zero = str_repeat("0", $length - $sumstr);
    $results = $zero . $num;

    return $results;
}

?>

<table width="100%" border="0" cellspacing="1" cellpadding="3" bgcolor="#ffffff">

    <tr bgcolor="#eeeeee">
        <td align="right" class="txt_bold_gray">Raservetion Package ID :</td>
        <td><?= C ?><?= setNumberLength($row[0], 7) ?>       </td>
        <td align="right" class="txt_bold_gray">Agent :</td>
        <td><?= get_value('agents', 'ag_id', 'ag_name', $row['agents_id']) ?>        </td>
    </tr>

    <tr bgcolor="#eeeeee">
        <td align="right" class="txt_bold_gray">Name :</td>
        <td>
            <?= get_value('lis_titlename', 'lis_id', 'lis_name', $row['titlename_id']) ?>
            <?= $row['rpa_fname'] ?>
            <?= $row['rpa_lname'] ?>       </td>
        <td align="right" class="txt_bold_gray">Email :</td>
        <td><?= $row['rpa_email'] ?>        </td>
    </tr>


    <tr bgcolor="#eeeeee">
        <td align="right" class="txt_bold_gray">Phone Number :</td>
        <td><?= $row['rpa_phone'] ?>        </td>
        <td align="right" class="txt_bold_gray">Fax Number :</td>
        <td><?= $row['rpa_fax'] ?>        </td>
    </tr>


    <tr bgcolor="#eeeeee">
        <td align="right" class="txt_bold_gray">Address :</td>
        <td><?= nl2br($row['rpa_address']) ?>        </td>
        <td align="right" class="txt_bold_gray">Additional request :</td>
        <td><?= nl2br($row['rpa_request']) ?>        </td>
    </tr>


    <tr bgcolor="#eeeeee">
        <td align="right" class="txt_bold_gray" width="317"> Book Date :</td>
        <td><?= DateFormat($row['rpa_date'], "s") ?></td>
        <td align="right" class="txt_bold_gray">Booking Status :</td>
        <td><?= get_value('lis_booking_status', 'lis_id', 'lis_name', $row['bookingstatus_id']) ?>        </td>
    </tr>


    <tr bgcolor="#eeeeee">
        <td align="right" class="txt_bold_gray">Note :</td>
        <td><?= nl2br($row['rpa_note']) ?>        </td>
        <td align="right" class="txt_bold_gray">Payment Status :</td>
        <td><?= get_value('lis_payment_status', 'lis_id', 'lis_name', $row['paymentstatus_id']) ?>        </td>
    </tr>


</table>

<table width="100%" border="0" cellspacing="1" cellpadding="3" bgcolor="<?= $color_inctop1 ?>">
    <tr bgcolor="<?= $color_inctop2 ?>">
        <td><span class="txt_bold_white">
        <a href="./?mode=reservation_packages/view&id=<?= $id ?>" class="txt_bold_white">View</a><strong
                    style="color:<?= $color_inctop1 ?>;"> | </strong>
        <a href="./?mode=reservation_packages/add&id=<?= $id ?>" class="txt_bold_white">Edit</a><strong
                    style="color:<?= $color_inctop1 ?>;"> | </strong>

        </span></td>
    </tr>
</table>
<br/>
