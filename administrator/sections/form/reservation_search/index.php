<?php

function setNumberLength($num, $length)
{
    $sumstr = strlen($num);
    $zero = str_repeat("0", $length - $sumstr);
    $results = $zero . $num;

    return $results;
}

?>

<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td valign="top">
            <table width="100%" border="0" cellspacing="0" cellpadding="3">
                <tr>
                    <td class="txt_big_gray">Search Reservation List
                        <!--[ <a href="./?mode=reservation_packages/add">Create New Reservation </a> ]--></td>
                </tr>
            </table>
            <!---- Listing Body ---->
            <table width="100%" border="0" cellspacing="0" cellpadding="3">
                <tr>
                    <td>
                        <!---- Search Box ---->

                        <table border="0" cellspacing="3" cellpadding="3" style="border: solid #CCCCCC 1px;">
                            <form name="formSearch" method="post" action="./index.php?mode=reservation_search/view">
                                <tr>
                                    <td class="txt_bold_gray" align="right">
                                        Reservation ID<br/>
                                        <input type="text" name="search_id" id="search_id" value="<?= $search_id ?>"
                                               style="width:150px;"></td>
                                    <td class="txt_bold_gray" align="right">
                                        Service Name<br/>
                                        <input type="text" name="search_name" id="search_name"
                                               value="<?= $search_name ?>" style="width:150px;"></td>
                                    <td class="txt_bold_gray" align="right">
                                        Agent<br/>
                                        <select name="agents_id" style="width:155px;">
                                            <option value="">-- Select --</option>
                                            <?php listbox('agents', ' ag_id', ' ag_name', $agents_id, 'N'); ?>
                                        </select></td>
                                </tr>

                                <tr>
                                    <td class="txt_bold_gray" align="right">
                                        First Name<br/>
                                        <input type="text" name="search_fname" id="search_fname"
                                               value="<?= $search_fname ?>" style="width:150px;"></td>
                                    <td class="txt_bold_gray" align="right">
                                        Last Name<br/>
                                        <input type="text" name="search_lname" id="search_lname"
                                               value="<?= $search_lname ?>" style="width:150px;"></td>
                                    <td class="txt_bold_gray" align="right">
                                        Booking Status<br/>
                                        <select name="bookingstatus_id" style="width:155px;">
                                            <option value="">-- Select --</option>
                                            <?php listbox('lis_booking_status', ' lis_id', ' lis_name', $bookingstatus_id, 'N'); ?>
                                        </select></td>
                                </tr>
                                <tr>
                                    <td class="txt_bold_gray" align="right">
                                        Payment Status<br/>
                                        <select name="paymentstatus_id" style="width:155px;">
                                            <option value="">-- Select --</option>
                                            <?php listbox('lis_payment_status', ' lis_id', ' lis_name', $paymentstatus_id, 'N'); ?>
                                        </select></td>
                                    <td class="txt_bold_gray" align="right">
                                        From Service Date<br/>
                                        <script>DateInput('form_service_date', true, 'yyyy-mm-dd' <?php if (isset($form_service_date)) {
                                                echo ",'$form_service_date'";
                                            }?>)</script>
                                    </td>
                                    <td class="txt_bold_gray" align="right">
                                        To Service Date<br/>
                                        <script>DateInput('to_service_date', true, 'yyyy-mm-dd' <?php if (isset($to_service_date)) {
                                                echo ",'$to_service_date'";
                                            }?>)</script>
                                    </td>
                                    <td>&nbsp;</td>
                                    <td valign="bottom">
                                        <input type="submit" name="Submit" value="SEARCH" style="height:25px;"></td>
                                </tr>
                            </form>
                        </table>
                        <!---- Search Box ---->
                    </td>
                    <td align="right" valign="bottom"></td>
                </tr>
            </table>

            <!---- Listing Body ---->
        </td>
    </tr>
</table>