<?php

function setNumberLength($num, $length)
{
    $sumstr = strlen($num);
    $zero = str_repeat("0", $length - $sumstr);
    $results = $zero . $num;

    return $results;
}

function DateDiff($strDate1, $strDate2)
{
    return (strtotime($strDate2) - strtotime($strDate1)) / (60 * 60 * 24);  // 1 day = 60*60*24
}

##### POST #####

$search_id = $_POST['search_id'];
$search_name = $_POST['search_name'];
$search_fname = $_POST['search_fname'];
$search_lname = $_POST['search_lname'];
$agents_id = $_POST['agents_id'];
$bookingstatus_id = $_POST['bookingstatus_id'];
$paymentstatus_id = $_POST['paymentstatus_id'];
$form_service_date = $_POST['form_service_date'];
$to_service_date = $_POST['to_service_date'];

##### end POST #####

$numres = 0;
$numres_combo = 0;

?>

<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td valign="top">

            <table width="100%" border="0" cellspacing="0" cellpadding="3">
                <tr>
                    <td class="txt_big_gray">Search Reservation List</td>
                    <td align="right"><a href="./index.php?mode=reservation_search/index"
                                         style="background-color:#ffffff; color:#000000"><<< Back</a></td>
                </tr>
                <tr>
                    <td colspan="2">
                        <hr>
                    </td>
                </tr>
                <tr>
                    <td style="font-size:15px;"><b>Reservation List</b></td>
                </tr>
            </table>

            <table width="100%" border="0" cellspacing="1" cellpadding="3" bgcolor="#FFFFFF">
                <tr bgcolor="<?= $color_1 ?>">
                    <td width="100" class="txt_bold_white">ID</td>
                    <td width="100" class="txt_bold_white">&nbsp;Book&nbsp;Date&nbsp;</td>
                    <td width="100" class="txt_bold_white">Agent&nbsp;Name</td>
                    <!--<td width="100" class="txt_bold_white" >Email</td>-->
                    <td width="100" class="txt_bold_white">First&nbsp;Name</td>
                    <td width="100" class="txt_bold_white">Last&nbsp;Name</td>
                    <td width="500" class="txt_bold_white">&nbsp;Service&nbsp;Name&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td width="100" class="txt_bold_white" align="center">Account Code (Adult)</td>
                    <td width="100" class="txt_bold_white" align="center">Account Code (Child)</td>
                    <td width="100" class="txt_bold_white">Service&nbsp;Date</td>
                    <td width="100" class="txt_bold_white">Booking Status</td>
                    <td width="100" class="txt_bold_white">Payment</td>
                    <td width="100" class="txt_bold_white">Total Amount</td>

                    <td class="txt_bold_white" width="43">&nbsp;</td>
                </tr>

                <?php
                $sql = "select * from reservations ";
                $sql .= "where res_id > 0 ";

                if ($search_id) {
                    $sql .= "AND res_id_str like '%" . $search_id . "%' ";
                }
                if ($search_fname) {
                    $sql .= "AND res_fname like '%" . $search_fname . "%' ";
                }
                if ($search_lname) {
                    $sql .= "AND res_lname like '%" . $search_lname . "%' ";
                }
                if ($agents_id) {
                    $sql .= "AND agents_id = '" . $agents_id . "' ";
                }
                if ($bookingstatus_id) {
                    $sql .= "AND bookingstatus_id = '" . $bookingstatus_id . "' ";
                }
                if ($paymentstatus_id) {
                    $sql .= "AND paymentstatus_id = '" . $paymentstatus_id . "' ";
                }

                $sql .= " order by res_id DESC";

                $result = mysql_query($sql);
                while ($row = mysql_fetch_array($result)) {
                    ?>

                    <?php
                    ### Loop For Boat Transfer
                    $sql_rbt = "SELECT * ";
                    $sql_rbt .= "FROM reservation_boattransfer_items ";
                    $sql_rbt .= "where reservations_id = $row[0] ";
                    if ($search_name) {
                        $sql_rbt .= "AND rbt_name like '%" . $search_name . "%' ";
                    }

                    if ($form_service_date) {
                        $sql_rbt .= "AND rbt_travel_date BETWEEN '" . $form_service_date . "' AND '" . $to_service_date . "' ";
                    }

                    $sql_rbt .= "order by rbt_id ASC ";

                    $results_rbt = mysql_query($sql_rbt);
                    while ($rows_rbt = mysql_fetch_array($results_rbt)) {

                        if ($color == $color_2) $color = $color_3; else $color = $color_2;
                        ?>

                        <tr bgcolor="<?= $color ?>">
                            <td><a href="./?mode=reservations/view&id=<?= $row[0] ?>"><?= $row['res_id_str'] ?></a></td>
                            <td><?= DateFormat($row['res_date'], "s") ?></td>
                            <td><?= get_value('agents', 'ag_id', 'ag_name', $row['agents_id']) ?></td>
                            <!--<td><?= $row['res_email'] ?></td>-->
                            <td><?= $row['res_fname'] ?></td>
                            <td><?= $row['res_lname'] ?></td>
                            <td><?= $rows_rbt['rbt_name'] ?></td>
                            <td align="center">
                                <?php if ($row['company'] == 1) { ?>
                                    <?= get_value('boattransfers', 'bot_id', 'ac_andaman_adult', $rows_rbt['boattransfers_id']) ?>
                                <?php } elseif ($row['company'] == 2) { ?>
                                    <?= get_value('boattransfers', 'bot_id', 'ac_dotcom_adult', $rows_rbt['boattransfers_id']) ?>
                                <?php } elseif ($row['company'] == 3) { ?>
                                    <?= get_value('boattransfers', 'bot_id', 'ac_btoc_adult', $rows_rbt['boattransfers_id']) ?>
                                <?php } else { ?>

                                <?php } ?>
                            </td>
                            <td align="center">
                                <?php if ($row['company'] == 1) { ?>
                                    <?= get_value('boattransfers', 'bot_id', 'ac_andaman_child', $rows_rbt['boattransfers_id']) ?>
                                <?php } elseif ($row['company'] == 2) { ?>
                                    <?= get_value('boattransfers', 'bot_id', 'ac_dotcom_child', $rows_rbt['boattransfers_id']) ?>
                                <?php } elseif ($row['company'] == 3) { ?>
                                    <?= get_value('boattransfers', 'bot_id', 'ac_btoc_child', $rows_rbt['boattransfers_id']) ?>
                                <?php } else { ?>

                                <?php } ?>
                            </td>
                            <td><?= DateFormat($rows_rbt['rbt_travel_date'], "s") ?></td>
                            <td><?= get_value('lis_booking_status', 'lis_id', 'lis_name', $row['bookingstatus_id']) ?></td>
                            <td><?= get_value('lis_payment_status', 'lis_id', 'lis_name', $row['paymentstatus_id']) ?></td>
                            <td align="right"><?= number_format($rows_rbt['rbt_prices'], 0) ?></td>
                            <td align="center"><a href="./?mode=reservations/view&id=<?= $row[0] ?>">
                                    <img src="images/icon_menu/edit.png" border="0" width="20" height="20"
                                         align="absmiddle"/></a></td>
                        </tr>

                        <?php
                        $numres = $numres + 1;

                    } # end while($rows_rbt = mysql_fetch_array($results_rbt)){
                    ?>

                    <?php
                    ### Loop For Pick Up Transfer
                    $sql_rpt = "SELECT * ";
                    $sql_rpt .= "FROM reservation_pickuptransfer_items ";
                    $sql_rpt .= "where reservations_id = $row[0] ";
                    if ($search_name) {
                        $sql_rpt .= "AND rpt_name like '%" . $search_name . "%' ";
                    }

                    if ($form_service_date) {
                        $sql_rpt .= "AND rpt_travel_date BETWEEN '" . $form_service_date . "' AND '" . $to_service_date . "' ";
                    }

                    $sql_rpt .= "order by rpt_id ASC ";

                    $results_rpt = mysql_query($sql_rpt);
                    while ($rows_rpt = mysql_fetch_array($results_rpt)) {

                        if ($color == $color_2) $color = $color_3; else $color = $color_2;
                        ?>

                        <tr bgcolor="<?= $color ?>">
                            <td><a href="./?mode=reservations/view&id=<?= $row[0] ?>"><?= $row['res_id_str'] ?></a></td>
                            <td><?= DateFormat($row['res_date'], "s") ?></td>
                            <td><?= get_value('agents', 'ag_id', 'ag_name', $row['agents_id']) ?></td>
                            <!--<td><?= $row['res_email'] ?></td>-->
                            <td><?= $row['res_fname'] ?></td>
                            <td><?= $row['res_lname'] ?></td>
                            <td><?= $rows_rpt['rpt_name'] ?></td>
                            <td align="center">
                                <?php if ($row['company'] == 1) { ?>
                                    <?= get_value('pickuptransfers', 'put_id', 'ac_andaman_adult', $rows_rpt['pickuptransfers_id']) ?>
                                <?php } elseif ($row['company'] == 2) { ?>
                                    <?= get_value('pickuptransfers', 'put_id', 'ac_dotcom_adult', $rows_rpt['pickuptransfers_id']) ?>
                                <?php } elseif ($row['company'] == 3) { ?>
                                    <?= get_value('pickuptransfers', 'put_id', 'ac_btoc_adult', $rows_rpt['pickuptransfers_id']) ?>
                                <?php } else { ?>

                                <?php } ?>
                            </td>
                            <td align="center">
                                <?php if ($row['company'] == 1) { ?>
                                    <?= get_value('pickuptransfers', 'put_id', 'ac_andaman_child', $rows_rpt['pickuptransfers_id']) ?>
                                <?php } elseif ($row['company'] == 2) { ?>
                                    <?= get_value('pickuptransfers', 'put_id', 'ac_dotcom_child', $rows_rpt['pickuptransfers_id']) ?>
                                <?php } elseif ($row['company'] == 3) { ?>
                                    <?= get_value('pickuptransfers', 'put_id', 'ac_btoc_child', $rows_rpt['pickuptransfers_id']) ?>
                                <?php } else { ?>

                                <?php } ?>
                            </td>
                            <td><?= DateFormat($rows_rpt['rpt_travel_date'], "s") ?></td>
                            <td><?= get_value('lis_booking_status', 'lis_id', 'lis_name', $row['bookingstatus_id']) ?></td>
                            <td><?= get_value('lis_payment_status', 'lis_id', 'lis_name', $row['paymentstatus_id']) ?></td>
                            <td align="right"><?= number_format($rows_rpt['rpt_prices'], 0) ?></td>
                            <td align="center"><a href="./?mode=reservations/view&id=<?= $row[0] ?>">
                                    <img src="images/icon_menu/edit.png" border="0" width="20" height="20"
                                         align="absmiddle"/></a></td>
                        </tr>

                        <?php
                        $numres = $numres + 1;

                    } # end while($rows_rpt = mysql_fetch_array($results_rpt)){
                    ?>

                    <?php
                    ### Loop For Tour
                    $sql_rtt = "SELECT * ";
                    $sql_rtt .= "FROM reservation_tour_items ";
                    $sql_rtt .= "where reservations_id = $row[0] ";
                    if ($search_name) {
                        $sql_rtt .= "AND rtt_name like '%" . $search_name . "%' ";
                    }

                    if ($form_service_date) {
                        $sql_rtt .= "AND rtt_travel_date BETWEEN '" . $form_service_date . "' AND '" . $to_service_date . "' ";
                    }

                    $sql_rtt .= "order by rtt_id ASC ";

                    $results_rtt = mysql_query($sql_rtt);
                    while ($rows_rtt = mysql_fetch_array($results_rtt)) {

                        if ($color == $color_2) $color = $color_3; else $color = $color_2;
                        ?>
                        <tr bgcolor="<?= $color ?>">
                            <td><a href="./?mode=reservations/view&id=<?= $row[0] ?>"><?= $row['res_id_str'] ?></a></td>
                            <td><?= DateFormat($row['res_date'], "s") ?></td>
                            <td><?= get_value('agents', 'ag_id', 'ag_name', $row['agents_id']) ?></td>
                            <!--<td><?= $row['res_email'] ?></td>-->
                            <td><?= $row['res_fname'] ?></td>
                            <td><?= $row['res_lname'] ?></td>
                            <td><?= $rows_rtt['rtt_name'] ?></td>
                            <td align="center">
                                <?php if ($row['company'] == 1) { ?>
                                    <?= get_value('tours', 'tou_id', 'ac_andaman_adult', $rows_rtt['tours_id']) ?>
                                <?php } elseif ($row['company'] == 2) { ?>
                                    <?= get_value('tours', 'tou_id', 'ac_dotcom_adult', $rows_rtt['tours_id']) ?>
                                <?php } elseif ($row['company'] == 3) { ?>
                                    <?= get_value('tours', 'tou_id', 'ac_btoc_adult', $rows_rtt['tours_id']) ?>
                                <?php } else { ?>

                                <?php } ?>
                            </td>
                            <td align="center">
                                <?php if ($row['company'] == 1) { ?>
                                    <?= get_value('tours', 'tou_id', 'ac_andaman_child', $rows_rtt['tours_id']) ?>
                                <?php } elseif ($row['company'] == 2) { ?>
                                    <?= get_value('tours', 'tou_id', 'ac_dotcom_child', $rows_rtt['tours_id']) ?>
                                <?php } elseif ($row['company'] == 3) { ?>
                                    <?= get_value('tours', 'tou_id', 'ac_btoc_child', $rows_rtt['tours_id']) ?>
                                <?php } else { ?>

                                <?php } ?>
                            </td>
                            <td><?= DateFormat($rows_rtt['rtt_travel_date'], "s") ?></td>
                            <td><?= get_value('lis_booking_status', 'lis_id', 'lis_name', $row['bookingstatus_id']) ?></td>
                            <td><?= get_value('lis_payment_status', 'lis_id', 'lis_name', $row['paymentstatus_id']) ?></td>
                            <td align="right"><?= number_format($rows_rtt['rtt_prices'], 0) ?></td>
                            <td align="center"><a href="./?mode=reservations/view&id=<?= $row[0] ?>"><img
                                        src="images/icon_menu/edit.png" border="0" width="20" height="20"
                                        align="absmiddle"/></a></td>
                        </tr>

                        <?php
                        $numres = $numres + 1;

                    }// end while($rows_rtt = mysql_fetch_array($results_rtt)){
                    ?>

                    <?php
                    ### Loop For Activity
                    $sql_rat = "SELECT * ";
                    $sql_rat .= "FROM reservation_activity_items ";
                    $sql_rat .= "where reservations_id = $row[0] ";
                    if ($search_name) {
                        $sql_rat .= "AND rat_name like '%" . $search_name . "%' ";
                    }

                    if ($form_service_date) {
                        $sql_rat .= "AND rat_travel_date BETWEEN '" . $form_service_date . "' AND '" . $to_service_date . "' ";
                    }

                    $sql_rat .= "order by rat_id ASC ";

                    $results_rat = mysql_query($sql_rat);
                    while ($rows_rat = mysql_fetch_array($results_rat)) {

                        if ($color == $color_2) $color = $color_3; else $color = $color_2;
                        ?>
                        <tr bgcolor="<?= $color ?>">
                            <td><a href="./?mode=reservations/view&id=<?= $row[0] ?>"><?= $row['res_id_str'] ?></a></td>
                            <td><?= DateFormat($row['res_date'], "s") ?></td>
                            <td><?= get_value('agents', 'ag_id', 'ag_name', $row['agents_id']) ?></td>
                            <!--<td><?= $row['res_email'] ?></td>-->
                            <td><?= $row['res_fname'] ?></td>
                            <td><?= $row['res_lname'] ?></td>
                            <td><?= $rows_rat['rat_name'] ?></td>
                            <td align="center">
                                <?php if ($row['company'] == 1) { ?>
                                    <?= get_value('activities', 'act_id', 'ac_andaman_adult', $rows_rat['activities_id']) ?>
                                <?php } elseif ($row['company'] == 2) { ?>
                                    <?= get_value('activities', 'act_id', 'ac_dotcom_adult', $rows_rat['activities_id']) ?>
                                <?php } elseif ($row['company'] == 3) { ?>
                                    <?= get_value('activities', 'act_id', 'ac_btoc_adult', $rows_rat['activities_id']) ?>
                                <?php } else { ?>

                                <?php } ?>
                            </td>
                            <td align="center">
                                <?php if ($row['company'] == 1) { ?>
                                    <?= get_value('activities', 'act_id', 'ac_andaman_child', $rows_rat['activities_id']) ?>
                                <?php } elseif ($row['company'] == 2) { ?>
                                    <?= get_value('activities', 'act_id', 'ac_dotcom_child', $rows_rat['activities_id']) ?>
                                <?php } elseif ($row['company'] == 3) { ?>
                                    <?= get_value('activities', 'act_id', 'ac_btoc_child', $rows_rat['activities_id']) ?>
                                <?php } else { ?>

                                <?php } ?>
                            </td>
                            <td><?= DateFormat($rows_rat['rat_travel_date'], "s") ?></td>
                            <td><?= get_value('lis_booking_status', 'lis_id', 'lis_name', $row['bookingstatus_id']) ?></td>
                            <td><?= get_value('lis_payment_status', 'lis_id', 'lis_name', $row['paymentstatus_id']) ?></td>
                            <td align="right"><?= number_format($rows_rat['rat_prices'], 0) ?></td>
                            <td align="center"><a href="./?mode=reservations/view&id=<?= $row[0] ?>"><img
                                        src="images/icon_menu/edit.png" border="0" width="20" height="20"
                                        align="absmiddle"/></a></td>
                        </tr>

                        <?php
                        $numres = $numres + 1;

                    }// end while($rows_rat = mysql_fetch_array($results_rat)){
                    ?>

                    <?php
                    ### Loop For Bus Transfer
                    $sql_rct = "SELECT * ";
                    $sql_rct .= "FROM reservation_bustransfer_items ";
                    $sql_rct .= "where reservations_id = $row[0] ";
                    if ($search_name) {
                        $sql_rct .= "AND rct_name like '%" . $search_name . "%' ";
                    }

                    if ($form_service_date) {
                        $sql_rct .= "AND rct_travel_date BETWEEN '" . $form_service_date . "' AND '" . $to_service_date . "' ";
                    }

                    $sql_rct .= "order by rct_id ASC ";

                    $results_rct = mysql_query($sql_rct);
                    while ($rows_rct = mysql_fetch_array($results_rct)) {

                        if ($color == $color_2) $color = $color_3; else $color = $color_2;
                        ?>
                        <tr bgcolor="<?= $color ?>">
                            <td><a href="./?mode=reservations/view&id=<?= $row[0] ?>"><?= $row['res_id_str'] ?></a></td>
                            <td><?= DateFormat($row['res_date'], "s") ?></td>
                            <td><?= get_value('agents', 'ag_id', 'ag_name', $row['agents_id']) ?></td>
                            <!--<td><?= $row['res_email'] ?></td>-->
                            <td><?= $row['res_fname'] ?></td>
                            <td><?= $row['res_lname'] ?></td>
                            <td><?= $rows_rct['rct_name'] ?></td>
                            <td align="center">
                                <?php if ($row['company'] == 1) { ?>
                                    <?= get_value('cartransfers', 'ct_id', 'ac_andaman_adult', $rows_rct['bustransfers_id']) ?>
                                <?php } elseif ($row['company'] == 2) { ?>
                                    <?= get_value('cartransfers', 'ct_id', 'ac_dotcom_adult', $rows_rct['bustransfers_id']) ?>
                                <?php } elseif ($row['company'] == 3) { ?>
                                    <?= get_value('cartransfers', 'ct_id', 'ac_btoc_adult', $rows_rct['bustransfers_id']) ?>
                                <?php } else { ?>

                                <?php } ?>
                            </td>
                            <td align="center">
                                <?php if ($row['company'] == 1) { ?>
                                    <?= get_value('cartransfers', 'ct_id', 'ac_andaman_child', $rows_rct['bustransfers_id']) ?>
                                <?php } elseif ($row['company'] == 2) { ?>
                                    <?= get_value('cartransfers', 'ct_id', 'ac_dotcom_child', $rows_rct['bustransfers_id']) ?>
                                <?php } elseif ($row['company'] == 3) { ?>
                                    <?= get_value('cartransfers', 'ct_id', 'ac_btoc_child', $rows_rct['bustransfers_id']) ?>
                                <?php } else { ?>

                                <?php } ?>
                            </td>
                            <td><?= DateFormat($rows_rct['rct_travel_date'], "s") ?></td>
                            <td><?= get_value('lis_booking_status', 'lis_id', 'lis_name', $row['bookingstatus_id']) ?></td>
                            <td><?= get_value('lis_payment_status', 'lis_id', 'lis_name', $row['paymentstatus_id']) ?></td>
                            <td align="right"><?= number_format($rows_rct['rct_prices'], 0) ?></td>
                            <td align="center"><a href="./?mode=reservations/view&id=<?= $row[0] ?>"><img
                                        src="images/icon_menu/edit.png" border="0" width="20" height="20"
                                        align="absmiddle"/></a></td>
                        </tr>

                        <?php
                        $numres = $numres + 1;

                    }// end while($rows_rct = mysql_fetch_array($results_rct)){
                    ?>

                    <?php
                    ### Loop For Private Land Transfer
                    $sql_rplt = "SELECT * ";
                    $sql_rplt .= "FROM reservation_privatelandtransfer_items ";
                    $sql_rplt .= "where reservations_id = $row[0] ";
                    if ($search_name) {
                        $sql_rplt .= "AND rplt_name like '%" . $search_name . "%' ";
                    }

                    if ($form_service_date) {
                        $sql_rplt .= "AND rplt_travel_date BETWEEN '" . $form_service_date . "' AND '" . $to_service_date . "' ";
                    }

                    $sql_rplt .= "order by rplt_id asc ";

                    $results_rplt = mysql_query($sql_rplt);
                    while ($rows_rplt = mysql_fetch_array($results_rplt)) {

                        if ($color == $color_2) $color = $color_3; else $color = $color_2;
                        ?>
                        <tr bgcolor="<?= $color ?>">
                            <td><a href="./?mode=reservations/view&id=<?= $row[0] ?>"><?= $row['res_id_str'] ?></a></td>
                            <td><?= DateFormat($row['res_date'], "s") ?></td>
                            <td><?= get_value('agents', 'ag_id', 'ag_name', $row['agents_id']) ?></td>
                            <!--<td><?= $row['res_email'] ?></td>-->
                            <td><?= $row['res_fname'] ?></td>
                            <td><?= $row['res_lname'] ?></td>
                            <td><?= $rows_rplt['rplt_name'] ?></td>
                            <td align="center">
                                <?php if ($row['company'] == 1) { ?>
                                    <?= get_value('privatelandtransfers', 'plt_id', 'ac_andaman_adult', $rows_rplt['privatelandtransfers_id']) ?>
                                <?php } elseif ($row['company'] == 2) { ?>
                                    <?= get_value('privatelandtransfers', 'plt_id', 'ac_dotcom_adult', $rows_rplt['privatelandtransfers_id']) ?>
                                <?php } elseif ($row['company'] == 3) { ?>
                                    <?= get_value('privatelandtransfers', 'plt_id', 'ac_btoc_adult', $rows_rplt['privatelandtransfers_id']) ?>
                                <?php } else { ?>

                                <?php } ?>
                            </td>
                            <td align="center">
                                <?php if ($row['company'] == 1) { ?>
                                    <?= get_value('privatelandtransfers', 'plt_id', 'ac_andaman_child', $rows_rplt['privatelandtransfers_id']) ?>
                                <?php } elseif ($row['company'] == 2) { ?>
                                    <?= get_value('privatelandtransfers', 'plt_id', 'ac_dotcom_child', $rows_rplt['privatelandtransfers_id']) ?>
                                <?php } elseif ($row['company'] == 3) { ?>
                                    <?= get_value('privatelandtransfers', 'plt_id', 'ac_btoc_child', $rows_rplt['privatelandtransfers_id']) ?>
                                <?php } else { ?>

                                <?php } ?>
                            </td>
                            <td><?= DateFormat($rows_rplt['rplt_travel_date'], "s") ?></td>
                            <td><?= get_value('lis_booking_status', 'lis_id', 'lis_name', $row['bookingstatus_id']) ?></td>
                            <td><?= get_value('lis_payment_status', 'lis_id', 'lis_name', $row['paymentstatus_id']) ?></td>
                            <td align="right"><?= number_format($rows_rplt['rplt_prices'], 0) ?></td>
                            <td align="center"><a href="./?mode=reservations/view&id=<?= $row[0] ?>"><img
                                        src="images/icon_menu/edit.png" border="0" width="20" height="20"
                                        align="absmiddle"/></a></td>
                        </tr>

                        <?php
                        $numres = $numres + 1;

                    }// end while($rows_rplt = mysql_fetch_array($results_rplt)){
                    ?>

                    <?php
                    ### Loop For Hotel
                    $sql_rht = "SELECT * ";
                    $sql_rht .= "FROM reservation_hotel_items ";
                    $sql_rht .= "where reservations_id = $row[0] ";
                    if ($search_name) {
                        $sql_rht .= "AND rht_name like '%" . $search_name . "%' ";
                    }

                    if ($form_service_date) {
                        $sql_rht .= "AND rht_check_in BETWEEN '" . $form_service_date . "' AND '" . $to_service_date . "' ";
                    }

                    $sql_rht .= "order by rht_id asc ";

                    $results_rht = mysql_query($sql_rht);
                    while ($rows_rht = mysql_fetch_array($results_rht)) {

                        if ($color == $color_2) $color = $color_3; else $color = $color_2;
                        ?>
                        <tr bgcolor="<?= $color ?>">
                            <td><a href="./?mode=reservations/view&id=<?= $row[0] ?>"><?= $row['res_id_str'] ?></a></td>
                            <td><?= DateFormat($row['res_date'], "s") ?></td>
                            <td><?= get_value('agents', 'ag_id', 'ag_name', $row['agents_id']) ?></td>
                            <!--<td><?= $row['res_email'] ?></td>-->
                            <td><?= $row['res_fname'] ?></td>
                            <td><?= $row['res_lname'] ?></td>
                            <td><?= $rows_rht['rht_name'] ?></td>
                            <td align="center">
                                <?php if ($row['company'] == 1) { ?>
                                    <?= get_value('hotels', 'hot_id', 'ac_andaman_adult', $rows_rht['hotels_id']) ?>
                                <?php } elseif ($row['company'] == 2) { ?>
                                    <?= get_value('hotels', 'hot_id', 'ac_dotcom_adult', $rows_rht['hotels_id']) ?>
                                <?php } elseif ($row['company'] == 3) { ?>
                                    <?= get_value('hotels', 'hot_id', 'ac_btoc_adult', $rows_rht['hotels_id']) ?>
                                <?php } else { ?>

                                <?php } ?>
                            </td>
                            <td align="center">
                                <?php if ($row['company'] == 1) { ?>
                                    <?= get_value('hotels', 'hot_id', 'ac_andaman_child', $rows_rht['hotels_id']) ?>
                                <?php } elseif ($row['company'] == 2) { ?>
                                    <?= get_value('hotels', 'hot_id', 'ac_dotcom_child', $rows_rht['hotels_id']) ?>
                                <?php } elseif ($row['company'] == 3) { ?>
                                    <?= get_value('hotels', 'hot_id', 'ac_btoc_child', $rows_rht['hotels_id']) ?>
                                <?php } else { ?>

                                <?php } ?>
                            </td>
                            <td><?= DateFormat($rows_rht['rht_check_in'], "s") ?></td>
                            <td><?= get_value('lis_booking_status', 'lis_id', 'lis_name', $row['bookingstatus_id']) ?></td>
                            <td><?= get_value('lis_payment_status', 'lis_id', 'lis_name', $row['paymentstatus_id']) ?></td>
                            <td align="right"><?= number_format($rows_rht['rht_prices'], 0) ?></td>
                            <td align="center"><a href="./?mode=reservations/view&id=<?= $row[0] ?>"><img
                                        src="images/icon_menu/edit.png" border="0" width="20" height="20"
                                        align="absmiddle"/></a></td>
                        </tr>

                        <?php
                        $numres = $numres + 1;

                    }// end while($rows_rht = mysql_fetch_array($results_rht)){
                    ?>

                    <?php
                    ### Loop For Train Transfer
                    $sql_rrt = "SELECT * ";
                    $sql_rrt .= "FROM reservation_traintransfer_items ";
                    $sql_rrt .= "where reservations_id = $row[0] ";
                    if ($search_name) {
                        $sql_rrt .= "AND rrt_name like '%" . $search_name . "%' ";
                    }

                    if ($form_service_date) {
                        $sql_rrt .= "AND rrt_travel_date BETWEEN '" . $form_service_date . "' AND '" . $to_service_date . "' ";
                    }

                    $sql_rrt .= "order by rrt_id asc ";

                    $results_rrt = mysql_query($sql_rrt);
                    while ($rows_rrt = mysql_fetch_array($results_rrt)) {

                        if ($color == $color_2) $color = $color_3; else $color = $color_2;
                        ?>
                        <tr bgcolor="<?= $color ?>">
                            <td><a href="./?mode=reservations/view&id=<?= $row[0] ?>"><?= $row['res_id_str'] ?></a></td>
                            <td><?= DateFormat($row['res_date'], "s") ?></td>
                            <td><?= get_value('agents', 'ag_id', 'ag_name', $row['agents_id']) ?></td>
                            <!--<td><?= $row['res_email'] ?></td>-->
                            <td><?= $row['res_fname'] ?></td>
                            <td><?= $row['res_lname'] ?></td>
                            <td><?= $rows_rrt['rrt_name'] ?></td>
                            <td align="center">
                                <?php if ($row['company'] == 1) { ?>
                                    <?= get_value('traintransfers', 'train_id', 'ac_andaman_adult', $rows_rrt['traintransfers_id']) ?>
                                <?php } elseif ($row['company'] == 2) { ?>
                                    <?= get_value('traintransfers', 'train_id', 'ac_dotcom_adult', $rows_rrt['traintransfers_id']) ?>
                                <?php } elseif ($row['company'] == 3) { ?>
                                    <?= get_value('traintransfers', 'train_id', 'ac_btoc_adult', $rows_rrt['traintransfers_id']) ?>
                                <?php } else { ?>

                                <?php } ?>
                            </td>
                            <td align="center">
                                <?php if ($row['company'] == 1) { ?>
                                    <?= get_value('traintransfers', 'train_id', 'ac_andaman_child', $rows_rrt['traintransfers_id']) ?>
                                <?php } elseif ($row['company'] == 2) { ?>
                                    <?= get_value('traintransfers', 'train_id', 'ac_dotcom_child', $rows_rrt['traintransfers_id']) ?>
                                <?php } elseif ($row['company'] == 3) { ?>
                                    <?= get_value('traintransfers', 'train_id', 'ac_btoc_child', $rows_rrt['traintransfers_id']) ?>
                                <?php } else { ?>

                                <?php } ?>
                            </td>
                            <td><?= DateFormat($rows_rrt['rrt_travel_date'], "s") ?></td>
                            <td><?= get_value('lis_booking_status', 'lis_id', 'lis_name', $row['bookingstatus_id']) ?></td>
                            <td><?= get_value('lis_payment_status', 'lis_id', 'lis_name', $row['paymentstatus_id']) ?></td>
                            <td align="right"><?= number_format($rows_rrt['rrt_prices'], 0) ?></td>
                            <td align="center"><a href="./?mode=reservations/view&id=<?= $row[0] ?>"><img
                                        src="images/icon_menu/edit.png" border="0" width="20" height="20"
                                        align="absmiddle"/></a></td>
                        </tr>

                        <?php
                        $numres = $numres + 1;

                    }// end while($rows_rrt = mysql_fetch_array($results_rrt)){
                    ?>

                    <?php
                } # end while($row = mysql_fetch_array($result)){
                ?>

                <tr>
                    <td colspan="12" align="right"><br/></td>
                </tr>
                <tr>
                    <td colspan="12" align="right"><b>TOTAL:</b> <font color="#0000FF"><b><?= $numres ?></b></font>
                        Reservation(s)
                    </td>
                </tr>

            </table>


            <!-- Reservation Combo Product -->

            <table width="100%" border="0" cellspacing="0" cellpadding="3">
                <tr>
                    <td>
                        <hr>
                        <br></td>
                </tr>
                <tr>
                    <td style="font-size:15px;"><b>Reservation Combo Product List</b></td>
                </tr>
            </table>

            <table width="100%" border="0" cellspacing="1" cellpadding="3" bgcolor="#FFFFFF">
                <tr bgcolor="<?= $color_1 ?>">
                    <td width="100" class="txt_bold_white">ID</td>
                    <td width="100" class="txt_bold_white">&nbsp;Book&nbsp;Date&nbsp;</td>
                    <td width="100" class="txt_bold_white">Agent&nbsp;Name</td>
                    <!--<td width="100" class="txt_bold_white" >Email</td>-->
                    <td width="100" class="txt_bold_white">First&nbsp;Name</td>
                    <td width="100" class="txt_bold_white">Last&nbsp;Name</td>
                    <td width="500" class="txt_bold_white">&nbsp;Service&nbsp;Name&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td width="100" class="txt_bold_white" align="center">Account Code (Adult)</td>
                    <td width="100" class="txt_bold_white" align="center">Account Code (Child)</td>
                    <td width="100" class="txt_bold_white">Service&nbsp;Date</td>
                    <td width="100" class="txt_bold_white">Booking Status</td>
                    <td width="100" class="txt_bold_white">Payment</td>
                    <td width="100" class="txt_bold_white">Total Amount</td>

                    <td class="txt_bold_white" width="43">&nbsp;</td>
                </tr>

                <?php

                $sql_rpa = "select * from reservation_packages ";
                $sql_rpa .= "where rpa_id > 0 ";

                if (isset($search_id)) {
                    $sql_rpa .= "AND rpa_id_str like '%" . $search_id . "%' ";
                }
                if (isset($search_fname)) {
                    $sql_rpa .= "AND rpa_fname like '%" . $search_fname . "%' ";
                }
                if (isset($search_lname)) {
                    $sql_rpa .= "AND rpa_lname like '%" . $search_lname . "%' ";
                }
                if (isset($agents_id)) {
                    $sql_rpa .= "AND agents_id = '" . $agents_id . "' ";
                }
                if (isset($bookingstatus_id)) {
                    $sql_rpa .= "AND bookingstatus_id = '" . $bookingstatus_id . "' ";
                }
                if (isset($paymentstatus_id)) {
                    $sql_rpa .= "AND paymentstatus_id = '" . $paymentstatus_id . "' ";
                }

                $sql_rpa .= " order by rpa_id DESC ";

                //echo $sql_rpa . "<br>";

                $result_rpa = mysql_query($sql_rpa);
                while ($row_rpa = mysql_fetch_array($result_rpa)) {

                    $status_show = 0;

                    ### Loop For Package Item
                    $sql_pac = "SELECT * ";
                    $sql_pac .= "FROM reservationpackage_item ";
                    $sql_pac .= "where reservationpackages_id = $row_rpa[0] ";
                    if ($search_name) {
                        $sql_pac .= "AND rpt_name like '%" . $search_name . "%' ";
                    }

                    $sql_pac .= "order by rpt_id ASC ";

                    //echo $sql_pac . "<br>";

                    $result_pac = mysql_query($sql_pac);
                    while ($row_pac = mysql_fetch_array($result_pac)) {
                        //$row_pac = mysql_fetch_array($result_pac);

                        $travel_date = "9999-99-99";

                        $rpt_item_travel_date_arr = explode("~", $row_pac['rpt_item_travel_date_arr']);
                        $rpt_item_producttype_id_arr = explode("~", $row_pac['rpt_item_producttype_id_arr']);

                        foreach ($rpt_item_travel_date_arr as $ids => $value) {

                            if ($value) {

                                if ($rpt_item_producttype_id_arr[$ids] == 1) {
                                    if ($rpt_item_travel_date_arr[$ids] < $travel_date) {
                                        $travel_date = $rpt_item_travel_date_arr[$ids];
                                    };

                                } // END if($rpt_item_producttype_id_arr[$ids] == 1){

                                if ($rpt_item_producttype_id_arr[$ids] == 2) {
                                    if ($rpt_item_travel_date_arr[$ids] < $travel_date) {
                                        $travel_date = $rpt_item_travel_date_arr[$ids];
                                    };

                                } // END if($rpt_item_producttype_id_arr[$ids] == 2){

                                if ($rpt_item_producttype_id_arr[$ids] == 3) {
                                    if ($rpt_item_travel_date_arr[$ids] < $travel_date) {
                                        $travel_date = $rpt_item_travel_date_arr[$ids];
                                    };

                                } // END if($rpt_item_producttype_id_arr[$ids] == 3){

                                if ($rpt_item_producttype_id_arr[$ids] == 4) {
                                    if ($rpt_item_travel_date_arr[$ids] < $travel_date) {
                                        $travel_date = $rpt_item_travel_date_arr[$ids];
                                    };

                                } // END if($rpt_item_producttype_id_arr[$ids] == 4){

                                if ($rpt_item_producttype_id_arr[$ids] == 5) {
                                    if ($rpt_item_travel_date_arr[$ids] < $travel_date) {
                                        $travel_date = $rpt_item_travel_date_arr[$ids];
                                    };

                                } // END if($rpt_item_producttype_id_arr[$ids] == 5){

                                if ($rpt_item_producttype_id_arr[$ids] == 6) {
                                    if ($rpt_item_travel_date_arr[$ids] < $travel_date) {
                                        $travel_date = $rpt_item_travel_date_arr[$ids];
                                    };

                                } // END if($rpt_item_producttype_id_arr[$ids] == 6){

                                if ($rpt_item_producttype_id_arr[$ids] == 7) {
                                    if ($rpt_item_travel_date_arr[$ids] < $travel_date) {
                                        $travel_date = $rpt_item_travel_date_arr[$ids];
                                    };

                                } // END if($rpt_item_producttype_id_arr[$ids] == 7){

                                if ($rpt_item_producttype_id_arr[$ids] == 8) {
                                    if ($rpt_item_travel_date_arr[$ids] < $travel_date) {
                                        $travel_date = $rpt_item_travel_date_arr[$ids];
                                    };

                                } // END if($rpt_item_producttype_id_arr[$ids] == 8){

                                $datediff = DateDiff($form_service_date, $to_service_date);
                                $date_result = $datediff + 1;

                                $service_date = $form_service_date;

                                //echo $service_date . "<br><br>";

                                $x = 1;

                                for ($i = 1; $i <= $date_result; $i++) {

                                    //if($service_date == $form_service_date){ $date_str = $form_service_date; }else{ $date_timestem = strtotime($service_date  . '+ 1 day'); $date_str = date("Y-m-d"','$date_timestem); }

                                    if ($x < $i) {
                                        $date_timestem = strtotime($date_str . '+ 1 day');
                                        $date_str = date("Y-m-d", $date_timestem);

                                    } else {
                                        $date_str = $form_service_date;

                                    }

                                    //echo $date_str . "',' ";

                                    if ($rpt_item_travel_date_arr[$ids] == $date_str) {

                                        $status_show = 1;

                                    } # end if($rpt_item_travel_date_arr[$ids] == $date_str){

                                } # end for($i=1; $i<=$date_result; $i++){

                                //echo "<br><br>";

                            } # end if($value){

                        } # end foreach($rpt_item_travel_date_arr as $ids => $value){


                        if ($status_show == 1) {

                            if ($color == $color_2) $color = $color_3; else $color = $color_2;

                            ?>

                            <tr bgcolor="<?= $color ?>">
                                <td>
                                    <a href="./?mode=reservation_packages/view&id=<?= $row_rpa[0] ?>"><?= $row_rpa['rpa_id_str'] ?></a>
                                </td>
                                <td><?= DateFormat($row_rpa['rpa_date'], "s") ?></td>
                                <td><?= get_value('agents', 'ag_id', 'ag_name', $row_rpa['agents_id']) ?></td>
                                <!--<td><?= $row_rpa['rpa_email'] ?></td>-->
                                <td><?= $row_rpa['rpa_fname'] ?></td>
                                <td><?= $row_rpa['rpa_lname'] ?></td>
                                <td><?= $row_pac['rpt_name'] ?></td>
                                <td align="center">
                                    <?php if ($row_rpa['company'] == 1) { ?>
                                        <?= get_value('packages', 'pac_id', 'ac_andaman_adult', $row_pac['packages_id']) ?>
                                    <?php } elseif ($row_rpa['company'] == 2) { ?>
                                        <?= get_value('packages', 'pac_id', 'ac_dotcom_adult', $row_pac['packages_id']) ?>
                                    <?php } elseif ($row_rpa['company'] == 3) { ?>
                                        <?= get_value('packages', 'pac_id', 'ac_btoc_adult', $row_pac['packages_id']) ?>
                                    <?php } else { ?>

                                    <?php } ?>
                                </td>
                                <td align="center">
                                    <?php if ($row_rpa['company'] == 1) { ?>
                                        <?= get_value('packages', 'pac_id', 'ac_andaman_child', $row_pac['packages_id']) ?>
                                    <?php } elseif ($row_rpa['company'] == 2) { ?>
                                        <?= get_value('packages', 'pac_id', 'ac_dotcom_child', $row_pac['packages_id']) ?>
                                    <?php } elseif ($row_rpa['company'] == 3) { ?>
                                        <?= get_value('packages', 'pac_id', 'ac_btoc_child', $row_pac['packages_id']) ?>
                                    <?php } else { ?>

                                    <?php } ?>
                                </td>
                                <td><?= DateFormat($travel_date, "s") ?></td>
                                <td><?= get_value('lis_booking_status', 'lis_id', 'lis_name', $row_rpa['bookingstatus_id']) ?></td>
                                <td><?= get_value('lis_payment_status', 'lis_id', 'lis_name', $row_rpa['paymentstatus_id']) ?></td>
                                <td align="right"><?= number_format($row_pac['rpt_prices'], 0) ?></td>
                                <td align="center"><a
                                        href="./?mode=reservation_packages/view&id=<?= $row_rpa[0] ?>"><img
                                            src="images/icon_menu/edit.png" border="0" width="20" height="20"
                                            align="absmiddle"/></a></td>
                            </tr>

                            <?php
                            $numres_combo = $numres_combo + 1;

                        }

                    } # end while($row_pac = mysql_fetch_array($result_pac)){

                } # end while($row_rpa = mysql_fetch_array($result_rpa)){

                ?>

                <tr>
                    <td colspan="12" align="right"><br/></td>
                </tr>
                <tr>
                    <td colspan="12" align="right"><b>TOTAL:</b> <font
                            color="#0000FF"><b><?= $numres_combo ?></b></font> Reservation(s)
                    </td>
                </tr>

            </table>

        </td>
    </tr>
</table>