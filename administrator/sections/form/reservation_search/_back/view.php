<?php

function setNumberLength($num, $length)
{
    $sumstr = strlen($num);
    $zero = str_repeat("0", $length - $sumstr);
    $results = $zero . $num;

    return $results;
}

function DateDiff($strDate1, $strDate2)
{
    return (strtotime($strDate2) - strtotime($strDate1)) / (60 * 60 * 24);  // 1 day = 60*60*24
}

?>

<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td valign="top">
            <table width="100%" border="0" cellspacing="0" cellpadding="3">
                <tr>
                    <td class="txt_big_gray">Search Reservation List
                        <hr/>
                    </td>
                </tr>
                <tr>
                    <td style="font-size:15px;"><b>Reservation List</b></td>
                </tr>
            </table>
            <!---- Listing Body ---->
            <table width="100%" border="0" cellspacing="0" cellpadding="3">
                <?php
                $search_id = $_POST['search_id'];
                $search_fname = $_POST['search_fname'];
                $search_lname = $_POST['search_lname'];
                $agents_id = $_POST['agents_id'];
                $bookingstatus_id = $_POST['bookingstatus_id'];
                $paymentstatus_id = $_POST['paymentstatus_id'];
                $form_service_date = $_POST['form_service_date'];
                $to_service_date = $_POST['to_service_date'];

                #DateDiff

                //echo "Date Diff = ".DateDiff($form_service_date,$to_service_date);
                $datediff = DateDiff($form_service_date, $to_service_date);
                $date_result = $datediff + 1;

                //echo "Date Result : ".$date_result;
                //echo "Today : ".$today;

                //$linkp = "&search=".$search."&plt_ref=".$plt_ref."";
                //$linkp = '';

                //$sql = "select * from reservations where res_fname like '%".$search."%'";
                $sql = "select * from reservations ";
                $sql .= "where res_id > 0 ";

                if ($search_id) {
                    $sql .= "AND res_id_str like '%" . $search_id . "%' ";
                    //$linkp .='&res_id_str='.$search_id ;
                }
                if ($search_fname) {
                    $sql .= "AND res_fname like '%" . $search_fname . "%' ";
                    //$linkp .='&res_fname='.$res_fname ;
                }
                if ($search_lname) {
                    $sql .= "AND res_lname like '%" . $search_lname . "%' ";
                    //$linkp .='&res_lname='.$res_lname ;
                }
                if ($agents_id) {
                    $sql .= "AND agents_id = '" . $agents_id . "' ";
                    //$linkp .='&agents_id='.$agents_id ;
                }
                if ($bookingstatus_id) {
                    $sql .= "AND bookingstatus_id = '" . $bookingstatus_id . "' ";
                    //$linkp .='&bookingstatus_id='.$bookingstatus_id ;
                }
                if ($paymentstatus_id) {
                    $sql .= "AND paymentstatus_id = '" . $paymentstatus_id . "' ";
                    //$linkp .='&paymentstatus_id='.$paymentstatus_id ;
                }
                //if($form_service_date){
                //$sql .= "AND res_date BETWEEN '".$form_service_date."' AND '".$to_service_date."' ";
                //$linkp .='&paymentstatus_id='.$paymentstatus_id ;
                //}
                //if($sevice_name){
                // In Item
                //$linkp .='&sevice_name='.$sevice_name ;
                //}
                //if($service_date){
                // In Item
                //$linkp .='&service_date='.$service_date ;
                //}
                //if($reserv_producttype_id){
                // In Item
                //$linkp .='&reserv_producttype_id='.$reserv_producttype_id ;
                //}

                $sql .= " order by res_id DESC";

                //echo "sql : $sql";
                //echo "<br />";
                //echo "linkp : $linkp";

                $results = mysql_query($sql);
                $numrows = mysql_num_rows($results);
                $perpage = $numrows;
                $result = $perpage;
                //$numpage = $numrows/$result;
                if (!$paper || $paper == 1) {
                    $start = 1;
                    $paper = 1;
                } else {
                    $start = (($paper - 1) * $result) + 1;
                    $result = $result * $paper;
                }
                //if ($numrows != 0) $show_page .= "<strong>Pages : </strong>";
                //for ($i=1;$i<$numpage+1;$i++)
                //{
                //	if ($paper == $i) $linkpage = "<strong style=\"color:#CCCCCC\">".$i."</strong>";
                //	else $linkpage = "<a href='./?mode=reservation_search/view&paper=".$i.$linkp."'><strong>".$i."</strong></a>";
                //	$show_page .= $linkpage." ";
                //}
                if ($paper == $i - 1) $result = $numrows;

                if ($linkp == '') {
                    //$show_row = "<b>Found : </b>".$numrows." Reservation(s)";
                }
                ?>
                <tr>

                    <td align="right" valign="bottom" colspan="2"><?php echo $show_row;
                        if ($show_page) echo "  " . $show_page; ?></td>
                </tr>
                <?php
                if ($numrows > 0)
                {
                for ($a = 1; $a < $start; $a++) mysql_fetch_array($results);
                ?>
            </table>
            <table width="100%" border="0" cellspacing="1" cellpadding="3" bgcolor="#FFFFFF">
                <form name="form" action="process.php?mode=reservations/save" method="post"
                      enctype="multipart/form-data">
                    <tr bgcolor="<?= $color_1 ?>">
                        <td width="100" class="txt_bold_white">ID</td>
                        <td width="100" class="txt_bold_white">&nbsp;Book&nbsp;Date&nbsp;</td>
                        <td width="100" class="txt_bold_white">Agent&nbsp;Name</td>
                        <td width="100" class="txt_bold_white">Email</td>
                        <td width="100" class="txt_bold_white">First&nbsp;Name</td>
                        <td width="100" class="txt_bold_white">Last&nbsp;Name</td>
                        <td width="100" class="txt_bold_white">&nbsp;Service&nbsp;Name&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        <td width="100" class="txt_bold_white">Service&nbsp;Date</td>
                        <td width="100" class="txt_bold_white">Booking Status</td>
                        <td width="100" class="txt_bold_white">Payment</td>
                        <td width="100" class="txt_bold_white">Total Amount</td>


                        <td class="txt_bold_white" width="43">&nbsp;</td>
                    </tr>
                    <?php
                    for ($start; $start < $result + 1; $start++) {
                        $row = mysql_fetch_array($results);

                        ?>

                        <?php
                        for ($i = 1; $i <= $date_result; $i++) {

                            if ($service_date == $form_service_date) {
                                $date_str = $service_date;
                            } else {
                                $date_timestem = strtotime($service_date . '+ 1 day');
                                $date_str = date("Y-m-d", $date_timestem);
                            }

                            $service_date = $date_str;


                            # Loop For Boat Transfer
                            $sql_rbt = "SELECT * ";
                            $sql_rbt .= "FROM reservation_boattransfer_items ";
                            $sql_rbt .= "where reservations_id = $row[0] ";

                            if ($service_date) {
                                $sql_rbt .= "AND rbt_travel_date = '" . $service_date . "' ";
                            }

                            $sql_rbt .= "order by rbt_id asc ";
                            //echo "sql_rbt : $sql_rbt";

                            $wlinks = "";
                            //if($p_id){$wlinks .= "&p_id=$p_id";}


                            $results_rbt = mysql_query($sql_rbt);
                            while ($rows_rbt = mysql_fetch_array($results_rbt)) {

                                if ($color == $color_2) $color = $color_3; else $color = $color_2;
                                ?>
                                <tr bgcolor="<?= $color ?>">
                                    <td>
                                        <a href="./?mode=reservations/view&id=<?= $row[0] ?>"><?= $row[res_id_str] ?></a>
                                    </td>
                                    <td><?= DateFormat($row[res_date], "s") ?></td>
                                    <td><?= get_value(agents, ag_id, ag_name, $row[agents_id]) ?></td>
                                    <td><?= $row[res_email] ?></td>
                                    <td><?= $row[res_fname] ?></td>
                                    <td><?= $row[res_lname] ?></td>
                                    <td><?= $rows_rbt[rbt_name] ?></td>
                                    <td><?= DateFormat($rows_rbt[rbt_travel_date], "s") ?></td>
                                    <td><?= get_value(lis_booking_status, lis_id, lis_name, $row[bookingstatus_id]) ?></td>
                                    <td><?= get_value(lis_payment_status, lis_id, lis_name, $row[paymentstatus_id]) ?></td>
                                    <td align="right"><?= number_format($rows_rbt[rbt_prices], 0) ?></td>
                                    <td align="center"><a href="./?mode=reservations/view&id=<?= $row[0] ?>"><img
                                                src="images/icon_menu/edit.png" border="0" width="20" height="20"
                                                align="absmiddle"/></a></td>
                                </tr>
                                <?php
                            }// END while($rows_rbt = mysql_fetch_array($results_rbt)){
                            ?>

                            <?php
                            # Loop For Pick Up Transfer
                            $sql_rpt = "SELECT * ";
                            $sql_rpt .= "FROM reservation_pickuptransfer_items ";
                            $sql_rpt .= "where reservations_id = $row[0] ";

                            if ($service_date) {
                                $sql_rpt .= "AND rpt_travel_date = '" . $service_date . "' ";
                            }


                            $sql_rpt .= "order by rpt_id asc ";
                            //echo "sql_rpt : $sql_rpt";


                            $wlinks = "";
                            //if($p_id){$wlinks .= "&p_id=$p_id";}


                            $results_rpt = mysql_query($sql_rpt);
                            while ($rows_rpt = mysql_fetch_array($results_rpt)) {

                                if ($color == $color_2) $color = $color_3; else $color = $color_2;
                                ?>
                                <tr bgcolor="<?= $color ?>">
                                    <td>
                                        <a href="./?mode=reservations/view&id=<?= $row[0] ?>"><?= $row[res_id_str] ?></a>
                                    </td>
                                    <td><?= DateFormat($row[res_date], "s") ?></td>
                                    <td><?= get_value(agents, ag_id, ag_name, $row[agents_id]) ?></td>
                                    <td><?= $row[res_email] ?></td>
                                    <td><?= $row[res_fname] ?></td>
                                    <td><?= $row[res_lname] ?></td>
                                    <td><?= $rows_rpt[rpt_name] ?></td>
                                    <td><?= DateFormat($rows_rpt[rpt_travel_date], "s") ?></td>
                                    <td><?= get_value(lis_booking_status, lis_id, lis_name, $row[bookingstatus_id]) ?></td>
                                    <td><?= get_value(lis_payment_status, lis_id, lis_name, $row[paymentstatus_id]) ?></td>
                                    <td align="right"><?= number_format($rows_rpt[rpt_prices]) ?></td>
                                    <td align="center"><a href="./?mode=reservations/view&id=<?= $row[0] ?>"><img
                                                src="images/icon_menu/edit.png" border="0" width="20" height="20"
                                                align="absmiddle"/></a></td>
                                </tr>
                                <?php
                            }// END while($rows_rpt = mysql_fetch_array($results_rpt)){
                            ?>

                            <?php
                            # Loop For Tour
                            $sql_rtt = "SELECT * ";
                            $sql_rtt .= "FROM reservation_tour_items ";
                            $sql_rtt .= "where reservations_id = $row[0] ";

                            if ($service_date) {
                                $stl_rbt .= "AND rtt_travel_date = '" . $service_date . "' ";
                            }

                            $sql_rtt .= "order by rtt_id asc ";
                            //echo "sql_rtt : $sql_rtt";

                            $wlinks = "";
                            //if($p_id){$wlinks .= "&p_id=$p_id";}


                            $results_rtt = mysql_query($sql_rtt);
                            while ($rows_rtt = mysql_fetch_array($results_rtt)) {

                                if ($color == $color_2) $color = $color_3; else $color = $color_2;
                                ?>
                                <tr bgcolor="<?= $color ?>">
                                    <td>
                                        <a href="./?mode=reservations/view&id=<?= $row[0] ?>"><?= $row[res_id_str] ?></a>
                                    </td>
                                    <td><?= DateFormat($row[res_date], "s") ?></td>
                                    <td><?= get_value(agents, ag_id, ag_name, $row[agents_id]) ?></td>
                                    <td><?= $row[res_email] ?></td>
                                    <td><?= $row[res_fname] ?></td>
                                    <td><?= $row[res_lname] ?></td>
                                    <td><?= $rows_rtt[rtt_name] ?></td>
                                    <td><?= DateFormat($rows_rtt[rtt_travel_date], "s") ?></td>
                                    <td><?= get_value(lis_booking_status, lis_id, lis_name, $row[bookingstatus_id]) ?></td>
                                    <td><?= get_value(lis_payment_status, lis_id, lis_name, $row[paymentstatus_id]) ?></td>
                                    <td align="right"><?= number_format($rows_rtt[rtt_prices], 0) ?></td>
                                    <td align="center"><a href="./?mode=reservations/view&id=<?= $row[0] ?>"><img
                                                src="images/icon_menu/edit.png" border="0" width="20" height="20"
                                                align="absmiddle"/></a></td>
                                </tr>
                                <?php
                            }// END while($rows_rtt = mysql_fetch_array($results_rtt)){
                            ?>

                            <?php
                            # Loop For Activity
                            $sql_rat = "SELECT * ";
                            $sql_rat .= "FROM reservation_activity_items ";
                            $sql_rat .= "where reservations_id = $row[0] ";

                            if ($service_date) {
                                $sql_rat .= "AND rat_travel_date = '" . $service_date . "' ";
                            }

                            $sql_rat .= "order by rat_id asc ";
                            //echo "sql_rat : $sql_rat";

                            $wlinks = "";
                            //if($p_id){$wlinks .= "&p_id=$p_id";}


                            $results_rat = mysql_query($sql_rat);
                            while ($rows_rat = mysql_fetch_array($results_rat)) {

                                if ($color == $color_2) $color = $color_3; else $color = $color_2;
                                ?>
                                <tr bgcolor="<?= $color ?>">
                                    <td>
                                        <a href="./?mode=reservations/view&id=<?= $row[0] ?>"><?= $row[res_id_str] ?></a>
                                    </td>
                                    <td><?= DateFormat($row[res_date], "s") ?></td>
                                    <td><?= get_value(agents, ag_id, ag_name, $row[agents_id]) ?></td>
                                    <td><?= $row[res_email] ?></td>
                                    <td><?= $row[res_fname] ?></td>
                                    <td><?= $row[res_lname] ?></td>
                                    <td><?= $rows_rat[rat_name] ?></td>
                                    <td><?= DateFormat($rows_rat[rat_travel_date], "s") ?></td>
                                    <td><?= get_value(lis_booking_status, lis_id, lis_name, $row[bookingstatus_id]) ?></td>
                                    <td><?= get_value(lis_payment_status, lis_id, lis_name, $row[paymentstatus_id]) ?></td>
                                    <td align="right"><?= number_format($rows_rat[rat_prices], 0) ?></td>
                                    <td align="center"><a href="./?mode=reservations/view&id=<?= $row[0] ?>"><img
                                                src="images/icon_menu/edit.png" border="0" width="20" height="20"
                                                align="absmiddle"/></a></td>
                                </tr>
                                <?php
                            }// END while($rows_rat = mysql_fetch_array($results_rat)){
                            ?>

                            <?php
                            # Loop For Bus Transfer
                            $sql_rct = "SELECT * ";
                            $sql_rct .= "FROM reservation_bustransfer_items ";
                            $sql_rct .= "where reservations_id = $row[0] ";

                            if ($service_date) {
                                $sql_rct .= "AND rct_travel_date = '" . $service_date . "' ";
                            }

                            $sql_rct .= "order by rct_id asc ";
                            //echo "sql_rct : $sql_rct";

                            $wlinks = "";
                            //if($p_id){$wlinks .= "&p_id=$p_id";}


                            $results_rct = mysql_query($sql_rct);
                            while ($rows_rct = mysql_fetch_array($results_rct)) {

                                if ($color == $color_2) $color = $color_3; else $color = $color_2;
                                ?>
                                <tr bgcolor="<?= $color ?>">
                                    <td>
                                        <a href="./?mode=reservations/view&id=<?= $row[0] ?>"><?= $row[res_id_str] ?></a>
                                    </td>
                                    <td><?= DateFormat($row[res_date], "s") ?></td>
                                    <td><?= get_value(agents, ag_id, ag_name, $row[agents_id]) ?></td>
                                    <td><?= $row[res_email] ?></td>
                                    <td><?= $row[res_fname] ?></td>
                                    <td><?= $row[res_lname] ?></td>
                                    <td><?= $rows_rct[rct_name] ?></td>
                                    <td><?= DateFormat($rows_rct[rct_travel_date], "s") ?></td>
                                    <td><?= get_value(lis_booking_status, lis_id, lis_name, $row[bookingstatus_id]) ?></td>
                                    <td><?= get_value(lis_payment_status, lis_id, lis_name, $row[paymentstatus_id]) ?></td>
                                    <td align="right"><?= number_format($rows_rct[rct_prices], 0) ?></td>
                                    <td align="center"><a href="./?mode=reservations/view&id=<?= $row[0] ?>"><img
                                                src="images/icon_menu/edit.png" border="0" width="20" height="20"
                                                align="absmiddle"/></a></td>
                                </tr>
                                <?php
                            }// END while($rows_rct = mysql_fetch_array($results_rct)){
                            ?>

                            <?php
                            # Loop For Private Land Transfer
                            $sql_rplt = "SELECT * ";
                            $sql_rplt .= "FROM reservation_privatelandtransfer_items ";
                            $sql_rplt .= "where reservations_id = $row[0] ";

                            if ($service_date) {
                                $sql_rplt .= "AND rplt_travel_date = '" . $service_date . "' ";
                            }

                            $sql_rplt .= "order by rplt_id asc ";
                            //echo "sql_rplt : $sql_rplt";

                            $wlinks = "";
                            //if($p_id){$wlinks .= "&p_id=$p_id";}


                            $results_rplt = mysql_query($sql_rplt);
                            while ($rows_rplt = mysql_fetch_array($results_rplt)) {

                                if ($color == $color_2) $color = $color_3; else $color = $color_2;
                                ?>
                                <tr bgcolor="<?= $color ?>">
                                    <td>
                                        <a href="./?mode=reservations/view&id=<?= $row[0] ?>"><?= $row[res_id_str] ?></a>
                                    </td>
                                    <td><?= DateFormat($row[res_date], "s") ?></td>
                                    <td><?= get_value(agents, ag_id, ag_name, $row[agents_id]) ?></td>
                                    <td><?= $row[res_email] ?></td>
                                    <td><?= $row[res_fname] ?></td>
                                    <td><?= $row[res_lname] ?></td>
                                    <td><?= $rows_rplt[rplt_name] ?></td>
                                    <td><?= DateFormat($rows_rplt[rplt_travel_date], "s") ?></td>
                                    <td><?= get_value(lis_booking_status, lis_id, lis_name, $row[bookingstatus_id]) ?></td>
                                    <td><?= get_value(lis_payment_status, lis_id, lis_name, $row[paymentstatus_id]) ?></td>
                                    <td align="right"><?= number_format($rows_rplt[rplt_prices], 0) ?></td>
                                    <td align="center"><a href="./?mode=reservations/view&id=<?= $row[0] ?>"><img
                                                src="images/icon_menu/edit.png" border="0" width="20" height="20"
                                                align="absmiddle"/></a></td>
                                </tr>
                                <?php
                            }// END while($rows_rplt = mysql_fetch_array($results_rplt)){
                            ?>

                            <?php
                            # Loop For Hotel
                            $sql_rht = "SELECT * ";
                            $sql_rht .= "FROM reservation_hotel_items ";
                            $sql_rht .= "where reservations_id = $row[0] ";

                            if ($service_date) {
                                $sql_rht .= "AND rht_travel_date = '" . $service_date . "' ";
                            }

                            $sql_rht .= "order by rht_id asc ";
                            //echo "sql_rht : $sql_rht";

                            $wlinks = "";
                            //if($p_id){$wlinks .= "&p_id=$p_id";}


                            $results_rht = mysql_query($sql_rht);
                            while ($rows_rht = mysql_fetch_array($results_rht)) {

                                if ($color == $color_2) $color = $color_3; else $color = $color_2;
                                ?>
                                <tr bgcolor="<?= $color ?>">
                                    <td>
                                        <a href="./?mode=reservations/view&id=<?= $row[0] ?>"><?= $row[res_id_str] ?></a>
                                    </td>
                                    <td><?= DateFormat($row[res_date], "s") ?></td>
                                    <td><?= get_value(agents, ag_id, ag_name, $row[agents_id]) ?></td>
                                    <td><?= $row[res_email] ?></td>
                                    <td><?= $row[res_fname] ?></td>
                                    <td><?= $row[res_lname] ?></td>
                                    <td><?= $rows_rht[rht_name] ?></td>
                                    <td><?= DateFormat($rows_rht[rht_travel_date], "s") ?></td>
                                    <td><?= get_value(lis_booking_status, lis_id, lis_name, $row[bookingstatus_id]) ?></td>
                                    <td><?= get_value(lis_payment_status, lis_id, lis_name, $row[paymentstatus_id]) ?></td>
                                    <td align="right"><?= number_format($rows_rht[rht_prices], 0) ?></td>
                                    <td align="center"><a href="./?mode=reservations/view&id=<?= $row[0] ?>"><img
                                                src="images/icon_menu/edit.png" border="0" width="20" height="20"
                                                align="absmiddle"/></a></td>
                                </tr>
                                <?php
                            }// END while($rows_rht = mysql_fetch_array($results_rht)){
                            ?>

                            <?php
                            # Loop For Train Transfer
                            $sql_rrt = "SELECT * ";
                            $sql_rrt .= "FROM reservation_traintransfer_items ";
                            $sql_rrt .= "where reservations_id = $row[0] ";

                            if ($service_date) {
                                $sql_rrt .= "AND rrt_travel_date = '" . $service_date . "' ";
                            }

                            $sql_rrt .= "order by rrt_id asc ";
                            //echo "sql_rrt : $sql_rrt";

                            $wlinks = "";
                            //if($p_id){$wlinks .= "&p_id=$p_id";}


                            $results_rrt = mysql_query($sql_rrt);
                            while ($rows_rrt = mysql_fetch_array($results_rrt)) {

                                if ($color == $color_2) $color = $color_3; else $color = $color_2;
                                ?>
                                <tr bgcolor="<?= $color ?>">
                                    <td>
                                        <a href="./?mode=reservations/view&id=<?= $row[0] ?>"><?= $row[res_id_str] ?></a>
                                    </td>
                                    <td><?= DateFormat($row[res_date], "s") ?></td>
                                    <td><?= get_value(agents, ag_id, ag_name, $row[agents_id]) ?></td>
                                    <td><?= $row[res_email] ?></td>
                                    <td><?= $row[res_fname] ?></td>
                                    <td><?= $row[res_lname] ?></td>
                                    <td><?= $rows_rrt[rrt_name] ?></td>
                                    <td><?= DateFormat($rows_rrt[rrt_travel_date], "s") ?></td>
                                    <td><?= get_value(lis_booking_status, lis_id, lis_name, $row[bookingstatus_id]) ?></td>
                                    <td><?= get_value(lis_payment_status, lis_id, lis_name, $row[paymentstatus_id]) ?></td>
                                    <td align="right"><?= number_format($rows_rrt[rrt_prices], 0) ?></td>
                                    <td align="center"><a href="./?mode=reservations/view&id=<?= $row[0] ?>"><img
                                                src="images/icon_menu/edit.png" border="0" width="20" height="20"
                                                align="absmiddle"/></a></td>
                                </tr>
                                <?php
                            }// END while($rows_rrt = mysql_fetch_array($results_rrt)){
                            ?>


                            <?php
                        } // end for($i=1; $i<=$date_result; $i++){

                    } // END for ($start;$start<$result+1;$start++)
                    ?>
                </form>
                <?php
                }
                ?>
            </table>

            <!---- Reservation Combo Product ---->
            <table width="100%" border="0" cellspacing="0" cellpadding="3">
                <tr>
                    <td>
                        <hr/>
                    </td>
                </tr>
                <tr>
                    <td style="font-size:15px;"><b>Reservation Combo Product List</b></td>
                </tr>
            </table>

            <!---- Listing Body ---->
            <table width="100%" border="0" cellspacing="0" cellpadding="3">
                <?php
                //$linkp = "&search=".$search."&plt_ref=".$plt_ref."";
                //$linkp = '';

                $sql_rpa = "select * from reservation_packages ";
                $sql_rpa .= "where rpa_id > 0 ";

                if ($search_id) {
                    $sql_rpa .= "AND rpa_id_str like '%" . $search_id . "%' ";
                    //$linkp .='&rpa_id_str='.$rpa_id_str ;
                }
                if ($search_fname) {
                    $sql_rpa .= "AND rpa_fname like '%" . $search_fname . "%' ";
                    //$linkp .='&rpa_fname='.$rpa_fname ;
                }
                if ($search_lname) {
                    $sql_rpa .= "AND rpa_lname like '%" . $search_lname . "%' ";
                    //$linkp .='&rpa_lname='.$rpa_lname ;
                }
                if ($agents_id) {
                    $sql_rpa .= "AND agents_id = '" . $agents_id . "' ";
                    //$linkp .='&agents_id='.$agents_id ;
                }
                if ($bookingstatus_id) {
                    $sql_rpa .= "AND bookingstatus_id = '" . $bookingstatus_id . "' ";
                    //$linkp .='&bookingstatus_id='.$bookingstatus_id ;
                }
                if ($paymentstatus_id) {
                    $sql_rpa .= "AND paymentstatus_id = '" . $paymentstatus_id . "' ";
                    //$linkp .='&paymentstatus_id='.$paymentstatus_id ;
                }
                //if($form_service_date){
                //$sql_rpa .= "AND rpa_date BETWEEN '".$form_service_date."' AND '".$to_service_date."' ";
                //$linkp .='&paymentstatus_id='.$paymentstatus_id ;
                //}
                //if($sevice_name){
                // In Item
                //$linkp .='&sevice_name='.$sevice_name ;
                //}
                //if($service_date){
                // In Item
                //$linkp .='&service_date='.$service_date ;
                //}
                //if($reserv_producttype_id){
                // In Item
                //$linkp .='&reserv_producttype_id='.$reserv_producttype_id ;
                //}

                $sql_rpa .= " order by rpa_id DESC";

                //echo "sql : $sql";
                //echo "<br />";
                //echo "linkp : $linkp";

                $results_rpa = mysql_query($sql_rpa);
                //$perpage_rpa = 10;
                //$result_rpa = $perpage_rpa;
                $numrows_rpa = mysql_num_rows($results_rpa);
                $perpage_rpa = $numrows_rpa;
                $result_rpa = $perpage_rpa;
                //$numpage_rpa = $numrows_rpa/$result_rpa;
                if (!$paper_rpa || $paper_rpa == 1) {
                    $start_rpa = 1;
                    $paper_rpa = 1;
                } else {
                    $start_rpa = (($paper_rpa - 1) * $result_rpa) + 1;
                    $result_rpa = $result_rpa * $paper_rpa;
                }
                //if ($numrows_rpa != 0) $show_page_rpa .= "<strong>Pages : </strong>";
                //for ($j=1;$j<$numpage_rpa+1;$j++)
                //{
                //	if ($paper_rpa == $j) $linkpage_rpa = "<strong style=\"color:#CCCCCC\">".$j."</strong>";
                //	else $linkpage_rpa = "<a href='./?mode=reservation_packages/index&paper=".$j.$linkp_rpa."'><strong>".$j."</strong></a>";
                //	$show_page_rpa .= $linkpage_rpa." ";
                //}
                if ($paper_rpa == $j - 1) $result_rpa = $numrows_rpa;

                if ($linkp_rpa == '') {
                    //$show_row_rpa = "<b>Found : </b>".$numrows_rpa." Reservation(s)";
                }

                ?>
                <tr>
                    <td align="right" valign="bottom" colspan="2"><?php echo $show_row_rpa;
                        if ($show_page_rpa) echo "  " . $show_page_rpa; ?></td>
                </tr>
                <?php
                if ($numrows_rpa > 0)
                {
                for ($b = 1; $b < $start_rpa; $b++) mysql_fetch_array($results_rpa);
                ?>
            </table>
            <table width="100%" border="0" cellspacing="1" cellpadding="3" bgcolor="#FFFFFF">
                <form name="form" action="process.php?mode=reservation_packages/save" method="post"
                      enctype="multipart/form-data">
                    <tr bgcolor="<?= $color_1 ?>">
                        <td width="100" class="txt_bold_white">ID</td>
                        <td width="100" class="txt_bold_white">&nbsp;Book&nbsp;Date&nbsp;</td>
                        <td width="100" class="txt_bold_white">Agent&nbsp;Name</td>
                        <td width="100" class="txt_bold_white">Email</td>
                        <td width="100" class="txt_bold_white">First&nbsp;Name</td>
                        <td width="100" class="txt_bold_white">Last&nbsp;Name</td>
                        <td width="100" class="txt_bold_white">Combo Product&nbsp;Name&nbsp;&nbsp;&nbsp;&nbsp;</td>

                        <td width="100" class="txt_bold_white">Booking Status</td>
                        <td width="100" class="txt_bold_white">Payment</td>
                        <td width="100" class="txt_bold_white">Total Amount</td>


                        <td class="txt_bold_white" width="43">&nbsp;</td>
                    </tr>
                    <?php
                    for ($start_rpa; $start_rpa < $result_rpa + 1; $start_rpa++) {

                        $row_rpa = mysql_fetch_array($results_rpa);

                        # Query For Explode Date

                        $sql_date = "SELECT * ";
                        $sql_date .= "FROM reservationpackage_item ";
                        $sql_date .= "where reservationpackages_id = $row_rpa[0] ";
                        $sql_date .= "order by rpt_id asc ";

                        $results_date = mysql_query($sql_date);
                        $rows_date = mysql_fetch_array($results_date);

                        $rpt_item_travel_date_arr = explode("~", $rows_date[rpt_item_travel_date_arr]);


                        # Loop For Package Item
                        $sql_pac = "SELECT * ";
                        $sql_pac .= "FROM reservationpackage_item ";
                        $sql_pac .= "where reservationpackages_id = $row_rpa[0] ";

                        $service_date_show = "0000-00-00";
                        $service_date = $form_service_date;

                        foreach ($rpt_item_travel_date_arr as $ids => $value) {
                            for ($i = 1; $i <= $date_result; $i++) {
                                if ($service_date == $form_service_date) {
                                    $date_str = $service_date;
                                } else {
                                    $date_timestem = strtotime($service_date . '+ 1 day');
                                    $date_str = date("Y-m-d", $date_timestem);
                                }

                                $service_date = $date_str;
                            }
                        }

                        //if($form_service_date){
                        //	$sql_pac .= "AND rpa_date BETWEEN '".$form_service_date."' AND '".$to_service_date."' ";
                        //	$sql_pac .= "AND rpt_item_travel_date_arr like '%".$form_service_date."%' ";

                        //}


                        $sql_pac .= "order by rpt_id asc ";
                        //echo "sql_rpt : $sql_rpt";


                        //$wlinks = "";
                        //if($p_id){$wlinks .= "&p_id=$p_id";}


                        $results_pac = mysql_query($sql_pac);

                        while ($rows_pac = mysql_fetch_array($results_pac)) {

                            if ($color == $color_2) $color = $color_3; else $color = $color_2;
                            ?>
                            <tr bgcolor="<?= $color ?>">
                                <td>
                                    <a href="./?mode=reservation_packages/view&id=<?= $row_rpa[0] ?>"><?= $row_rpa[rpa_id_str] ?></a>
                                </td>
                                <td><?= DateFormat($row_rpa[rpa_date], "s") ?></td>
                                <td><?= get_value(agents, ag_id, ag_name, $row_rpa[agents_id]) ?></td>
                                <td><?= $row_rpa[rpa_email] ?></td>
                                <td><?= $row_rpa[rpa_fname] ?></td>
                                <td><?= $row_rpa[rpa_lname] ?></td>
                                <td><?= $rows_pac[rpt_name] ?></td>

                                <td><?= get_value(lis_booking_status, lis_id, lis_name, $row_rpa[bookingstatus_id]) ?></td>
                                <td><?= get_value(lis_payment_status, lis_id, lis_name, $row_rpa[paymentstatus_id]) ?></td>
                                <td align="right"><?= number_format($rows_pac[rpt_prices], 0) ?></td>
                                <td align="center"><a
                                        href="./?mode=reservation_packages/view&id=<?= $row_rpa[0] ?>"><img
                                            src="images/icon_menu/edit.png" border="0" width="20" height="20"
                                            align="absmiddle"/></a></td>
                            </tr>
                            <?php
                        }// end while($rows_pac = mysql_fetch_array($results_pac)){
                        ?>

                        <?php
                    } // end for ($start_rpa;$start_rpa<$result_rpa+1;$start_rpa++)
                    ?>
                </form>
                <?php
                } // end if ($numrows_rpa > 0)
                ?>
            </table>
            <!---- Listing Body ---->
            <!---- End Reservation Combo Product ---->
            <!---- Listing Body ---->
        </td>
    </tr>
</table>