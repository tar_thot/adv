<?php

function setNumberLength($num, $length)
{
    $sumstr = strlen($num);
    $zero = str_repeat("0", $length - $sumstr);
    $results = $zero . $num;

    return $results;
}

if ($_POST['product_type_id']) {
    $product_type_id_link = $_POST['product_type_id'];
    $product_id_link = $_POST['product_id'];
    $agent_id_link = $_POST['agent_id'];

} else {
    $product_type_id_link = $product_type_id;
    $product_id_link = $product_id;
    $agent_id_link = $agent_id;

}

?>

    <table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
    <td valign="top">

    <table width="100%" border="0" cellspacing="0" cellpadding="3">
        <tr>
            <td class="txt_big_gray">Agent Rate Report (By Agent) List</td>
            <td width="500" align="right"><a href="./index.php?mode=exports/agent_rate_agent_st1"
                                             style="background-color:#ffffff; color:#000000"><<< Back</a></td>
        </tr>
    </table>


<?php
$i = 1;

$sql = "select * from agents where ag_id = $agent_id_link";
$result = mysql_query($sql);
$row = mysql_fetch_array($result);

?>


<?php if ($product_type_id_link != "99") { //Not Combo Product ?>


    <?php
    if ($product_id && $agent_id_link) {
        $sql_package_producttype = "select * from lis_package_producttype where lis_id = $product_type_id_link ";
        $result_package_producttype = mysql_query($sql_package_producttype);

        $row_package_producttype = mysql_fetch_array($result_package_producttype);

        $item_table = $row_package_producttype['item_table'];
        $item_id = $row_package_producttype['item_id'];
        $item_name = $row_package_producttype['item_name'];

        if ($product_type_id_link == "1") {
            $pricetype_id = get_value('$item_table', $item_id, 'boattransferpricetype_id', $product_id_link);
            $grade_id_link = get_value('agents', 'ag_id', 'agentgrade_id_boat', $agent_id_link);

        } else if ($product_type_id_link == "2") {
            $pricetype_id = get_value('$item_table', $item_id, 'pickuptransfer_price_type_id', $product_id_link);
            $grade_id_link = get_value('agents', 'ag_id', 'agentgrade_id_pickuptran', $agent_id_link);

        } else if ($product_type_id_link == "3") {
            $pricetype_id = get_value('$item_table', $item_id, 'tourpricetype_id', $product_id_link);
            $grade_id_link = get_value('agents', 'ag_id', 'agentgrade_id_tour', $agent_id_link);

        } else if ($product_type_id_link == "4") {
            $pricetype_id = get_value('$item_table', $item_id, 'activitypricetype_id', $product_id_link);
            $grade_id_link = get_value('agents', 'ag_id', 'agentgrade_id_activity', $agent_id_link);

        } else if ($product_type_id_link == "5") {
            $pricetype_id = get_value('$item_table', $item_id, 'cartransferpricetype_id', $product_id_link);
            $grade_id_link = get_value('agents', 'ag_id', 'agentgrade_id_bustran', $agent_id_link);

        } else if ($product_type_id_link == "6") {
            $pricetype_id = get_value('$item_table', $item_id, 'privatelandtransfer_price_type_id', $product_id_link);
            $grade_id_link = get_value('agents', 'ag_id', 'agentgrade_id_privatetran', $agent_id_link);

        } else if ($product_type_id_link == "6") {
            $pricetype_id = get_value('$item_table', $item_id, 'hotelpricetype_id', $product_id_link);
            $grade_id_link = get_value('agents', 'ag_id', 'agentgrade_id_hotel', $agent_id_link);

        } else {
            $pricetype_id = get_value('$item_table', $item_id, 'traintransferpricetype_id', $product_id_link);
            $grade_id_link = get_value('agents', 'ag_id', 'agentgrade_id_train', $agent_id_link);

        }

    } // END if($product_id && $grade_id){
    ?>

    <br/>

    <table align="center">
        <tr>
            <td class="txt_bold_gray" align="center">
                Product Name : <?= get_value($item_table, $item_id, $item_name, $product_id_link); ?>
                &nbsp;&nbsp;
                Agent Name : <?= get_value('agents', 'ag_id', 'ag_name', $agent_id_link); ?>
                &nbsp;&nbsp;
                Company : <?php if ($row['agentfor_id'] == "1") {
                    echo "WAVE";
                } else {
                    echo "DOT COM";
                } ?>

            </td>

        </tr>
    </table>

    <br/>

    <?php // Table 1 ?>

    <table border="1" cellspacing="0" cellpadding="3" bgcolor="#FFFFFF" bordercolor="#000000">
        <tr bgcolor="#CCCCCC">
            <td width="30" rowspan="2" align="center" class="txt_bold_gray">No.</td>
            <td width="800" rowspan="2" align="center" class="txt_bold_gray">Rate Type</td>
            <!--<td width="100" colspan="2" align="center" class="txt_bold_gray">Company</td>
            <td width="200" colspan="4" align="center" class="txt_bold_gray">Internet Rate</td>-->

            <?php
            $sql_grade = "select * from agent_grade where a_id = $grade_id_link";
            $result_grade = mysql_query($sql_grade);

            while ($row_grade = mysql_fetch_array($result_grade)) {
                ?>
                <td width="200" colspan="4" align="center" class="txt_bold_gray"><?= $row_grade['a_name'] ?></td>

            <?php } // END while($row_grade = mysql_fetch_array($result_grade)) ?>

        </tr>

        <tr bgcolor="#CCCCCC">
            <!--<td width="50" align="center" class="txt_bold_gray">WAVE</td>
            <td width="50" align="center" class="txt_bold_gray">DOT COM</td>-->

            <td width="50" align="center" class="txt_bold_gray">Adult</td>
            <td width="50" align="center" class="txt_bold_gray">Child</td>
            <td width="50" align="center" class="txt_bold_gray">Car (Private)</td>
            <td width="50" align="center" class="txt_bold_gray">Room</td>

        </tr>


        <?php if ($product_type_id_link == "1") { // Boat Transfer

            $ratetype = "select * from boattransfer_ratetypes where boattransfers_id = $product_id_link order by boattransfers_id ASC";
            $ratetyperatetype = mysql_query($ratetype);

            //echo $ratetype;

            while ($ratetype = mysql_fetch_array($ratetyperatetype)) {
                $boattransferperiod_status = "";
                $boattransfer_period = "select * from boattransfer_period where boattransfers_id = $product_id_link
				and botpe_datefrom != '0000-00-00' and botpe_dateto != '0000-00-00' order by botpe_datefrom ASC";
                $boattransfer_periodboattransfer_period = mysql_query($boattransfer_period);
                while ($boattransfer_period = mysql_fetch_array($boattransfer_periodboattransfer_period)) {
                    ?>

                    <tr>
                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?= $i ?></td>
                        <td class="txt_bold_gray" align="left" bgcolor="#F0F0F0"><?= $ratetype['botrt_name'] ?>
                            <?= DateFormat($boattransfer_period['botpe_datefrom'], 's') . " to " . DateFormat($boattransfer_period['botpe_dateto'], 's') ?>
                            ( <?= $boattransfer_period['botpe_name'] ?> )
                        </td>
                        <!--<td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?php if ($row['agentfor_id'] == "1") {
                            echo "&#10003;";
                        } ?></td>
        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?php if ($row['agentfor_id'] == "2") {
                            echo "&#10003;";
                        } ?></td>-->

                        <?php $row_rates = "";
                        $sql_rates = "select * from boattransfer_rates where boattransfers_id = $product_id_link ";
                        $sql_rates .= "and boattransferratetype_id = $ratetype[0] ";
                        $sql_rates .= "and boattransferperiod_id = $boattransfer_period[0] ";

                        if ($pricetype_id == "1") {
                            $sql_rates .= "and agentgrade_id = '0' ";
                        } else {
                            $sql_rates .= "and agentgrade_id = $grade_id_link ";
                        }

                        $sql_rates .= "order by botrate_id DESC ";

                        //echo $sql_rates;

                        $result_rates = mysql_query($sql_rates);
                        $row_rates = mysql_fetch_array($result_rates);

                        if ($pricetype_id == "1") {
                            $sql_percents = "select * from boattransfer_ratepercents where boattransfers_id = $product_id_link ";
                            $sql_percents .= "and agentgrade_id = $grade_id_link ";
                            $sql_percents .= "order by botrateper_id DESC ";

                            //echo $sql_percents;

                            $result_percents = mysql_query($sql_percents);
                            $row_percents = mysql_fetch_array($result_percents);

                            $rate_per1 = ($row_percents['botrateper_discount'] * $row_rates['rate_1']) / 100;
                            $rate_1 = $row_rates['rate_1'] - $rate_per1;
                            $rate_per2 = ($row_percents['botrateper_discount'] * $row_rates['rate_2']) / 100;
                            $rate_2 = $row_rates['rate_2'] - $rate_per2;
                            $rate_per3 = ($row_percents['botrateper_discount'] * $row_rates['rate_3']) / 100;
                            $rate_3 = $row_rates['rate_3'] - $rate_per3;
                            $rate_per4 = ($row_percents['botrateper_discount'] * $row_rates['rate_4']) / 100;
                            $rate_4 = $row_rates['rate_4'] - $rate_per4;

                        }
                        ?>

                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?php if ($pricetype_id == "2") {
                                echo number_format($row_rates['rate_1']);
                            } else {
                                echo number_format($rate_1);
                            } ?></td>
                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?php if ($pricetype_id == "2") {
                                echo number_format($row_rates['rate_2']);
                            } else {
                                echo number_format($rate_2);
                            } ?></td>
                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0">0</td>
                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0">0</td>

                    </tr>

                    <?php
                    $i++;

                } // END while ($boattransfer_period = mysql_fetch_array($boattransfer_periodboattransfer_period))

            } // END while($ratetype = mysql_fetch_array($ratetyperatetype))

        } // END [Boat Transfer] if($product_type_id_link == "1"){

        ?>





        <?php if ($product_type_id_link == "2") { // Pick Up Transfer

            $pickuptransfer_type_id = get_value('pickuptransfers', 'put_id', 'pickuptransfer_type_id', $product_id_link);

            $ratetype = "select * from pickuptransfer_ratetypes where pickuptransfers_id = $product_id_link order by pickuptransfers_id ASC";
            $ratetyperatetype = mysql_query($ratetype);

            //echo $ratetype;

            while ($ratetype = mysql_fetch_array($ratetyperatetype)) {
                $pickuptransferperiod_status = "";
                $pickuptransfer_period = "select * from pickuptransfer_period where pickuptransfers_id = $product_id_link
				and putpe_datefrom != '0000-00-00' and putpe_dateto != '0000-00-00' order by putpe_datefrom ASC";
                $pickuptransfer_periodpickuptransfer_period = mysql_query($pickuptransfer_period);
                while ($pickuptransfer_period = mysql_fetch_array($pickuptransfer_periodpickuptransfer_period)) {
                    ?>

                    <tr>
                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?= $i ?></td>
                        <td class="txt_bold_gray" align="left" bgcolor="#F0F0F0"><?= $ratetype['putrt_name'] ?>
                            <?= DateFormat($pickuptransfer_period['putpe_datefrom'], 's') . " to " . DateFormat($pickuptransfer_period['putpe_dateto'], 's') ?>
                            ( <?= $pickuptransfer_period['putpe_name'] ?> )
                        </td>
                        <!--<td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?php if ($row['agentfor_id'] == "1") {
                            echo "&#10003;";
                        } ?></td>
        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?php if ($row['agentfor_id'] == "2") {
                            echo "&#10003;";
                        } ?></td>-->

                        <?php $row_rates = "";
                        $sql_rates = "select * from pickuptransfer_rates where pickuptransfers_id = $product_id_link ";
                        $sql_rates .= "and pickuptransferratetype_id = $ratetype[0] ";
                        $sql_rates .= "and pickuptransferperiod_id = $pickuptransfer_period[0] ";

                        if ($pricetype_id == "1") {
                            $sql_rates .= "and agentgrade_id = '0' ";
                        } else {
                            $sql_rates .= "and agentgrade_id = $grade_id_link ";
                        }

                        $sql_rates .= "order by putrate_id DESC ";

                        $result_rates = mysql_query($sql_rates);
                        $row_rates = mysql_fetch_array($result_rates);

                        if ($pricetype_id == "1") {
                            $sql_percents = "select * from pickuptransfer_ratepercents where pickuptransfers_id = $product_id_link ";
                            $sql_percents .= "and agentgrade_id = $grade_id_link ";
                            $sql_percents .= "order by putrateper_id DESC ";

                            $result_percents = mysql_query($sql_percents);
                            $row_percents = mysql_fetch_array($result_percents);

                            $rate_per1 = ($row_percents['putrateper_discount'] * $row_rates['rate_1']) / 100;
                            $rate_1 = $row_rates['rate_1'] - $rate_per1;
                            $rate_per2 = ($row_percents['putrateper_discount'] * $row_rates['rate_2']) / 100;
                            $rate_2 = $row_rates['rate_2'] - $rate_per2;
                            $rate_per3 = ($row_percents['putrateper_discount'] * $row_rates['rate_3']) / 100;
                            $rate_3 = $row_rates['rate_3'] - $rate_per3;
                            $rate_per4 = ($row_percents['putrateper_discount'] * $row_rates['rate_4']) / 100;
                            $rate_4 = $row_rates['rate_4'] - $rate_per4;

                        }
                        ?>

                        <?php if ($pickuptransfer_type_id == "2") { ?>

                            <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0">
                                <?php if ($pricetype_id == "2") {
                                    echo number_format($row_rates['rate_1']);
                                } else {
                                    echo number_format($rate_1);
                                } ?></td>
                            <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0">
                                <?php if ($pricetype_id == "2") {
                                    echo number_format($row_rates['rate_2']);
                                } else {
                                    echo number_format($rate_2);
                                } ?></td>
                            <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0">0</td>
                            <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0">0</td>

                        <?php } else { ?>

                            <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0">0</td>
                            <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0">0</td>
                            <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0">
                                <?php if ($pricetype_id == "2") {
                                    echo number_format($row_rates['rate_4']);
                                } else {
                                    echo number_format($rate_4);
                                } ?></td>
                            <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0">0</td>

                        <?php } // END if($pickuptransfer_type_id == "2"){
                        ?>

                    </tr>

                    <?php
                    $i++;

                } // END while($pickuptransfer_period = mysql_fetch_array($pickuptransfer_periodpickuptransfer_period))

            } // END while($ratetype = mysql_fetch_array($ratetyperatetype))

        } // END [Pick Up Transfer] if($product_type_id_link == "2"){

        ?>





        <?php if ($product_type_id_link == "3") { // Tour

            $ratetype = "select * from tour_ratetypes where tours_id = $product_id_link order by tours_id ASC";
            $ratetyperatetype = mysql_query($ratetype);

            //echo $ratetype;

            while ($ratetype = mysql_fetch_array($ratetyperatetype)) {
                $tour_status = "";
                $tour_period = "select * from tour_period where tours_id = $product_id_link
				and toupe_datefrom != '0000-00-00' and toupe_dateto != '0000-00-00' order by toupe_datefrom ASC";
                $tour_periodtour_period = mysql_query($tour_period);
                while ($tour_period = mysql_fetch_array($tour_periodtour_period)) {
                    ?>

                    <tr>
                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?= $i ?></td>
                        <td class="txt_bold_gray" align="left" bgcolor="#F0F0F0"><?= $ratetype['tourt_name'] ?>
                            <?= DateFormat($tour_period['toupe_datefrom'], 's') . " to " . DateFormat($tour_period['toupe_dateto'], 's') ?>
                            ( <?= $tour_period['toupe_name'] ?> )
                        </td>
                        <!--<td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?php if ($row['agentfor_id'] == "1") {
                            echo "&#10003;";
                        } ?></td>
        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?php if ($row['agentfor_id'] == "2") {
                            echo "&#10003;";
                        } ?></td>-->

                        <?php $row_rates = "";
                        $sql_rates = "select * from tour_rates where tours_id = $product_id_link ";
                        $sql_rates .= "and tourratetype_id = $ratetype[0] ";
                        $sql_rates .= "and tourperiod_id = $tour_period[0] ";

                        if ($pricetype_id == "1") {
                            $sql_rates .= "and agentgrade_id = '0' ";
                        } else {
                            $sql_rates .= "and agentgrade_id = $grade_id_link ";
                        }

                        $sql_rates .= "order by tourate_id DESC ";

                        $result_rates = mysql_query($sql_rates);
                        $row_rates = mysql_fetch_array($result_rates);

                        if ($pricetype_id == "1") {
                            $sql_percents = "select * from tour_ratepercents where tours_id = $product_id_link ";
                            $sql_percents .= "and agentgrade_id = $grade_id_link ";
                            $sql_percents .= "order by tourateper_id DESC ";

                            $result_percents = mysql_query($sql_percents);
                            $row_percents = mysql_fetch_array($result_percents);

                            $rate_per1 = ($row_percents['tourateper_discount'] * $row_rates['rate_1']) / 100;
                            $rate_1 = $row_rates['rate_1'] - $rate_per1;
                            $rate_per2 = ($row_percents['tourateper_discount'] * $row_rates['rate_2']) / 100;
                            $rate_2 = $row_rates['rate_2'] - $rate_per2;
                            $rate_per4 = ($row_percents['tourateper_discount'] * $row_rates['rate_4']) / 100;
                            $rate_4 = $row_rates['rate_4'] - $rate_per4;

                        }
                        ?>

                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?php if ($pricetype_id == "2") {
                                echo number_format($row_rates['rate_1']);
                            } else {
                                echo number_format($rate_1);
                            } ?></td>
                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?php if ($pricetype_id == "2") {
                                echo number_format($row_rates['rate_2']);
                            } else {
                                echo number_format($rate_2);
                            } ?></td>
                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0">0</td>
                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0">0</td>

                    </tr>

                    <?php
                    $i++;

                } // END while($tour_period = mysql_fetch_array($tour_periodtour_period))

            } // END while($ratetype = mysql_fetch_array($ratetyperatetype))

        } // END [Tour] if($product_type_id_link == "3"){

        ?>





        <?php if ($product_type_id_link == "4") { // Activity

            $ratetype = "select * from activity_ratetypes where activities_id = $product_id_link order by activities_id ASC";
            $ratetyperatetype = mysql_query($ratetype);

            //echo $ratetype;

            while ($ratetype = mysql_fetch_array($ratetyperatetype)) {
                $activity_status = "";
                $activity_period = "select * from activity_period where activities_id = $product_id_link
				and actpe_datefrom != '0000-00-00' and actpe_dateto != '0000-00-00' order by actpe_datefrom ASC";
                $activity_periodactivity_period = mysql_query($activity_period);
                while ($activity_period = mysql_fetch_array($activity_periodactivity_period)) {
                    ?>

                    <tr>
                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?= $i ?></td>
                        <td class="txt_bold_gray" align="left" bgcolor="#F0F0F0"><?= $ratetype['actrt_name'] ?>
                            <?= DateFormat($activity_period['actpe_datefrom'], 's') . " to " . DateFormat($activity_period['actpe_dateto'], 's') ?>
                            ( <?= $activity_period['actpe_name'] ?> )
                        </td>
                        <!--<td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?php if ($row['agentfor_id'] == "1") {
                            echo "&#10003;";
                        } ?></td>
        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?php if ($row['agentfor_id'] == "2") {
                            echo "&#10003;";
                        } ?></td>-->

                        <?php $row_rates = "";
                        $sql_rates = "select * from activity_rates where activities_id = $product_id_link ";
                        $sql_rates .= "and activityratetype_id = $ratetype[0] ";
                        $sql_rates .= "and activityperiod_id = $activity_period[0] ";

                        if ($pricetype_id == "1") {
                            $sql_rates .= "and agentgrade_id = '0' ";
                        } else {
                            $sql_rates .= "and agentgrade_id = $grade_id_link ";
                        }

                        $sql_rates .= "order by actrate_id DESC ";

                        $result_rates = mysql_query($sql_rates);
                        $row_rates = mysql_fetch_array($result_rates);

                        if ($pricetype_id == "1") {
                            $sql_percents = "select * from activity_ratepercents where activities_id = $product_id_link ";
                            $sql_percents .= "and agentgrade_id = $grade_id_link ";
                            $sql_percents .= "order by actrateper_id DESC ";

                            $result_percents = mysql_query($sql_percents);
                            $row_percents = mysql_fetch_array($result_percents);

                            $rate_per1 = ($row_percents['actrateper_discount'] * $row_rates['rate_1']) / 100;
                            $rate_1 = $row_rates['rate_1'] - $rate_per1;
                            $rate_per2 = ($row_percents['actrateper_discount'] * $row_rates['rate_2']) / 100;
                            $rate_2 = $row_rates['rate_2'] - $rate_per2;
                            //$rate_per3 = ($row_percents['actrateper_discount']*$row_rates['rate_3'])/100;
                            //	$rate_3 = $row_rates['rate_3']-$rate_per3;
                            $rate_per4 = ($row_percents['actrateper_discount'] * $row_rates['rate_4']) / 100;
                            $rate_4 = $row_rates['rate_4'] - $rate_per4;

                        }
                        ?>

                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?php if ($pricetype_id == "2") {
                                echo number_format($row_rates['rate_1']);
                            } else {
                                echo number_format($rate_1);
                            } ?></td>
                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?php if ($pricetype_id == "2") {
                                echo number_format($row_rates['rate_2']);
                            } else {
                                echo number_format($rate_2);
                            } ?></td>
                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0">0</td>
                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0">0</td>

                    </tr>

                    <?php
                    $i++;

                } // END while($activity_period = mysql_fetch_array($activity_periodactivity_period))

            } // END while($ratetype = mysql_fetch_array($ratetyperatetype))

        } // END [Tour] if($product_type_id_link == "4"){

        ?>





        <?php if ($product_type_id_link == "5") { // Bus Transfer

            $ratetype = "select * from cartransfer_ratetypes where cartransfers_id = $product_id_link order by cartransfers_id ASC";
            $ratetyperatetype = mysql_query($ratetype);

            //echo $ratetype;

            while ($ratetype = mysql_fetch_array($ratetyperatetype)) {
                $cartransfer_status = "";
                $cartransfer_period = "select * from cartransfer_period where cartransfers_id = $product_id_link
				and catpe_datefrom != '0000-00-00' and catpe_dateto != '0000-00-00' order by catpe_datefrom ASC";
                $cartransfer_periodcartransfer_period = mysql_query($cartransfer_period);
                while ($cartransfer_period = mysql_fetch_array($cartransfer_periodcartransfer_period)) {
                    ?>

                    <tr>
                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?= $i ?></td>
                        <td class="txt_bold_gray" align="left" bgcolor="#F0F0F0"><?= $ratetype['catrt_name'] ?>
                            <?= DateFormat($cartransfer_period['catpe_datefrom'], 's') . " to " . DateFormat($cartransfer_period['catpe_dateto'], s) ?>
                            ( <?= $cartransfer_period['catpe_name'] ?> )
                        </td>
                        <!--<td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?php if ($row['agentfor_id'] == "1") {
                            echo "&#10003;";
                        } ?></td>
        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?php if ($row['agentfor_id'] == "2") {
                            echo "&#10003;";
                        } ?></td>-->

                        <?php $row_rates = "";
                        $sql_rates = "select * from cartransfer_rates where cartransfers_id = $product_id_link ";
                        $sql_rates .= "and cartransferratetype_id = $ratetype[0] ";
                        $sql_rates .= "and cartransferperiod_id = $cartransfer_period[0] ";

                        if ($pricetype_id == "1") {
                            $sql_rates .= "and agentgrade_id = '0' ";
                        } else {
                            $sql_rates .= "and agentgrade_id = $grade_id_link ";
                        }

                        $sql_rates .= "order by catrate_id DESC ";

                        $result_rates = mysql_query($sql_rates);
                        $row_rates = mysql_fetch_array($result_rates);

                        if ($pricetype_id == "1") {
                            $sql_percents = "select * from cartransfer_ratepercents where cartransfers_id = $product_id_link ";
                            $sql_percents .= "and agentgrade_id = $grade_id_link ";
                            $sql_percents .= "order by catrateper_id DESC ";

                            $result_percents = mysql_query($sql_percents);
                            $row_percents = mysql_fetch_array($result_percents);

                            $rate_per1 = ($row_percents['catrateper_discount'] * $row_rates['rate_1']) / 100;
                            $rate_1 = $row_rates['rate_1'] - $rate_per1;
                            $rate_per2 = ($row_percents['catrateper_discount'] * $row_rates['rate_2']) / 100;
                            $rate_2 = $row_rates['rate_2'] - $rate_per2;
                            //$rate_per3 = ($row_percents['catrateper_discount']*$row_rates['rate_3'])/100;
                            //	$rate_3 = $row_rates['rate_3']-$rate_per3;
                            $rate_per4 = ($row_percents['catrateper_discount'] * $row_rates['rate_4']) / 100;
                            $rate_4 = $row_rates['rate_4'] - $rate_per4;

                        }
                        ?>

                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?php if ($pricetype_id == "2") {
                                echo number_format($row_rates['rate_1']);
                            } else {
                                echo number_format($rate_1);
                            } ?></td>
                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?php if ($pricetype_id == "2") {
                                echo number_format($row_rates['rate_2']);
                            } else {
                                echo number_format($rate_2);
                            } ?></td>
                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0">0</td>
                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0">0</td>

                    </tr>

                    <?php
                    $i++;

                } // END while($cartransfer_period = mysql_fetch_array($cartransfer_periodcartransfer_period))

            } // END while($ratetype = mysql_fetch_array($ratetyperatetype))

        } // END [Tour] if($product_type_id_link == "5"){

        ?>





        <?php if ($product_type_id_link == "6") { // Private Land Transfer

            $ratetype = "select * from privatelandtransfer_ratetypes where privatelandtransfers_id = $product_id_link order by privatelandtransfers_id ASC";
            $ratetyperatetype = mysql_query($ratetype);

            //echo $ratetype;

            while ($ratetype = mysql_fetch_array($ratetyperatetype)) {
                $privatelandtransfer_status = "";
                $privatelandtransfer_period = "select * from privatelandtransfer_period where privatelandtransfers_id = $product_id_link
				and pltpe_datefrom != '0000-00-00' and pltpe_dateto != '0000-00-00' order by pltpe_datefrom ASC";
                $privatelandtransfer_periodprivatelandtransfer_period = mysql_query($privatelandtransfer_period);
                while ($privatelandtransfer_period = mysql_fetch_array($privatelandtransfer_periodprivatelandtransfer_period)) {
                    ?>

                    <tr>
                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?= $i ?></td>
                        <td class="txt_bold_gray" align="left" bgcolor="#F0F0F0"><?= $ratetype['pltrt_name'] ?>
                            <?= DateFormat($privatelandtransfer_period['pltpe_datefrom'], 's') . " to " . DateFormat($privatelandtransfer_period['pltpe_dateto'], s) ?>
                            ( <?= $privatelandtransfer_period['catpe_name'] ?> )
                        </td>
                        <!--<td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?php if ($row['agentfor_id'] == "1") {
                            echo "&#10003;";
                        } ?></td>
        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?php if ($row['agentfor_id'] == "2") {
                            echo "&#10003;";
                        } ?></td>-->

                        <?php $row_rates = "";
                        $sql_rates = "select * from privatelandtransfer_rates where privatelandtransfers_id = $product_id_link ";
                        $sql_rates .= "and privatelandtransferratetype_id = $ratetype[0] ";
                        $sql_rates .= "and privatelandtransferperiod_id = $privatelandtransfer_period[0] ";

                        if ($pricetype_id == "1") {
                            $sql_rates .= "and agentgrade_id = '0' ";
                        } else {
                            $sql_rates .= "and agentgrade_id = $grade_id_link ";
                        }

                        $sql_rates .= "order by pltrate_id DESC ";

                        $result_rates = mysql_query($sql_rates);
                        $row_rates = mysql_fetch_array($result_rates);

                        if ($pricetype_id == "1") {
                            $sql_percents = "select * from privatelandtransfer_ratepercents where privatelandtransfers_id = $product_id_link ";
                            $sql_percents .= "and agentgrade_id = $grade_id_link ";
                            $sql_percents .= "order by pltrateper_id DESC ";

                            $result_percents = mysql_query($sql_percents);
                            $row_percents = mysql_fetch_array($result_percents);

                            $rate_per1 = ($row_percents['pltrateper_discount'] * $row_rates['rate_1']) / 100;
                            $rate_1 = $row_rates['rate_1'] - $rate_per1;
                            $rate_per2 = ($row_percents['pltrateper_discount'] * $row_rates['rate_2']) / 100;
                            $rate_2 = $row_rates['rate_2'] - $rate_per2;
                            //$rate_per3 = ($row_percents[pltrateper_discount]*$row_rates['rate_3'])/100;
                            //	$rate_3 = $row_rates['rate_3']-$rate_per3;
                            $rate_per4 = ($row_percents['pltrateper_discount'] * $row_rates['rate_4']) / 100;
                            $rate_4 = $row_rates['rate_4'] - $rate_per4;

                        }
                        ?>

                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0">0</td>
                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0">0</td>
                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?php if ($pricetype_id == "2") {
                                echo number_format($row_rates['rate_4']);
                            } else {
                                echo number_format($rate_4);
                            } ?></td>
                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0">0</td>

                    </tr>

                    <?php
                    $i++;

                } // END while($privatelandtransfer_period = mysql_fetch_array($privatelandtransfer_periodprivatelandtransfer_period))

            } // END while($ratetype = mysql_fetch_array($ratetyperatetype))

        } // END [Tour] if($product_type_id_link == "6"){

        ?>





        <?php if ($product_type_id_link == "7") { // Hotel

            $ratetype = "select * from hotel_ratetypes where hotels_id = $product_id_link order by hotels_id ASC";
            $ratetyperatetype = mysql_query($ratetype);

            //echo $ratetype;

            while ($ratetype = mysql_fetch_array($ratetyperatetype)) {
                $hotel_status = "";
                $hotel_period = "select * from hotel_period where hotels_id = $product_id_link
				and hotpe_datefrom != '0000-00-00' and hotpe_dateto != '0000-00-00' order by hotpe_datefrom ASC";
                $hotel_periodhotel_period = mysql_query($hotel_period);
                while ($hotel_period = mysql_fetch_array($hotel_periodhotel_period)) {
                    ?>

                    <tr>
                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?= $i ?></td>
                        <td class="txt_bold_gray" align="left" bgcolor="#F0F0F0"><?= $ratetype['hotrt_name'] ?>
                            <?= DateFormat($hotel_period['hotpe_datefrom'], 's') . " to " . DateFormat($hotel_period['hotpe_dateto'], 's') ?>
                            ( <?= $hotel_period['hotpe_name'] ?> )
                        </td>
                        <!--<td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?php if ($row['agentfor_id'] == "1") {
                            echo "&#10003;";
                        } ?></td>
        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?php if ($row['agentfor_id'] == "2") {
                            echo "&#10003;";
                        } ?></td>-->

                        <?php $row_rates = "";
                        $sql_rates = "select * from hotel_rates where hotels_id = $product_id_link ";
                        $sql_rates .= "and hotelratetype_id = $ratetype[0] ";
                        $sql_rates .= "and hotelperiod_id = $hotel_period[0] ";

                        if ($pricetype_id == "1") {
                            $sql_rates .= "and agentgrade_id = '0' ";
                        } else {
                            $sql_rates .= "and agentgrade_id = $grade_id_link ";
                        }

                        $sql_rates .= "order by hotrate_id DESC ";

                        $result_rates = mysql_query($sql_rates);
                        $row_rates = mysql_fetch_array($result_rates);

                        if ($pricetype_id == "1") {
                            $sql_percents = "select * from hotel_ratepercents where hotels_id = $product_id_link ";
                            $sql_percents .= "and agentgrade_id = $grade_id_link ";
                            $sql_percents .= "order by hotrateper_id DESC ";

                            $result_percents = mysql_query($sql_percents);
                            $row_percents = mysql_fetch_array($result_percents);

                            $rate_per1 = ($row_percents['hotrateper_discount'] * $row_rates['rate_1']) / 100;
                            $rate_1 = $row_rates['rate_1'] - $rate_per1;
                            $rate_per2 = ($row_percents['hotrateper_discount'] * $row_rates['rate_2']) / 100;
                            $rate_2 = $row_rates['rate_2'] - $rate_per2;
                            //$rate_per3 = ($row_percents['hotrateper_discount']*$row_rates['rate_3'])/100;
                            //	$rate_3 = $row_rates['rate_3']-$rate_per3;
                            $rate_per4 = ($row_percents['hotrateper_discount'] * $row_rates['rate_4']) / 100;
                            $rate_4 = $row_rates['rate_4'] - $rate_per4;

                        }
                        ?>

                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0">0</td>
                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0">0</td>
                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0">0</td>
                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?php if ($pricetype_id == "2") {
                                echo number_format($row_rates['rate_4']);
                            } else {
                                echo number_format($rate_4);
                            } ?></td>

                    </tr>

                    <?php
                    $i++;

                } // END while($hotel_period = mysql_fetch_array($hotel_periodhotel_period))

            } // END while($ratetype = mysql_fetch_array($ratetyperatetype))

        } // END [Tour] if($product_type_id_link == "7"){

        ?>





        <?php if ($product_type_id_link == "8") { // Train Transfer

            $ratetype = "select * from traintransfer_ratetypes where traintransfers_id = $product_id_link order by traintransfers_id ASC";
            $ratetyperatetype = mysql_query($ratetype);

            //echo $ratetype;

            while ($ratetype = mysql_fetch_array($ratetyperatetype)) {
                $traintransferperiod_status = "";
                $traintransfer_period = "select * from traintransfer_period where traintransfers_id = $product_id_link
				and trape_datefrom != '0000-00-00' and trape_dateto != '0000-00-00' order by trape_datefrom ASC";
                $traintransfer_periodtraintransfer_period = mysql_query($traintransfer_period);
                while ($traintransfer_period = mysql_fetch_array($traintransfer_periodtraintransfer_period)) {
                    ?>

                    <tr>
                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?= $i ?></td>
                        <td class="txt_bold_gray" align="left" bgcolor="#F0F0F0"><?= $ratetype['trart_name'] ?>
                            <?= DateFormat($traintransfer_period['trape_datefrom'], 's') . " to " . DateFormat($traintransfer_period['trape_dateto'], 's') ?>
                            ( <?= $traintransfer_period['trape_name'] ?> )
                        </td>
                        <!--<td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?php if ($row['agentfor_id'] == "1") {
                            echo "&#10003;";
                        } ?></td>
        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?php if ($row['agentfor_id'] == "2") {
                            echo "&#10003;";
                        } ?></td>-->

                        <?php $row_rates = "";
                        $sql_rates = "select * from traintransfer_rates where traintransfers_id = $product_id_link ";
                        $sql_rates .= "and traintransferratetype_id = $ratetype[0] ";
                        $sql_rates .= "and traintransferperiod_id = $traintransfer_period[0] ";

                        if ($pricetype_id == "1") {
                            $sql_rates .= "and agentgrade_id = '0' ";
                        } else {
                            $sql_rates .= "and agentgrade_id = $grade_id_link ";
                        }

                        $sql_rates .= "order by trarate_id DESC ";

                        //echo $sql_rates;

                        $result_rates = mysql_query($sql_rates);
                        $row_rates = mysql_fetch_array($result_rates);

                        if ($pricetype_id == "1") {
                            $sql_percents = "select * from traintransfer_ratepercents where traintransfers_id = $product_id_link ";
                            $sql_percents .= "and agentgrade_id = $grade_id_link ";
                            $sql_percents .= "order by trarateper_id DESC ";

                            //echo $sql_percents;

                            $result_percents = mysql_query($sql_percents);
                            $row_percents = mysql_fetch_array($result_percents);

                            $rate_per1 = ($row_percents['trarateper_discount'] * $row_rates['rate_1']) / 100;
                            $rate_1 = $row_rates['rate_1'] - $rate_per1;
                            $rate_per2 = ($row_percents['trarateper_discount'] * $row_rates['rate_2']) / 100;
                            $rate_2 = $row_rates['rate_2'] - $rate_per2;
                            $rate_per3 = ($row_percents['trarateper_discount'] * $row_rates['rate_3']) / 100;
                            $rate_3 = $row_rates['rate_3'] - $rate_per3;
                            $rate_per4 = ($row_percents['trarateper_discount'] * $row_rates['rate_4']) / 100;
                            $rate_4 = $row_rates['rate_4'] - $rate_per4;

                        }
                        ?>

                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?php if ($pricetype_id == "2") {
                                echo number_format($row_rates['rate_1']);
                            } else {
                                echo number_format($rate_1);
                            } ?></td>
                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?php if ($pricetype_id == "2") {
                                echo number_format($row_rates['rate_2']);
                            } else {
                                echo number_format($rate_2);
                            } ?></td>
                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0">0</td>
                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0">0</td>

                    </tr>

                    <?php
                    $i++;

                } // END while ($traintransfer_period = mysql_fetch_array($traintransfer_periodtraintransfer_period))

            } // END while($ratetype = mysql_fetch_array($ratetyperatetype))

        } // END [Train Transfer] if($product_type_id_link == "8"){

        ?>


    </table>

    </td>
    <td align="right" valign="bottom"></td>
    </tr>
    </table>


<?php } else { // END if($product_type_id_link != "99"){

    if ($product_id && $agent_id_link) {
        $pricetype_id = get_value('packages', 'pac_id', 'package_price_type_id', $product_id_link);
        $grade_id_link = get_value('agents', 'ag_id', 'agentgrade_id_package', $agent_id_link);

    } // END if($product_id && $agent_id_link){

    ?>

    <br/>

    <table align="center">
        <tr>
            <td class="txt_bold_gray" align="center">
                Product Name : <?= get_value('packages', 'pac_id', 'pac_name', $product_id_link); ?>
                &nbsp;&nbsp;
                Agent Name : <?= get_value('agents', 'ag_id', 'ag_name', $agent_id_link); ?>
                &nbsp;&nbsp;
                Company : <?php if ($row['agentfor_id'] == "1") {
                    echo "WAVE";
                } else {
                    echo "DOT COM";
                } ?>

            </td>
        </tr>
    </table>

    <br/>

    <table border="1" cellspacing="0" cellpadding="3" bgcolor="#FFFFFF" bordercolor="#000000">
        <tr bgcolor="#CCCCCC">
            <td width="30" rowspan="2" align="center" class="txt_bold_gray">No.</td>
            <td width="800" rowspan="2" align="center" class="txt_bold_gray">Rate Type</td>
            <!--<td width="100" colspan="2" align="center" class="txt_bold_gray">Company</td>
            <td width="200" colspan="4" align="center" class="txt_bold_gray">Internet Rate</td>-->

            <?php
            $sql_grade = "select * from agent_grade where a_id = $grade_id_link";
            $result_grade = mysql_query($sql_grade);

            while ($row_grade = mysql_fetch_array($result_grade)) {
                ?>
                <td width="200" colspan="4" align="center" class="txt_bold_gray"><?= $row_grade['a_name'] ?></td>

            <?php } // END while($row_grade = mysql_fetch_array($result_grade)) ?>

        </tr>

        <tr bgcolor="#CCCCCC">
            <!--<td width="50" align="center" class="txt_bold_gray">WAVE</td>
            <td width="50" align="center" class="txt_bold_gray">DOT COM</td>-->

            <td width="50" align="center" class="txt_bold_gray">Adult</td>
            <td width="50" align="center" class="txt_bold_gray">Child</td>
            <td width="50" align="center" class="txt_bold_gray">Car (Private)</td>
            <td width="50" align="center" class="txt_bold_gray">Room</td>

        </tr>


        <?php if ($product_type_id_link == "99") { // Combo Product

            $ratetype = "select * from package_ratetypes where packages_id = $product_id_link order by packages_id ASC";
            $ratetyperatetype = mysql_query($ratetype);

            //echo $ratetype;

            while ($ratetype = mysql_fetch_array($ratetyperatetype)) {
                $packageperiod_status = "";
                $package_period = "select * from package_period where packages_id = $product_id_link
				and pacpe_datefrom != '0000-00-00' and pacpe_dateto != '0000-00-00' order by pacpe_datefrom ASC";
                $package_periodpackage_period = mysql_query($package_period);
                while ($package_period = mysql_fetch_array($package_periodpackage_period)) {
                    ?>

                    <tr>
                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?= $i ?></td>
                        <td class="txt_bold_gray" align="left" bgcolor="#F0F0F0"><?= $ratetype['pacrt_name'] ?>
                            <?= DateFormat($package_period['pacpe_datefrom'], s) . " to " . DateFormat($package_period['pacpe_dateto'], s) ?>
                            ( <?= $package_period['pacpe_name'] ?> )
                        </td>
                        <!--<td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?php if ($row['agentfor_id'] == "1") {
                            echo "&#10003;";
                        } ?></td>
        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?php if ($row['agentfor_id'] == "2") {
                            echo "&#10003;";
                        } ?></td>-->

                        <?php $row_rates = "";
                        $sql_rates = "select * from package_rates where packages_id = $product_id_link ";
                        $sql_rates .= "and packageratetype_id = $ratetype[0] ";
                        $sql_rates .= "and packageperiod_id = $package_period[0] ";

                        if ($pricetype_id == "1") {
                            $sql_rates .= "and agentgrade_id = '0' ";
                        } else {
                            $sql_rates .= "and agentgrade_id = $grade_id_link ";
                        }

                        $sql_rates .= "order by pacrate_id DESC ";

                        $result_rates = mysql_query($sql_rates);
                        $row_rates = mysql_fetch_array($result_rates);

                        if ($pricetype_id == "1") {
                            $sql_percents = "select * from package_ratepercents where packages_id = $product_id_link ";
                            $sql_percents .= "and agentgrade_id = $grade_id_link ";
                            $sql_percents .= "order by pacrateper_id DESC ";

                            $result_percents = mysql_query($sql_percents);
                            $row_percents = mysql_fetch_array($result_percents);

                            $rate_per1 = ($row_percents['pacrateper_discount'] * $row_rates['rate_1']) / 100;
                            $rate_1 = $row_rates['rate_1'] - $rate_per1;
                            $rate_per2 = ($row_percents['pacrateper_discount'] * $row_rates['rate_2']) / 100;
                            $rate_2 = $row_rates['rate_2'] - $rate_per2;
                            //$rate_per3 = ($row_percents['pacrateper_discount]*$row_rates[rate_3])/100;
                            //	$rate_3 = $row_rates[rate_3]-$rate_per3;
                            $rate_per4 = ($row_percents['pacrateper_discount'] * $row_rates['rate_4']) / 100;
                            $rate_4 = $row_rates['rate_4'] - $rate_per4;

                        }
                        ?>

                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><? if ($pricetype_id == "2") {
                                echo number_format($row_rates['rate_1']);
                            } else {
                                echo number_format($rate_1);
                            } ?></td>
                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?php if ($pricetype_id == "2") {
                                echo number_format($row_rates['rate_2']);
                            } else {
                                echo number_format($rate_2);
                            } ?></td>
                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0">0</td>
                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0">0</td>

                    </tr>

                    <?php
                    $i++;

                } // END while($package_period = mysql_fetch_array($package_periodpackage_period))

            } // END while($ratetype = mysql_fetch_array($ratetyperatetype))

        } // END [Boat Transfer] if($product_type_id_link == "99"){

        ?>


    </table>

    </td>
    <td align="right" valign="bottom"></td>
    </tr>
    </table>

<?php } ?>