<?php

function setNumberLength($num, $length)
{
    $sumstr = strlen($num);
    $zero = str_repeat("0", $length - $sumstr);
    $results = $zero . $num;

    return $results;
}

?>

<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td valign="top">
            <table width="100%" border="0" cellspacing="0" cellpadding="3">
                <tr>
                    <td class="txt_big_gray">DL</td>
                    <!--<td width="500" align="right"><a href="./index.php?mode=reports/customer_confirm_st1" style="background-color:#ffffff; color:#000000"><<< Back</a></td>-->
                </tr>
            </table>
            <!---- Listing Body ---->
            <table width="100%" border="0" cellspacing="0" cellpadding="3">
                <tr>
                    <td>
                        <!---- Search Box ---->

                        <table border="0" cellspacing="3" cellpadding="3" style="border: solid #CCCCCC 1px;">
                            <form name="formSt1" method="post" action="./index.php?mode=exports/dl_list">

                                <tr>
                                    <td class="txt_bold_gray" align="right">Date :</td>
                                    <td align="left">
                                        <script>DateInput('from_date', true, 'yyyy-mm-dd' <? if ($from_date) {
                                                echo ",'$from_date'";
                                            }?>)</script>
                                    </td>
                                </tr>

                                <tr>
                                    <td class="txt_bold_gray" align="right">&nbsp;</td>
                                    <td align="right"><input type="submit" name="Submit" value="SEARCH"
                                                             style="height:25px;"></td>
                                </tr>
                            </form>
                        </table>
                        <!---- Search Box ---->
                    </td>
                    <td align="right" valign="bottom"></td>
                </tr>
            </table>

            <!---- Listing Body ---->
        </td>
    </tr>
</table>