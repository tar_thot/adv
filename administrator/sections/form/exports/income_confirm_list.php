<?php

function setNumberLength($num, $length)
{
    $sumstr = strlen($num);
    $zero = str_repeat("0", $length - $sumstr);
    $results = $zero . $num;

    return $results;
}

function DateDiff($strDate1, $strDate2)
{
    return (strtotime($strDate2) - strtotime($strDate1)) / (60 * 60 * 24);  // 1 day = 60*60*24
}

?>

<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td valign="top">
            <table width="100%" border="0" cellspacing="0" cellpadding="3">
                <tr>
                    <td class="txt_big_gray">Income-Confirm Date List</td>
                    <td width="500" align="right"><a href="./index.php?mode=exports/income_confirm_st1"
                                                     style="background-color:#ffffff; color:#000000"><<< Back</a></td>
                </tr>
            </table>
            <!---- Listing Body ---->
            <table width="100%" border="0" cellspacing="0" cellpadding="3">
                <tr>
                    <td>
                        <!---- Search Box ---->
                        <br/>

                        <? //$strNewDate = date("Y-m-d", strtotime("+3 day", strtotime($_POST['to_date]))); ?>

                        <table align="center">
                            <tr>
                                <td class="txt_bold_gray" align="center">Confirm Date
                                    : <?= DateFormat($_POST['confirm_date'], "f") ?></td>

                            </tr>
                        </table>


                        <? // Income-Confirm Date ?>

                        <br/>

                        <table border="1" cellspacing="0" cellpadding="3" bgcolor="#FFFFFF" bordercolor="#000000">
                            <tr bgcolor="#CCCCCC">
                                <td width="30" rowspan="2" align="center" class="txt_bold_gray">No.</td>
                                <td width="200" rowspan="2" align="center" class="txt_bold_gray">Agent Name</td>
                                <td width="200" rowspan="2" align="center" class="txt_bold_gray">Guest Name</td>
                                <td width="100" rowspan="2" align="center" class="txt_bold_gray">Reservation ID</td>
                                <td width="100" rowspan="2" align="center" class="txt_bold_gray">Voucher No.</td>
                                <td width="100" rowspan="2" align="center" class="txt_bold_gray">Service Date</td>
                                <td width="200" rowspan="2" align="center" class="txt_bold_gray">ชนิดสินค้า</td>
                                <td width="150" colspan="3" align="center" class="txt_bold_gray">Pax, Unit</td>
                                <td width="300" colspan="3" align="center" class="txt_bold_gray">Price/Unit</td>
                                <td width="200" rowspan="2" align="center" class="txt_bold_gray">รวมจำนวนเงิน</td>
                                <td width="200" rowspan="2" align="center" class="txt_bold_gray">Remark</td>

                            </tr>

                            <tr bgcolor="#CCCCCC">
                                <td width="50" align="center" class="txt_bold_gray">ADL</td>
                                <td width="50" align="center" class="txt_bold_gray">CHD</td>
                                <td width="50" align="center" class="txt_bold_gray">ROOM</td>
                                <td width="100" align="center" class="txt_bold_gray">ADL</td>
                                <td width="100" align="center" class="txt_bold_gray">CHD</td>
                                <td width="100" align="center" class="txt_bold_gray">ROOM</td>
                            </tr>

                            <? $i = 1;
                            $total_price = 0;
                            $pax_adult = 0;
                            $pax_child = 0;
                            $pax_room = 0;
                            $total_adult = 0;
                            $total_child = 0;
                            $total_room = 0;

                            // Query Reservations

                            $sql = "SELECT * ";
                            $sql .= "FROM reservations ";
                            $sql .= "WHERE confirm_date = '" . $_POST['confirm_date'] . "' ";
                            $sql .= "AND bookingstatus_id = '3' ";
                            $sql .= "ORDER BY res_id ASC ";

                            //echo $sql;
                            //exit();

                            $result = mysql_query($sql);
                            while ($row = mysql_fetch_array($result)) {

                                // Get Value

                                $reservation_id = $row['res_id'];
                                $service_no = 0;

                                $code_agent = get_value('agents', 'ag_id', 'ag_ref', $row['agents_id']);
                                $agent_name = get_value('agents', 'ag_id', 'ag_name', $row['agents_id']);
                                $agentpaytype_id = get_value('agents', 'ag_id', 'agentpaytype_id', $row['agents_id']);

                                $lis_name = get_value('lis_titlename', 'lis_id', 'lis_name', $row['titlename_id']);
                                $confirm_payment = get_value('lis_comfirm_payment', 'lis_id', 'lis_name', $row['confirm_pay_id']);

                                // Query Reservations Boat Transfer

                                $sql_boat = "SELECT * ";
                                $sql_boat .= "FROM reservation_boattransfer_items ";
                                $sql_boat .= "WHERE reservations_id = '" . $row['res_id'] . "' ";
                                $sql_boat .= "ORDER BY rbt_id ASC ";

                                $result_boat = mysql_query($sql_boat);
                                while ($row_boat = mysql_fetch_array($result_boat)) {

                                    if ($reservation_id == $row['res_id']) {
                                        $service_no++;

                                    } else {
                                        $service_no = 1;

                                    }

                                    //$pax = "";
                                    //$pax = $row_boat['rbt_adult_num''] + $row_boat['rbt_child_num''];

                                    $bot_name = get_value('boattransfers', 'bot_id', 'bot_name', $row_boat['boattransfers_id']);
                                    $confirm_payment = get_value('lis_comfirm_payment', 'lis_id', 'lis_name', $row['confirm_pay_id']);

                                    // Voucher

                                    $sql_boat_vo = "SELECT * ";
                                    $sql_boat_vo .= "FROM voucher ";
                                    $sql_boat_vo .= "WHERE vo_res_id = '" . $row['res_id_str'] . "' ";
                                    $sql_boat_vo .= "AND vo_item_id = '$bot_name' ";
                                    $sql_boat_vo .= "AND vo_status = '2' ";
                                    $sql_boat_vo .= "ORDER BY vo_id ASC ";

                                    $result_boat_vo = mysql_query($sql_boat_vo);
                                    $row_boat_vo = mysql_fetch_array($result_boat_vo);
                                    ?>

                                    <tr>
                                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?= $i ?></td>
                                        <td class="txt_bold_gray" bgcolor="#F0F0F0"><?= $agent_name ?></td>
                                        <td class="txt_bold_gray"
                                            bgcolor="#F0F0F0"><?= $lis_name ?> <?= $row['res_fname'] ?> <?= $row['res_lname'] ?></td>
                                        <td class="txt_bold_gray" align="center"
                                            bgcolor="#F0F0F0"><?= $row['res_id_str'] ?></td>

                                        <? if ($agentpaytype_id != 2) { ?>
                                            <td class="txt_bold_gray" align="center"
                                                bgcolor="#F0F0F0"><?= $row_boat_vo['vo_no'] ?>
                                                Service <?= $service_no ?></td>

                                        <? } else { ?>
                                            <td class="txt_bold_gray" align="center"
                                                bgcolor="#F0F0F0"><?= $row['res_agent_voucher'] ?></td>

                                        <? } ?>

                                        <td class="txt_bold_gray" align="center"
                                            bgcolor="#F0F0F0"><?= DateFormat($row_boat['rbt_travel_date'], "s") ?></td>
                                        <td class="txt_bold_gray" bgcolor="#F0F0F0"><?= $bot_name ?></td>
                                        <td class="txt_bold_gray" align="center"
                                            bgcolor="#F0F0F0"><?= $row_boat['rbt_adult_num'] ?></td>
                                        <td class="txt_bold_gray" align="center"
                                            bgcolor="#F0F0F0"><?= $row_boat['rbt_child_num'] ?></td>
                                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0">0</td>
                                        <td class="txt_bold_gray" align="center"
                                            bgcolor="#F0F0F0"><?= number_format($row_boat['rbt_adult_prices'], 0) ?></td>
                                        <td class="txt_bold_gray" align="center"
                                            bgcolor="#F0F0F0"><?= number_format($row_boat['rbt_child_prices'], 0) ?></td>
                                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0">0</td>
                                        <td class="txt_bold_gray" align="center"
                                            bgcolor="#F0F0F0"><?= number_format($row_boat['rbt_prices'], 0) ?></td>
                                        <td class="txt_bold_gray" align="center"
                                            bgcolor="#F0F0F0"><?= $confirm_payment ?></td>
                                    </tr>

                                    <?
                                    $i++;
                                    $total_price = $total_price + $row_boat['rbt_prices'];
                                    $pax_adult = $pax_adult + $row_boat['rbt_adult_num'];
                                    $pax_child = $pax_child + $row_boat['rbt_child_num'];
                                    $pax_room = $pax_room + 0;
                                    $total_adult = $total_adult + $row_boat['rbt_adult_prices'];
                                    $total_child = $total_child + $row_boat['rbt_child_prices'];
                                    $total_room = $total_room + 0;
                                    ?>

                                <? } // END while($row_boat = mysql_fetch_array($result_boat)){
                                ?>


                                <?
                                // Query Reservations Pick up Transfer

                                $sql_pickup = "SELECT * ";
                                $sql_pickup .= "FROM reservation_pickuptransfer_items ";
                                $sql_pickup .= "WHERE reservations_id = '" . $row['res_id'] . "' ";
                                $sql_pickup .= "ORDER BY rpt_id ASC ";

                                $result_pickup = mysql_query($sql_pickup);
                                while ($row_pickup = mysql_fetch_array($result_pickup)) {

                                    if ($reservation_id == $row['res_id']) {
                                        $service_no++;

                                    } else {
                                        $service_no = 1;

                                    }

                                    //$pax = "";
                                    //$pax = $row_pickup['rpt_adult_num] + $row_pickup['rpt_child_num];

                                    $put_name = get_value('pickuptransfers', 'put_id', 'put_name', $row_pickup['pickuptransfers_id']);
                                    $confirm_payment = get_value('lis_comfirm_payment', 'lis_id', 'lis_name', $row['confirm_pay_id']);

                                    // Voucher

                                    $sql_pick_vo = "SELECT * ";
                                    $sql_pick_vo .= "FROM voucher ";
                                    $sql_pick_vo .= "WHERE vo_res_id = '" . $row['res_id_str'] . "' ";
                                    $sql_pick_vo .= "AND vo_item_id = '$put_name' ";
                                    $sql_pick_vo .= "AND vo_status = '2' ";
                                    $sql_pick_vo .= "ORDER BY vo_id ASC ";

                                    $result_pick_vo = mysql_query($sql_pick_vo);
                                    $row_pick_vo = mysql_fetch_array($result_pick_vo);
                                    ?>

                                    <tr>
                                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?= $i ?></td>
                                        <td class="txt_bold_gray" bgcolor="#F0F0F0"><?= $agent_name ?></td>
                                        <td class="txt_bold_gray"
                                            bgcolor="#F0F0F0"><?= $lis_name ?> <?= $row['res_fname'] ?> <?= $row['res_lname'] ?></td>
                                        <td class="txt_bold_gray" align="center"
                                            bgcolor="#F0F0F0"><?= $row['res_id_str'] ?></td>

                                        <? if ($agentpaytype_id != 2) { ?>
                                            <td class="txt_bold_gray" align="center"
                                                bgcolor="#F0F0F0"><?= $row_pick_vo['vo_no'] ?>
                                                Service <?= $service_no ?></td>

                                        <? } else { ?>
                                            <td class="txt_bold_gray" align="center"
                                                bgcolor="#F0F0F0"><?= $row['res_agent_voucher'] ?></td>

                                        <? } ?>

                                        <td class="txt_bold_gray" align="center"
                                            bgcolor="#F0F0F0"><?= DateFormat($row_pickup['rpt_travel_date'], "s") ?></td>
                                        <td class="txt_bold_gray" bgcolor="#F0F0F0"><?= $put_name ?></td>
                                        <td class="txt_bold_gray" align="center"
                                            bgcolor="#F0F0F0"><?= $row_pickup['rpt_adult_num'] ?></td>
                                        <td class="txt_bold_gray" align="center"
                                            bgcolor="#F0F0F0"><?= $row_pickup['rpt_child_num'] ?></td>
                                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0">0</td>
                                        <td class="txt_bold_gray" align="center"
                                            bgcolor="#F0F0F0"><?= number_format($row_pickup['rpt_adult_prices'], 0) ?></td>
                                        <td class="txt_bold_gray" align="center"
                                            bgcolor="#F0F0F0"><?= number_format($row_pickup['rpt_child_prices'], 0) ?></td>
                                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0">0</td>
                                        <td class="txt_bold_gray" align="center"
                                            bgcolor="#F0F0F0"><?= number_format($row_pickup['rpt_prices'], 0) ?></td>
                                        <td class="txt_bold_gray" align="center"
                                            bgcolor="#F0F0F0"><?= $confirm_payment ?></td>
                                    </tr>

                                    <?
                                    $i++;
                                    $total_price = $total_price + $row_pickup['rpt_prices'];
                                    $pax_adult = $pax_adult + $row_pickup['rpt_adult_num'];
                                    $pax_child = $pax_child + $row_pickup['rpt_child_num'];
                                    $pax_room = $pax_room + 0;
                                    $total_adult = $total_adult + $row_pickup['rpt_adult_prices'];
                                    $total_child = $total_child + $row_pickup['rpt_child_prices'];
                                    $total_room = $total_room + 0;
                                    ?>

                                <? } // END while($row_pickup = mysql_fetch_array($result_pickup)){
                                ?>


                                <?
                                // Query Reservations Tour

                                $sql_tour = "SELECT * ";
                                $sql_tour .= "FROM reservation_tour_items ";
                                $sql_tour .= "WHERE reservations_id = '" . $row['res_id'] . "' ";
                                $sql_tour .= "ORDER BY rtt_id ASC ";

                                $result_tour = mysql_query($sql_tour);
                                while ($row_tour = mysql_fetch_array($result_tour)) {

                                    if ($reservation_id == $row['res_id']) {
                                        $service_no++;

                                    } else {
                                        $service_no = 1;

                                    }

                                    //$pax = "";
                                    //$pax = $row_tour['rtt_adult_num'] + $row_tour['rtt_child_num'];

                                    $tou_name = get_value('tours', 'tou_id', 'tou_name', $row_tour['tours_id']);
                                    $confirm_payment = get_value('lis_comfirm_payment', 'lis_id', 'lis_name', $row['confirm_pay_id']);

                                    // Voucher

                                    $sql_tour_vo = "SELECT * ";
                                    $sql_tour_vo .= "FROM voucher ";
                                    $sql_tour_vo .= "WHERE vo_res_id = '" . $row['res_id_str'] . "' ";
                                    $sql_tour_vo .= "AND vo_item_id = '$tou_name' ";
                                    $sql_tour_vo .= "AND vo_status = '2' ";
                                    $sql_tour_vo .= "ORDER BY vo_id ASC ";

                                    $result_tour_vo = mysql_query($sql_tour_vo);
                                    $row_tour_vo = mysql_fetch_array($result_tour_vo);
                                    ?>

                                    <tr>
                                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?= $i ?></td>
                                        <td class="txt_bold_gray" bgcolor="#F0F0F0"><?= $agent_name ?></td>
                                        <td class="txt_bold_gray"
                                            bgcolor="#F0F0F0"><?= $lis_name ?> <?= $row['res_fname'] ?> <?= $row['res_lname'] ?></td>
                                        <td class="txt_bold_gray" align="center"
                                            bgcolor="#F0F0F0"><?= $row['res_id_str'] ?></td>

                                        <? if ($agentpaytype_id != 2) { ?>
                                            <td class="txt_bold_gray" align="center"
                                                bgcolor="#F0F0F0"><?= $row_tour_vo['vo_no'] ?>
                                                Service <?= $service_no ?></td>

                                        <? } else { ?>
                                            <td class="txt_bold_gray" align="center"
                                                bgcolor="#F0F0F0"><?= $row['res_agent_voucher'] ?></td>

                                        <? } ?>

                                        <td class="txt_bold_gray" align="center"
                                            bgcolor="#F0F0F0"><?= DateFormat($row_tour['rtt_travel_date'], "s") ?></td>
                                        <td class="txt_bold_gray" bgcolor="#F0F0F0"><?= $tou_name ?></td>
                                        <td class="txt_bold_gray" align="center"
                                            bgcolor="#F0F0F0"><?= $row_tour['rtt_adult_num'] ?></td>
                                        <td class="txt_bold_gray" align="center"
                                            bgcolor="#F0F0F0"><?= $row_tour['rtt_child_num'] ?></td>
                                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0">0</td>
                                        <td class="txt_bold_gray" align="center"
                                            bgcolor="#F0F0F0"><?= number_format($row_tour['rtt_adult_prices'], 0) ?></td>
                                        <td class="txt_bold_gray" align="center"
                                            bgcolor="#F0F0F0"><?= number_format($row_tour['rtt_child_prices'], 0) ?></td>
                                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0">0</td>
                                        <td class="txt_bold_gray" align="center"
                                            bgcolor="#F0F0F0"><?= number_format($row_tour['rtt_prices'], 0) ?></td>
                                        <td class="txt_bold_gray" align="center"
                                            bgcolor="#F0F0F0"><?= $confirm_payment ?></td>
                                    </tr>

                                    <?
                                    $i++;
                                    $total_price = $total_price + $row_tour['rtt_prices'];
                                    $pax_adult = $pax_adult + $row_tour['rtt_adult_num'];
                                    $pax_child = $pax_child + $row_tour['rtt_child_num'];
                                    $pax_room = $pax_room + 0;
                                    $total_adult = $total_adult + $row_tour['rtt_adult_prices'];
                                    $total_child = $total_child + $row_tour['rtt_child_prices'];
                                    $total_room = $total_room + 0;
                                    ?>

                                <? } // END while($row_tour = mysql_fetch_array($result_tour)){
                                ?>


                                <?
                                // Query Reservations Activity

                                $sql_activity = "SELECT * ";
                                $sql_activity .= "FROM reservation_activity_items ";
                                $sql_activity .= "WHERE reservations_id = '" . $row['res_id'] . "' ";
                                $sql_activity .= "ORDER BY rat_id ASC ";

                                $result_activity = mysql_query($sql_activity);
                                while ($row_activity = mysql_fetch_array($result_activity)) {

                                    if ($reservation_id == $row['res_id']) {
                                        $service_no++;

                                    } else {
                                        $service_no = 1;

                                    }

                                    //$pax = "";
                                    //$pax = $row_activity['rat_adult_num] + $row_activity['rat_child_num];

                                    $act_name = get_value('activities', 'act_id', 'act_name', $row_activity['activities_id']);
                                    $confirm_payment = get_value('lis_comfirm_payment', 'lis_id', 'lis_name', $row['confirm_pay_id']);

                                    // Voucher

                                    $sql_act_vo = "SELECT * ";
                                    $sql_act_vo .= "FROM voucher ";
                                    $sql_act_vo .= "WHERE vo_res_id = '" . $row['res_id_str'] . "' ";
                                    $sql_act_vo .= "AND vo_item_id = '$act_name' ";
                                    $sql_act_vo .= "AND vo_status = '2' ";
                                    $sql_act_vo .= "ORDER BY vo_id ASC ";

                                    $result_act_vo = mysql_query($sql_act_vo);
                                    $row_act_vo = mysql_fetch_array($result_act_vo);
                                    ?>

                                    <tr>
                                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?= $i ?></td>
                                        <td class="txt_bold_gray" bgcolor="#F0F0F0"><?= $agent_name ?></td>
                                        <td class="txt_bold_gray"
                                            bgcolor="#F0F0F0"><?= $lis_name ?> <?= $row['res_fname'] ?> <?= $row['res_lname'] ?></td>
                                        <td class="txt_bold_gray" align="center"
                                            bgcolor="#F0F0F0"><?= $row['res_id_str'] ?></td>

                                        <? if ($agentpaytype_id != 2) { ?>
                                            <td class="txt_bold_gray" align="center"
                                                bgcolor="#F0F0F0"><?= $row_act_vo['vo_no'] ?>
                                                Service <?= $service_no ?></td>

                                        <? } else { ?>
                                            <td class="txt_bold_gray" align="center"
                                                bgcolor="#F0F0F0"><?= $row['res_agent_voucher'] ?></td>

                                        <? } ?>

                                        <td class="txt_bold_gray" align="center"
                                            bgcolor="#F0F0F0"><?= DateFormat($row_activity['rat_travel_date'], "s") ?></td>
                                        <td class="txt_bold_gray" bgcolor="#F0F0F0"><?= $act_name ?></td>
                                        <td class="txt_bold_gray" align="center"
                                            bgcolor="#F0F0F0"><?= $row_activity['rat_adult_num'] ?></td>
                                        <td class="txt_bold_gray" align="center"
                                            bgcolor="#F0F0F0"><?= $row_activity['rat_child_num'] ?></td>
                                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0">0</td>
                                        <td class="txt_bold_gray" align="center"
                                            bgcolor="#F0F0F0"><?= number_format($row_activity['rat_adult_prices'], 0) ?></td>
                                        <td class="txt_bold_gray" align="center"
                                            bgcolor="#F0F0F0"><?= number_format($row_activity['rat_child_prices'], 0) ?></td>
                                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0">0</td>
                                        <td class="txt_bold_gray" align="center"
                                            bgcolor="#F0F0F0"><?= number_format($row_activity['rat_prices'], 0) ?></td>
                                        <td class="txt_bold_gray" align="center"
                                            bgcolor="#F0F0F0"><?= $confirm_payment ?></td>
                                    </tr>

                                    <?
                                    $i++;
                                    $total_price = $total_price + $row_activity['rat_prices'];
                                    $pax_adult = $pax_adult + $row_activity['rat_adult_num'];
                                    $pax_child = $pax_child + $row_activity['rat_child_num'];
                                    $pax_room = $pax_room + 0;
                                    $total_adult = $total_adult + $row_activity['rat_adult_prices'];
                                    $total_child = $total_child + $row_activity['rat_child_prices'];
                                    $total_room = $total_room + 0;
                                    ?>

                                <? } // END while($row_activity = mysql_fetch_array($result_activity)){
                                ?>


                                <?
                                // Query Reservations Bus Transfer

                                $sql_bus = "SELECT * ";
                                $sql_bus .= "FROM reservation_bustransfer_items ";
                                $sql_bus .= "WHERE reservations_id = '" . $row['res_id'] . "' ";
                                $sql_bus .= "ORDER BY rct_id ASC ";

                                $result_bus = mysql_query($sql_bus);
                                while ($row_bus = mysql_fetch_array($result_bus)) {

                                    if ($reservation_id == $row['res_id']) {
                                        $service_no++;

                                    } else {
                                        $service_no = 1;

                                    }

                                    //$pax = "";
                                    //$pax = $row_bus['rct_adult_num'] + $row_bus['rct_child_num'];

                                    $ct_name = get_value('cartransfers', 'ct_id', 'ct_name', $row_bus['bustransfers_id']);
                                    $confirm_payment = get_value('lis_comfirm_payment', 'lis_id', 'lis_name', $row['confirm_pay_id']);

                                    // Voucher

                                    $sql_bus_vo = "SELECT * ";
                                    $sql_bus_vo .= "FROM voucher ";
                                    $sql_bus_vo .= "WHERE vo_res_id = '" . $row['res_id_str'] . "' ";
                                    $sql_bus_vo .= "AND vo_item_id = '$ct_name' ";
                                    $sql_bus_vo .= "AND vo_status = '2' ";
                                    $sql_bus_vo .= "ORDER BY vo_id ASC ";

                                    $result_bus_vo = mysql_query($sql_bus_vo);
                                    $row_bus_vo = mysql_fetch_array($result_bus_vo);
                                    ?>

                                    <tr>
                                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?= $i ?></td>
                                        <td class="txt_bold_gray" bgcolor="#F0F0F0"><?= $agent_name ?></td>
                                        <td class="txt_bold_gray"
                                            bgcolor="#F0F0F0"><?= $lis_name ?> <?= $row['res_fname'] ?> <?= $row['res_lname'] ?></td>
                                        <td class="txt_bold_gray" align="center"
                                            bgcolor="#F0F0F0"><?= $row['res_id_str'] ?></td>

                                        <? if ($agentpaytype_id != 2) { ?>
                                            <td class="txt_bold_gray" align="center"
                                                bgcolor="#F0F0F0"><?= $row_bus_vo['vo_no'] ?>
                                                Service <?= $service_no ?></td>

                                        <? } else { ?>
                                            <td class="txt_bold_gray" align="center"
                                                bgcolor="#F0F0F0"><?= $row['res_agent_voucher'] ?></td>

                                        <? } ?>

                                        <td class="txt_bold_gray" align="center"
                                            bgcolor="#F0F0F0"><?= DateFormat($row_bus['rct_travel_date'], "s") ?></td>
                                        <td class="txt_bold_gray" bgcolor="#F0F0F0"><?= $ct_name ?></td>
                                        <td class="txt_bold_gray" align="center"
                                            bgcolor="#F0F0F0"><?= $row_bus['rct_adult_num'] ?></td>
                                        <td class="txt_bold_gray" align="center"
                                            bgcolor="#F0F0F0"><?= $row_bus['rct_child_num'] ?></td>
                                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0">0</td>
                                        <td class="txt_bold_gray" align="center"
                                            bgcolor="#F0F0F0"><?= number_format($row_bus['rct_adult_prices'], 0) ?></td>
                                        <td class="txt_bold_gray" align="center"
                                            bgcolor="#F0F0F0"><?= number_format($row_bus['rct_child_prices'], 0) ?></td>
                                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0">0</td>
                                        <td class="txt_bold_gray" align="center"
                                            bgcolor="#F0F0F0"><?= number_format($row_bus['rct_prices'], 0) ?></td>
                                        <td class="txt_bold_gray" align="center"
                                            bgcolor="#F0F0F0"><?= $confirm_payment ?></td>
                                    </tr>

                                    <?
                                    $i++;
                                    $total_price = $total_price + $row_bus['rct_prices'];
                                    $pax_adult = $pax_adult + $row_bus['rct_adult_num'];
                                    $pax_child = $pax_child + $row_bus['rct_child_num'];
                                    $pax_room = $pax_room + 0;
                                    $total_adult = $total_adult + $row_bus['rct_adult_prices'];
                                    $total_child = $total_child + $row_bus['rct_child_prices'];
                                    $total_room = $total_room + 0;
                                    ?>

                                <? } // END while($row_bus = mysql_fetch_array($result_bus)){
                                ?>


                                <?
                                // Query Reservations Private Land Transfer

                                $sql_private = "SELECT * ";
                                $sql_private .= "FROM reservation_privatelandtransfer_items ";
                                $sql_private .= "WHERE reservations_id = '" . $row['res_id'] . "' ";
                                $sql_private .= "ORDER BY rplt_id ASC ";

                                $result_private = mysql_query($sql_private);
                                while ($row_private = mysql_fetch_array($result_private)) {

                                    if ($reservation_id == $row['res_id']) {
                                        $service_no++;

                                    } else {
                                        $service_no = 1;

                                    }

                                    //$pax = "";
                                    //$pax = $row_private['rplt_adult_num'] + $row_private['rplt_child_num'];

                                    $plt_name = get_value('privatelandtransfers', 'plt_id', 'plt_name', $row_private['privatelandtransfers_id']);
                                    $confirm_payment = get_value('lis_comfirm_payment', 'lis_id', 'lis_name', $row['confirm_pay_id']);

                                    // Voucher

                                    $sql_private_vo = "SELECT * ";
                                    $sql_private_vo .= "FROM voucher ";
                                    $sql_private_vo .= "WHERE vo_res_id = '" . $row['res_id_str'] . "' ";
                                    $sql_private_vo .= "AND vo_item_id = '$plt_name' ";
                                    $sql_private_vo .= "AND vo_status = '2' ";
                                    $sql_private_vo .= "ORDER BY vo_id ASC ";

                                    $result_private_vo = mysql_query($sql_private_vo);
                                    $row_private_vo = mysql_fetch_array($result_private_vo);
                                    ?>

                                    <tr>
                                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?= $i ?></td>
                                        <td class="txt_bold_gray" bgcolor="#F0F0F0"><?= $agent_name ?></td>
                                        <td class="txt_bold_gray"
                                            bgcolor="#F0F0F0"><?= $lis_name ?> <?= $row['res_fname'] ?> <?= $row['res_lname'] ?></td>
                                        <td class="txt_bold_gray" align="center"
                                            bgcolor="#F0F0F0"><?= $row['res_id_str'] ?></td>

                                        <? if ($agentpaytype_id != 2) { ?>
                                            <td class="txt_bold_gray" align="center"
                                                bgcolor="#F0F0F0"><?= $row_private_vo['vo_no'] ?>
                                                Service <?= $service_no ?></td>

                                        <? } else { ?>
                                            <td class="txt_bold_gray" align="center"
                                                bgcolor="#F0F0F0"><?= $row['res_agent_voucher'] ?></td>

                                        <? } ?>

                                        <td class="txt_bold_gray" align="center"
                                            bgcolor="#F0F0F0"><?= DateFormat($row_private['rplt_travel_date'], "s") ?></td>
                                        <td class="txt_bold_gray" bgcolor="#F0F0F0"><?= $plt_name ?></td>
                                        <td class="txt_bold_gray" align="center"
                                            bgcolor="#F0F0F0"><?= $row_private['rplt_adult_num'] ?></td>
                                        <td class="txt_bold_gray" align="center"
                                            bgcolor="#F0F0F0"><?= $row_private['rplt_child_num'] ?></td>
                                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0">0</td>
                                        <td class="txt_bold_gray" align="center"
                                            bgcolor="#F0F0F0"><?= number_format($row_private['rplt_adult_prices'], 0) ?></td>
                                        <td class="txt_bold_gray" align="center"
                                            bgcolor="#F0F0F0"><?= number_format($row_private['rplt_child_prices'], 0) ?></td>
                                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0">0</td>
                                        <td class="txt_bold_gray" align="center"
                                            bgcolor="#F0F0F0"><?= number_format($row_private['rplt_prices'], 0) ?></td>
                                        <td class="txt_bold_gray" align="center"
                                            bgcolor="#F0F0F0"><?= $confirm_payment ?></td>
                                    </tr>

                                    <?
                                    $i++;
                                    $total_price = $total_price + $row_private['rplt_prices'];
                                    $pax_adult = $pax_adult + $row_private['rplt_adult_num'];
                                    $pax_child = $pax_child + $row_private['rplt_child_num'];
                                    $pax_room = $pax_room + 0;
                                    $total_adult = $total_adult + $row_private['rplt_adult_prices'];
                                    $total_child = $total_child + $row_private['rplt_child_prices'];
                                    $total_room = $total_room + 0;
                                    ?>

                                <? } // END while($row_private = mysql_fetch_array($result_private)){
                                ?>


                                <?
                                // Query Reservations Hotel

                                $sql_hotel = "SELECT * ";
                                $sql_hotel .= "FROM reservation_hotel_items ";
                                $sql_hotel .= "WHERE reservations_id = '" . $row['res_id'] . "' ";
                                $sql_hotel .= "ORDER BY rht_id ASC ";

                                $result_hotel = mysql_query($sql_hotel);
                                while ($row_hotel = mysql_fetch_array($result_hotel)) {

                                    if ($reservation_id == $row['res_id']) {
                                        $service_no++;

                                    } else {
                                        $service_no = 1;

                                    }

                                    //$pax = "";
                                    //$pax = $row_hotel['rht_adult_num'] + $row_hotel['rht_child_num'];
                                    //$pax = $row_hotel['rht_room_num']*1;

                                    $hot_name = get_value('hotels', 'hot_id', 'hot_name', $row_hotel['hotels_id']);
                                    $confirm_payment = get_value('lis_comfirm_payment', 'lis_id', 'lis_name', $row['confirm_pay_id']);

                                    $prices = $row_hotel['rht_prices'] / $row_hotel['rht_room_num'];

                                    // Voucher

                                    $sql_hotel_vo = "SELECT * ";
                                    $sql_hotel_vo .= "FROM voucher ";
                                    $sql_hotel_vo .= "WHERE vo_res_id = '" . $row['res_id_str'] . "' ";
                                    $sql_hotel_vo .= "AND vo_item_id = '$hot_name' ";
                                    $sql_hotel_vo .= "AND vo_status = '2' ";
                                    $sql_hotel_vo .= "ORDER BY vo_id ASC ";

                                    $result_hotel_vo = mysql_query($sql_hotel_vo);
                                    $row_hotel_vo = mysql_fetch_array($result_hotel_vo);
                                    ?>

                                    <tr>
                                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?= $i ?></td>
                                        <td class="txt_bold_gray" bgcolor="#F0F0F0"><?= $agent_name ?></td>
                                        <td class="txt_bold_gray"
                                            bgcolor="#F0F0F0"><?= $lis_name ?> <?= $row['res_fname'] ?> <?= $row['res_lname'] ?></td>
                                        <td class="txt_bold_gray" align="center"
                                            bgcolor="#F0F0F0"><?= $row['res_id_str'] ?></td>

                                        <? if ($agentpaytype_id != 2) { ?>
                                            <td class="txt_bold_gray" align="center"
                                                bgcolor="#F0F0F0"><?= $row_hotel_vo['vo_no'] ?>
                                                Service <?= $service_no ?></td>

                                        <? } else { ?>
                                            <td class="txt_bold_gray" align="center"
                                                bgcolor="#F0F0F0"><?= $row['res_agent_voucher'] ?></td>

                                        <? } ?>

                                        <td class="txt_bold_gray" align="center"
                                            bgcolor="#F0F0F0"><?= DateFormat($row_hotel['rht_check_in'], "s") ?></td>
                                        <td class="txt_bold_gray" bgcolor="#F0F0F0"><?= $hot_name ?></td>
                                        <td class="txt_bold_gray" align="center"
                                            bgcolor="#F0F0F0"><?= $row_hotel['rht_adult_num'] ?></td>
                                        <td class="txt_bold_gray" align="center"
                                            bgcolor="#F0F0F0"><?= $row_hotel['rht_child_num'] ?></td>
                                        <td class="txt_bold_gray" align="center"
                                            bgcolor="#F0F0F0"><?= $row_hotel['rht_room_num'] ?></td>
                                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0">0</td>
                                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0">0</td>
                                        <td class="txt_bold_gray" align="center"
                                            bgcolor="#F0F0F0"><?= number_format($prices, 0) ?></td>
                                        <td class="txt_bold_gray" align="center"
                                            bgcolor="#F0F0F0"><?= number_format($row_hotel['rht_prices'], 0) ?></td>
                                        <td class="txt_bold_gray" align="center"
                                            bgcolor="#F0F0F0"><?= $confirm_payment ?></td>
                                    </tr>

                                    <?
                                    $i++;
                                    $total_price = $total_price + $row_hotel['rht_prices'];
                                    $pax_adult = $pax_adult + $row_hotel['rht_adult_num'];
                                    $pax_child = $pax_child + $row_hotel['rht_child_num'];
                                    $pax_room = $pax_room + $row_hotel['rht_room_num'];
                                    $total_adult = $total_adult + 0;
                                    $total_child = $total_child + 0;
                                    $total_room = $total_room + $prices;
                                    ?>

                                <? } // END while($row_hotel = mysql_fetch_array($result_hotel)){
                                ?>


                                <?
                                // Query Reservations Train Transfer

                                $sql_train = "SELECT * ";
                                $sql_train .= "FROM reservation_traintransfer_items ";
                                $sql_train .= "WHERE reservations_id = '" . $row['res_id'] . "' ";
                                $sql_train .= "ORDER BY rrt_id ASC ";

                                $result_train = mysql_query($sql_train);
                                while ($row_train = mysql_fetch_array($result_train)) {

                                    if ($reservation_id == $row['res_id']) {
                                        $service_no++;

                                    } else {
                                        $service_no = 1;

                                    }

                                    //$pax = "";
                                    //$pax = $row_train['rrt_adult_num'] + $row_train['rrt_child_num'];

                                    $train_name = get_value('traintransfers', 'train_id', 'train_name', $row_train['traintransfers_id']);
                                    $confirm_payment = get_value('lis_comfirm_payment', 'lis_id', 'lis_name', $row['confirm_pay_id']);

                                    // Voucher

                                    $sql_train_vo = "SELECT * ";
                                    $sql_train_vo .= "FROM voucher ";
                                    $sql_train_vo .= "WHERE vo_res_id = '" . $row['res_id_str'] . "' ";
                                    $sql_train_vo .= "AND vo_item_id = '$train_name' ";
                                    $sql_train_vo .= "AND vo_status = '2' ";
                                    $sql_train_vo .= "ORDER BY vo_id ASC ";

                                    $result_train_vo = mysql_query($sql_train_vo);
                                    $row_train_vo = mysql_fetch_array($result_train_vo);
                                    ?>

                                    <tr>
                                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?= $i ?></td>
                                        <td class="txt_bold_gray" bgcolor="#F0F0F0"><?= $agent_name ?></td>
                                        <td class="txt_bold_gray"
                                            bgcolor="#F0F0F0"><?= $lis_name ?> <?= $row['res_fname'] ?> <?= $row['res_lname'] ?></td>
                                        <td class="txt_bold_gray" align="center"
                                            bgcolor="#F0F0F0"><?= $row['res_id_str'] ?></td>

                                        <? if ($agentpaytype_id != 2) { ?>
                                            <td class="txt_bold_gray" align="center"
                                                bgcolor="#F0F0F0"><?= $row_train_vo['vo_no'] ?>
                                                Service <?= $service_no ?></td>

                                        <? } else { ?>
                                            <td class="txt_bold_gray" align="center"
                                                bgcolor="#F0F0F0"><?= $row['res_agent_voucher'] ?></td>

                                        <? } ?>

                                        <td class="txt_bold_gray" align="center"
                                            bgcolor="#F0F0F0"><?= DateFormat($row_train['rrt_travel_date'], "s") ?></td>
                                        <td class="txt_bold_gray" bgcolor="#F0F0F0"><?= $train_name ?></td>
                                        <td class="txt_bold_gray" align="center"
                                            bgcolor="#F0F0F0"><?= $row_train['rrt_adult_num'] ?></td>
                                        <td class="txt_bold_gray" align="center"
                                            bgcolor="#F0F0F0"><?= $row_train['rrt_child_num'] ?></td>
                                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0">0</td>
                                        <td class="txt_bold_gray" align="center"
                                            bgcolor="#F0F0F0"><?= number_format($row_train['rrt_adult_prices'], 0) ?></td>
                                        <td class="txt_bold_gray" align="center"
                                            bgcolor="#F0F0F0"><?= number_format($row_train['rrt_child_prices'], 0) ?></td>
                                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0">0</td>
                                        <td class="txt_bold_gray" align="center"
                                            bgcolor="#F0F0F0"><?= number_format($row_train['rrt_prices'], 0) ?></td>
                                        <td class="txt_bold_gray" align="center"
                                            bgcolor="#F0F0F0"><?= $confirm_payment ?></td>
                                    </tr>

                                    <?
                                    $i++;
                                    $total_price = $total_price + $row_train['rrt_prices'];
                                    $pax_adult = $pax_adult + $row_train['rrt_adult_num'];
                                    $pax_child = $pax_child + $row_train['rrt_child_num'];
                                    $pax_room = $pax_room + 0;
                                    $total_adult = $total_adult + $row_train['rrt_adult_prices'];
                                    $total_child = $total_child + $row_train['rrt_child_prices'];
                                    $total_room = $total_room + 0;
                                    ?>

                                <? } // END while($row_train = mysql_fetch_array($result_train)){
                                ?>


                            <? } // END while($row = mysql_fetch_array($result)){	?>





                            <? // Query Reservations Combo Product

                            $sql_combo = "SELECT * ";
                            $sql_combo .= "FROM reservation_packages ";
                            $sql_combo .= "WHERE confirm_date = '" . $_POST['confirm_date'] . "' ";
                            $sql_combo .= "AND bookingstatus_id = '3' ";
                            $sql_combo .= "ORDER BY rpa_id ASC ";

                            //echo $sql_combo;

                            $result_combo = mysql_query($sql_combo);
                            while ($row_combo = mysql_fetch_array($result_combo)) {

                                // Get Value

                                $reservation_id = $row_combo['rpa_id'];
                                $service_no = 0;

                                $code_agent = get_value('agents', 'ag_id', 'ag_ref', $row_combo['agents_id']);
                                $agent_name = get_value('agents', 'ag_id', 'ag_name', $row_combo['agents_id']);
                                $agentpaytype_id = get_value('agents', 'ag_id', 'agentpaytype_id', $row_combo['agents_id']);

                                $lis_name = get_value('lis_titlename', 'lis_id', 'lis_name', $row_combo['titlename_id']);
                                $confirm_payment = get_value('lis_comfirm_payment', 'lis_id', 'lis_name', $row_combo['confirm_pay_id']);

                                // Query Reservations Combo Product items

                                $sql_product = "SELECT * ";
                                $sql_product .= "FROM reservationpackage_item ";
                                $sql_product .= "WHERE reservationpackages_id = '" . $row_combo['rpa_id'] . "' ";
                                $sql_product .= "ORDER BY rpt_id ASC ";

                                //echo $sql_product;

                                $result_product = mysql_query($sql_product);
                                while ($row_product = mysql_fetch_array($result_product)) {

                                    //$pax = "";
                                    //$pax = $row_product['rpt_adult_num'] + $row_product['rpt_child_num'];

                                    $rpt_item_travel_date = explode("~", $row_product['rpt_item_travel_date_arr']);
                                    $rpt_item_name = explode("~", $row_product['rpt_item_name_arr']);
                                    //$rpt_item_producttype_id = explode("~",$row_product['rpt_item_producttype_id_arr']);
                                    //$rpt_item_id = explode("~",$row_product['rpt_item_id_arr']);

                                    $count_item = "";
                                    $count_item = count($rpt_item_travel_date);
                                    $count_item = $count_item - 1;

                                    for ($x = 1; $x < $count_item; $x++) {

                                        // Voucher

                                        $sql_vo = "SELECT * ";
                                        $sql_vo .= "FROM voucher ";
                                        $sql_vo .= "WHERE vo_res_id = '" . $row_combo['rpa_id_str'] . "' ";
                                        $sql_vo .= "AND vo_item_id = $rpt_item_name[$x]' ";
                                        $sql_vo .= "AND vo_status = '2' ";
                                        $sql_vo .= "ORDER BY vo_id ASC ";

                                        //echo $sql_vo;

                                        $result_vo = mysql_query($sql_vo);
                                        $row_vo = mysql_fetch_array($result_vo);


                                        if ($reservation_id == $row_combo['rpa_id']) {
                                            $service_no++;

                                            if ($service_no == 1) {
                                                $product_prices = $row_product['rpt_prices'];
                                            } else {
                                                $product_prices = 0;
                                            }

                                        } else {
                                            $service_no = 1;

                                        }

                                        ?>

                                        <tr>
                                            <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?= $i ?></td>
                                            <td class="txt_bold_gray" bgcolor="#F0F0F0"><?= $agent_name ?></td>
                                            <td class="txt_bold_gray"
                                                bgcolor="#F0F0F0"><?= $lis_name ?> <?= $row_combo['rpa_fname'] ?> <?= $row_combo['rpa_lname'] ?></td>
                                            <td class="txt_bold_gray" align="center"
                                                bgcolor="#F0F0F0"><?= $row_combo['rpa_id_str'] ?></td>

                                            <? if ($agentpaytype_id != 2) { ?>
                                                <td class="txt_bold_gray" align="center"
                                                    bgcolor="#F0F0F0"><?= $row_vo['vo_no'] ?>
                                                    Service <?= $service_no ?></td>

                                            <? } else { ?>
                                                <td class="txt_bold_gray" align="center"
                                                    bgcolor="#F0F0F0"><?= $row_combo['rpa_agent_voucher'] ?></td>

                                            <? } ?>

                                            <td class="txt_bold_gray" align="center"
                                                bgcolor="#F0F0F0"><?= DateFormat($rpt_item_travel_date[$x], "s") ?></td>
                                            <td class="txt_bold_gray" bgcolor="#F0F0F0"><?= $rpt_item_name[$x] ?></td>
                                            <td class="txt_bold_gray" align="center"
                                                bgcolor="#F0F0F0"><?= $row_product['rpt_adult_num'] ?></td>
                                            <td class="txt_bold_gray" align="center"
                                                bgcolor="#F0F0F0"><?= $row_product['rpt_child_num'] ?></td>
                                            <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0">0</td>
                                            <td class="txt_bold_gray" align="center"
                                                bgcolor="#F0F0F0"><?= number_format($row_product['rpt_adult_prices'], 0) ?></td>
                                            <td class="txt_bold_gray" align="center"
                                                bgcolor="#F0F0F0"><?= number_format($row_product['rpt_child_prices'], 0) ?></td>
                                            <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0">0</td>
                                            <td class="txt_bold_gray" align="center"
                                                bgcolor="#F0F0F0"><?= number_format($product_prices, 0) ?></td>
                                            <td class="txt_bold_gray" align="center"
                                                bgcolor="#F0F0F0"><?= $confirm_payment ?></td>
                                        </tr>

                                        <?
                                        $i++;
                                        $total_price = $total_price + $product_prices;
                                        $pax_adult = $pax_adult + $row_product['rpt_adult_num'];
                                        $pax_child = $pax_child + $row_product['rpt_child_num'];
                                        $pax_room = $pax_room + 0;
                                        $total_adult = $total_adult + $row_product['rpt_adult_prices'];
                                        $total_child = $total_child + $row_product['rpt_child_prices'];
                                        $total_room = $total_room + 0;
                                        ?>

                                    <? } // END for($i=1;$i<count($rpt_item_travel_date);$i++){
                                    ?>

                                <? } // END while($row_product = mysql_fetch_array($result_product)){
                                ?>


                            <? } // END while($row_combo = mysql_fetch_array($result_combo)){	?>

                            <tr>
                                <td class="txt_bold_gray" colspan="7" align="right">TOTAL</td>
                                <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?= $pax_adult ?></td>
                                <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?= $pax_child ?></td>
                                <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?= $pax_room ?></td>
                                <!--<td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?= number_format($total_adult, 0) ?></td>
        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?= number_format($total_child, 0) ?></td>
        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?= number_format($total_room, 0) ?></td>-->
                                <td class="txt_bold_gray" colspan="3" align="center"></td>
                                <td class="txt_bold_gray" align="center"
                                    bgcolor="#F0F0F0"><?= number_format($total_price, 0) ?></td>
                                <td class="txt_bold_gray" align="center">&nbsp;</td>
                            </tr>

                        </table>

                        <!---- Search Box ---->
                    </td>
                    <td align="right" valign="bottom"></td>
                </tr>
            </table>

            <!---- Listing Body ---->
        </td>
    </tr>
</table>