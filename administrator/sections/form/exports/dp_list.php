<?php

function setNumberLength($num, $length)
{
    $sumstr = strlen($num);
    $zero = str_repeat("0", $length - $sumstr);
    $results = $zero . $num;

    return $results;
}

function DateDiff($strDate1, $strDate2)
{
    return (strtotime($strDate2) - strtotime($strDate1)) / (60 * 60 * 24);  // 1 day = 60*60*24
}

?>

<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td valign="top">
            <table width="100%" border="0" cellspacing="0" cellpadding="3">
                <tr>
                    <td class="txt_big_gray">DP List</td>
                    <td width="500" align="right"><a href="./index.php?mode=exports/dp_st1"
                                                     style="background-color:#ffffff; color:#000000"><<< Back</a></td>
                </tr>
            </table>
            <!---- Listing Body ---->
            <table width="100%" border="0" cellspacing="0" cellpadding="3">
                <tr>
                    <td>
                        <!---- Search Box ---->
                        <br/>

                        <table width="100%" align="center">
                            <tr>
                                <td class="txt_bold_gray" align="center">Confirm
                                    Date: <?= DateFormat($_POST['from_date'], "f") ?></td>

                            </tr>
                            <tr>
                                <td class="txt_bold_gray" align="right"><input type="button" name="export" id="export"
                                                                               value="Export .txt"
                                                                               onclick="window.location='process.php?mode=exports/dp_export_txt&cddate=<?= $_POST['from_date'] ?>'"/>
                                </td>

                            </tr>
                        </table>


                        <? // DP ?>

                        <br/>

                        <table width="100%" border="1" cellspacing="0" cellpadding="3" bgcolor="#FFFFFF"
                               bordercolor="#000000">
                            <tr bgcolor="#CCCCCC">
                                <td width="50" align="center" class="txt_bold_gray">Confirm Date</td>
                                <td width="100" align="center" class="txt_bold_gray">Agent Code</td>
                                <td width="100" align="center" class="txt_bold_gray">Agent Name</td>
                                <td width="50" align="center" class="txt_bold_gray">Payment Code</td>
                                <td width="100" align="center" class="txt_bold_gray">Payment Description</td>
                                <td width="100" align="center" class="txt_bold_gray">Amount</td>

                            </tr>

                            <?
                            // Query Agent

                            $sql_ag = "SELECT * ";
                            $sql_ag .= "FROM agents ";
                            $sql_ag .= "WHERE ag_id > 0 ";
                            $sql_ag .= "ORDER BY ag_name ASC ";

                            //echo $sql_ag;

                            $result_ag = mysql_query($sql_ag);
                            while ($row_ag = mysql_fetch_array($result_ag)) {

                                $total_amount = 0;
                                $total_amount1 = 0;
                                $total_amount2 = 0;
                                $total_amount3 = 0;
                                $total_amount4 = 0;
                                $total_amount_show = 0;

                                // Query Reservations

                                $sql = "SELECT * ";
                                $sql .= "FROM reservations ";
                                $sql .= "WHERE confirm_date = '" . $_POST['from_date'] . "' ";
                                $sql .= "AND bookingstatus_id = '3' ";
                                $sql .= "AND res_voucher_status = '1' ";
                                $sql .= "AND agents_id = '" . $row_ag['ag_id'] . "' ";
                                $sql .= "ORDER BY res_id ASC ";

                                //echo $sql;

                                $result = mysql_query($sql);
                                while ($row = mysql_fetch_array($result)) {

                                    $comfirm_payment = get_value('lis_comfirm_payment', 'lis_id', 'lis_name', $row['confirm_pay_id']);

                                    ?>

                                    <!--<tr>
                            <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?= $_POST['from_date'] ?></td>
                            <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?= $row_ag['ag_ref'] ?></td>
                            <td class="txt_bold_gray" align="left" bgcolor="#F0F0F0"><?= $row_ag['ag_name'] ?></td>
                            <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?= "P00" . $row['confirm_pay_id'] ?></td>
                            <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?= $comfirm_payment ?></td>
                            <td class="txt_bold_gray" align="right" bgcolor="#F0F0F0"><?= number_format($row['prices'], 2) ?></td>
                                
                        </tr>-->

                                    <?

                                    if ($row['confirm_pay_id'] == 1) {
                                        $total_amount1 = $total_amount1 + $row['prices'];

                                    } elseif ($row['confirm_pay_id'] == 2) {
                                        $total_amount2 = $total_amount2 + $row['prices'];

                                    } elseif ($row['confirm_pay_id'] == 3) {
                                        $total_amount3 = $total_amount3 + $row['prices'];

                                    } else {
                                        $total_amount4 = $total_amount4 + $row['prices'];

                                    }

                                    $total_amount = $total_amount + $row['prices'];

                                } // END while($row = mysql_fetch_array($result)){


                                // Query Reservations Combo Product

                                $sql_combo = "SELECT * ";
                                $sql_combo .= "FROM reservation_packages ";
                                $sql_combo .= "WHERE confirm_date = '" . $_POST['from_date'] . "' ";
                                $sql_combo .= "AND bookingstatus_id = '3' ";
                                $sql_combo .= "AND rpa_v_status = '1' ";
                                $sql_combo .= "AND agents_id = '" . $row_ag['ag_id'] . "' ";
                                $sql_combo .= "ORDER BY rpa_id ASC ";

                                //echo $sql_combo;

                                $result_combo = mysql_query($sql_combo);
                                while ($row_combo = mysql_fetch_array($result_combo)) {

                                    $comfirm_payment = get_value('lis_comfirm_payment', 'lis_id', 'lis_name', $row_combo['confirm_pay_id']);

                                    ?>

                                    <!--<tr>
                            <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?= $_POST['from_date'] ?></td>
                            <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?= $row_ag['ag_ref'] ?></td>
                            <td class="txt_bold_gray" align="left" bgcolor="#F0F0F0"><?= $row_ag['ag_name'] ?></td>
                            <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?= "P00" . $row_combo['confirm_pay_id'] ?></td>
                            <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?= $comfirm_payment ?></td>
                            <td class="txt_bold_gray" align="right" bgcolor="#F0F0F0"><?= number_format($row_combo['prices'], 2) ?></td>
                                
                        </tr>-->

                                    <?

                                    if ($row_combo['confirm_pay_id'] == 1) {
                                        $total_amount1 = $total_amount1 + $row_combo['prices'];

                                    } elseif ($row_combo['confirm_pay_id'] == 2) {
                                        $total_amount2 = $total_amount2 + $row_combo['prices'];

                                    } elseif ($row_combo['confirm_pay_id'] == 3) {
                                        $total_amount3 = $total_amount3 + $row_combo['prices'];

                                    } else {
                                        $total_amount4 = $total_amount4 + $row_combo['prices'];

                                    }

                                    $total_amount = $total_amount + $row_combo['prices'];

                                } // END while($row_combo = mysql_fetch_array($result_combo)){


                                if ($total_amount != 0) {

                                    $sql_pay = "SELECT * ";
                                    $sql_pay .= "FROM lis_comfirm_payment ";
                                    $sql_pay .= "WHERE lis_id > 0 ";
                                    $sql_pay .= "ORDER BY lis_id ASC ";

                                    $result_pay = mysql_query($sql_pay);
                                    while ($row_pay = mysql_fetch_array($result_pay)) {

                                        if ($row_pay['lis_id'] == 1) {
                                            $total_amount_show = $total_amount1;

                                        } elseif ($row_pay['lis_id'] == 2) {
                                            $total_amount_show = $total_amount2;

                                        } elseif ($row_pay['lis_id'] == 3) {
                                            $total_amount_show = $total_amount3;

                                        } else {
                                            $total_amount_show = $total_amount4;

                                        }
                                        ?>

                                        <!-- Confirm Payment -->
                                        <tr>
                                            <td class="txt_bold_gray" align="center"
                                                bgcolor="#F0F0F0"><?= $_POST['from_date'] ?></td>
                                            <td class="txt_bold_gray" align="center"
                                                bgcolor="#F0F0F0"><?= $row_ag['ag_ref'] ?></td>
                                            <td class="txt_bold_gray" align="left"
                                                bgcolor="#F0F0F0"><?= $row_ag['ag_name'] ?></td>
                                            <td class="txt_bold_gray" align="center"
                                                bgcolor="#F0F0F0"><?= "P00" . $row_pay['lis_id'] ?></td>
                                            <td class="txt_bold_gray" align="center"
                                                bgcolor="#F0F0F0"><?= $row_pay['lis_name'] ?></td>
                                            <td class="txt_bold_gray" align="right"
                                                bgcolor="#F0F0F0"><?= number_format($total_amount_show, 2) ?></td>

                                        </tr>
                                        <!-- Confirm Payment -->

                                        <?
                                    } // END while($row_pay = mysql_fetch_array($result_pay)){
                                    ?>
                                    <tr bgcolor="#CCCCCC">
                                        <td colspan="6" class="txt_bold_gray" align="center">&nbsp;</td>
                                    </tr>
                                    <?
                                } // END if($total_amount != 0){

                            } // END while($row_ag = mysql_fetch_array($result_ag)){

                            // END Query Agent


                            // Query Reservations B to C

                            $total_amount = 0;
                            $total_amount1 = 0;
                            $total_amount2 = 0;
                            $total_amount3 = 0;
                            $total_amount4 = 0;
                            $total_amount_show = 0;

                            $sql_btoc = "SELECT * ";
                            $sql_btoc .= "FROM reservations ";
                            $sql_btoc .= "WHERE confirm_date = '" . $_POST['from_date'] . "' ";
                            $sql_btoc .= "AND bookingstatus_id = '3' ";
                            $sql_btoc .= "AND res_voucher_status = '1' ";
                            $sql_btoc .= "AND agents_id = '0' ";
                            $sql_btoc .= "ORDER BY res_id ASC ";

                            //echo $sql;

                            $result_btoc = mysql_query($sql_btoc);
                            $num_rows_btoc = mysql_num_rows($result_btoc);
                            while ($row_btoc = mysql_fetch_array($result_btoc)) {

                                $comfirm_payment = get_value('lis_comfirm_payment', 'lis_id', 'lis_name', $row_btoc['confirm_pay_id']);

                                ?>
                                <!--<tr>
                            <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?= $_POST['from_date'] ?></td>
                            <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0">&nbsp;</td>
                            <td class="txt_bold_gray" align="left" bgcolor="#F0F0F0">B to C</td>
                            <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?= "P00" . $row_btoc['confirm_pay_id'] ?></td>
                            <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?= $comfirm_payment ?></td>
                            <td class="txt_bold_gray" align="right" bgcolor="#F0F0F0"><?= number_format($row_btoc['prices'], 2) ?></td>
                                
                        </tr>-->
                                <?

                                if ($row_btoc['confirm_pay_id'] == 1) {
                                    $total_amount1 = $total_amount1 + $row_btoc['prices'];

                                } elseif ($row_btoc['confirm_pay_id'] == 2) {
                                    $total_amount2 = $total_amount2 + $row_btoc['prices'];

                                } elseif ($row_btoc['confirm_pay_id'] == 3) {
                                    $total_amount3 = $total_amount3 + $row_btoc['prices'];

                                } else {
                                    $total_amount4 = $total_amount4 + $row_btoc['prices'];

                                }

                            } // END while($row_btoc = mysql_fetch_array($result_btoc)){


                            // Query Reservations B to C Combo Product

                            $sql_combo = "SELECT * ";
                            $sql_combo .= "FROM reservation_packages ";
                            $sql_combo .= "WHERE confirm_date = '" . $_POST['from_date'] . "' ";
                            $sql_combo .= "AND bookingstatus_id = '3' ";
                            $sql_combo .= "AND rpa_v_status = '1' ";
                            $sql_combo .= "AND agents_id = '0' ";
                            $sql_combo .= "ORDER BY rpa_id ASC ";

                            //echo $sql_combo;

                            $result_combo = mysql_query($sql_combo);
                            $num_rows_combo = mysql_num_rows($result_combo);
                            while ($row_combo = mysql_fetch_array($result_combo)) {

                                $comfirm_payment = get_value('lis_comfirm_payment', 'lis_id', 'lis_name', $row_combo['confirm_pay_id']);

                                ?>

                                <!--<tr>
                            <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?= $_POST['from_date'] ?></td>
                            <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?= $row_combo['rpa_id'] ?></td>
                            <td class="txt_bold_gray" align="left" bgcolor="#F0F0F0">B to C</td>
                            <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?= "P00" . $row_combo['confirm_pay_id'] ?></td>
                            <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?= $comfirm_payment ?></td>
                            <td class="txt_bold_gray" align="right" bgcolor="#F0F0F0"><?= number_format($row_combo['prices'], 2) ?></td>
                                
                        </tr>-->

                                <?

                                if ($row_combo['confirm_pay_id'] == 1) {
                                    $total_amount1 = $total_amount1 + $row_combo['prices'];

                                } elseif ($row_combo['confirm_pay_id'] == 2) {
                                    $total_amount2 = $total_amount2 + $row_combo['prices'];

                                } elseif ($row_combo['confirm_pay_id'] == 3) {
                                    $total_amount3 = $total_amount3 + $row_combo['prices'];

                                } else {
                                    $total_amount4 = $total_amount4 + $row_combo['prices'];

                                }

                                $total_amount = $total_amount + $row_combo['prices'];

                            } // END while($row_combo = mysql_fetch_array($result_combo)){


                            if ($num_rows_btoc != 0 && $num_rows_combo != 0) {

                                $sql_pay = "SELECT * ";
                                $sql_pay .= "FROM lis_comfirm_payment ";
                                $sql_pay .= "WHERE lis_id > 0 ";
                                $sql_pay .= "ORDER BY lis_id ASC ";

                                $result_pay = mysql_query($sql_pay);
                                while ($row_pay = mysql_fetch_array($result_pay)) {

                                    if ($row_pay['lis_id'] == 1) {
                                        $total_amount_show = $total_amount1;

                                    } elseif ($row_pay['lis_id'] == 2) {
                                        $total_amount_show = $total_amount2;

                                    } elseif ($row_pay['lis_id'] == 3) {
                                        $total_amount_show = $total_amount3;

                                    } else {
                                        $total_amount_show = $total_amount4;

                                    }
                                    ?>
                                    <!-- Confirm Payment -->
                                    <tr>
                                        <td class="txt_bold_gray" align="center"
                                            bgcolor="#F0F0F0"><?= $_POST['from_date'] ?></td>
                                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0">&nbsp;</td>
                                        <td class="txt_bold_gray" align="left" bgcolor="#F0F0F0">B to C</td>
                                        <td class="txt_bold_gray" align="center"
                                            bgcolor="#F0F0F0"><?= "P00" . $row_pay['lis_id'] ?></td>
                                        <td class="txt_bold_gray" align="center"
                                            bgcolor="#F0F0F0"><?= $row_pay['lis_name'] ?></td>
                                        <td class="txt_bold_gray" align="right"
                                            bgcolor="#F0F0F0"><?= number_format($total_amount_show, 2) ?></td>

                                    </tr>
                                    <!-- Confirm Payment -->

                                    <?
                                } // END while($row_pay = mysql_fetch_array($result_pay)){

                            } // END if($num_rows_btoc != 0 && $num_rows_combo != 0){
                            ?>

                        </table>

                        <!---- Search Box ---->
                    </td>
                    <td align="right" valign="bottom"></td>
                </tr>
            </table>

            <!---- Listing Body ---->
        </td>
    </tr>
</table>