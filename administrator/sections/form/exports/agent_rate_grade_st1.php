<?php

function setNumberLength($num, $length)
{
    $sumstr = strlen($num);
    $zero = str_repeat("0", $length - $sumstr);
    $results = $zero . $num;

    return $results;
}

?>

<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td valign="top">
            <table width="100%" border="0" cellspacing="0" cellpadding="3">
                <tr>
                    <td class="txt_big_gray">Agent Rate Report (By Grade)</td>
                    <!--<td width="500" align="right"><a href="./index.php?mode=reports/customer_confirm_st1" style="background-color:#ffffff; color:#000000"><<< Back</a></td>-->
                </tr>
            </table>
            <!---- Listing Body ---->
            <table width="100%" border="0" cellspacing="0" cellpadding="3">
                <tr>
                    <td>
                        <!---- Search Box ---->

                        <table border="0" cellspacing="3" cellpadding="3" style="border: solid #CCCCCC 1px;">
                            <form name="formSt1" method="post" action="./index.php?mode=exports/agent_rate_grade_st2">

                                <tr>
                                    <td class="txt_bold_gray" align="right">Product Type :</td>
                                    <td align="left">
                                        <select name="product_type_id">
                                            <option value="">-- Select --</option>
                                            <?php
                                            $sql_product_type = "select * from lis_reserv_producttype where lis_id > 0";
                                            $result_product_type = mysql_query($sql_product_type);

                                            while ($row_product_type = mysql_fetch_array($result_product_type)) {

                                                ?>
                                                <option
                                                    value="<?= $row_product_type[0] ?>"><?= $row_product_type['lis_name'] ?></option>
                                                <?php
                                            }

                                            ?>
                                            <option value="99">Combo Product</option>
                                        </select>
                                    </td>
                                </tr>

                                <!--<tr>
		<td class="txt_bold_gray" align="right">Travel Date :</td>
		<td align="left"><script>DateInput('travel_date', true, 'yyyy-mm-dd' <? if ($travel_date) {
                                    echo ",'$travel_date'";
                                } ?>)</script></td>
	</tr>-->

                                <tr>
                                    <td class="txt_bold_gray" align="right">&nbsp;</td>
                                    <td align="right"><input type="submit" name="Submit" value="SEARCH"
                                                             style="height:25px;"></td>
                                </tr>
                            </form>
                        </table>
                        <!---- Search Box ---->
                    </td>
                    <td align="right" valign="bottom"></td>
                </tr>
            </table>

            <!---- Listing Body ---->
        </td>
    </tr>
</table>