<?php

function setNumberLength($num, $length)
{
    $sumstr = strlen($num);
    $zero = str_repeat("0", $length - $sumstr);
    $results = $zero . $num;

    return $results;
}

function DateDiff($strDate1, $strDate2)
{
    return (strtotime($strDate2) - strtotime($strDate1)) / (60 * 60 * 24);  // 1 day = 60*60*24
}

?>

<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td valign="top">
            <table width="100%" border="0" cellspacing="0" cellpadding="3">
                <tr>
                    <td class="txt_big_gray">DL List</td>
                    <td width="500" align="right"><a href="./index.php?mode=exports/dl_st1"
                                                     style="background-color:#ffffff; color:#000000"><<< Back</a></td>
                </tr>
            </table>
            <!---- Listing Body ---->
            <table width="100%" border="0" cellspacing="0" cellpadding="3">
                <tr>
                    <td>
                        <!---- Search Box ---->
                        <br/>

                        <? //$strNewDate = date("Y-m-d", strtotime("+3 day", strtotime($_POST[to_date]))); ?>

                        <table align="center">
                            <tr>
                                <td class="txt_bold_gray" align="center">
                                    Date: <?= DateFormat($_POST[from_date], "f") ?></td>

                            </tr>
                        </table>


                        <? // TR ?>

                        <br/>

                        <table border="1" cellspacing="0" cellpadding="3" bgcolor="#FFFFFF" bordercolor="#000000">
                            <tr bgcolor="#CCCCCC">
                                <td width="100" align="center" class="txt_bold_gray">DATE</td>
                                <td width="100" align="center" class="txt_bold_gray">AGENT CODE/PAYMENT CODE</td>
                                <td width="200" align="center" class="txt_bold_gray">PRODUCT DESCRIPTION/PAYMENT
                                    DESCRIPTION
                                </td>
                                <td width="200" align="center" class="txt_bold_gray">P/S (POSTING/PAYMENT)</td>
                                <td width="50" align="center" class="txt_bold_gray">QTY</td>
                                <td width="100" align="center" class="txt_bold_gray">GROSS</td>
                                <td width="100" align="center" class="txt_bold_gray">VOID</td>
                                <td width="100" align="center" class="txt_bold_gray">NET</td>

                            </tr>

                            <? // Query Reservations

                            $sql = "SELECT * ";
                            $sql .= "FROM reservations ";
                            $sql .= "WHERE res_date = '$_POST[from_date]' ";
                            $sql .= "AND bookingstatus_id = '3' ";
                            $sql .= "AND res_voucher_status = '1' ";
                            $sql .= "ORDER BY res_id ASC ";

                            //echo $sql;
                            //exit();

                            $result = mysql_query($sql);
                            while ($row = mysql_fetch_array($result)) {

                                // Get Value

                                $code_agent = get_value(agents, ag_id, ag_ref, $row[agents_id]);
                                $agent_name = get_value(agents, ag_id, ag_name, $row[agents_id]);

                                $lis_name = get_value(lis_titlename, lis_id, lis_name, $row[titlename_id]);

                                // Query Reservations Boat Transfer

                                $sql_boat = "SELECT * ";
                                $sql_boat .= "FROM reservation_boattransfer_items ";
                                $sql_boat .= "WHERE reservations_id = '$row[res_id]' ";
                                $sql_boat .= "ORDER BY rbt_id ASC ";

                                $result_boat = mysql_query($sql_boat);
                                while ($row_boat = mysql_fetch_array($result_boat)) {

                                    $pax = "";
                                    $pax = $row_boat[rbt_adult_num] + $row_boat[rbt_child_num];

                                    $account_code_andaman = get_value(boattransfers, bot_id, account_code_andaman, $row_boat[boattransfers_id]);
                                    $account_code_phuket = get_value(boattransfers, bot_id, account_code_phuket, $row_boat[boattransfers_id]);

                                    // Voucher No.

                                    $sql_vo = "SELECT * ";
                                    $sql_vo .= "FROM voucher ";
                                    $sql_vo .= "WHERE vo_res_id = '$row[res_id_str]' ";
                                    $sql_vo .= "AND vo_status = '2' ";
                                    $sql_vo .= "AND vo_item_id = '$row_boat[rbt_name]' ";
                                    $sql_vo .= "ORDER BY vo_id ASC ";

                                    //echo $sql_vo;
                                    //exit();

                                    $result_vo = mysql_query($sql_vo);
                                    $row_vo = mysql_fetch_array($result_vo);

                                    // END Voucher No.
                                    ?>

                                    <tr>
                                        <td class="txt_bold_gray" align="center"
                                            bgcolor="#F0F0F0"><?= DateFormat($row[res_date], "s") ?></td>
                                        <td class="txt_bold_gray" align="center"
                                            bgcolor="#F0F0F0"><?= $code_agent ?></td>
                                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0">&nbsp;</td>
                                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0">&nbsp;</td>
                                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?= $pax ?></td>
                                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0">&nbsp;</td>
                                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0">&nbsp;</td>
                                        <td class="txt_bold_gray" align="right"
                                            bgcolor="#F0F0F0"><?= $row_boat[rbt_prices] ?></td>

                                    </tr>

                                <? } // END while($row_boat = mysql_fetch_array($result_boat)){
                                ?>


                                <?
                                // Query Reservations Pick up Transfer

                                $sql_pickup = "SELECT * ";
                                $sql_pickup .= "FROM reservation_pickuptransfer_items ";
                                $sql_pickup .= "WHERE reservations_id = '$row[res_id]' ";
                                $sql_pickup .= "ORDER BY rpt_id ASC ";

                                $result_pickup = mysql_query($sql_pickup);
                                while ($row_pickup = mysql_fetch_array($result_pickup)) {

                                    $pax = "";
                                    $pax = $row_pickup[rpt_adult_num] + $row_pickup[rpt_child_num];

                                    $account_code_andaman = get_value(pickuptransfers, put_id, account_code_andaman, $row_pickup[pickuptransfers_id]);
                                    $account_code_phuket = get_value(pickuptransfers, put_id, account_code_phuket, $row_pickup[pickuptransfers_id]);

                                    // Voucher No.

                                    $sql_vo = "SELECT * ";
                                    $sql_vo .= "FROM voucher ";
                                    $sql_vo .= "WHERE vo_res_id = '$row[res_id_str]' ";
                                    $sql_vo .= "AND vo_status = '2' ";
                                    $sql_vo .= "AND vo_item_id = '$row_pickup[rpt_name]' ";
                                    $sql_vo .= "ORDER BY vo_id ASC ";

                                    //echo $sql_vo;
                                    //exit();

                                    $result_vo = mysql_query($sql_vo);
                                    $row_vo = mysql_fetch_array($result_vo);

                                    // END Voucher No.
                                    ?>

                                    <tr>
                                        <td class="txt_bold_gray" align="center"
                                            bgcolor="#F0F0F0"><?= DateFormat($row[res_date], "s") ?></td>
                                        <td class="txt_bold_gray" align="center"
                                            bgcolor="#F0F0F0"><?= $code_agent ?></td>
                                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0">&nbsp;</td>
                                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0">&nbsp;</td>
                                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?= $pax ?></td>
                                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0">&nbsp;</td>
                                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0">&nbsp;</td>
                                        <td class="txt_bold_gray" align="right"
                                            bgcolor="#F0F0F0"><?= $row_pickup[rpt_prices] ?></td>

                                    </tr>

                                <? } // END while($row_pickup = mysql_fetch_array($result_pickup)){
                                ?>


                                <?
                                // Query Reservations Tour

                                $sql_tour = "SELECT * ";
                                $sql_tour .= "FROM reservation_tour_items ";
                                $sql_tour .= "WHERE reservations_id = '$row[res_id]' ";
                                $sql_tour .= "ORDER BY rtt_id ASC ";

                                $result_tour = mysql_query($sql_tour);
                                while ($row_tour = mysql_fetch_array($result_tour)) {

                                    $pax = "";
                                    $pax = $row_tour[rtt_adult_num] + $row_tour[rtt_child_num];

                                    $account_code_andaman = get_value(tours, tou_id, account_code_andaman, $row_tour[tours_id]);
                                    $account_code_phuket = get_value(tours, tou_id, account_code_phuket, $row_tour[tours_id]);

                                    // Voucher No.

                                    $sql_vo = "SELECT * ";
                                    $sql_vo .= "FROM voucher ";
                                    $sql_vo .= "WHERE vo_res_id = '$row[res_id_str]' ";
                                    $sql_vo .= "AND vo_status = '2' ";
                                    $sql_vo .= "AND vo_item_id = '$row_tour[rtt_name]' ";
                                    $sql_vo .= "ORDER BY vo_id ASC ";

                                    //echo $sql_vo;
                                    //exit();

                                    $result_vo = mysql_query($sql_vo);
                                    $row_vo = mysql_fetch_array($result_vo);

                                    // END Voucher No.
                                    ?>

                                    <tr>
                                        <td class="txt_bold_gray" align="center"
                                            bgcolor="#F0F0F0"><?= DateFormat($row[res_date], "s") ?></td>
                                        <td class="txt_bold_gray" align="center"
                                            bgcolor="#F0F0F0"><?= $code_agent ?></td>
                                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0">&nbsp;</td>
                                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0">&nbsp;</td>
                                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?= $pax ?></td>
                                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0">&nbsp;</td>
                                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0">&nbsp;</td>
                                        <td class="txt_bold_gray" align="right"
                                            bgcolor="#F0F0F0"><?= $row_tour[rtt_prices] ?></td>

                                    </tr>

                                <? } // END while($row_tour = mysql_fetch_array($result_tour)){
                                ?>


                                <?
                                // Query Reservations Activity

                                $sql_activity = "SELECT * ";
                                $sql_activity .= "FROM reservation_activity_items ";
                                $sql_activity .= "WHERE reservations_id = '$row[res_id]' ";
                                $sql_activity .= "ORDER BY rat_id ASC ";

                                $result_activity = mysql_query($sql_activity);
                                while ($row_activity = mysql_fetch_array($result_activity)) {

                                    $pax = "";
                                    $pax = $row_activity[rat_adult_num] + $row_activity[rat_child_num];

                                    $account_code_andaman = get_value(activities, act_id, account_code_andaman, $row_activity[activities_id]);
                                    $account_code_phuket = get_value(activities, act_id, account_code_phuket, $row_activity[activities_id]);

                                    // Voucher No.

                                    $sql_vo = "SELECT * ";
                                    $sql_vo .= "FROM voucher ";
                                    $sql_vo .= "WHERE vo_res_id = '$row[res_id_str]' ";
                                    $sql_vo .= "AND vo_status = '2' ";
                                    $sql_vo .= "AND vo_item_id = '$row_activity[rat_name]' ";
                                    $sql_vo .= "ORDER BY vo_id ASC ";

                                    //echo $sql_vo;
                                    //exit();

                                    $result_vo = mysql_query($sql_vo);
                                    $row_vo = mysql_fetch_array($result_vo);

                                    // END Voucher No.
                                    ?>

                                    <tr>
                                        <td class="txt_bold_gray" align="center"
                                            bgcolor="#F0F0F0"><?= DateFormat($row[res_date], "s") ?></td>
                                        <td class="txt_bold_gray" align="center"
                                            bgcolor="#F0F0F0"><?= $code_agent ?></td>
                                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0">&nbsp;</td>
                                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0">&nbsp;</td>
                                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?= $pax ?></td>
                                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0">&nbsp;</td>
                                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0">&nbsp;</td>
                                        <td class="txt_bold_gray" align="right"
                                            bgcolor="#F0F0F0"><?= $row_activity[rat_prices] ?></td>

                                    </tr>

                                <? } // END while($row_activity = mysql_fetch_array($result_activity)){
                                ?>


                                <?
                                // Query Reservations Bus Transfer

                                $sql_bus = "SELECT * ";
                                $sql_bus .= "FROM reservation_bustransfer_items ";
                                $sql_bus .= "WHERE reservations_id = '$row[res_id]' ";
                                $sql_bus .= "ORDER BY rct_id ASC ";

                                $result_bus = mysql_query($sql_bus);
                                while ($row_bus = mysql_fetch_array($result_bus)) {

                                    $pax = "";
                                    $pax = $row_bus[rct_adult_num] + $row_bus[rct_child_num];

                                    $account_code_andaman = get_value(cartransfers, ct_id, account_code_andaman, $row_bus[bustransfers_id]);
                                    $account_code_phuket = get_value(cartransfers, ct_id, account_code_phuket, $row_bus[bustransfers_id]);

                                    // Voucher No.

                                    $sql_vo = "SELECT * ";
                                    $sql_vo .= "FROM voucher ";
                                    $sql_vo .= "WHERE vo_res_id = '$row[res_id_str]' ";
                                    $sql_vo .= "AND vo_status = '2' ";
                                    $sql_vo .= "AND vo_item_id = '$row_bus[rct_name]' ";
                                    $sql_vo .= "ORDER BY vo_id ASC ";

                                    //echo $sql_vo;
                                    //exit();

                                    $result_vo = mysql_query($sql_vo);
                                    $row_vo = mysql_fetch_array($result_vo);

                                    // END Voucher No.
                                    ?>

                                    <tr>
                                        <td class="txt_bold_gray" align="center"
                                            bgcolor="#F0F0F0"><?= DateFormat($row[res_date], "s") ?></td>
                                        <td class="txt_bold_gray" align="center"
                                            bgcolor="#F0F0F0"><?= $code_agent ?></td>
                                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0">&nbsp;</td>
                                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0">&nbsp;</td>
                                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?= $pax ?></td>
                                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0">&nbsp;</td>
                                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0">&nbsp;</td>
                                        <td class="txt_bold_gray" align="right"
                                            bgcolor="#F0F0F0"><?= $row_bus[rct_prices] ?></td>

                                    </tr>

                                <? } // END while($row_bus = mysql_fetch_array($result_bus)){
                                ?>


                                <?
                                // Query Reservations Private Land Transfer

                                $sql_private = "SELECT * ";
                                $sql_private .= "FROM reservation_privatelandtransfer_items ";
                                $sql_private .= "WHERE reservations_id = '$row[res_id]' ";
                                $sql_private .= "ORDER BY rplt_id ASC ";

                                $result_private = mysql_query($sql_private);
                                while ($row_private = mysql_fetch_array($result_private)) {

                                    $pax = "";
                                    $pax = $row_private[rplt_adult_num] + $row_private[rplt_child_num];

                                    $account_code_andaman = get_value(privatelandtransfers, plt_id, account_code_andaman, $row_private[privatelandtransfers_id]);
                                    $account_code_phuket = get_value(privatelandtransfers, plt_id, account_code_phuket, $row_private[privatelandtransfers_id]);

                                    // Voucher No.

                                    $sql_vo = "SELECT * ";
                                    $sql_vo .= "FROM voucher ";
                                    $sql_vo .= "WHERE vo_res_id = '$row[res_id_str]' ";
                                    $sql_vo .= "AND vo_status = '2' ";
                                    $sql_vo .= "AND vo_item_id = '$row_private[rplt_name]' ";
                                    $sql_vo .= "ORDER BY vo_id ASC ";

                                    //echo $sql_vo;
                                    //exit();

                                    $result_vo = mysql_query($sql_vo);
                                    $row_vo = mysql_fetch_array($result_vo);

                                    // END Voucher No.
                                    ?>

                                    <tr>
                                        <td class="txt_bold_gray" align="center"
                                            bgcolor="#F0F0F0"><?= DateFormat($row[res_date], "s") ?></td>
                                        <td class="txt_bold_gray" align="center"
                                            bgcolor="#F0F0F0"><?= $code_agent ?></td>
                                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0">&nbsp;</td>
                                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0">&nbsp;</td>
                                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?= $pax ?></td>
                                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0">&nbsp;</td>
                                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0">&nbsp;</td>
                                        <td class="txt_bold_gray" align="right"
                                            bgcolor="#F0F0F0"><?= $row_private[rplt_prices] ?></td>

                                    </tr>

                                <? } // END while($row_private = mysql_fetch_array($result_private)){
                                ?>


                                <?
                                // Query Reservations Hotel

                                $sql_hotel = "SELECT * ";
                                $sql_hotel .= "FROM reservation_hotel_items ";
                                $sql_hotel .= "WHERE reservations_id = '$row[res_id]' ";
                                $sql_hotel .= "ORDER BY rht_id ASC ";

                                $result_hotel = mysql_query($sql_hotel);
                                while ($row_hotel = mysql_fetch_array($result_hotel)) {

                                    $pax = "";
                                    //$pax = $row_hotel[rht_adult_num] + $row_hotel[rht_child_num];
                                    $pax = $row_hotel[rht_room_num] * 1;

                                    $account_code_andaman = get_value(hotels, hot_id, account_code_andaman, $row_hotel[hotels_id]);
                                    $account_code_phuket = get_value(hotels, hot_id, account_code_phuket, $row_hotel[hotels_id]);

                                    // Voucher No.

                                    $sql_vo = "SELECT * ";
                                    $sql_vo .= "FROM voucher ";
                                    $sql_vo .= "WHERE vo_res_id = '$row[res_id_str]' ";
                                    $sql_vo .= "AND vo_status = '2' ";
                                    $sql_vo .= "AND vo_item_id = '$row_hotel[rht_name]' ";
                                    $sql_vo .= "ORDER BY vo_id ASC ";

                                    //echo $sql_vo;
                                    //exit();

                                    $result_vo = mysql_query($sql_vo);
                                    $row_vo = mysql_fetch_array($result_vo);

                                    // END Voucher No.
                                    ?>

                                    <tr>
                                        <td class="txt_bold_gray" align="center"
                                            bgcolor="#F0F0F0"><?= DateFormat($row[res_date], "s") ?></td>
                                        <td class="txt_bold_gray" align="center"
                                            bgcolor="#F0F0F0"><?= $code_agent ?></td>
                                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0">&nbsp;</td>
                                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0">&nbsp;</td>
                                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?= $pax ?></td>
                                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0">&nbsp;</td>
                                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0">&nbsp;</td>
                                        <td class="txt_bold_gray" align="right"
                                            bgcolor="#F0F0F0"><?= $row_hotel[rht_prices] ?></td>

                                    </tr>

                                <? } // END while($row_hotel = mysql_fetch_array($result_hotel)){
                                ?>


                            <? } // END while($row = mysql_fetch_array($result)){	?>





                            <? // Query Reservations Combo Product

                            $sql_combo = "SELECT * ";
                            $sql_combo .= "FROM reservation_packages ";
                            $sql_combo .= "WHERE rpa_date = '$_POST[from_date]' ";
                            $sql_combo .= "AND bookingstatus_id = '3' ";
                            $sql_combo .= "AND rpa_v_status = '1' ";
                            $sql_combo .= "ORDER BY rpa_id ASC ";

                            //echo $sql_combo;

                            $result_combo = mysql_query($sql_combo);
                            while ($row_combo = mysql_fetch_array($result_combo)) {

                                // Get Value

                                $code_agent = get_value(agents, ag_id, ag_ref, $row_combo[agents_id]);
                                $agent_name = get_value(agents, ag_id, ag_name, $row_combo[agents_id]);

                                $lis_name = get_value(lis_titlename, lis_id, lis_name, $row_combo[titlename_id]);

                                // Query Reservations Combo Product items

                                $sql_product = "SELECT * ";
                                $sql_product .= "FROM reservationpackage_item ";
                                $sql_product .= "WHERE reservationpackages_id = '$row_combo[rpa_id]' ";
                                $sql_product .= "ORDER BY rpt_id ASC ";

                                //echo $sql_product;

                                $result_product = mysql_query($sql_product);
                                while ($row_product = mysql_fetch_array($result_product)) {

                                    $pax = "";
                                    $pax = $row_product[rpt_adult_num] + $row_product[rpt_child_num];

                                    $account_code_andaman = get_value(packages, pac_id, account_code_andaman, $row_product[packages_id]);
                                    $account_code_phuket = get_value(packages, pac_id, account_code_phuket, $row_product[packages_id]);

                                    $rpt_item_travel_date = explode("~", $row_product[rpt_item_travel_date_arr]);
                                    $rpt_item_name = explode("~", $row_product[rpt_item_name_arr]);
                                    //$rpt_item_producttype_id = explode("~",$row_product[rpt_item_producttype_id_arr]);
                                    //$rpt_item_id = explode("~",$row_product[rpt_item_id_arr]);

                                    $count_item = "";
                                    $count_item = count($rpt_item_travel_date);
                                    $count_item = $count_item - 1;

                                    for ($i = 1; $i < $count_item; $i++) {

                                        // Voucher No.

                                        $sql_vo = "SELECT * ";
                                        $sql_vo .= "FROM voucher ";
                                        $sql_vo .= "WHERE vo_res_id = '$row_combo[rpa_id_str]' ";
                                        $sql_vo .= "AND vo_status = '2' ";
                                        $sql_vo .= "AND vo_item_id = '$rpt_item_name[$i]' ";
                                        $sql_vo .= "ORDER BY vo_id ASC ";

                                        //echo $sql_vo;
                                        //exit();

                                        $result_vo = mysql_query($sql_vo);
                                        $row_vo = mysql_fetch_array($result_vo);

                                        if ($i > 1) {
                                            $voucher_no = $voucher_no . ", " . $row_vo[vo_no];

                                        } else {
                                            $voucher_no = $row_vo[vo_no];

                                        }

                                        // END Voucher No.
                                        ?>

                                    <? } // END for($i=1;$i<count($rpt_item_travel_date);$i++){
                                    ?>

                                    <tr>
                                        <td class="txt_bold_gray" align="center"
                                            bgcolor="#F0F0F0"><?= DateFormat($row_combo[rpa_date], "s") ?></td>
                                        <td class="txt_bold_gray" align="center"
                                            bgcolor="#F0F0F0"><?= $code_agent ?></td>
                                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0">&nbsp;</td>
                                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0">&nbsp;</td>
                                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?= $pax ?></td>
                                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0">&nbsp;</td>
                                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0">&nbsp;</td>
                                        <td class="txt_bold_gray" align="right"
                                            bgcolor="#F0F0F0"><?= $row_product[rpt_prices] ?></td>

                                    </tr>

                                <? } // END while($row_product = mysql_fetch_array($result_product)){
                                ?>


                            <? } // END while($row_combo = mysql_fetch_array($result_combo)){	?>


                        </table>

                        <!---- Search Box ---->
                    </td>
                    <td align="right" valign="bottom"></td>
                </tr>
            </table>

            <!---- Listing Body ---->
        </td>
    </tr>
</table>