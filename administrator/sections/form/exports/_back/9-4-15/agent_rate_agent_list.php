<?php

function setNumberLength($num, $length)
{
    $sumstr = strlen($num);
    $zero = str_repeat("0", $length - $sumstr);
    $results = $zero . $num;

    return $results;
}

if ($_POST[product_type_id]) {
    $product_type_id_link = $_POST[product_type_id];
    $product_id_link = $_POST[product_id];
    $agent_id_link = $_POST[agent_id];

} else {
    $product_type_id_link = $product_type_id;
    $product_id_link = $product_id;
    $agent_id_link = $agent_id;

}

?>

    <table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
    <td valign="top">

    <table width="100%" border="0" cellspacing="0" cellpadding="3">
        <tr>
            <td class="txt_big_gray">Agent Rate Report (By Agent) List</td>
            <td width="500" align="right"><a href="./index.php?mode=exports/agent_rate_agent_st1"
                                             style="background-color:#ffffff; color:#000000"><<< Back</a></td>
        </tr>
    </table>


<? if ($product_type_id_link != "99") { //Not Combo Product ?>


    <?
    if ($product_id && $agent_id_link) {
        $sql_package_producttype = "select * from lis_package_producttype where lis_id = $product_type_id_link ";
        $result_package_producttype = mysql_query($sql_package_producttype);

        $row_package_producttype = mysql_fetch_array($result_package_producttype);

        $item_table = "$row_package_producttype[item_table]";
        $item_id = "$row_package_producttype[item_id]";
        $item_name = "$row_package_producttype[item_name]";

        if ($product_type_id_link == "1") {
            $pricetype_id = get_value($item_table, $item_id, boattransferpricetype_id, $product_id_link);

        } else if ($product_type_id_link == "2") {
            $pricetype_id = get_value($item_table, $item_id, pickuptransfer_price_type_id, $product_id_link);

        } else if ($product_type_id_link == "3") {
            $pricetype_id = get_value($item_table, $item_id, tourpricetype_id, $product_id_link);

        } else if ($product_type_id_link == "4") {
            $pricetype_id = get_value($item_table, $item_id, activitypricetype_id, $product_id_link);

        } else if ($product_type_id_link == "5") {
            $pricetype_id = get_value($item_table, $item_id, cartransferpricetype_id, $product_id_link);

        } else if ($product_type_id_link == "6") {
            $pricetype_id = get_value($item_table, $item_id, privatelandtransfer_price_type_id, $product_id_link);

        } else {
            $pricetype_id = get_value($item_table, $item_id, hotelpricetype_id, $product_id_link);

        }

    } // END if($product_id && $grade_id){
    ?>

    <br/>

    <?
    $i = 1;
    //$grade_id_link = 1;

    $sql = "select * from agents where ag_id = $agent_id_link";
    $result = mysql_query($sql);
    $row = mysql_fetch_array($result);

    ?>

    <table align="center">
        <tr>
            <td class="txt_bold_gray" align="center">
                Product Name : <?= get_value($item_table, $item_id, $item_name, $product_id_link); ?>
                &nbsp;&nbsp;
                Agent Name : <?= get_value(agents, ag_id, ag_name, $agent_id_link); ?>
                &nbsp;&nbsp;
                Company : <? if ($row[agentfor_id] == "1") {
                    echo "WAVE";
                } else {
                    echo "DOT COM";
                } ?>

            </td>

        </tr>
    </table>

    <br/>

    <? // Table 1 ?>

    <table border="1" cellspacing="0" cellpadding="3" bgcolor="#FFFFFF" bordercolor="#000000">
        <tr bgcolor="#CCCCCC">
            <td width="30" rowspan="2" align="center" class="txt_bold_gray">No.</td>
            <td width="500" rowspan="2" align="center" class="txt_bold_gray">Rate Type</td>
            <!--<td width="100" colspan="2" align="center" class="txt_bold_gray">Company</td>
            <td width="200" colspan="4" align="center" class="txt_bold_gray">Internet Rate</td>-->

            <?
            $sql_grade = "select * from agent_grade where a_id <= 5";
            $result_grade = mysql_query($sql_grade);

            while ($row_grade = mysql_fetch_array($result_grade)) {
                ?>
                <td width="200" colspan="4" align="center" class="txt_bold_gray"><?= $row_grade[a_name] ?></td>

            <? } // END while($row_grade = mysql_fetch_array($result_grade)) ?>

        </tr>

        <tr bgcolor="#CCCCCC">
            <!--<td width="50" align="center" class="txt_bold_gray">WAVE</td>
            <td width="50" align="center" class="txt_bold_gray">DOT COM</td>-->

            <? for ($n = 1; $n <= 5; $n++) { ?>
                <td width="50" align="center" class="txt_bold_gray">Adult</td>
                <td width="50" align="center" class="txt_bold_gray">Child</td>
                <td width="50" align="center" class="txt_bold_gray">Car (Private)</td>
                <td width="50" align="center" class="txt_bold_gray">Room</td>
            <? } ?>

        </tr>


        <? if ($product_type_id_link == "1") { // Boat Transfer

            $ratetype = "select * from boattransfer_ratetypes where boattransfers_id = $product_id_link order by boattransfers_id ASC";
            $ratetyperatetype = mysql_query($ratetype);

            //echo $ratetype;

            while ($ratetype = mysql_fetch_array($ratetyperatetype)) {
                $boattransferperiod_status = "";
                $boattransfer_period = "select * from boattransfer_period where boattransfers_id = $product_id_link
				and botpe_datefrom != '0000-00-00' and botpe_dateto != '0000-00-00' order by botpe_datefrom ASC";
                $boattransfer_periodboattransfer_period = mysql_query($boattransfer_period);
                while ($boattransfer_period = mysql_fetch_array($boattransfer_periodboattransfer_period)) {
                    ?>

                    <tr>
                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?= $i ?></td>
                        <td class="txt_bold_gray" align="left" bgcolor="#F0F0F0"><?= $ratetype[botrt_name] ?>
                            <?= DateFormat($boattransfer_period[botpe_datefrom], s) . " to " . DateFormat($boattransfer_period[botpe_dateto], s) ?>
                            ( <?= $boattransfer_period[botpe_name] ?> )
                        </td>
                        <!--<td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><? if ($row[agentfor_id] == "1") {
                            echo "&#10003;";
                        } ?></td>
        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><? if ($row[agentfor_id] == "2") {
                            echo "&#10003;";
                        } ?></td>-->

                        <? for ($grade_id_link = 1; $grade_id_link <= 5; $grade_id_link++) {
                            $row_rates = "";
                            $sql_rates = "select * from boattransfer_rates where boattransfers_id = $product_id_link ";
                            $sql_rates .= "and boattransferratetype_id = $ratetype[0] ";
                            $sql_rates .= "and boattransferperiod_id = $boattransfer_period[0] ";

                            if ($pricetype_id == "1") {
                                $sql_rates .= "and agentgrade_id = '0' ";
                            } else {
                                $sql_rates .= "and agentgrade_id = $grade_id_link ";
                            }

                            $sql_rates .= "order by botrate_id DESC ";

                            $result_rates = mysql_query($sql_rates);
                            $row_rates = mysql_fetch_array($result_rates);

                            if ($pricetype_id == "1") {
                                $sql_percents = "select * from boattransfer_ratepercents where boattransfers_id = $product_id_link ";
                                $sql_percents .= "and agentgrade_id = $grade_id_link ";
                                $sql_percents .= "order by botrateper_id DESC ";

                                $result_percents = mysql_query($sql_percents);
                                $row_percents = mysql_fetch_array($result_percents);

                                $rate_per1 = ($row_percents[botrateper_discount] * $row_rates[rate_1]) / 100;
                                $rate_1 = $row_rates[rate_1] - $rate_per1;
                                $rate_per2 = ($row_percents[botrateper_discount] * $row_rates[rate_2]) / 100;
                                $rate_2 = $row_rates[rate_2] - $rate_per2;
                                $rate_per3 = ($row_percents[botrateper_discount] * $row_rates[rate_3]) / 100;
                                $rate_3 = $row_rates[rate_3] - $rate_per3;
                                $rate_per4 = ($row_percents[botrateper_discount] * $row_rates[rate_4]) / 100;
                                $rate_4 = $row_rates[rate_4] - $rate_per4;

                            }
                            ?>

                            <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><? if ($pricetype_id == "2") {
                                    echo number_format($row_rates[rate_1]);
                                } else {
                                    echo number_format($rate_1);
                                } ?></td>
                            <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><? if ($pricetype_id == "2") {
                                    echo number_format($row_rates[rate_2]);
                                } else {
                                    echo number_format($rate_2);
                                } ?></td>
                            <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0">0</td>
                            <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0">0</td>

                        <? } // END for($grade_id_link=1; $grade_id_link<=5; $grade_id_link++){
                        ?>

                    </tr>

                    <?
                    $i++;

                } // END while ($boattransfer_period = mysql_fetch_array($boattransfer_periodboattransfer_period))

            } // END while($ratetype = mysql_fetch_array($ratetyperatetype))

        } // END [Boat Transfer] if($product_type_id_link == "1"){

        ?>

    </table>

    <? // END Table 1 ?>


    <br/><br/>


    <? // Table 2 ?>

    <table border="1" cellspacing="0" cellpadding="3" bgcolor="#FFFFFF" bordercolor="#000000">
        <tr bgcolor="#CCCCCC">
            <td width="30" rowspan="2" align="center" class="txt_bold_gray">No.</td>
            <td width="500" rowspan="2" align="center" class="txt_bold_gray">Rate Type</td>
            <!--<td width="100" colspan="2" align="center" class="txt_bold_gray">Company</td>
            <td width="200" colspan="4" align="center" class="txt_bold_gray">Internet Rate</td>-->

            <?
            $i = 1;

            $sql_grade = "select * from agent_grade where a_id > 5";
            $result_grade = mysql_query($sql_grade);

            while ($row_grade = mysql_fetch_array($result_grade)) {
                ?>
                <td width="200" colspan="4" align="center" class="txt_bold_gray"><?= $row_grade[a_name] ?></td>

            <? } // END while($row_grade = mysql_fetch_array($result_grade)) ?>

        </tr>

        <tr bgcolor="#CCCCCC">
            <!--<td width="50" align="center" class="txt_bold_gray">WAVE</td>
            <td width="50" align="center" class="txt_bold_gray">DOT COM</td>-->

            <? for ($n = 1; $n <= 5; $n++) { ?>
                <td width="50" align="center" class="txt_bold_gray">Adult</td>
                <td width="50" align="center" class="txt_bold_gray">Child</td>
                <td width="50" align="center" class="txt_bold_gray">Car (Private)</td>
                <td width="50" align="center" class="txt_bold_gray">Room</td>
            <? } ?>

        </tr>


        <? if ($product_type_id_link == "1") { // Boat Transfer

            $ratetype = "select * from boattransfer_ratetypes where boattransfers_id = $product_id_link order by boattransfers_id ASC";
            $ratetyperatetype = mysql_query($ratetype);

            //echo $ratetype;

            while ($ratetype = mysql_fetch_array($ratetyperatetype)) {
                $boattransferperiod_status = "";
                $boattransfer_period = "select * from boattransfer_period where boattransfers_id = $product_id_link
				and botpe_datefrom != '0000-00-00' and botpe_dateto != '0000-00-00' order by botpe_datefrom ASC";
                $boattransfer_periodboattransfer_period = mysql_query($boattransfer_period);
                while ($boattransfer_period = mysql_fetch_array($boattransfer_periodboattransfer_period)) {

                    ?>

                    <tr>
                        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><?= $i ?></td>
                        <td class="txt_bold_gray" align="left" bgcolor="#F0F0F0"><?= $ratetype[botrt_name] ?>
                            <?= DateFormat($boattransfer_period[botpe_datefrom], s) . " to " . DateFormat($boattransfer_period[botpe_dateto], s) ?>
                            ( <?= $boattransfer_period[botpe_name] ?> )
                        </td>
                        <!--<td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><? if ($row[agentfor_id] == "1") {
                            echo "&#10003;";
                        } ?></td>
        <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><? if ($row[agentfor_id] == "2") {
                            echo "&#10003;";
                        } ?></td>-->

                        <? for ($grade_id_link = 6; $grade_id_link <= 10; $grade_id_link++) {
                            $row_rates = "";
                            $sql_rates = "select * from boattransfer_rates where boattransfers_id = $product_id_link ";
                            $sql_rates .= "and boattransferratetype_id = $ratetype[0] ";
                            $sql_rates .= "and boattransferperiod_id = $boattransfer_period[0] ";

                            if ($pricetype_id == "1") {
                                $sql_rates .= "and agentgrade_id = '0' ";
                            } else {
                                $sql_rates .= "and agentgrade_id = $grade_id_link ";
                            }

                            $sql_rates .= "order by botrate_id DESC ";

                            $result_rates = mysql_query($sql_rates);
                            $row_rates = mysql_fetch_array($result_rates);

                            if ($pricetype_id == "1") {
                                $sql_percents = "select * from boattransfer_ratepercents where boattransfers_id = $product_id_link ";
                                $sql_percents .= "and agentgrade_id = $grade_id_link ";
                                $sql_percents .= "order by botrateper_id DESC ";

                                $result_percents = mysql_query($sql_percents);
                                $row_percents = mysql_fetch_array($result_percents);

                                $rate_per1 = ($row_percents[botrateper_discount] * $row_rates[rate_1]) / 100;
                                $rate_1 = $row_rates[rate_1] - $rate_per1;
                                $rate_per2 = ($row_percents[botrateper_discount] * $row_rates[rate_2]) / 100;
                                $rate_2 = $row_rates[rate_2] - $rate_per2;
                                $rate_per3 = ($row_percents[botrateper_discount] * $row_rates[rate_3]) / 100;
                                $rate_3 = $row_rates[rate_3] - $rate_per3;
                                $rate_per4 = ($row_percents[botrateper_discount] * $row_rates[rate_4]) / 100;
                                $rate_4 = $row_rates[rate_4] - $rate_per4;

                            }
                            ?>

                            <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><? if ($pricetype_id == "2") {
                                    echo number_format($row_rates[rate_1]);
                                } else {
                                    echo number_format($rate_1);
                                } ?></td>
                            <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0"><? if ($pricetype_id == "2") {
                                    echo number_format($row_rates[rate_2]);
                                } else {
                                    echo number_format($rate_2);
                                } ?></td>
                            <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0">0</td>
                            <td class="txt_bold_gray" align="center" bgcolor="#F0F0F0">0</td>

                        <? } // END for($grade_id_link=6; $grade_id_link<=10; $grade_id_link++){
                        ?>

                    </tr>

                    <?
                    $i++;

                } // END while ($boattransfer_period = mysql_fetch_array($boattransfer_periodboattransfer_period))

            } // END while($ratetype = mysql_fetch_array($ratetyperatetype))

        } // END [Boat Transfer] if($product_type_id_link == "1"){

        ?>

    </table>

    <? // END Table 2 ?>

    </td>
    <td align="right" valign="bottom"></td>
    </tr>
    </table>


<? } else { // END if($product_type_id_link != "99"){ ?>

<? } ?>