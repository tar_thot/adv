<?php
include("value_get.php");
if (isset($id)) {
    $page_title = "View Items  ";
    $sql = "select * from packages where pac_id = $id";
    $result = mysql_query($sql);
    $row = mysql_fetch_array($result);
    include("inc.top.php");
}


?>

<script language="JavaScript" type="text/JavaScript">
    <!--
    function MM_findObj(n, d) { //v4.01
        var p, i, x;
        if (!d) d = document;
        if ((p = n.indexOf("?")) > 0 && parent.frames.length) {
            d = parent.frames[n.substring(p + 1)].document;
            n = n.substring(0, p);
        }
        if (!(x = d[n]) && d.all) x = d.all[n];
        for (i = 0; !x && i < d.forms.length; i++) x = d.forms[i][n];
        for (i = 0; !x && d.layers && i < d.layers.length; i++) x = MM_findObj(n, d.layers[i].document);
        if (!x && d.getElementById) x = d.getElementById(n);
        return x;
    }
    function MM_validateForm() { //v4.0
        var i, p, q, nm, test, num, min, max, errors = '', args = MM_validateForm.arguments;
        for (i = 0; i < (args.length - 2); i += 3) {
            test = args[i + 2];
            val = MM_findObj(args[i]);
            if (val) {
                nm = val.name;
                if ((val = val.value) != "") {
                    if (test.indexOf('isEmail') != -1) {
                        p = val.indexOf('@');
                        if (p < 1 || p == (val.length - 1)) errors += '- E-mail must contain an e-mail address.\n';
                    } else if (test != 'R') {
                        num = parseFloat(val);
                        if (isNaN(val)) {
                            if (nm == 'category_id') nm = 'Category';
                            errors += '- ' + nm + ' must contain a number.\n';
                        }
                        if (test.indexOf('inRange') != -1) {
                            p = test.indexOf(':');
                            min = test.substring(8, p);
                            max = test.substring(p + 1);
                            if (num < min || max < num) errors += '- ' + nm + ' must contain a number between ' + min + ' and ' + max + '.\n';
                        }
                    }
                } else if (test.charAt(0) == 'R') {
                    if (nm == 'category_id') nm = 'Category';
                    if (nm == 'pac_name') nm = 'Name';
                    if (nm == 'pac_ref') nm = 'Ref';
                    if (nm == 'check_pac_ref') nm = 'Ref';
                    if (nm == 'activitypricetype_id') nm = 'Price Type';
                    errors += '- ' + nm + ' is required.\n';
                }
            }
        }
        if (errors) alert('The following error(s) occurred:\n' + errors);
        document.MM_returnValue = (errors == '');
    }
    //-->
</script>


<br/>

<!-- *************************************************Start Item List************************************************* -->


<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td class="txt_big_gray">Items List<br><br>
            [ <a href="./?mode=packages/items_add&id=<?= $id ?>&package_producttype_id=1">Add Boat Transfer </a> ]
            [ <a href="./?mode=packages/items_add&id=<?= $id ?>&package_producttype_id=2">Add Pick Up Transfer </a> ]
            [ <a href="./?mode=packages/items_add&id=<?= $id ?>&package_producttype_id=3">Add Tour </a> ]
            [ <a href="./?mode=packages/items_add&id=<?= $id ?>&package_producttype_id=4">Add Activity </a> ]
            [ <a href="./?mode=packages/items_add&id=<?= $id ?>&package_producttype_id=5">Add Bus Transfer </a> ]
            [ <a href="./?mode=packages/items_add&id=<?= $id ?>&package_producttype_id=6">Add Private Land Transfer </a>
            ]
            [ <a href="./?mode=packages/items_add&id=<?= $id ?>&package_producttype_id=8">Add Train Transfer </a> ]<br>
            [ <a href="./?mode=packages/items_add&id=<?= $id ?>&package_producttype_id=7">Add Hotel </a><span
                class="remark"><span style="font-size:12px">&nbsp;(Choose only one)</span></span> ]
            <br><br>

        </td>
    </tr>
</table>
<table width="100%" border="0" cellspacing="1" cellpadding="3" bgcolor="#FFFFFF">
    <?php

    $row = "";
    $sql = "select * from packages_items Where packages_id = $id ";

    $sql .= "order by pacitem_id ASC ";
    $result = mysql_query($sql);
    $numrows = mysql_num_rows($result);
    $i = 0;
    while ($row = mysql_fetch_array($result))
    {
    if ($color == $color_2) $color = $color_3; else $color = $color_2;
    if ($i == 0)
    {
    ?>
    <form name="form" action="process.php?mode=packages/items_save&id=<?= $id ?>" method="post"
          enctype="multipart/form-data">
        <tr bgcolor="<?= $color_1 ?>">
            <td class="txt_bold_white" align="center" width="50">Delete</td>
            <td class="txt_bold_white">Name</td>
            <td class="txt_bold_white">Ref</td>
            <td class="txt_bold_white">Product Type</td>

            <!--<td class="txt_bold_white" width="50">&nbsp;</td>-->
        </tr>
        <?php
        }
        ?>


        <?php
        $package_producttype_id = $row['package_producttype_id'];

        if ($package_producttype_id) {
            $sql_package_producttype = "select * from lis_package_producttype where lis_id = $package_producttype_id ";
            $result_package_producttype = mysql_query($sql_package_producttype);

            $row_package_producttype = mysql_fetch_array($result_package_producttype);

            $item_table = "$row_package_producttype[item_table]";
            $item_id = "$row_package_producttype[item_id]";
            $item_name = "$row_package_producttype[item_name]";
            $item_ref = "$row_package_producttype[item_ref]";
        }

        ?>
        <tr bgcolor="<?= $color ?>">
            <td align="center"><input type="checkbox" name="del_ids[]" value="<?= $row[0] ?>"/></td>
            <td><?= get_value($item_table, $item_id, $item_name, $row['product_id']) ?></td>
            <td><?= get_value($item_table, $item_id, $item_ref, $row['product_id']) ?></td>
            <td><?= get_value('lis_package_producttype', 'lis_id', 'lis_name', $row['package_producttype_id']) ?></td>
            <!--<td align="center"></td>-->
        </tr>
        <?php
        $i++;
        }
        if ($i != 0)
        {
        ?>
        <tr bgcolor="<?= $color_1 ?>">
            <td colspan="20"><input type="submit" name="submit_array" value="SAVE SETTING" style="width:125px;"
                                    onClick="return confirm('Do you want to do this ?')"></td>
        </tr>
    </form>
<?php
}
else {
    ?>
    <tr bgcolor="<?= $color_3 ?>" height="100">
        <td align="center">Record Not Found!</td>
    </tr>
    <?php
}
?>
</table>


<table width="100%" border="0" cellspacing="1" cellpadding="3">
    <tr>
        <td width="12%">&nbsp;</td>
        <td>&nbsp;</td>
        <td width="10%" align="right">&nbsp;</td>
        <td width="15%" align="right">&nbsp;</td>
        <td width="2%">&nbsp;</td>
    </tr>
</table>


<!-- *************************************************END Item List************************************************* -->


<table width="100%" border="0" cellspacing="1" cellpadding="3">
    <tr>
        <td colspan="5" bgcolor="#000000" height="1" style="padding:0px;"></td>
    </tr>
</table>


