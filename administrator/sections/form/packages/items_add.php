<?php
include("value_get.php");
if (isset($id)) {
    $sql = "select * from packages where pac_id = $id";
    $result = mysql_query($sql);
    $row = mysql_fetch_array($result);
    include("inc.top.php");

    $row = "";
    $page_title = "Add " . get_value('lis_package_producttype', 'lis_id', 'lis_name', $package_producttype_id);
    ?>
    <script language="JavaScript" type="text/JavaScript">
        <!--
        function MM_findObj(n, d) { //v4.01
            var p, i, x;
            if (!d) d = document;
            if ((p = n.indexOf("?")) > 0 && parent.frames.length) {
                d = parent.frames[n.substring(p + 1)].document;
                n = n.substring(0, p);
            }
            if (!(x = d[n]) && d.all) x = d.all[n];
            for (i = 0; !x && i < d.forms.length; i++) x = d.forms[i][n];
            for (i = 0; !x && d.layers && i < d.layers.length; i++) x = MM_findObj(n, d.layers[i].document);
            if (!x && d.getElementById) x = d.getElementById(n);
            return x;
        }
        function MM_validateForm() { //v4.0
            var i, p, q, nm, test, num, min, max, errors = '', args = MM_validateForm.arguments;
            for (i = 0; i < (args.length - 2); i += 3) {
                test = args[i + 2];
                val = MM_findObj(args[i]);
                if (val) {
                    nm = val.name;
                    if ((val = val.value) != "") {
                        if (test.indexOf('isEmail') != -1) {
                            p = val.indexOf('@');
                            if (p < 1 || p == (val.length - 1)) errors += '- E-mail must contain an e-mail address.\n';
                        } else if (test != 'R') {
                            num = parseFloat(val);
                            if (isNaN(val)) {
                                if (nm == 'name') nm = 'Room Type';
                                errors += '- ' + nm + ' must contain a number.\n';
                            }
                            if (test.indexOf('inRange') != -1) {
                                p = test.indexOf(':');
                                min = test.substring(8, p);
                                max = test.substring(p + 1);
                                if (num < min || max < num) errors += '- ' + nm + ' must contain a number between ' + min + ' and ' + max + '.\n';
                            }
                        }
                    } else if (test.charAt(0) == 'R') {
                        if (nm == 'putrt_name') nm = 'Rate Type';
                        errors += '- ' + nm + ' is required.\n';
                    }
                }
            }
            if (errors) alert('The following error(s) occurred:\n' + errors);
            document.MM_returnValue = (errors == '');
        }
        //-->
    </script>

    <script language="JavaScript" type="text/JavaScript">
        function checkProductFroShowRatetype() {


            //alert('country_id : ' + $("#country_id").val());

            var package_producttype_id = '<?= $package_producttype_id?>';

            $("#td_ratetype_id").prepend("<img src='../admin_ajax/images/icon/ajax-loader.gif' /> ");

            $.post("../admin_ajax/ajx_ratetypelist.php", {
                    dateproduct: package_producttype_id,
                    data1: $("#product_id").val(),
                    data2: $("#product_ratetype_id").val()
                },
                function (result) {
                    $("#td_ratetype_id").html(result);
                    //checkProvinceFroShowArea();
                }
            );


        }

    </script>

    <script language="JavaScript" type="text/JavaScript">
        $(document).ready(function () {
            checkProductFroShowRatetype();
            //checkProvinceFroShowArea();

        });
    </script>

    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="txt_big_gray"><?= $page_title ?></td>
        </tr>
    </table>
    <form action="process.php?mode=packages/items_save&id=<?= $id ?>&putrt_id=<?= $putrt_id ?>" onsubmit="MM_validateForm(
'putrt_name','','R'
);return document.MM_returnValue" name="form" id="form" method="post" enctype="multipart/form-data">
        <input type="hidden" name="packages_id" value="<?= $id ?>"/>
        <input type="hidden" name="package_producttype_id" id="package_producttype_id"
               value="<?= $package_producttype_id ?>"/>
        <table width="100%" border="0" cellspacing="0" cellpadding="3">
            <tr>
                <td align="right" class="txt_bold_gray" width="200">Product Name :</td>
                <td>
                    <?php
                    if ($package_producttype_id) {
                        $sql_package_producttype = "select * from lis_package_producttype where lis_id = $package_producttype_id ";
                        $result_package_producttype = mysql_query($sql_package_producttype);

                        $row_package_producttype = mysql_fetch_array($result_package_producttype);

                        $item_table = "$row_package_producttype[item_table]";
                        $item_id = "$row_package_producttype[item_id]";
                        $item_name = "$row_package_producttype[item_name]";
                    }

                    ?>
                    <select name="product_id" id="product_id" onchange="checkProductFroShowRatetype();">
                        <option value="">-- Select --</option>
                        <?php
                        $sql_product = "select * from $item_table where $item_id > 0 order by $item_name ASC";
                        $result_product = mysql_query($sql_product);

                        while ($row_product = mysql_fetch_array($result_product)) {

                            ?>
                            <option value="<?= $row_product[0] ?>"><?= $row_product[$item_name] ?></option>
                            <?php
                        }

                        ?>
                    </select>

                    <span class="remark"> * </span></td>
            </tr>

            <?php
            if ($package_producttype_id == 4 || $package_producttype_id == 8) {
                ?>

                <tr>
                    <td align="right" class="txt_bold_gray" width="200">Ratetype Name :</td>
                    <td id="td_ratetype_id">

                        <select name="product_ratetype_id">
                            <option value="">-- Select --</option>
                        </select>

                        <span class="remark"> * </span></td>
                </tr>

                <?php
            } // end if($package_producttype_id == 4 || $package_producttype_id == 8){
            ?>


            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td><input type="submit" name="submit" value="SUBMIT" style="width:75px;"></td>
            </tr>
        </table>
    </form>
    <?php
}
?>
