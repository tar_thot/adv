<?php
// Convert Variable Array To Variable

while (list($xVarName, $xVarvalue) = each($_GET)) {
    ${$xVarName} = $xVarvalue;
}


while (list($xVarName, $xVarvalue) = each($_POST)) {
    ${$xVarName} = $xVarvalue;
}

while (list($xVarName, $xVarvalue) = each($_FILES)) {
    ${$xVarName . "_name"} = $xVarvalue['name'];
    ${$xVarName . "_type"} = $xVarvalue['type'];
    ${$xVarName . "_size"} = $xVarvalue['size'];
    ${$xVarName . "_error"} = $xVarvalue['error'];
    ${$xVarName} = $xVarvalue['tmp_name'];
}
if ($id) {
    $page_title = "Update Supplier";
    $sql = "select * from suppliers where sp_id = $id";
    $result = mysql_query($sql);
    $row = mysql_fetch_array($result);
} else {
    $page_title = "Create New Supplier";
}

?>

<script language="JavaScript" type="text/JavaScript">
    <!--
    function MM_findObj(n, d) { //v4.01
        var p, i, x;
        if (!d) d = document;
        if ((p = n.indexOf("?")) > 0 && parent.frames.length) {
            d = parent.frames[n.substring(p + 1)].document;
            n = n.substring(0, p);
        }
        if (!(x = d[n]) && d.all) x = d.all[n];
        for (i = 0; !x && i < d.forms.length; i++) x = d.forms[i][n];
        for (i = 0; !x && d.layers && i < d.layers.length; i++) x = MM_findObj(n, d.layers[i].document);
        if (!x && d.getElementById) x = d.getElementById(n);
        return x;
    }
    function MM_validateForm() { //v4.0
        var i, p, q, nm, test, num, min, max, errors = '', args = MM_validateForm.arguments;
        for (i = 0; i < (args.length - 2); i += 3) {
            test = args[i + 2];
            val = MM_findObj(args[i]);
            if (val) {
                nm = val.name;
                if ((val = val.value) != "") {
                    if (test.indexOf('isEmail') != -1) {
                        p = val.indexOf('@');
                        if (p < 1 || p == (val.length - 1)) errors += '- E-mail must contain an e-mail address.\n';
                    } else if (test != 'R') {
                        num = parseFloat(val);
                        if (isNaN(val)) {
                            if (nm == 'sp_balance_credit') nm = 'Credit Balance';
                            if (nm == 'sp_min_credit') nm = 'Minimum of Credit available';
                            errors += '- ' + nm + ' must contain a number.\n';
                        }
                        if (test.indexOf('inRange') != -1) {
                            p = test.indexOf(':');
                            min = test.substring(8, p);
                            max = test.substring(p + 1);
                            if (num < min || max < num) errors += '- ' + nm + ' must contain a number between ' + min + ' and ' + max + '.\n';
                        }
                    }
                } else if (test.charAt(0) == 'R') {
                    if (nm == 'sp_ref') nm = 'Supplier Ref';
                    if (nm == 'sp_name') nm = 'Name';
                    if (nm == 'sp_email') nm = 'Email';
                    if (nm == 'check_sp_email') nm = 'Email';

                    errors += '- ' + nm + ' is required.\n';
                }
            }
        }
        if (errors) alert('The following error(s) occurred:\n' + errors);
        document.MM_returnValue = (errors == '');
    }
    //-->
</script>

<script language="JavaScript" type="text/JavaScript">
    function checkCountryFroShowProvince() {


        //alert('country_id : ' + $("#country_id").val());
        $("#td_province_id").prepend("<img src='../admin_ajax/images/icon/ajax-loader.gif' /> ");

        $.post("../admin_ajax/ajx_supplier_provincelist.php", {
                data1: $("#country_id").val(),
                data2: $("#province_id").val()
            },
            function (result) {
                $("#td_province_id").html(result);
                checkProvinceFroShowArea();
            }
        );


    }

    function checkProvinceFroShowCity() {
        //alert('province_id : ' + $("#province_id").val());
        $("#td_city_id").prepend("<img src='../admin_ajax/images/icon/ajax-loader.gif' /> ");

        $.post("../admin_ajax/ajx_supplier_citylist.php", {
                data1: $("#province_id").val(),
                data2: $("#city_id").val()
            },
            function (result) {
                $("#td_city_id").html(result);
            }
        );
    }


</script>

<script language="JavaScript" type="text/JavaScript">
    $(document).ready(function () {
        checkCountryFroShowProvince();
        checkProvinceFroShowCity();

    });
</script>


<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td class="txt_big_gray"><?= $page_title ?></td>
    </tr>
</table>
<form action="process.php?mode=suppliers/save&id=<?= $id ?>" onsubmit="MM_validateForm(
'sp_ref','','R',
'sp_name','','R',
'sp_balance_credit','','isNaN',
'sp_min_credit','','isNaN',
'sp_email','','R',
'check_sp_email','','R',
'check_sp_ref','','R',
'sp_email','','isEmail'
);return document.MM_returnValue" name="form" id="form1" method="post" enctype="multipart/form-data">
    <input type="hidden" name="id" id="id" value="<?= $id ?>"/>
    <table width="100%" border="0" cellspacing="0" cellpadding="3">

        <tr>
            <td align="right" class="txt_bold_gray" width="250"></td>
            <td width="1126"><b style="font-size:18px">ข้อมูล Supplier</b></td>
        </tr>


        <tr>
            <td align="right" class="txt_bold_gray" width="250">Supplier ID :</td>
            <td width="1126"><input type="text" name="sp_id_auto" value="<?php if ($row['sp_id']) {
                    echo "S " . $row['sp_id'];
                } ?>" size="50" readonly="readonly" style="background-color:#EAEAEA;"></td>
        </tr>


        <script type="text/javascript">
            function checkRefRepeatedly() {
                //alert('checkEmailRepeatedly : ' + $("#sp_email").val());
                $("#spn_sp_ref").html("<img src='../admin_ajax/images/icon/ajax-loader.gif' /> ");

                $.post("../admin_ajax/ajx_ref_repeatedly.php", {
                        data1: $("#sp_ref").val(),
                        data2: 'sp_',
                        data3: 'suppliers',
                        data4: '<?=$row[0]?>'
                    },
                    function (result) {
                        $("#spn_sp_ref").html(result);
                    }
                );
            }
        </script>

        <tr>
            <td align="right" class="txt_bold_gray" width="250">Supplier Ref :</td>
            <td width="1126"><input type="text" name="sp_ref" id="sp_ref" value="<?= $row['sp_ref'] ?>" size="50"
                                    onkeyup="checkRefRepeatedly();">
            <span id="spn_sp_ref" class="remark"> * <input type="hidden" name="check_sp_ref"
                                                           value="<?= $row['sp_ref'] ?>"></span>
            </td>
        </tr>
        <tr>
            <td align="right" class="txt_bold_gray" width="250">Company Name :</td>
            <td><input type="text" name="sp_cname" value="<?= $row['sp_cname'] ?>" size="50"><span
                    class="remark"> * </span>
            </td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray">Type Of Service :</td>
            <td><select name="servicetype_id" id="servicetype_id" style="width:200px">
                    <option value="">-- Select --</option>
                    <?php listbox('supplier_con_servicetype', 'con_id', 'con_name', $row['servicetype_id'], 'N'); ?>
                </select></td>
        </tr>

        <script type="text/javascript">
            function checkEmailRepeatedly() {
                //alert('checkEmailRepeatedly : ' + $("#sp_email").val());
                $("#spn_sp_email").html("<img src='../admin_ajax/images/icon/ajax-loader.gif' /> ");

                $.post("../admin_ajax/ajx_email_repeatedly.php", {
                        data1: $("#sp_email").val(),
                        data3: 'forsuppliers',
                        data2: '<?=$row[0]?>'
                    },
                    function (result) {
                        $("#spn_sp_email").html(result);
                    }
                );
            }
        </script>


        <tr>
            <td align="right" class="txt_bold_gray" width="250">Email (User Name) :</td>
            <td><input type="text" name="sp_email" id="sp_email" value="<?= $row['sp_email'] ?>" size="50"
                       onkeyup="checkEmailRepeatedly();">
            <span id="spn_sp_email" class="remark"> * <input type="hidden" name="check_sp_email"
                                                             value="<?= $row['sp_email'] ?>"></span></td>
        </tr>


        <script type="text/javascript">
            function reSetPasswordSupplier() {

                var new_pass = '';

                if (new_pass = prompt('กรุณาป้อนรหัสผ่านใหม่')) {
                    $("#sp_password").val(new_pass);
                }

            }
        </script>
        <tr>
            <td align="right" class="txt_bold_gray" width="250">Password :</td>
            <td><input type="text" name="sp_password" id="sp_password" value="<?= $row['sp_password'] ?>" size="50"
                       readonly="readonly" style="background-color:#EAEAEA;">
                <?php
                if ($id) {
                    echo '<input type="button" name="repass" value="เปลี่ยนรหัสผ่าน" onclick="reSetPasswordSupplier();" />';
                }

                ?>

            </td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="250">Payment Term Type :</td>
            <td>
                <input type="radio" name="sp_pyamenttermtype" value="prepayment"
                       size="50" <?php if ($row['sp_pyamenttermtype'] == 'prepayment') {
                    echo ' checked ';
                } ?> /> Prepayment
                <input type="radio" name="sp_pyamenttermtype" value="credit"
                       size="50" <?php if ($row['sp_pyamenttermtype'] == 'credit') {
                    echo ' checked ';
                } ?> /> Credit

                <select name="sp_paymentterm_num">
                    <option value="">-- Select --</option>
                    <?php list_number($row['sp_paymentterm_num'], 1, 60); ?>
                </select>
            </td>
        </tr>


        <tr>
            <td align="right" class="txt_bold_gray" width="250" valign="top">ที่อยู่ Supplier 1 :</td>
            <td>
                <textarea name="sp_addr1" cols="50"><?= $row['sp_addr1'] ?></textarea>
            </td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="250" valign="top">ที่อยู่ Supplier 2 :</td>
            <td>
                <textarea name="sp_addr2" cols="50"><?= $row['sp_addr2'] ?></textarea>
            </td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray">Country :</td>
            <td><select name="country_id" id="country_id" style="width:200px" onchange="checkCountryFroShowProvince();">
                    <option value="">-- Select --</option>
                    <?php listbox('supplier_con_country', 'con_id', 'con_name', $row['country_id'], 'N'); ?>
                </select></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray">Province :</td>
            <td id="td_province_id"><select name="province_id" id="province_id" style="width:200px"
                                            onchange="checkProvinceFroShowCity();">
                    <option value="">-- Select --</option>
                    <?php listbox('supplier_con_province', 'con_id', 'con_name', $row['province_id'], 'N'); ?>
                </select></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray">City :</td>
            <td id="td_city_id"><select name="city_id" id="city_id" style="width:200px">
                    <option value="">-- Select --</option>
                    <?php listbox('supplier_con_city', 'con_id', 'con_name', $row['city_id'], 'N'); ?>
                </select></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="250"> Zip Code :</td>
            <td><input type="text" name="sp_zipcode" value="<?= $row['sp_zipcode'] ?>" size="50"></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="250">เบอร์โทรศัพท์ Supplier 1 :</td>
            <td><input type="text" name="sp_phone1" value="<?= $row['sp_phone1'] ?>" size="50"></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="250">เบอร์โทรศัพท์ Supplier 2 :</td>
            <td><input type="text" name="sp_phone2" value="<?= $row['sp_phone2'] ?>" size="50"></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="250">เบอร์โทรศัพท์มือถือ Supplier :</td>
            <td><input type="text" name="sp_mobile" value="<?= $row['sp_mobile'] ?>" size="50"></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="250">Fax :</td>
            <td><input type="text" name="sp_fax" value="<?= $row['sp_fax'] ?>" size="50"></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="250"> Email Reservatin :</td>
            <td><input type="text" name="sp_email_reser" value="<?= $row['sp_email_reser'] ?>" size="50"></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="250"> Email Account :</td>
            <td><input type="text" name="sp_email_account" value="<?= $row['sp_email_account'] ?>" size="50"></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="250"> URL :</td>
            <td><input type="text" name="sp_url" value="<?= $row['sp_url'] ?>" size="50"></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="250"> License Upload 1 :</td>
            <td><?php $img_tmp = $row['file1'];
                if (isset($img_tmp)) { ?>
                    <a href="../files_upload/suppliers/<?= $img_tmp ?>" target="_blank">Download file 1</a><br>
                    <label><input type="checkbox" name="del_file1" value="<?= $img_tmp ?>"><span class="remark">Delete File</span></label>
                    <br>
                <?php } ?>
                <input name="file1" id="file1" type="file"><input name="tmp_file1" type="hidden"
                                                                  value="<?= $img_tmp ?>">
            </td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="250"> License Upload 2 :</td>
            <td><?php $img_tmp = $row['file2'];
                if (isset($img_tmp)) { ?>
                    <a href="../files_upload/suppliers/<?= $img_tmp ?>" target="_blank">Download file 2</a><br>
                    <label><input type="checkbox" name="del_file2" value="<?= $img_tmp ?>"><span class="remark">Delete File</span></label>
                    <br>
                <?php } ?>
                <input name="file2" id="file2" type="file"><input name="tmp_file2" type="hidden"
                                                                  value="<?= $img_tmp ?>">
            </td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="250"> License Upload 3 :</td>
            <td><?php $img_tmp = $row['file3'];
                if (isset($img_tmp)) { ?>
                    <a href="../files_upload/suppliers/<?= $img_tmp ?>" target="_blank">Download file 3</a><br>
                    <label><input type="checkbox" name="del_file3" value="<?= $img_tmp ?>"><span class="remark">Delete File</span></label>
                    <br>
                <?php } ?>
                <input name="file3" id="file3" type="file"><input name="tmp_file3" type="hidden"
                                                                  value="<?= $img_tmp ?>">
            </td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="250"> License Upload 4 :</td>
            <td><?php $img_tmp = $row['file4'];
                if (isset($img_tmp)) { ?>
                    <a href="../files_upload/suppliers/<?= $img_tmp ?>" target="_blank">Download file 4</a><br>
                    <label><input type="checkbox" name="del_file4" value="<?= $img_tmp ?>"><span class="remark">Delete File</span></label>
                    <br>
                <?php } ?>
                <input name="file4" id="file4" type="file"><input name="tmp_file4" type="hidden"
                                                                  value="<?= $img_tmp ?>">
            </td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="250"> License Upload 5 :</td>
            <td><?php $img_tmp = $row['file5'];
                if (isset($img_tmp)) { ?>
                    <a href="../files_upload/suppliers/<?= $img_tmp ?>" target="_blank">Download file 5</a><br>
                    <label><input type="checkbox" name="del_file5" value="<?= $img_tmp ?>"><span class="remark">Delete File</span></label>
                    <br>
                <?php } ?>
                <input name="file5" id="file5" type="file"><input name="tmp_file5" type="hidden"
                                                                  value="<?= $img_tmp ?>">
            </td>
        </tr>

        <tr>
            <td></td>
            <td bgcolor="#FFFFFF">
                <div
                    style="font-size:0.9em; color:#C00; padding:5px; width:300px; background-color:#FFE0DD; position:relative;">
                    - Please use file size less than 2MB.<br/>
                </div>
            </td>
        </tr>


        <tr>
            <td align="right" class="txt_bold_gray" width="250"> Member Association :</td>
            <td><input type="text" name="sp_memassociation" value="<?= $row['sp_memassociation'] ?>" size="50"></td>
        </tr>

        <tr>
            <?php
            if ($row['sp_create_date'] > 0) {
                $sp_create_date = $row['sp_create_date'];
            } else {
                $sp_create_date = $today;
            }
            ?>
            <td align="right" class="txt_bold_gray" width="250"> Create Date :</td>
            <td>
                <script>DateInput('sp_create_date', false, 'yyyy-mm-dd', '<?= $sp_create_date?>')</script>
            </td>
        </tr>


        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="250"></td>
            <td width="1126"><b style="font-size:18px">Contact</b></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="250">คำนำหน้าชื่อ ผู้ติดต่อ :</td>
            <td><select name="sp_title_id">
                    <option value="">-- Select --</option>
                    <?php listbox(nametitles, nt_id, nt_name, $row['sp_title_id'], 'N'); ?>
                </select></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="250"> ชื่อ ผู้ติดต่อ :</td>
            <td><input type="text" name="sp_fname" value="<?= $row['sp_fname'] ?>" size="50"></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="250"> สกุล ผู้ติดต่อ :</td>
            <td><input type="text" name="sp_lname" value="<?= $row['sp_lname'] ?>" size="50"></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="250"> Department :</td>
            <td><input type="text" name="sp_department" value="<?= $row['sp_department'] ?>" size="50"></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="250"> Position :</td>
            <td><input type="text" name="sp_position" value="<?= $row['sp_position'] ?>" size="50"></td>
        </tr>


        <tr>
            <td align="right" class="txt_bold_gray" width="250"> เบอร์โทรศัพท์ ผู้ติดต่อ :</td>
            <td><input type="text" name="sp_phone3" value="<?= $row['sp_phone3'] ?>" size="50"></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="250"> Fax ผู้ติดต่อ :</td>
            <td><input type="text" name="sp_fax2" value="<?= $row['sp_fax2'] ?>" size="50"></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="250"> เบอร์โทรศัพท์มือถือ ผู้ติดต่อ :</td>
            <td><input type="text" name="sp_mobile2" value="<?= $row['sp_mobile2'] ?>" size="50"></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="250"> Email ผู้ติดต่อ :</td>
            <td><input type="text" name="sp_email_c" value="<?= $row['sp_email_c'] ?>" size="50"></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray">Office Map :</td>
            <td><?php $img_tmp = $row['photo1'];
                if ($img_tmp) { ?>
                    <img src="./resizer.php?imgfile=../photo/suppliers/<?= $img_tmp ?>&size=300" border="0"><br>
                    <label><input type="checkbox" name="del_photo1" value="<?= $img_tmp ?>"><span class="remark">Delete Photo</span></label>
                    <br>
                <?php } ?>
                <input name="photo1" type="file"><input name="tmp_photo1" type="hidden" value="<?= $img_tmp ?>"></td>
        </tr>

        <tr>
            <td></td>
            <td bgcolor="#FFFFFF">
                <div
                    style="font-size:0.9em; color:#C00; padding:5px; width:300px; background-color:#FFE0DD; position:relative;">
                    - Support JPG Format Only<br>
                    - Please use file size less than 2MB.<br/>
                </div>
            </td>
        </tr>


        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="250"></td>
            <td width="1126"><b style="font-size:18px">Bank Details</b></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="250"> Bank Name :</td>
            <td><input type="text" name="sp_bankname" value="<?= $row['sp_bankname'] ?>" size="50"></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="250"> Bank Branch :</td>
            <td><input type="text" name="sp_bankbranch" value="<?= $row['sp_bankbranch'] ?>" size="50"></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="250"> Beneficiary Name :</td>
            <td><input type="text" name="sp_bankbenef" value="<?= $row['sp_bankbenef'] ?>" size="50"></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="250"> Account Number :</td>
            <td><input type="text" name="sp_bankaccount" value="<?= $row['sp_bankaccount'] ?>" size="50"></td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray" width="250"> Swift Code :</td>
            <td><input type="text" name="sp_bankswift" value="<?= $row['sp_bankswift'] ?>" size="50"></td>
        </tr>


        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td><input type="submit" name="submit" value="SUBMIT" style="width:75px;"><?php
                if ($id) echo " <input type=\"submit\" name=\"del\" value=\"DELETE\" style=\"width:75px;\" onClick=\"return confirm('Do you want to delete this record ?')\">";
                else echo " <input type=\"reset\" name=\"Reset\" value=\"RESET\" style=\"width:75px;\">";
                ?></td>
        </tr>
    </table>
</form>


<script type="text/javascript">
    checkPayType();
</script>