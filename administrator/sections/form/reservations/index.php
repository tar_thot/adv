<?php

function setNumberLength($num, $length)
{
    $sumstr = strlen($num);
    $zero = str_repeat("0", $length - $sumstr);
    $results = $zero . $num;

    return $results;
}

?>

<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td valign="top">
            <table width="100%" border="0" cellspacing="0" cellpadding="3">
                <tr>
                    <td class="txt_big_gray">Reservation List
                        <!--[ <a href="./?mode=reservations/add">Create New Reservation </a> ]--></td>
                </tr>
            </table>
            <!---- Listing Body ---->
            <table width="100%" border="0" cellspacing="0" cellpadding="3">
                <?php
                include("value_get.php");
                $linkp = "&search=" . $search . "&plt_ref=" . $plt_ref . "";
                $linkp = '';

                //$sql = "select * from reservations where res_fname like '%".$search."%'";
                $sql = "select * from reservations ";
                $sql .= "where res_id > 0 ";

                if (isset($res_id_str)) {
                    $sql .= "AND res_id_str like '%" . $res_id_str . "%' ";
                    $linkp .= '&res_id_str=' . $res_id_str;
                }
                if (isset($agents_id)) {
                    $sql .= "AND agents_id = '" . $agents_id . "' ";
                    $linkp .= '&agents_id=' . $agents_id;
                }
                if (isset($res_fname)) {
                    $sql .= "AND res_fname like '%" . $res_fname . "%' ";
                    $linkp .= '&res_fname=' . $res_fname;
                }
                if (isset($res_lname)) {
                    $sql .= "AND res_lname like '%" . $res_lname . "%' ";
                    $linkp .= '&res_lname=' . $res_lname;
                }
                if (isset($bookingstatus_id)) {
                    $sql .= "AND bookingstatus_id = '" . $bookingstatus_id . "' ";
                    $linkp .= '&bookingstatus_id=' . $bookingstatus_id;
                }
                if (isset($paymentstatus_id)) {
                    $sql .= "AND paymentstatus_id = '" . $paymentstatus_id . "' ";
                    $linkp .= '&paymentstatus_id=' . $paymentstatus_id;
                }
                if (isset($sevice_name)) {
                    // In Item
                    $linkp .= '&sevice_name=' . $sevice_name;
                }
                if (isset($service_date)) {
                    // In Item
                    $linkp .= '&service_date=' . $service_date;
                }
                if (isset($reserv_producttype_id)) {
                    // In Item
                    $linkp .= '&reserv_producttype_id=' . $reserv_producttype_id;
                }

                if (isset($vo_no)) {

                    $sql_vo = "SELECT * FROM voucher WHERE vo_no = '$vo_no' ";
                    $sql_vo .= "AND vo_status = 2 ";
                    $sql_vo .= "ORDER BY vo_id ASC ";

                    $result_vo = mysql_query($sql_vo);
                    $row_vo = mysql_fetch_array($result_vo);

                    $sql .= "AND res_id_str = '" . $row_vo['vo_res_id'] . "' ";
                    $linkp .= '&voucher_no=' . $vo_no;
                }
                if (isset($res_agent_voucher)) {
                    $sql .= "AND res_agent_voucher = '" . $res_agent_voucher . "' ";
                    $linkp .= '&res_agent_voucher=' . $res_agent_voucher;
                }


                $sql .= " order by res_id DESC";
                $results = mysql_query($sql);
                $perpage = 15;
                $result = $perpage;
                $numrows = mysql_num_rows($results);
                $numpage = $numrows / $result;
                if (!$paper || $paper == 1) {
                    $start = 1;
                    $paper = 1;
                } else {
                    $start = (($paper - 1) * $result) + 1;
                    $result = $result * $paper;
                }
                if ($numrows != 0) $show_page .= "<strong>Pages : </strong>";
                for ($i = 1; $i < $numpage + 1; $i++) {
                    if ($paper == $i) $linkpage = "<strong style=\"color:#CCCCCC\">" . $i . "</strong>";
                    else $linkpage = "<a href='./?mode=reservations/index&paper=" . $i . $linkp . "'><strong>" . $i . "</strong></a>";
                    $show_page .= $linkpage . " ";
                }
                if ($paper == $i - 1) $result = $numrows;

                if ($linkp == '') {
                    $show_row = "<b>Found : </b>" . $numrows . " Reservation(s)";
                }
                ?>
                <tr>
                    <td>
                        <!---- Search Box ---->

                        <table border="0" cellspacing="3" cellpadding="3" style="border: solid #CCCCCC 1px;">
                            <form name="formSearch" method="get" action="./">
                                <tr>
                                    <td class="txt_bold_gray" align="right">
                                        Reservation ID<br/>
                                        <input type="text" name="res_id_str" value="<?= $res_id_str ?>"
                                               style="width:150px;">
                                    </td>
                                    <td class="txt_bold_gray" align="right">
                                        Agent<br/>
                                        <select name="agents_id" style="width:155px;">
                                            <option value="">-- Select --</option>
                                            <?php listbox('agents', ' ag_id', ' ag_name', $agents_id, 'N'); ?>
                                        </select>
                                    </td>
                                    <td class="txt_bold_gray" align="right">
                                        Service Name<br/>
                                        <input type="text" name="sevice_name" value="<?= $sevice_name ?>"
                                               style="width:150px;">
                                    </td>

                                </tr>

                                <tr>
                                    <td class="txt_bold_gray" align="right">
                                        Service Date<br/>
                                        <script>DateInput('service_date', false, ' '
                                            yyyy - mm - dd
                                            ' <?php if (isset($service_date)) {
                                                echo "',$service_date'";
                                            }?>)</script>
                                    </td>
                                    <td class="txt_bold_gray" align="right">
                                        First Name<br/>
                                        <input type="text" name="res_fname" value="<?= $res_fname ?>"
                                               style="width:150px;">
                                    </td>
                                    <td class="txt_bold_gray" align="right">
                                        Last Name<br/>
                                        <input type="text" name="res_lname" value="<?= $res_lname ?>"
                                               style="width:150px;">
                                    </td>

                                </tr>

                                <tr>
                                    <td class="txt_bold_gray" align="right">
                                        Product Type<br/>
                                        <select name="reserv_producttype_id" style="width:155px;">
                                            <option value="">-- Select --</option>
                                            <?php listbox('lis_reserv_producttype', 'lis_id', 'lis_name', $reserv_producttype_id, 'N'); ?>
                                        </select>
                                    </td>
                                    <td class="txt_bold_gray" align="right">
                                        Booking Status<br/>
                                        <select name="bookingstatus_id" style="width:155px;">
                                            <option value="">-- Select --</option>
                                            <?php listbox('lis_booking_status', 'lis_id', 'lis_name', $bookingstatus_id, 'N'); ?>
                                        </select>
                                    </td>
                                    <td class="txt_bold_gray" align="right">
                                        Payment Status<br/>
                                        <select name="paymentstatus_id" style="width:155px;">
                                            <option value="">-- Select --</option>
                                            <?php listbox('lis_payment_status', ' lis_id', ' lis_name', $paymentstatus_id, 'N'); ?>
                                        </select>
                                    </td>
                                </tr>

                                <tr>
                                    <td class="txt_bold_gray" align="right">
                                        Voucher No.<br/>
                                        <input type="text" name="vo_no" value="<?= $vo_no ?>" style="width:150px;">
                                    </td>

                                    <td class="txt_bold_gray" align="right">
                                        Agent Voucher No.<br/>
                                        <input type="text" name="res_agent_voucher" value="<?= $res_agent_voucher ?>"
                                               style="width:150px;">
                                    </td>

                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td valign="bottom"><input type="hidden" name="mode" value="<?= $mode ?>"><input
                                            type="submit" name="Submit" value="SEARCH" style="height:25px;"></td>

                                </tr>


                            </form>
                        </table>
                        <!---- Search Box ---->
                    </td>
                    <td align="right" valign="bottom"></td>
                </tr>
                <tr>

                    <td align="right" valign="bottom" colspan="2"><?php
                        echo $show_row;
                        if (isset($show_page)) echo "  " . $show_page;
                        ?></td>
                </tr>
                <?php
                if ($numrows > 0)
                {
                for ($a = 1; $a < $start; $a++) mysql_fetch_array($results);
                ?>
            </table>
            <table width="100%" border="0" cellspacing="1" cellpadding="3" bgcolor="#FFFFFF">
                <form name="form" action="process.php?mode=reservations/save" method="post"
                      enctype="multipart/form-data">
                    <tr bgcolor="<?= $color_1 ?>">
                        <td class="txt_bold_white" align="center" width="45">Delete</td>
                        <td width="100" class="txt_bold_white">ID</td>
                        <td width="100" class="txt_bold_white">&nbsp;Book&nbsp;Date&nbsp;</td>
                        <td width="100" class="txt_bold_white">Agent&nbsp;Name</td>
                        <!--<td width="100" class="txt_bold_white" >Email</td>-->
                        <td width="100" class="txt_bold_white">First&nbsp;Name</td>
                        <td width="100" class="txt_bold_white">Last&nbsp;Name</td>
                        <td width="1000" class="txt_bold_white">&nbsp;Service&nbsp;Name&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        <td width="100" class="txt_bold_white" align="center">Account Code (Adult)</td>
                        <td width="100" class="txt_bold_white" align="center">Account Code (Child)</td>
                        <td width="100" class="txt_bold_white">Service&nbsp;Date</td>
                        <td width="100" class="txt_bold_white">Booking Status</td>
                        <td width="100" class="txt_bold_white">Payment</td>
                        <td width="100" class="txt_bold_white">Total Amount</td>


                        <td class="txt_bold_white" width="43">&nbsp;</td>
                    </tr>
                    <?php
                    for ($start; $start < $result + 1; $start++) {
                        $row = mysql_fetch_array($results);


                        ?>

                        <?php
                        # Loop For Boat Transfer			
                        $sql_rbt = "SELECT * ";
                        $sql_rbt .= "FROM reservation_boattransfer_items ";
                        $sql_rbt .= "where reservations_id = $row[0] ";


                        if (isset($sevice_name)) {
                            $sql_rbt .= "AND rbt_name like '%" . $sevice_name . "%' ";;
                        }
                        if (isset($service_date)) {
                            $sql_rbt .= "AND rbt_travel_date = '" . $service_date . "' ";
                        }
                        if (isset($reserv_producttype_id) && $reserv_producttype_id != '1') {
                            $sql_rbt .= "AND rbt_id = 0 ";
                        }


                        $sql_rbt .= "order by rbt_id asc ";
                        //echo "sql_rbt : $sql_rbt";


                        $wlinks = "";
                        //if($p_id){$wlinks .= "&p_id=$p_id";}


                        $results_rbt = mysql_query($sql_rbt);
                        while ($rows_rbt = mysql_fetch_array($results_rbt)) {

                            if ($color == $color_2) $color = $color_3; else $color = $color_2;
                            ?>
                            <tr bgcolor="<?= $color ?>">
                                <td align="center"><input type="checkbox" name="del_ids[']" value="<?= $row[0] ?>"/>
                                </td>
                                <td><a href="./?mode=reservations/view&id=<?= $row[0] ?>"><?= $row['res_id_str'] ?></a>
                                </td>
                                <td align="center"><?= DateFormat($row['res_date'], "s") ?></td>
                                <td><?= get_value('agents', ' ag_id', ' ag_name', $row['agents_id']) ?></td>
                                <!--<td><?= $row['res_email'] ?></td>-->
                                <td><?= $row['res_fname'] ?></td>
                                <td><?= $row['res_lname'] ?></td>
                                <td><?= $rows_rbt['rbt_name'] ?></td>
                                <td align="center">
                                    <?php if ($row['company'] == 1) { ?>
                                        <?= get_value('boattransfers', ' bot_id', 'ac_andaman_adult', $rows_rbt['boattransfers_id']) ?>
                                    <?php } elseif ($row['company'] == 2) { ?>
                                        <?= get_value('boattransfers', ' bot_id', ' ac_dotcom_adult', $rows_rbt['boattransfers_id']) ?>
                                    <?php } elseif ($row['company'] == 3) { ?>
                                        <?= get_value('boattransfers', ' bot_id', ' ac_btoc_adult', $rows_rbt['boattransfers_id']) ?>
                                    <?php } else { ?>

                                    <?php } ?>
                                </td>
                                <td align="center">
                                    <?php if ($row['company'] == 1) { ?>
                                        <?= get_value('boattransfers', ' bot_id', ' ac_andaman_child', $rows_rbt['boattransfers_id']) ?>
                                    <?php } elseif ($row['company'] == 2) { ?>
                                        <?= get_value('boattransfers', ' bot_id', ' ac_dotcom_child', $rows_rbt['boattransfers_id']) ?>
                                    <?php } elseif ($row['company'] == 3) { ?>
                                        <?= get_value('boattransfers', ' bot_id', ' ac_btoc_child', $rows_rbt['boattransfers_id']) ?>
                                    <?php } else { ?>

                                    <?php } ?>
                                </td>
                                <td align="center"><?= DateFormat($rows_rbt['rbt_travel_date'], "s") ?></td>
                                <td><?= get_value('lis_booking_status', 'lis_id', 'lis_name', $row['bookingstatus_id']) ?></td>
                                <td><?= get_value('lis_payment_status', 'lis_id', 'lis_name', $row['paymentstatus_id']) ?></td>
                                <td align="right"><?= number_format($rows_rbt['rbt_prices'], 0) ?></td>
                                <td align="center"><a href="./?mode=reservations/view&id=<?= $row[0] ?>"><img
                                            src="images/icon_menu/edit.png" border="0" width="20" height="20"
                                            align="absmiddle"/></a></td>
                            </tr>
                            <?php
                        }// END while($rows_rbt = mysql_fetch_array($results_rbt)){		
                        ?>


                        <?php
                        # Loop For Pick Up Transfer			
                        $sql_rpt = "SELECT * ";
                        $sql_rpt .= "FROM reservation_pickuptransfer_items ";
                        $sql_rpt .= "where reservations_id = $row[0] ";

                        if (isset($sevice_name)) {
                            $sql_rpt .= "AND rpt_name like '%" . $sevice_name . "%' ";
                        }
                        if (isset($service_date)) {
                            $sql_rpt .= "AND rpt_travel_date = '" . $service_date . "' ";
                        }
                        if (isset($reserv_producttype_id) && $reserv_producttype_id != '2') {
                            $sql_rpt .= "AND rpt_id = 0 ";
                        }


                        $sql_rpt .= "order by rpt_id asc ";
                        //echo "sql_rpt : $sql_rpt";


                        $wlinks = "";
                        //if($p_id){$wlinks .= "&p_id=$p_id";}


                        $results_rpt = mysql_query($sql_rpt);
                        while ($rows_rpt = mysql_fetch_array($results_rpt)) {

                            if ($color == $color_2) $color = $color_3; else $color = $color_2;
                            ?>
                            <tr bgcolor="<?= $color ?>">
                                <td align="center"><input type="checkbox" name="del_ids[]" value="<?= $row[0] ?>"/></td>
                                <td><a href="./?mode=reservations/view&id=<?= $row[0] ?>"><?= $row['res_id_str'] ?></a>
                                </td>
                                <td align="center"><?= DateFormat($row['res_date'], "s") ?></td>
                                <td><?= get_value('agents', ' ag_id', ' ag_name', $rows['agents_id']) ?></td>
                                <!--<td><?= $row['res_email'] ?></td>-->
                                <td><?= $row['res_fname'] ?></td>
                                <td><?= $row['res_lname'] ?></td>
                                <td><?= $rows_rpt['rpt_name'] ?></td>
                                <td align="center">
                                    <?php if ($row['company'] == 1) { ?>
                                        <?= get_value('pickuptransfers', ' put_id', ' ac_andaman_adult', $rows_rpt['pickuptransfers_id']) ?>
                                    <?php } elseif ($row['company'] == 2) { ?>
                                        <?= get_value('pickuptransfers', ' put_id', ' ac_dotcom_adult', $rows_rpt['pickuptransfers_id']) ?>
                                    <?php } elseif ($row['company'] == 3) { ?>
                                        <?= get_value('pickuptransfers', ' put_id', ' ac_btoc_adult', $rows_rpt['pickuptransfers_id']) ?>
                                    <?php } else { ?>

                                    <?php } ?>
                                </td>
                                <td align="center">
                                    <?php if ($row['company'] == 1) { ?>
                                        <?= get_value('pickuptransfers', ' put_id', ' ac_andaman_child', $rows_rpt['pickuptransfers_id']) ?>
                                    <?php } elseif ($row['company'] == 2) { ?>
                                        <?= get_value('pickuptransfers', ' put_id', ' ac_dotcom_child', $rows_rpt['pickuptransfers_id']) ?>
                                    <?php } elseif ($row['company'] == 3) { ?>
                                        <?= get_value('pickuptransfers', ' put_id', ' ac_btoc_child', $rows_rpt['pickuptransfers_id']) ?>
                                    <?php } else { ?>

                                    <?php } ?>
                                </td>
                                <td align="center"><?= DateFormat($rows_rpt['rpt_travel_date'], "s") ?></td>
                                <td><?= get_value('lis_booking_status', ' lis_id', ' lis_name', $rows['bookingstatus_id']) ?></td>
                                <td><?= get_value('lis_payment_status', ' lis_id', ' lis_name', $rows['paymentstatus_id']) ?></td>
                                <td align="right"><?= number_format($rows_rpt['rpt_prices']) ?></td>
                                <td align="center"><a href="./?mode=reservations/view&id=<?= $row[0] ?>"><img
                                            src="images/icon_menu/edit.png" border="0" width="20" height="20"
                                            align="absmiddle"/></a></td>
                            </tr>
                            <?php
                        }// END while($rows_rpt = mysql_fetch_array($results_rpt)){		
                        ?>


                        <?php
                        # Loop For Tour			
                        $sql_rtt = "SELECT * ";
                        $sql_rtt .= "FROM reservation_tour_items ";
                        $sql_rtt .= "where reservations_id = $row[0] ";


                        if (isset($sevice_name)) {
                            $sql_rtt .= "AND rtt_name like '%" . $sevice_name . "%' ";;
                        }
                        if (isset($service_date)) {
                            $sql_rtt .= "AND rtt_travel_date = '" . $service_date . "' ";
                        }
                        if (isset($reserv_producttype_id) && $reserv_producttype_id != '3') {
                            $sql_rtt .= "AND rtt_id = 0 ";
                        }


                        $sql_rtt .= "order by rtt_id asc ";
                        //echo "sql_rtt : $sql_rtt";


                        $wlinks = "";
                        //if($p_id){$wlinks .= "&p_id=$p_id";}


                        $results_rtt = mysql_query($sql_rtt);
                        while ($rows_rtt = mysql_fetch_array($results_rtt)) {

                            if ($color == $color_2) $color = $color_3; else $color = $color_2;
                            ?>
                            <tr bgcolor="<?= $color ?>">
                                <td align="center"><input type="checkbox" name="del_ids[]" value="<?= $row[0] ?>"/></td>
                                <td><a href="./?mode=reservations/view&id=<?= $row[0] ?>"><?= $row['res_id_str'] ?></a>
                                </td>
                                <td align="center"><?= DateFormat($row['res_date'], "s") ?></td>
                                <td><?= get_value('agents', ' ag_id', ' ag_name', $rows['agents_id']) ?></td>
                                <!--<td><?= $row['res_email'] ?></td>-->
                                <td><?= $row['res_fname'] ?></td>
                                <td><?= $row['res_lname'] ?></td>
                                <td><?= $rows_rtt['rtt_name'] ?></td>
                                <td align="center">
                                    <?php if ($row['company'] == 1) { ?>
                                        <?= get_value('tours', ' tou_id', 'ac_andaman_adult', $rows_rtt['tours_id']) ?>
                                    <?php } elseif ($row['company'] == 2) { ?>
                                        <?= get_value('tours', ' tou_id', ' ac_dotcom_adult', $rows_rtt['tours_id']) ?>
                                    <?php } elseif ($row['company'] == 3) { ?>
                                        <?= get_value('tours', ' tou_id', ' ac_btoc_adult', $rows_rtt['tours_id']) ?>
                                    <?php } else { ?>

                                    <?php } ?>
                                </td>
                                <td align="center">
                                    <?php if ($row['company'] == 1) { ?>
                                        <?= get_value('tours', ' tou_id', ' ac_andaman_child', $rows_rtt['tours_id']) ?>
                                    <?php } elseif ($row['company'] == 2) { ?>
                                        <?= get_value('tours', ' tou_id', ' ac_dotcom_child', $rows_rtt['tours_id']) ?>
                                    <?php } elseif ($row['company'] == 3) { ?>
                                        <?= get_value('tours', ' tou_id', ' ac_btoc_child', $rows_rtt['tours_id']) ?>
                                    <?php } else { ?>

                                    <?php } ?>
                                </td>
                                <td align="center"><?= DateFormat($rows_rtt['rtt_travel_date'], "s") ?></td>
                                <td><?= get_value('lis_booking_status', ' lis_id', ' lis_name', $rows['bookingstatus_id']) ?></td>
                                <td><?= get_value('lis_payment_status', ' lis_id', ' lis_name', $rows['paymentstatus_id']) ?></td>
                                <td align="right"><?= number_format($rows_rtt['rtt_prices'], 0) ?></td>
                                <td align="center"><a href="./?mode=reservations/view&id=<?= $row[0] ?>"><img
                                            src="images/icon_menu/edit.png" border="0" width="20" height="20"
                                            align="absmiddle"/></a></td>
                            </tr>
                            <?php
                        }// END while($rows_rtt = mysql_fetch_array($results_rtt)){		
                        ?>


                        <?php
                        # Loop For Activity			
                        $sql_rat = "SELECT * ";
                        $sql_rat .= "FROM reservation_activity_items ";
                        $sql_rat .= "where reservations_id = $row[0] ";


                        if (isset($sevice_name)) {
                            $sql_rat .= "AND rat_name like '%" . $sevice_name . "%' ";;
                        }
                        if (isset($service_date)) {
                            $sql_rat .= "AND rat_travel_date = '" . $service_date . "' ";
                        }
                        if (isset($reserv_producttype_id) && $reserv_producttype_id != '4') {
                            $sql_rat .= "AND rat_id = 0 ";
                        }


                        $sql_rat .= "order by rat_id asc ";
                        //echo "sql_rat : $sql_rat";


                        $wlinks = "";
                        //if($p_id){$wlinks .= "&p_id=$p_id";}


                        $results_rat = mysql_query($sql_rat);
                        while ($rows_rat = mysql_fetch_array($results_rat)) {

                            if ($color == $color_2) $color = $color_3; else $color = $color_2;
                            ?>
                            <tr bgcolor="<?= $color ?>">
                                <td align="center"><input type="checkbox" name="del_ids[]" value="<?= $row[0] ?>"/></td>
                                <td><a href="./?mode=reservations/view&id=<?= $row[0] ?>"><?= $row['res_id_str'] ?></a>
                                </td>
                                <td align="center"><?= DateFormat($row['res_date'], "s") ?></td>
                                <td><?= get_value('agents', ' ag_id', ' ag_name', $rows['agents_id']) ?></td>
                                <!--<td><?= $row['res_email'] ?></td>-->
                                <td><?= $row['res_fname'] ?></td>
                                <td><?= $row['res_lname'] ?></td>
                                <td><?= $rows_rat['rat_name'] ?></td>
                                <td align="center">
                                    <?php if ($row['company'] == 1) { ?>
                                        <?= get_value('activities', ' act_id', ' ac_andaman_adult', $rows_rat['activities_id']) ?>
                                    <?php } elseif ($row['company'] == 2) { ?>
                                        <?= get_value('activities', ' act_id', ' ac_dotcom_adult', $rows_rat['activities_id']) ?>
                                    <?php } elseif ($row['company'] == 3) { ?>
                                        <?= get_value('activities', ' act_id', ' ac_btoc_adult', $rows_rat['activities_id']) ?>
                                    <?php } else { ?>

                                    <?php } ?>
                                </td>
                                <td align="center">
                                    <?php if ($row['company'] == 1) { ?>
                                        <?= get_value('activities', ' act_id', ' ac_andaman_child', $rows_rat['activities_id']) ?>
                                    <?php } elseif ($row['company'] == 2) { ?>
                                        <?= get_value('activities', ' act_id', ' ac_dotcom_child', $rows_rat['activities_id']) ?>
                                    <?php } elseif ($row['company'] == 3) { ?>
                                        <?= get_value('activities', ' act_id', ' ac_btoc_child', $rows_rat['activities_id']) ?>
                                    <?php } else { ?>

                                    <?php } ?>
                                </td>
                                <td align="center"><?= DateFormat($rows_rat['rat_travel_date'], "s") ?></td>
                                <td><?= get_value('lis_booking_status', ' lis_id', ' lis_name', $rows['bookingstatus_id']) ?></td>
                                <td><?= get_value('lis_payment_status', ' lis_id', ' lis_name', $rows['paymentstatus_id']) ?></td>
                                <td align="right"><?= number_format($rows_rat['rat_prices'], 0) ?></td>
                                <td align="center"><a href="./?mode=reservations/view&id=<?= $row[0] ?>"><img
                                            src="images/icon_menu/edit.png" border="0" width="20" height="20"
                                            align="absmiddle"/></a></td>
                            </tr>
                            <?php
                        }// END while($rows_rat = mysql_fetch_array($results_rat)){		
                        ?>


                        <?php
                        # Loop For Bus Transfer			
                        $sql_rct = "SELECT * ";
                        $sql_rct .= "FROM reservation_bustransfer_items ";
                        $sql_rct .= "where reservations_id = $row[0] ";


                        if (isset($sevice_name)) {
                            $sql_rct .= "AND rct_name like '%" . $sevice_name . "%' ";;
                        }
                        if (isset($service_date)) {
                            $sql_rct .= "AND rct_travel_date = '" . $service_date . "' ";
                        }
                        if (isset($reserv_producttype_id) && $reserv_producttype_id != '5') {
                            $sql_rct .= "AND rct_id = 0 ";
                        }


                        $sql_rct .= "order by rct_id asc ";
                        //echo "sql_rct : $sql_rct";


                        $wlinks = "";
                        //if($p_id){$wlinks .= "&p_id=$p_id";}


                        $results_rct = mysql_query($sql_rct);
                        while ($rows_rct = mysql_fetch_array($results_rct)) {

                            if ($color == $color_2) $color = $color_3; else $color = $color_2;
                            ?>
                            <tr bgcolor="<?= $color ?>">
                                <td align="center"><input type="checkbox" name="del_ids[]" value="<?= $row[0] ?>"/></td>
                                <td><a href="./?mode=reservations/view&id=<?= $row[0] ?>"><?= $row['res_id_str'] ?></a>
                                </td>
                                <td align="center"><?= DateFormat($row['res_date'], "s") ?></td>
                                <td><?= get_value('agents', ' ag_id', ' ag_name', $rows['agents_id']) ?></td>
                                <!--<td><?= $row['res_email'] ?></td>-->
                                <td><?= $row['res_fname'] ?></td>
                                <td><?= $row['res_lname'] ?></td>
                                <td><?= $rows_rct['rct_name'] ?></td>
                                <td align="center">
                                    <?php if ($row['company'] == 1) { ?>
                                        <?= get_value('cartransfers', 'ct_id', 'ac_andaman_adult', $rows_rct['bustransfers_id']) ?>
                                    <?php } elseif ($row['company'] == 2) { ?>
                                        <?= get_value('cartransfers', ' ct_id', ' ac_dotcom_adult', $rows_rct['bustransfers_id']) ?>
                                    <?php } elseif ($row['company'] == 3) { ?>
                                        <?= get_value('cartransfers', ' ct_id', ' ac_btoc_adult', $rows_rct['bustransfers_id']) ?>
                                    <?php } else { ?>

                                    <?php } ?>
                                </td>
                                <td align="center">
                                    <?php if ($row['company'] == 1) { ?>
                                        <?= get_value('cartransfers', ' ct_id', ' ac_andaman_child', $rows_rct['bustransfers_id']) ?>
                                    <?php } elseif ($row['company'] == 2) { ?>
                                        <?= get_value('cartransfers', ' ct_id', ' ac_dotcom_child', $rows_rct['bustransfers_id']) ?>
                                    <?php } elseif ($row['company'] == 3) { ?>
                                        <?= get_value('cartransfers', ' ct_id', ' ac_btoc_child', $rows_rct['bustransfers_id']) ?>
                                    <?php } else { ?>

                                    <?php } ?>
                                </td>
                                <td align="center"><?= DateFormat($rows_rct['rct_travel_date'], "s") ?></td>
                                <td><?= get_value('lis_booking_status', ' lis_id', ' lis_name', $rows['bookingstatus_id']) ?></td>
                                <td><?= get_value('lis_payment_status', ' lis_id', ' lis_name', $rows['paymentstatus_id']) ?></td>
                                <td align="right"><?= number_format($rows_rct['rct_prices'], 0) ?></td>
                                <td align="center"><a href="./?mode=reservations/view&id=<?= $row[0] ?>"><img
                                            src="images/icon_menu/edit.png" border="0" width="20" height="20"
                                            align="absmiddle"/></a></td>
                            </tr>
                            <?php
                        }// END while($rows_rct = mysql_fetch_array($results_rct)){		
                        ?>


                        <?php
                        # Loop For Private Land Transfer		
                        $sql_rplt = "SELECT * ";
                        $sql_rplt .= "FROM reservation_privatelandtransfer_items ";
                        $sql_rplt .= "where reservations_id = $row[0] ";


                        if (isset($sevice_name)) {
                            $sql_rplt .= "AND rplt_name like '%" . $sevice_name . "%' ";;
                        }
                        if (isset($service_date)) {
                            $sql_rplt .= "AND rplt_travel_date = '" . $service_date . "' ";
                        }
                        if (isset($reserv_producttype_id) && $reserv_producttype_id != '6') {
                            $sql_rplt .= "AND rplt_id = 0 ";
                        }


                        $sql_rplt .= "order by rplt_id asc ";
                        //echo "sql_rplt : $sql_rplt";


                        $wlinks = "";
                        //if($p_id){$wlinks .= "&p_id=$p_id";}


                        $results_rplt = mysql_query($sql_rplt);
                        while ($rows_rplt = mysql_fetch_array($results_rplt)) {

                            if ($color == $color_2) $color = $color_3; else $color = $color_2;
                            ?>
                            <tr bgcolor="<?= $color ?>">
                                <td align="center"><input type="checkbox" name="del_ids[]" value="<?= $row[0] ?>"/></td>
                                <td><a href="./?mode=reservations/view&id=<?= $row[0] ?>"><?= $row['res_id_str'] ?></a>
                                </td>
                                <td align="center"><?= DateFormat($row['res_date'], "s") ?></td>
                                <td><?= get_value('agents', ' ag_id', ' ag_name', $rows['agents_id']) ?></td>
                                <!--<td><?= $row['res_email'] ?></td>-->
                                <td><?= $row['res_fname'] ?></td>
                                <td><?= $row['res_lname'] ?></td>
                                <td><?= $rows_rplt['rplt_name'] ?></td>
                                <td align="center">
                                    <?php if ($row['company'] == 1) { ?>
                                        <?= get_value('privatelandtransfers', 'plt_id', 'ac_andaman_adult', $rows_rplt['privatelandtransfers_id']) ?>
                                    <?php } elseif ($row['company'] == 2) { ?>
                                        <?= get_value('privatelandtransfers', ' plt_id', ' ac_dotcom_adult', $rows_rplt['privatelandtransfers_id']) ?>
                                    <?php } elseif ($row['company'] == 3) { ?>
                                        <?= get_value('privatelandtransfers', ' plt_id', ' ac_btoc_adult', $rows_rplt['privatelandtransfers_id']) ?>
                                    <?php } else { ?>

                                    <?php } ?>
                                </td>
                                <td align="center">
                                    <?php if ($row['company'] == 1) { ?>
                                        <?= get_value('privatelandtransfers', ' plt_id', ' ac_andaman_child', $rows_rplt['privatelandtransfers_id']) ?>
                                    <?php } elseif ($row['company'] == 2) { ?>
                                        <?= get_value('privatelandtransfers', ' plt_id', ' ac_dotcom_child', $rows_rplt['privatelandtransfers_id']) ?>
                                    <?php } elseif ($row['company'] == 3) { ?>
                                        <?= get_value('privatelandtransfers', ' plt_id', ' ac_btoc_child', $rows_rplt['privatelandtransfers_id']) ?>
                                    <?php } else { ?>

                                    <?php } ?>
                                </td>
                                <td align="center"><?= DateFormat($rows_rplt['rplt_travel_date'], "s") ?></td>
                                <td><?= get_value('lis_booking_status', ' lis_id', ' lis_name', $rows['bookingstatus_id']) ?></td>
                                <td><?= get_value('lis_payment_status', ' lis_id', ' lis_name', $rows['paymentstatus_id']) ?></td>
                                <td align="right"><?= number_format($rows_rplt['rplt_prices'], 0) ?></td>
                                <td align="center"><a href="./?mode=reservations/view&id=<?= $row[0] ?>"><img
                                            src="images/icon_menu/edit.png" border="0" width="20" height="20"
                                            align="absmiddle"/></a></td>
                            </tr>
                            <?php
                        }// END while($rows_rplt = mysql_fetch_array($results_rplt)){		
                        ?>


                        <?php
                        # Loop For Hotel
                        $sql_rht = "SELECT * ";
                        $sql_rht .= "FROM reservation_hotel_items ";
                        $sql_rht .= "where reservations_id = $row[0] ";


                        if (isset($sevice_name)) {
                            $sql_rht .= "AND rht_name like '%" . $sevice_name . "%' ";;
                        }
                        if (isset($service_date)) {
                            $sql_rht .= "AND rht_check_in = '" . $service_date . "' ";
                        }
                        if (isset($reserv_producttype_id) && $reserv_producttype_id != '7') {
                            $sql_rht .= "AND rht_id = 0 ";
                        }


                        $sql_rht .= "order by rht_id asc ";
                        //echo "sql_rht : $sql_rht";


                        $wlinks = "";
                        //if($p_id){$wlinks .= "&p_id=$p_id";}


                        $results_rht = mysql_query($sql_rht);
                        while ($rows_rht = mysql_fetch_array($results_rht)) {

                            if ($color == $color_2) $color = $color_3; else $color = $color_2;
                            ?>
                            <tr bgcolor="<?= $color ?>">
                                <td align="center"><input type="checkbox" name="del_ids[]" value="<?= $row[0] ?>"/></td>
                                <td><a href="./?mode=reservations/view&id=<?= $row[0] ?>"><?= $row['res_id_str'] ?></a>
                                </td>
                                <td align="center"><?= DateFormat($row['res_date'], "s") ?></td>
                                <td><?= get_value('agents', ' ag_id', ' ag_name', $rows['agents_id']) ?></td>
                                <!--<td><?= $row['res_email'] ?></td>-->
                                <td><?= $row['res_fname'] ?></td>
                                <td><?= $row['res_lname'] ?></td>
                                <td><?= $rows_rht['rht_name'] ?></td>
                                <td align="center">
                                    <?php if ($row['company'] == 1) { ?>
                                        <?= get_value('hotels', ' hot_id', ' ac_andaman_adult', $rows_rht['hotels_id']) ?>
                                    <?php } elseif ($row['company'] == 2) { ?>
                                        <?= get_value('hotels', ' hot_id', ' ac_dotcom_adult', $rows_rht['hotels_id']) ?>
                                    <?php } elseif ($row['company'] == 3) { ?>
                                        <?= get_value('hotels', ' hot_id', ' ac_btoc_adult', $rows_rht['hotels_id']) ?>
                                    <?php } else { ?>

                                    <?php } ?>
                                </td>
                                <td align="center">
                                    <?php if ($row['company'] == 1) { ?>
                                        <?= get_value('hotels', ' hot_id', ' ac_andaman_child', $rows_rht['hotels_id']) ?>
                                    <?php } elseif ($row['company'] == 2) { ?>
                                        <?= get_value('hotels', ' hot_id', ' ac_dotcom_child', $rows_rht['hotels_id']) ?>
                                    <?php } elseif ($row['company'] == 3) { ?>
                                        <?= get_value('hotels', ' hot_id', ' ac_btoc_child', $rows_rht['hotels_id']) ?>
                                    <?php } else { ?>

                                    <?php } ?>
                                </td>
                                <td align="center"><?= DateFormat($rows_rht['rht_check_in'], "s") ?></td>
                                <td><?= get_value('lis_booking_status', ' lis_id', ' lis_name', $rows['bookingstatus_id']) ?></td>
                                <td><?= get_value('lis_payment_status', ' lis_id', ' lis_name', $rows['paymentstatus_id']) ?></td>
                                <td align="right"><?= number_format($rows_rht['rht_prices'], 0) ?></td>
                                <td align="center"><a href="./?mode=reservations/view&id=<?= $row[0] ?>"><img
                                            src="images/icon_menu/edit.png" border="0" width="20" height="20"
                                            align="absmiddle"/></a></td>
                            </tr>
                            <?php
                        }// END while($rows_rht = mysql_fetch_array($results_rht)){		
                        ?>


                        <?php
                        # Loop For Train Transfer			
                        $sql_rrt = "SELECT * ";
                        $sql_rrt .= "FROM reservation_traintransfer_items ";
                        $sql_rrt .= "where reservations_id = $row[0] ";


                        if (isset($sevice_name)) {
                            $sql_rrt .= "AND rrt_name like '%" . $sevice_name . "%' ";;
                        }
                        if (isset($service_date)) {
                            $sql_rrt .= "AND rrt_travel_date = '" . $service_date . "' ";
                        }
                        if (isset($reserv_producttype_id) && $reserv_producttype_id != '8') {
                            $sql_rrt .= "AND rrt_id = 0 ";
                        }


                        $sql_rrt .= "order by rrt_id asc ";
                        //echo "sql_rrt : $sql_rrt";


                        $wlinks = "";
                        //if($p_id){$wlinks .= "&p_id=$p_id";}


                        $results_rrt = mysql_query($sql_rrt);
                        while ($rows_rrt = mysql_fetch_array($results_rrt)) {

                            if ($color == $color_2) $color = $color_3; else $color = $color_2;
                            ?>
                            <tr bgcolor="<?= $color ?>">
                                <td align="center"><input type="checkbox" name="del_ids[]" value="<?= $row[0] ?>"/></td>
                                <td><a href="./?mode=reservations/view&id=<?= $row[0] ?>"><?= $row['res_id_str'] ?></a>
                                </td>
                                <td align="center"><?= DateFormat($row['res_date'], "s") ?></td>
                                <td><?= get_value('agents', ' ag_id', ' ag_name', $rows['agents_id']) ?></td>
                                <!--<td><?= $row['res_email'] ?></td>-->
                                <td><?= $row['res_fname'] ?></td>
                                <td><?= $row['res_lname'] ?></td>
                                <td><?= $rows_rrt['rrt_name'] ?></td>
                                <td align="center">
                                    <?php if ($row['company'] == 1) { ?>
                                        <?= get_value('traintransfers', 'train_id', 'ac_andaman_adult', $rows_rrt['traintransfers_id']) ?>
                                    <?php } elseif ($row['company'] == 2) { ?>
                                        <?= get_value('traintransfers', ' train_id', ' ac_dotcom_adult', $rows_rrt['traintransfers_id']) ?>
                                    <?php } elseif ($row['company'] == 3) { ?>
                                        <?= get_value('traintransfers', ' train_id', ' ac_btoc_adult', $rows_rrt['traintransfers_id']) ?>
                                    <?php } else { ?>

                                    <?php } ?>
                                </td>
                                <td align="center">
                                    <?php if ($row['company'] == 1) { ?>
                                        <?= get_value('traintransfers', ' train_id', ' ac_andaman_child', $rows_rrt['traintransfers_id']) ?>
                                    <?php } elseif ($row['company'] == 2) { ?>
                                        <?= get_value('traintransfers', ' train_id', ' ac_dotcom_child', $rows_rrt['traintransfers_id']) ?>
                                    <?php } elseif ($row['company'] == 3) { ?>
                                        <?= get_value('traintransfers', ' train_id', ' ac_btoc_child', $rows_rrt['traintransfers_id']) ?>
                                    <?php } else { ?>

                                    <?php } ?>
                                </td>
                                <td align="center"><?= DateFormat($rows_rrt['rrt_travel_date'], "s") ?></td>
                                <td><?= get_value('lis_booking_status', ' lis_id', ' lis_name', $rows['bookingstatus_id']) ?></td>
                                <td><?= get_value('lis_payment_status', ' lis_id', ' lis_name', $rows['paymentstatus_id']) ?></td>
                                <td align="right"><?= number_format($rows_rrt['rrt_prices'], 0) ?></td>
                                <td align="center"><a href="./?mode=reservations/view&id=<?= $row[0] ?>"><img
                                            src="images/icon_menu/edit.png" border="0" width="20" height="20"
                                            align="absmiddle"/></a></td>
                            </tr>
                            <?php
                        }// END while($rows_rrt = mysql_fetch_array($results_rrt)){		
                        ?>


                        <?php
                    } // END for ($start;$start<$result+1;$start++)
                    ?>
                    <tr bgcolor="<?= $color_1 ?>">
                        <td colspan="20"><input type="submit" name="submit_array" value="SAVE SETTING"
                                                style="width:125px;"
                                                onClick="return confirm('Do you want to do this ?')"></td>
                    </tr>
                </form>
                <?php
                }
                ?>
            </table>
            <!---- Listing Body ---->
        </td>
    </tr>
</table>