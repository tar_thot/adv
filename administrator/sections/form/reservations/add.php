<?php
include("value_get.php");
if (isset($id)) {
    $page_title = "Update Client Information ";
    $sql = "select * from reservations where res_id = $id";
    $result = mysql_query($sql);
    $row = mysql_fetch_array($result);
    include("inc.top.php");
} else {
    $page_title = "Create New Client Information ";
}
?>


<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td class="txt_big_gray"><?= $page_title ?></td>
    </tr>
</table>
<form action="process.php?mode=reservations/save&id=<?= $id ?>" onsubmit="MM_validateForm(
'check_res_ref','','R',
'res_name','','R',
'res_ref','','R'
);return document.MM_returnValue" name="form" method="post" enctype="multipart/form-data">
    <input type="hidden" name="agents_id" value="<?= $row['agents_id'] ?>"/>
    <table width="100%" border="0" cellspacing="0" cellpadding="3">

        <tr>
            <td width="200"></td>
            <td></td>
        </tr>


        <tr>
            <td align="right" class="txt_bold_gray">Raservetion ID:</td>
            <td><?= $row['res_id_str'] ?> </td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray">Agent :</td>
            <td>
                <select name="agents_id">
                    <option value="">-- Select --</option>
                    <?php listbox(agents, ag_id, ag_name, $row['agents_id'], N); ?>
                </select>
            </td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray">Title Name :</td>
            <td><select name="titlename_id">

                    <?php listbox('lis_titlename', 'lis_id', 'lis_name', $row['titlename_id'], 'N'); ?>
                </select>
            </td>
        </tr>


        <tr>
            <td align="right" class="txt_bold_gray">First Name :</td>
            <td><input type="text" name="res_fname" value="<?= $row['res_fname'] ?>" size="50">
            </td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray">Last Name :</td>
            <td><input type="text" name="res_lname" value="<?= $row['res_lname'] ?>" size="50">
            </td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray">Email :</td>
            <td><input type="text" name="res_email" value="<?= $row['res_email'] ?>" size="50">
            </td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray">Phone Number :</td>
            <td><input type="text" name="res_phone" value="<?= $row['res_phone'] ?>" size="50">
            </td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray">Fax Number :</td>
            <td><input type="text" name="res_fax" value="<?= $row['res_fax'] ?>" size="50">
            </td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray">Booking By :</td>
            <td><input type="text" name="res_book_by" value="<?= $row['res_book_by'] ?>" size="50">
            </td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray">Agent Voucher :</td>
            <td><input type="text" name="res_agent_voucher" value="<?= $row['res_agent_voucher'] ?>" size="50">
            </td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray">Cash Collection :</td>
            <td><input type="text" name="res_cash_collection" value="<?= $row['res_cash_collection'] ?>" size="50">
            </td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray">Address :</td>
            <td><textarea cols="70" rows="10" name="res_address"><?= $row['res_address'] ?></textarea>
            </td>
        </tr>


        <tr>
            <td align="right" class="txt_bold_gray">Remark :</td>
            <td><textarea cols="70" rows="10" name="res_request"><?= $row['res_request'] ?></textarea>
            </td>
        </tr>

        <tr>
            <?php
            if ($row['res_date'] > 0) {
                $res_date = $row['res_date'];
            } else {
                $res_date = $today;
            }
            ?>
            <td align="right" class="txt_bold_gray" width="317"> Book Date :</td>
            <td>
                <script>DateInput('res_date', false, 'yyyy-mm-dd', '<?= $res_date?>')</script>
            </td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray">Booking Status :</td>
            <td><select name="bookingstatus_id">
                    <option value="">-- Select --</option>
                    <?php listbox('lis_booking_status', 'lis_id', 'lis_name', $row['bookingstatus_id'], 'N'); ?>
                </select>
            </td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray">Note :</td>
            <td><textarea cols="70" rows="10" name="res_note"><?= $row['res_note'] ?></textarea>
            </td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray">Payment Status :</td>
            <td><select name="paymentstatus_id">
                    <option value="">-- Select --</option>
                    <?php listbox('lis_payment_status', 'lis_id', 'lis_name', $row['paymentstatus_id'], 'N'); ?>
                </select>
            </td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray">Company :</td>
            <td><select name="company">
                    <option value="">-- Select --</option>
                    <?php listbox('lis_company', 'lis_id', 'lis_name', $row['company'], 'N'); ?>
                </select>
            </td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray">Comfirmed By :</td>
            <td><input type="text" name="confirmed_by" value="<?= $row['confirmed_by'] ?>" size="50">
            </td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray">Prepared By :</td>
            <td><input type="text" name="prepared_by" value="<?= $row['prepared_by'] ?>" size="50">
            </td>
        </tr>

        <tr>
            <td align="right" class="txt_bold_gray">Confirm Payment :</td>
            <td><select name="confirm_pay_id">
                    <option value="">-- Select --</option>
                    <?php listbox('lis_comfirm_payment', 'lis_id', 'lis_name', $row['confirm_pay_id'], 'N'); ?>
                </select>
            </td>
        </tr>

        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td><input type="submit" name="submit" value="SUBMIT" style="width:75px;"><?php
                if (isset($id)) echo " <input type=\"submit\" name=\"del\" value=\"DELETE\" style=\"width:75px;\" onClick=\"return confirm('Do you want to delete this record ?')\">";
                else echo " <input type=\"reset\" name=\"Reset\" value=\"RESET\" style=\"width:75px;\">";
                ?></td>
        </tr>
    </table>
</form>
<script language="JavaScript" type="text/JavaScript">
    <!--
    function MM_findObj(n, d) { //v4.01
        var p, i, x;
        if (!d) d = document;
        if ((p = n.indexOf("?")) > 0 && parent.frames.length) {
            d = parent.frames[n.substring(p + 1)].document;
            n = n.substring(0, p);
        }
        if (!(x = d[n]) && d.all) x = d.all[n];
        for (i = 0; !x && i < d.forms.length; i++) x = d.forms[i][n];
        for (i = 0; !x && d.layers && i < d.layers.length; i++) x = MM_findObj(n, d.layers[i].document);
        if (!x && d.getElementById) x = d.getElementById(n);
        return x;
    }
    function MM_validateForm() { //v4.0
        var i, p, q, nm, test, num, min, max, errors = '', args = MM_validateForm.arguments;
        for (i = 0; i < (args.length - 2); i += 3) {
            test = args[i + 2];
            val = MM_findObj(args[i]);
            if (val) {
                nm = val.name;
                if ((val = val.value) != "") {
                    if (test.indexOf('isEmail') != -1) {
                        p = val.indexOf('@');
                        if (p < 1 || p == (val.length - 1)) errors += '- E-mail must contain an e-mail address.\n';
                    } else if (test != 'R') {
                        num = parseFloat(val);
                        if (isNaN(val)) {
                            if (nm == 'category_id') nm = 'Category';
                            errors += '- ' + nm + ' must contain a number.\n';
                        }
                        if (test.indexOf('inRange') != -1) {
                            p = test.indexOf(':');
                            min = test.substring(8, p);
                            max = test.substring(p + 1);
                            if (num < min || max < num) errors += '- ' + nm + ' must contain a number between ' + min + ' and ' + max + '.\n';
                        }
                    }
                } else if (test.charAt(0) == 'R') {
                    if (nm == 'category_id') nm = 'Category';
                    if (nm == 'res_name') nm = 'Name';
                    if (nm == 'res_ref') nm = 'Ref';
                    if (nm == 'check_res_ref') nm = 'Ref';
                    if (nm == 'activitypricetype_id') nm = 'Price Type';
                    errors += '- ' + nm + ' is required.\n';
                }
            }
        }
        if (errors) alert('The following error(s) occurred:\n' + errors);
        document.MM_returnValue = (errors == '');
    }
    //-->
</script>