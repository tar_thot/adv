<table width="100%" border="0" cellspacing="1" cellpadding="3">
    <tr>
        <td><strong>Type : </strong></td>
        <td width="61%">
            Boat Transfer
        </td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td><strong>Name : </strong></td>
        <td width="61%">
            <?= $rows['rbt_name'] ?></td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td><strong>Ref : </strong></td>
        <td width="61%">
            <?= $rows['rbt_ref'] ?></td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td><strong>Travel Date : </strong></td>
        <td width="61%">
            <?= DateFormat($rows['rbt_travel_date'], "s") ?></td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td><strong>Rate Type : </strong></td>
        <td width="61%">
            <?= $rows['rbt_ratetype'] ?></td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td width="12%"><strong>Numbers of guests : </strong></td>
        <td><?= number_format($rows['rbt_adult_num'], 0) ?> Adults + <?= number_format($rows['rbt_child_num']) ?>
            Children + <?= number_format($rows['rbt_infant_num']) ?> Infant
        </td>
        <td width="10%" align="right"><strong>Subtotal : </strong></td>
        <td width="15%" align="right"><?= number_format($rows['rbt_prices'], 0) ?> THB
            <?php
            $total_prices += $rows['rbt_prices'];
            ?>
        </td>
        <td width="2%">&nbsp;</td>
    </tr>
    <tr>
        <td colspan="5" bgcolor="#000000" height="1" style="padding:0px;"></td>
    </tr>
</table>
