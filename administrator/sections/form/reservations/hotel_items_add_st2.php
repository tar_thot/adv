<?php
include("value_get.php");
if (isset($id)) {
    $sql = "select * from reservations where res_id = $id";
    $result = mysql_query($sql);
    $row = mysql_fetch_array($result);

    $rht_name = $_POST['rht_name'];
    $current_rht_name = $_POST['current_rht_name'];

    include("inc.top.php");
    //if ($rht_id)
    if ($current_rht_name == $rht_name) {
        $page_title = "Update Reservation Hotel";
        $sql = "select * from reservation_hotel_items where rht_id = $rht_id";
        $result = mysql_query($sql);
        $row = mysql_fetch_array($result);

        //echo "sql : $sql";
    } else {
        $row = "";
        $page_title = "Create New Reservation Hotel";
    }
    ?>


    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="txt_big_gray"><?= $page_title ?></td>
        </tr>
    </table>

    <?php
    $sql_hot = "select * from hotels where hot_name = '$rht_name'";
    $result_hot = mysql_query($sql_hot);
    $row_hot = mysql_fetch_array($result_hot);
    ?>

    <form action="process.php?mode=reservations/hotel_items_save&id=<?= $id ?>&rht_id=<?= $rht_id ?>" onsubmit="MM_validateForm(
'tourt_name','','R'
);return document.MM_returnValue" name="form" method="post" enctype="multipart/form-data">
        <input type="hidden" name="last_edit_by" value="<?= $perms["NAME"] ?>(Back Office System)"/>
        <input type="hidden" name="rht_hot_id" value="<?= $row_hot['hot_id'] ?>"/>
        <input type="hidden" name="rht_name" value="<?= $rht_name ?>"/>
        <input type="hidden" name="rht_ref" value="<?= $row_hot['hot_ref'] ?>"/>

        <table width="100%" border="0" cellspacing="0" cellpadding="3">
            <tr>
                <td align="right" class="txt_bold_gray" width="317">Name :</td>
                <td width="1076"><?= $rht_name ?></td>
            </tr>

            <tr>
                <td align="right" class="txt_bold_gray">Ref :</td>
                <td><?= $row_hot['hot_ref'] ?></td>
            </tr>

            <tr>
                <?php
                if ($row['rht_check_in'] > 0) {
                    $rht_check_in = $row['rht_check_in'];
                } else {
                    $rht_check_in = $today;
                }
                ?>
                <td align="right" class="txt_bold_gray"> Check In Date :</td>
                <td>
                    <script>DateInput('rht_check_in', false, 'yyyy-mm-dd', '<?= $rht_check_in?>')</script>
                </td>
            </tr>

            <tr>
                <?php
                if ($row['rht_check_out'] > 0) {
                    $rht_check_out = $row['rht_check_out'];
                } else {
                    $rht_check_out = $today;
                }
                ?>
                <td align="right" class="txt_bold_gray"> Check Out Date :</td>
                <td>
                    <script>DateInput('rht_check_out', false, 'yyyy-mm-dd', '<?= $rht_check_out?>')</script>
                </td>
            </tr>

            <tr>
                <td align="right" class="txt_bold_gray">Rate Type :</td>
                <td>
                    <select name="rht_ratetype">
                        <option value="">-- Select --</option>
                        <?php
                        $sql_rate = "select * from hotel_ratetypes where hotels_id = " . $row_hot['hot_id'] . " ";
                        $result_rate = mysql_query($sql_rate);
                        while ($row_rate = mysql_fetch_array($result_rate)) {
                            ?>
                            <option
                                value="<?= $row_rate['hotrt_name'] ?>" <?php if ($row_rate['hotrt_name'] == $row['rht_ratetype']) {
                                echo 'selected="selected"';
                            } ?>><?= $row_rate['hotrt_name'] ?></option>
                        <?php } ?>
                    </select>
                </td>
            </tr>

            <tr>
                <td align="right" class="txt_bold_gray">Room :</td>
                <td><select name="rht_room_num">

                        <?php list_number($row['rht_room_num'], 1, 10); ?>
                    </select></td>
            </tr>

            <tr>
                <td align="right" class="txt_bold_gray">Adult :</td>
                <td><select name="rht_adult_num">

                        <?php list_number($row['rht_adult_num'], 1, 10); ?>
                    </select></td>
            </tr>

            <tr>
                <td align="right" class="txt_bold_gray">Child :</td>
                <td><select name="rht_child_num">

                        <?php list_number($row['rht_child_num'], 0, 11); ?>
                    </select></td>
            </tr>

            <tr>
                <td align="right" class="txt_bold_gray">Infant :</td>
                <td><select name="rht_infant_num">

                        <?php list_number($row['rht_infant_num'], 0, 11); ?>
                    </select></td>
            </tr>

            <tr>
                <td align="right" class="txt_bold_gray">G :</td>
                <td><select name="rht_g_num">

                        <?php list_number($row['rht_g_num'], 0, 11); ?>
                    </select></td>
            </tr>

            <tr>
                <td align="right" class="txt_bold_gray">FOC :</td>
                <td><select name="rht_foc_num">

                        <?php list_number($row['rht_foc_num'], 0, 11); ?>
                    </select></td>
            </tr>

            <tr>
                <td align="right" class="txt_bold_gray">Price :</td>
                <td><input type="text" name="rht_prices" value="<?= $row['rht_prices'] ?>" size="50"/></td>
            </tr>


            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td><input type="submit" name="submit" value="SUBMIT" style="width:75px;"><?php
                    echo " <input type=\"reset\" name=\"Reset\" value=\"RESET\" style=\"width:75px;\">";
                    ?></td>
            </tr>
        </table>
    </form>
    <?php
}
?>
<script language="JavaScript" type="text/JavaScript">
    <!--
    function MM_findObj(n, d) { //v4.01
        var p, i, x;
        if (!d) d = document;
        if ((p = n.indexOf("?")) > 0 && parent.frames.length) {
            d = parent.frames[n.substring(p + 1)].document;
            n = n.substring(0, p);
        }
        if (!(x = d[n]) && d.all) x = d.all[n];
        for (i = 0; !x && i < d.forms.length; i++) x = d.forms[i][n];
        for (i = 0; !x && d.layers && i < d.layers.length; i++) x = MM_findObj(n, d.layers[i].document);
        if (!x && d.getElementById) x = d.getElementById(n);
        return x;
    }
    function MM_validateForm() { //v4.0
        var i, p, q, nm, test, num, min, max, errors = '', args = MM_validateForm.arguments;
        for (i = 0; i < (args.length - 2); i += 3) {
            test = args[i + 2];
            val = MM_findObj(args[i]);
            if (val) {
                nm = val.name;
                if ((val = val.value) != "") {
                    if (test.indexOf('isEmail') != -1) {
                        p = val.indexOf('@');
                        if (p < 1 || p == (val.length - 1)) errors += '- E-mail must contain an e-mail address.\n';
                    } else if (test != 'R') {
                        num = parseFloat(val);
                        if (isNaN(val)) {
                            if (nm == 'name') nm = 'Room Type';
                            errors += '- ' + nm + ' must contain a number.\n';
                        }
                        if (test.indexOf('inRange') != -1) {
                            p = test.indexOf(':');
                            min = test.substring(8, p);
                            max = test.substring(p + 1);
                            if (num < min || max < num) errors += '- ' + nm + ' must contain a number between ' + min + ' and ' + max + '.\n';
                        }
                    }
                } else if (test.charAt(0) == 'R') {
                    if (nm == 'tourt_name') nm = 'Reservation Boat Transfer';
                    errors += '- ' + nm + ' is required.\n';
                }
            }
        }
        if (errors) alert('The following error(s) occurred:\n' + errors);
        document.MM_returnValue = (errors == '');
    }
    //-->
</script>