<?php
include("value_get.php");
if (isset($id)) {
    $sql = "select * from reservations where res_id = $id";
    $result = mysql_query($sql);
    $row = mysql_fetch_array($result);

    $rat_name = $_POST['rat_name'];
    $current_rat_name = $_POST['current_rat_name'];

    include("inc.top.php");
    //if ($rat_id)
    if ($current_rat_name == $rat_name) {
        $page_title = "Update Reservation Activity";
        $sql = "select * from reservation_activity_items where rat_id = $rat_id";
        $result = mysql_query($sql);
        $row = mysql_fetch_array($result);

        //echo "sql : $sql";
    } else {
        $row = "";
        $page_title = "Create New Reservation Activity";
    }
    ?>

    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="txt_big_gray"><?= $page_title ?></td>
        </tr>
    </table>

    <?php
    $sql_act = "select * from activities where act_name = '$rat_name'";
    $result_act = mysql_query($sql_act);
    $row_act = mysql_fetch_array($result_act);
    ?>

    <form action="process.php?mode=reservations/activity_items_save&id=<?= $id ?>&rat_id=<?= $rat_id ?>" onsubmit="MM_validateForm(
'tourt_name','','R'
);return document.MM_returnValue" name="form" method="post" enctype="multipart/form-data">
        <input type="hidden" name="last_edit_by" value="<?= $perms["NAME"] ?>(Back Office System)"/>
        <input type="hidden" name="rat_act_id" value="<?= $row_act['act_id'] ?>"/>
        <input type="hidden" name="rat_name" value="<?= $rat_name ?>"/>
        <input type="hidden" name="rat_ref" value="<?= $row_act['act_ref'] ?>"/>

        <table width="100%" border="0" cellspacing="0" cellpadding="3">
            <tr>
                <td align="right" class="txt_bold_gray" width="317">Name :</td>
                <td width="1076"><?= $rat_name ?></td>
            </tr>


            <tr>
                <td align="right" class="txt_bold_gray">Ref :</td>
                <td><?= $row_act['act_ref'] ?></td>
            </tr>

            <tr>
                <?php
                if ($row['rat_travel_date'] > 0) {
                    $rat_travel_date = $row['rat_travel_date'];
                } else {
                    $rat_travel_date = $today;
                }
                ?>
                <td align="right" class="txt_bold_gray"> Travel Date :</td>
                <td>
                    <script>DateInput('rat_travel_date', false, 'yyyy-mm-dd', '<?= $rat_travel_date?>')</script>
                </td>
            </tr>

            <tr>
                <td align="right" class="txt_bold_gray">Rate Type :</td>
                <td>
                    <select name="rat_ratetype">
                        <option value="">-- Select --</option>
                        <?php
                        $sql_rate = "select * from activity_ratetypes where activities_id = " . $row_act['act_id'];
                        $result_rate = mysql_query($sql_rate);
                        while ($row_rate = mysql_fetch_array($result_rate)) {
                            ?>
                            <option
                                value="<?= $row_rate['actrt_name'] ?>" <?php if ($row_rate['actrt_name'] == $row['rat_ratetype']) {
                                echo 'selected="selected"';
                            } ?>><?= $row_rate['actrt_name'] ?></option>
                        <?php } ?>
                    </select>
                </td>
            </tr>

            <tr>
                <td align="right" class="txt_bold_gray">Adult :</td>
                <td><select name="rat_adult_num">

                        <?php list_number($row['rat_adult_num'], 1, 10); ?>
                    </select></td>
            </tr>

            <tr>
                <td align="right" class="txt_bold_gray">Child :</td>
                <td><select name="rat_child_num">

                        <?php list_number($row['rat_child_num'], 0, 11); ?>
                    </select></td>
            </tr>

            <tr>
                <td align="right" class="txt_bold_gray">Infant :</td>
                <td><select name="rat_infant_num">

                        <?php list_number($row['rat_infant_num'], 0, 11); ?>
                    </select></td>
            </tr>

            <tr>
                <td align="right" class="txt_bold_gray">G :</td>
                <td><select name="rat_g_num">

                        <?php list_number($row['rat_g_num'], 0, 11); ?>
                    </select></td>
            </tr>

            <tr>
                <td align="right" class="txt_bold_gray">FOC :</td>
                <td><select name="rat_foc_num">

                        <?php list_number($row['rat_foc_num'], 0, 11); ?>
                    </select></td>
            </tr>

            <tr>
                <td align="right" class="txt_bold_gray">Price :</td>
                <td><input type="text" name="rat_prices" value="<?= $row['rat_prices'] ?>" size="50"/></td>
            </tr>


            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td><input type="submit" name="submit" value="SUBMIT" style="width:75px;"><?php
                    echo " <input type=\"reset\" name=\"Reset\" value=\"RESET\" style=\"width:75px;\">";
                    ?></td>
            </tr>
        </table>
    </form>
    <?php
}
?>
<script language="JavaScript" type="text/JavaScript">
    <!--
    function MM_findObj(n, d) { //v4.01
        var p, i, x;
        if (!d) d = document;
        if ((p = n.indexOf("?")) > 0 && parent.frames.length) {
            d = parent.frames[n.substring(p + 1)].document;
            n = n.substring(0, p);
        }
        if (!(x = d[n]) && d.all) x = d.all[n];
        for (i = 0; !x && i < d.forms.length; i++) x = d.forms[i][n];
        for (i = 0; !x && d.layers && i < d.layers.length; i++) x = MM_findObj(n, d.layers[i].document);
        if (!x && d.getElementById) x = d.getElementById(n);
        return x;
    }
    function MM_validateForm() { //v4.0
        var i, p, q, nm, test, num, min, max, errors = '', args = MM_validateForm.arguments;
        for (i = 0; i < (args.length - 2); i += 3) {
            test = args[i + 2];
            val = MM_findObj(args[i]);
            if (val) {
                nm = val.name;
                if ((val = val.value) != "") {
                    if (test.indexOf('isEmail') != -1) {
                        p = val.indexOf('@');
                        if (p < 1 || p == (val.length - 1)) errors += '- E-mail must contain an e-mail address.\n';
                    } else if (test != 'R') {
                        num = parseFloat(val);
                        if (isNaN(val)) {
                            if (nm == 'name') nm = 'Room Type';
                            errors += '- ' + nm + ' must contain a number.\n';
                        }
                        if (test.indexOf('inRange') != -1) {
                            p = test.indexOf(':');
                            min = test.substring(8, p);
                            max = test.substring(p + 1);
                            if (num < min || max < num) errors += '- ' + nm + ' must contain a number between ' + min + ' and ' + max + '.\n';
                        }
                    }
                } else if (test.charAt(0) == 'R') {
                    if (nm == 'tourt_name') nm = 'Reservation Activity';
                    errors += '- ' + nm + ' is required.\n';
                }
            }
        }
        if (errors) alert('The following error(s) occurred:\n' + errors);
        document.MM_returnValue = (errors == '');
    }
    //-->
</script>
