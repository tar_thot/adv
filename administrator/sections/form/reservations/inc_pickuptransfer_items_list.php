<table width="100%" border="0" cellspacing="1" cellpadding="3">
    <tr>
        <td><strong>Type : </strong></td>
        <td width="61%">
            Pick Up Transfer
        </td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td><strong>Name : </strong></td>
        <td width="61%">
            <?= $rows['rpt_name'] ?></td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td><strong>Ref : </strong></td>
        <td width="61%">
            <?= $rows['rpt_ref'] ?></td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td><strong>Travel Date : </strong></td>
        <td width="61%">
            <?= DateFormat($rows['rpt_travel_date'], "s") ?></td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td><strong>Rate Type : </strong></td>
        <td width="61%">
            <?= $rows['rpt_ratetype'] ?></td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>

    <?php
    if ($rows['rpt_car_num'] > 0) {
        ?>
        <tr>
            <td><strong>Car : </strong></td>
            <td width="61%">
                <?= number_format($rows['rpt_car_num'], 0) ?></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <?php
    } // END  if($rows[rpt_car_num] > 0){	
    ?>

    <tr>
        <td width="12%"><strong>Numbers of guests : </strong></td>
        <td><?= number_format($rows['rpt_adult_num'], 0) ?> Adults + <?= number_format($rows['rpt_child_num']) ?>
            Children
            + <?= number_format($rows['rpt_infant_num']) ?> Infant
        </td>
        <td width="10%" align="right"><strong>Subtotal : </strong></td>
        <td width="15%" align="right"><?= number_format($rows['rpt_prices'], 0) ?> THB
            <?php
            $total_prices += $rows['rpt_prices'];
            ?>
        </td>
        <td width="2%">&nbsp;</td>
    </tr>
    <tr>
        <td colspan="5" bgcolor="#000000" height="1" style="padding:0px;"></td>
    </tr>
</table>
