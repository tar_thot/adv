<?php
include("value_get.php");
if ($id) {
    $page_title = "View Reservation ";
    $sql = "select * from reservations where res_id = $id";
    $result = mysql_query($sql);
    $row = mysql_fetch_array($result);
    include("inc.top.php");
}

$total_prices = 0;
?>

<script language="JavaScript" type="text/JavaScript">
    <!--
    function MM_findObj(n, d) { //v4.01
        var p, i, x;
        if (!d) d = document;
        if ((p = n.indexOf("?")) > 0 && parent.frames.length) {
            d = parent.frames[n.substring(p + 1)
            '].document; n=n.substring(0,p);}
            if (!(x = d[n'])&&d.all) x=d.all[n'
        ]
            ;
            for (i = 0; !x && i < d.forms.length; i++) x = d.forms[i
            '][n'
        ]
            ;
            for (i = 0; !x && d.layers && i < d.layers.length; i++) x = MM_findObj(n, d.layers[i
            '].document);
            if (!x && d.getElementById) x = d.getElementById(n);
            return x;
        }
        function MM_validateForm() { //v4.0
            var i, p, q, nm, test, num, min, max, errors = '', args = MM_validateForm.arguments;
            for (i = 0; i < (args.length - 2); i += 3) {
                test = args[i + 2
                ']; val=MM_findObj(args[i'
            ])
    ;
    if (val) {
        nm = val.name;
        if ((val = val.value) != "") {
            if (test.indexOf('isEmail') != -1) {
                p = val.indexOf('@');
                if (p < 1 || p == (val.length - 1)) errors += '- E-mail must contain an e-mail address.\n';
            } else if (test != 'R') {
                num = parseFloat(val);
                if (isNaN(val)) {
                    if (nm == 'category_id') nm = 'Category';
                    errors += '- ' + nm + ' must contain a number.\n';
                }
                if (test.indexOf('inRange') != -1) {
                    p = test.indexOf(':');
                    min = test.substring(8, p);
                    max = test.substring(p + 1);
                    if (num < min || max < num) errors += '- ' + nm + ' must contain a number between ' + min + ' and ' + max + '.\n';
                }
            }
        } else if (test.charAt(0) == 'R') {
            if (nm == 'category_id') nm = 'Category';
            if (nm == 'res_name') nm = 'Name';
            if (nm == 'res_ref') nm = 'Ref';
            if (nm == 'check_res_ref') nm = 'Ref';
            if (nm == 'activitypricetype_id') nm = 'Price Type';
            errors += '- ' + nm + ' is required.\n';
        }
    }
    }
    if (errors) alert('The following error(s) occurred:\n' + errors);
    document.MM_returnValue = (errors == '');
    }
    //-->
</script>


<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td class="txt_big_gray"><?= $page_title ?></td>
    </tr>
</table>


<table width="100%" border="0" cellspacing="1" cellpadding="3">
    <tr>
        <td colspan="5" bgcolor="#000000" height="1" style="padding:0px;"></td>
    </tr>
</table>
<br/>

<!-- *************************************************Start Item List************************************************* -->


<!-- Start Boat Transfer Items List -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td class="txt_big_gray">Reservation Boat Transfer List [ <a
                href="./?mode=reservations/boattransfer_items_add_st1&id=<?= $id ?>">Create New Reservation Boat
                Transfer </a> ']
        </td>
    </tr>
</table>
<table width="100%" border="0" cellspacing="1" cellpadding="3" bgcolor="#FFFFFF">
    <?php

    $row = "";
    $sql = "select * from reservation_boattransfer_items where reservations_id = $id order by rbt_id ASC";
    $result = mysql_query($sql);
    $numrows = mysql_num_rows($result);
    $i = 0;
    while ($row = mysql_fetch_array($result))
    {
    if ($color == $color_2) $color = $color_3; else $color = $color_2;
    if ($i == 0)
    {
    ?>
    <form name="form" action="process.php?mode=reservations/boattransfer_items_save&id=<?= $id ?>" method="post"
          enctype="multipart/form-data">
        <tr bgcolor="<?= $color_1 ?>">
            <td class="txt_bold_white" align="center" width="50">Delete</td>
            <td class="txt_bold_white">Name</td>
            <td class="txt_bold_white">Ticket No.</td>
            <td class="txt_bold_white">Ref</td>
            <td class="txt_bold_white">Travel&nbsp;Date</td>
            <td class="txt_bold_white">Time</td>
            <td class="txt_bold_white">Rate Type</td>
            <td class="txt_bold_white" align="center">Adult</td>
            <td class="txt_bold_white" align="center">Child</td>
            <td class="txt_bold_white" align="center">Infant</td>
            <td class="txt_bold_white" align="center">G</td>
            <td class="txt_bold_white" align="center">FOC</td>
            <td class="txt_bold_white" align="right">Price</td>

            <td class="txt_bold_white" width="50">&nbsp;</td>
        </tr>
        <?php
        }
        ?>
        <tr bgcolor="<?= $color ?>">
            <td align="center"><input type="checkbox" name="del_ids[']" value="<?= $row[0] ?>"/></td>
            <td>
                <a href="./?mode=reservations/boattransfer_items_add_st1&id=<?= $id ?>&rbt_id=<?= $row[0] ?>"><?= $row['rbt_name'] ?></a>
            </td>
            <td><?= $row['rbt_ticket'] ?></td>
            <td><?= $row['rbt_ref'] ?></td>
            <td><?= DateFormat($row['rbt_travel_date'], "s") ?></td>
            <td><?= $row['rbt_time'] ?></td>
            <td><?= $row['rbt_ratetype'] ?></td>
            <td align="center"><?= $row['rbt_adult_num'] ?></td>
            <td align="center"><?= $row['rbt_child_num'] ?></td>
            <td align="center"><?= $row['rbt_infant_num'] ?></td>
            <td align="center"><?= $row['rbt_g_num'] ?></td>
            <td align="center"><?= $row['rbt_foc_num'] ?></td>
            <td align="right"><?= number_format($row['rbt_prices'], 2) ?>
                <?php
                $total_prices += $row['rbt_prices'];
                ?>
            </td>
            <td align="center"><a
                    href="./?mode=reservations/boattransfer_items_add_st1&id=<?= $id ?>&rbt_id=<?= $row[0] ?>"><img
                        src="images/icon_menu/edit.png" border="0" width="20" height="20" alt="Edit" title="Edit"/></a>
            </td>
        </tr>
        <?php
        $i++;
        }
        if ($i != 0)
        {
        ?>
        <tr bgcolor="<?= $color_1 ?>">
            <td colspan="20"><input type="submit" name="submit_array" value="SAVE SETTING" style="width:125px;"
                                    onClick="return confirm('Do you want to do this ?')"></td>
        </tr>
    </form>
<?php
}
else {
    ?>
    <tr bgcolor="<?= $color_3 ?>" height="100">
        <td align="center">Record Not Found!</td>
    </tr>
    <?php
}
?>
</table>
<!-- END Boat Transfer Items List -->


<br/>

<!-- Start Pick Up Transfer Items List -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td class="txt_big_gray">Reservation Pick Up Transfer List [ <a
                href="./?mode=reservations/pickuptransfer_items_add_st1&id=<?= $id ?>">Create New Reservation Pick Up
                Transfer </a> ']
        </td>
    </tr>
</table>

<table width="100%" border="0" cellspacing="1" cellpadding="3" bgcolor="#FFFFFF">
    <?php

    $row = "";
    $sql = "select * from reservation_pickuptransfer_items where reservations_id = $id order by rpt_id ASC";
    $result = mysql_query($sql);
    $numrows = mysql_num_rows($result);
    $i = 0;
    while ($row = mysql_fetch_array($result))
    {
    if ($color == $color_2) $color = $color_3; else $color = $color_2;
    if ($i == 0)
    {
    ?>
    <form name="form" action="process.php?mode=reservations/pickuptransfer_items_save&id=<?= $id ?>" method="post"
          enctype="multipart/form-data">
        <tr bgcolor="<?= $color_1 ?>">
            <td class="txt_bold_white" align="center" width="50">Delete</td>
            <td class="txt_bold_white">Name</td>
            <td class="txt_bold_white">Ref</td>
            <td class="txt_bold_white">Travel&nbsp;Date</td>
            <td class="txt_bold_white">Rate Type</td>
            <td class="txt_bold_white" align="center">Car</td>
            <td class="txt_bold_white" align="center">Adult</td>
            <td class="txt_bold_white" align="center">Child</td>
            <td class="txt_bold_white" align="center">Infant</td>
            <td class="txt_bold_white" align="center">G</td>
            <td class="txt_bold_white" align="center">FOC</td>
            <td class="txt_bold_white">Pick Up From</td>
            <td class="txt_bold_white">Drop-off to</td>
            <td class="txt_bold_white">Pick Up Time</td>
            <td class="txt_bold_white" align="right">Price</td>

            <td class="txt_bold_white" width="50">&nbsp;</td>
        </tr>
        <?php
        }
        ?>
        <tr bgcolor="<?= $color ?>">
            <td align="center"><input type="checkbox" name="del_ids[']" value="<?= $row[0] ?>"/></td>
            <td>
                <a href="./?mode=reservations/pickuptransfer_items_add_st1&id=<?= $id ?>&rpt_id=<?= $row[0] ?>"><?= $row['rpt_name'] ?></a>
            </td>
            <td><?= $row['rpt_ref'] ?></td>
            <td><?= DateFormat($row['rpt_travel_date'], "s") ?></td>
            <td><?= $row['rpt_ratetype'] ?></td>
            <td align="center"><?php if ($row['rpt_car_num']) {
                    echo $row['rpt_car_num'];
                } ?></td>
            <td align="center"><?= $row['rpt_adult_num'] ?></td>
            <td align="center"><?= $row['rpt_child_num'] ?></td>
            <td align="center"><?= $row['rpt_infant_num'] ?></td>
            <td align="center"><?= $row['rpt_g_num'] ?></td>
            <td align="center"><?= $row['rpt_foc_num'] ?></td>
            <td><?= $row['rpt_pickupfrom'] ?></td>
            <td><?= $row['rpt_pickupto'] ?></td>
            <td><?= $row['rpt_pickuptime'] ?></td>
            <td align="right"><?= number_format($row['rpt_prices'], 2) ?>
                <?php
                $total_prices += $row['rpt_prices'];
                ?>
            </td>
            <td align="center"><a
                    href="./?mode=reservations/pickuptransfer_items_add_st1&id=<?= $id ?>&rpt_id=<?= $row[0] ?>"><img
                        src="images/icon_menu/edit.png" border="0" width="20" height="20" alt="Edit" title="Edit"/></a>
            </td>
        </tr>
        <?php
        $i++;
        }
        if ($i != 0)
        {
        ?>
        <tr bgcolor="<?= $color_1 ?>">
            <td colspan="20"><input type="submit" name="submit_array" value="SAVE SETTING" style="width:125px;"
                                    onClick="return confirm('Do you want to do this ?')"></td>
        </tr>
    </form>
<?php
}
else {
    ?>
    <tr bgcolor="<?= $color_3 ?>" height="100">
        <td align="center">Record Not Found!</td>
    </tr>
    <?php
}
?>
</table>

<!-- END Pick Up Transfer Items List -->


<br/>


<!-- Start Tour Items List -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td class="txt_big_gray">Reservation Tour List [ <a
                href="./?mode=reservations/tour_items_add_st1&id=<?= $id ?>">Create New Reservation Tour </a> ']
        </td>
    </tr>
</table>
<table width="100%" border="0" cellspacing="1" cellpadding="3" bgcolor="#FFFFFF">
    <?php

    $row = "";
    $sql = "select * from reservation_tour_items where reservations_id = $id order by rtt_id ASC";
    $result = mysql_query($sql);
    $numrows = mysql_num_rows($result);
    $i = 0;
    while ($row = mysql_fetch_array($result))
    {
    if ($color == $color_2) $color = $color_3; else $color = $color_2;
    if ($i == 0)
    {
    ?>
    <form name="form" action="process.php?mode=reservations/tour_items_save&id=<?= $id ?>" method="post"
          enctype="multipart/form-data">
        <tr bgcolor="<?= $color_1 ?>">
            <td class="txt_bold_white" align="center" width="50">Delete</td>
            <td class="txt_bold_white">Name</td>
            <td class="txt_bold_white">Ref</td>
            <td class="txt_bold_white">Travel&nbsp;Date</td>
            <td class="txt_bold_white">Rate Type</td>
            <td class="txt_bold_white" align="center">Adult</td>
            <td class="txt_bold_white" align="center">Child</td>
            <td class="txt_bold_white" align="center">Infant</td>
            <td class="txt_bold_white" align="center">G</td>
            <td class="txt_bold_white" align="center">FOC</td>
            <td class="txt_bold_white" align="right">Price</td>

            <td class="txt_bold_white" width="50">&nbsp;</td>
        </tr>
        <?php
        }
        ?>
        <tr bgcolor="<?= $color ?>">
            <td align="center"><input type="checkbox" name="del_ids[']" value="<?= $row[0] ?>"/></td>
            <td>
                <a href="./?mode=reservations/tour_items_add_st1&id=<?= $id ?>&rtt_id=<?= $row[0] ?>"><?= $row['rtt_name'] ?></a>
            </td>
            <td><?= $row['rtt_ref'] ?></td>
            <td><?= DateFormat($row['rtt_travel_date'], "s") ?></td>
            <!--<td><?= $row['rtt_time'] ?></td>-->
            <td><?= $row['rtt_ratetype'] ?></td>
            <td align="center"><?= $row['rtt_adult_num'] ?></td>
            <td align="center"><?= $row['rtt_child_num'] ?></td>
            <td align="center"><?= $row['rtt_infant_num'] ?></td>
            <td align="center"><?= $row['rtt_g_num'] ?></td>
            <td align="center"><?= $row['rtt_foc_num'] ?></td>
            <td align="right"><?= number_format($row['rtt_prices'], 2) ?>
                <?php
                $total_prices += $row['rtt_prices'];
                ?>
            </td>
            <td align="center"><a
                    href="./?mode=reservations/tour_items_add_st1&id=<?= $id ?>&rtt_id=<?= $row[0] ?>"><img
                        src="images/icon_menu/edit.png" border="0" width="20" height="20" alt="Edit" title="Edit"/></a>
            </td>
        </tr>
        <?php
        $i++;
        }
        if ($i != 0)
        {
        ?>
        <tr bgcolor="<?= $color_1 ?>">
            <td colspan="20"><input type="submit" name="submit_array" value="SAVE SETTING" style="width:125px;"
                                    onClick="return confirm('Do you want to do this ?')"></td>
        </tr>
    </form>
<?php
}
else {
    ?>
    <tr bgcolor="<?= $color_3 ?>" height="100">
        <td align="center">Record Not Found!</td>
    </tr>
    <?php
}
?>
</table>
<!-- END Tour Items List -->


<br/>


<!-- Start Activity Items List -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td class="txt_big_gray">Reservation Activity List [ <a
                href="./?mode=reservations/activity_items_add_st1&id=<?= $id ?>">Create New Reservation Activity </a> ']
        </td>
    </tr>
</table>
<table width="100%" border="0" cellspacing="1" cellpadding="3" bgcolor="#FFFFFF">
    <?php

    $row = "";
    $sql = "select * from reservation_activity_items where reservations_id = $id order by rat_id ASC";
    $result = mysql_query($sql);
    $numrows = mysql_num_rows($result);
    $i = 0;
    while ($row = mysql_fetch_array($result))
    {
    if ($color == $color_2) $color = $color_3; else $color = $color_2;
    if ($i == 0)
    {
    ?>
    <form name="form" action="process.php?mode=reservations/activity_items_save&id=<?= $id ?>" method="post"
          enctype="multipart/form-data">
        <tr bgcolor="<?= $color_1 ?>">
            <td class="txt_bold_white" align="center" width="50">Delete</td>
            <td class="txt_bold_white">Name</td>
            <td class="txt_bold_white">Ref</td>
            <td class="txt_bold_white">Travel&nbsp;Date</td>
            <td class="txt_bold_white">Rate Type</td>
            <td class="txt_bold_white" align="center">Adult</td>
            <td class="txt_bold_white" align="center">Child</td>
            <td class="txt_bold_white" align="center">Infant</td>
            <td class="txt_bold_white" align="center">G</td>
            <td class="txt_bold_white" align="center">FOC</td>
            <td class="txt_bold_white" align="right">Price</td>

            <td class="txt_bold_white" width="50">&nbsp;</td>
        </tr>
        <?php
        }
        ?>
        <tr bgcolor="<?= $color ?>">
            <td align="center"><input type="checkbox" name="del_ids[']" value="<?= $row[0] ?>"/></td>
            <td>
                <a href="./?mode=reservations/activity_items_add_st1&id=<?= $id ?>&rat_id=<?= $row[0] ?>"><?= $row['rat_name'] ?></a>
            </td>
            <td><?= $row['rat_ref'] ?></td>
            <td><?= DateFormat($row['rat_travel_date'], "s") ?></td>
            <td><?= $row['rat_ratetype'] ?></td>
            <td align="center"><?= $row['rat_adult_num'] ?></td>
            <td align="center"><?= $row['rat_child_num'] ?></td>
            <td align="center"><?= $row['rat_infant_num'] ?></td>
            <td align="center"><?= $row['rat_g_num'] ?></td>
            <td align="center"><?= $row['rat_foc_num'] ?></td>
            <td align="right"><?= number_format($row['rat_prices'], 2) ?>
                <?php
                $total_prices += $row['rat_prices'];
                ?>
            </td>
            <td align="center"><a
                    href="./?mode=reservations/activity_items_add_st1&id=<?= $id ?>&rat_id=<?= $row[0] ?>"><img
                        src="images/icon_menu/edit.png" border="0" width="20" height="20" alt="Edit" title="Edit"/></a>
            </td>
        </tr>
        <?php
        $i++;
        }
        if ($i != 0)
        {
        ?>
        <tr bgcolor="<?= $color_1 ?>">
            <td colspan="20"><input type="submit" name="submit_array" value="SAVE SETTING" style="width:125px;"
                                    onClick="return confirm('Do you want to do this ?')"></td>
        </tr>
    </form>
<?php
}
else {
    ?>
    <tr bgcolor="<?= $color_3 ?>" height="100">
        <td align="center">Record Not Found!</td>
    </tr>
    <?php
}
?>
</table>
<!-- END Activity Items List -->


<br/>


<!-- Start Bus Transfer Items List -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td class="txt_big_gray">Reservation Bus Transfer List [ <a
                href="./?mode=reservations/bustransfer_items_add_st1&id=<?= $id ?>">Create New Reservation Bus
                Transfer </a> ']
        </td>
    </tr>
</table>

<table width="100%" border="0" cellspacing="1" cellpadding="3" bgcolor="#FFFFFF">
    <?php

    $row = "";
    $sql = "select * from reservation_bustransfer_items where reservations_id = $id order by rct_id ASC";
    $result = mysql_query($sql);
    $numrows = mysql_num_rows($result);
    $i = 0;
    while ($row = mysql_fetch_array($result))
    {
    if ($color == $color_2) $color = $color_3; else $color = $color_2;
    if ($i == 0)
    {
    ?>
    <form name="form" action="process.php?mode=reservations/bustransfer_items_save&id=<?= $id ?>" method="post"
          enctype="multipart/form-data">
        <tr bgcolor="<?= $color_1 ?>">
            <td class="txt_bold_white" align="center" width="50">Delete</td>
            <td class="txt_bold_white">Name</td>
            <td class="txt_bold_white">Ref</td>
            <td class="txt_bold_white">Travel&nbsp;Date</td>
            <td class="txt_bold_white">Rate Type</td>
            <td class="txt_bold_white" align="center">Adult</td>
            <td class="txt_bold_white" align="center">Child</td>
            <td class="txt_bold_white" align="center">Infant</td>
            <td class="txt_bold_white" align="center">G</td>
            <td class="txt_bold_white" align="center">FOC</td>
            <td class="txt_bold_white">Pick Up From</td>
            <td class="txt_bold_white">Drop-off to</td>
            <td class="txt_bold_white">Pick Up Time</td>
            <td class="txt_bold_white" align="right">Price</td>

            <td class="txt_bold_white" width="50">&nbsp;</td>
        </tr>
        <?php
        }
        ?>
        <tr bgcolor="<?= $color ?>">
            <td align="center"><input type="checkbox" name="del_ids[']" value="<?= $row[0] ?>"/></td>
            <td>
                <a href="./?mode=reservations/bustransfer_items_add_st1&id=<?= $id ?>&rct_id=<?= $row[0] ?>"><?= $row['rct_name'] ?></a>
            </td>
            <td><?= $row['rct_ref'] ?></td>
            <td><?= DateFormat($row['rct_travel_date'], "s") ?></td>
            <td><?= $row['rct_ratetype'] ?></td>
            <!--<td align="center"><?php if ($row['rct_car_num']) {
                echo $row['rct_car_num'];
            } ?></td>-->
            <td align="center"><?= $row['rct_adult_num'] ?></td>
            <td align="center"><?= $row['rct_child_num'] ?></td>
            <td align="center"><?= $row['rct_infant_num'] ?></td>
            <td align="center"><?= $row['rct_g_num'] ?></td>
            <td align="center"><?= $row['rct_foc_num'] ?></td>
            <td><?= $row['rct_pickupfrom'] ?></td>
            <td><?= $row['rct_pickupto'] ?></td>
            <td><?= $row['rct_pickuptime'] ?></td>
            <td align="right"><?= number_format($row['rct_prices'], 2) ?>
                <?php
                $total_prices += $row['rct_prices'];
                ?>
            </td>
            <td align="center"><a
                    href="./?mode=reservations/bustransfer_items_add_st1&id=<?= $id ?>&rct_id=<?= $row[0] ?>"><img
                        src="images/icon_menu/edit.png" border="0" width="20" height="20" alt="Edit" title="Edit"/></a>
            </td>
        </tr>
        <?php
        $i++;
        }
        if ($i != 0)
        {
        ?>
        <tr bgcolor="<?= $color_1 ?>">
            <td colspan="20"><input type="submit" name="submit_array" value="SAVE SETTING" style="width:125px;"
                                    onClick="return confirm('Do you want to do this ?')"></td>
        </tr>
    </form>
<?php
}
else {
    ?>
    <tr bgcolor="<?= $color_3 ?>" height="100">
        <td align="center">Record Not Found!</td>
    </tr>
    <?php
}
?>
</table>

<!-- END Bus Transfer Items List -->


<br/>


<!-- Start Private Land Transfer Items List -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td class="txt_big_gray">Reservation Private Land Transfer List [ <a
                href="./?mode=reservations/privatelandtransfer_items_add_st1&id=<?= $id ?>">Create New Reservation
                Private Land Transfer </a> ']
        </td>
    </tr>
</table>

<table width="100%" border="0" cellspacing="1" cellpadding="3" bgcolor="#FFFFFF">
    <?php

    $row = "";
    $sql = "select * from reservation_privatelandtransfer_items where reservations_id = $id order by rplt_id ASC";
    $result = mysql_query($sql);
    $numrows = mysql_num_rows($result);
    $i = 0;
    while ($row = mysql_fetch_array($result))
    {
    if ($color == $color_2) $color = $color_3; else $color = $color_2;
    if ($i == 0)
    {
    ?>
    <form name="form" action="process.php?mode=reservations/privatelandtransfer_items_save&id=<?= $id ?>" method="post"
          enctype="multipart/form-data">
        <tr bgcolor="<?= $color_1 ?>">
            <td class="txt_bold_white" align="center" width="50">Delete</td>
            <td class="txt_bold_white">Name</td>
            <td class="txt_bold_white">Ref</td>
            <td class="txt_bold_white">Travel&nbsp;Date</td>
            <td class="txt_bold_white">Rate Type</td>
            <td class="txt_bold_white" align="center">Car</td>
            <td class="txt_bold_white" align="center">Adult</td>
            <td class="txt_bold_white" align="center">Child</td>
            <td class="txt_bold_white" align="center">Infant</td>
            <td class="txt_bold_white" align="center">G</td>
            <td class="txt_bold_white" align="center">FOC</td>
            <td class="txt_bold_white">Pick Up From</td>
            <td class="txt_bold_white">Drop-off to</td>
            <td class="txt_bold_white">Pick Up Time</td>
            <td class="txt_bold_white" align="right">Price</td>

            <td class="txt_bold_white" width="50">&nbsp;</td>
        </tr>
        <?php
        }
        ?>
        <tr bgcolor="<?= $color ?>">
            <td align="center"><input type="checkbox" name="del_ids[']" value="<?= $row[0] ?>"/></td>
            <td>
                <a href="./?mode=reservations/privatelandtransfer_items_add_st1&id=<?= $id ?>&rplt_id=<?= $row[0] ?>"><?= $row['rplt_name'] ?></a>
            </td>
            <td><?= $row['rplt_ref'] ?></td>
            <td><?= DateFormat($row['rplt_travel_date'], "s") ?></td>
            <td><?= $row['rplt_ratetype'] ?></td>
            <td align="center"><?php if ($row['rplt_car_num']) {
                    echo $row['rplt_car_num'];
                } ?></td>
            <td align="center"><?= $row['rplt_adult_num'] ?></td>
            <td align="center"><?= $row['rplt_child_num'] ?></td>
            <td align="center"><?= $row['rplt_infant_num'] ?></td>
            <td align="center"><?= $row['rplt_g_num'] ?></td>
            <td align="center"><?= $row['rplt_foc_num'] ?></td>
            <td><?= $row['rplt_pickupfrom'] ?></td>
            <td><?= $row['rplt_pickupto'] ?></td>
            <td><?= $row['rplt_pickuptime'] ?></td>
            <td align="right"><?= number_format($row['rplt_prices'], 2) ?>
                <?php
                $total_prices += $row['rplt_prices'];
                ?>
            </td>
            <td align="center"><a
                    href="./?mode=reservations/privatelandtransfer_items_add_st1&id=<?= $id ?>&rplt_id=<?= $row[0] ?>"><img
                        src="images/icon_menu/edit.png" border="0" width="20" height="20" alt="Edit" title="Edit"/></a>
            </td>
        </tr>
        <?php
        $i++;
        }
        if ($i != 0)
        {
        ?>
        <tr bgcolor="<?= $color_1 ?>">
            <td colspan="20"><input type="submit" name="submit_array" value="SAVE SETTING" style="width:125px;"
                                    onClick="return confirm('Do you want to do this ?')"></td>
        </tr>
    </form>
<?php
}
else {
    ?>
    <tr bgcolor="<?= $color_3 ?>" height="100">
        <td align="center">Record Not Found!</td>
    </tr>
    <?php
}
?>
</table>

<!-- END Private Land Transfer Items List -->


<!-- Start Hotel Items List -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td class="txt_big_gray">Reservation Hotel List [ <a
                href="./?mode=reservations/hotel_items_add_st1&id=<?= $id ?>">Create New Reservation Hotel </a> ']
        </td>
    </tr>
</table>
<table width="100%" border="0" cellspacing="1" cellpadding="3" bgcolor="#FFFFFF">
    <?php

    $row = "";
    $sql = "select * from reservation_hotel_items where reservations_id = $id order by rht_id ASC";
    $result = mysql_query($sql);
    $numrows = mysql_num_rows($result);
    $i = 0;
    while ($row = mysql_fetch_array($result))
    {
    if ($color == $color_2) $color = $color_3; else $color = $color_2;
    if ($i == 0)
    {
    ?>
    <form name="form" action="process.php?mode=reservations/hotel_items_save&id=<?= $id ?>" method="post"
          enctype="multipart/form-data">
        <tr bgcolor="<?= $color_1 ?>">
            <td class="txt_bold_white" align="center" width="50">Delete</td>
            <td class="txt_bold_white">Name</td>
            <td class="txt_bold_white">Ref</td>
            <td class="txt_bold_white" align="center">Check&nbsp;In&nbsp;Date</td>
            <td class="txt_bold_white" align="center">Check&nbsp;Out&nbsp;Date</td>
            <td class="txt_bold_white">Room Type</td>
            <td class="txt_bold_white">Rate Type</td>
            <td class="txt_bold_white" align="center">Room</td>
            <td class="txt_bold_white" align="center">Adult</td>
            <td class="txt_bold_white" align="center">Child</td>
            <td class="txt_bold_white" align="center">Infant</td>
            <td class="txt_bold_white" align="center">G</td>
            <td class="txt_bold_white" align="center">FOC</td>
            <td class="txt_bold_white" align="right">Price</td>

            <td class="txt_bold_white" width="50">&nbsp;</td>
        </tr>
        <?php
        }
        ?>
        <tr bgcolor="<?= $color ?>">
            <td align="center"><input type="checkbox" name="del_ids[']" value="<?= $row[0] ?>"/></td>
            <td>
                <a href="./?mode=reservations/hotel_items_add_st1&id=<?= $id ?>&rht_id=<?= $row[0] ?>"><?= $row['rht_name'] ?></a>
            </td>
            <td><?= $row['rht_ref'] ?></td>
            <td align="center"><?= DateFormat($row['rht_check_in'], "s") ?></td>
            <td align="center"><?= DateFormat($row['rht_check_out'], "s") ?></td>
            <td><?= get_value('hotel_con_room_type', 'con_id', 'con_name', $row['hotelroomtype_id']) ?></td>
            <td><?= $row['rht_ratetype'] ?></td>
            <td align="center"><?= $row['rht_room_num'] ?></td>
            <td align="center"><?= $row['rht_adult_num'] ?></td>
            <td align="center"><?= $row['rht_child_num'] ?></td>
            <td align="center"><?= $row['rht_infant_num'] ?></td>
            <td align="center"><?= $row['rht_g_num'] ?></td>
            <td align="center"><?= $row['rht_foc_num'] ?></td>
            <td align="right"><?= number_format($row['rht_prices'], 2) ?>
                <?php
                $total_prices += $row['rht_prices'];
                ?>
            </td>
            <td align="center"><a
                    href="./?mode=reservations/hotel_items_add_st1&id=<?= $id ?>&rht_id=<?= $row[0] ?>"><img
                        src="images/icon_menu/edit.png" border="0" width="20" height="20" alt="Edit" title="Edit"/></a>
            </td>
        </tr>
        <?php
        $i++;
        }
        if ($i != 0)
        {
        ?>
        <tr bgcolor="<?= $color_1 ?>">
            <td colspan="20"><input type="submit" name="submit_array" value="SAVE SETTING" style="width:125px;"
                                    onClick="return confirm('Do you want to do this ?')"></td>
        </tr>
    </form>
<?php
}
else {
    ?>
    <tr bgcolor="<?= $color_3 ?>" height="100">
        <td align="center">Record Not Found!</td>
    </tr>
    <?php
}
?>
</table>
<!-- END Hotel Items List -->


<!-- Start Train Transfer Items List -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td class="txt_big_gray">Reservation Train Transfer List [ <a
                href="./?mode=reservations/traintransfer_items_add_st1&id=<?= $id ?>">Create New Reservation Train
                Transfer </a> ']
        </td>
    </tr>
</table>
<table width="100%" border="0" cellspacing="1" cellpadding="3" bgcolor="#FFFFFF">
    <?php

    $row = "";
    $sql = "select * from reservation_traintransfer_items where reservations_id = $id order by rrt_id ASC";
    $result = mysql_query($sql);
    $numrows = mysql_num_rows($result);
    $i = 0;
    while ($row = mysql_fetch_array($result))
    {
    if ($color == $color_2) $color = $color_3; else $color = $color_2;
    if ($i == 0)
    {
    ?>
    <form name="form" action="process.php?mode=reservations/traintransfer_items_save&id=<?= $id ?>" method="post"
          enctype="multipart/form-data">
        <tr bgcolor="<?= $color_1 ?>">
            <td class="txt_bold_white" align="center" width="50">Delete</td>
            <td class="txt_bold_white">Name</td>
            <td class="txt_bold_white">Ref</td>
            <td class="txt_bold_white">Train No.</td>
            <td class="txt_bold_white">Travel&nbsp;Date</td>
            <td class="txt_bold_white">Time</td>
            <td class="txt_bold_white">Rate Type</td>
            <td class="txt_bold_white" align="center">Adult</td>
            <td class="txt_bold_white" align="center">Child</td>
            <td class="txt_bold_white" align="center">Infant</td>
            <td class="txt_bold_white" align="center">G</td>
            <td class="txt_bold_white" align="center">FOC</td>
            <td class="txt_bold_white" align="right">Price</td>

            <td class="txt_bold_white" width="50">&nbsp;</td>
        </tr>
        <?php
        }
        ?>
        <tr bgcolor="<?= $color ?>">
            <td align="center"><input type="checkbox" name="del_ids[']" value="<?= $row[0] ?>"/></td>
            <td>
                <a href="./?mode=reservations/traintransfer_items_add_st1&id=<?= $id ?>&rrt_id=<?= $row[0] ?>"><?= $row['rrt_name'] ?></a>
            </td>
            <td><?= $row['rrt_ref'] ?></td>
            <td><?= $row['rrt_train_no'] ?></td>
            <td><?= DateFormat($row['rrt_travel_date'], "s") ?></td>
            <td><?= $row['rrt_time_dep'] ?> - <?= $row['rrt_time_arr'] ?></td>
            <td><?= $row['rrt_ratetype'] ?></td>
            <td align="center"><?= $row['rrt_adult_num'] ?></td>
            <td align="center"><?= $row['rrt_child_num'] ?></td>
            <td align="center"><?= $row['rrt_infant_num'] ?></td>
            <td align="center"><?= $row['rrt_g_num'] ?></td>
            <td align="center"><?= $row['rrt_foc_num'] ?></td>
            <td align="right"><?= number_format($row['rrt_prices'], 2) ?>
                <?php
                $total_prices += $row['rrt_prices'];
                ?>
            </td>
            <td align="center"><a
                    href="./?mode=reservations/traintransfer_items_add_st1&id=<?= $id ?>&rrt_id=<?= $row[0] ?>"><img
                        src="images/icon_menu/edit.png" border="0" width="20" height="20" alt="Edit" title="Edit"/></a>
            </td>
        </tr>
        <?php
        $i++;
        }
        if ($i != 0)
        {
        ?>
        <tr bgcolor="<?= $color_1 ?>">
            <td colspan="20"><input type="submit" name="submit_array" value="SAVE SETTING" style="width:125px;"
                                    onClick="return confirm('Do you want to do this ?')"></td>
        </tr>
    </form>
<?php
}
else {
    ?>
    <tr bgcolor="<?= $color_3 ?>" height="100">
        <td align="center">Record Not Found!</td>
    </tr>
    <?php
}
?>
</table>
<!-- END Train Transfer Items List -->


<table width="100%" border="0" cellspacing="1" cellpadding="3">
    <tr>
        <td width="12%">&nbsp;</td>
        <td>&nbsp;</td>
        <td width="10%" align="right"><strong>Total : </strong></td>
        <td width="15%" align="right"><?= number_format($total_prices, 2) ?>
            THB
        </td>
        <td width="2%">&nbsp;</td>
    </tr>
</table>


<!-- *************************************************END Item List************************************************* -->


<table width="100%" border="0" cellspacing="1" cellpadding="3">
    <tr>
        <td colspan="5" bgcolor="#000000" height="1" style="padding:0px;"></td>
    </tr>
</table>

<?php
$row = "";
$sql = "select * from reservations where res_id = $id";
$result = mysql_query($sql);
$row = mysql_fetch_array($result);
?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td class="txt_big_gray"><br/>History</td>
    </tr>
    <tr>
        <td class="txt_big_gray"><br/><textarea cols="155" rows="10" name="res_history"
                                                readonly="readonly"><?= $row['res_history'] ?></textarea></td>
    </tr>
</table>