<?php
include("value_get.php");
if ($id) {
    $sql = "select * from reservations where res_id = $id";
    $result = mysql_query($sql);
    $row = mysql_fetch_array($result);

    $rrt_name = $_POST['rrt_name'];
    $current_rrt_name = $_POST['current_rrt_name'];

    include("inc.top.php");
    //if ($rbt_id)
    if ($current_rrt_name == $rrt_name) {
        $page_title = "Update Reservation Train Transfer";
        $sql = "select * from reservation_traintransfer_items where rrt_id = $rrt_id";
        $result = mysql_query($sql);
        $row = mysql_fetch_array($result);

        //echo "sql : $sql";
    } else {
        $row = "";
        $page_title = "Create New Reservation Train Transfer";
    }
    ?>
    <script language="JavaScript" type="text/JavaScript">
        <!--
        function MM_findObj(n, d) { //v4.01
            var p, i, x;
            if (!d) d = document;
            if ((p = n.indexOf("?")) > 0 && parent.frames.length) {
                d = parent.frames[n.substring(p + 1)].document;
                n = n.substring(0, p);
            }
            if (!(x = d[n]) && d.all) x = d.all[n];
            for (i = 0; !x && i < d.forms.length; i++) x = d.forms[i][n];
            for (i = 0; !x && d.layers && i < d.layers.length; i++) x = MM_findObj(n, d.layers[i].document);
            if (!x && d.getElementById) x = d.getElementById(n);
            return x;
        }
        function MM_validateForm() { //v4.0
            var i, p, q, nm, test, num, min, max, errors = '', args = MM_validateForm.arguments;
            for (i = 0; i < (args.length - 2); i += 3) {
                test = args[i + 2];
                val = MM_findObj(args[i]);
                if (val) {
                    nm = val.name;
                    if ((val = val.value) != "") {
                        if (test.indexOf('isEmail') != -1) {
                            p = val.indexOf('@');
                            if (p < 1 || p == (val.length - 1)) errors += '- E-mail must contain an e-mail address.\n';
                        } else if (test != 'R') {
                            num = parseFloat(val);
                            if (isNaN(val)) {
                                if (nm == 'name') nm = 'Room Type';
                                errors += '- ' + nm + ' must contain a number.\n';
                            }
                            if (test.indexOf('inRange') != -1) {
                                p = test.indexOf(':');
                                min = test.substring(8, p);
                                max = test.substring(p + 1);
                                if (num < min || max < num) errors += '- ' + nm + ' must contain a number between ' + min + ' and ' + max + '.\n';
                            }
                        }
                    } else if (test.charAt(0) == 'R') {
                        if (nm == 'tourt_name') nm = 'Reservation Boat Transfer';
                        errors += '- ' + nm + ' is required.\n';
                    }
                }
            }
            if (errors) alert('The following error(s) occurred:\n' + errors);
            document.MM_returnValue = (errors == '');
        }
        //-->
    </script>

    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="txt_big_gray"><?= $page_title ?></td>
        </tr>
    </table>

    <?php
    $sql_tra = "select * from traintransfers where train_name = '$rrt_name'";
    $result_tra = mysql_query($sql_tra);
    $row_tra = mysql_fetch_array($result_tra);
    ?>

    <form action="process.php?mode=reservations/traintransfer_items_save&id=<?= $id ?>&rrt_id=<?= $rrt_id ?>" onsubmit="MM_validateForm(
'tourt_name','','R'
);return document.MM_returnValue" name="form" method="post" enctype="multipart/form-data">
        <input type="hidden" name="last_edit_by" value="<?= $perms["NAME"] ?>(Back Office System)"/>
        <input type="hidden" name="rrt_train_id" value="<?= $row_tra['train_id'] ?>"/>
        <input type="hidden" name="rrt_name" value="<?= $rrt_name ?>"/>
        <input type="hidden" name="rrt_ref" value="<?= $row_tra['train_ref'] ?>"/>

        <table width="100%" border="0" cellspacing="0" cellpadding="3">
            <tr>
                <td align="right" class="txt_bold_gray" width="317">Name :</td>
                <td width="1076"><?= $rrt_name ?></td>
            </tr>

            <tr>
                <td align="right" class="txt_bold_gray">Ref :</td>
                <td><?= $row_tra['train_ref'] ?></td>
            </tr>

            <tr>
                <td align="right" class="txt_bold_gray">Train No. :</td>
                <td><input type="text" name="rrt_train_no" value="<?= $row['rrt_train_no'] ?>" size="50"/></td>
            </tr>

            <tr>
                <?php
                if ($row['rrt_travel_date'] > 0) {
                    $rrt_travel_date = $row['rrt_travel_date'];
                } else {
                    $rrt_travel_date = $today;
                }
                ?>
                <td align="right" class="txt_bold_gray"> Travel Date :</td>
                <td>
                    <script>DateInput('rrt_travel_date', false, 'yyyy-mm-dd', '<?= $rrt_travel_date?>')</script>
                </td>
            </tr>

            <tr>
                <td align="right" class="txt_bold_gray">Rate Type :</td>
                <td>
                    <select name="rrt_ratetype">
                        <option value="">-- Select --</option>
                        <?php
                        $sql_rate = "select * from traintransfer_ratetypes where traintransfers_id = " . $row_tra['train_id'] . " ";
                        $result_rate = mysql_query($sql_rate);
                        while ($row_rate = mysql_fetch_array($result_rate)) {
                            ?>
                            <option
                                value="<?= $row_rate['trart_name'] ?>" <?php if ($row_rate['trart_name'] == $row['rrt_ratetype']) {
                                echo 'selected="selected"';
                            } ?>><?= $row_rate['trart_name'] ?></option>
                        <?php } ?>
                    </select>
                </td>
            </tr>

            <tr>
                <td align="right" class="txt_bold_gray">Adult :</td>
                <td><select name="rrt_adult_num">

                        <?php list_number($row['rrt_adult_num'], 1, 10); ?>
                    </select></td>
            </tr>

            <tr>
                <td align="right" class="txt_bold_gray">Child :</td>
                <td><select name="rrt_child_num">

                        <?php list_number($row['rrt_child_num'], 0, 11); ?>
                    </select></td>
            </tr>

            <tr>
                <td align="right" class="txt_bold_gray">Infant :</td>
                <td><select name="rrt_infant_num">

                        <?php list_number($row['rrt_infant_num'], 0, 11); ?>
                    </select></td>
            </tr>

            <tr>
                <td align="right" class="txt_bold_gray">G :</td>
                <td><select name="rrt_g_num">

                        <?php list_number($row['rrt_g_num'], 0, 11); ?>
                    </select></td>
            </tr>

            <tr>
                <td align="right" class="txt_bold_gray">FOC :</td>
                <td><select name="rrt_foc_num">

                        <?php list_number($row['rrt_foc_num'], 0, 11); ?>
                    </select></td>
            </tr>

            <tr>
                <td align="right" class="txt_bold_gray">Price :</td>
                <td><input type="text" name="rrt_prices" value="<?= $row['rrt_prices'] ?>" size="50"/></td>
            </tr>


            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td><input type="submit" name="submit" value="SUBMIT" style="width:75px;"><?php
                    echo " <input type=\"reset\" name=\"Reset\" value=\"RESET\" style=\"width:75px;\">";
                    ?></td>
            </tr>
        </table>
    </form>
    <?php
}
?>
