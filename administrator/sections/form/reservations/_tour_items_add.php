<?php
include("value_get.php");
if ($id) {
    $sql = "select * from reservations where res_id = $id";
    $result = mysql_query($sql);
    $row = mysql_fetch_array($result);
    include("inc.top.php");
    if ($rtt_id) {
        $page_title = "Update Reservation Tour";
        $sql = "select * from reservation_tour_items where rtt_id = $rtt_id";
        $result = mysql_query($sql);
        $row = mysql_fetch_array($result);

        //echo "sql : $sql";
    } else {
        $row = "";
        $page_title = "Create New Reservation Tour";
    }
    ?>

    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="txt_big_gray"><?= $page_title ?></td>
        </tr>
    </table>
    <form action="process.php?mode=reservations/tour_items_save&id=<?= $id ?>&rtt_id=<?= $rtt_id ?>" onsubmit="MM_validateForm(
'tourt_name','','R'
);return document.MM_returnValue" name="form" method="post" enctype="multipart/form-data">
        <input type="hidden" name="last_edit_by" value="<?= $perms["NAME"] ?>(Back Office System)"/>
        <table width="100%" border="0" cellspacing="0" cellpadding="3">
            <tr>
                <td align="right" class="txt_bold_gray" width="317">Name :</td>
                <td width="1076"><input type="text" name="rtt_name" value="<?= $row['rtt_name'] ?>" size="50"/></td>
            </tr>


            <tr>
                <td align="right" class="txt_bold_gray">Ref :</td>
                <td><input type="text" name="rtt_ref" value="<?= $row['rtt_ref'] ?>" size="50"/></td>
            </tr>

            <tr>
                <?php
                if ($row['rtt_travel_date'] > 0) {
                    $rtt_travel_date = $row['rtt_travel_date'];
                } else {
                    $rtt_travel_date = $today;
                }
                ?>
                <td align="right" class="txt_bold_gray"> Travel Date :</td>
                <td>
                    <script>DateInput('rtt_travel_date', false, 'yyyy-mm-dd', '<?= $rtt_travel_date?>')</script>
                </td>
            </tr>

            <!--<tr>
        <td align="right" class="txt_bold_gray" >Time : </td>
      <td><input type="text" name="rbt_time" value="<?= $row['rbt_time'] ?>" size="50" /></td>
    </tr>-->

            <tr>
                <td align="right" class="txt_bold_gray">Rate Type :</td>
                <td><input type="text" name="rtt_ratetype" value="<?= $row['rtt_ratetype'] ?>" size="50"/></td>
            </tr>

            <tr>
                <td align="right" class="txt_bold_gray">Audlt :</td>
                <td><select name="rtt_adult_num">

                        <?php list_number($row['rtt_adult_num'], 1, 10); ?>
                    </select></td>
            </tr>

            <tr>
                <td align="right" class="txt_bold_gray">Child :</td>
                <td><select name="rtt_child_num">

                        <?php list_number($row['rtt_child_num'], 0, 11); ?>
                    </select></td>
            </tr>

            <tr>
                <td align="right" class="txt_bold_gray">Infant :</td>
                <td><select name="rtt_infant_num">

                        <?php list_number($row['rtt_infant_num'], 0, 11); ?>
                    </select></td>
            </tr>

            <tr>
                <td align="right" class="txt_bold_gray">Price :</td>
                <td><input type="text" name="rtt_prices" value="<?= $row['rtt_prices'] ?>" size="50"/></td>
            </tr>


            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td><input type="submit" name="submit" value="SUBMIT" style="width:75px;"><?php
                    if ($rbt_id) echo " <input type=\"submit\" name=\"del\" value=\"DELETE\" style=\"width:75;\" onClick=\"return confirm('Do you want to delete this record ?')\">";
                    else echo " <input type=\"reset\" name=\"Reset\" value=\"RESET\" style=\"width:75;\">";
                    ?></td>
            </tr>
        </table>
    </form>
    <?php
}
?>
<script language="JavaScript" type="text/JavaScript">
    <!--
    function MM_findObj(n, d) { //v4.01
        var p, i, x;
        if (!d) d = document;
        if ((p = n.indexOf("?")) > 0 && parent.frames.length) {
            d = parent.frames[n.substring(p + 1)].document;
            n = n.substring(0, p);
        }
        if (!(x = d[n]) && d.all) x = d.all[n];
        for (i = 0; !x && i < d.forms.length; i++) x = d.forms[i][n];
        for (i = 0; !x && d.layers && i < d.layers.length; i++) x = MM_findObj(n, d.layers[i].document);
        if (!x && d.getElementById) x = d.getElementById(n);
        return x;
    }
    function MM_validateForm() { //v4.0
        var i, p, q, nm, test, num, min, max, errors = '', args = MM_validateForm.arguments;
        for (i = 0; i < (args.length - 2); i += 3) {
            test = args[i + 2];
            val = MM_findObj(args[i]);
            if (val) {
                nm = val.name;
                if ((val = val.value) != "") {
                    if (test.indexOf('isEmail') != -1) {
                        p = val.indexOf('@');
                        if (p < 1 || p == (val.length - 1)) errors += '- E-mail must contain an e-mail address.\n';
                    } else if (test != 'R') {
                        num = parseFloat(val);
                        if (isNaN(val)) {
                            if (nm == 'name') nm = 'Room Type';
                            errors += '- ' + nm + ' must contain a number.\n';
                        }
                        if (test.indexOf('inRange') != -1) {
                            p = test.indexOf(':');
                            min = test.substring(8, p);
                            max = test.substring(p + 1);
                            if (num < min || max < num) errors += '- ' + nm + ' must contain a number between ' + min + ' and ' + max + '.\n';
                        }
                    }
                } else if (test.charAt(0) == 'R') {
                    if (nm == 'tourt_name') nm = 'Reservation Tour';
                    errors += '- ' + nm + ' is required.\n';
                }
            }
        }
        if (errors) alert('The following error(s) occurred:\n' + errors);
        document.MM_returnValue = (errors == '');
    }
    //-->
</script>
