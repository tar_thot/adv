<?php
$perpage = 999;    //<--- For Show Record Per Pages

$color_1 = "#AAAAAA";
$color_2 = "#DDDDDD";
$color_3 = "#EEEEEE";
$color_inctop1 = "#999999";
$color_inctop2 = "#CCCCCC";

$today = date("Y-m-d"); //<--- For Get Date ### Today ### YYYY-MM-DD
$times = date("H:i:s"); //<--- For Get Time ###  Now  ### HH:MM:SS

$program_name = "ADV Tour Co., Ltd."; // <--- For Set Program Name
$Header_Text = "ADV Tour Co., Ltd."; // <--- For Set Program Name
$program_type = "ADV Tour Back Office System"; // <--- For Set Program Name
$IMG_PATH = "../photo/";    // <--- Upload Images Path

$_CURRENCY = "THB";

//$c_URL = "http://192.168.0.119/solution/tour_andaman_wave";
$c_URL = "http://booking.adv-tour.com";
