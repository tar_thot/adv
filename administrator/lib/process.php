<?php
session_start();

require("lib/connect.php");
require("lib/function.php");
require("lib/constant.php");

// Convert Variable Array To Variable

while (list($xVarName, $xVarvalue) = each($_GET)) {
    ${$xVarName} = $xVarvalue;
}


while (list($xVarName, $xVarvalue) = each($_POST)) {
    ${$xVarName} = $xVarvalue;
}

while (list($xVarName, $xVarvalue) = each($_FILES)) {
    ${$xVarName . "_name"} = $xVarvalue['name'];
    ${$xVarName . "_type"} = $xVarvalue['type'];
    ${$xVarName . "_size"} = $xVarvalue['size'];
    ${$xVarName . "_error"} = $xVarvalue['error'];
    ${$xVarName} = $xVarvalue['tmp_name'];
}

$perms = $_SESSION['perms'];

if (!$mode) {
    include("lib/header.php");
    include("sections/form/login/index.php");
    $error = "8";
    include("lib/footer.php");
} elseif (!$perms["ID"]) {
    if ($mode == "login/check") {
        include("sections/process/$mode.php");
    } else {
        include("sections/form/login/index.php");
        $error = "7";
    }
} else {
    include("sections/process/$mode.php");
}

include("lib/error.php");
?>