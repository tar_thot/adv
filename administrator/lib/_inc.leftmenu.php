<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td width="200" bgcolor="#DDDDDD" valign="top" align="left">
            <table width="177" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                    <td height="25"><img src="images/homeIcon.png"/><a href="index.php?mode=welcome/index">Home</a></td>
                </tr>


                <? if ($perms["PERMISSION"] == "Admin" || $perms["PERMISSION"] == "Marketing Admin" || $perms["PERMISSION"] == "Marketing") { ?>
                    <tr>
                        <td height="25" align="center" background="images/bordertop.png"><strong style="color:#FFFFFF">Agent</strong>
                        </td>
                    </tr>
                    <tr>
                        <td height="18" background="images/bordermiddle.png">&nbsp;&nbsp;&#8226;&nbsp;<a
                                href="index.php?mode=agents/index">ข้อมูล Agent</a></td>
                    </tr>
                    <tr>
                        <td height="5" background="images/borderbottom.png"></td>
                    </tr>
                    <tr>
                        <td height="5"></td>
                    </tr>
                <? } ?>


                <? if ($perms["PERMISSION"] == "Admin" || $perms["PERMISSION"] == "Marketing Admin" || $perms["PERMISSION"] == "Marketing") { ?>
                    <tr>
                        <td height="25" align="center" background="images/bordertop.png"><strong style="color:#FFFFFF">Client</strong>
                        </td>
                    </tr>
                    <tr>
                        <td height="18" background="images/bordermiddle.png">&nbsp;&nbsp;&#8226;&nbsp;<a
                                href="index.php?mode=clients/index">ข้อมูล Client</a></td>
                    </tr>
                    <tr>
                        <td height="5" background="images/borderbottom.png"></td>
                    </tr>
                    <tr>
                        <td height="5"></td>
                    </tr>
                <? } ?>


                <? if ($perms["PERMISSION"] == "Admin" || $perms["PERMISSION"] == "Marketing Admin" || $perms["PERMISSION"] == "Marketing") { ?>
                    <tr>
                        <td height="25" align="center" background="images/bordertop.png"><strong style="color:#FFFFFF">Supplier</strong>
                        </td>
                    </tr>
                    <tr>
                        <td height="18" background="images/bordermiddle.png">&nbsp;&nbsp;&#8226;&nbsp;<a
                                href="index.php?mode=suppliers/index">ข้อมูล Supplier</a></td>
                    </tr>
                    <tr>
                        <td height="5" background="images/borderbottom.png"></td>
                    </tr>
                    <tr>
                        <td height="5"></td>
                    </tr>
                <? } ?>


                <? if ($perms["PERMISSION"] == "Admin" || $perms["PERMISSION"] == "Account" || $perms["PERMISSION"] == "Marketing Admin" || $perms["PERMISSION"] == "Marketing" || $perms["PERMISSION"] == "Reservation Chief") { ?>
                    <tr>
                        <td height="25" align="center" background="images/bordertop.png"><strong style="color:#FFFFFF">สินค้า</strong>
                        </td>
                    </tr>
                    <tr>
                        <td height="18" background="images/bordermiddle.png">&nbsp;&nbsp;&#8226;&nbsp;<a
                                href="index.php?mode=tours/index">Tour</a></td>
                    </tr>
                    <tr>
                        <td height="18" background="images/bordermiddle.png">&nbsp;&nbsp;&#8226;&nbsp;<a
                                href="index.php?mode=activities/index">Activity</a></td>
                    </tr>
                    <tr>
                        <td height="18" background="images/bordermiddle.png">&nbsp;&nbsp;&#8226;&nbsp;<a
                                href="index.php?mode=hotels/index">Hotel</a></td>
                    </tr>
                    <tr>
                        <td height="18" background="images/bordermiddle.png">&nbsp;&nbsp;&#8226;&nbsp;<a
                                href="index.php?mode=boattransfers/index">Boat Transfer</a></td>
                    </tr>
                    <tr>
                        <td height="18" background="images/bordermiddle.png">&nbsp;&nbsp;&#8226;&nbsp;<a
                                href="index.php?mode=cartransfers/index">Bus Transfer</a></td>
                    </tr>
                    <tr>
                        <td height="18" background="images/bordermiddle.png">&nbsp;&nbsp;&#8226;&nbsp;<a
                                href="index.php?mode=pickuptransfers/index">Pick Up Transfer</a></td>
                    </tr>
                    <tr>
                        <td height="18" background="images/bordermiddle.png">&nbsp;&nbsp;&#8226;&nbsp;<a
                                href="index.php?mode=privatelandtransfers/index">Private Land Transfer</a></td>
                    </tr>
                    <tr>
                        <td height="18" background="images/bordermiddle.png">&nbsp;&nbsp;&#8226;&nbsp;<a
                                href="index.php?mode=traintransfers/index">Train Transfer</a></td>
                    </tr>
                    <tr>
                        <td height="18" background="images/bordermiddle.png">&nbsp;&nbsp;&#8226;&nbsp;<a
                                href="index.php?mode=underconstruction/index">Air Ticket</a></td>
                    </tr>
                    <tr>
                        <td height="5" background="images/borderbottom.png"></td>
                    </tr>
                    <tr>
                        <td height="5"></td>
                    </tr>
                <? } ?>


                <? if ($perms["PERMISSION"] == "Admin" || $perms["PERMISSION"] == "Account" || $perms["PERMISSION"] == "Marketing Admin" || $perms["PERMISSION"] == "Marketing" || $perms["PERMISSION"] == "Reservation Chief") { ?>
                    <tr>
                        <td height="25" align="center" background="images/bordertop.png"><strong style="color:#FFFFFF">สินค้า
                                (Combo Product)</strong></td>
                    </tr>
                    <tr>
                        <td height="18" background="images/bordermiddle.png">&nbsp;&nbsp;&#8226;&nbsp;<a
                                href="index.php?mode=packages/index">Combo Product</a></td>
                    </tr>
                    <tr>
                        <td height="5" background="images/borderbottom.png"></td>
                    </tr>
                    <tr>
                        <td height="5"></td>
                    </tr>
                <? } ?>


                <? if ($perms["PERMISSION"] == "Admin" || $perms["PERMISSION"] == "Account" || $perms["PERMISSION"] == "Marketing Admin" || $perms["PERMISSION"] == "Marketing" || $perms["PERMISSION"] == "Reservation Chief" || $perms["PERMISSION"] == "Reservation") { ?>
                    <tr>
                        <td height="25" align="center" background="images/bordertop.png"><strong style="color:#FFFFFF">Reservation</strong>
                        </td>
                    </tr>
                    <tr>
                        <td height="18" background="images/bordermiddle.png">&nbsp;&nbsp;&#8226;&nbsp;<a
                                href="index.php?mode=reservations/index">Reservation List</a></td>
                    </tr>
                    <tr>
                        <td height="5" background="images/borderbottom.png"></td>
                    </tr>
                    <tr>
                        <td height="5"></td>
                    </tr>
                <? } ?>


                <? if ($perms["PERMISSION"] == "Admin" || $perms["PERMISSION"] == "Account" || $perms["PERMISSION"] == "Marketing Admin" || $perms["PERMISSION"] == "Marketing" || $perms["PERMISSION"] == "Reservation Chief" || $perms["PERMISSION"] == "Reservation") { ?>
                    <tr>
                        <td height="25" align="center" background="images/bordertop.png"><strong style="color:#FFFFFF">Reservation<br/>(Combo
                                Product)</strong></td>
                    </tr>
                    <tr>
                        <td height="18" background="images/bordermiddle.png">&nbsp;&nbsp;&#8226;&nbsp;<a
                                href="index.php?mode=reservation_packages/index">Reservation List</a></td>
                    </tr>
                    <tr>
                        <td height="5" background="images/borderbottom.png"></td>
                    </tr>
                    <tr>
                        <td height="5"></td>
                    </tr>
                <? } ?>


                <? if ($perms["PERMISSION"] == "Admin" || $perms["PERMISSION"] == "Account" || $perms["PERMISSION"] == "Marketing Admin" || $perms["PERMISSION"] == "Marketing" || $perms["PERMISSION"] == "Reservation Chief" || $perms["PERMISSION"] == "Reservation") { ?>
                    <tr>
                        <td height="25" align="center" background="images/bordertop.png"><strong style="color:#FFFFFF">Search
                                Reservation</strong></td>
                    </tr>
                    <tr>
                        <td height="18" background="images/bordermiddle.png">&nbsp;&nbsp;&#8226;&nbsp;<a
                                href="index.php?mode=reservation_search/index">Reservation List</a></td>
                    </tr>
                    <tr>
                        <td height="5" background="images/borderbottom.png"></td>
                    </tr>
                    <tr>
                        <td height="5"></td>
                    </tr>
                <? } ?>


                <? if ($perms["PERMISSION"] == "Admin" || $perms["PERMISSION"] == "Account" || $perms["PERMISSION"] == "Marketing Admin" || $perms["PERMISSION"] == "Marketing" || $perms["PERMISSION"] == "Reservation Chief" || $perms["PERMISSION"] == "Reservation") { ?>
                    <tr>
                        <td height="25" align="center" background="images/bordertop.png"><strong style="color:#FFFFFF">Report</strong>
                        </td>
                    </tr>
                    <tr>
                        <td height="18" background="images/bordermiddle.png">&nbsp;&nbsp;&#8226;&nbsp;<a
                                href="index.php?mode=reports/customer_confirm_st1">Customer Confirm</a></td>
                    </tr>
                    <tr>
                        <td height="18" background="images/bordermiddle.png">&nbsp;&nbsp;&#8226;&nbsp;<a
                                href="index.php?mode=reports/pickup_transfer_st1">Pick Up Transfer</a></td>
                    </tr>
                    <tr>
                        <td height="18" background="images/bordermiddle.png">&nbsp;&nbsp;&#8226;&nbsp;<a
                                href="index.php?mode=reports/dropoff_transfer_st1">Drop-off Transfer</a></td>
                    </tr>
                    <tr>
                        <td height="18" background="images/bordermiddle.png">&nbsp;&nbsp;&#8226;&nbsp;<a
                                href="index.php?mode=reports/daily_passenger_st1">Daily Passenger</a></td>
                    </tr>
                    <!--<tr>
                      <td height="18" background="images/bordermiddle.png">&nbsp;&nbsp;&#8226;&nbsp;<a href="index.php?mode=underconstruction/index">Cash Payment</a></td>
                    </tr>
                    <tr>
                      <td height="18" background="images/bordermiddle.png">&nbsp;&nbsp;&#8226;&nbsp;<a href="index.php?mode=underconstruction/index">Pax Number</a></td>
                    </tr>
                    <tr>
                      <td height="18" background="images/bordermiddle.png">&nbsp;&nbsp;&#8226;&nbsp;<a href="index.php?mode=underconstruction/index">Total Pax Number</a></td>
                    </tr>-->
                    <tr>
                        <td height="18" background="images/bordermiddle.png">&nbsp;&nbsp;&#8226;&nbsp;<a
                                href="index.php?mode=reports/pax_by_ratetype_st1">Pax by Rate Type</a></td>
                    </tr>
                    <tr>
                        <td height="18" background="images/bordermiddle.png">&nbsp;&nbsp;&#8226;&nbsp;<a
                                href="index.php?mode=exports/income_confirm_st1">Income-Confirm Date</a></td>
                    </tr>
                    <tr>
                        <td height="18" background="images/bordermiddle.png">&nbsp;&nbsp;&#8226;&nbsp;<a
                                href="index.php?mode=exports/income_travel_st1">Income-Travel Date</a></td>
                    </tr>
                    <tr>
                        <td height="18" background="images/bordermiddle.png">&nbsp;&nbsp;&#8226;&nbsp;<a
                                href="index.php?mode=exports/voucher_st1">Voucher Report</a></td>
                    </tr>
                    <tr>
                        <td height="18" background="images/bordermiddle.png">&nbsp;&nbsp;&#8226;&nbsp;<a
                                href="index.php?mode=exports/agent_rate_grade_st1">Agent Rate (By Grade)</a></td>
                    </tr>
                    <tr>
                        <td height="18" background="images/bordermiddle.png">&nbsp;&nbsp;&#8226;&nbsp;<a
                                href="index.php?mode=exports/agent_rate_agent_st1">Agent Rate (By Agent)</a></td>
                    </tr>
                    <tr>
                        <td height="5" background="images/borderbottom.png"></td>
                    </tr>
                    <tr>
                        <td height="5"></td>
                    </tr>
                <? } ?>


                <? if ($perms["PERMISSION"] == "Admin" || $perms["PERMISSION"] == "Account" || $perms["PERMISSION"] == "Marketing Admin") { ?>
                    <tr>
                        <td height="25" align="center" background="images/bordertop.png"><strong style="color:#FFFFFF">Export
                                .txt</strong></td>
                    </tr>
                    <tr>
                        <td height="18" background="images/bordermiddle.png">&nbsp;&nbsp;&#8226;&nbsp;<a
                                href="index.php?mode=exports/ar_st1">AR</a></td>
                        <!--<td height="18" background="images/bordermiddle.png">&nbsp;&nbsp;&#8226;&nbsp;<a href="index.php?mode=underconstruction/index">AR</a></td>-->
                    </tr>
                    <tr>
                        <td height="18" background="images/bordermiddle.png">&nbsp;&nbsp;&#8226;&nbsp;<a
                                href="index.php?mode=exports/tr_st1">TR</a></td>
                        <!--<td height="18" background="images/bordermiddle.png">&nbsp;&nbsp;&#8226;&nbsp;<a href="index.php?mode=underconstruction/index">TR</a></td>-->
                    </tr>
                    <tr>
                        <td height="18" background="images/bordermiddle.png">&nbsp;&nbsp;&#8226;&nbsp;<a
                                href="index.php?mode=exports/dl_st1">DL</a></td>
                        <!--<td height="18" background="images/bordermiddle.png">&nbsp;&nbsp;&#8226;&nbsp;<a href="index.php?mode=underconstruction/index">DL</a></td>-->
                    </tr>
                    <tr>
                        <td height="18" background="images/bordermiddle.png">&nbsp;&nbsp;&#8226;&nbsp;<a
                                href="index.php?mode=exports/dp_st1">DP</a></td>
                        <!--<td height="18" background="images/bordermiddle.png">&nbsp;&nbsp;&#8226;&nbsp;<a href="index.php?mode=underconstruction/index">DP</a></td>-->
                    </tr>

                    <tr>
                        <td height="5" background="images/borderbottom.png"></td>
                    </tr>
                    <tr>
                        <td height="5"></td>
                    </tr>
                <? } ?>


                <? if ($perms["PERMISSION"] == "Admin" || $perms["PERMISSION"] == "Marketing Admin" || $perms["PERMISSION"] == "Reservation Chief") { ?>
                    <tr>
                        <td height="25" align="center" background="images/bordertop.png"><strong style="color:#FFFFFF">Guest
                                Comment</strong></td>
                    </tr>
                    <tr>
                        <td height="18" background="images/bordermiddle.png">&nbsp;&nbsp;&#8226;&nbsp;<a
                                href="index.php?mode=underconstruction/index">Guest Comment</a></td>
                    </tr>
                    <tr>
                        <td height="5" background="images/borderbottom.png"></td>
                    </tr>
                    <tr>
                        <td height="5"></td>
                    </tr>
                <? } ?>


                <? if ($perms["PERMISSION"] == "Admin" || $perms["PERMISSION"] == "Reservation Chief") { ?>
                    <tr>
                        <td height="5" background="images/bordertopW.png"></td>
                    </tr>
                    <tr>
                        <td height="18" background="images/bordermiddle.png">&nbsp;&nbsp;&#8226;&nbsp;<a
                                href="index.php?mode=member/index">User</a></td>
                    </tr>

                    <tr>
                        <td height="5" background="images/borderbottom.png"></td>
                    </tr>
                    <tr>
                        <td height="5"></td>
                    </tr>
                <? } ?>


                <? if ($perms["PERMISSION"] == "Admin" || $perms["PERMISSION"] == "Marketing Admin" || $perms["PERMISSION"] == "Marketing") { ?>
                    <tr>
                        <td height="5" background="images/bordertopW.png"></td>
                    </tr>
                    <tr>
                        <td height="18" background="images/bordermiddle.png">&nbsp;&nbsp;&#8226;&nbsp;<a
                                href="index.php?mode=config/index">Configuration</a></td>
                    </tr>

                    <tr>
                        <td height="5" background="images/borderbottom.png"></td>
                    </tr>
                    <tr>
                        <td height="5"></td>
                    </tr>
                <? } ?>


                <tr>
                    <td height="10"></td>
                </tr>
            </table>
</table>