<?php
/********************************************************************************************************
 * get_value    --> Get Value From Some Relation Field That you want
 * Ex :    get_value($table,$field_id,$field_name,$val)
 * get_value(property,property_id,location_id,$id)
 *
 * check_name    --> Check We Have Same Primary Name In This Table Or Not?
 * Not Working For Phuketland Project...
 *
 * DateFormat    --> Convert From YYYY-MM-DD To Original Format
 * Ex :    $date = 2004-02-01
 * DateFormat($date,"f") --> 01 February 2004
 * DateFormat($date,"s") --> 01 Feb 2004
 *
 * newline        -->    Replace "CHR(13)" With "<BR>" For Show New Line
 *
 * listbox        --> Make "List Box" With "Selected" Value Feature
 * Ex :    listbox($table,$f_id,$f_name,$link,$cri);
 * listbox(contact_master,running_no,"TITLE , firstname , lastname",$row[8],"category_agent = '1'");
 ********************************************************************************************************/
function get_value($table, $field_id, $field_name, $val)
{
    require("connect.php");
    $sql = "select $field_id , $field_name from $table where $field_id = '$val'";
    $result = mysql_query($sql);
    $rows = mysql_fetch_array($result);
    $value = "$rows[1]";
//	$value = ereg_replace("'","",$value);
    return $value;
}

/********************************************************************************************************/
function check_name($table, $fields, $val, $case, $col) // <----------- For Check Name Value Of Table.
{
    if (empty($col1)) {
        $col1 = 0;
    }
    //require ("./lib/connect.php");
    require("connect.php");
    $value = 1;
    $sql = "select * from $table where $fields = '$val'";
    $result = mysql_query($sql);
    while ($rows = mysql_fetch_array($result)) {
        $val = strtolower($val);
        $text = strtolower($rows[$col]);
        if ($val == $text && $rows[0] != $case) {
            $value = 0;
        }
    }
    return $value;
}

/********************************************************************************************************/
function DateFormat($val, $full)
{

    $a = explode("-", $val);
    if ($full == "S" || $full == "s") {
        switch ($a[1]) {
            case 1 :
                $mon = "Jan";
                break;
            case 2 :
                $mon = "Feb";
                break;
            case 3 :
                $mon = "Mar";
                break;
            case 4 :
                $mon = "Apr";
                break;
            case 5 :
                $mon = "May";
                break;
            case 6 :
                $mon = "Jun";
                break;
            case 7 :
                $mon = "Jul";
                break;
            case 8 :
                $mon = "Aug";
                break;
            case 9 :
                $mon = "Sep";
                break;
            case 10 :
                $mon = "Oct";
                break;
            case 11 :
                $mon = "Nov";
                break;
            case 12 :
                $mon = "Dec";
                break;
        }
    } else {
        switch ($a[1]) {
            case 1 :
                $mon = "January";
                break;
            case 2 :
                $mon = "February";
                break;
            case 3 :
                $mon = "March";
                break;
            case 4 :
                $mon = "April";
                break;
            case 5 :
                $mon = "May";
                break;
            case 6 :
                $mon = "June";
                break;
            case 7 :
                $mon = "July";
                break;
            case 8 :
                $mon = "August";
                break;
            case 9 :
                $mon = "September";
                break;
            case 10 :
                $mon = "October";
                break;
            case 11 :
                $mon = "November";
                break;
            case 12 :
                $mon = "December";
                break;
        }
    }
    $value = "$a[2] $mon $a[0]";
    return $value;
}

/********************************************************************************************************/
function listbox($table, $f_id, $f_name, $link, $cri)
{
    //require ("./lib/connect.php");

    require("connect.php");
    $sql = "select $f_id , $f_name from $table";
    if ($cri != 'N') {
        $sql .= " Where $cri";
    }
    $sql .= " Order By $f_name";
    $result = mysql_query($sql);

    while ($row = mysql_fetch_array($result)) {
        echo "<option value=\"$row[0]\"";
        if ($row[0] == $link) {
            echo " selected";
        }
        //old
        //echo ">".stripslashes($row[1])." ".stripslashes($row[2])." ".stripslashes($row[3])." ".stripslashes($row[4])." ".stripslashes($row[5])."</option>". chr(13);
        echo ">" . stripslashes($row[1]) . "</option>" . chr(13);
    }
}

/********************************************************************************************************/
function record_count($table, $criteria)
{
    require("connect.php");
    $sql = "select * from $table";
    if ($criteria && $criteria <> "N") {
        $sql .= " where " . $criteria;
    }
    $result = mysql_query($sql);
    return mysql_num_rows($result);
}

/********************************************************************************************************/
function list_number($select, $start, $loop)
{
    $select = $select * 1;
    $selected[$select] = " selected";
    $i = 0;
    while ($i != $loop) {
        echo "<option value=\"$start\"" . $selected[$start] . ">$start</option>" . chr(13);
        $start++;
        $i++;
    }
}

/********************************************************************************************************/
function img_upload($photo, $photo_name, $tmp_photo, $uploaddir, $photo_id, $paramiter, $redirect_url)
{
    if (!$photo) {
        $photo = $tmp_photo;
    } else {
        $ext = explode(".", $photo_name);
        $ext_n = count($ext) - 1;
        if (strtoupper($ext[$ext_n]) == "JPG" || strtoupper($ext[$ext_n]) == "JPEG") {
            $final_filename = $photo_id . $paramiter . ".jpg";
            $newfile = $uploaddir . "$final_filename";
            if (is_uploaded_file($photo)) {
                if (!copy($photo, "$newfile")) {
                    print "Error Uploading File.";
                    exit();
                }
            }
            $photo = $final_filename;
            if ($tmp_photo) {
                unlink($uploaddir . $tmp_photo);
            }
        } else {
            echo "<meta http-equiv=\"refresh\" content=\"0; url = '" . $redirect_url . "'\" >";
            exit;
        }
    }
    return $photo;
}

/********************************************************************************************************/
function cal_date($val, $cal, $type)
{
    $val = explode("-", $val);
    $val[0] = $val[0] * 1;
    $val[1] = $val[1] * 1;
    $val[2] = $val[2] * 1;
    /********** Year *************/
    if ($type == "y") {
        $val[0] = $val[0] + $cal;
        $final = $val[0] . "-" . sprintf("%02d", $val[1]) . "-" . sprintf("%02d", $val[2]);
    }
    /********** Month *************/
    if ($type == "m") {
        $val[1] = $val[1] + $cal;
        if ($val[1] > 12) {
            $val[0] = $val[0] + 1;
            $val[1] = $val[1] - 12;
        }
        $final = $val[0] . "-" . sprintf("%02d", $val[1]) . "-" . sprintf("%02d", $val[2]);
    }
    /********** Day *************/
    if ($type == "d") {
        $jd = GregorianToJD($val[1], $val[2], $val[0]);
        $jd = $jd + $cal;
        $final = JDToGregorian($jd);
        $final = explode("/", $final);
        $final = $final[2] . "-" . sprintf("%02d", $final[0]) . "-" . sprintf("%02d", $final[1]);
    }
    return $final;
}

/********************************************************************************************************/
function all_upload($photo, $photo_name, $tmp_photo, $uploaddir, $photo_id, $paramiter, $redirect_url)
{
    if (!$photo) $photo = $tmp_photo;
    else {
        $ext = explode(".", $photo_name);
        $ext_n = count($ext) - 1;
        if (strtoupper($ext[$ext_n]) != "EXE") {
            $final_filename = $photo_id . $paramiter . "." . $ext[$ext_n];
            $newfile = $uploaddir . "$final_filename";
            if (is_uploaded_file($photo)) {
                if (!copy($photo, "$newfile")) {
                    print "Error Uploading File.";
                    exit();
                }
            }
            $photo = $final_filename;
            if ($tmp_photo) unlink($uploaddir . $tmp_photo);
        } else {
            echo "<meta http-equiv=\"refresh\" content=\"0; url='" . $redirect_url . "'\" >";
            exit;
        }
    }
    return $photo;
}

/********************************************************************************************************/
//15/2/2559
//check empyt value
function issetValue($value)
{

    return isset($value) ? $value : '';
}

?>