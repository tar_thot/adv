<?php session_start(); ?>
<?php include("./administrator/lib/connect.php"); ?>
<?php include("./administrator/lib/function.php"); ?>
<?php include("./administrator/lib/constant.php"); ?>

<div class="">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <h3>Detail</h3>
    <hr/>
    <div class="">
        <?php
        $res_id = $_GET['id'];
        $bookingno = $_GET['bookingno'];
        $select_detail = "SELECT rbt_id as id, rbt_travel_date as travel_date ,rbt_adult_num as adult_num, rbt_child_num as child_num,rbt_infant_num as infant_num ,rbt_prices as prices,rbt_name as name ,rbt_ref as ref,rbt_ratetype as ratetype,rbt_time as time FROM reservation_boattransfer_items where reservations_id = $res_id union
				SELECT rat_id as id, rat_travel_date as travel_date ,rat_adult_num as adult_num, rat_child_num as child_num,rat_infant_num as infant_num ,rat_prices as prices,rat_name as name ,rat_ref as ref,rat_ratetype as ratetype,'-' as time FROM reservation_activity_items where reservations_id = $res_id union
				SELECT rct_id as id, rct_travel_date as travel_date ,rct_adult_num as adult_num, rct_child_num as child_num,rct_infant_num as infant_num ,rct_prices as prices,rct_name as name ,rct_ref as ref,rct_ratetype as ratetype,'-' as time FROM reservation_bustransfer_items where reservations_id = $res_id union
				SELECT rht_id as id, '-' as travel_date ,rht_adult_num as adult_num, rht_child_num as child_num,rht_infant_num as infant_num ,rht_prices as prices,rht_name as name ,rht_ref as ref,rht_ratetype as ratetype,'-' as time FROM reservation_hotel_items where reservations_id = $res_id union
				SELECT rpt_id as id, rpt_travel_date as travel_date ,rpt_adult_num as adult_num, rpt_child_num as child_num,rpt_infant_num as infant_num ,rpt_prices as prices,rpt_name as name ,rpt_ref as ref,rpt_ratetype as ratetype,'-'as time FROM reservation_pickuptransfer_items where reservations_id = $res_id union
				SELECT rplt_id as id, rplt_travel_date as travel_date ,rplt_adult_num as adult_num, rplt_child_num as child_num,rplt_infant_num as infant_num ,rplt_prices as prices,rplt_name as name ,rplt_ref as ref,rplt_ratetype as ratetype,'-' as time FROM reservation_privatelandtransfer_items where reservations_id = $res_id union
				SELECT rtt_id as id, rtt_travel_date as travel_date ,rtt_adult_num as adult_num, rtt_child_num as child_num,rtt_infant_num as infant_num ,rtt_prices as prices,rtt_name as name ,rtt_ref as ref,rtt_ratetype as ratetype,'-' as time FROM reservation_tour_items where reservations_id = $res_id union
				SELECT rrt_id as id, rrt_travel_date as travel_date ,rrt_adult_num as adult_num, rrt_child_num as child_num,rrt_infant_num as infant_num ,rrt_prices as prices,rrt_name as name ,rrt_ref as ref,rrt_ratetype as ratetype,'-' as time FROM reservation_traintransfer_items where reservations_id = $res_id";

        $result_detail = mysql_query($select_detail);
        $total = mysql_num_rows($result_detail);
        ?>
        <h4>Book No. <?= $bookingno ?></h4>
        <table class=" table table-hover ">
            <thead class="thead-default">
            <tr bgcolor="#cccccc">
                <th>Travel Date</th>
                <th>Adult Num</th>
                <th>Child Num</th>
                <th>Infant Num</th>
                <th>Prices</th>
                <th>Name</th>
                <th>Ref</th>
                <th>Ratetype</th>
                <th>Time</th>
            </tr>
            </thead>
            <?php while ($row_detail = mysql_fetch_assoc($result_detail)) { ?>
                <tr>
                    <td style="vertical-align: middle;"><?= $row_detail['travel_date'] ?></td>
                    <td style="vertical-align: middle;"><?= $row_detail['adult_num'] ?></td>
                    <td style="vertical-align: middle;"><?= $row_detail['child_num'] ?></td>
                    <td style="vertical-align: middle;"><?= $row_detail['infant_num'] ?></td>
                    <td style="vertical-align: middle;"><?= $row_detail['prices'] ?></td>
                    <td style="vertical-align: middle;"><?= $row_detail['name'] ?></td>
                    <td style="vertical-align: middle;"><?= $row_detail['ref'] ?></td>
                    <td style="vertical-align: middle;"><?= $row_detail['ratetype'] ?></td>
                    <td style="vertical-align: middle;"><?= $row_detail['time'] ?></td>
                </tr>
                <?php
            } ?>
        </table>
        <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
    </div>
</div>
