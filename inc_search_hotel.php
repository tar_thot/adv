<table width="1000" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
        <td height="100" bgcolor="#FFFFFF">

            <script type="text/javascript">
                function frmSearchClick() {
                    document.frm_search.submit();
                }
            </script>
            <form name="frm_search" id="frm_search" action="hotel_list.php" method="post">
                <table width="980" border="0" align="center" cellpadding="0" cellspacing="0">
                    <tr>
                        <td height="5" align="center" valign="top"><img src="images/blank.gif" width="100" height="5"
                                                                        border="0"/></td>
                    </tr>
                    <tr>
                        <td width="60"><img src="images/icon_location.jpg" width="50" height="50" hspace="5" vspace="5"
                                            border="0"/></td>
                        <td width="190" valign="middle">
                            <table width="200" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class="h1"><font color="#000000">SEARCH PRODUCTS</font></td>
                                </tr>
                                <tr>
                                    <td class="h4"><font color="#000000">Product: Hotel</font></td>
                                </tr>
                                <tr>
                                    <td>www.booking.adv-tour.com</td>
                                </tr>
                            </table>
                        </td>
                        <td width="1" bgcolor="#CCCCCC"><img src="images/blank.gif" width="1" height="50" border="0"/>
                        </td>
                        <td width="9" valign="top"><img src="images/blank.gif" width="9" height="20" border="0"/></td>
                        <td width="720" valign="top">
                            <table width="720" border="0" cellspacing="1" cellpadding="1">
                                <tr>
                                    <td width="49">Name:</td>
                                    <td width="146"><input class=" form-control" type="text" name="hot_name"
                                                           value="<?= $hot_name ?>"/></td>
                                    <td width="44">Area:</td>
                                    <td width="220"><select class="form-control" name="area_id" id="area_id"
                                                            style="width:170px">
                                            <option value="">-- Select --</option>
                                            <?php listbox('hotel_con_area', 'con_id', 'con_name', $area_id, 'N'); ?>
                                        </select></td>
                                    <td width="78">Room Type:</td>
                                    <td width="164"><select class="form-control" name="room_type_id" id="room_type_id"
                                                            style="width:150px;">
                                            <option value="">-- Select --</option>
                                            <?php listbox('hotel_con_room_type', 'con_id', 'con_name', $room_type_id, 'N'); ?>
                                        </select></td>

                                </tr>
                                <tr>
                                    <td></td>
                                    <td height="5" align="center" valign="top"><img src="images/blank.gif" width="100"
                                                                                    height="5" border="0"/></td>
                                </tr>
                                <tr>
                                    <td>Ref:</td>
                                    <td><input class="form-control" class="form-control" type="text" name="hot_ref"
                                               class="search-01" value="<?= $hot_ref ?>"/></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td height="5" align="center" valign="top"><img src="images/blank.gif" width="100"
                                                                                    height="5" border="0"/></td>
                                </tr>
                            </table>
                            <table width="720" border="0" cellspacing="0" cellpadding="0">

                                <tr>
                                    <div height="30" align="center">
                                        <a href="#" class="btn btn-primary btn-block"
                                           onclick="frmSearchClick();">Search</a>
                                    </div>
                                </tr>
                                <tr>
                                    <td height="5" align="center" valign="top"><img src="images/blank.gif" width="100"
                                                                                    height="5" border="0"/></td>
                                </tr>


                            </table>

                        </td>
                    </tr>
                </table>
            </form>
        </td>
    </tr>
</table>