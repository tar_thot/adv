<?php session_start();
include("./administrator/lib/connect.php"); ?>
<?php include("./administrator/lib/function.php"); ?>
<?php include("./administrator/lib/constant.php"); ?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Best price Package, Hotels, Transfer, Air Tickets</title>
    <meta name="description"
          content="ADV Tour, Phi Phi Island Tour & Transfer operator, Hotel & Transport arrangement in the south of Thailand"/>
    <meta name="keywords"
          content="Ko Tao Island, Samui Island, Phi Phi Island, Ferries, Boat Transfer, Tours, Package, Hotels, Diving, Liveaboard, Thailand"/>
    <meta name="Classification" content="World wide tour operate">
    <meta name="Author" content="S.S.ADV Co., Ltd. Phuket,Thailand">


    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link href="css/font.css" rel="stylesheet" type="text/css"/>
    <!-- menu slide -->
    <script type="text/javascript" src="js/script.js"></script>
    <!-- menu slide -->

    <!-- js-vallenato -->
    <script type="text/javascript" src="js-vtip/jquery-1.3.2.min.js"></script>
    <script type="text/javascript" src="js-vtip/jquery-plugin-vtip.js"></script>
    <script src="js-vallenato/vallenato.js" type="text/javascript" charset="utf-8"></script>
    <link rel="stylesheet" href="js-vallenato/vallenato.css" type="text/css" media="screen" charset="utf-8">
    <!-- js-vallenato -->


    <script type="text/javascript" src="./javascript/calendar/calendarDateInput.js"></script>
    <script type="text/javascript" src="./javascript/for_popup_calendar/cal2.js"></script>
    <script type="text/javascript" src="./javascript/for_popup_calendar/cal_conf2.js"></script>
</head>

<body>
<!-- include top login-->
<?php include("inc_login.php"); ?>
<!-- end include top login -->

<!-- include logo & menu-->
<?php include("inc_menu.php"); ?>
<!-- end include logo & menu -->

<!-- Detail -->

<!-- Sort by -->

<table width="1000" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
        <td valign="top" class="h1"><img src="images/blank.gif" width="100" height="10" border="0"/></td>
    </tr>
    <tr>
        <td height="40" bgcolor="#FFFFFF" class="h2"><img src="images/blank.gif" width="10" height="40" border="0"
                                                          align="absmiddle"> Contact Us
        </td>
    </tr>
</table>
<table width="1000" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
        <td width="500" align="left" valign="top"><span class="h1"><img src="images/pic_under_line.jpg" width="200"
                                                                        height="15" border="0"/></span></td>
        <td align="right"><span class="h1"><img src="images/pic_under_line1.jpg" width="200" height="15"
                                                border="0"/></span></td>
    </tr>
</table>

<table width="800" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
        <td height="100" bgcolor="#FFFFFF">
            <h4><img src="images/blamk.gif" width="10" height="1"></h4>
            <table width="800" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>

                    <td width="1" valign="middle"></td>

                    <td width="800" valign="top" align="center">
                        <table width="720" border="0" cellspacing="1" cellpadding="1">
                            <tr>
                                <td width="720">
                                    <strong>S.S.ADV. Co., Ltd.</strong><br/>
                                    <!--                                    23/4 Luang-Poh Road, T. Talad-Yai, Ampur Muang, Phuket 83000 Thailand<br />-->
                                    172 Khaosan road, Taladyod, Pranakorn Bangkok 10200 Thailand<br/>
                                    <strong>TAT License : </strong>34/00549<br/>
                                    <strong>Tel. </strong>+ 66 280 8074 <strong>Fax.</strong> + 66 280 1961<br/>
                                    <!--                                    <strong>Email :</strong> <a href="mailto:info@adv-tour.com" target="_parent" class="linkfoot">info@adv-tour.com</a>,-->
                                    <strong>Email :</strong> <a href="mailto:info@adv-tour.com" target="_blank"
                                                                class="linkfoot">info@adv-tour.com</a><br/><br/>
                                </td>
                            </tr>
                            <tr>
                                <td width="720" align="center">
                                    <iframe
                                        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3952.085936773129!2d98.39823609999999!3d7.886077700000001!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x305031f8022cbae9%3A0xe1208069e40e53af!2z4Lit4Lix4LiZ4LiU4Liy4Lih4Lix4LiZIOC5gOC4p-C4nyDguKHguLLguKrguYDguJXguK3guKPguYw!5e0!3m2!1sth!2s!4v1435632861013"
                                        width="565" height="365" frameborder="0" style="border:0"
                                        allowfullscreen></iframe>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <h4><img src="images/blamk.gif" width="10" height="1"></h4>
        </td>
    </tr>
</table>

<br/>

<table width="800" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
        <td height="100" bgcolor="#FFFFFF">

            <script type="text/javascript">
                function checkForm() {
                    var verify_code = document.frm_contact.verify_code.value;
                    var name = document.frm_contact.name.value;
                    var messages = document.frm_contact.messages.value;
                    var emailFilter = /^[_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)*\@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*(\.([a-zA-Z]){2,4})$/;

                    var str = document.frm_contact.email.value;

                    if (str == '') {
                        alert("E-mail is required");
                        return false;
                    }


                    if (!(emailFilter.test(str))) {
                        alert("E-mail not valid");
                        return false;
                    }

                    if (name == '') {
                        alert('Name is required. ');
                        return false;
                    }

                    if (messages == '') {
                        alert('Message is required. ');
                        return false;
                    }

                    if (verify_code == '') {
                        alert('Verify Code is required. ');
                        return false;
                    }

                    return true;
                }
            </script>

            <h4><img src="images/blamk.gif" width="10" height="1"> Please enter below information.</h4>

            <!--<form name="frm_contact" id="frm_contact" action="send_contact.php" method="post" onSubmit="MM_validateForm('name','','R','comment','','R');return document.MM_returnValue">-->
            <form name="frm_contact" id="frm_contact" action="send_contact.php" method="post"
                  enctype="multipart/form-data" onSubmit="return checkForm();">
                <table width="800" border="0" align="center" cellpadding="0" cellspacing="0">
                    <tr>

                        <td width="1" valign="middle"></td>

                        <td width="800" valign="top" align="center">
                            <table width="720" border="0" cellspacing="1" cellpadding="1">
                                <tr>
                                    <td width="148" align="right"><font color="red">*</font> Name : &nbsp;</td>
                                    <td width="565">
                                        <input class="form-control" type="text" name="name" id="name" value=""
                                               size="40">
                                    </td>
                                </tr>
                                <tr>
                                    <td height="5" align="center" valign="top"><img src="images/blank.gif" width="100"
                                                                                    height="5" border="0"/></td>
                                </tr>
                                <tr>
                                    <td width="148" align="right"><font color="red">*</font> Email : &nbsp;</td>
                                    <td width="565"><input class="form-control" type="text" name="email" id="email"
                                                           value="" size="40"></td>
                                </tr>
                                <tr>
                                    <td height="5" align="center" valign="top"><img src="images/blank.gif" width="100"
                                                                                    height="5" border="0"/></td>
                                </tr>
                                <tr>
                                    <td width="148" align="right">Message : &nbsp;</td>
                                    <td width="565"><textarea class="form-control" name="messages" id="messages"
                                                              cols="50" rows="5"></textarea></td>
                                </tr>
                                <tr>
                                    <td height="5" align="center" valign="top"><img src="images/blank.gif" width="100"
                                                                                    height="5" border="0"/></td>
                                </tr>
                                <tr>
                                    <td width="148" align="right"><font color="red">*</font>Verify&nbsp;Code&nbsp; :
                                        &nbsp;</td>
                                    <td width="565">
                                        <img src="./captcha/captcha.php" align="absmiddle"/>
                                        <input class="form-control" type="text" name="verify_code" id="verify_code"
                                               maxlength="5" size="5"/>

                                    </td>
                                </tr>
                                <tr>
                                    <td height="5" align="center" valign="top">
                                        <img src="images/blank.gif" width="100" height="5" border="0"/>
                                    </td>
                                </tr>


                                <table width="720" border="0" cellspacing="1" cellpadding="1">
                                    <tr>
                                        <td height="5" align="center" valign="top"><img src="images/blank.gif"
                                                                                        width="100"
                                                                                        height="5" border="0"/></td>
                                    </tr>
                                    <tr>
                                        <td width="148" align="right"></td>
                                        <td width="" align="center"><input class="form-control btn btn-success"
                                                                           type="submit" name="Submit"
                                                                           value="Submit"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td height="5" align="center" valign="top"><img src="images/blank.gif"
                                                                                        width="100"
                                                                                        height="5" border="0"/></td>
                                    </tr>
                                </table>
                            </table>

                        </td>
                    </tr>
                </table>
            </form>
        </td>
    </tr>
</table>

<!-- END Comment -->

<br/>

<!-- Hotel Photo-->

<!-- Hotel Map--><!-- Hotel Map-->

<!-- Hotel Map-->
<!-- Hotel Map-->

<!-- Detail -->

<!-- include footer-->
<?php include("inc_footer.php"); ?>
<!-- end include footer -->


</body>
</html>
