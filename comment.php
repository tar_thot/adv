<?php session_start(); ?>
<?php include("./administrator/lib/connect.php"); ?>
<?php include("./administrator/lib/function.php"); ?>
<?php include("./administrator/lib/constant.php"); ?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<title>Best price Package, Hotels, Transfer, Air Tickets</title>
	<meta name="description"
		  content="ADV Tour., Phi Phi Island Tour & Transfer operator, Hotel & Transport arrangement in the south of Thailand"/>
	<meta name="keywords"
		  content="Ko Tao Island, Samui Island, Phi Phi Island, Ferries, Boat Transfer, Tours, Package, Hotels, Diving, Liveaboard, Thailand"/>
	<meta name="Classification" content="World wide tour operate">
	<meta name="Author" content="S.S.ADV. Co., Ltd. Bangkok,Thailand">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link href="css/font.css" rel="stylesheet" type="text/css"/>

	<!-- menu slide -->
	<script type="text/javascript" src="js/script.js"></script>
	<!-- menu slide -->

	<!-- js-vallenato -->
	<script type="text/javascript" src="js-vtip/jquery-1.3.2.min.js"></script>
	<script type="text/javascript" src="js-vtip/jquery-plugin-vtip.js"></script>
	<script src="js-vallenato/vallenato.js" type="text/javascript" charset="utf-8"></script>
	<link rel="stylesheet" href="js-vallenato/vallenato.css" type="text/css" media="screen" charset="utf-8">

	<!-- js-vallenato -->


	<script type="text/javascript" src="./javascript/calendar/calendarDateInput.js"></script>
	<script type="text/javascript" src="./javascript/for_popup_calendar/cal2.js"></script>
	<script type="text/javascript" src="./javascript/for_popup_calendar/cal_conf2.js"></script>
</head>

<body>
<!-- include top login-->
<?php include("inc_login.php"); ?>
<!-- end include top login -->

<!-- include logo & menu-->
<?php include("inc_menu.php"); ?>
<!-- end include logo & menu -->

<!-- Detail -->

<!-- Sort by -->

<table width="1000" border="0" align="center" cellpadding="0" cellspacing="0">
	<tr>
		<td valign="top" class="h1"><img src="images/blank.gif" width="100" height="10" border="0"/></td>
	</tr>
	<tr>
		<td height="40" bgcolor="#FFFFFF" class="h2"><img src="images/blank.gif" width="10" height="40" border="0"
														  align="absmiddle"> Comments
		</td>
	</tr>
</table>
<table width="1000" border="0" align="center" cellpadding="0" cellspacing="0">
	<tr>
		<td width="500" align="left" valign="top"><span class="h1"><img src="images/pic_under_line.jpg" width="200"
																		height="15" border="0"/></span></td>
		<td align="right"><span class="h1"><img src="images/pic_under_line1.jpg" width="200" height="15"
												border="0"/></span></td>
	</tr>
</table>

<!-- Comment -->

<?php
$sql_comments = "select * from comments_home where status = '1' order by comment_date DESC";
$result_comments = mysql_query($sql_comments);
$row_comments = mysql_fetch_array($result_comments);

if ($row_comments[0]) {
	?>

	<table width="700" border="0" align="center" cellpadding="0" cellspacing="0">
		<tr>
			<td height="100" bgcolor="#FFFFFF">


				<h4><img src="images/blamk.gif" width="10" height="1"></h4>

				<table width="700" border="0" align="center" cellpadding="0" cellspacing="0">
					<tr>

						<td width="1" valign="middle"></td>

						<td width="1000" valign="top" align="center">
							<table width="500" border="0" cellspacing="1" cellpadding="1">

								<?php
								$sql_comment = "select * from comments_home where status = '1' order by comment_date DESC";
								$result_comment = mysql_query($sql_comment);
								while ($row_comment = mysql_fetch_array($result_comment)) {
									?>

									<tr>
										<td width="69" align="right"><b>Name : </b></td>
										<td width="199">
											<?php if ($row_comment['ag_id'] != 0 && $row_comment['cl_id'] == 0) {
												$ag_name = get_value('agents', 'ag_id', 'ag_name', $row_comment['ag_id']);

												if ($ag_name) {
													echo "$ag_name";
												}

											} elseif ($row_comment['ag_id'] == 0 && $row_comment['cl_id'] != 0) {
												$cl_name = get_value('clients', 'cl_id', 'cl_name', $row_comment['cl_id']);
												$cl_lname = get_value('clients', 'cl_id', 'cl_lname', $row_comment['cl_id']);

												//if($cl_name){echo "$cl_name $cl_lname";}
												if ($row_comment['cl_id'] == 1) {
													echo "<b><font color=\"red\">$cl_name</font></b>";
												} else {
													echo "$cl_name $cl_lname";
												}

											} else {
												if ($row_comment['name']) {
													echo $row_comment['name'];
												}

											}
											?>
										</td>
										<td width="69" align="right"><b>Date : </b></td>
										<td width="150"><?= $row_comment['comment_date'] ?></td>
									</tr>

									<tr>
										<td align="right" valign="top"><b>Comment : </b></td>
										<td colspan="3"><?= $row_comment['comment'] ?></td>
									</tr>
									<tr>
										<td colspan="4" align="center">
											<hr/>
										</td>
									</tr>

								<?php } // END while($row_comment = mysql_fetch_array($result_comment)){
								?>

							</table>

						</td>
					</tr>
				</table>

				<h4><img src="images/blamk.gif" width="10" height="1"></h4>

			</td>
		</tr>
	</table>

	<?php
} // END if($row_comments[0]){
?>

<br/>


<table width="1000" border="0" align="center" cellpadding="0" cellspacing="0">
	<tr>
		<td height="100" bgcolor="#FFFFFF">

			<script type="text/javascript">
				<!--
				function MM_validateForm() { //v4.0
					if (document.getElementById) {
						var i, p, q, nm, test, num, min, max, errors = '', args = MM_validateForm.arguments;
						for (i = 0; i < (args.length - 2); i += 3) {
							test = args[i + 2];
							val = document.getElementById(args[i]);
							if (val) {
								nm = val.name;
								if ((val = val.value) != "") {
									if (test.indexOf('isEmail') != -1) {
										p = val.indexOf('@');
										if (p < 1 || p == (val.length - 1)) errors += '- ' + nm + ' must contain an e-mail address.\n';
									} else if (test != 'R') {
										num = parseFloat(val);
										if (isNaN(val)) errors += '- ' + nm + ' must contain a number.\n';
										if (test.indexOf('inRange') != -1) {
											p = test.indexOf(':');
											min = test.substring(8, p);
											max = test.substring(p + 1);
											if (num < min || max < num) errors += '- ' + nm + ' must contain a number between ' + min + ' and ' + max + '.\n';
										}
									}
								} else if (test.charAt(0) == 'R') errors += '- ' + nm + ' is required.\n';
							}
						}
						if (errors) alert('The following error(s) occurred:\n' + errors);
						document.MM_returnValue = (errors == '');
					}
				}
				//-->
			</script>

			<h4><img src="images/blamk.gif" width="10" height="1"> Please enter below information.</h4>

			<?php
			if (isset($_SESSION['clients']['cl_id'])) {
				$cl_id = isset($_SESSION['clients']['cl_id']) ? $_SESSION['clients']['cl_id'] : 0;
				$ag_id = isset($_SESSION['agents']['ag_id']) ? $_SESSION['agents']['ag_id'] : null;

				$ag_names = get_value('agents', 'ag_id', 'ag_name', $ag_id);

				$cl_names = get_value('clients', 'cl_id', 'cl_name', $cl_id);
				$cl_lnames = get_value('clients', 'cl_id', 'cl_lname', $cl_id);
				$cl_fname = $cl_names . " " . $cl_lnames;
			}


			?>

			<form name="frm_comment" id="frm_comment" action="save_comment_home.php" method="post"
				  onSubmit="MM_validateForm('name','','R','comment','','R');return document.MM_returnValue">
				<input type="hidden" name="cl_id" value="<?= $cl_id ?>">
				<input type="hidden" name="ag_id" value="<?= $ag_id ?>">
				<table width="980" border="0" align="center" cellpadding="0" cellspacing="0">
					<tr>

						<td width="1" valign="middle"></td>

						<td width="1000" valign="top" align="center">
							<table width="720" border="0" cellspacing="1" cellpadding="1">
								<tr>
									<td width="148" align="right">Name : &nbsp;</td>
									<td width="565">

										<?php if (isset($ag_id)) { ?>
											<input class="form-control" type="text" name="name" id="name"
												   value="<?= $ag_names ?>" size="40" readonly="readonly"
												   style="background-color:#EAEAEA;">
										<?php } elseif (isset($cl_id)) { ?>
											<input class="form-control" type="text" name="name" id="name"
												   value="<?= $cl_fname ?>" size="40" readonly="readonly"
												   style="background-color:#EAEAEA;">
										<?php } else { ?>
											<input class="form-control" type="text" name="name" id="name" value=""
												   size="40">
										<?php } ?>

									</td>

								</tr>
								<tr>
									<td height="5" align="center" valign="top"><img src="images/blank.gif" width="100"
																					height="5" border="0"/></td>
								</tr>
								<tr>
									<td width="148" align="right">Comment :&nbsp;</td>
									<td width="565"><textarea class="form-control" name="comment" id="comment" cols="50"
															  rows="5"></textarea></td>
								</tr>
								<tr>
									<td height="5" align="center" valign="top"><img src="images/blank.gif" width="100"
																					height="5" border="0"/></td>
								</tr>
								<tr>
									<td width="148" align="right"><font color="red">*</font> Verify&nbsp;Code&nbsp;:&nbsp;
									</td>
									<td width="400">
										<img src="./captcha/captcha.php" align="absmiddle"/>
										<input class="form-control" type="text" name="verify_code" id="verify_code"
											   maxlength="5" size="5"/>
									</td>
							</table>

							<table width="720" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td height="5" align="center" valign="top"><img src="images/blank.gif" width="100"
																					height="5" border="0"/></td>
								</tr>
								<tr>
									<td width="148" align="right"></td>
									<td height="30" align="center"><input class="form-control btn btn-success"
																		  type="submit" name="Submit" value="Submit"/>
									</td>
								</tr>
								<tr>
									<td height="5" align="center" valign="top"><img src="images/blank.gif" width="100"
																					height="5" border="0"/></td>
								</tr>
							</table>

						</td>
					</tr>
				</table>
			</form>
		</td>
	</tr>
</table>

<!-- END Comment -->

<br/>

<!-- Hotel Photo-->

<!-- Hotel Map--><!-- Hotel Map-->

<!-- Hotel Map-->
<!-- Hotel Map-->

<!-- Detail -->

<!-- include footer-->
<?php include("inc_footer.php"); ?>
<!-- end include footer -->


</body>
</html>
