<?php session_start(); ?>
<?php include("./administrator/lib/connect.php"); ?>
<?php include("./administrator/lib/function.php"); ?>
<?php include("./administrator/lib/constant.php"); ?>
<?php ob_start(); ?>
<?php

function setNumberLength($num, $length)
{
	$sumstr = strlen($num);
	$zero = str_repeat("0", $length - $sumstr);
	$results = "9" . $zero . $num;

	return $results;
}

function watermarkImage($SourceFile, $WaterMarkText, $DestinationFile, $axis_x, $axis_y)
{
	list($width, $height) = getimagesize($SourceFile);
	$image_p = imagecreatetruecolor($width, $height);
	$image = imagecreatefromjpeg($SourceFile);
	imagecopyresampled($image_p, $image, 0, 0, 0, 0, $width, $height, $width, $height);
	$black = imagecolorallocate($image_p, 005, 0, 0);//Colour

	$font = './photo/invoice/psldisplay-webfont.ttf';//The font name.
	$font_size = 16; //Set font size.
	$axis_x = $axis_x;
	$axis_y = $axis_y;
	imagettftext($image_p, $font_size, 0, $axis_x, $axis_y, $black, $font, $WaterMarkText);

	if ($DestinationFile <> '') {
		imagejpeg($image_p, $DestinationFile, 100);
	} else {
		header('Content-Type: image/jpeg');
		imagejpeg($image_p, null, 100);
	};

	imagedestroy($image);
	imagedestroy($image_p);
}

;

$client_id = $_SESSION['clients']['cl_id'];

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<title>Best price Package, Hotels, Transfer, Air Tickets</title>
	<meta name="description" content="ADV Tour., Phi Phi Island Tour & Transfer operator, Hotel & Transport arrangement in the south of Thailand
"/>
	<meta name="keywords"
		  content="Ko Tao Island, Samui Island, Phi Phi Island, Ferries, Boat Transfer, Tours, Package, Hotels, Diving, Liveaboard, Thailand"/>
	<meta name="Classification" content="World wide tour operate">
	<meta name="Author" content="S.S.ADV. Co., Ltd. Bangkok,Thailand">


</head>

<body>

<?php
include("value_get.php");
$verify_code = isset($_POST['verify_code']) ? $_POST['verify_code'] : null;
if (($_SESSION['verify_code'] == $verify_code)) {


	$cart_total_price = 0;
	$sum_total_price = $_POST['sum_total_price'];

	$agent_id = $_SESSION['agents']['ag_id'];
	$agent_name = $_SESSION['agents']['ag_name'];

	$agentpaytype_id = $_SESSION['agents']['agentpaytype_id'];
	$agent_balance_credit = $_SESSION['agents']['ag_balance_credit'];
	$agent_min_credit = $_SESSION['agents']['ag_min_credit'];

	$agent_re_credit = $agent_balance_credit - $sum_total_price;

	# Check Payment
	if ($agent_id && $agentpaytype_id == 2) {
		if ($agent_balance_credit <= $agent_min_credit || $sum_total_price > $agent_balance_credit) {
			echo '<script>alert("You have not enough credit.")</script>';
			echo "<meta http-equiv=\"refresh\" content=\"0; url = './confirm_package.php'\" >";
			exit();
		} else if ($agent_re_credit <= $agent_min_credit) {
			echo '<script>alert("You have not enough credit.")</script>';
			echo "<meta http-equiv=\"refresh\" content=\"0; url = './confirm_package.php'\" >";
			exit();
		} else {
			$sql_ag = "update agents set ";

			$sql_ag .= "ag_balance_credit = '$agent_re_credit', ";

			$sql_ag .= "last_edit_time = now() ";
			$sql_ag .= " where ag_id = '$agent_id'";

			//echo "sql : $sql ";
			//exit();

			$result_ag = mysql_query($sql_ag);

			$_SESSION['agents']['ag_balance_credit'] = $agent_re_credit;

		}
	}

	?>

	<br/>

	<!-- Start Reservations Package-->
	<?php
	$agents_id = isset($_SESSION['agents']['ag_id']) ? $_SESSION['agents']['ag_id'] : null;

	$agentfor_id = get_value('agents', 'ag_id', 'agentfor_id', $agents_id);
	$agentpaytype_id = get_value('agents', 'ag_id', 'agentpaytype_id', $agents_id);

	if (!$agents_id) {
		$agents_id = 0;
		$agentfor_id = 3;
		$last_edit_by = 'Online User';

		//$confirm_pay_id = 1;

	} else {
		$last_edit_by = isset($_SESSION['agents']['ag_name']) ? $_SESSION['agents']['ag_name'] : null;

		//if($agentpaytype_id == 2){ $confirm_pay_id = 2; }else{ $confirm_pay_id = 1; }

	}
	# ---- Insert Table reservation_packages
	if (!$reservationpackages_id) {
		$sql = "insert into reservation_packages () values ()";
		$result = mysql_query($sql);
		$reservationpackages_id = mysql_insert_id();
	}
	if ($reservationpackages_id) {

		$sql = "update reservation_packages set ";


		$sql .= "titlename_id = '" . $_POST['titlename_id'] . "', ";
		$sql .= "rpa_fname = '" . $_POST['firstname'] . "', ";
		$sql .= "rpa_lname = '" . $_POST['lastname'] . "', ";
		$sql .= "rpa_email = '" . $_POST['email'] . "', ";
		$sql .= "rpa_phone = '" . $_POST['phone'] . "', ";
		$sql .= "rpa_fax = '" . $_POST['fax_number'] . "', ";
		$sql .= "rpa_address = '" . $_POST['address'] . "', ";
		$sql .= "rpa_request = '" . $_POST['comment'] . "', ";
		$sql .= "agents_id = '$agents_id', ";
		$sql .= "rpa_date = now(), ";

		if ($agentpaytype_id == 2) {
			$sql .= "bookingstatus_id = '3', ";
		} else {
			$sql .= "bookingstatus_id = '1', ";
		}

		$sql .= "paymentstatus_id = '1', ";
		$sql .= "rpa_id_str = 'C" . setNumberLength($reservationpackages_id, 7) . "', ";
		$sql .= "rpa_bookby = '" . $_POST['book_by'] . "', ";
		$sql .= "rpa_agent_voucher = '" . $_POST['agent_voucher'] . "', ";
		$sql .= "rpa_cash_collection = '" . $_POST['cash_collection'] . "', ";
		$sql .= "company = '$agentfor_id', ";
		$sql .= "cl_id = '$client_id', ";
		$sql .= "prices = '$sum_total_price', ";
		//$sql .= "confirm_pay_id = '$confirm_pay_id', ";

		$sql .= "last_edit_time = now() ";
		$sql .= " where rpa_id = '$reservationpackages_id'";

		//echo "sql : $sql ";
		//exit();

		$result = mysql_query($sql);

		# ---- Send Email

		$sql_email = "SELECT * FROM reservation_packages WHERE rpa_id = $reservationpackages_id";
		$result_email = mysql_query($sql_email);
		$row_email = mysql_fetch_array($result_email);

		if ($row_email['rpa_email'] != "") {

			$title_email = get_value('lis_titlename', 'lis_id', 'lis_name', $row_email['titlename_id']);
			$rpa_date = DateFormat($row_email['rpa_date'], "s");
			$ag_name = get_value('agents', 'ag_id', 'ag_name', $row_email['agents_id']);
			$company_name = get_value('lis_company', 'lis_id', 'lis_name', $row_email['company']);
			$ag_email = get_value('agents', 'ag_id', 'ag_email', $row_email['agents_id']);
			$ag_tel = get_value('agents', 'ag_id', 'ag_tel', $row_email['agents_id']);
			$ag_fax = get_value('agents', 'ag_id', 'ag_fax', $row_email['agents_id']);
			$bookingstatus = get_value('lis_booking_status', 'lis_id', 'lis_name', $row_email['bookingstatus_id']);
			$paymentstatus = get_value('lis_payment_status', 'lis_id', 'lis_name', $row_email['paymentstatus_id']);

			$companyname = "S.S.ADV.";
			$from_mail = "phiphibooking@gmail.com";
			//$from_mail = "info@andamanwavemaster.com";
			//$from_mail = "boss@phuketsolution.com";

			$to_real = $title_email . " " . $row_email['rpa_fname'] . " " . $row_email['rpa_lname'];

			if ($agentpaytype_id == 3) {
				//$to_mail = "$ag_email";
				$cc_mail = "phiphibooking@gmail.com, ac.phiphibooking@gmail.com";

			} else {
				$to_mail = $row_email['res_email'];
				$cc_mail = $from_mail;

			}

			# ---- TO SEND HTML MAIL, YOU CAN SET THE CONTENT-TYPE HEADER.
			$head = "MIME-Version: 1.0\r\n";
			$head .= "Content-type: text/html; charset=utf-8\r\n";
			$head .= "From: \"" . $companyname . "\" <" . $from_mail . ">\n";
			//$head .= "To: \"".$to_real."\" <".$to_mail.">\n";
			if ($cc_mail) $head .= "Cc: " . $cc_mail . "\r\n";
			$head .= "X-Priority: 1 (High)\r\n";
			$head .= "X-Mailer: <phuket-phiphi-krabi.com>\r\n";
			$head .= "MIME-Version: 1.0\r\n";

			$message = "";

			if ($row_email['agents_id']) {

				$text_message = "<table style='border:1px solid black; width:800px'>
										<tr>
											<td>Reservation ID : " . $row_email['rpa_id_str'] . "</td>
											<td>Book Date : " . $rpa_date . "</td>
										</tr>
										<tr>
											<td colspan='2'>&nbsp;</td>
										</tr>
										<tr>
											<td colspan='2'><b>Agent Detail</b></td>
										</tr>
										<tr>
											<td>Agent Name : " . $ag_name . "</td>
											<td>Company : " . $company_name . "</td>
										</tr>
										<tr>
											<td>Booking by : " . $row_email['rpa_bookby'] . "</td>
											<td>Email : " . $ag_email . "</td>
										</tr>
										<tr>
											<td>Phone Number : " . $ag_tel . "</td>
											<td>Cash Collection : " . $row_email['rpa_cash_collection'] . "</td>
										</tr>
										<tr>
											<td>Fax Number : " . $ag_fax . "</td>
											<td>&nbsp;</td>
										</tr>
										<tr>
											<td colspan='2'>&nbsp;</td>
										</tr>
										<tr>
											<td colspan='2'><b>Customer Detail</b></td>
										</tr>
										<tr>
											<td>Customer Name : " . $title_email . " " . $row_email['rpa_fname'] . " " . $row_email['rpa_lname'] . "</td>
											<td>Email : " . $row_email['rpa_email'] . "</td>
										</tr>
										<tr>
											<td>Phone Number : " . $row_email['rpa_phone'] . "</td>
											<td>Booking Status : " . $bookingstatus . "</td>
										</tr>
										<tr>
											<td>Address : " . $row_email['rpa_address'] . "</td>
											<td>Payment Status : " . $paymentstatus . "</td>
										</tr>
										<tr>
											<td>Remark : " . $row_email['rpa_request'] . "</td>
											<td>&nbsp;</td>
										</tr>";

			} else {

				$text_message = "<table style='border:1px solid black; width:800px'>
										<tr>
											<td>Reservation ID : " . $row_email['rpa_id_str'] . "</td>
											<td>Book Date : " . $rpa_date . "</td>
										</tr>
										<tr>
											<td colspan='2'>&nbsp;</td>
										</tr>
										<tr>
											<td colspan='2'><b>Customer Detail</b></td>
										</tr>
										<tr>
											<td>Customer Name : " . $title_email . " " . $row_email['rpa_fname'] . " " . $row_email['rpa_lname'] . "</td>
											<td>Email : " . $row_email['rpa_email'] . "</td>
										</tr>
										<tr>
											<td>Phone Number : " . $row_email['rpa_phone'] . "</td>
											<td>Booking Status : " . $bookingstatus . "</td>
										</tr>
										<tr>
											<td>Address : " . $row_email['rpa_address'] . "</td>
											<td>Payment Status : " . $paymentstatus . "</td>
										</tr>
										<tr>
											<td>Remark : " . $row_email['rpa_request'] . "</td>
											<td>&nbsp;</td>
										</tr>";

			} // END if($row_email['agents_id']){


		} // END if($row_email[rpa_email] != ""){

		# ---- END Send Email

	} // END if ($reservationpackages_id)


	# ---- Invoice

	if ($agent_id && $agentpaytype_id == 3) {

		$i = 1;
		$price_total = 0;

		$invoice_today = date('y') . date('m') . date('d');
		$in_date = DateFormat($today, "s");

		$add_one_day = strtotime($today . '+ 1 day');
		$add_one_day = date("Y-m-d", $add_one_day);
		$in_date_one_day = DateFormat($add_one_day, "s");

		if (!$in_id) {
			$sql_in = "insert into invoice () values ()";
			$result_in = mysql_query($sql_in);
			$in_id = mysql_insert_id();
		}

		$in_no = $invoice_today . setNumberLength($in_id, 7);
		$in_pic = $invoice_today . setNumberLength($in_id, 7);

		$in_fname = $in_no . ".jpg";

		if ($in_id) {

			$sql_in = "update invoice set ";

			$sql_in .= "in_no = '$in_no', ";
			$sql_in .= "in_res_id = '" . $row_email['rpa_id_str'] . "', ";
			$sql_in .= "in_pic = '$in_fname', ";
			$sql_in .= "in_status = '2', ";
			$sql_in .= "in_page_no = '1', ";

			$sql_in .= "in_date = now() ";
			$sql_in .= " where in_id = '$in_id'";

			//echo "sql : $sql ";
			//exit();

			$result_in = mysql_query($sql_in);

		}

		$new_name = "./photo/invoice/$in_fname";
		$SourceFile = './photo/invoice/invoice-form.jpg'; //To Enter the water Mark
		$DestinationFile = $new_name; //Image files are stored

		$WaterMarkText = "$ag_name"; //The text is clear
		$axis_x = '170';
		$axis_y = '250';
		watermarkImage($SourceFile, $WaterMarkText, $DestinationFile, $axis_x, $axis_y);

		$SourceFile = $new_name; //To Enter the waterMark

		$WaterMarkText = "$ag_tel";
		$axis_x = '170';
		$axis_y = '285';
		watermarkImage($SourceFile, $WaterMarkText, $DestinationFile, $axis_x, $axis_y);

		$WaterMarkText = "$ag_email";
		$axis_x = '170';
		$axis_y = '320';
		watermarkImage($SourceFile, $WaterMarkText, $DestinationFile, $axis_x, $axis_y);

		$WaterMarkText = "$in_date";
		$axis_x = '800';
		$axis_y = '255';
		watermarkImage($SourceFile, $WaterMarkText, $DestinationFile, $axis_x, $axis_y);

		$WaterMarkText = "$in_date_one_day";
		$axis_x = '800';
		$axis_y = '285';
		watermarkImage($SourceFile, $WaterMarkText, $DestinationFile, $axis_x, $axis_y);

		$WaterMarkText = "$in_date";
		$axis_x = '800';
		$axis_y = '320';
		watermarkImage($SourceFile, $WaterMarkText, $DestinationFile, $axis_x, $axis_y);

		$WaterMarkText = $row_email['rpa_id_str'];
		$axis_x = '800';
		$axis_y = '360';
		watermarkImage($SourceFile, $WaterMarkText, $DestinationFile, $axis_x, $axis_y);

	}


	?>
	<!-- End Reservations Package-->


	<?php $text_message = $text_message . "<tr><td colspan='2'>&nbsp;</td></tr>	<tr><td colspan='2'><b>Booking Detail</b></td></tr>"; ?>


	<!-- Start Reservation Package Item -->
	<?php

	# Get Variable Cart from SESSION

	$packages_id = $_SESSION['cart_package']['packages_id'];
	$agentgrade_id = $_SESSION['cart_package']['agentgrade_id'];
	$res_pac_ratetype_id = $_SESSION['cart_package']['res_pac_ratetype_id'];
	$res_pac_room_num = $_SESSION['cart_package']['res_pac_room_num'];
	$res_pac_adult_num = $_SESSION['cart_package']['res_pac_adult_num'];
	$res_pac_child_num = $_SESSION['cart_package']['res_pac_child_num'];
	$res_pac_infant_num = $_SESSION['cart_package']['res_pac_infant_num'];
	$items_package_producttype_id_arr = $_SESSION['cart_package']['items_package_producttype_id_arr'];
	$items_id_arr = $_SESSION['cart_package']['items_id_arr'];
	$items_name_arr = $_SESSION['cart_package']['items_name_arr'];
	$items_ratetype_arr = $_SESSION['cart_package']['items_ratetype_arr'];
	$res_pac_travel_date_arr = $_SESSION['cart_package']['res_pac_travel_date_arr'];
	$res_pac_pickup_from_arr = $_SESSION['cart_package']['res_pac_pickup_from_arr'];
	$res_pac_pickup_to_arr = $_SESSION['cart_package']['res_pac_pickup_to_arr'];
	$res_pac_room_no_arr = $_SESSION['cart_package']['res_pac_room_no_arr'];
	$adults_prices = $_SESSION['cart_package']['adults_prices'];
	$childs_prices = $_SESSION['cart_package']['childs_prices'];
	$package_prices = $_SESSION['cart_package']['package_prices'];

	$num_of_people = $res_pac_adult_num * 1 + $res_pac_child_num * 1;
	$num_of_room = $res_pac_room_num * 1;

	if (isset($packages_id)) {

		$message_detail = "";

		$travel_date = "9999-99-99";

		foreach ($items_name_arr as $items_name_ids => $value) {
			$res_pac_travel_date = $res_pac_travel_date_arr[$items_name_ids];
			$res_pac_pickup_from = $res_pac_pickup_from_arr[$items_name_ids];
			$res_pac_pickup_to = $res_pac_pickup_to_arr[$items_name_ids];
			$res_pac_room_no = $res_pac_room_no_arr[$items_name_ids];
			$items_package_producttype_id = $items_package_producttype_id_arr[$items_name_ids];
			$items_id = $items_id_arr[$items_name_ids];
			$items_name = $items_name_arr[$items_name_ids];
			$items_ratetype = $items_ratetype_arr[$items_name_ids];


			if ($items_package_producttype_id == 1) {
				// Boattransfer
				#Get Variable Allocation $boaalloday_id
				$sql_boattransfer_allocationdaily = "select * from boattransfer_allocationdaily where boattransfers_id = '$items_id' ";
				$sql_boattransfer_allocationdaily .= "AND boaalloday_date = '$res_pac_travel_date' ";

				$result_boattransfer_allocationdaily = mysql_query($sql_boattransfer_allocationdaily);
				$row_boattransfer_allocationdaily = mysql_fetch_array($result_boattransfer_allocationdaily);
				$boaalloday_value = $row_boattransfer_allocationdaily['boaalloday_value'] * 1;
				$boaalloday_id = $row_boattransfer_allocationdaily[0];

				#Cut Allocation
				$sql_boattransfer_allocationdaily = "update boattransfer_allocationdaily set ";
				$sql_boattransfer_allocationdaily .= "boaalloday_value = boaalloday_value - $num_of_people , ";
				$sql_boattransfer_allocationdaily .= "last_edit_time = now() ";
				$sql_boattransfer_allocationdaily .= " where boaalloday_id = '$boaalloday_id'";
				//echo "sql_boattransfer_allocationdaily : $sql_boattransfer_allocationdaily <br />";
				$result_boattransfer_allocationdaily = mysql_query($sql_boattransfer_allocationdaily);

				if ($res_pac_travel_date < $travel_date) {
					$travel_date = $res_pac_travel_date;
				};

				$item_name = get_value('boattransfers', 'bot_id', 'bot_name', $items_id);
				$item_ref = get_value('boattransfers', 'bot_id', 'bot_ref', $items_id);
				$date_detail = DateFormat($res_pac_travel_date, "s");

				$message_detail = $message_detail . "<tr>
																			<td>Boat Transfer Name : $item_name</td>
																			<td>Ref : $item_ref</td>
																		 </tr>
																		 <tr>
																			<td>Travel Date : $date_detail</td>
																			<td>&nbsp;</td>
																		 </tr>
																		 <tr><td colspan='2'>&nbsp;</td></tr>";

			} // END if($items_package_producttype_id == 1){

			if ($items_package_producttype_id == 2) {
				// Pick Up Transfer

				if ($res_pac_travel_date < $travel_date) {
					$travel_date = $res_pac_travel_date;
				};

				$item_name = get_value('pickuptransfers', 'put_id', 'put_name', $items_id);
				$item_ref = get_value('pickuptransfers', 'put_id', 'put_ref', $items_id);
				$date_detail = DateFormat($res_pac_travel_date, "s");

				$message_detail = $message_detail . "<tr>
																			<td>Pick Up Transfer Name : $item_name</td>
																			<td>Ref : $item_ref</td>
																		 </tr>
																		 <tr>
																			<td>Pick Up From : $res_pac_pickup_from</td>
																			<td>Drop-off to : $res_pac_pickup_to</td>
																		 </tr>
																		 <tr>
																			<td>Room / Flight No. : $res_pac_room_no</td>
																			<td>Travel Date : $date_detail</td>
																		 </tr>
																		 <tr><td colspan='2'>&nbsp;</td></tr>";

			} // END if($items_package_producttype_id == 2){

			if ($items_package_producttype_id == 3) {
				// Tour
				#Get Variable Allocation $toualloday_id
				$sql_tour_allocationdaily = "select * from tour_allocationdaily where tours_id = '$items_id' ";
				$sql_tour_allocationdaily .= "AND toualloday_date = '$res_pac_travel_date' ";

				$result_tour_allocationdaily = mysql_query($sql_tour_allocationdaily);
				$row_tour_allocationdaily = mysql_fetch_array($result_tour_allocationdaily);
				$toualloday_value = $row_tour_allocationdaily['toualloday_value'] * 1;
				$toualloday_id = $row_tour_allocationdaily[0];

				#Cut Allocation
				$sql_tour_allocationdaily = "update tour_allocationdaily set ";
				$sql_tour_allocationdaily .= "toualloday_value = toualloday_value - $num_of_people , ";
				$sql_tour_allocationdaily .= "last_edit_time = now() ";
				$sql_tour_allocationdaily .= " where toualloday_id = '$toualloday_id'";
				//echo "sql_tour_allocationdaily : $sql_tour_allocationdaily <br />";
				$result_tour_allocationdaily = mysql_query($sql_tour_allocationdaily);

				if ($res_pac_travel_date < $travel_date) {
					$travel_date = $res_pac_travel_date;
				};

				$item_name = get_value('tours', 'tou_id', 'tou_name', $items_id);
				$item_ref = get_value('tours', 'tou_id', 'tou_ref', $items_id);

				$message_detail = $message_detail . "<tr>
																			<td>Tour Name : $item_name</td>
																			<td>Ref : $item_ref</td>
																		 </tr>
																		 <tr><td colspan='2'>&nbsp;</td></tr>";

			} // END if($items_package_producttype_id == 3){

			if ($items_package_producttype_id == 4) {
				// Activity
				#Get Variable Allocation $actalloday_id
				$sql_activity_allocationdaily = "select * from activity_allocationdaily where activities_id = '$items_id' ";
				$sql_activity_allocationdaily .= "AND actalloday_date = '$res_pac_travel_date' ";
				$sql_activity_allocationdaily .= "AND activityratetype_id = '$items_ratetype' ";

				$result_activity_allocationdaily = mysql_query($sql_activity_allocationdaily);
				$row_activity_allocationdaily = mysql_fetch_array($result_activity_allocationdaily);
				$actalloday_value = $row_activity_allocationdaily['actalloday_value'] * 1;
				$actalloday_id = $row_activity_allocationdaily[0];

				#Cut Allocation
				$sql_activity_allocationdaily = "update activity_allocationdaily set ";
				$sql_activity_allocationdaily .= "actalloday_value = actalloday_value - $num_of_people , ";
				$sql_activity_allocationdaily .= "last_edit_time = now() ";
				$sql_activity_allocationdaily .= " where actalloday_id = '$actalloday_id'";
				//echo "sql_activity_allocationdaily : $sql_activity_allocationdaily <br />";
				$result_activity_allocationdaily = mysql_query($sql_activity_allocationdaily);

				if ($res_pac_travel_date < $travel_date) {
					$travel_date = $res_pac_travel_date;
				};

				$item_name = get_value('activities', 'act_id', 'act_name', $items_id);
				$item_ref = get_value('activities', 'act_id', 'act_ref', $items_id);

				$message_detail = $message_detail . "<tr>
																			<td>Activity Name : $item_name</td>
																			<td>Ref : $item_ref</td>
																		 </tr>
																		 <tr><td colspan='2'>&nbsp;</td></tr>";

			} // END if($items_package_producttype_id == 4){

			if ($items_package_producttype_id == 5) {
				// Bus Transfer
				#Get Variable Allocation $catalloday_id
				$sql_bustransfer_allocationdaily = "select * from cartransfer_allocationdaily where cartransfers_id = '$items_id' ";
				$sql_bustransfer_allocationdaily .= "AND catalloday_date = '$res_pac_travel_date' ";

				$result_bustransfer_allocationdaily = mysql_query($sql_bustransfer_allocationdaily);
				$row_bustransfer_allocationdaily = mysql_fetch_array($result_bustransfer_allocationdaily);
				$catalloday_value = $row_bustransfer_allocationdaily['catalloday_value'] * 1;
				$catalloday_id = $row_bustransfer_allocationdaily[0];

				#Cut Allocation
				$sql_bustransfer_allocationdaily = "update cartransfer_allocationdaily set ";
				$sql_bustransfer_allocationdaily .= "catalloday_value = catalloday_value - $num_of_people , ";
				$sql_bustransfer_allocationdaily .= "last_edit_time = now() ";
				$sql_bustransfer_allocationdaily .= " where catalloday_id = '$catalloday_id'";
				//echo "sql_bustransfer_allocationdaily : $sql_bustransfer_allocationdaily <br />";
				$result_bustransfer_allocationdaily = mysql_query($sql_bustransfer_allocationdaily);

				if ($res_pac_travel_date < $travel_date) {
					$travel_date = $res_pac_travel_date;
				};

				$item_name = get_value('cartransfers', 'ct_id', 'ct_name', $items_id);
				$item_ref = get_value('cartransfers', 'ct_id', 'ct_ref', $items_id);
				$date_detail = DateFormat($res_pac_travel_date, "s");

				$message_detail = $message_detail . "<tr>
																			<td>Bus Transfer Name : $item_name</td>
																			<td>Ref : $item_ref</td>
																		 </tr>
																		 <tr>
																			<td>Pick Up From : $res_pac_pickup_from</td>
																			<td>Drop-off to : $res_pac_pickup_to</td>
																		 </tr>
																		 <tr>
																			<td>Room / Flight No. : $res_pac_room_no</td>
																			<td>Travel Date : $date_detail</td>
																		 </tr>
																		 <tr><td colspan='2'>&nbsp;</td></tr>";

			} // END if($items_package_producttype_id == 5){

			if ($items_package_producttype_id == 6) {
				// Bus Transfer
				#Get Variable Allocation $catalloday_id
				$sql_bustransfer_allocationdaily = "select * from cartransfer_allocationdaily where cartransfers_id = '$items_id' ";
				$sql_bustransfer_allocationdaily .= "AND catalloday_date = '$res_pac_travel_date' ";

				$result_bustransfer_allocationdaily = mysql_query($sql_bustransfer_allocationdaily);
				$row_bustransfer_allocationdaily = mysql_fetch_array($result_bustransfer_allocationdaily);
				$catalloday_value = $row_bustransfer_allocationdaily['catalloday_value'] * 1;
				$catalloday_id = $row_bustransfer_allocationdaily[0];

				#Cut Allocation
				$sql_bustransfer_allocationdaily = "update cartransfer_allocationdaily set ";
				$sql_bustransfer_allocationdaily .= "catalloday_value = catalloday_value - $num_of_people , ";
				$sql_bustransfer_allocationdaily .= "last_edit_time = now() ";
				$sql_bustransfer_allocationdaily .= " where catalloday_id = '$catalloday_id'";
				//echo "sql_bustransfer_allocationdaily : $sql_bustransfer_allocationdaily <br />";
				$result_bustransfer_allocationdaily = mysql_query($sql_bustransfer_allocationdaily);

				if ($res_pac_travel_date < $travel_date) {
					$travel_date = $res_pac_travel_date;
				};

				$item_name = get_value('privatelandtransfers', 'plt_id', 'plt_name', $items_id);
				$item_ref = get_value('privatelandtransfers', 'plt_id', 'plt_ref', $items_id);
				$date_detail = DateFormat($res_pac_travel_date, "s");

				$message_detail = $message_detail . "<tr>
																			<td>Private Land Transfer Name : $item_name</td>
																			<td>Ref : $item_ref</td>
																		 </tr>
																		 <tr>
																			<td>Pick Up From : $res_pac_pickup_from</td>
																			<td>Drop-off to : $res_pac_pickup_to</td>
																		 </tr>
																		 <tr>
																			<td>Room / Flight No. : $res_pac_room_no</td>
																			<td>Travel Date : $date_detail</td>
																		 </tr>
																		 <tr><td colspan='2'>&nbsp;</td></tr>";

			} // END if($items_package_producttype_id == 6){

			if ($items_package_producttype_id == 7) {
				// Hotel

				$nights = get_value('packages', 'pac_id', 'nights', $packages_id);

				for ($i = 1; $i <= $nights; $i++) {

					$x = 1;

					if ($x < $i) {
						$date_timestem = strtotime($date_str . '+ 1 day');
						$date_str = date("Y-m-d", $date_timestem);
					} else {
						$date_str = $res_pac_travel_date;
					}

					#Get Variable Allocation $hotalloday_id
					$sql_hotel_allocationdaily = "select * from hotel_allocationdaily where hotels_id = '$items_id' ";
					$sql_hotel_allocationdaily .= "AND hotalloday_date = '$date_str' ";

					$result_hotel_allocationdaily = mysql_query($sql_hotel_allocationdaily);
					$row_hotel_allocationdaily = mysql_fetch_array($result_hotel_allocationdaily);

					$hotalloday_value = $row_hotel_allocationdaily['hotalloday_value'] * 1;
					$hotalloday_id = $row_hotel_allocationdaily[0];

					#Cut Allocation
					$sql_hotel_allocationdaily = "update hotel_allocationdaily set ";
					$sql_hotel_allocationdaily .= "hotalloday_value = hotalloday_value - $num_of_room , ";
					$sql_hotel_allocationdaily .= "last_edit_time = now() ";
					$sql_hotel_allocationdaily .= " where hotalloday_id = '$hotalloday_id'";
					//echo "sql_hotel_allocationdaily : $sql_hotel_allocationdaily <br />";
					$result_hotel_allocationdaily = mysql_query($sql_hotel_allocationdaily);

				} // END for($i=1; $i<=$nights; $i++){

				//exit();

				if ($res_pac_travel_date < $travel_date) {
					$travel_date = $res_pac_travel_date;
				};

				$item_name = get_value('hotels', 'hot_id', 'hot_name', $items_id);
				$item_ref = get_value('hotels', 'hot_id', 'hot_ref', $items_id);
				$date_detail = DateFormat($res_pac_travel_date, "s");

				$message_detail = $message_detail . "<tr>
																			<td>Hotel Name : $item_name</td>
																			<td>Ref : $item_ref</td>
																		 </tr>
																		 <tr>
																			<td>Check in Date : $date_detail</td>
																			<td>&nbsp;</td>
																		 </tr>
																		 <tr><td colspan='2'>&nbsp;</td></tr>";

			} // END if($items_package_producttype_id == 7){

			if ($items_package_producttype_id == 8) {
				// Train Transfer
				#Get Variable Allocation $traalloday_id
				$sql_traintransfer_allocationdaily = "select * from traintransfer_allocationdaily where traintransfers_id = '$items_id' ";
				$sql_traintransfer_allocationdaily .= "AND traalloday_date = '$res_pac_travel_date' ";
				$sql_traintransfer_allocationdaily .= "AND trainratetype_id = '$items_ratetype' ";

				//echo "sql_traintransfer_allocationdaily : $sql_traintransfer_allocationdaily <br />";
				//exit();

				$result_traintransfer_allocationdaily = mysql_query($sql_traintransfer_allocationdaily);
				$row_traintransfer_allocationdaily = mysql_fetch_array($result_traintransfer_allocationdaily);
				$traalloday_value = $row_traintransfer_allocationdaily['traalloday_value'] * 1;
				$traalloday_id = $row_traintransfer_allocationdaily[0];

				#Cut Allocation
				$sql_traintransfer_allocationdaily = "update traintransfer_allocationdaily set ";
				$sql_traintransfer_allocationdaily .= "traalloday_value = traalloday_value - $num_of_people , ";
				$sql_traintransfer_allocationdaily .= "last_edit_time = now() ";
				$sql_traintransfer_allocationdaily .= " where traalloday_id = '$traalloday_id'";
				//echo "sql_traintransfer_allocationdaily : $sql_traintransfer_allocationdaily <br />";
				$result_traintransfer_allocationdaily = mysql_query($sql_traintransfer_allocationdaily);

				if ($res_pac_travel_date < $travel_date) {
					$travel_date = $res_pac_travel_date;
				};

				$item_name = get_value('traintransfers', 'train_id', 'train_name', $items_id);
				$item_ref = get_value('traintransfers', 'train_id', 'train_ref', $items_id);
				$date_detail = DateFormat($res_pac_travel_date, "s");

				$item_train_number = get_value('traintransfers', 'train_id', 'train_ref', $items_id);

				$province_id = get_value('traintransfers', 'train_id', 'province_id', $items_id);
				$province_to_id = get_value('traintransfers', 'train_id', 'province_to_id', $items_id);
				$time_from_id = get_value('traintransfers', 'train_id', 'time_from_id', $items_id);
				$time_to_id = get_value('traintransfers', 'train_id', 'time_to_id', $items_id);

				$province_name = get_value('traintransfer_con_province', 'con_id', 'con_name', $province_id);
				$province_to_name = get_value('traintransfer_con_province', 'con_id', 'con_name', $province_to_id);
				$time_from = get_value('traintransfer_con_time', 'con_id', 'con_name', $time_from_id);
				$time_to = get_value('traintransfer_con_time', 'con_id', 'con_name', $time_to_id);

				$message_detail = $message_detail . "<tr>
																			<td>Train Name : $item_name</td>
																			<td>Ref : $item_ref</td>
																		 </tr>
																		 <tr>
																			<td>From : $province_name</td>
																			<td>to : $province_to_name</td>
																		 </tr>
																		 <tr>
																			<td>Train No. : $item_train_number</td>
																			<td>Time : $time_from - $time_to</td>
																		 </tr>
																		 <tr>
																			<td>Travel Date : $date_detail</td>
																			<td>&nbsp;</td>
																		 </tr>
																		 <tr><td colspan='2'>&nbsp;</td></tr>";

			} // END if($items_package_producttype_id == 8){


		}


		#Insert Table reservationpackage_item
		$rpt_id = '';
		if (!$rpt_id) {
			$sql = "insert into reservationpackage_item () values ()";
			$result = mysql_query($sql);
			$rpt_id = mysql_insert_id();
		}
		if ($rpt_id) {

			$cart_total_price = $cart_total_price + $package_prices;
			$rpt_name = get_value('packages', 'pac_id', 'pac_name', $packages_id);
			$rpt_ref = get_value('packages', 'pac_id', 'pac_ref', $packages_id);
			$rpt_ratetype = get_value('package_ratetypes', 'pacrt_id', 'pacrt_name', $res_pac_ratetype_id);
			$num_of_people = $res_pac_adult_num + $res_pac_child_num;
			$prices = number_format($package_prices, 0);


			$sql = "update reservationpackage_item set ";
			$sql .= "reservationpackages_id = '$reservationpackages_id', ";
			$sql .= "packages_id = '$packages_id', ";
			$sql .= "agentgrade_id = '$agentgrade_id', ";
			$sql .= "rpt_item_producttype_id_arr = '~" . @implode("~", $items_package_producttype_id_arr) . "~' , ";
			$sql .= "rpt_item_id_arr = '~" . @implode("~", $items_id_arr) . "~' , ";
			$sql .= "rpt_item_name_arr = '~" . @implode("~", $items_name_arr) . "~' , ";
			$sql .= "rpt_item_travel_date_arr = '~" . @implode("~", $res_pac_travel_date_arr) . "~' , ";
			$sql .= "rpt_ratetype = '" . get_value('package_ratetypes', 'pacrt_id', 'pacrt_name', $res_pac_ratetype_id) . "', ";
			$sql .= "rpt_room_num = '$res_pac_room_num', ";
			$sql .= "rpt_adult_num = '$res_pac_adult_num', ";
			$sql .= "rpt_child_num = '$res_pac_child_num', ";
			$sql .= "rpt_infant_num = '$res_pac_infant_num', ";
			$sql .= "rpt_adult_prices = '$adults_prices', ";
			$sql .= "rpt_child_prices = '$childs_prices', ";
			$sql .= "rpt_prices = '$package_prices', ";
			$sql .= "rpt_pickup_from_arr = '~" . @implode("~", $res_pac_pickup_from_arr) . "~' , ";
			$sql .= "rpt_pickup_to_arr = '~" . @implode("~", $res_pac_pickup_to_arr) . "~' , ";
			$sql .= "rpt_room_no_arr = '~" . @implode("~", $res_pac_room_no_arr) . "~' , ";

			$sql .= "rpt_name = '" . get_value('packages', 'pac_id', 'pac_name', $packages_id) . "', ";
			$sql .= "rpt_ref = '" . get_value('packages', 'pac_id', 'pac_ref', $packages_id) . "', ";


			$sql .= "last_edit_time = now() ";
			$sql .= " where rpt_id = '$rpt_id'";
			$result = mysql_query($sql);

		}


		//exit();

	}    // END if(isset($packages_id)){


	# ---- Invoice

	//if($i == 1){$add_y = 470;}else{$plus_y = $i * 15; $add_y = $plus_y + 470;}

	$travel_date_fast = DateFormat($travel_date, "s");
	$price_total_show = number_format($package_prices, 2);
	$rpt_detail = $rpt_name . " [" . $rpt_ratetype . "]";
	$newtext = wordwrap($rpt_detail, 55, "\n");

	if ($agent_id && $agentpaytype_id == 3) {

		$WaterMarkText = "$newtext";
		$axis_x = '50';
		$axis_y = '470';
		watermarkImage($SourceFile, $WaterMarkText, $DestinationFile, $axis_x, $axis_y);

		$WaterMarkText = "$travel_date_fast";
		$axis_x = '450';
		$axis_y = '470';
		watermarkImage($SourceFile, $WaterMarkText, $DestinationFile, $axis_x, $axis_y);

		$WaterMarkText = "$res_pac_room_num ROOM ($res_pac_adult_num ADL), ($res_pac_child_num CHD)";
		$axis_x = '580';
		$axis_y = '470';
		watermarkImage($SourceFile, $WaterMarkText, $DestinationFile, $axis_x, $axis_y);

		$WaterMarkText = "$price_total_show";
		$axis_x = '820';
		$axis_y = '470';
		watermarkImage($SourceFile, $WaterMarkText, $DestinationFile, $axis_x, $axis_y);

		$WaterMarkText = "$price_total_show";
		$axis_x = '820';
		$axis_y = '945';
		watermarkImage($SourceFile, $WaterMarkText, $DestinationFile, $axis_x, $axis_y);

	} // END if($agent_id && $agentpaytype_id == 3){

	//$price_total = $price_total + $boattransfer_prices_arr[$ids];

	//$i++;

	?>
	<!-- END Reservation Package Item -->


	<?php

	$text_message = $text_message . "	<tr>
										<td>Combo Product Name : $rpt_name</td>
										<td>Ref : $rpt_ref</td>
									</tr>
									<tr>
										<td>Rate Type : $rpt_ratetype</td>
										<td>Num of People : $num_of_people</td>
									</tr>
									<tr>
										<td>Price : $prices THB</td>
										<td>&nbsp;</td>
									</tr>
									<tr><td colspan='2'>&nbsp;</td></tr>";

	$text_message = $text_message . $message_detail . "<tr><td colspan='2'>&nbsp;</td></tr>";

	### Send Value Payment

	$invoice_num = "1" . setNumberLength($reservationpackages_id, 10);
	$resid_payment = $row_email[rpa_id_str];


	if ($agent_id && $agentpaytype_id == 3) {

		$text_message = $text_message . "</table>
									 <br>
									
									<table style='border:1px solid black; width:735px'>
										<tr>
											<td colspan='3' width='735' height='auto'><img src='$c_URL/photo/invoice/$in_fname' width='735' border='0'></td>
										</tr>
									 </table>
									 
									 <br>
									 
									 <strong>COMPLETION OF RESERVATION : STATUS PENDING</strong><br>

									 Thank you for booking with phuket-phiphi-krabi.com<br>
									 Your initial booking status is 'PENDING'.<br><br>

									 <strong>In order to confirm the booking,  kindly please proceed the payment to our bank account within due time as specified in attached invoice.</strong><br>
									 Once we confirm your payment settled to our bank account, you will receive confirmation email together with E-Voucher.<br><br>
									 
									 Please note that your booking may be automatically cancelled in case your payment is not settled within due time.<br><br><br>
									
									 Best Regards,<br><br>
									
									 Reservation Team<br>
									 S.S.ADV. Co., Ltd.<br>
									 23/4 Luang Poh Rd.,<br>
									 Ampur Muang, Phuket<br>
									 83000 Thailand<br>
									 Phone : +66 76 232 561, 232 562, 232 095<br>
									 Fax : + 66 76 232 096<br>
									 TAT License : 34/00549<br>";

	} elseif ($agent_id && $agentpaytype_id == 2) {

		$text_message = $text_message . "</table>

									 <br>
									 
									 <strong>COMPLETION OF RESERVATION : CONFIRMED</strong><br>

									 Thank you for booking with phuket-phiphi-krabi.com<br>
									 Your booking status is 'CONFIRMED'.<br><br>

									 <strong>In case you need to modify/cancel your booking, please send request by email to <a href=\"mailto:phiphibooking@gmail.com\">phiphibooking@gmail.com</a></strong><br><br><br>

									
									 Best Regards,<br><br>
									
									 Reservation Team<br>
									 S.S.ADV. Co., Ltd.<br>
									 23/4 Luang Poh Rd.,<br>
									 Ampur Muang, Phuket<br>
									 83000 Thailand<br>
									 Phone : +66 76 232 561, 232 562, 232 095<br>
									 Fax : + 66 76 232 096<br>
									 TAT License : 34/00549<br>";

	} elseif ($agent_id && $agentpaytype_id == 1) {

		$text_message = $text_message . "</table>
									 <br>
									 <strong>COMPLETION OF RESERVATION : STATUS PENDING</strong><br>

									 Thank you for booking with phuket-phiphi-krabi.com<br>
									 Your initial booking status is 'PENDING'.<br>
									 Once your payment is confirmed from our bank system, you will receive confirmation email together with E-Voucher.<br><br>

									 If you still have not completed your online payment, please proceed the payment at following link within 24 hours after the booking.<br><br>
									 
									 <a href='" . $c_URL . "/auto_payment.php?reservation_id=" . $row_email['rpa_id_str'] . "&type_id=2&invoice=" . $invoice_num . "&resid=" . $resid_payment . "'>
									 <strong>- Payment Click Link Here !</strong></a><br><br>
									 
									 Please note that your booking may be automatically cancelled in case your payment is not confirmed from the bank within due time.
									 <br><br><br>
									
									 Best Regards,<br><br>
									
									 Reservation Team<br>
									 S.S.ADV. Co., Ltd.<br>
									 23/4 Luang Poh Rd.,<br>
									 Ampur Muang, Phuket<br>
									 83000 Thailand<br>
									 Phone : +66 76 232 561, 232 562, 232 095<br>
									 Fax : + 66 76 232 096<br>
									 TAT License : 34/00549<br>";

	} else {

		$text_message = $text_message . "</table>
									 <br>
									 <strong>COMPLETION OF RESERVATION : STATUS PENDING</strong><br>

									 Thank you for booking with phuket-phiphi-krabi.com<br>
									 Your initial booking status is 'PENDING'.<br>
									 Once your payment is confirmed from our bank system, you will receive confirmation email together with E-Voucher.<br><br>

									 If you still have not completed your online payment, please proceed the payment at following link within 24 hours after the booking.<br><br>
									 
									 <a href='" . $c_URL . "/auto_payment.php?reservation_id=" . $row_email['rpa_id_str'] . "&type_id=2&invoice=" . $invoice_num . "&resid=" . $resid_payment . "'>
									 <strong>- Payment Click Link Here !</strong></a><br><br>
									 
									 Please note that your booking may be automatically cancelled in case your payment is not confirmed from the bank within due time.
									 <br><br><br>
									
									 Best Regards,<br><br>
									
									 Reservation Team<br>
									 S.S.ADV. Co., Ltd.<br>
									 23/4 Luang Poh Rd.,<br>
									 Ampur Muang, Phuket<br>
									 83000 Thailand<br>
									 Phone : +66 76 232 561, 232 562, 232 095<br>
									 Fax : + 66 76 232 096<br>
									 TAT License : 34/00549<br>";

	} // END if($agent_id && $agentpaytype_id == 3){

	$message = $text_message;

	//echo $message;
	//exit();
	$st = "Detail for Reservation ID : " . $row_email['rpa_id_str'];
	if ((mail($to_mail, $st, $message, $head)) == false) {
		$res_text = "SMTP Error!";
	}
	//else{ $res_text = "Send Completed"; }

	?>


	<?php
	# Clear Cart
	unset($_SESSION['cart_package']);


	# Clear verify_code
	unset($_SESSION['verify_code']);

	$cart_total_price = $cart_total_price * 100;
	//$invoice_num = time();

	if ($agentpaytype_id == 2 || $agentpaytype_id == 3) {

		echo '<script type="text/javascript">alert("+++++++++++++++++++++++++++++++++++\nThank you!\nYour booking has been sent successfully.\nOur system will send your booking details and status to your email address shortly.\n\nIn case you have not received any email response from us within 24 hours, please contact us by email at\nphiphibooking@gmail.com.\n+++++++++++++++++++++++++++++++++++")</script>';

		echo "<meta http-equiv=\"refresh\" content=\"0; url = './index.php'\" >";

	} else {
		include('inc_formpayment.php');
		exit();

	}

} else { // END if(($_SESSION['verify_code'] == $_POST['verify_code']) && (!empty($_SESSION['verify_code']))) {
	echo '<script>alert("CODE ERROR!")</script>';
	echo "<meta http-equiv=\"refresh\" content=\"0; url = './cart.php'\" >";
}
?>


<br/>




</body>
</html>
