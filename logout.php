<?php
session_start();

$agent_id = isset($_SESSION['agents']['ag_id']) ? $_SESSION['agents']['ag_id'] : 0;
$client_id = isset($_SESSION['clients']['cl_id']) ? $_SESSION['clients']['cl_id'] : 0;

if (!$agent_id && !$client_id) {
	echo "<meta http-equiv=\"refresh\" content=\"0; url = './'\" >";
	exit();
}

session_destroy();

echo "<meta http-equiv=\"refresh\" content=\"0; url = './'\" >";
?>