<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Best price Package, Hotels, Transfer, Air Tickets</title>
    <meta name="description" content="ADV Tour., Phi Phi Island Tour & Transfer operator, Hotel & Transport arrangement in the south of Thailand
"/>
    <meta name="keywords"
          content="Ko Tao Island, Samui Island, Phi Phi Island, Ferries, Boat Transfer, Tours, Package, Hotels, Diving, Liveaboard, Thailand"/>
    <meta name="Classification" content="World wide tour operate">
    <meta name="Author" content="S.S.ADV. Co., Ltd. Bangkok,Thailand">
    <link href="css/font.css" rel="stylesheet" type="text/css"/>
    <!-- menu -->
    <script type="text/javascript" src="js/script.js"></script>
    <!-- menu -->
</head>

<body>
<!-- include top login-->
<? include("_lib/_login.php"); ?>
<!-- end include top login -->

<!-- include logo & menu-->
<? include("_lib/_menu.php"); ?>
<!-- end include logo & menu -->



<!-- pic slide -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td height="400" bgcolor="#e9f1f4">&nbsp;</td>
    </tr>
    <tr>
        <td height="15" valign="top" background="images/under_pic.jpg"><img src="images/blank.gif" width="100"
                                                                            height="15" border="0"/></td>
    </tr>
</table>
<!-- pic slide -->

<!-- Detail -->
<br/>
<table width="1000" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
        <td width="250" valign="top">

            <!-- box search -->
            <table width="250" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="50" align="center" valign="top" bgcolor="#FFFFFF"><img src="images/icon_location.jpg"
                                                                                      width="50" height="50" hspace="5"
                                                                                      vspace="5" border="0"/></td>
                    <td valign="middle" bgcolor="#FFFFFF">
                        <table width="185" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="h1"><font color="#000000">SEARCH PRODUCTS</font></td>
                            </tr>
                            <tr>
                                <td>www.booking.adv-tour.com</td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <table width="250" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td height="150" valign="top" bgcolor="#FFFFFF">

                        <!--01-->
                        <table width="230" border="0" align="center" cellpadding="0" cellspacing="0">
                            <tr>
                                <td valign="middle" bgcolor="#CCCCCC"><img src="images/blank.gif" width="100" height="1"
                                                                           border="0"/></td>
                            </tr>
                            <tr>
                                <td height="30">Location</td>
                            </tr>
                            <tr>
                                <td><input type="text" name="textfield3" id="textfield3" class="boxsearch"/></td>
                            </tr>
                            <tr>
                                <td height="30">Package Tours</td>
                            </tr>
                            <tr>
                                <td><input type="text" name="textfield3" id="textfield3" class="boxsearch"/></td>
                            </tr>
                            <tr>
                                <td height="30">Car Services</td>
                            </tr>
                            <tr>
                                <td><input type="text" name="textfield3" id="textfield3" class="boxsearch"/></td>
                            </tr>
                            <tr>
                                <td height="30">Boat Services</td>
                            </tr>
                            <tr>
                                <td><input type="text" name="textfield3" id="textfield3" class="boxsearch"/></td>
                            </tr>
                            <tr>
                                <td height="30">Room Services</td>
                            </tr>
                            <tr>
                                <td><input type="text" name="textfield3" id="textfield3" class="boxsearch"/></td>
                            </tr>
                            <tr>
                                <td height="30">Availability</td>
                            </tr>
                            <tr>
                                <td><input type="text" name="textfield3" id="textfield3" class="boxsearch"/></td>
                            </tr>
                            <tr>
                                <td height="10"><img src="images/blank.gif" width="100" height="10" border="0"/></td>
                            </tr>
                            <tr>
                                <td height="30" align="center" bgcolor="#1e73a4"><a href="#" class="search">BEST SELLER
                                        HOTEL</a> <img src="images/icon_search.jpg" width="25" height="25" border="0"
                                                       align="absmiddle"/></td>
                            </tr>
                            <tr>
                                <td height="10"><img src="images/blank.gif" width="100" height="10" border="0"/></td>
                            </tr>
                        </table>

                        <!--01-->

                    </td>
                </tr>
                <tr>
                    <td height="10"><img src="images/linebox.png" width="250" height="10"/></td>
                </tr>
            </table>
            <!-- box search -->
            <br/>
            <!-- Top hotel -->
            <table width="250" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td height="40" valign="middle" bgcolor="#ffcc00" class="h1"><img src="images/icon_hotel.jpg"
                                                                                      width="40" height="40" border="0"
                                                                                      align="absmiddle"/> Top Hotel
                    </td>
                </tr>
                <tr>
                    <td height="100" bgcolor="#FFFFFF">&nbsp;</td>
                </tr>
                <tr>
                    <td height="10"><img src="images/linebox.png" width="250" height="10"/></td>
                </tr>
            </table>
            <!-- Top hotel -->
            <br/>
            <!-- Top Exchang -->
            <table width="250" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td height="40" bgcolor="#90b51f" class="h1"><img src="images/icon_exchang.jpg" width="40"
                                                                      height="40" border="0" align="absmiddle"/> Exchang
                        Rages
                    </td>
                </tr>
                <tr>
                    <td height="150" bgcolor="#FFFFFF">&nbsp;</td>
                </tr>
                <tr>
                    <td height="10"><img src="images/linebox.png" width="250" height="10"/></td>
                </tr>
            </table>
            <!-- Top Exchang -->
            <br/>
            <!-- Top Weather -->
            <table width="250" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td height="40" bgcolor="#457498" class="h1"><img src="images/icon_weather.jpg" width="40"
                                                                      height="40" border="0" align="absmiddle"/> Weather
                    </td>
                </tr>
                <tr>
                    <td height="150" bgcolor="#FFFFFF">&nbsp;</td>
                </tr>
                <tr>
                    <td height="10"><img src="images/linebox.png" width="250" height="10"/></td>
                </tr>
            </table>
            <!-- Top Weather -->
            <br/>

        </td>
        <td width="20">&nbsp;</td>
        <td valign="top">&nbsp;</td>
    </tr>
</table>
<br/>
<!-- Detail -->

<!-- include footer->
<? include("_lib/_footer.php"); ?>
<!-- end include footer -->


</body>
</html>
