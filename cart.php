<?php include("./administrator/lib/connect.php"); ?>
<?php include("./administrator/lib/function.php"); ?>
<?php include("./administrator/lib/constant.php"); ?>
<?php ob_start(); ?>
<?php session_start();
error_reporting(E_ALL & ~E_NOTICE); ?>
<?php

if (isset($_SESSION['clients']['cl_id'])) {
    $client_id = $_SESSION['clients']['cl_id'];

} else {
    $client_id = null;
}
$num_of_items = 0;
$sum_total_price = 0;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Best price Package, Hotels, Transfer, Air Tickets</title>
    <meta name="description" content="ADV Tour., Phi Phi Island Tour & Transfer operator, Hotel & Transport arrangement in the south of Thailand
"/>
    <meta name="keywords"
          content="Ko Tao Island, Samui Island, Phi Phi Island, Ferries, Boat Transfer, Tours, Package, Hotels, Diving, Liveaboard, Thailand"/>
    <meta name="Classification" content="World wide tour operate">
    <meta name="Author" content="S.S.ADV. Co., Ltd. Bangkok,Thailand">

    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="css/font.css" rel="stylesheet" type="text/css"/>

    <!-- menu slide -->
    <script type="text/javascript" src="js/script.js"></script>
    <!-- menu slide -->

    <!-- js-vallenato -->
    <script type="text/javascript" src="js-vtip/jquery-1.3.2.min.js"></script>
    <script type="text/javascript" src="js-vtip/jquery-plugin-vtip.js"></script>
    <script src="js-vallenato/vallenato.js" type="text/javascript" charset="utf-8"></script>
    <link rel="stylesheet" href="./js-vallenato/vallenato.css" type="text/css" media="screen" charset="utf-8">
    <!-- js-vallenato -->
</head>

<body>
<!-- include top login-->
<?php include("inc_login.php"); ?>
<!-- end include top login -->

<!-- include logo & menu-->
<?php include("inc_menu.php"); ?>
<!-- end include logo & menu -->


<table width="1000" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
        <td valign="top" class="h1"><img src="images/blank.gif" width="100" height="10" border="0"/></td>
    </tr>
    <tr>
        <td height="40" bgcolor="#FFFFFF" class="h2"><img src="images/blank.gif" width="10" height="40" border="0"
                                                          align="absmiddle">Shopping Cart Infomation
        </td>
    </tr>
</table>
<table width="1000" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
        <td width="500" align="left" valign="top"><span class="h1"><img src="images/pic_under_line.jpg" width="200"
                                                                        height="15" border="0"/></span></td>
        <td align="right"><span class="h1"><img src="images/pic_under_line1.jpg" width="200" height="15"
                                                border="0"/></span></td>
    </tr>
</table>

<table width="1000" align="center" border="0" cellspacing="0" cellpadding="2" bgcolor="#e9b120">
    <tr bgcolor="#cccccc">
        <td width="50" align="center"><strong>Delete</strong></td>
        <td width="803"><strong>Info.</strong></td>
        <td width="125" align="right" class="list-right"><strong>Price &nbsp;</strong></td>
    </tr>
</table>


<!-- Start Get Boat Transfer Variable -->


<?php
# Get Variable Cart from SESSION
if (isset($_SESSION['cart_boat']['boattransfers_id_arr'])) {
    $boattransfers_id_arr = $_SESSION['cart_boat']['boattransfers_id_arr'];
    $agentgrade_id_arr = $_SESSION['cart_boat']['agentgrade_id_arr'];
    $res_bot_travel_date_arr = $_SESSION['cart_boat']['res_bot_travel_date_arr'];
    $res_bot_ratetype_id_arr = $_SESSION['cart_boat']['res_bot_ratetype_id_arr'];
    $res_bot_adult_num_arr = $_SESSION['cart_boat']['res_bot_adult_num_arr'];
    $res_bot_child_num_arr = $_SESSION['cart_boat']['res_bot_child_num_arr'];
    $res_bot_infant_num_arr = $_SESSION['cart_boat']['res_bot_infant_num_arr'];
    $res_bot_ticket_arr = $_SESSION['cart_boat']['res_bot_ticket_arr'];
    $adults_prices_arr = $_SESSION['cart_boat']['adults_prices_arr'];
    $childs_prices_arr = $_SESSION['cart_boat']['childs_prices_arr'];
    $boattransfer_prices_arr = $_SESSION['cart_boat']['boattransfer_prices_arr'];
    $boaalloday_id_arr = $_SESSION['cart_boat']['boaalloday_id_arr'];
    $num_of_people_arr = $_SESSION['cart_boat']['num_of_people_arr'];
}
$i = 0;
if (isset($boattransfers_id_arr)) {
    ?>
    <table width="1000" align="center" border="0" cellspacing="0" cellpadding="0">
        <tr height="0">
            <td width="50" align="center"></td>
            <td width="803"></td>
            <td width="125"></td>
        </tr>
        <?php
        foreach ($boattransfers_id_arr as $ids => $value) {
            $i++;
            $num_of_items++;
            $sum_total_price = $sum_total_price + $boattransfer_prices_arr[$ids] * 1;


            ?>
            <tr bgcolor="#EEEEEE">
                <td align="center">
                    <a href="delete_boattransfer_cart.php?delete_index=<?= $ids ?>"
                       onClick="return(confirm('Are you sure to delete this item ?'))">
                        <img src="images/delete_16.png"/>
                    </a></td>
                <td>
                    <b>Type</b> : Boat Transfer <br/>
                    <b>Name</b> : <?= get_value('boattransfers', 'bot_id', 'bot_name', $value) ?><br/>
                    <b>Ref</b> : <?= get_value('boattransfers', 'bot_id', 'bot_ref', $value) ?> <br/>
                    <b>Travel Date</b> : <?= DateFormat($res_bot_travel_date_arr[$ids], "s") ?> <br/>
                    <b>Rate Type</b>
                    : <?= get_value('boattransfer_ratetypes', 'botrt_id', 'botrt_name', $res_bot_ratetype_id_arr[$ids]) ?>
                    <br/>
                    <?= number_format($res_bot_adult_num_arr[$ids], 0) ?> Adult
                    + <?= number_format($res_bot_child_num_arr[$ids], 0) ?> Child
                    + <?= number_format($res_bot_infant_num_arr[$ids], 0) ?> Infant
                </td>
                <td align="right"><?= number_format($boattransfer_prices_arr[$ids], 2) ?> THB&nbsp;</td>
            </tr>
            <tr bgcolor="#e9b120">
                <td colspan="20" height="1"></td>
            </tr>


            <?php
        }// END foreach($boattransfers_id_arr as $ids => $value){

        ?>


    </table>
    <?php
}// END if(isset($boattransfers_id_arr)){
?>

<?php
# Get Variable Cart from SESSION
if (isset($_SESSION['cart_pickup']['pickuptransfers_id_arr'])) {
    $pickuptransfers_id_arr = $_SESSION['cart_pickup']['pickuptransfers_id_arr'];
    $agentgrade_id_arr = $_SESSION['cart_pickup']['agentgrade_id_arr'];
    $pickuptransfer_type_id_for_check_arr = $_SESSION['cart_pickup']['pickuptransfer_type_id_for_check_arr'];
    $res_put_travel_date_arr = $_SESSION['cart_pickup']['res_put_travel_date_arr'];
    $res_put_ratetype_id_arr = $_SESSION['cart_pickup']['res_put_ratetype_id_arr'];
    $res_put_car_num_arr = $_SESSION['cart_pickup']['res_put_car_num_arr'];
    $res_put_adult_num_arr = $_SESSION['cart_pickup']['res_put_adult_num_arr'];
    $res_put_child_num_arr = $_SESSION['cart_pickup']['res_put_child_num_arr'];
    $res_put_infant_num_arr = $_SESSION['cart_pickup']['res_put_infant_num_arr'];
    $adults_prices_arr = $_SESSION['cart_pickup']['adults_prices_arr'];
    $childs_prices_arr = $_SESSION['cart_pickup']['childs_prices_arr'];
    $pickuptransfer_prices_arr = $_SESSION['cart_pickup']['pickuptransfer_prices_arr'];
    $rpt_pickupfrom_arr = $_SESSION['cart_pickup']['rpt_pickupfrom_arr'];
    $rpt_pickupto_arr = $_SESSION['cart_pickup']['rpt_pickupto_arr'];
    $rpt_room_arr = $_SESSION['cart_pickup']['rpt_room_arr'];
}
$i = 0;
if (isset($pickuptransfers_id_arr)) {
    ?>
    <table width="1000" align="center" border="0" cellspacing="0" cellpadding="0" bgcolor="#e9b120">
        <tr bgcolor="#e9b120" height="0">
            <td width="50" align="center"></td>
            <td width="803"></td>
            <td width="125"></td>
        </tr>
        <?php
        foreach ($pickuptransfers_id_arr as $ids => $value) {
            $i++;
            $num_of_items++;
            $sum_total_price = $sum_total_price + $pickuptransfer_prices_arr[$ids] * 1;


            ?>
            <tr bgcolor="#EEEEEE">
                <td align="center">
                    <a href="delete_pickuptransfer_cart.php?delete_index=<?= $ids ?>"
                       onClick="return(confirm('Are you sure to delete this item ?'))">
                        <img src="images/delete_16.png"/>
                    </a></td>
                <td>
                    <b>Type</b> : Pick Up Transfer <br/>
                    <b>Name</b> : <?= get_value('pickuptransfers', 'put_id', 'put_name', $value) ?><br/>
                    <b>Ref</b> : <?= get_value('pickuptransfers', 'put_id', 'put_ref', $value) ?> <br/>
                    <b>Travel Date</b> : <?= DateFormat($res_put_travel_date_arr[$ids], "s") ?> <br/>
                    <b>Rate Type</b>
                    : <?= get_value('pickuptransfer_ratetypes', 'putrt_id', 'putrt_name', $res_put_ratetype_id_arr[$ids]) ?>
                    <br/>
                    <?php
                    if ($pickuptransfer_type_id_for_check_arr[$ids] == 1) {
                        #For Private
                        echo "<b>Car</b> : " . number_format($res_put_car_num_arr[$ids], 0) . " <br />";
                    }
                    ?>
                    <?= number_format($res_put_adult_num_arr[$ids], 0) ?> Adult
                    + <?= number_format($res_put_child_num_arr[$ids], 0) ?> Child
                    + <?= number_format($res_put_infant_num_arr[$ids], 0) ?> Infant
                    <br/>
                    <b>Pick Up From</b> : <?= $rpt_pickupfrom_arr[$ids] ?> <br/>
                    <b>Pick Up To</b> : <?= $rpt_pickupto_arr[$ids] ?> <br/>
                    <b>Room No. </b> : <?= $rpt_room_arr[$ids] ?> <br/>
                </td>
                <td align="right"><?= number_format($pickuptransfer_prices_arr[$ids], 2) ?> THB &nbsp;</td>
            </tr>
            <tr bgcolor="#e9b120">
                <td colspan="20" height="1"></td>
            </tr>


            <?php
        }// END foreach($pickuptransfers_id_arr as $ids => $value){

        ?>


    </table>
    <?php
}// END if(isset($pickuptransfers_id_arr)){
?>


<!-- End Get Pick Up Transfer Variable -->


<!-- Start Get Tour Variable -->


<?php
# Get Variable Cart from SESSION
if (isset($_SESSION['cart_tour']['tours_id_arr'])) {
    $tours_id_arr = $_SESSION['cart_tour']['tours_id_arr'];
    $agentgrade_id_arr = $_SESSION['cart_tour']['agentgrade_id_arr'];
    $res_tou_travel_date_arr = $_SESSION['cart_tour']['res_tou_travel_date_arr'];
    $res_tou_ratetype_id_arr = $_SESSION['cart_tour']['res_tou_ratetype_id_arr'];
    $res_tou_adult_num_arr = $_SESSION['cart_tour']['res_tou_adult_num_arr'];
    $res_tou_child_num_arr = $_SESSION['cart_tour']['res_tou_child_num_arr'];
    $res_tou_infant_num_arr = $_SESSION['cart_tour']['res_tou_infant_num_arr'];
    $adults_prices_arr = $_SESSION['cart_tour']['adults_prices_arr'];
    $childs_prices_arr = $_SESSION['cart_tour']['childs_prices_arr'];
    $tour_prices_arr = $_SESSION['cart_tour']['tour_prices_arr'];
    $toualloday_id_arr = $_SESSION['cart_tour']['toualloday_id_arr'];
    $num_of_people_arr = $_SESSION['cart_tour']['num_of_people_arr'];
}
$i = 0;
if (isset($tours_id_arr)) {
    ?>
    <table width="1000" align="center" border="0" cellspacing="0" cellpadding="0" bgcolor="#e9b120">
        <tr bgcolor="#e9b120" height="0">
            <td width="50" align="center"></td>
            <td width="803"></td>
            <td width="125"></td>
        </tr>
        <?php
        foreach ($tours_id_arr as $ids => $value) {
            $i++;
            $num_of_items++;
            $sum_total_price = $sum_total_price + $tour_prices_arr[$ids] * 1;


            ?>
            <tr bgcolor="#EEEEEE">
                <td align="center">
                    <a href="delete_tour_cart.php?delete_index=<?= $ids ?>"
                       onClick="return(confirm('Are you sure to delete this item ?'))">
                        <img src="images/delete_16.png"/>
                    </a></td>
                <td>
                    <b>Type</b> : Tour <br/>
                    <b>Name</b> : <?= get_value('tours', 'tou_id', 'tou_name', $value) ?><br/>
                    <b>Ref</b> : <?= get_value('tours', 'tou_id', 'tou_ref', $value) ?> <br/>
                    <b>Travel Date</b> : <?= DateFormat($res_tou_travel_date_arr[$ids], "s") ?> <br/>
                    <b>Rate Type</b>
                    : <?= get_value('tour_ratetypes', 'tourt_id', 'tourt_name', $res_tou_ratetype_id_arr[$ids]) ?> <br/>
                    <?= number_format($res_tou_adult_num_arr[$ids], 0) ?> Adult
                    + <?= number_format($res_tou_child_num_arr[$ids], 0) ?> Child
                    + <?= number_format($res_tou_infant_num_arr[$ids], 0) ?> Infant
                </td>
                <td align="right"><?= number_format($tour_prices_arr[$ids], 2) ?> THB &nbsp;</td>
            </tr>
            <tr bgcolor="#e9b120">
                <td colspan="20" height="1"></td>
            </tr>


            <?php
        }// END foreach($tours_id_arr as $ids => $value){

        ?>


    </table>
    <?php
}// END if(isset($tours_id_arr)){
?>

<?php
# Get Variable Cart from SESSION
if (isset($_SESSION['cart_activity']['activities_id_arr'])) {
    $activities_id_arr = $_SESSION['cart_activity']['activities_id_arr'];
    $agentgrade_id_arr = $_SESSION['cart_activity']['agentgrade_id_arr'];
    $res_act_travel_date_arr = $_SESSION['cart_activity']['res_act_travel_date_arr'];
    $res_act_ratetype_id_arr = $_SESSION['cart_activity']['res_act_ratetype_id_arr'];
    $res_act_adult_num_arr = $_SESSION['cart_activity']['res_act_adult_num_arr'];
    $res_act_child_num_arr = $_SESSION['cart_activity']['res_act_child_num_arr'];
    $res_act_infant_num_arr = $_SESSION['cart_activity']['res_act_infant_num_arr'];
    $adults_prices_arr = $_SESSION['cart_activity']['adults_prices_arr'];
    $childs_prices_arr = $_SESSION['cart_activity']['childs_prices_arr'];
    $activity_prices_arr = $_SESSION['cart_activity']['activity_prices_arr'];
    $actalloday_id_arr = $_SESSION['cart_activity']['actalloday_id_arr'];
    $num_of_people_arr = $_SESSION['cart_activity']['num_of_people_arr'];
}
$i = 0;
if (isset($activities_id_arr)) {
    ?>
    <table width="1000" align="center" border="0" cellspacing="0" cellpadding="0" bgcolor="#e9b120">
        <tr bgcolor="#e9b120" height="0">
            <td width="50" align="center"></td>
            <td width="803"></td>
            <td width="125"></td>
        </tr>
        <?php
        foreach ($activities_id_arr as $ids => $value) {
            $i++;
            $num_of_items++;
            $sum_total_price = $sum_total_price + $activity_prices_arr[$ids] * 1;

            ?>
            <tr bgcolor="#EEEEEE">
                <td align="center">
                    <a href="delete_activity_cart.php?delete_index=<?= $ids ?>"
                       onClick="return(confirm('Are you sure to delete this item ?'))">
                        <img src="images/delete_16.png"/>
                    </a></td>
                <td>
                    <b>Type</b> : Activity <br/>
                    <b>Name</b> : <?= get_value('activities', 'act_id', 'act_name', $value) ?><br/>
                    <b>Ref</b> : <?= get_value('activities', 'act_id', 'act_ref', $value) ?> <br/>
                    <b>Travel Date</b> : <?= DateFormat($res_act_travel_date_arr[$ids], "s") ?> <br/>
                    <b>Rate Type</b>
                    : <?= get_value('activity_ratetypes', 'actrt_id', 'actrt_name', $res_act_ratetype_id_arr[$ids]) ?>
                    <br/>
                    <?= number_format($res_act_adult_num_arr[$ids], 0) ?> Adult
                    + <?= number_format($res_act_child_num_arr[$ids], 0) ?> Child
                    + <?= number_format($res_act_infant_num_arr[$ids], 0) ?> Infant
                </td>
                <td align="right"><?= number_format($activity_prices_arr[$ids], 2) ?> THB &nbsp;</td>
            </tr>
            <tr bgcolor="#e9b120">
                <td colspan="20" height="1"></td>
            </tr>


            <?php
        }// END foreach($activities_id_arr as $ids => $value){

        ?>


    </table>
    <?php
}// END if(isset($activities_id_arr)){
?>


<?php
# Get Variable Cart from SESSION
if (isset($_SESSION['cart_bus']['bustransfer_id_arr'])) {
    $bustransfer_id_arr = $_SESSION['cart_bus']['bustransfer_id_arr'];
    $agentgrade_id_arr = $_SESSION['cart_bus']['agentgrade_id_arr'];
    $res_ct_travel_date_arr = $_SESSION['cart_bus']['res_ct_travel_date_arr'];
    $res_ct_ratetype_id_arr = $_SESSION['cart_bus']['res_ct_ratetype_id_arr'];
    $res_ct_adult_num_arr = $_SESSION['cart_bus']['res_ct_adult_num_arr'];
    $res_ct_child_num_arr = $_SESSION['cart_bus']['res_ct_child_num_arr'];
    $res_ct_infant_num_arr = $_SESSION['cart_bus']['res_ct_infant_num_arr'];
    $adults_prices_arr = $_SESSION['cart_bus']['adults_prices_arr'];
    $childs_prices_arr = $_SESSION['cart_bus']['childs_prices_arr'];
    $bustransfer_prices_arr = $_SESSION['cart_bus']['bustransfer_prices_arr'];
    $catalloday_id_arr = $_SESSION['cart_bus']['catalloday_id_arr'];
    $num_of_people_arr = $_SESSION['cart_bus']['num_of_people_arr'];
    $rct_pickupfrom_arr = $_SESSION['cart_bus']['rct_pickupfrom_arr'];
    $rct_pickupto_arr = $_SESSION['cart_bus']['rct_pickupto_arr'];
    $rct_room_arr = $_SESSION['cart_bus']['rct_room_arr'];
}
$i = 0;
if (isset($bustransfer_id_arr)) {
    ?>
    <table width="1000" align="center" border="0" cellspacing="0" cellpadding="0" bgcolor="#e9b120">
        <tr bgcolor="#e9b120" height="0">
            <td width="50" align="center"></td>
            <td width="803"></td>
            <td width="125"></td>
        </tr>
        <?php
        foreach ($bustransfer_id_arr as $ids => $value) {
            $i++;
            $num_of_items++;
            $sum_total_price = $sum_total_price + $bustransfer_prices_arr[$ids] * 1;


            ?>
            <tr bgcolor="#EEEEEE">
                <td align="center">
                    <a href="delete_bustransfer_cart.php?delete_index=<?= $ids ?>"
                       onClick="return(confirm('Are you sure to delete this item ?'))">
                        <img src="images/delete_16.png"/>
                    </a></td>
                <td>
                    <b>Type</b> : Bus Transfer <br/>
                    <b>Name</b> : <?= get_value('cartransfers', 'ct_id', 'ct_name', $value) ?><br/>
                    <b>Ref</b> : <?= get_value('cartransfers', 'ct_id', 'ct_ref', $value) ?> <br/>
                    <b>Travel Date</b> : <?= DateFormat($res_ct_travel_date_arr[$ids], "s") ?> <br/>
                    <b>Rate Type</b>
                    : <?= get_value('cartransfer_ratetypes', 'catrt_id', 'catrt_name', $res_ct_ratetype_id_arr[$ids]) ?>
                    <br/>
                    <?= number_format($res_ct_adult_num_arr[$ids], 0) ?> Adult
                    + <?= number_format($res_ct_child_num_arr[$ids], 0) ?> Child
                    + <?= number_format($res_ct_infant_num_arr[$ids], 0) ?> Infant
                    <br/>
                    <!--<b>Pick Up From</b> : <?= $rct_pickupfrom_arr[$ids] ?> <br />
          <b>Pick Up To</b> : <?= $rct_pickupto_arr[$ids] ?> <br />
          <b>Room No. </b> : <?= $rct_room_arr[$ids] ?> <br />-->
                </td>
                <td align="right"><?= number_format($bustransfer_prices_arr[$ids], 2) ?> THB &nbsp;</td>
            </tr>
            <tr bgcolor="#e9b120">
                <td colspan="20" height="1"></td>
            </tr>


            <?php
        }// END foreach($bustransfer_id_arr as $ids => $value){

        ?>


    </table>
    <?php
}// END if(isset($bustransfer_id_arr)){
?>

<?php
# Get Variable Cart from SESSION
if (isset($_SESSION['cart_private']['privatelandtransfers_id_arr'])) {
    $privatelandtransfers_id_arr = $_SESSION['cart_private']['privatelandtransfers_id_arr'];
    $agentgrade_id_arr = $_SESSION['cart_private']['agentgrade_id_arr'];
    $privatelandtransfer_type_id_arr = $_SESSION['cart_pickup']['privatelandtransfer_type_id_arr'];
    $res_plt_travel_date_arr = $_SESSION['cart_private']['res_plt_travel_date_arr'];
    $res_plt_ratetype_id_arr = $_SESSION['cart_private']['res_plt_ratetype_id_arr'];
    $res_plt_car_num_arr = $_SESSION['cart_private']['res_plt_car_num_arr'];
    $res_plt_adult_num_arr = $_SESSION['cart_private']['res_plt_adult_num_arr'];
    $res_plt_child_num_arr = $_SESSION['cart_private']['res_plt_child_num_arr'];
    $res_plt_infant_num_arr = $_SESSION['cart_private']['res_plt_infant_num_arr'];
    $privatelandtransfer_prices_arr = $_SESSION['cart_private']['privatelandtransfer_prices_arr'];
    $rplt_pickupfrom_arr = $_SESSION['cart_private']['rplt_pickupfrom_arr'];
    $rplt_pickupto_arr = $_SESSION['cart_private']['rplt_pickupto_arr'];
    $rplt_room_arr = $_SESSION['cart_private']['rplt_room_arr'];
}
$i = 0;
if (isset($privatelandtransfers_id_arr)) {
    ?>
    <table width="1000" align="center" border="0" cellspacing="0" cellpadding="0" bgcolor="#e9b120">
        <tr bgcolor="#e9b120" height="0">
            <td width="50" align="center"></td>
            <td width="803"></td>
            <td width="125"></td>
        </tr>
        <?php
        foreach ($privatelandtransfers_id_arr as $ids => $value) {
            $i++;
            $num_of_items++;
            $sum_total_price = $sum_total_price + $privatelandtransfer_prices_arr[$ids] * 1;


            ?>
            <tr bgcolor="#EEEEEE">
                <td align="center">
                    <a href="delete_privatelandtransfer_cart.php?delete_index=<?= $ids ?>"
                       onClick="return(confirm('Are you sure to delete this item ?'))">
                        <img src="images/delete_16.png"/>
                    </a></td>
                <td>
                    <b>Type</b> : Private Land Transfer <br/>
                    <b>Name</b> : <?= get_value('privatelandtransfers', 'plt_id', 'plt_name', $value) ?><br/>
                    <b>Ref</b> : <?= get_value('privatelandtransfers', 'plt_id', 'plt_ref', $value) ?> <br/>
                    <b>Travel Date</b> : <?= DateFormat($res_plt_travel_date_arr[$ids], "s") ?> <br/>
                    <b>Rate Type</b>
                    : <?= get_value('privatelandtransfer_ratetypes', 'pltrt_id', 'pltrt_name', $res_plt_ratetype_id_arr[$ids]) ?>
                    <br/>
                    <?php
                    #For Private
                    echo "<b>Car</b> : " . number_format($res_plt_car_num_arr[$ids], 0) . " <br />";
                    ?>
                    <?= number_format($res_plt_adult_num_arr[$ids], 0) ?> Adult
                    + <?= number_format($res_plt_child_num_arr[$ids], 0) ?> Child
                    + <?= number_format($res_plt_infant_num_arr[$ids], 0) ?> Infant
                    <br/>
                    <b>Pick Up From</b> : <?= $rplt_pickupfrom_arr[$ids] ?> <br/>
                    <b>Pick Up To</b> : <?= $rplt_pickupto_arr[$ids] ?> <br/>
                    <b>Room No. </b> : <?= $rplt_room_arr[$ids] ?> <br/>
                </td>
                <td align="right"><?= number_format($privatelandtransfer_prices_arr[$ids], 2) ?> THB &nbsp;</td>
            </tr>
            <tr bgcolor="#e9b120">
                <td colspan="20" height="1"></td>
            </tr>


            <?php
        }// END foreach($privatelandtransfers_id_arr as $ids => $value){

        ?>


    </table>
    <?php
}// END if(isset($privatelandtransfers_id_arr)){
?>


<!-- End Get Private Land Transfer Variable -->


<!-- Start Get Hotel Variable -->

<?php
# Get Variable Cart from SESSION
if (isset($_SESSION['cart_hotel']['hotels_id_arr'])) {
    $hotels_id_arr = $_SESSION['cart_hotel']['hotels_id_arr'];
    $agentgrade_id_arr = $_SESSION['cart_hotel']['agentgrade_id_arr'];
    $res_hot_check_in_arr = $_SESSION['cart_hotel']['res_hot_check_in_arr'];
    $res_hot_check_out_arr = $_SESSION['cart_hotel']['res_hot_check_out_arr'];
    $res_hot_ratetype_id_arr = $_SESSION['cart_hotel']['res_hot_ratetype_id_arr'];
    $res_hot_adult_num_arr = $_SESSION['cart_hotel']['res_hot_adult_num_arr'];
    $res_hot_child_num_arr = $_SESSION['cart_hotel']['res_hot_child_num_arr'];
    $res_hot_infant_num_arr = $_SESSION['cart_hotel']['res_hot_infant_num_arr'];
    $hotel_prices_arr = $_SESSION['cart_hotel']['hotel_prices_arr'];
    $hotalloday_id_arr = $_SESSION['cart_hotel']['hotalloday_id_arr'];
    $num_of_room_arr = $_SESSION['cart_hotel']['num_of_room_arr'];
}
$i = 0;
if (isset($hotels_id_arr)) {
    ?>
    <table width="1000" align="center" border="0" cellspacing="0" cellpadding="0" bgcolor="#e9b120">
        <tr bgcolor="#e9b120" height="0">
            <td width="50" align="center"></td>
            <td width="803"></td>
            <td width="125"></td>
        </tr>
        <?php
        foreach ($hotels_id_arr as $ids => $value) {
            $i++;
            $num_of_items++;
            $sum_total_price = $sum_total_price + $hotel_prices_arr[$ids] * 1;


            $room_type_id = get_value('hotels', 'hot_id', 'room_type_id', $value);
            $room_type_name = get_value('hotel_con_room_type', 'con_id', 'con_name', $room_type_id);


            ?>
            <tr bgcolor="#EEEEEE">
                <td align="center">
                    <a href="delete_hotel_cart.php?delete_index=<?= $ids ?>"
                       onClick="return(confirm('Are you sure to delete this item ?'))">
                        <img src="images/delete_16.png"/>
                    </a></td>
                <td>
                    <b>Type</b> : Hotel <br/>
                    <b>Name</b> : <?= get_value('hotels', 'hot_id', 'hot_name', $value) ?><br/>
                    <b>Ref</b> : <?= get_value('hotels', 'hot_id', 'hot_ref', $value) ?> <br/>
                    <b>Check In</b> : <?= DateFormat($res_hot_check_in_arr[$ids], "s") ?> <br/>
                    <b>Check Out</b> : <?= DateFormat($res_hot_check_out_arr[$ids], "s") ?> <br/>
                    <b>Rate Type</b>
                    : <?= get_value('hotel_ratetypes', 'hotrt_id', 'hotrt_name', $res_hot_ratetype_id_arr[$ids]) ?>
                    <br/>
                    <b>Room Type</b> : <?= $room_type_name ?> <br/>
                    <b>Rooms</b> : <?= $num_of_room_arr[$ids] ?> <br/>
                    <?= number_format($res_hot_adult_num_arr[$ids], 0) ?> Adult
                    + <?= number_format($res_hot_child_num_arr[$ids], 0) ?> Child
                    + <?= number_format($res_hot_infant_num_arr[$ids], 0) ?> Infant
                </td>
                <td align="right"><?= number_format($hotel_prices_arr[$ids], 2) ?> THB &nbsp;</td>
            </tr>
            <tr bgcolor="#e9b120">
                <td colspan="20" height="1"></td>
            </tr>


            <?php
        }// END foreach($hotels_id_arr as $ids => $value){

        ?>


    </table>
    <?php
}// END if(isset($hotels_id_arr)){
?>


<!-- End Get Hotel Variable -->


<!-- Start Get Train Transfer Variable -->


<?php
# Get Variable Cart from SESSION
if (isset($_SESSION['cart_train']['traintransfers_id_arr'])) {
    $traintransfers_id_arr = $_SESSION['cart_train']['traintransfers_id_arr'];
    $agentgrade_id_arr = $_SESSION['cart_train']['agentgrade_id_arr'];
    $res_tra_travel_date_arr = $_SESSION['cart_train']['res_tra_travel_date_arr'];
    $res_tra_ratetype_id_arr = $_SESSION['cart_train']['res_tra_ratetype_id_arr'];
    $res_tra_adult_num_arr = $_SESSION['cart_train']['res_tra_adult_num_arr'];
    $res_tra_child_num_arr = $_SESSION['cart_train']['res_tra_child_num_arr'];
    $res_tra_infant_num_arr = $_SESSION['cart_train']['res_tra_infant_num_arr'];
    $adults_prices_arr = $_SESSION['cart_train']['adults_prices_arr'];
    $childs_prices_arr = $_SESSION['cart_train']['childs_prices_arr'];
    $traintransfer_prices_arr = $_SESSION['cart_train']['traintransfer_prices_arr'];
    $traalloday_id_arr = $_SESSION['cart_train']['traalloday_id_arr'];
    $num_of_people_arr = $_SESSION['cart_train']['num_of_people_arr'];
}
$i = 0;
if (isset($traintransfers_id_arr)) {
    ?>
    <table width="1000" align="center" border="0" cellspacing="0" cellpadding="0" bgcolor="#e9b120">
        <tr bgcolor="#e9b120" height="0">
            <td width="50" align="center"></td>
            <td width="803"></td>
            <td width="125"></td>
        </tr>
        <?php
        foreach ($traintransfers_id_arr as $ids => $value) {
            $i++;
            $num_of_items++;
            $sum_total_price = $sum_total_price + $traintransfer_prices_arr[$ids] * 1;


            ?>
            <tr bgcolor="#EEEEEE">
                <td align="center">
                    <a href="delete_traintransfer_cart.php?delete_index=<?= $ids ?>"
                       onClick="return(confirm('Are you sure to delete this item ?'))">
                        <img src="images/delete_16.png"/>
                    </a></td>
                <td>
                    <b>Type</b> : Train Transfer <br/>
                    <b>Name</b> : <?= get_value('traintransfers', 'train_id', 'train_name', $value) ?><br/>
                    <b>Ref</b> : <?= get_value('traintransfers', 'train_id', 'train_ref', $value) ?> <br/>
                    <b>Travel Date</b> : <?= DateFormat($res_tra_travel_date_arr[$ids], "s") ?> <br/>
                    <b>Rate Type</b>
                    : <?= get_value('traintransfer_ratetypes', 'trart_id', 'trart_name', $res_tra_ratetype_id_arr[$ids]) ?>
                    <br/>
                    <?= number_format($res_tra_adult_num_arr[$ids], 0) ?> Adult
                    + <?= number_format($res_tra_child_num_arr[$ids], 0) ?> Child
                    + <?= number_format($res_tra_infant_num_arr[$ids], 0) ?> Infant
                </td>
                <td align="right"><?= number_format($traintransfer_prices_arr[$ids], 2) ?> THB &nbsp;</td>
            </tr>
            <tr bgcolor="#e9b120">
                <td colspan="20" height="1"></td>
            </tr>


            <?php
        }// END foreach($traintransfers_id_arr as $ids => $value){

        ?>


    </table>
    <?php
}// END if(isset($traintransfers_id_arr)){
?>


<!-- End Get Train Transfer Variable -->


<?php
//echo "num_of_items : $num_of_items";
if ($num_of_items > 0) {
    ?>
    <table width="1000" align="center" border="0" cellspacing="0" cellpadding="2" bgcolor="#e9b120">
        <tr bgcolor="#eeeeee">
            <td colspan="20" align="right"><strong>Total : THB <?= number_format($sum_total_price, 2) ?>&nbsp; </strong>
            </td>
        </tr>
        <tr bgcolor="#e9b120">
            <td colspan="20" height="1"></td>
        </tr>
        <tr bgcolor="#e9b120">
            <td colspan="20" height="1"></td>
        </tr>
    </table>


    <form action="clear_cart.php" method="POST">
        <input type="hidden" name="clear_cart" value="1"/>
        <p align="center">
            <input class="btn btn-default" type="submit" name="clearorder" value="Clear Shopping Cart"
                   onClick="return(confirm('Are you sure to Clear Shopping Cart ?'))" style="width:150;">
        </p>
    </form>


    <table width="1000" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
            <td valign="top" class="h1"><img src="images/blank.gif" width="100" height="10" border="0"/></td>
        </tr>
        <tr>
            <td height="40" bgcolor="#FFFFFF" class="h2"><img src="images/blank.gif" width="10" height="40" border="0"
                                                              align="absmiddle">Reservation Form
            </td>
        </tr>
    </table>
    <table width="1000" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
            <td width="500" align="left" valign="top"><span class="h1"><img src="images/pic_under_line.jpg" width="200"
                                                                            height="15" border="0"/></span></td>
            <td align="right"><span class="h1"><img src="images/pic_under_line1.jpg" width="200" height="15"
                                                    border="0"/></span></td>
        </tr>
    </table>

    <script Language="JavaScript">
        function check(theForm) {
            if (theForm.firstname.value == "") {
                alert("Please enter a value for the \"First Name\" field.");
                theForm.firstname.focus();
                return (false);
            }
            if (theForm.lastname.value == "") {
                alert("Please enter a value for the \"Last Name\" field.");
                theForm.lastname.focus();
                return (false);
            }

            if (theForm.email.value == "") {
                alert("Please enter a value for the \"E-mail\" field.");
                theForm.email.focus();
                return (false);
            }
            if (theForm.email.value.indexOf("@") < 0 || theForm.email.value.indexOf(".") < 0) {
                alert("Your \"E-mail\" is invalid");
                theForm.email.focus();
                return (false);
            }

            var check_term = document.form.check_term.checked;
            if (check_term == "") {
                alert('Terms  Conditions is required.');
                form.check_term.focus();
                return (false);
            }

            if (theForm.verify_code.value == "") {
                alert("Please enter a value for the \"Verify Code\" field.");
                theForm.verify_code.focus();
                return (false);
            }
            return (true);
        }
    </script>

    <table width="1000" align="center" border="0" cellspacing="1" cellpadding="3" bgcolor="#ffffff">
        <tr bgcolor="#cccccc">
            <td align="left"><strong>&nbsp;PERSONAL INFORMATION</strong></td>
        </tr>
    </table>
    <form method="POST" action="save_cart.php" onSubmit="return check(this)" name="form" id="form">
        <input type="hidden" name="sum_total_price" value="<?= $sum_total_price ?>"/>

        <?php
        if (!isset($client_id)) { ?>

            <table class="tablee1" width="1000" align="center" border="0" cellspacing="1" cellpadding="3"
                   bgcolor="#EEEEEE">
                <tr>
                    <td width="30%" align="right"><strong>Title Name : &nbsp;</strong></td>
                    <td width="70%">
                        <select class="form-control" name="titlename_id" style="width:300px;">
                            <?php listbox('lis_titlename', 'lis_id', 'lis_name', $titlename_id, 'N'); ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td width="30%" align="right"><strong style="color:#FF0000;"> * </strong><strong>First Name
                            : &nbsp;</strong></td>
                    <td width="70%">
                        <input class="form-control" type="text" name="firstname" style="width:300px;"></td>
                </tr>
                <tr>
                    <td width="30%" align="right"><strong style="color:#FF0000;"> * </strong><strong>Last Name
                            : &nbsp;</strong></td>
                    <td width="70%">
                        <input class="form-control" type="text" name="lastname" style="width:300px;"></td>
                </tr>
                <tr>
                    <td align="right"><strong style="color:#FF0000;"> * </strong><strong>Email Address : &nbsp;</strong>
                    </td>
                    <td><input class="form-control" type="text" name="email" style="width:300px;"></td>
                </tr>
                <tr>
                    <td align="right"><strong>Phone Number : &nbsp;</strong></td>
                    <td><input class="form-control" type="text" name="phone" style="width:300px;"></td>
                </tr>

                <tr>
                    <td align="right"><strong>Fax Number : &nbsp; </strong></td>
                    <td><input class="form-control" type="text" name="fax_number" style="width:300px;"></td>
                </tr>

                <tr>
                    <td align="right"><strong>Address : &nbsp;</strong></td>
                    <td><textarea class="form-control" name="address" rows="6" style="width:60%;"></textarea></td>
                </tr>

                <tr>
                    <td align="right"><strong>Remark : &nbsp;</strong></td>
                    <td><textarea class="form-control" name="comment" rows="6" style="width:60%;"></textarea></td>
                </tr>

                <tr>
                    <td align="right"><strong>Booking By : &nbsp;</strong></td>
                    <td><input class="form-control" type="text" name="book_by" style="width:300px;"></td>
                </tr>
                <?php if (isset($_SESSION['agents']['ag_id'])) { ?>
                    <tr>
                        <td align="right"><strong>Agent Voucher : &nbsp;</strong></td>
                        <td><input class="form-control" class="form-control" type="text" name="agent_voucher"
                                   style="width:300px;"></td>
                    </tr>

                    <tr>
                        <td align="right"><strong>Cash Collection : &nbsp;</strong></td>
                        <td><input class="form-control" type="text" name="cash_collection" style="width:300px;"></td>
                    </tr>
                <?php } ?>
                <tr>
                    <td align="right"></td>
                    <td><input type="checkbox" name="check_term" id="check_term" value="1"> I have read and accept all
                        <a href="files_upload/Terms_Condition_Tour.pdf" target="_blank">Terms and Conditions</a>.
                    </td>
                </tr>
                <tr>
                    <td align="right"><font color="red">*</font> Verify&nbsp;Code&nbsp;:&nbsp;</td>
                    <td valign="top"><img src="./captcha/captcha.php" align="absmiddle"/>
                        <input class="form-control" type="text" name="verify_code" id="verify_code" maxlength="5"
                               size="5" style="width:300px;"/>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td> * Please enter the code shown above in image format.</td>
                </tr>
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2" align="center"><input class="btn btn-success" name="submit" type="submit"
                                                          value="Submit">&nbsp;<input class="btn btn-default"
                                                                                      name="reset" type="reset"
                                                                                      value="Reset">
                    </td>
                </tr>
            </table>

        <?php } else { // END if($client_id){
            $sql_client = "SELECT * FROM clients WHERE cl_id = '$client_id' ";
            $result_client = mysql_query($sql_client);
            $num_client = mysql_num_rows($result_client);
            $row_client = mysql_fetch_array($result_client);

            $client_titlename = get_value('lis_titlename', 'lis_id', 'lis_name', $row_client['titlename_id']); ?>

            <table class="tablee1" border="0" width="1000" align="center" cellspacing="1" cellpadding="3"
                   bgcolor="#EEEEEE">

                <tr>
                    <td width="30%" align="right"><strong>Title Name :&nbsp; </strong></td>
                    <td width="70%"><input class="form-control" type="text" name="titlename" style="width:300px;"
                                           value="<?= $client_titlename ?>" readonly="readonly"></td>
                    <input type="hidden" name="titlename_id" value="<?= $row_client['titlename_id'] ?>"/>
                </tr>
                <tr>
                    <td width="30%" align="right"><strong>First Name :&nbsp; </strong></td>
                    <td width="70%">
                        <input class="form-control" type="text" name="firstname" style="width:300px;"
                               value="<?= $row_client['cl_name'] ?>" readonly="readonly"></td>
                </tr>
                <tr>
                    <td width="30%" align="right"><strong>Last Name : &nbsp; </strong></td>
                    <td width="70%">
                        <input class="form-control" type="text" name="lastname" style="width:300px;"
                               value="<?= $row_client['cl_lname'] ?>" readonly="readonly"></td>
                </tr>
                <tr>
                    <td align="right"><strong>Email Address : &nbsp; </strong></td>
                    <td><input class="form-control" type="text" name="email" style="width:300px;"
                               value="<?= $row_client['cl_email'] ?>" readonly="readonly"></td>
                </tr>


                <tr>
                    <td align="right"><strong>Phone Number : &nbsp; </strong></td>
                    <td><input class="form-control" type="text" name="phone" style="width:300px;"
                               value="<?= $row_client['cl_phone'] ?>" readonly="readonly"></td>
                </tr>

                <tr>
                    <td align="right"><strong>Fax Number : &nbsp; </strong></td>
                    <td><input class="form-control" type="text" name="fax_number" style="width:300px;"
                               value="<?= $row_client['cl_fax'] ?>" readonly="readonly"></td>
                </tr>

                <tr>
                    <td align="right"><strong>Address : &nbsp; </strong></td>
                    <td><textarea class="form-control" name="address" rows="6" style="width:60%;"
                                  readonly="readonly"><?= $row_client['cl_addr1'] ?></textarea></td>
                </tr>

                <tr>
                    <td align="right"><strong>Remark : &nbsp; </strong></td>
                    <td><textarea class="form-control" name="comment" rows="6" style="width:60%;"></textarea></td>
                </tr>

                <tr>
                    <td align="right"><strong>Booking By : &nbsp; </strong></td>
                    <td><input class="form-control" type="text" name="book_by" style="width:300px;"
                               value="<?= $client_titlename ?> <?= $row_client['cl_name'] ?> <?= $row_client['cl_lname'] ?>"
                               readonly="readonly"></td>
                </tr>
                <tr>
                    <td align="right"></td>
                    <td><input type="checkbox" name="check_term" id="check_term" value="1"> I have read and accept all
                        <a href="files_upload/Terms_Condition_Tour.pdf">Terms and Conditions</a>.
                    </td>
                </tr>

                <tr>
                    <td align="right"><font color="red">*</font>Verify&nbsp;Code&nbsp;:&nbsp;</td>
                    <td valign="top"><img src="./captcha/captcha.php" align="absmiddle"/>
                        <input class="form-control" type="text" name="verify_code" id="verify_code" maxlength="5"
                               size="5" style="width:300px;"/>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td> * Please enter the code shown above in image format.</td>
                </tr>
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2" align="center"><input class="btn btn-success" name="submit" type="submit"
                                                          value="Submit">&nbsp;<input class="btn btn-default"
                                                                                      name="reset" type="reset"
                                                                                      value="Reset"></td>
                </tr>
            </table>

        <?php } ?>

    </form>


<?php
}else{ // END if ($num_of_items > 0){
?>
    <table width="1000" align="center" border="0" cellspacing="1" cellpadding="3" bgcolor="#ffffff">
        <tr bgcolor="#eeeeee">
            <td align="center" colspan="3"><strong> ---- Empty Cart. ---- </strong></td>
        </tr>
    </table>
    <?php
} // END }else{ // END if ($num_of_items > 0){
?>


<br/>
<!-- include footer-->
<?php include("inc_footer.php"); ?>
<!-- end include footer -->


</body>
</html>
