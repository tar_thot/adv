<?php include("./administrator/lib/connect.php"); ?>
<?php include("./administrator/lib/function.php"); ?>
<?php include("./administrator/lib/constant.php"); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Best price Package, Hotels, Transfer, Air Tickets</title>
    <meta name="description" content="ADV Tour., Phi Phi Island Tour & Transfer operator, Hotel & Transport arrangement in the south of Thailand
"/>
    <meta name="keywords"
          content="Ko Tao Island, Samui Island, Phi Phi Island, Ferries, Boat Transfer, Tours, Package, Hotels, Diving, Liveaboard, Thailand"/>
    <meta name="Classification" content="World wide tour operate">
    <meta name="Author" content="S.S.ADV. Co., Ltd. Bangkok,Thailand">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link href="css/font.css" rel="stylesheet" type="text/css"/>

    <!-- menu slide -->
    <script type="text/javascript" src="js/script.js"></script>
    <!-- menu slide -->
    <!-- js-vallenato -->
    <script type="text/javascript" src="js-vtip/jquery-1.3.2.min.js"></script>
    <script type="text/javascript" src="js-vtip/jquery-plugin-vtip.js"></script>
    <script src="js-vallenato/vallenato.js" type="text/javascript" charset="utf-8"></script>
    <link rel="stylesheet" href="js-vallenato/vallenato.css" type="text/css" media="screen" charset="utf-8">
    <!-- js-vallenato -->
</head>

<body>
<!-- include top login-->
<?php include("inc_login.php"); ?>
<!-- end include top login -->

<!-- include logo & menu-->
<?php include("inc_menu.php"); ?>
<!-- end include logo & menu -->


<!-- Detail -->
<br/>
<?php
$combo_product_category_id = isset($_POST['combo_product_category_id']) ? $_POST['combo_product_category_id'] : null;
$pac_name = isset($_POST['pac_name']) ? $_POST['pac_name'] : null;
$pac_ref = isset($_POST['pac_ref']) ? $_POST['pac_ref'] : null;
$pac_forsearch_id = isset($_POST['pac_forsearch_id']) ? $_POST['pac_forsearch_id'] : null;
?>
<!--  Search  -->
<?php include('inc_search_package.php'); ?>
<table width="1000" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
        <td width="500" align="left" valign="top"><span class="h1"><img src="images/pic_under_line.jpg" width="200"
                                                                        height="15" border="0"/></span></td>
        <td align="right"><span class="h1"><img src="images/pic_under_line1.jpg" width="200" height="15"
                                                border="0"/></span></td>
    </tr>
</table>
<!--  Search  -->

<?php
//เพิ่มเติม 10/08/58======================================
while (list($xVarName, $xVarvalue) = each($_GET)) {
    ${$xVarName} = $xVarvalue;
}


while (list($xVarName, $xVarvalue) = each($_POST)) {
    ${$xVarName} = $xVarvalue;
}

while (list($xVarName, $xVarvalue) = each($_FILES)) {
    ${$xVarName . "_name"} = $xVarvalue['name'];
    ${$xVarName . "_type"} = $xVarvalue['type'];
    ${$xVarName . "_size"} = $xVarvalue['size'];
    ${$xVarName . "_error"} = $xVarvalue['error'];
    ${$xVarName} = $xVarvalue['tmp_name'];
}
//เพิ่มเติม 10/08/58======================================

$sql = "SELECT * ";
$sql .= "FROM packages ";
$sql .= "where pac_id > 0 ";

if (isset($combo_product_category_id)) {
    $sql .= "AND combo_product_category_id = '$combo_product_category_id' ";
}
if (isset($pac_name)) {
    $sql .= "AND pac_name LIKE '%$pac_name%' ";
}
if (isset($pac_ref)) {
    $sql .= "AND pac_ref LIKE '%$pac_ref%' ";
}
if (isset($pac_forsearch_id)) {
    $sql .= "AND pac_forsearch_id_arr LIKE '%$pac_forsearch_id%' ";
}


$sql .= "order by pac_name ASC ";
//echo "sql : $sql <br />";

//$wlinks = "&p_id=$p_id&starrate_id=$starrate_id&pricerange_id=$pricerange_id";
$wlinks = "&combo_product_category_id=$combo_product_category_id";

if (isset($pac_name)) {
    $wlinks .= "&pac_name=$pac_name";
}
if (isset($pac_ref)) {
    $wlinks .= "&pac_ref=$pac_ref";
}
if (isset($pac_forsearch_id)) {
    $wlinks .= "&pac_forsearch_id=$pac_forsearch_id";
}

$start = isset($_GET['start']) ? $_GET['start'] : 0;

$limit = '10'; // แสดงผลหน้าละกี่หัวข้อ
$Qtotal = mysql_query($sql);
$total = mysql_num_rows($Qtotal); // หาจำนวน record

$sql .= "LIMIT $start,$limit ";
$results = mysql_query($sql);
$totalp = mysql_num_rows($results); // หาจำนวน record ที่เรียกออกมา

/* อันนี้ ไม่เกี่ยวเอาออกได้ */
//printf("มีหัวข้อทั้งหมด %d หัวข้อ / ",$total);
//printf("แสดงหน้าละ %d หัวข้อ<br />",$totalp);
//echo "<hr />";

/* ตัวแบ่งหน้า */
$page = ceil($total / $limit); // เอา record ทั้งหมด หารด้วย จำนวนที่จะแสดงของแต่ละหน้า
//echo "$page <hr />";

/* เอาผลหาร มาวน เป็นตัวเลข เรียงกัน เช่น สมมุติว่าหารได้ 3 เอามาวลก็จะได้ 1 2 3 */
if (isset($_GET['page'])) {
    $get_page = $_GET['page'];
} else {
    $get_page = 1;
}


?>


<!-- Detail -->
<table width="1000" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
        <td height="40" bgcolor="#FFFFFF" class="h1"><img src="images/blank.gif" width="10" height="40" border="0"
                                                          align="absmiddle"><font color="#1e73a4">S.S.ADV.: </font><font
                color="000000">Combo Product</font></td>
        <td width="250" align="center" bgcolor="#FFFFFF">Data Found : <?= $total ?> Record(s)</td>
    </tr>
</table>
<table width="1000" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
        <td width="500" align="left" valign="top"><span class="h1"><img src="images/pic_under_line.jpg" width="200"
                                                                        height="15" border="0"/></span></td>
        <td align="right"><span class="h1"><img src="images/pic_under_line1.jpg" width="200" height="15"
                                                border="0"/></span></td>
    </tr>
</table>

<!-- Sort by -->

<!-- Sort by -->
<!-- Product-->


<table width="1001" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
        <td width="490"></td>
        <td width="20"></td>
        <td width="490" valign="top"></td>
    </tr>

    <?php

    while ($rows = mysql_fetch_array($results)) {

        ?>


        <tr>
            <td bgcolor="#FFFFFF">

                <!-- Product 01 -->
                <?php include('inc_package.php'); ?>
                <!-- Product 01 --></td>
            <td><img src="images/blank.gif" width="20" height="20" border="0"/></td>
            <?php
            if ($rows = mysql_fetch_array($results)) {
                ?>
                <td valign="top" bgcolor="#FFFFFF">
                    <!-- Product 01 -->
                    <?php include('inc_package.php'); ?>
                    <!-- Product 01 --></td>
                <?php

            }
            ?>
        </tr>

        <tr>
            <td><img src="images/blank.gif" width="20" height="10" border="0"/></td>
            <td><img src="images/blank.gif" width="20" height="10" border="0"/></td>
            <td valign="top"><img src="images/blank.gif" width="20" height="10" border="0"/></td>
        </tr>


        <?php
    }
    ?>
</table>
<!-- Product-->

<table width="1000" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
        <td><img src="images/blank.gif" width="20" height="10" border="0"/></td>
    </tr>
    <tr>
        <td valign="top" class="footdot"><img src="images/blank.gif" width="100" height="1" border="0"/></td>
    </tr>

    <tr>
        <td height="30" align="center"><?php include('inc_link_page_list.php'); ?></td>
    </tr>
    <tr>
        <td valign="top" class="footdot"><img src="images/blank.gif" width="100" height="1" border="0"/></td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
</table>

<!-- Detail -->


<!-- include footer-->
<?php include("inc_footer.php"); ?>
<!-- end include footer -->


</body>
</html>
